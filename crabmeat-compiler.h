/*!
 * @brief
 *    attempts to detect platform and compiler. we
 *    also set compiler specific and platform specific
 *    macros.
 *
 *    based on:
 *    http://sourceforge.net/apps/mediawiki/predef/index.php?title=Main_Page
 *    or
 *    http://sourceforge.net/p/predef/wiki/Compilers/
 *
 * @author
 *    steffen klatt (created on: sep 02, 2012)
 */

#ifndef  CRABMEAT_COMPILERS_H_
#define  CRABMEAT_COMPILERS_H_

/** compiler detection **/

/* clang (llvm), note: have before gcc because clang also identifies as gcc.. */
#if  defined(__clang__) /*|| defined(__llvm__)*/
#  define  CRABMEAT_COMPILER_CLANG
#  define  CRABMEAT_COMPILER_NAME "Clang"
/* pgi */
#elif  defined(__PGI)
#  define  CRABMEAT_COMPILER_PGI
#  define  CRABMEAT_COMPILER_NAME "PGI"
/* gnu c/c++ */
#elif  defined(__GNUC__) || defined(__GNUG__)
#  define  CRABMEAT_COMPILER_GCC
#  define  CRABMEAT_COMPILER_NAME "GCC"
/* intel */
#elif  defined(__INTEL_COMPILER) || defined(__ICL)
#  define  CRABMEAT_COMPILER_INTEL
#  define  CRABMEAT_COMPILER_NAME "Intel"
/* ms c */
#elif  defined(_MSC_VER)
#  define  CRABMEAT_COMPILER_MSC
#  define  CRABMEAT_COMPILER_NAME "MSVC"
/* ? */
#else
        /* we wish you the best of luck */
#endif

/* mingw */
#if  defined(__MINGW32__) || defined(__MINGW64__)
#  define  CRABMEAT_MINGW
#endif



/* mechanism to enable/disable suppression of warnings */
#ifndef  CRABMEAT_SUPPRESS_WARNINGS_NOT_OK
#  define  CRABMEAT_SUPPRESS_WARNINGS
#endif

/* disable warnings */
#ifdef  CRABMEAT_SUPPRESS_WARNINGS
#  if  defined(CRABMEAT_COMPILER_GCC)
        /* suppress g++ warnings here (older (before 4.2) do not support this so there is no point in using it.) */
#  elif  defined(CRABMEAT_COMPILER_MSC)
        /* suppress ms-c++ warnings here */
#    ifndef  _CRT_SECURE_NO_WARNINGS
#      define  _CRT_SECURE_NO_WARNINGS
#    endif
#    ifndef  _SCL_SECURE_NO_WARNINGS
#      define  _SCL_SECURE_NO_WARNINGS
#    endif
#    pragma warning (disable:4001)  /* allowing single line comments */
#    pragma warning (disable:4127)  /* conditional expression is constant */
#    pragma warning (disable:4189)  /* local variable is initialized but not referenced */
#    pragma warning (disable:4244)  /* conversion from 'type1' to 'type2', possible loss of data */
#    pragma warning (disable:4221)  /* no public symbols found */
#    pragma warning (disable:4250)  /* inherits 'class2::member' via dominance */
#    pragma warning (disable:4251)  /* needs to have dll-interface to be used by clients of class */
/*#    pragma warning (disable:4389) */  /* signed/unsigned mismatch */
#    pragma warning (disable:4505)  /* unreferenced local function has been removed */
#    pragma warning (disable:4512)  /* assignment operator could not be generated */
#    pragma warning (disable:4514)  /* unreferenced inline function has been removed */
/*#    pragma warning (disable:4548) */  /* expression before comma has no effect; expected expression with side-effect */
#    pragma warning (disable:4625)  /* copy constructor could not be generated because a base class copy constructor is inaccessible */
#    pragma warning (disable:4626)  /* assignement operator could not be generated because a base class assignement operator is inaccessible */
#    pragma warning (disable:4640)  /* local static object is not thread safe */
#    pragma warning (disable:4702)  /* unreachable code (TODO  compilers have different opinions here..) */
#    pragma warning (disable:4706)  /* assignment within conditional expression */
#    pragma warning (disable:4710)  /* function not inlined */
#    pragma warning (disable:4800)  /* forcing value to bool 'true' or 'false' (performance warning) */
#    pragma warning (disable:4820)  /* bytes padding added after construct 'member_name' */
#    pragma warning (disable:4996)  /* was declared deprecated */
#  else
        /* ... */
#  endif
#endif  /*  CRABMEAT_SUPPRESS_WARNINGS  */

/* additionally disable warnings that are easy to fix
 *
 * ~~~  NOT A GOOD IDEA!  ~~~
 *
 * needed for building native mobile2d modules
 * which are extremely messy.
 */
#ifdef  CRABMEAT_SUPPRESS_ESSENTIAL_WARNINGS
#  if  defined(CRABMEAT_COMPILER_GCC)
        /* suppress essential g++ warnings here */
#  elif  defined(CRABMEAT_COMPILER_MSC)
        /* suppress essential ms-c++ warnings here */
#    pragma warning (disable:4189)  /* local variable is initialized but not referenced */
#  else
        /* ... */
#  endif
#endif  /*  CRABMEAT_SUPPRESS_ESSENTIAL_WARNINGS  */


#endif  /*  !CRABMEAT_COMPILERS_H_  */

