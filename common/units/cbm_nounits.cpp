/*!
 * @brief
 */

#include  "units/cbm_units.h"

cbm_units_t::cbm_units_t( char const * /*units db*/)
    : m_usys( NULL)
    { }

cbm_units_t::~cbm_units_t()
    { }

bool
cbm_units_t::convertible(
        cbm_unit_t /*unit*/, cbm_unit_t /*unit*/) const
    { return true; }
bool
cbm_units_t::equal(
        cbm_unit_t /*unit*/, cbm_unit_t /*unit*/) const
    { return true; }

cbm_unit_t
cbm_units_t::parse_unit( char const * /*unit*/)
    { return cbm_unit_t(); }
int
cbm_units_t::parse_unit( cbm_unit_t * /*unit buffer*/,
        char const * /*unit*/)
    { return  0; }
std::string
cbm_units_t::symbols( cbm_unit_t /*unit*/)
    { return  std::string(); }
int
cbm_units_t::convert_float( float * /*target data*/, cbm_unit_t /*target unit*/,
    float const * /*source data*/, cbm_unit_t /*source unit*/, size_t /*data size*/)
    { return 0; }
int
cbm_units_t::convert_double( double * /*target data*/, cbm_unit_t /*target unit*/,
    double const *  /*source data*/, cbm_unit_t /*source unit*/, size_t /*data size*/)
    { return 0; }

int
cbm_units_t::initialize( char const * /*units db*/)
    { return 0; }
void
cbm_units_t::finalize()
    { }

