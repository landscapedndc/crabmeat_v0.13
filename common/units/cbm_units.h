/*!
 * @brief
 *  provides interface to units parsing and conversion
 *
 * @author
 *  steffen klatt (created on: nov. 15, 2016)
 */

#ifndef  CRABMEAT_UNITS_H_
#define  CRABMEAT_UNITS_H_

#include  <string>

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

/* opaque type */
struct CBM_API cbm_unit_t
{
#ifdef  CBM_UNITS
    int  u;
#endif
};

class CBM_API cbm_units_t
{
public:
    static int  initialize(
        char const * /*default units database*/);
    static void  finalize();

    cbm_units_t(
        char const * = NULL /*custom units database*/);
    ~cbm_units_t();

    /* Dimensionless unit is represented by
     *  - a NULL pointer,
     *  - an empty string or,
     *  - a hyphen ("-")
     */
    cbm_unit_t  parse_unit( char const * /*unit*/);
    int  parse_unit( cbm_unit_t *, char const * /*unit*/);

    std::string  symbols( cbm_unit_t);
    bool  convertible( cbm_unit_t, cbm_unit_t) const;
    bool  equal( cbm_unit_t, cbm_unit_t) const;

    int  convert_float( float * /*target data*/, cbm_unit_t,
        float const * /*source data*/, cbm_unit_t, size_t /*data size*/);
    int  convert_double( double * /*target data*/, cbm_unit_t,
        double const * /*source data*/, cbm_unit_t, size_t /*data size*/);

private:
    void *  m_usys;

private:
    /* hide */
    cbm_units_t( cbm_units_t const &);
    cbm_units_t &  operator=( cbm_units_t const &);
};

#endif  /*  !CRABMEAT_UNITS_H_  */

