/*!
 * @brief
 */

#include  <udunits2.h>
#include  <cstring>

#include  "units/cbm_units.h"
#include  "memory/cbm_mem.h"
#include  "log/cbm_baselog.h"


static ut_system *  cbm_default_usys = 0;
#define  CBM_AsUtSystem(ptr) ((ut_system*)(ptr))

static ut_unit **  cbm_ustore;
#define  cbm_uindex(u) ((u).u)
static cv_converter **  cbm_cstore;
#define  cbm_cindex(s,t) ((s).u*cbm_ustore_sz+(t).u)
static int  cbm_ustore_sz;

cbm_units_t::cbm_units_t( char const *  _units_db)
    : m_usys( NULL)
{
    if ( _units_db)
    {
        this->m_usys = ut_read_xml( _units_db);
        if ( !this->m_usys)
            { CBM_LogError( "Failed to read units database;",
                   " falling back to default"); }
    }

    if ( !this->m_usys)
        { this->m_usys = cbm_default_usys; }
}

cbm_units_t::~cbm_units_t()
{
    if ( this->m_usys
            && this->m_usys != cbm_default_usys)
        { ut_free_system( CBM_AsUtSystem(this->m_usys)); }
}

bool
cbm_units_t::convertible(
        cbm_unit_t  _unit1, cbm_unit_t  _unit2)
const
{
    CBM_Assert( cbm_ustore[_unit1.u]);
    CBM_Assert( cbm_ustore[_unit2.u]);
    if ( ut_are_convertible( cbm_ustore[_unit1.u],
                cbm_ustore[_unit2.u]))
        { return true; }
    return false;
}

bool
cbm_units_t::equal(
        cbm_unit_t  _unit1, cbm_unit_t  _unit2)
const
{
    if ( this->convertible( _unit1, _unit2)
        && ut_compare( cbm_ustore[_unit1.u],
                    cbm_ustore[_unit2.u]) == 0)
        { return true; }
    return false;
}

static
int  resize_unit_and_converter_stores( int  _new_slot)
{
    if ( cbm_ustore_sz > _new_slot)
        { return 0; }

    int  new_ustore_sz = _new_slot + 4; /*increase by four slots */
    ut_unit **  new_ustore =
        (ut_unit**)CBM_Allocate( sizeof(ut_unit*) * new_ustore_sz);
    if ( !new_ustore)
        { return -1; }

    cv_converter **  new_cstore = (cv_converter**)CBM_Allocate(
            sizeof(cv_converter*) * new_ustore_sz * new_ustore_sz);
    if ( !new_cstore)
    {
        /*leave consistent state..*/
        CBM_Free( new_ustore);
        return  -1;
    }

    /* initialize and copy contents */
    memset( new_ustore, 0, sizeof(ut_unit*) * new_ustore_sz);
    memcpy( new_ustore, cbm_ustore, sizeof(ut_unit*) * cbm_ustore_sz);

    CBM_Free( cbm_ustore);
    /* update unit store */
    cbm_ustore = new_ustore;

    /* initialize and copy contents */
    memset( new_cstore, 0, sizeof(cv_converter*)*new_ustore_sz*new_ustore_sz);
    for ( int  j = 0; j < cbm_ustore_sz;  ++j) {
        for ( int  k = 0; k < cbm_ustore_sz;  ++k)
        {
// sk:dbg            CBM_LogDebug( "moving conv #",j*cbm_ustore_sz+k,
// sk:dbg                    " to #",j*new_ustore_sz+k);
            new_cstore[j*new_ustore_sz+k] =
                cbm_cstore[j*cbm_ustore_sz+k];
        }
    }

    CBM_Free( cbm_cstore);
    /* update converter store */
    cbm_cstore = new_cstore;

    /* update store sizes */
    cbm_ustore_sz = new_ustore_sz;

    return 0;
}

static
ut_unit *  find_unit(
        ut_unit *  _unit, int *  _slot)
{
    int  k = 0;
    ut_unit *  unit_buffered = NULL;
    for ( ;  k < cbm_ustore_sz;  ++k)
    {
        /* do NOT support deleting unit entries from store */
        ut_unit *  unit = cbm_ustore[k];
        if ( !unit || (( ut_compare( _unit, unit) == 0)
                && ut_are_convertible( _unit, unit)))
        {
            unit_buffered = unit;
            break;
        }
    }

    *_slot = k;
    return unit_buffered;
}
static
int  insert_new_unit( ut_unit *  _unit)
{
    CBM_Assert( _unit!=0);

    int  slot = -1;
    ut_unit *  new_unit = find_unit( _unit, &slot);
    if ( new_unit != 0)
        { ut_free( _unit); CBM_Assert( slot>=0); CBM_LogDebug("reusing unit #",slot); }
    else
    {
        int  rc = resize_unit_and_converter_stores( slot);
        if ( rc)
            { return -1; }
        cbm_ustore[slot] = _unit;
    }

    return slot;
}

cbm_unit_t
cbm_units_t::parse_unit( char const *  _unit)
{
    cbm_unit_t  cbmunit;
    (void)this->parse_unit( &cbmunit, _unit);
    return cbmunit;
}
int
cbm_units_t::parse_unit( cbm_unit_t *  _unit_buf,
        char const *  _unit)
{
    cbm_unit_t  cbmunit;
    cbmunit.u = -1;

/* TODO  trim whitespace from _unit (ut_trim) */
    ut_unit *  unit = NULL;
    if ( _unit && ( *_unit!='\0') && ( *_unit!='-'))
        { unit = ut_parse( CBM_AsUtSystem(this->m_usys), _unit, UT_ASCII); }
    else
        { unit = ut_get_dimensionless_unit_one( CBM_AsUtSystem(this->m_usys)); }
    if ( unit)
    {
        int const  slot = insert_new_unit( unit);
        CBM_Assert(slot>=0);
        cbmunit.u = slot;
    }

    if ( _unit_buf)
        { _unit_buf->u = cbmunit.u; }

    if ( cbmunit.u == -1)
        { return -1; /*error*/ }
    return  0;
}
std::string
cbm_units_t::symbols( cbm_unit_t  _unit)
{
    char  buf[128];
    int  len = ut_format( cbm_ustore[_unit.u],
            buf, sizeof(buf), UT_ASCII);
    if ( len == -1)
        { return std::string( "ERROR"); }
    return  std::string( buf);
}
static
cv_converter const *
find_converter( cbm_unit_t  _unit_src, cbm_unit_t  _unit_tgt)
{
    if ( !cbm_cstore)
        { return NULL; }
    CBM_Assert(cbm_cindex(_unit_src,_unit_tgt)<(cbm_ustore_sz*cbm_ustore_sz));
    cv_converter *  new_converter =
            cbm_cstore[cbm_cindex(_unit_src,_unit_tgt)];
    if ( !new_converter)
    {
        cbm_cstore[cbm_cindex(_unit_src,_unit_tgt)] =
            ut_get_converter( cbm_ustore[_unit_src.u], cbm_ustore[_unit_tgt.u]);
        new_converter =
            cbm_cstore[cbm_cindex(_unit_src,_unit_tgt)];
    }
    else
        { /* CBM_LogDebug( "reusing converter #",cbm_cindex(_unit_src,_unit_tgt)); */ }
    return new_converter;
}
int
cbm_units_t::convert_float( float *  _data_tgt, cbm_unit_t  _unit_tgt,
    float const *  _data_src, cbm_unit_t  _unit_src, size_t  _data_sz)
{
    cv_converter const *  converter =
            find_converter( _unit_src, _unit_tgt);
    if ( !converter)
        { return -1; }

    cv_convert_floats( converter, _data_src, _data_sz, _data_tgt);
    return 0;
}
int
cbm_units_t::convert_double( double *  _data_tgt, cbm_unit_t  _unit_tgt,
    double const *  _data_src, cbm_unit_t  _unit_src, size_t  _data_sz)
{
    cv_converter const *  converter =
            find_converter( _unit_src, _unit_tgt);
    if ( !converter)
        { return -1; }

    cv_convert_doubles( converter, _data_src, _data_sz, _data_tgt);
    return 0;
}

// sk:off static int
// sk:off ut_error_handler_quiet(
// sk:off     char const * const, va_list)
// sk:off     { return 0; }
#include  "string/cbm_string.h"
static int
ut_error_handler_redirect(
    char const * const  _fmt, va_list  _args)
{
    char  err_msg[1024];
    (void)cbm::vsnprintf(
            err_msg, sizeof(err_msg), _fmt, _args);
    CBM_LogError( "udunits: ", err_msg);
    return 0;
}
int
cbm_units_t::initialize( char const *  _units_db)
{
    ut_set_error_message_handler( ut_error_handler_redirect);

    cbm_ustore = NULL;
    cbm_ustore_sz = 0;
    cbm_cstore = NULL;

    cbm_default_usys = ut_read_xml( _units_db);
    if ( cbm_default_usys == NULL)
        { return -1; }
    return 0;
}
void
cbm_units_t::finalize()
{
    for ( int k = 0; k < cbm_ustore_sz; ++k)
        { if ( cbm_ustore[k])
            { ut_free( cbm_ustore[k]); } }
    CBM_Free( cbm_ustore);
    for ( int k = 0;  k < cbm_ustore_sz*cbm_ustore_sz; ++k)
        { if ( cbm_cstore[k])
            { cv_free( cbm_cstore[k]); } }
    CBM_Free( cbm_cstore);

    if ( cbm_default_usys)
        { ut_free_system( cbm_default_usys); }
}

