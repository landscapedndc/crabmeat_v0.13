/*!
 * @brief
 *    ldndc object ID
 *
 * @author 
 *    steffen klatt  (created on: mar 26, 2013)
 */

#ifndef  CBM_ID_H_
#define  CBM_ID_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"
#include  "cbm_types.h"

/* ldndc entity ID (e.g. identifier for kernels in input sources) */
typedef  ldndc_uint32_t  lid_t;
#define  LDNDC_ID  LDNDC_UINT32
#define  LDNDC_ID_BYTESIZE  (sizeof(lid_t))

/* invalid object ID */
extern CBM_API lid_t const  invalid_lid;

#ifndef  LDNDC_PRINTF_SEQ_ID
#  define  LDNDC_PRINTF_SEQ_ID  LDNDC_PRINTF_SEQ_UINT32
#endif

namespace cbm {

class  CBM_API  objectid_owner_interface_t
{
public:
    virtual  lid_t const &  object_id() const = 0;

    virtual  void  set_object_id( lid_t const &) = 0;

protected:
    virtual  ~objectid_owner_interface_t();
};

class  CBM_API  objectid_owner_t  :  public  objectid_owner_interface_t
{
public:
    virtual  lid_t const &  object_id() const;

    virtual  void  set_object_id( lid_t const &);

protected:
    objectid_owner_t();
    objectid_owner_t(
            lid_t const &  /*id*/);
    virtual  ~objectid_owner_t();

private:
    lid_t  m_objectid;
};

struct CBM_API source_descriptor_t
{
    /*! core block id */
    lid_t  block_id;
    /*! identifier of input source stream */
    ldndc_string_t  source_identifier;
};

int
CBM_API
set_source_descriptor(
        cbm::source_descriptor_t * /*buffer*/,
        lid_t const & /*block id*/,
        char const * /*source identifier*/);
void
CBM_API
set_source_descriptor_defaults(
        cbm::source_descriptor_t * /*buffer*/,
        char const * /*class*/,
        lid_t const & = invalid_lid /*block id*/);

std::ostream CBM_API &
operator<<( std::ostream &,
        cbm::source_descriptor_t const &);


struct  CBM_API  neighbor_t
{
    cbm::source_descriptor_t  desc;
    double  intersect;
};

} /* namespace cbm */

#endif  /*  !CBM_ID_H_  */

