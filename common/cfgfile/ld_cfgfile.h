/*!
 * @brief
 *    interface to the LandscapeDNDC configuration file
 *
 * @author
 *    steffen klatt (created on: dec 27, 2011),
 *    edwin haas
 */

#ifndef  LD_CFGFILE_H_
#define  LD_CFGFILE_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

#include  <map>

#define  cbm_config_block_environment  "environment"
#define  cbm_config_block_global  "global"

namespace  ldndc
{
    /*!
     * @brief
     *    structure representing values in global block
     *
     * @note
     *    when adding items to this structure, add...:
     *    
     *    @li
     *        initialization (see @fn init_property_in_global_section
     *        and @fn init_property_given_in_global_section)
     *    @li
     *        default value (see @fn set_property_in_global_section_from_defaults)
     *    @li
     *        getter if applicable
     *    @li
     *        handler (see @fn set_property_in_global_section)
     *    @li
     *        to configuration dump (see @fn dump)
     */
    struct  conf_global_t
    {
        /*!  default setup input stream */
        std::string  project_file;
        int  project_file_given;
        /*!  log file name or known stream specifier (i.e. stdout, stderr, stdlog) */
        std::string  log_file;
        int  log_file_given;
        /*!  log append to existing */
        bool  log_append;
        int  log_append_given;
        /*!  log level */
        unsigned int  log_level;
        int  log_level_given;
        /*!  log targets mask, xor'd powers of two representing log targets */
        lflags_t  log_targets_mask;
        int  log_targets_mask_given;
        /*!  draw progress bar to screen */
        bool  progress_bar;
        int  progress_bar_given;

        /*!  have balance check: yes or no */
        bool  balance_check;
        int  balance_check_given;
        /*!  globally applied balance check tolerance (e.g. 1.0e-09) */
        double  balance_tolerance;
        int  balance_tolerance_given;

        /*! path to search for resources database */
        std::string  resources_path;
        int  resources_path_given;
        /*! path to search for udunits database */
        std::string  udunits_path;
        int  udunits_path_given;
        /*!  base path to read input files from */
        std::string  input_path;
        int  input_path_given;
        /*!  have output: yes or no */
        bool  output;
        int  output_given;
        /*!  base path to write output files to */
        std::string  output_path;
        int  output_path_given;

        /*! default format for output sinks */
        std::string  sink_format;
        int  sink_format_given;

        /*! URL to search for service registry */
        std::string  service_registry_url;
        int  service_registry_url_given;

        /*!  set random seed to use */
        unsigned int  random_seed;
        int  random_seed_given;
    };

    struct  conf_section_t
    {
        typedef  std::map< std::string, std::string >::const_iterator  iterator;
        /* key, value pairs */
        std::map< std::string, std::string >  key_val;
        
        iterator  begin() const
            { return  this->key_val.begin(); }
        iterator  end() const
            { return  this->key_val.end(); }
    };

    class  CBM_API  config_file_t
    {
        typedef  std::map< std::string, conf_section_t >  conf_sections_t;
        public:
            //typedef  typename conf_section_t::iterator  iterator;
            struct  iterator : public conf_section_t::iterator
            {
                iterator( conf_section_t::iterator const &  _i)
                    : conf_section_t::iterator( _i) {}
                char const *  key() const
                    { return  (*this)->first.c_str(); }
                char const *  value() const
                    { return  (*this)->second.c_str(); }
            };
            /* return ldndc default configuration file */
            static  std::string  default_filename(
                    std::string const * = NULL /*dot path*/);

        public:
            config_file_t( char const * = NULL);
            ~config_file_t();

            /*!
             * @brief
             *    read configuration file. sets global properties and
             *    reads user sections key:value pairs.
             */
            lerr_t  read();
            lerr_t  status() const
                { return  this->read_status; }

            lerr_t  set_filename( char const *);            
            char const *  filename() const
                { return  this->cfg_filename.c_str(); }

            void  dump() const;

            char const *  project_file() const
                { return  this->global_section.project_file.c_str(); }
            int  project_file_given() const
                { return  this->global_section.project_file_given; }

            char const *  log_file() const
                { return  this->global_section.log_file.c_str(); }
            int  log_file_given() const
                { return  this->global_section.log_file_given; }

            bool  log_append() const
                { return  this->global_section.log_append; }
            int  log_append_given() const
                { return  this->global_section.log_append_given; }

            unsigned int  log_level() const
                { return  this->global_section.log_level; }
            int  log_level_given() const
                { return  this->global_section.log_level_given; }

            lflags_t const &  log_targets_mask() const
                { return  this->global_section.log_targets_mask; }
            int  log_targets_mask_given() const
                { return  this->global_section.log_targets_mask_given; }

            bool  progress_bar() const
                { return  this->global_section.progress_bar; }
            int  progress_bar_given() const
                { return  this->global_section.progress_bar_given; }


            char const *  resources_path() const
                { return  this->global_section.resources_path.c_str(); }
            int  resources_path_given() const
                { return  this->global_section.resources_path_given; }

            char const *  udunits_path() const
                { return  this->global_section.udunits_path.c_str(); }
            int  udunits_path_path_given() const
                { return  this->global_section.udunits_path_given; }
        
            char const *  input_path() const
                { return  this->global_section.input_path.c_str(); }
            int  input_path_given() const
                { return  this->global_section.input_path_given; }

            bool  have_output() const
                { return  this->global_section.output; }
            int  output_given() const
                { return  this->global_section.output_given; }
            char const *  output_path() const
                { return  this->global_section.output_path.c_str(); }
            int  output_path_given() const
                { return  this->global_section.output_path_given; }
            char const *  sink_format() const
                { return  this->global_section.sink_format.c_str(); }
            int  sink_format_given() const
                { return  this->global_section.sink_format_given; }

            char const *  service_registry_url() const
                { return  this->global_section.service_registry_url.c_str(); }
            int  service_registry_url_given() const
                { return  this->global_section.service_registry_url_given; }

            bool  have_balance_check() const
                { return  this->global_section.balance_check; }
            int  balance_check_given() const
                { return  this->global_section.balance_check_given; }
            double  balance_tolerance() const
                { return  this->global_section.balance_tolerance; }
            int  balance_tolerance_given() const
                { return  this->global_section.balance_tolerance_given; }

            unsigned int  random_seed() const
                { return  this->global_section.random_seed; }
            int  random_seed_given() const
                { return  this->global_section.random_seed_given; }

            /*!
             * @brief
             *    query method. give section name (must match name
             *    used in config file's section specifier [<section name>].
             */
            template < typename  _T >
            lerr_t  query(
                    char const * /*section*/,
                    char const * /*key*/,
                    _T * /*result buffer*/) const;
            template < typename  _T >
            lerr_t  query(
                    char const * /*section*/,
                    char const * /*key*/,
                    _T * /*result buffer*/, _T const & /*default*/) const;


            iterator  begin( char const *  _cf_section) const
            {
                if ( _cf_section && !cbm::is_empty( _cf_section))
                {
                    conf_sections_t::const_iterator  section_it( this->custom_section.find( _cf_section));
                    if ( section_it != this->custom_section.end())
                        { return  section_it->second.key_val.begin(); }
                }
                return  this->end();
            }
            iterator  end( char const *  _cf_section = NULL) const
            {
                if ( _cf_section && !cbm::is_equal( _cf_section, cbm_config_block_global))
                {
                    conf_sections_t::const_iterator  section_it( this->custom_section.find( _cf_section));
                    if ( section_it != this->custom_section.end())
                        { return  section_it->second.key_val.end(); }
                }
                return  this->custom_section.find( cbm_config_block_global)->second.end();
            }

            iterator  begin_env() const
                { return  this->begin( cbm_config_block_environment); }
            iterator  end_env() const
                { return  this->end( cbm_config_block_environment); }

        private:
            /* name of configuration file */
            std::string  cfg_filename;
            /* exit status of last read attempt (if NONE, first call to read())  */
            lerr_t  read_status;

            /* properties from [global] section */
            conf_global_t  global_section;

            /* key:value pairs per custom section ([<section name>] blocks) */
            conf_sections_t  custom_section;

            lerr_t  set_property_in_section(
                    std::string const &, std::string const &, std::string const &);
            lerr_t  set_property_in_global_section(
                    std::string const &, std::string const &);

            void  init_property_in_global_section();
            void  init_property_given_in_global_section();

            /* if no filename was given fall back to hardcoded system defaults */
            lerr_t  set_property_in_global_section_from_defaults();
    };

    template < typename  _T >

    lerr_t
    config_file_t::query(
            char const *  _cf_section, char const *  _cf_key,
            _T *  _cf_value)
    const
    {
        if ( !_cf_section || !_cf_key || cbm::is_empty( _cf_section) || cbm::is_empty( _cf_key))
        {
            return  LDNDC_ERR_INVALID_ARGUMENT;
        }

        std::string const  cf_section( _cf_section);
        conf_sections_t::const_iterator  section_it( this->custom_section.find( cf_section));

        if ( section_it != this->custom_section.end())
        {
            std::string const  cf_key( _cf_key);
            if ( section_it->second.key_val.find( cf_key) != section_it->second.key_val.end())
            {
                if ( !_cf_value)
                {
                    return  LDNDC_ERR_OK;
                }
                std::string const  cf_value =
                    this->custom_section.find( cf_section)->second.key_val.find( cf_key)->second;
                return  cbm::s2n( cf_value, _cf_value);
            }
        }
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    template < typename _T >
    lerr_t
    config_file_t::query(
            char const *  _cf_section, char const *  _cf_key,
            _T * _cf_value, _T const &  _default)
    const
    {
        lerr_t const  rc_query = this->query( _cf_section, _cf_key, _cf_value);
        if ( rc_query == LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
        {
            if ( _cf_value)
                { *_cf_value = _default; }
        }
        return  rc_query;
    }
}

#endif  /*  !LD_CFGFILE_H_  */

