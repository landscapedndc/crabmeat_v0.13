/*!
 * @brief
 *    interface to the LandscapeDNDC configuration file
 *
 * @author
 *    steffen klatt (created on: dec 27, 2011),
 *    edwin haas
 */

#ifndef  CBM_CFGFILE_H_
#define  CBM_CFGFILE_H_

#include  "crabmeat-common.h"
#include  "cfgfile/ld_cfgfile.h"

namespace cbm {
    typedef  ldndc::config_file_t  config_file_t; }

#endif  /*  !CBM_CFGFILE_H_  */

