" Vim syntax file
" Language:	ldndc configuration files (ldndc.conf)
"
" based on the samba syntax file
"
" put the following in your vim.rc:
"
" au BufRead,BufNewFile ldndc.conf set filetype=ldndcconf
" au! syntax ldndcconf source conffile.vim

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

"syn case ignore

" output formats
syn keyword ldndcOutputFormats nc4 sqlite3 txt xml
" boolean values
syn keyword ldndcBoolean true false yes no 0 1 on off set unset
" log levels
syn keyword ldndcLoggingLevels silent fatal error warn info verbose debug


syn match ldndcParameter /^[a-zA-Z_ \t]\+=/ contains=ldndcKeyword
syn match ldndcQuotedValue /\"[^\"]\+\"/
syn match ldndcSection /^\s*\[[a-zA-Z0-9_\-:*.$ ]\+\]/
syn match ldndcComment /^\s*[;#].*/
syn match ldndcComment /^\s*\zs#.*$/
syn match ldndcComment /\s\zs#.*$/

" keywords for ldndc configuration file 'global' block
syn keyword ldndcKeyword contained project_file log_file log_level log_append progress_bar
syn keyword ldndcKeyword contained random_seed
syn keyword ldndcKeyword contained resources_path input input_path output output_path sink_format dump restore dump_file
syn keyword ldndcKeyword contained balance_check balance_tolerance

" keywords from module sys:climate:copy-to-state, etc
syn keyword ldndcKeyword contained allow_synthesize

" keywords from module test:airchemistry:dump, etc
syn keyword ldndcKeyword contained target header

" keywords from module test:*:eventlist, etc
syn keyword ldndcKeyword contained print_empty format_long

" keywords from module watercycle:dndc, etc
syn keyword ldndcKeyword contained potential_evaporation

" keywords from module physiology:photofarquhar, etc
syn keyword ldndcKeyword contained stomatal_conductance

if version >= 508 || !exists("did_ldndc_syn_inits")
  if version < 508
    let did_ldndc_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink ldndcKeyword   Type
  HiLink ldndcSection   Statement
  HiLink ldndcComment   Comment
  HiLink ldndcParameter Normal
  HiLink ldndcQuotedValue     String

  HiLink ldndcOutputFormats  PreProc
  HiLink ldndcBoolean   PreProc
  HiLink ldndcLoggingLevels   PreProc
  delcommand HiLink
endif

let b:current_syntax = "ldndc"

" vim: ts=8
