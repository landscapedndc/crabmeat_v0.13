/*!
 * @brief
 *    interface to the LandscapeDNDC configuration file (implementation)
 *
 * @author
 *    steffen klatt (created on: dec 28, 2011),
 *    edwin haas
 */

#include  "cfgfile/ld_cfgfile.h"
#include  "cbm_rtcfg.h"

#include  "log/cbm_baselog.h"

#include  "utils/lutils.h"
#include  "utils/lutils-fs.h"

#include  "string/lstring-basic.h"
#include  "string/lstring-compare.h"
#include  "string/lstring-convert.h"

#include  <stdio.h>

#include  "string/lstring-transform.h"
std::string
ldndc::config_file_t::default_filename(
        std::string const *  _dot_path)
{
    if ( _dot_path)
    {
        return  *_dot_path+cbm::CONFIG_FILENAME;
    }
    return  cbm::CONFIG_FILE;
}


ldndc::config_file_t::config_file_t(
        char const *  _cfg_filename)
        : cfg_filename( ""),
          read_status( LDNDC_ERR_NONE)
{
    this->set_filename( _cfg_filename);
    this->init_property_in_global_section();
}

ldndc::config_file_t::~config_file_t()
    { }


void
ldndc::config_file_t::init_property_in_global_section()
{
    this->global_section.project_file.clear();
    this->global_section.log_file.clear();
    this->global_section.log_append = 0;
    this->global_section.log_level = 0;
    this->global_section.progress_bar = 0;
    this->global_section.resources_path.clear();
    this->global_section.udunits_path.clear();
    this->global_section.input_path.clear();
    this->global_section.output = 0;
    this->global_section.output_path.clear();
    this->global_section.sink_format.clear();
    this->global_section.service_registry_url.clear();
    this->global_section.balance_check = 0;
    this->global_section.balance_tolerance = 0.0;
    this->global_section.random_seed = 0;

    this->init_property_given_in_global_section();
    this->set_property_in_global_section_from_defaults();
}
void
ldndc::config_file_t::init_property_given_in_global_section()
{
    this->global_section.project_file_given = 0;
    this->global_section.log_file_given = 0;
    this->global_section.log_append_given = 0;
    this->global_section.log_level_given = 0;
    this->global_section.progress_bar_given = 0;
    this->global_section.resources_path_given = 0;
    this->global_section.udunits_path_given = 0;
    this->global_section.input_path_given = 0;
    this->global_section.output_given = 0;
    this->global_section.output_path_given = 0;
    this->global_section.sink_format_given = 0;
    this->global_section.service_registry_url_given = 0;
    this->global_section.balance_check_given = 0;
    this->global_section.balance_tolerance_given = 0;
    this->global_section.random_seed_given = 0;
}


lerr_t
ldndc::config_file_t::set_filename(
        char const *  _cfg_filename)
{
    this->cfg_filename = "";
    if ( _cfg_filename)
    {
        this->cfg_filename = _cfg_filename;
        cbm::format_expand( &this->cfg_filename);
    }

    this->read_status = LDNDC_ERR_NONE;

    this->init_property_in_global_section();
    this->custom_section.clear();
    this->custom_section[cbm_config_block_global] = conf_section_t();

    return  this->read_status;
}


/* inspired by options_parse( ..) from Damian Pietras' MOC player project (see options.c) */
lerr_t
ldndc::config_file_t::read()
{
    /* double read guard */
    if ( this->read_status != LDNDC_ERR_NONE)
    {
        /* even delayed configuration file creation is rejected .. yes, sorry */
        return  this->read_status;
    }
    this->read_status = LDNDC_ERR_OK;

    if ( this->cfg_filename.empty())
    {
        fprintf( stderr, "[WW] no file name given. falling back to defaults.  [file=<empty>]\n");
        this->read_status = LDNDC_ERR_OK;
        if ( this->set_property_in_global_section_from_defaults() != LDNDC_ERR_OK)
        {
            this->read_status = LDNDC_ERR_FAIL;
        }
        return  this->read_status;
    }

    FILE *  cf( NULL);
    if ( cbm::is_equal( this->cfg_filename.c_str(), "-"))
    {
        cf = stdin;
    }
    else
    {
        if ( !cbm::is_file( this->cfg_filename.c_str()))
        {
            return  LDNDC_ERR_FAIL;
        }

        cf = fopen( this->cfg_filename.c_str(), "r");
        if ( !cf)
        {
            fprintf( stderr, "failed to open configuration file  [file=%s]\n", this->cfg_filename.c_str());
            this->read_status = LDNDC_ERR_FAIL;
            return  LDNDC_ERR_FAIL;
        }
    }

    int  chr;
    int  assignment( 0);
    int  comment_line( 0);
    int  quotes ( 0);
    int  block_header( 0);
    int  eol_r( 0);

    std::string  block_name( "");

    std::string  key_name( "");
    std::string  prop_value( "");

    unsigned int  line_num( 1);

    while (( chr = getc( cf)) != EOF)
    {
        if ( chr == '\r')
        {
            continue;
        }
        if ( is_newline(chr))
        {
            eol_r = 1;
            comment_line = 0;
            line_num += 1;
        }

        /* discard each character until end of comment line */
        if ( comment_line && !eol_r)
        {
            continue;
        }

        /* start next line */
        if ( eol_r)
        {
            if ( !key_name.empty())
            {
                if ( !assignment)
                {
                    fprintf( stderr, "lonely key word [key=%s,line=%u]\n", key_name.c_str(), line_num);
                    this->read_status = LDNDC_ERR_FAIL;
                    break;
                }
                this->read_status =
                    this->set_property_in_section( block_name, key_name, prop_value);
                if ( this->read_status != LDNDC_ERR_OK)
                {
                    break;
                }
            }

            assignment = 0;
            comment_line = 0;
            quotes = 0;
            block_header = 0;

            key_name.clear();
            prop_value.clear();

            eol_r = 0;
        }
        else if ( is_comment(chr) && (!quotes) && (!block_header))
        {
            comment_line = 1;
        }
        else if (( !quotes) && (!assignment) && (key_name.empty()) && is_block_open(chr))
        {
            block_name.clear();
            block_header = 1;
            continue;
        }
        else if ( block_header && is_block_close(chr))
        {
            block_header = 0;
        }
        else if ( block_header)
        {
            /* removes all blanks from block name */
            if ( !is_space( chr))
            {
                block_name += chr;
            }
        }
        else if (( !quotes) && is_quotes_open(chr))
        {
            quotes = 1;
        }
        else if ((  quotes) && is_quotes_close(chr))
        {
            quotes = 0;
        }
        else if (( !quotes) && is_assignment(chr))
        {
            assignment = 1;
        }
        else if (( quotes || (!is_space( chr))) && assignment)
        {
            if ( key_name.empty())
            {
                fprintf( stderr, "value without key [line=%u]\n", line_num);
                this->read_status = LDNDC_ERR_FAIL;
                break;
            }
            prop_value += chr;
        }
        else if ( quotes || (!is_space( chr)))
        {
            key_name += chr;
        }
        else
        {
            /* blanks */
        }
    }

    fclose( cf);
    return  this->read_status;
}


// TODO  use keyword array and query interface to generalize
void
ldndc::config_file_t::dump()
    const
{
    if ( this->cfg_filename.c_str())
    {
        printf( "dumping configuration file \"%s\"\n", this->cfg_filename.c_str());
    }

    printf( "[" cbm_config_block_global "]"
            "\n\tlog_file = \"%s\""
            "\n\tlog_level = \"%s\""
            "\n\tlog_append = \"%s\""
            "\n\tlog_targets_mask = \"%lu\""
            "\n\tprogress_bar = \"%s\""
            "\n",
            this->log_file(),
            LOGLEVEL_NAMES[this->log_level()],
            this->log_append() ? "yes" : "no",
            (unsigned long int)this->log_targets_mask(),
            this->progress_bar() ? "yes" : "no");

    printf( "\n\trandom_seed = \"%u\""
            "\n",
            this->random_seed());

    printf( "\n\tinput = \"%s\""
            "\n\tresources_path = \"%s\""
            "\n\tudunits_path = \"%s\""
            "\n\tinput_path = \"%s\""
            "\n\tproject_file = \"%s\""
            "\n",
            "yes",
            this->resources_path(),
            this->udunits_path(),
            this->input_path(),
            this->project_file());

    printf( "\n\tservice_registry_url = \"%s\"",
            this->service_registry_url());

    printf( "\n\toutput = \"%s\""
            "\n\toutput_path = \"%s\""
            "\n\tsink_format = \"%s\""
            "\n"
            "\n\n\tbalance_check = \"%s\"\n\tbalance_tolerance = \"%e\"", 
            ((this->have_output()) ? "yes" : "no"),
            this->output_path(),
            (this->sink_format_given() ? this->sink_format() : ""),
            (this->have_balance_check() ? "yes" : "no"), this->balance_tolerance());

    printf( "\n");
    for ( conf_sections_t::const_iterator  m_props( this->custom_section.begin());  m_props != custom_section.end();  ++m_props)
    {
        printf( "\n[%s]", m_props->first.c_str());
        for ( conf_section_t::iterator  prop( m_props->second.begin());  prop != m_props->second.end();  ++prop)
        {
            printf( "\n\t%s = \"%s\"", prop->first.c_str(), prop->second.c_str());
        }
    }
    printf( "\n");
}


lerr_t
ldndc::config_file_t::set_property_in_section(
        std::string const &  _block,
        std::string const &  _key,
        std::string const &  _value)
{
    if ( cbm::is_empty( _key))
    {
        fprintf( stderr, "empty key  [block=%s,value=%s]\n", _block.c_str(), _value.c_str());
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }
    int const  is_envvar = (( _key.size() > 0) && ( _key[0] == '$')) ? 1 : 0;
    if ( is_envvar && ( _key.size() < 2))
    {
        fprintf( stderr, "environment variable with zero-length name  [value=%s]\n", _value.c_str());
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }
    if ( cbm::is_equal( _block, cbm_config_block_global) && !is_envvar)
    {
        /* global section block */
        return  this->set_property_in_global_section( _key, _value);
    }
    else
    {
        std::string const  block =
            ( cbm::is_empty( _block) || is_envvar) ? cbm_config_block_environment : _block.c_str();
        std::string const  key =
            ( is_envvar) ? _key.c_str()+1 : _key.c_str();
        /* user section block */
        if ( this->custom_section.find( block) == this->custom_section.end())
        {
            this->custom_section[block] = conf_section_t();
        }
        if ( this->custom_section[block].key_val.find( key) != this->custom_section[block].key_val.end())
        {
            fprintf( stderr, "overwriting previously given attribute [key=%s,block=%s]\n", key.c_str(), block.c_str());
            return  LDNDC_ERR_FAIL;
        }
        this->custom_section[block].key_val[key] = _value;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::config_file_t::set_property_in_global_section(
        std::string const &  _key,
        std::string const &  _value)
{
    /* catch value conversion errors */
    lerr_t  value_conv_rc( LDNDC_ERR_OK);

    if ( _value.empty())
    {
        fprintf( stderr, "key without value in '" cbm_config_block_global "' block;"
               " using default [key=%s]\n", _key.c_str());
        return  LDNDC_ERR_OK;
    }

    if (      cbm::is_equal( _key, "project_file"))
    {
        this->global_section.project_file_given = 1;
        this->global_section.project_file = _value;
    }
    else if ( cbm::is_equal( _key, "log_file"))
    {
        this->global_section.log_file_given = 1;
        this->global_section.log_file = _value;
    }
    else if ( cbm::is_equal( _key, "log_append"))
    {
        this->global_section.log_append_given = 1;
        value_conv_rc = cbm::s2n< bool >(
                _value, &this->global_section.log_append);
    }
    else if ( cbm::is_equal( _key, "log_level"))
    {
        this->global_section.log_level_given = 1;
        global_section.log_level = LOGLEVEL_MAX+1;
        cbm::find_index( _value.c_str(), LOGLEVEL_NAMES, LOGLEVEL_CNT,
                &this->global_section.log_level);
        if ( this->global_section.log_level > LOGLEVEL_MAX)
        {
            fprintf( stderr, "unknown logging level"
                  "  [requested level=%s, available levels=", _value.c_str());
            crabmeat_assert( LOGLEVEL_CNT > 0);
            fprintf( stderr, "%s", LOGLEVEL_NAMES[0]);
            for ( size_t  k = 1;  k < LOGLEVEL_CNT;  ++k)
            {
                fprintf( stderr, ",%s", LOGLEVEL_NAMES[k]);
            }
            fprintf( stderr, "]\n");
            global_section.log_level = LOGLEVEL_MAX;
            fprintf( stderr, "falling back to: '%s'\n",
                    LOGLEVEL_NAMES[global_section.log_level]);
            return  LDNDC_ERR_OK;
        }
    }
    else if ( cbm::is_equal( _key, "log_targets_mask"))
    {
        this->global_section.log_targets_mask_given = 1;
        value_conv_rc = cbm::s2n< lflags_t >(
                _value, &this->global_section.log_targets_mask);
    }
// sk:??    else if ( cbm::is_equal( _key, "input"))
// sk:??    {
// sk:??        this->global_section.input_given = 1;
// sk:??        value_conv_rc = cbm::s2n< bool >( _value, &this->global_section.input);
// sk:??    }
    else if ( cbm::is_equal( _key, "progress_bar"))
    {
        this->global_section.progress_bar_given = 1;
        value_conv_rc = cbm::s2n< bool >(
                _value, &this->global_section.progress_bar);
    }
    else if ( cbm::is_equal( _key, "resources_path"))
    {
        this->global_section.resources_path_given = 1;
        this->global_section.resources_path = _value;
    }
    else if ( cbm::is_equal( _key, "udunits_path"))
    {
        this->global_section.udunits_path_given = 1;
        this->global_section.udunits_path = _value;
    }
    else if ( cbm::is_equal( _key, "input_path"))
    {
        this->global_section.input_path_given = 1;
        this->global_section.input_path = _value;
    }
    else if ( cbm::is_equal( _key, "output"))
    {
        this->global_section.output_given = 1;
        value_conv_rc = cbm::s2n< bool >(
                _value, &this->global_section.output);
    }
    else if ( cbm::is_equal( _key, "output_path"))
    {
        this->global_section.output_path_given = 1;
        this->global_section.output_path = _value;
    }
    else if ( cbm::is_equal( _key, "sink_format"))
    {
        this->global_section.sink_format_given = 1;
        this->global_section.sink_format = _value;
    }
    else if ( cbm::is_equal( _key, "service_registry_url"))
    {
        this->global_section.service_registry_url_given = 1;
        this->global_section.service_registry_url = _value;
    }
    else if ( cbm::is_equal( _key, "dump"))
    {
        fprintf( stderr, "CONFIGURATION: key \"dump\" is obsolete and will be ignored. "
                "This message will disappear once you remove this key from your configuration file\n");
    }
    else if ( cbm::is_equal( _key, "restore"))
    {
        fprintf( stderr, "CONFIGURATION: key \"restore\" is obsolete and will be ignored. "
                "This message will disappear once you remove this key from your configuration file\n");
    }
    else if ( cbm::is_equal( _key, "dump_file"))
    {
        fprintf( stderr, "CONFIGURATION: key \"dump_file\" is obsolete and will be ignored. "
                "This message will disappear once you remove this key from your configuration file\n");
    }
    else if ( cbm::is_equal( _key, "balance_check"))
    {
        this->global_section.balance_tolerance_given = 1;
        value_conv_rc = cbm::s2n< bool >( _value, &this->global_section.balance_check);
    }
    else if ( cbm::is_equal( _key, "balance_tolerance"))
    {
        this->global_section.balance_tolerance_given = 1;
        value_conv_rc = cbm::s2n< double >( _value, &this->global_section.balance_tolerance);
    }
    else if ( cbm::is_equal( _key, "random_seed"))
    {
        this->global_section.random_seed_given = 1;
        value_conv_rc = cbm::s2n< unsigned int >( _value, &this->global_section.random_seed);
    }
    else
    {
        fprintf( stderr, "unknown item in '" cbm_config_block_global "' block [item=%s]\n", _key.c_str());
        return  LDNDC_ERR_OK;
    }

    return  value_conv_rc;
}


lerr_t
ldndc::config_file_t::set_property_in_global_section_from_defaults()
{
    static std::string  BALANCE_TOLERANCE_STR( cbm::n2s( cbm::BALANCE_TOLERANCE));
    static char const * const  CONF_DEFAULTS[] =
    {
        "project_file", cbm::PROJECT_FILE,
        "log_file", cbm::LOG_FILE,
        "log_append", "yes",
        "log_level", LOGLEVEL_NAMES[LOGLEVEL_DEFAULT],
        "log_targets_mask", "0",
        "progress_bar", "no",
        "resources_path", cbm::RESOURCES_PATH,
        "udunits_path", cbm::RESOURCES_PATH,
// sk:??        "input", "yes",
        "input_path", cbm::INPUT_PATH,
        "output", "yes",
        "output_path", cbm::OUTPUT_PATH,
        "sink_format", "unknown",
        "service_registry_url", /*"file://"*/ LDNDC_DOT_PATH "service-registry.json",
        "balance_check", "yes",
        "balance_tolerance", BALANCE_TOLERANCE_STR.c_str(),
        "random_seed", "0",

        /* sentinel, always last entries :: DO NOT REMOVE */
        NULL, NULL
    };
    for ( size_t  k = 0;  CONF_DEFAULTS[k] != NULL;  k += 2)
    {
        if ( this->set_property_in_global_section( CONF_DEFAULTS[k], CONF_DEFAULTS[k+1]) != LDNDC_ERR_OK)
        {
            this->init_property_given_in_global_section();
            return  LDNDC_ERR_FAIL;
        }
    }

    this->init_property_given_in_global_section();
    return  LDNDC_ERR_OK;
}

