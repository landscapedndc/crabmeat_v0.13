/*!
 * @brief
 *    simple flag type
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 */

#ifndef  CRABMEAT_GLOBAL_FLAGS_H_
#define  CRABMEAT_GLOBAL_FLAGS_H_

#include  "cbm_types.h"
/* bit-flag type */
typedef  unsigned long long int  lflags_t;
#define  LDNDC_LFLAGS  LDNDC_UINT64
#define  LDNDC_LFLAGS_BYTESIZE  (sizeof(lflags_t))

static const lflags_t  lflag_noflag = 0u;

#ifndef  LDNDC_PRINTF_SEQ_LFLAGS
#  define  LDNDC_PRINTF_SEQ_LFLAGS  LDNDC_PRINTF_SEQ_UINT64
#endif


#endif  /*  !CRABMEAT_GLOBAL_FLAGS_H_  */

