/*!
 * @brief
 *    structure represents elements in "files" block
 *    in setup's global block (deprecated)
 *
 *    input source information have been
 *    moved into project file
 *
 *
 * @author
 *    steffen klatt (created on: aug 11, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_IO_SOURCEINFO_H_
#define  LDNDC_IO_SOURCEINFO_H_

#include  "crabmeat-common.h"
#include  "io/data-source/data-source.h"

namespace  ldndc
{
    enum  source_info_flags_e
    {
        SOURCEINFO_NONE,
        SOURCEINFO_FALLBACKRESOURCES
    };

    struct  source_info_t;
    lerr_t  CBM_API  source_info_default(
            source_info_t *, char const *);

    struct  CBM_API  source_info_t
    {
        /* source identifier */
        std::string  identifier;
        /* input class type (e.g. climate) */
        std::string  type;

        /* source name ( e.g. file name) */
        std::string  name;
        /* source name prefix ( e.g. path) */
        std::string  name_prefix;

        /* source format (e.g. xml) */
        cbm::string_t  format;

        /* specifies how a reader/handler should treat its data source */
        persistence_type_e  persis;

        lflags_t  flags;

        source_info_t();
        source_info_t( char const * /*input class*/);

        std::string  full_name() const;
        std::string  full_path() const;

        void  set_nodev();

        bool  is_mutable() const;
        bool  is_nodev() const;
        bool  is_fully_servable_by_resources() const;
    };

#define  NODEV_SOURCE_NAME  "|"
    struct  default_source_info_t
    {
        char const *  iclass;
        bool  fully_servable_by_resources;
    };
    /* default input names, if one is missing. empty name means input
     * is optional. 
     */
    extern CBM_API int  sourceinfo_defaults( char const * /*class*/,
                    default_source_info_t * /*buffer*/);

    std::ostream CBM_API &
    operator<<( std::ostream &, ldndc::source_info_t const &);

}  /*  namespace ldndc  */


#endif  /*  !LDNDC_IO_SOURCEINFO_H_  */


