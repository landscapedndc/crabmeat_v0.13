/*!
 * @brief
 *    global output stream attributes (implementation)
 *
 * @author
 *    steffen klatt (created on: oct 31, 2013),
 *    edwin haas
 */

#include  "io/sink-attrs.h"

ldndc::sink_attributes_t const  ldndc::sink_attributes_defaults =
{
    1 /*uniform*/,
    0 /*ordered*/,
    1 /*overwrite*/,
    0 /*noheader*/,
    DEFAULT_OUTPUT_SINK_ENTITY_DELIMITER /*delimiter*/,
    ldndc::invalid_t< size_t >::value /*sizelimit*/,
    -1 /*cachesize*/
};


