/*!
 * @brief
 *    abstraction layer for input source object descriptions
 *
 * @author
 *    steffen klatt (created on: oct 17, 2011)
 */

#include  "io/data-source/data-source-base.h"

#include  "string/lstring-compare.h"
#include  "string/lstring-nstr.h"

#include  <stdlib.h>

/***  DATA SOURCE ATTRIBUTE WRAPPER  ***/


#include  "string/lstring-nstr.h"
ldndc::persistence_type_e
ldndc::data_source_t::persistence_type_from_source_name(
        char const *  _source_name,
        std::string *  _n_norm)
{
    /* check for empty source name... */
    if ( !_source_name || cbm::is_empty( _source_name))
    {
        return  PERSISTENCE_TYPE_INVALID;
    }

    cbm::nstr  source_name_n( _source_name, CNAME_STRIP);

    /* is it empty now? */
    if ( source_name_n.size() == 0)
    {
        return  PERSISTENCE_TYPE_INVALID;
    }

    /* there can be no name magic */
    if ( source_name_n.size() == 1)
    {
        return  PERSISTENCE_TYPE_FILE;
    }

    /* check for magic in the soure description */
// sk:later    if ( source_name_n[0] == '|')
// sk:later    {
// sk:later        return  PERSISTENCE_TYPE_FIFO;
// sk:later    }
// sk:later    else
    if ( source_name_n[0] == '=')
    {
        if ( _n_norm)
        {
            *_n_norm = source_name_n.str();
            _n_norm->erase( 0, 1);
        }
        return  PERSISTENCE_TYPE_MUTABLE_FILE;
    }
// sk:later    else if ( source_name_n[0] == '$')
// sk:later    {
// sk:later        return  PERSISTENCE_TYPE_SOCKET;
// sk:later    }
// sk:later
    else
    {
        /* interpret source name as filename... */
        if ( _n_norm)
        {
            *_n_norm = source_name_n.str();
        }
        return  PERSISTENCE_TYPE_FILE;
    }

    /* no way to get here */
}

ldndc::data_source_t::~data_source_t()
{
}

LDNDC_OBJECT_DEFN(ldndc::data_source_file_t)

ldndc::data_source_file_t::data_source_file_t()
        : data_source_t( ""),
          filename_( "")
{
}

ldndc::data_source_t *
ldndc::data_source_file_t::clone()
const
{
    data_source_file_t *  this_clone = data_source_file_t::new_instance();
    this_clone->set_source_name( this->source_name());
    this_clone->set_io_format( this->io_format());

    return  this_clone;
}


ldndc::data_source_t *
ldndc::data_source_factory_t::create(
                ldndc::persistence_type_e  _ptype)
{
        switch ( _ptype)
    {
        case  ldndc::PERSISTENCE_TYPE_FILE:
        {
                        return  data_source_file_t::new_instance();
        }
// sk:later                case PERSISTENCE_TYPE_FIFO:
// sk:later                        return  new data_source_fifo_t();
                default:
        {
                        CBM_LogFatal( "not implemented yet");
                        return  NULL;
        }
        }
}


ldndc::data_source_t *
ldndc::data_source_factory_t::clone(
                ldndc::data_source_t const *  _data_source)
{
        if ( ! _data_source)
    {
                CBM_LogError( "attempt to clone null object");
                return  NULL;
        }
        return  _data_source->clone();
}


ldndc::data_source_t *
ldndc::data_source_factory_t::new_from_source_desc(
        char const *  _source_name)
{
    persistence_type_e const  p_type = data_source_t::persistence_type_from_source_name( _source_name);
    if ( p_type == PERSISTENCE_TYPE_INVALID)
    {
        return  NULL;
    }

    data_source_t *  data_source = data_source_factory_t::create( p_type);
    if ( data_source)
    {
        data_source->set_source_name( _source_name);
    }
    return  data_source;
}


void
ldndc::data_source_factory_t::free(
        ldndc::data_source_t *  _data_source)
{
    if ( _data_source)
    {
        _data_source->delete_instance();
    }
}

