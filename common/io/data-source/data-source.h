/*!
 * @brief
 *    abstraction layer for input source object descriptions
 *    (convenience header pulling all input source descriptor types)
 *
 * @author
 *    steffen klatt (created on: apr 22, 2013)
 */

#ifndef  LDNDC_DATASOURCES_H_
#define  LDNDC_DATASOURCES_H_

#include  "io/data-source/data-source-base.h"

#endif  /*  !LDNDC_DATASOURCES_H_  */

