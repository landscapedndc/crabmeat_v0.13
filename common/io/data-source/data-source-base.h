/*!
 * @brief
 *    abstraction layer for input source object descriptions
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#ifndef PERSISTENCE_ATTR_H_
#define PERSISTENCE_ATTR_H_

#include  "crabmeat-common.h"
#include  "cbm_object.h"
#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

/***  DATA SOURCE ATTRIBUTE WRAPPER  ***/

namespace  ldndc
{
    /* TODO  this still seems odd to me... ;-) */
    enum  persistence_type_e
    {
        /*! data source is file */
        PERSISTENCE_TYPE_FILE                = 0U,
// sk:later        /*! data source is opened file (same as pipe/fifo?) */
// sk:later        PERSISTENCE_TYPE_HANDLE,
// sk:later        /*! data source is a (unix,internet?) socket */
// sk:later        PERSISTENCE_TYPE_SOCKET,
// sk:later        /*! data source is a (un)named pipe */
// sk:later        PERSISTENCE_TYPE_PIPE,

        /*! data source is mutable file, e.g. written by other process */
        PERSISTENCE_TYPE_MUTABLE_FILE,

        PERSISTENCE_TYPE_CNT,
        PERSISTENCE_TYPE_INVALID
    };



    class  CBM_API  data_source_t  :  public  ldndc::object_t
    {
        public:
            static  persistence_type_e  persistence_type_from_source_name(
                    char const * /*source name*/,
                    std::string * = NULL /*normalized name (optional)*/);

        public:
            explicit  data_source_t( cbm::string_t const &  _io_fmt)
                    : io_fmt_( _io_fmt)
                { }
            virtual  data_source_t *  clone() const = 0;

            virtual  ~data_source_t() = 0;

            virtual  char const *  source_name() const = 0;
            virtual  void  set_source_name(
                    char const * /*source name*/) = 0;


            cbm::string_t const &  io_format() const
                { return  this->io_fmt_; }
            void  set_io_format( cbm::string_t const &  _io_fmt)
                { this->io_fmt_ = _io_fmt; }

            virtual  persistence_type_e  persistence_type() const = 0;

            template < typename _T >
            typename _T::attribute_type const * source() const
            {
                return  static_cast< typename _T::attribute_type const * >( this->source_());
            }

        protected:
            virtual  void const *  source_() const = 0;
        private:
            cbm::string_t  io_fmt_;

    };

    class  CBM_API  data_source_file_t  :  public  data_source_t 
    {
        LDNDC_OBJECT(data_source_file_t)
        public:
            typedef  std::string  attribute_type;

        public:
            data_source_file_t();
            data_source_t *  clone() const;

            char const *  source_name()
            const
            {
                return  filename_.c_str();
            }
            void  set_source_name(
                    char const *  _data_source)
            {
                this->filename_ = _data_source;
            }

            persistence_type_e  persistence_type()
            const
            {
                return  PERSISTENCE_TYPE_FILE;
            }


        protected:
            void const *  source_() const
                { return  static_cast< void const * >( &filename_); }

        private:
            attribute_type  filename_;
    };




    /* persistence attribute factory */
    struct  CBM_API  data_source_factory_t
    {
        static  ldndc::data_source_t *  create(
                persistence_type_e);

        static  ldndc::data_source_t *  clone(
                ldndc::data_source_t const *);

        static  ldndc::data_source_t *  new_from_source_desc(
                char const *);

        static void  free(
                ldndc::data_source_t *);
    };

}  /*  namespace ldndc  */


#endif  /*  !PERSISTENCE_ATTR_H_  */

