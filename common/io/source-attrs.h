/*!
 * @brief
 *    global input stream attributes
 *
 * @author
 *    steffen klatt (created on: aug 11, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_IO_SOURCEATTRIBUTES_H_
#define  LDNDC_IO_SOURCEATTRIBUTES_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"
#include  "math/cbm_math.h"

namespace  ldndc
{
    struct  CBM_API  source_attributes_t
    {
        /* the type this information refers to (e.g. climate) */
        cbm::string_t  type;

        enum  stream_attribute_flag_e
        {
            /*! empty flag */
            IFLAG_NONE        = 0u,
            /*! indicates whether input data continues from top on eof */
            IFLAG_ENDLESS        = 1u << 0,
// sk:later            /*! (not supported) */
// sk:later            IFLAG_FORCE_TIME    = 1u << 1,

            POWEROF2_ENUM_CNT(IFLAG)
        };

        static char const *  IFLAG_NAMES[IFLAG_CNT];
        static char const  IFLAG_SEP;

        /* default ID to use if none was given in core block definition */
        lid_t  default_id;

        /* some flags controlling input stream handling */
        lflags_t  flags;

        source_attributes_t();
    };

    /* stream attribute getters .. */
    bool CBM_API stream_attribute_endless(
            ldndc::source_attributes_t const *);

}  /*  namespace ldndc  */


#endif  /*  !LDNDC_IO_SOURCEATTRIBUTES_H_  */

