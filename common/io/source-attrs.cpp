/*!
 * @brief
 *    global input stream attributes (implementation)
 *
 * @author
 *    steffen klatt (created on: aug 11, 2012),
 *    edwin haas
 */

#include  "io/source-attrs.h"

char const *  ldndc::source_attributes_t::IFLAG_NAMES[ldndc::source_attributes_t::IFLAG_CNT] = { "", "endless"/*, "time"*/};
char const  ldndc::source_attributes_t::IFLAG_SEP = ',';

ldndc::source_attributes_t::source_attributes_t()
    : type( ""), default_id( invalid_lid), flags( lflag_noflag)
    { }

#include  "log/cbm_baselog.h"
bool
ldndc::stream_attribute_endless(
                ldndc::source_attributes_t const *  _attr)
{
    if ( _attr)
    {
        return  ( 0 != ( _attr->flags & ldndc::source_attributes_t::IFLAG_ENDLESS));
    }

    CBM_LogWarn( "stream_attribute_endless(): received stream attributes object was null");
    return  false;
}

