/*!
 * @brief
 *    output related system-wide agreed upon types
 *
 * @author
 *    steffen klatt (created on: nov 09, 2013),
 *    edwin haas
 *
 * @note
 *    yes, this is highly non C++
 */

#ifndef  LDNDC_IO_OUTPUTTYPES_H_
#define  LDNDC_IO_OUTPUTTYPES_H_

#include  "crabmeat-common.h"

/* signed integral type */
typedef  ldndc_int8_t  ldndc_output_rank_t;
/* signed integral type */
typedef  ldndc_int32_t  ldndc_output_size_t;

#include  "time/cbm_date.h"
/* integral type */
typedef  ldndc_timestamp_t  ldndc_sink_timestamp_t;
typedef  ldndc_uint64_t  ldndc_sink_meta_timestamp_t;

namespace ldndc {

namespace  io
{
    enum  record_meta_e
    {
        _CLIENT_SOURCE, _CLIENT_ID,

        _CLIENT_X, _CLIENT_Y, _CLIENT_Z, _CLIENT_AREA,

        _DATETIME, _DATE, _TIME,
        _YEAR, _MONTH, _DAY, _HOUR, _MINUTE, _SECOND,
        _JULIANDAY, _SUBDAY,

        _SEQNB,
        _REG_CNT,

        CLIENT_ID = 1 << _CLIENT_ID, CLIENT_SOURCE = 1 << _CLIENT_SOURCE,
        CLIENT_X = 1 << _CLIENT_X, CLIENT_Y = 1 << _CLIENT_Y, CLIENT_Z = 1 << _CLIENT_Z, CLIENT_AREA = 1 << _CLIENT_AREA,

        DATETIME = 1 << _DATETIME, DATE = 1 << _DATE, TIME = 1 << _TIME,
        YEAR = 1 << _YEAR, MONTH = 1 << _MONTH, DAY = 1 << _DAY, HOUR = 1 << _HOUR, MINUTE = 1 << _MINUTE, SECOND = 1 << _SECOND,
        JULIANDAY = 1 << _JULIANDAY, SUBDAY = 1 << _SUBDAY,

        SEQNB = 1 << _SEQNB
    };
    extern CBM_API ldndc_string_t const  record_meta_ids[_REG_CNT];
// sk:off    extern CBM_API atomic_datatype_t const  record_meta_types[_REG_CNT];
}

struct CBM_API sink_client_t
{
    sink_client_t();

    lid_t  target_id;
    ldndc_string_t  target_sid;

    double  x, y, z, area;
};
extern CBM_API sink_client_t const  sink_client_defaults;

struct CBM_API  sink_record_sequence_t
{
    sink_record_sequence_t();

    typedef  ldndc_uint32_t  time_type;
    /* timestep of simulation */
    time_type  timestep;

    typedef  ldndc_uint32_t  seqnb_type;
    /* number of written records in single commence/commit cycle */
    seqnb_type  records_in_timestep;
};

/* list of IDs and names (human readable) of identical size <size> */
struct CBM_API sink_entities_t
{
    /* ids/names size */
    ldndc_output_size_t  size;

    /* entity IDs */
    ldndc_string_t const *  ids;
    /* entity names (human readable) */
    ldndc_string_t const *  names;
};
extern CBM_API sink_entities_t const  sink_entities_defaults;

/* list of types for each entity in ID list above given depending
 * on order: that is, entities of same type following each other
 * are grouped and "belong" to the same rank. each rank's size is
 * stored in <sizes>. <types> and <sizes> have size <rank>.
 */
struct CBM_API sink_layout_t
{
    /* rank of data */
    ldndc_output_rank_t  rank;

    /* sizes of data along each rank */
    ldndc_output_size_t const *  sizes;
    /* data types of entities */
    atomic_datatype_t const *  types;

    /* size of each entity (use for vector-valued entities) */
    ldndc_output_size_t const *  entsizes;
};
extern CBM_API sink_layout_t const  sink_layout_defaults;

/* a complete data layout description with entity names */
struct CBM_API sink_entity_layout_t
{
    sink_entities_t  ents;
    sink_layout_t  layout;
};
extern CBM_API sink_entity_layout_t const  sink_entity_layout_defaults;

enum CBM_API sink_gap_e
{
    /* do not fill gaps */
    SINKGAP_NONE,
    /* fill gaps with zeroes */
    SINKGAP_ZERO,
// sk:later    /* fill gaps with type-specific invalid values */
// sk:later    SINKGAP_INVALID,

    SINKGAP_CNT
};
struct CBM_API sink_record_meta_t
{
    sink_record_meta_t();
    ldndc_sink_meta_timestamp_t  timestamp_to_scalar() const;
    void  timestamp_from_scalar(
            ldndc_sink_meta_timestamp_t const &);

    lflags_t  useregs;

    struct  timestamp_registers_t
    {
        ldndc_uint32_t  subday : 16;
        ldndc_uint32_t  year : 12;
        ldndc_uint32_t  month : 4;
        ldndc_uint32_t  day : 5;
        ldndc_uint32_t  hour : 6;
        ldndc_uint32_t  minute : 6;
        ldndc_uint32_t  second : 6;

        ldndc_uint32_t  julianday : 9;
    };
    union  timestamp_registers_u
    {
        timestamp_registers_t  reg;
        ldndc_sink_meta_timestamp_t  ts;
    };
    timestamp_registers_u  t;

    int  record_seqnb;
};
struct CBM_API sink_record_meta_srv_t
{
    sink_record_meta_srv_t();
    sink_record_meta_srv_t(
            sink_record_meta_t const &);
    sink_record_meta_srv_t &  operator=(
            sink_record_meta_t const &);

    lflags_t  useregs;
    ldndc_sink_meta_timestamp_t  ts;
    int  record_seqnb;
};

struct CBM_API sink_layout_hint_t
{
    typedef  ldndc_int32_t  sink_layout_id_t;
    sink_layout_id_t  id;
};
extern CBM_API sink_layout_hint_t const  sink_layout_hint_defaults;

struct CBM_API sink_fixed_record_t
{
    sink_fixed_record_t();

    sink_record_meta_t  meta;
    /* data block (rank and content defined by layout) */
    void **  data;

    /*  fill omitted ranks according to gap type */
    sink_gap_e  gap;
};
extern CBM_API sink_fixed_record_t const  sink_fixed_record_defaults;


struct CBM_API sink_nonfixed_record_t  :  public sink_fixed_record_t
{
    sink_nonfixed_record_t();

    /* entities */
    sink_entities_t  ents;
    /* layout */
    sink_layout_t  layout;
};
extern CBM_API sink_nonfixed_record_t const  sink_nonfixed_record_defaults;

struct CBM_API sink_public_record_t  :  public sink_fixed_record_t
{
    sink_public_record_t();

    ldndc_sink_timestamp_t  timestamp;

    /* layout (only used on client side, injected from receiver on server side) */
    sink_layout_t  layout;
    /* layout hint (if only a subset of the full data layout is sent) */
    sink_layout_hint_t  layout_hint;
};
extern CBM_API sink_public_record_t const  sink_public_record_defaults;


struct CBM_API sink_record_srv_t
{
    sink_record_srv_t();
    sink_record_srv_t(
            sink_fixed_record_t const &);
    sink_record_srv_t(
            sink_nonfixed_record_t const &);
    sink_record_srv_t(
            sink_public_record_t const &);

    sink_record_meta_srv_t  meta;

    /* data block (rank and content defined by layout) */
    void **  data;

    sink_record_sequence_t const *  seq;

    /* fill omitted ranks according to gap type */
    sink_gap_e  gap;

    /* time stamp (optional) */
    ldndc_sink_timestamp_t  timestamp;

    /* entities */
    sink_entities_t  ents;
    /* layout */
    sink_layout_t  layout;
    /* layout hint (if only a subset of the full data layout is sent) */
    sink_layout_hint_t  layout_hint;
};
extern CBM_API sink_record_srv_t const  sink_record_srv_defaults;

extern
CBM_API
lerr_t
malloc_from_layout(
        void *** /*buffer*/, sink_layout_t const * /*layout*/);
extern
CBM_API
lerr_t
free_from_layout(
        void ** /*buffer*/, sink_layout_t const * /*layout*/);
extern
CBM_API
size_t
sizeof_layout(
        sink_layout_t const * /*layout*/);

extern
CBM_API
lerr_t
memclr_with_layout(
        void ** /*destination buffer*/, sink_layout_t const *);
extern
CBM_API
lerr_t
memcpy_with_layout(
        void ** /*destination buffer*/, void const ** /*source buffer*/,
        sink_layout_t const *);

extern
CBM_API
lerr_t
dump_from_layout(
        void const ** /*buffer*/, sink_layout_t const * /*layout*/);

}

#endif  /*  !LDNDC_IO_OUTPUTTYPES_H_  */

