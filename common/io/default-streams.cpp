
/*!
 * @brief
 *    define system wide default streams (e.g. stdout)
 *
 * @author
 *    steffen klatt (created on: nov 07, 2013),
 *    edwin haas,
 */

#include  "io/default-streams.h"

char const *  ldndc::STANDARD_SOURCE_NAMES[ldndc::STANDARD_SOURCE_CNT] = { "-", "FILE"};
char const *  ldndc::STANDARD_SINK_NAMES[ldndc::STANDARD_SINK_CNT] = { "stdout", "stderr", "stdlog", "null", "FILE"};

#include  "string/lstring-compare.h"
bool
ldndc::is_standard_source(
        char const *  _source_name)
{
    for ( size_t  k = 0;  k < STANDARD_SOURCE_FILE;  ++k)
    {
        if ( cbm::is_equal_i( _source_name, STANDARD_SOURCE_NAMES[k]))
        {
            return  true;
        }
    }
    return  false;
}
bool
ldndc::is_standard_sink(
        char const *  _sink_name)
{
    for ( size_t  k = 0;  k < STANDARD_SINK_FILE;  ++k)
    {
        if ( cbm::is_equal_i( _sink_name, STANDARD_SINK_NAMES[k]))
        {
            return  true;
        }
    }
    return  false;
}

#include  <iostream>
lerr_t
ldndc::standard_sink_buffer(
        std::ios *  _io_stream, char const *  _sink_name)
{
    if ( !_io_stream || !_sink_name)
    {
        return  LDNDC_ERR_FAIL;
    }

    if ( cbm::is_equal_i( _sink_name, STANDARD_SINK_NAMES[STANDARD_SINK_ERR]))
    {
        _io_stream->rdbuf( std::cerr.rdbuf());
    }
           else if ( cbm::is_equal_i( _sink_name, STANDARD_SINK_NAMES[STANDARD_SINK_OUT]))
    {
        _io_stream->rdbuf( std::cout.rdbuf());
    }
    else if ( cbm::is_equal_i( _sink_name, STANDARD_SINK_NAMES[STANDARD_SINK_LOG]))
    {
        _io_stream->rdbuf( std::clog.rdbuf());
    }
    else if ( cbm::is_equal_i( _sink_name, STANDARD_SINK_NAMES[STANDARD_SINK_NULL]))
    {
        _io_stream->rdbuf( NULL);
        return  LDNDC_ERR_OK;
    }
    else
    {
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }
    return  _io_stream->bad() ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

char const * const  ldndc::DEFAULT_OUTPUT_SINK_FORMAT = "txt";

