/*!
 * @brief
 *    global output sink attributes
 *
 * @author
 *    steffen klatt (created on: oct 31, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_IO_SINKATTRIBUTES_H_
#define  LDNDC_IO_SINKATTRIBUTES_H_

#include  "crabmeat-common.h"

namespace  ldndc
{

    struct CBM_API sink_attributes_t
    {
        /* note, that attributes might not apply to all i/o formats */

        /*! single layout applies to all kernels (default=1) */
        int  uniform;
        /*! data is written in blocks (default=0) */
        int  ordered;
        /*! overwrite sink if it exists (e.g. file, default=1) */
        int  overwrite;
        /*! omit writing header (default=0) */
        int  noheader;

        /*! entity delimiter (default see DEFAULT_OUTPUT_SINK_ENTITY_DELIMITER) */
        char  delimiter;

        /*! limit number of bytes for output object (default=no limit) */
        size_t  size_limit;
        /*! specifies upper limit of bytes cached until data is flushed (default=-1, i.e. use internal sink default) */
        int  cache_size;
    };
    extern CBM_API sink_attributes_t const  sink_attributes_defaults;

#define  DEFAULT_OUTPUT_SINK_ENTITY_DELIMITER  '\t'

}  /*  namespace ldndc  */

#endif  /*  !LDNDC_IO_SINKATTRIBUTES_H_  */

