/*!
 * @brief
 *    declare system wide default streams (e.g. stdout)
 *
 * @author
 *    steffen klatt (created on: nov 07, 2013),
 *    edwin haas,
 */

#ifndef  LDNDC_STANDARD_STREAMS_H_
#define  LDNDC_STANDARD_STREAMS_H_

#include  "crabmeat-common.h"

namespace ldndc {
enum  standard_source_e
{
    STANDARD_SOURCE_IN    = 0u,

    STANDARD_SOURCE_FILE,

    STANDARD_SOURCE_CNT,
    STANDARD_SOURCE_NONE
};
extern CBM_API char const *  STANDARD_SOURCE_NAMES[STANDARD_SOURCE_CNT];
enum  standard_sink_e
{
    STANDARD_SINK_OUT    = 0u,
    STANDARD_SINK_ERR,
    STANDARD_SINK_LOG,

    STANDARD_SINK_NULL,

    STANDARD_SINK_FILE,

    STANDARD_SINK_CNT,
    STANDARD_SINK_NONE
};
extern CBM_API char const *  STANDARD_SINK_NAMES[STANDARD_SINK_CNT];
}

/* associate i/o stream with default buffer */
#include  <iostream>
namespace ldndc {
extern CBM_API bool is_standard_source(
        char const * /*source name*/);

extern CBM_API bool is_standard_sink(
        char const * /*sink name*/);
extern CBM_API lerr_t standard_sink_buffer(
        std::ios * /*i/o stream*/, char const * /*sink name*/);
}

namespace ldndc {
extern CBM_API char const * const  DEFAULT_OUTPUT_SINK_FORMAT;
}

#endif  /*  !LDNDC_STANDARD_STREAMS_H_  */

