/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: may 23, 2013),
 *    edwin haas
 */

#include  "io/source-info.h"

#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"
lerr_t
ldndc::source_info_default(
            source_info_t  *  _ssrc, char const *  _type)
{
    crabmeat_assert( _ssrc);
    if ( !_type || cbm::is_empty( _type))
        { return  LDNDC_ERR_FAIL; }

    /* default identifier */
    _ssrc->identifier = _type;
    /* input class type */
    _ssrc->type = _type;

    /* source description prefix */
    _ssrc->name_prefix = "";
    _ssrc->name = "";
    _ssrc->format = "";

    return  LDNDC_ERR_OK;
}


ldndc::source_info_t::source_info_t()
    : identifier( ""), type( ""),
      name( ""), name_prefix( ""), format( ""),
      persis( PERSISTENCE_TYPE_INVALID),
      flags( SOURCEINFO_NONE)
{
}
ldndc::source_info_t::source_info_t( char const *  _type)
    : identifier( ""), type( _type),
      name( ""), name_prefix( ""), format( ""),
      persis( PERSISTENCE_TYPE_INVALID),
      flags( SOURCEINFO_NONE)
{
    source_info_default( this, _type);
}


#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"
std::string
ldndc::source_info_t::full_name()
const
{
    if ( cbm::is_empty( this->type))
        { return  ldndc_empty_string; }
    if ( this->is_nodev())
        { return  NODEV_SOURCE_NAME; }

    std::stringstream  this_full_name;
    if ( !cbm::is_absolute_path( this->name_prefix.c_str()))
        { this_full_name << "%I"; }
    this_full_name << this->name_prefix << this->name;
    
    std::string const  fname = this_full_name.str();
    std::string  fname_expanded;
    lerr_t const  rc_fe = cbm::format_expand( &fname_expanded, &fname);
    if ( rc_fe)
        { return  "!ERR"; }
    return  fname_expanded;
}
std::string
ldndc::source_info_t::full_path()
const
{
    std::string  dst = this->full_name();
    std::string  src = this->full_name();
    cbm::dirname( &dst, src.c_str());
    dst += '/';
    return  dst;
}

void
ldndc::source_info_t::set_nodev()
{
    this->name = NODEV_SOURCE_NAME;
    this->name_prefix = "";
}

bool
ldndc::source_info_t::is_mutable()
const
{
    return  this->persis == PERSISTENCE_TYPE_MUTABLE_FILE;
}
bool
ldndc::source_info_t::is_nodev()
const
{
    if ( cbm::is_equal( this->name, NODEV_SOURCE_NAME))
    {
        return  true;
    }
    return  false;
}
bool
ldndc::source_info_t::is_fully_servable_by_resources()
const
{
    /* NOTE  this is a crude approach that simply uses
     * the type name to conclude if the information is
     * contained in the resources database */

    CBM_LogDebug( "type=",this->type);

    cbm::string_t const  this_type = this->type;
    if ( this_type.endswith( "parameters"))
        { return true; }
    return false;
}


std::ostream &
ldndc::operator<<( std::ostream &  _out,
        ldndc::source_info_t const &  _si)
{
    return  _out
        << '{'
        << "identifier=" << _si.identifier
        << ",name=" << _si.name
        << ",prefix=" << _si.name_prefix
        << ",type=" << ((_si.type != "") ? _si.type : std::string("?"))
        << ",format=" << ((_si.format != "") ? _si.format : std::string("?"))
        << '}';
}

