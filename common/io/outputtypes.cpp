/*!
 * @brief
 *    output related system-wide agreed upon types
 *    (implementation)
 *
 * @author
 *    steffen klatt (created on: jan 13, 2014),
 *    edwin haas
 */

#include  "io/outputtypes.h"
#include  "io/marshal/marshal.h"
#include  "memory/cbm_mem.h"
#include  "log/cbm_baselog.h"

ldndc::sink_client_t::sink_client_t()
    : target_id( invalid_lid)
{
    this->target_sid[0] = '\0';
}
ldndc::sink_client_t const  ldndc::sink_client_defaults;


ldndc::sink_record_sequence_t::sink_record_sequence_t()
    : timestep( invalid_t< time_type >::value),
      records_in_timestep( invalid_t< seqnb_type >::value)
{
}

ldndc_string_t const  ldndc::io::record_meta_ids[ldndc::io::_REG_CNT] =
{
    "source", "id",
    "x", "y", "z", "area",

    "datetime", "date", "time",
    "year", "month", "day", "hour", "minute", "second",

    "julianday", "subday", "layer"
};
// sk:off atomic_datatype_t const  ldndc::io::record_meta_types[ldndc::io::_REG_CNT] =
// sk:off {
// sk:off     LDNDC_STRING /*source*/, LDNDC_UINT32 /*id*/,
// sk:off     LDNDC_FLOAT64 /*x*/, LDNDC_FLOAT64 /*y*/, LDNDC_FLOAT64 /*z*/, LDNDC_FLOAT64 /*area*/,
// sk:off     LDNDC_UINT64 /*datetime*/, LDNDC_UINT64 /*date*/, LDNDC_UINT64 /*time*/,
// sk:off     LDNDC_UINT16 /*year*/, LDNDC_UINT8 /*month*/, LDNDC_UINT8 /*day*/,
// sk:off     LDNDC_UINT8 /*hour*/, LDNDC_UINT8/*minute*/, LDNDC_UINT8 /*second*/,
// sk:off     LDNDC_UINT16 /*julianday*/, LDNDC_UINT32 /*subday*/, LDNDC_INT32 /*layer*/
// sk:off };
ldndc::sink_record_meta_t::sink_record_meta_t()
    : useregs( 0),
      record_seqnb( invalid_int)
{
      t.ts = 0;
}
ldndc_sink_meta_timestamp_t
ldndc::sink_record_meta_t::timestamp_to_scalar()
const
{
    return  this->t.ts;
}
void
ldndc::sink_record_meta_t::timestamp_from_scalar(
    ldndc_sink_meta_timestamp_t const &  _ts)
{
    this->t.ts = _ts;
}
ldndc::sink_record_meta_srv_t::sink_record_meta_srv_t()
    : ts( 0), record_seqnb( invalid_int)
{
}
ldndc::sink_record_meta_srv_t::sink_record_meta_srv_t(
    ldndc::sink_record_meta_t const &  _r)
    : useregs( _r.useregs), ts( _r.timestamp_to_scalar()),
      record_seqnb( _r.record_seqnb)
{
}
ldndc::sink_record_meta_srv_t &
ldndc::sink_record_meta_srv_t::operator=(
    ldndc::sink_record_meta_t const &  _r)
{
    this->useregs = _r.useregs;
    this->ts = _r.timestamp_to_scalar();
    this->record_seqnb = _r.record_seqnb;

    return  *this;
}

ldndc::sink_entities_t const  ldndc::sink_entities_defaults =
{
    0, NULL, NULL
};

ldndc::sink_layout_t const  ldndc::sink_layout_defaults =
{
    0, NULL, NULL, NULL
};

ldndc::sink_entity_layout_t const  ldndc::sink_entity_layout_defaults =
{
    ldndc::sink_entities_defaults, ldndc::sink_layout_defaults
};

ldndc::sink_layout_hint_t const  ldndc::sink_layout_hint_defaults =
{
    0
};

ldndc::sink_fixed_record_t::sink_fixed_record_t()
    : data( NULL),
      gap( SINKGAP_NONE)
{
}
ldndc::sink_fixed_record_t const  ldndc::sink_fixed_record_defaults;

ldndc::sink_nonfixed_record_t::sink_nonfixed_record_t()
    : sink_fixed_record_t(),
      ents( ldndc::sink_entities_defaults), layout( ldndc::sink_layout_defaults)
{
}
ldndc::sink_nonfixed_record_t const  ldndc::sink_nonfixed_record_defaults;

ldndc::sink_public_record_t::sink_public_record_t()
    : sink_fixed_record_t(),
      timestamp( 0),
      layout( ldndc::sink_layout_defaults), layout_hint( ldndc::sink_layout_hint_defaults)
{
}
ldndc::sink_public_record_t const  ldndc::sink_public_record_defaults;

ldndc::sink_record_srv_t const  ldndc::sink_record_srv_defaults;
ldndc::sink_record_srv_t::sink_record_srv_t()
    : data( NULL), seq( NULL),
      gap( SINKGAP_NONE), timestamp( 0),
      ents( ldndc::sink_entities_defaults), layout( ldndc::sink_layout_defaults),
      layout_hint( ldndc::sink_layout_hint_defaults)
{
}
ldndc::sink_record_srv_t::sink_record_srv_t(
        ldndc::sink_fixed_record_t const &  _r)
    : meta( _r.meta), data( _r.data), seq( NULL),
      gap( _r.gap), timestamp( 0),
      ents( ldndc::sink_entities_defaults), layout( ldndc::sink_layout_defaults),
      layout_hint( ldndc::sink_layout_hint_defaults)
{
}
ldndc::sink_record_srv_t::sink_record_srv_t(
        ldndc::sink_nonfixed_record_t const &  _r)
    : meta( _r.meta), data( _r.data), seq( NULL),
      gap( _r.gap), timestamp( 0),
      ents( _r.ents), layout( _r.layout),
      layout_hint( ldndc::sink_layout_hint_defaults)
{
}
ldndc::sink_record_srv_t::sink_record_srv_t(
        ldndc::sink_public_record_t const &  _r)
    : meta( _r.meta), data( _r.data), seq( NULL),
      gap( _r.gap), timestamp( _r.timestamp),
      ents( ldndc::sink_entities_defaults), layout( _r.layout),
      layout_hint( _r.layout_hint)
{
}


//#define  LDNDC_OUTPUTTYPES_LOG_MALLOCS
#define  LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(__type__,__typeid__)        \
    if ( get_atomic_datatype_size( __typeid__) != sizeof( __type__))    \
    {                                    \
        return  LDNDC_ERR_RUNTIME_ERROR;                \
    }
#include  "log/cbm_baselog.h"
#include  "math/lmath-array.h"
lerr_t
ldndc::malloc_from_layout(
        void ***  _buf, ldndc::sink_layout_t const *  _layout)
{
    crabmeat_assert( _layout);
    void **  buf = (void **)CBM_DefaultAllocator->allocate_type< char * >( _layout->rank);
    if ( !buf)
    {
        return  LDNDC_ERR_NOMEM;
    }

    ldndc_output_size_t const *  entsizes = _layout->entsizes;

    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        buf[r] = NULL;

        size_t const  m_r = static_cast< size_t >( _layout->sizes[r]);
        if ( m_r == 0)
        {
            continue;
        }
        size_t  m = m_r;
        if ( entsizes)
        {
            m = static_cast< size_t >( cbm::sum( entsizes, m_r));
            entsizes += m_r;
        }

        if ( _layout->types[r] == LDNDC_BOOL)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_bool_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_bool_t,LDNDC_BOOL);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_CHAR)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_char_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_char_t,LDNDC_CHAR);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_INT8)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_int8_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_int8_t,LDNDC_INT8);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_UINT8)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_uint8_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_uint8_t,LDNDC_UINT8);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_INT16)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_int16_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_int16_t,LDNDC_INT16);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_UINT16)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_uint16_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_uint16_t,LDNDC_UINT16);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_INT32)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_int32_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_int32_t,LDNDC_INT32);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_UINT32)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_uint32_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_uint32_t,LDNDC_UINT32);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_INT64)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_int64_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_int64_t,LDNDC_INT64);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_UINT64)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_uint64_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_uint64_t,LDNDC_UINT64);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }

        else if ( _layout->types[r] == LDNDC_FLOAT32)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_flt32_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_flt32_t,LDNDC_FLOAT32);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_FLOAT64)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_flt64_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_flt64_t,LDNDC_FLOAT64);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
#ifdef  LDNDC_HAVE_FLOAT128
        else if ( _layout->types[r] == LDNDC_FLOAT128)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_flt128_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_flt128_t,LDNDC_FLOAT128);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
#endif

        else if ( _layout->types[r] == LDNDC_STRING)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc_string_t >( m);
            LDNDC_OUTPUTTYPES_MALLOCS_CHECK_SIZE(ldndc_string_t,LDNDC_STRING);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else if ( _layout->types[r] == LDNDC_COMPOUND)
        {
            buf[r] = (void *)CBM_DefaultAllocator->allocate_type< ldndc::marshal_compound_t >( m);
#ifdef  LDNDC_OUTPUTTYPES_LOG_MALLOCS
            CBM_LogDebug( "allocated ",m," ",LDNDC_ATOMIC_DATATYPE_NAMES[_layout->types[r]],"s  [@",(void*)buf[r],"{",(void*)buf,"}]");
#endif
        }
        else
        {
            CBM_LogWarn( "malloc_from_layout(): ",
                    "unsupported datatype, no memory allocated  [datatype=",_layout->types[r],"]");
            continue;
        }

        if ( !buf[r])
        {
            *_buf = buf;
            return  LDNDC_ERR_NOMEM;
        }
    }
    *_buf = buf;
    return  LDNDC_ERR_OK;
}

#ifdef  _DEBUG
#  include  "log/cbm_baselog.h"
#endif
lerr_t
ldndc::free_from_layout(
        void **  _buf, ldndc::sink_layout_t const *  _layout)
{
    if ( !_buf)
    {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _layout);

    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        if ( !_buf[r])
        {
            continue;
        }

        if ( _layout->types[r] == LDNDC_COMPOUND)
        {
            marshal_compound_t *  m_compound = (marshal_compound_t *)(_buf[r]);
            if ( m_compound->data && m_compound->mi)
            {
                ldndc::free_from_layout( m_compound->data, &m_compound->mi->layout);
            }
#ifdef  _DEBUG
            if ( m_compound->data && !m_compound->mi)
            {
                CBM_LogWarn( "free_from_layout: could not free compound due to missing layout information");
            }
#endif
        }
        CBM_DefaultAllocator->deallocate( _buf[r]);
    }
    CBM_DefaultAllocator->deallocate( _buf);

    return  LDNDC_ERR_OK;
}

#include  "math/lmath-array.h"
size_t
ldndc::sizeof_layout(
        ldndc::sink_layout_t const *  _layout)
{
    crabmeat_assert( _layout);

    size_t  memsize = 0;
    ldndc_output_size_t const *  entsizes = _layout->entsizes;

    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        size_t  sz = _layout->sizes[r];
        if ( entsizes)
        {
            sz = cbm::sum( entsizes, _layout->sizes[r]);
            entsizes += _layout->sizes[r];
        }
        memsize += get_atomic_datatype_size( _layout->types[r]) * sz;
    }

    return  memsize;
}

#include  "utils/lutils.h"
lerr_t
ldndc::memcpy_with_layout(
        void ** _dst, void const ** _src,
        ldndc::sink_layout_t const *  _layout)
{
    if ( !_dst || !_src || !_layout)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    ldndc_output_size_t const *  entsizes = _layout->entsizes;

    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        size_t  sz = _layout->sizes[r];
        if ( entsizes)
        {
            sz = cbm::sum( entsizes, _layout->sizes[r]);
            entsizes += _layout->sizes[r];
        }
        if ( sz == 0)
        {
            continue;
        }
        sz = get_atomic_datatype_size( _layout->types[r]) * sz;
        ::memcpy( _dst[r], _src[r], sz);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::memclr_with_layout(
        void ** _buf, ldndc::sink_layout_t const *  _layout)
{
    if ( !_buf || !_layout)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    ldndc_output_size_t const *  entsizes = _layout->entsizes;

    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        size_t  sz = _layout->sizes[r];
        if ( entsizes)
        {
            sz = cbm::sum( entsizes, _layout->sizes[r]);
            entsizes += _layout->sizes[r];
        }
        if ( sz == 0)
        {
            continue;
        }
        sz = get_atomic_datatype_size( _layout->types[r]) * sz;
        ::memset( _buf[r], 0, sz);
    }

    return  LDNDC_ERR_OK;
}

#include  <stdio.h>
lerr_t
ldndc::dump_from_layout(
        void const **  _buf, ldndc::sink_layout_t const *  _layout)
{
    FILE *  o_file = stdout;

    crabmeat_assert( _layout);
    void **  buf = (void **)CBM_DefaultAllocator->allocate_type< char * >( _layout->rank);
    if ( !buf)
    {
        return  LDNDC_ERR_NOMEM;
    }

    ldndc_output_size_t const *  entsizes = _layout->entsizes;

    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        size_t const  m_r = static_cast< size_t >( _layout->sizes[r]);
        if ( m_r == 0)
        {
            continue;
        }
        size_t  m = m_r;
        if ( entsizes)
        {
            m = static_cast< size_t >( cbm::sum( entsizes, m_r));
            entsizes += m_r;
        }

        if ( _layout->types[r] == LDNDC_BOOL)
        {
            ldndc_bool_t const *  d = (ldndc_bool_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_STRING, ( *d) ? "true" : "false");
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_STRING, ( *d) ? "true" : "false");
            }
        }
        else if (( _layout->types[r] == LDNDC_CHAR) || ( _layout->types[r] == LDNDC_BYTE))
        {
            ldndc_char_t const *  d = (ldndc_char_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_CHAR, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_CHAR, *d);
            }
        }
        else if ( _layout->types[r] == LDNDC_INT8)
        {
            ldndc_int8_t const *  d = (ldndc_int8_t const *)_buf[r];
            ldndc_int32_t  d_int32 = static_cast< ldndc_int32_t >( *d);
            fprintf( o_file, LDNDC_PRINTF_SEQ_INT32, d_int32);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                d_int32 = static_cast< ldndc_int32_t >( *d);
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_INT32, d_int32);
            }
        }
        else if ( _layout->types[r] == LDNDC_UINT8)
        {
            ldndc_uint8_t const *  d = (ldndc_uint8_t const *)_buf[r];
            ldndc_uint32_t  d_uint32 = static_cast< ldndc_uint32_t >( *d);
            fprintf( o_file, LDNDC_PRINTF_SEQ_UINT32, d_uint32);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                d_uint32 = static_cast< ldndc_uint32_t >( *d);
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_UINT32, d_uint32);
            }
        }
        else if ( _layout->types[r] == LDNDC_INT16)
        {
            ldndc_int16_t const *  d = (ldndc_int16_t const *)_buf[r];
            ldndc_int32_t  d_int32 = static_cast< ldndc_int32_t >( *d);
            fprintf( o_file, LDNDC_PRINTF_SEQ_INT32, d_int32);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                d_int32 = static_cast< ldndc_int32_t >( *d);
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_INT32, d_int32);
            }
        }
        else if ( _layout->types[r] == LDNDC_UINT16)
        {
            ldndc_uint16_t const *  d = (ldndc_uint16_t const *)_buf[r];
            ldndc_uint32_t  d_uint32 = static_cast< ldndc_uint32_t >( *d);
            fprintf( o_file, LDNDC_PRINTF_SEQ_UINT32, d_uint32);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                d_uint32 = static_cast< ldndc_uint32_t >( *d);
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_UINT32, d_uint32);
            }
        }
        else if ( _layout->types[r] == LDNDC_INT32)
        {
            ldndc_int32_t const *  d = (ldndc_int32_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_INT32, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_INT32, *d);
            }
        }
        else if ( _layout->types[r] == LDNDC_UINT32)
        {
            ldndc_uint32_t const *  d = (ldndc_uint32_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_UINT32, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_UINT32, *d);
            }
        }
        else if ( _layout->types[r] == LDNDC_INT64)
        {
            ldndc_int64_t const *  d = (ldndc_int64_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_INT64, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_INT64, *d);
            }
        }
        else if ( _layout->types[r] == LDNDC_UINT64)
        {
            ldndc_uint64_t const *  d = (ldndc_uint64_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_UINT64, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_UINT64, *d);
            }
        }

        else if ( _layout->types[r] == LDNDC_FLOAT32)
        {
            ldndc_flt32_t const *  d = (ldndc_flt32_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_FLT32, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_FLT32, *d);
            }
        }
        else if ( _layout->types[r] == LDNDC_FLOAT64)
        {
            ldndc_flt64_t const *  d = (ldndc_flt64_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_FLT64, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_FLT64, *d);
            }
        }
#ifdef  LDNDC_HAVE_FLOAT128
        else if ( _layout->types[r] == LDNDC_FLOAT128)
        {
            ldndc_flt128_t const *  d = (ldndc_flt128_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_FLT128, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_FLT128, *d);
            }
        }
#endif
        else if ( _layout->types[r] == LDNDC_STRING)
        {
            ldndc_string_t const *  d = (ldndc_string_t const *)_buf[r];
            fprintf( o_file, LDNDC_PRINTF_SEQ_STRING, *d);
            ++d;
            for ( size_t  k = 1;  k < m;  ++k, ++d)
            {
                fprintf( o_file, " " LDNDC_PRINTF_SEQ_STRING, *d);
            }
        }
        else
        {
            CBM_LogWarn( "dump_from_layout(): ",
                    "unsupported datatype, no memory allocated  [datatype=",_layout->types[r],"]");
            fprintf( o_file, "[unsupported datatype]");
        }

        fprintf( o_file, "\n");
    }

    return  LDNDC_ERR_OK;
}

