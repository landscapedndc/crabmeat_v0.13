/*!
 * @brief
 *    Global output sink information
 *
 * @author
 *    Steffen Klatt (created on: oct 31, 2013),
 *    Edwin Haas
 */

#ifndef  LDNDC_IO_SINKINFO_H_
#define  LDNDC_IO_SINKINFO_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"
#include  "io/sink-attrs.h"

namespace  ldndc
{
    struct  sink_info_t;
    struct  standard_sink_info_t;

    /* set defaults: give identifier and/or name, optionally an index.
     * if id or name is NULL, index is used to get default value
     */
    CBM_API lerr_t sink_info_default(
            standard_sink_info_t const *, sink_info_t *,
            char const * /*identifier*/, char const * /*name*/,
            unsigned char = invalid_uchr /*index for default sinks*/);
    CBM_API  bool sink_is_nodev(
            sink_info_t const *);
    CBM_API  bool sink_is_black_hole(
            sink_info_t const *);

    struct CBM_API sink_info_t
    {
        /* sink identifier */
        std::string  identifier;
        /* sink index */
        int  index;

        /* sink name ( e.g. file name) */
        std::string  name;
        int  name_forced;

        /* sink name prefix ( e.g. path) */
        std::string  name_prefix;

        /* receiver type (e.g. output writer) */
        char  rcv_type;
        /* sink format (e.g. txt) */
        cbm::string_t  format;
        /* sink format version specifies target layout */
        int  format_version;

        sink_attributes_t  attrs;

        sink_info_t();
        std::string  full_name() const;
        std::string  full_path() const;
    };

    extern CBM_API int const invalid_sink_index;

#define  BLACK_HOLE_SINK_NAME "*"
#define  NODEV_SINK_NAME "|"
    /* default output sinks, whose names are hard-coded */
    struct CBM_API standard_sink_info_t
    {
        /* sink identifier */
        char const *  identifier;
        /* sink name ( e.g. file name) */
        char const *  name;

        /* receiver type (e.g. output writer) */
        char  rcv_type;
    };
#define  STANDARD_SINKS_CNT  (\
          6*8/*standardsinks*/ \
        + 7/*arablereport*/ \
        + 3/*ggcmireport*/ \
        + 2/*eventreport*/ \
        + 3/*ecosystem*/ \
        + 4/*ggcmi*/ \
        + 1/*inventory*/ \
        + 1/*dss*/ \
        + 1/*null*/ \
        + 1/*sentinel*/)
    extern CBM_API standard_sink_info_t const STANDARD_SINKS[STANDARD_SINKS_CNT];

    CBM_API std::ostream &
    operator<<( std::ostream &,
            ldndc::sink_info_t const &);

}  /*  namespace ldndc  */


#endif  /*  !LDNDC_IO_SINKINFO_H_  */
