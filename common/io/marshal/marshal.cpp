/*!
 * @brief
 *    marshal data structures and functions (implementation)
 *
 * @author
 *    steffen klatt (created on: jan 19, 2014),
 *    edwin haas
 */

#include  "io/marshal/marshal.h"

ldndc::marshalling_buffer_t const  ldndc::marshalling_buffer_nodata = NULL;

ldndc::marshal_compound_t const  ldndc::marshal_compound_defaults =
{
    ldndc::marshalling_buffer_nodata,
    NULL,

    ""
};

