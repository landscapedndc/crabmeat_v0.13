/*!
 * @brief
 *    marshal data structures and functions
 *
 * @author
 *    steffen klatt (created on: jan 18, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_IO_MARSHAL_H_
#define  LDNDC_IO_MARSHAL_H_

#include  "crabmeat-common.h"
#include  "io/outputtypes.h"

namespace  ldndc
{

typedef  void** marshalling_buffer_t;
extern CBM_API marshalling_buffer_t const  marshalling_buffer_nodata;

struct CBM_API marshal_info_t
{
        sink_entities_t  ents;
        sink_layout_t  layout;

        sink_layout_hint_t  layout_hint;
};


struct  marshal_compound_t
{
    marshalling_buffer_t  data;
    marshal_info_t const *  mi;

    ldndc_string_t  name;
};
extern CBM_API marshal_compound_t const  marshal_compound_defaults;



}

#endif  /*  !LDNDC_IO_MARSHAL_H_  */

