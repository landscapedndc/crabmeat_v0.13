/*!
 * @brief
 *
 * @author
 *    Steffen Klatt (created on: oct 31, 2013),
 *    Edwin Haas
 */

#include  "io/sink-info.h"
#include  "cbm_rtcfg.h"
#include  "log/cbm_baselog.h"

#include  "io/default-streams.h"
#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"
static
char const *
ldndc_get_format_from_name( char const *  _format_name)
    { return  _format_name; }
lerr_t
ldndc::sink_info_default(
        ldndc::standard_sink_info_t const * _standard_sinks,
        sink_info_t *  _sink,
        char const *  _identifier, char const *  _name,
        unsigned char  _slot)
{
    crabmeat_assert( _sink);

    if (( !_identifier && ( _slot == invalid_uchr)) || (( _slot != invalid_uchr) && ( _slot >= STANDARD_SINKS_CNT)))
    {
        CBM_LogError( "[BUG]  insufficient information for resolving standard sink");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    unsigned char  sink_slot = _slot;
    if ( _identifier && ( _slot == invalid_uchr))
    {
        unsigned char  s = 0;
        /* find from default sink list */
        while ( _standard_sinks[s].identifier)
        {
            if ( cbm::is_equal( _identifier, _standard_sinks[s].identifier))
            {
                sink_slot = s;
                break;
            }
            ++s;
        }
    }

    _sink->identifier = ( _identifier) ? _identifier : _standard_sinks[sink_slot].identifier;
    _sink->index = 0;
    _sink->name_forced = 0;
    _sink->name = ( _name) ? _name : (( sink_slot == invalid_uchr) ? _sink->identifier : _standard_sinks[sink_slot].name);
    if ( !_name && !sink_is_black_hole( _sink) && !is_standard_sink( _sink->name.c_str()))
    {
        _sink->name += '.';
        _sink->name += CBM_LibRuntimeConfig.io.default_sink_format;
    }
    _sink->name_prefix = "";
    _sink->rcv_type =
	    ( sink_slot == invalid_uchr) ? 'O' : _standard_sinks[sink_slot].rcv_type;
    _sink->format =
	    ldndc_get_format_from_name( CBM_LibRuntimeConfig.io.default_sink_format.c_str());
    _sink->attrs = sink_attributes_defaults;

    return  LDNDC_ERR_OK;
}

#include  "string/lstring-compare.h"
bool
ldndc::sink_is_black_hole(
        sink_info_t const *  _sink)
{
    crabmeat_assert( _sink);
    return  ( _sink->attrs.size_limit == 0) || cbm::is_equal( _sink->name, BLACK_HOLE_SINK_NAME);
}
bool
ldndc::sink_is_nodev(
        sink_info_t const *  _sink)
{
    crabmeat_assert( _sink);
    return  cbm::is_equal( _sink->name, NODEV_SINK_NAME);
}

ldndc::sink_info_t::sink_info_t()
        : identifier( ""),
          index( 0),
          name( ""),
          name_forced( 0),
          name_prefix( ""),
          rcv_type( '-'),
          format( ""),
          attrs( sink_attributes_defaults)
{
}

#include  "string/lstring-transform.h"
#include  "string/lstring-tokenizer.h"
#include  "utils/lutils-fs.h"
std::string
ldndc::sink_info_t::full_name()
const
{
    if ( cbm::is_empty( this->identifier))
    {
        return  ldndc_empty_string;
    }
    if ( this->format == "")
    {
        return  ldndc_empty_string;
    }
    if ( sink_is_black_hole( this))
    {
        return  BLACK_HOLE_SINK_NAME;
    }
    if ( sink_is_nodev( this))
    {
        return  NODEV_SINK_NAME;
    }
    if ( is_standard_sink( this->name.c_str()))
    {
        return  this->name;
    }

    std::stringstream  this_index;
    if ( this->index > 0)
    {
           this_index << "-" << this->index;
    }

    std::stringstream  this_full_name;
    if ( !cbm::is_absolute_path( this->name_prefix.c_str()))
    {
        this_full_name << "%O";
    }
    this_full_name << this->name_prefix << "%S";
    if (( this->name_prefix.size() > 0)
            && ( this->name_prefix[this->name_prefix.size()-1] == '/'))
    {
        this_full_name << '/';
    }

    if ( this->name_forced)
    {
        this_full_name << this->name;
    }
    else
    {
        cbm::tokenizer  name_tokens( this->name, '.');
        if ( name_tokens.token_cnt() == 0)
        {
            this_full_name << this_index.str();
        }
        else if ( name_tokens.token_cnt() == 1)
        {
            this_full_name << name_tokens.n_str( 0, CNAME_STRIP_HEAD|CNAME_STRIP_TAIL).c_str() << this_index.str();
        }
        else if ( name_tokens.token_cnt() == 2)
        {
            this_full_name << name_tokens.n_str( 0, CNAME_STRIP_HEAD).c_str();
            this_full_name << this_index.str();
            this_full_name << '.';
            this_full_name << name_tokens.n_str( 1, CNAME_STRIP_TAIL).c_str();
        }
        else
        {
            size_t const  T = name_tokens.token_cnt() - 1;
            this_full_name << name_tokens.n_str( 0, CNAME_STRIP_HEAD).c_str();
            for ( size_t  t = 1;  t < T;  ++t)
            {
                this_full_name << '.' << name_tokens[t];
            }
            this_full_name << this_index.str();
            this_full_name << '.' << name_tokens.n_str( T, CNAME_STRIP_TAIL).c_str();
        }
    }

    std::string const  fname = this_full_name.str();
    std::string  fname_expanded;
    lerr_t const  rc_fe = cbm::format_expand( &fname_expanded, &fname);
    if ( rc_fe)
    {
        return  "!ERR";
    }
    return  fname_expanded;
}

#include  "utils/lutils-fs.h"
std::string
ldndc::sink_info_t::full_path()
const
{
    std::string  dst = this->full_name();
    std::string  src = this->full_name();
    cbm::dirname( &dst, src.c_str());
    dst += '/';
    return  dst;
}

int const  ldndc::invalid_sink_index = -1;

ldndc::standard_sink_info_t const  ldndc::STANDARD_SINKS[STANDARD_SINKS_CNT] =
{
    { "airchemistrysubdaily", "airchemistry-subdaily", 'O'},
    { "airchemistrylayersubdaily", "airchemistry-layer-subdaily", 'O'},
    { "airchemistrydaily", "airchemistry-daily", 'O'},
    { "airchemistrylayerdaily", "airchemistry-layer-daily", 'O'},
    { "airchemistryyearly", "airchemistry-yearly", 'O'},
    { "airchemistrylayeryearly", "airchemistry-layer-yearly", 'O'},
    { "airchemistrycumulativedaily", "airchemistry-cumulative-daily", 'O'},
    { "airchemistryhorizonsdaily", "airchemistry-horizons-daily", 'O'},
    
    { "microclimatesubdaily", "microclimate-subdaily", 'O'},
    { "microclimatelayersubdaily", "microclimate-layer-subdaily", 'O'},
    { "microclimatedaily", "microclimate-daily", 'O'},
    { "microclimatelayerdaily", "microclimate-layer-daily", 'O'},
    { "microclimateyearly", "microclimate-yearly", 'O'},
    { "microclimatelayeryearly", "microclimate-layer-yearly", 'O'},
    { "microclimatecumulativedaily", "microclimate-cumulative-daily", 'O'},
    { "microclimatehorizonsdaily", "microclimate-horizons-daily", 'O'},

    { "physiologysubdaily", "physiology-subdaily", 'O'},
    { "physiologylayersubdaily", "physiology-layer-subdaily", 'O'},
    { "physiologydaily", "physiology-daily", 'O'},
    { "physiologylayerdaily", "physiology-layer-daily", 'O'},
    { "physiologyyearly", "physiology-yearly", 'O'},
    { "physiologylayeryearly", "physiology-layer-yearly", 'O'},
    { "physiologycumulativedaily", "physiology-cumulative-daily", 'O'},
    { "physiologyhorizonsdaily", "physiology-horizons-daily", 'O'},

    { "soilchemistrysubdaily", "soilchemistry-subdaily", 'O'},
    { "soilchemistrylayersubdaily", "soilchemistry-layer-subdaily", 'O'},
    { "soilchemistrydaily", "soilchemistry-daily", 'O'},
    { "soilchemistrylayerdaily", "soilchemistry-layer-daily", 'O'},
    { "soilchemistryyearly", "soilchemistry-yearly", 'O'},
    { "soilchemistrylayeryearly", "soilchemistry-layer-yearly", 'O'},
    { "soilchemistrycumulativedaily", "soilchemistry-cumulative-daily", 'O'},
    { "soilchemistryhorizonsdaily", "soilchemistry-horizons-daily", 'O'},
    
    { "vegstructuresubdaily", "vegstructure-subdaily", 'O'},
    { "vegstructurelayersubdaily", "vegstructure-layer-subdaily", 'O'},
    { "vegstructuredaily", "vegstructure-daily", 'O'},
    { "vegstructurelayerdaily", "vegstructure-layer-daily", 'O'},
    { "vegstructureyearly", "vegstructure-yearly", 'O'},
    { "vegstructurelayeryearly", "vegstructure-layer-yearly", 'O'},
    { "vegstructurecumulativedaily", "vegstructure-cumulative-daily", 'O'},
    { "vegstructurehorizonsdaily", "vegstructure-horizons-daily", 'O'},
    
    { "watercyclesubdaily", "watercycle-subdaily", 'O'},
    { "watercyclelayersubdaily", "watercycle-layer-subdaily", 'O'},
    { "watercycledaily", "watercycle-daily", 'O'},
    { "watercyclelayerdaily", "watercycle-layer-daily", 'O'},
    { "watercycleyearly", "watercycle-yearly", 'O'},
    { "watercyclelayeryearly", "watercycle-layer-yearly", 'O'},
    { "watercyclecumulativedaily", "watercycle-cumulative-daily", 'O'},
    { "watercyclehorizonsdaily", "watercycle-horizons-daily", 'O'},

    { "arablereportcut", "report-cut", 'O'},
    { "arablereportfertilize", "report-fertilize", 'O'},
    { "arablereportgraze", "report-graze", 'O'},
    { "arablereportharvest", "report-harvest", 'O'},
    { "arablereportirrigate", "report-irrigate", 'O'},
    { "arablereportmanure", "report-manure", 'O'},
    { "arablereporttill", "report-till", 'O'},

    { "ggcmireportfertilize", "report-fertilize", 'O'},
    { "ggcmireportharvest", "report-harvest", 'O'},
    { "ggcmireportmanure", "report-manure", 'O'},

    { "eventreportthin", "report-thin", 'O'},
    { "eventreportthrow", "report-throw", 'O'},

    { "ecosystemsubdaily", "ecosystem-subdaily", 'O'},
    { "ecosystemdaily", "ecosystem-daily", 'O'},
    { "ecosystemyearly", "ecosystem-yearly", 'O'},

    { "ggcmidaily", "ggcmi-daily", 'O'},
    { "ggcmimonthly", "ggcmi-monthly", 'O'},
    { "ggcmiyearly", "ggcmi-yearly", 'O'},
    { "ggcmiseasonal", "ggcmi-seasonal", 'O'},

    { "inventory", "inventory", 'O'},

    { "dssyearly", "dss-yearly", 'O'},

    { "nullsink", "*", 'O'},

    { NULL, NULL, '-'}
};

std::ostream &
ldndc::operator<<(
        std::ostream &  _out,
        ldndc::sink_info_t const &  _si)
{
    return  _out
        << '{'
        << '[' << _si.rcv_type << ']'
               << ':' << ' '
        << "identifier=" << _si.identifier
        << ",index=" << _si.index
        << ",name=" << _si.name
        << ",prefix=" << _si.name_prefix
        << ",format=" << ((_si.format != "") ? _si.format : "?")
        << '}';
}
