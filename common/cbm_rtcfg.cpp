/*!
 * @brief
 *    library configuration structure
 *
 * @author 
 *    steffen klatt (created on: sep. 10, 2013),
 *    edwin haas
 */

#include  "cbm_rtcfg.h"

#include  "time/cbm_time.h"
#include  "log/cbm_baselog.h"

#include  "io/default-streams.h"

#include  "math/cbm_math.h"
#include  "utils/cbm_utils.h"

#ifdef  CRABMEAT_OPENMP
#  include  <omp.h>
#endif

char const *  cbm::DOT_PATH = LDNDC_DOT_PATH;
char const *  cbm::CONFIG_FILENAME = LDNDC_CONFIG_FILENAME;
char const *  cbm::CONFIG_FILE = LDNDC_CONFIG_FILE;

char const *  cbm::LOG_FILE = LDNDC_LOG_FILE;

char const *  cbm::RESOURCES_PATH = LDNDC_RESOURCES_PATH;
char const *  cbm::RESOURCES_FILENAME = LDNDC_RESOURCES_FILENAME;

char const *  cbm::INPUT_PATH = LDNDC_INPUT_PATH;
char const *  cbm::OUTPUT_PATH = LDNDC_OUTPUT_PATH;

char const *  cbm::PROJECT_FILE = LDNDC_PROJECT_FILE;

double const  cbm::BALANCE_TOLERANCE = 1.0e-09;

cbm::package_version_t::package_version_t()
    : m_name( NULL), m_version_long( NULL), m_version( NULL)
{}
char const *
cbm::package_version_t::name() const
{
    static char const *  empty_pkgv = "";
    return  this->m_name ?
            this->m_name : empty_pkgv;
}
void
cbm::package_version_t::set_name(
                char const *  _name)
{
    this->m_name = _name;
}
char const *
cbm::package_version_t::version_long() const
{
    static char const *  empty_pkgv = "";
    return  this->m_version_long ?
            this->m_version_long : empty_pkgv;
}
void
cbm::package_version_t::set_version_long(
                char const *  _version_long)
{
    this->m_version_long = _version_long;
}
char const *
cbm::package_version_t::version() const
{
    static char const *  empty_pkgv = "";
    return  this->m_version ?
            this->m_version : empty_pkgv;
}
void
cbm::package_version_t::set_version(
                char const *  _version)
{
    this->m_version = _version;
}

cbm::package_version_t &  cbm::CBM_PackageUser()
{
    static  package_version_t  global_CBM_PackageUser;
    return  global_CBM_PackageUser;
}


static cbm::sclock_t  cbm_clk;
cbm::libconfig_t::libconfig_t()
            : status( 0), cfg( NULL), prj( NULL),
                clk( &cbm_clk), kfactory( NULL)
{
    /* project meta information */
    this->pc.project_name = "";
    this->pc.project_filename = "";
    this->pc.global_source_prefix = "";
    this->pc.global_sink_prefix = "";
    this->pc.scenario_name = "";

    /* general library related configuration settings */
    this->cc.math_epsilon = LDNDC_EPS(double);
    this->cc.random_seed = 0;

    /* logging related configuration settings */
    this->lc.log_append = 1;
    this->lc.log_buffersize = -1;

    /* resource monitoring related configuration settings */
    this->rc.pid = cbm::get_pid();
    this->rc.rank = -1; /* NOTE do not attempt to set rank here, because MPI_Init may not have been called yet */
    this->rc.measure_time = 0;

    cbm::ifaddr_t  iface_addr;
    cbm::ifaddr( &iface_addr);
    this->rc.ipaddr = iface_addr.valid() ? iface_addr.addr : invalid_str;
    this->rc.ipaddr_version = iface_addr.version;
    this->rc.port = 6619;

    this->rc.coupler_statistics = false;

    /* things used during signal handling */
    for ( int  k = 0;  k < CRABMEAT_OPENMP_MAXTHREADS;  ++k)
    {
        this->sc.current_kernel[k] = -1;
    }
    this->sc.request_term = 0;
    this->sc.clean_term = 1;

    /* kernel related configuration settings */
    this->kc.balance_check = 0;
    this->kc.balance_tolerance = cc.math_epsilon;

    /* i/o related global settings */
    this->io.dot_path = "";
    this->set_dot_path();
    this->io.current_working_path = "";
    this->set_current_working_path();
    this->io.input_path = "";
    this->io.output_path = "";
    this->io.resources_path = "";
    this->io.udunits_path = "";
    this->io.service_registry_url = "";
    this->io.have_output = false;
    this->io.create_checkpoint = false;
    this->io.restore_checkpoint = false;
	this->io.default_sink_format = ldndc::DEFAULT_OUTPUT_SINK_FORMAT;
}

void
cbm::libconfig_t::set_current_working_path()
{
    char const *  cwd = cbm::getcwd();
    if ( !cwd)
    {
        CBM_LogError( "unable to obtain current working directory");
        this->status = -1;
    }
    else
    {
        this->io.current_working_path = cwd;
        cbm::freecwd( cwd);
    }
}
#include  "string/cbm_string.h"
void
cbm::libconfig_t::set_dot_path()
{
    this->io.dot_path = cbm::DOT_PATH;
    cbm::format_expand( &this->io.dot_path);
}

void
cbm::libconfig_t::dump_processed_kernels()
const
{
    CBM_LogWrite( "recently processed kernels:");
    int  k = 0;
    while (( k < CRABMEAT_OPENMP_MAXTHREADS) && ( this->sc.current_kernel[k] != -1))
    {
#ifdef  CRABMEAT_OPENMP
        CBM_LogWrite( "\tkernel ID ", this->sc.current_kernel[k],
                        " on thread #", omp_get_thread_num());
#else
        CBM_LogWrite( "\tkernel ID ", this->sc.current_kernel[k]);
#endif
        ++k;
    }
    if ( k == 0)
    {
        CBM_LogWrite( "\tnot processing any kernel");
    }
}
void
cbm::libconfig_t::dump_clock() const
{
    if ( this->clk)
        { CBM_LogWrite( "clock ", this->clk->to_string()); }
    else
        { CBM_LogWrite( "clock not set"); }
}

#include  "time/cbm_clock.h"
std::string  cbm::libconfig_t::now_as_iso8601() const
    { return this->clk->now().as_iso8601(); }

cbm::libconfig_t &  cbm::LibRuntimeConfig()
{
    static  libconfig_t  global_CBM_LibRuntimeConfig;
    return  global_CBM_LibRuntimeConfig;
}

