/*!
 * @brief
 *    interface for serializing/deserializing data
 *
 * @author
 *    steffen klatt (created on: nov 27, 2014)
 */

#include  "utils/lutils-serialize.h"
#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
#  include  <msgpack.h>
#endif
#include  <stdlib.h>

#include  "log/cbm_baselog.h"

void
cbm::free_pack_buffer( char *  _buf)
{
        if ( _buf)
        {
                free( _buf);
        }
}

#define  __LPACK_TYPE  flt32
#define  __MPACK_TYPE  float
#include  "utils/lutils-serialize-pack.cpp"
#define  __LPACK_TYPE  flt64
#define  __MPACK_TYPE  double
#include  "utils/lutils-serialize-pack.cpp"
#define  __LPACK_TYPE  int32
#include  "utils/lutils-serialize-pack.cpp"
#define  __LPACK_TYPE  uint32
#include  "utils/lutils-serialize-pack.cpp"
#define  __LPACK_TYPE  int64
#include  "utils/lutils-serialize-pack.cpp"
#define  __LPACK_TYPE  uint64
#include  "utils/lutils-serialize-pack.cpp"

lerr_t
cbm::pack(
                char **  _buf, size_t *  _buf_size,
                void const *  _array, int  _array_rank, int *  _extents,
                atomic_datatype_t  _datatype)
{
        lerr_t  rc_pack = LDNDC_ERR_INVALID_ARGUMENT;
        if ( _datatype == LDNDC_FLOAT64)
        {
                rc_pack = pack_array_flt64_s( _buf, _buf_size,
                                static_cast< ldndc_flt64_t const * >( _array), _array_rank, _extents);
        }
    else if ( _datatype == LDNDC_INT32)
    {
                rc_pack = pack_array_int32_s( _buf, _buf_size,
                                static_cast< ldndc_int32_t const * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_UINT32)
    {
                rc_pack = pack_array_uint32_s( _buf, _buf_size,
                                static_cast< ldndc_uint32_t const * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_UINT64)
    {
                rc_pack = pack_array_uint64_s( _buf, _buf_size,
                                static_cast< ldndc_uint64_t const * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_INT64)
    {
                rc_pack = pack_array_int64_s( _buf, _buf_size,
                                static_cast< ldndc_int64_t const * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_FLOAT32)
        {
                rc_pack = pack_array_flt32_s( _buf, _buf_size,
                                static_cast< ldndc_flt32_t const * >( _array), _array_rank, _extents);
        }
        else
        {
        CBM_LogError( "Packing data of type \"",
                LDNDC_ATOMIC_DATATYPE_NAMES[_datatype],"\" not implemented");
        }
        return  rc_pack;
}
#include  "utils/base64/encode.h"
void
cbm::free_pack64_buffer( char *  _buf)
{
    cbm::base64::encoder::free_buffer( _buf);
}
lerr_t
cbm::pack64(
                char **  _buf, size_t *  _buf_size,
                void const *  _array, int  _array_rank, int *  _extents,
                atomic_datatype_t  _datatype)
{
    lerr_t  rc_pack = 
        cbm::pack( _buf, _buf_size, _array, _array_rank, _extents, _datatype);
    if ( rc_pack)
    {
        return  LDNDC_ERR_FAIL;
    }
    if ( _buf && *_buf_size > 0u)
    {
        char *  b64buf = NULL;
        size_t  b64buf_size = 0;
        cbm::base64::encoder  b64enc;
        int  rc_enc64 = b64enc.encode( *_buf, *_buf_size, &b64buf, &b64buf_size);
        cbm::free_pack_buffer( *_buf);
        if ( rc_enc64)
        {
            *_buf = NULL;
            *_buf_size = 0;
            return  LDNDC_ERR_FAIL;
        }
        *_buf = b64buf;
        *_buf_size = b64buf_size;
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_INVALID_ARGUMENT;
}

#define  __LUNPACK_TYPE  flt32
#define  __MUNPACK_TYPE  f64
#include  "utils/lutils-serialize-unpack.cpp"
#define  __LUNPACK_TYPE  flt64
#define  __MUNPACK_TYPE  f64
#include  "utils/lutils-serialize-unpack.cpp"
#define  __LUNPACK_TYPE  int32
#define  __MUNPACK_TYPE  i64
#include  "utils/lutils-serialize-unpack.cpp"
#define  __LUNPACK_TYPE  uint32
#define  __MUNPACK_TYPE  u64
#include  "utils/lutils-serialize-unpack.cpp"
#define  __LUNPACK_TYPE  int64
#define  __MUNPACK_TYPE  i64
#include  "utils/lutils-serialize-unpack.cpp"
#define  __LUNPACK_TYPE  uint64
#define  __MUNPACK_TYPE  u64
#include  "utils/lutils-serialize-unpack.cpp"

lerr_t
cbm::unpack(
                char const *  _buf, size_t  _buf_size,
                void *  _array, int  _array_rank, int *  _extents,
                atomic_datatype_t  _datatype)
{
        lerr_t  rc_unpack = LDNDC_ERR_INVALID_ARGUMENT;
        if ( _datatype == LDNDC_FLOAT64)
        {
                rc_unpack = unpack_array_flt64_s( _buf, _buf_size,
                                static_cast< ldndc_flt64_t * >( _array), _array_rank, _extents);
        }
    else if ( _datatype == LDNDC_INT32)
    {
                rc_unpack = unpack_array_int32_s( _buf, _buf_size,
                                static_cast< ldndc_int32_t * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_UINT32)
    {
                rc_unpack = unpack_array_uint32_s( _buf, _buf_size,
                                static_cast< ldndc_uint32_t * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_UINT64)
    {
                rc_unpack = unpack_array_uint64_s( _buf, _buf_size,
                                static_cast< ldndc_uint64_t * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_INT64)
    {
                rc_unpack = unpack_array_int64_s( _buf, _buf_size,
                                static_cast< ldndc_int64_t * >( _array), _array_rank, _extents);
    }
    else if ( _datatype == LDNDC_FLOAT32)
        {
                rc_unpack = unpack_array_flt32_s( _buf, _buf_size,
                                static_cast< ldndc_flt32_t * >( _array), _array_rank, _extents);
        }
        else
        {
        CBM_LogError( "Unpacking data of type \"",
                LDNDC_ATOMIC_DATATYPE_NAMES[_datatype],"\" not implemented");
        }

        return  rc_unpack;
}
#include  "utils/base64/decode.h"
lerr_t
cbm::unpack64(
                char const *  _buf, size_t  _buf_size,
                void *  _array, int  _array_rank, int *  _extents,
                atomic_datatype_t  _datatype)
{
    if ( _buf && _buf_size > 0u)
    {
        char *  b64buf = NULL;
        size_t  b64buf_size = 0;
        cbm::base64::decoder  b64dec;
        int  rc_dec64 = b64dec.decode( _buf, _buf_size, &b64buf, &b64buf_size);

        if ( rc_dec64)
        {
            return  LDNDC_ERR_FAIL;
        }
        lerr_t  rc_unpack = 
            cbm::unpack( b64buf, b64buf_size, _array, _array_rank, _extents, _datatype);
        b64dec.free_buffer( b64buf);
        if ( rc_unpack)
        {
            return  LDNDC_ERR_FAIL;
        }
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_INVALID_ARGUMENT;
}

