/*!
 * @brief
 *    filesystem related functions  (implementation)
 *
 * @author
 *    steffen klatt (created on: may 21, 2013),
 *    edwin haas
 */

#include  "crabmeat-common.h"
#include  "utils/lutils-fs.h"
#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

#if  defined(__MINGW32__) && defined(__STRICT_ANSI__)
#  undef  __STRICT_ANSI__
#endif

#if  defined(CRABMEAT_OS_MSWIN)
#  include  <direct.h>
#  if  !defined(S_IFDIR)
#    define  S_IFDIR  _S_IFDIR
#  endif
#  if  !defined(S_IFREG)
#    define  S_IFREG  _S_IFREG
#  endif
#else
#endif
#include  <errno.h>
#include  <sys/stat.h>
#include  <sys/types.h>

#include  <stdio.h>

bool
cbm::is_file(
        char const *  _fn)
{
    if ( !_fn || cbm::is_empty( _fn))
    {
        return  false;
    }

    struct stat  f_stat;

    int  rc_stat = stat( _fn, &f_stat);
    if ( rc_stat == -1)
    {
        return  false;
    }
    else if ( f_stat.st_mode & S_IFREG)
    {
        return  true;
    }

    return  false;
}

bool
cbm::is_dir(
        char const *  _path)
{
    if ( !_path || cbm::is_empty( _path))
    {
        return  false;
    }

    struct stat  d_stat;

    int  rc_stat = stat( _path, &d_stat);
    if ( rc_stat == -1)
    {
        return  false;
    }
    else if ( d_stat.st_mode & S_IFDIR)
    {
        return  true;
    }

    return  false;
}

#include  "regex/cbm_regex.h"
bool
cbm::is_driveletter(
        char const *  _path)
{
    if ( !_path)
    {
        return  false;
    }

    int const  pathname_length = cbm::strlen( _path);
    if ((( pathname_length == 2) || ( pathname_length == 3))
            && (( cbm::regex_match( "^\\s*[a-zA-Z]:$", _path, pathname_length, NULL, 0) >= 2)
            || ( cbm::regex_match( "^\\s*[a-zA-Z]:\\\\$", _path, pathname_length, NULL, 0) >= 3)
            || ( cbm::regex_match( "^\\s*[a-zA-Z]:/$", _path, pathname_length, NULL, 0) >= 3)))
    {
        return  true;
    }
    return  false;
}
bool
cbm::path_exists(
        char const *  _path)
{
    if ( !_path || cbm::is_empty( _path))
    {
        return  false;
    }

    /* we are modest ... */
    return  cbm::is_file( _path) || cbm::is_dir( _path);
}

bool
cbm::is_absolute_path(
        char const *  _path)
{
    if ( !_path)
    {
        return  false;
    }

    int const  pathname_length = cbm::strlen( _path);
    if (( pathname_length >= 1)
            && ( cbm::regex_match( "^\\s*/.*$", _path, pathname_length, NULL, 0) >= 1))
    {
        /* have absolute unix path */
    }
    else if (( pathname_length >= 3)
            && (( cbm::regex_match( "^\\s*[a-zA-Z]:\\\\.*$", _path, pathname_length, NULL, 0) >= 3)
            || ( cbm::regex_match( "^\\s*[a-zA-Z]:/", _path, pathname_length, NULL, 0) >= 3)))
    {
        /* have some drive letter */
    }
    else
    {
        return  false;
    }
    return  true;    
}

lerr_t
cbm::mkdir(
        char const *  _path)
{
    if ( !_path)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    if ( cbm::is_empty( _path) || cbm::is_equal( ".", _path) || cbm::is_equal( "./", _path)
            || cbm::is_equal( "..", _path) || cbm::is_equal( "../", _path)
			|| cbm::is_equal( "/", _path) || cbm::is_equal( "\\", _path))
    {
        return  LDNDC_ERR_OK;
    }

    if ( cbm::is_dir( _path))
    {
        return  LDNDC_ERR_OK;
    }
    else if ( cbm::path_exists( _path))
    {
        /* if meanwhile someone created the directory */
        if ( cbm::is_dir( _path))
        {
            return  LDNDC_ERR_OK;
        }
        return  LDNDC_ERR_FAIL;
    }
    else
    {
#if  defined(CRABMEAT_OS_MSWIN)
        int  rc_mkdir = ::_mkdir( _path);
#else
        int  rc_mkdir = ::mkdir( _path, S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH);
#endif
        if ( rc_mkdir)
        {
            /* if meanwhile someone created the directory */
            if ( cbm::is_dir( _path))
            {
                return  LDNDC_ERR_OK;
            }
            perror( "mkdir");
            return  LDNDC_ERR_FAIL;
        }
    }
    return  LDNDC_ERR_OK;
}

#include  "string/lstring-basic.h"
#include  "string/lstring-tokenizer.h"
lerr_t
cbm::mkdirs(
        char const *  _path)
{
    if ( !_path || cbm::is_empty( _path))
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    std::stringstream  this_path;
#ifdef  CRABMEAT_OS_UNIXOID
    if ( is_absolute_path( _path))
    {
        this_path << '/';
    }
#endif

    /* replace backslashes with slashes */
    std::string  this_dirname = _path;
    cbm::replace_all( &this_dirname, "\\", "/");
// sk:dbg    CBM_LogDebug( "dirname=",_path, "  slashed-dirname=",this_dirname);

    cbm::tokenizer  dirs( this_dirname.c_str(), '/', CNAME_STRIP_NONE, NULL);
    for ( size_t  k = 0;  k < dirs.token_cnt();  ++k)
    {
        this_path << dirs[k];
// sk:dbg        CBM_LogDebug( "d[",k,"]=",dirs[k], "  d'=",this_path.str(), "  driveletter=",cbm::is_driveletter( dirs[k]));

        if ( cbm::is_empty( dirs[k]))
        {
            continue;
        }

        std::string  gpath_str = this_path.str();
        if ( cbm::is_dir( gpath_str.c_str()) || cbm::is_driveletter( dirs[k]))
        {
            this_path << "/";
            continue;
        }
        else if ( cbm::path_exists( gpath_str.c_str()))
        {
            fprintf( stderr, "cbm::mkdirs(): "
                    "directory name conflicts with existing non-directory entity  [directory=%s]\n",
                    this_path.str().c_str());
            return  LDNDC_ERR_FAIL;
        }

        lerr_t  rc_mkdir = cbm::mkdir( gpath_str.c_str());
        if ( rc_mkdir)
        {
            fprintf( stderr, "cbm::mkdirs(): "
                    "failed to create directory  [directory=%s,failed-directory=%s]\n",
                    this_path.str().c_str(),gpath_str.c_str());
            return  LDNDC_ERR_FAIL;
        }
        
        this_path << "/";
    }

    return  LDNDC_ERR_OK;
}

lerr_t
cbm::dirname(
        std::string *  _dirname,
        char const *  _path)
{
    if ( !_dirname)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    /* well .. */
    CBM_Assert( _dirname->c_str() != _path);

    if ( !_path || cbm::is_empty( _path) || cbm::is_equal( _path, "."))
    {
        *_dirname = '.';
        return  LDNDC_ERR_OK;
    }

    std::string  this_path = _path;
    cbm::replace_all( &this_path, "\\", "/");

    size_t  dpos = this_path.rfind( "/");
    *_dirname = ( dpos == std::string::npos) ? std::string( ".") : this_path.substr( 0, dpos);
// sk:dbg    CBM_LogDebug( "dirname:  path=",_path, "  dirname=",*_dirname);

    return  LDNDC_ERR_OK;
}

#ifdef  CRABMEAT_OS_UNIXOID
#  include  <unistd.h>
#endif
#include  "memory/cbm_mem.h"
char const *
cbm::getcwd()
{
    char *  cwd = NULL;
    size_t  n_cwd = 64;
    static size_t const  n_cwd_insane = 8192;

    while ( 1)
    {
        cwd = CBM_DefaultAllocator->allocate_type< char >( n_cwd);
        if ( !cwd)
        {
            return  NULL;
        }

        errno = 0;
#if  defined(CRABMEAT_OS_MSWIN)
        char const *  cdir = ::_getcwd( cwd, n_cwd);
#else
        char const *  cdir = ::getcwd( cwd, n_cwd);
#endif
        if ( cdir == NULL && errno == ERANGE)
        {
            CBM_DefaultAllocator->deallocate( cwd);
            n_cwd *= 2;
            if ( n_cwd > n_cwd_insane)
            {
                /* bail out .. something strange is happening */
                fprintf( stderr, "cbm::getcwd(): "
                        "length of current working directly grows insanely large .. bailing out! [size exceeds %u]\n", (unsigned int)n_cwd);
                break;
            }
        }
        else if ( cdir == NULL)
        {
            break;
        }
        else
        {
            return  cwd;
        }
    }

    return  NULL;
}
void
cbm::freecwd(
        char const *  _cwd)
{
    if ( _cwd)
    {
        CBM_DefaultAllocator->deallocate( _cwd);
    }
}

