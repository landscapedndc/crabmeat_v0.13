/*! 
 * @brief
 *      provides callback mechanisms
 *
 * @see
 *      http://www.codeguru.com/cpp/cpp/cpp_mfc/callbacks/article.php/c4129
 *      http://www.tutok.sk/fastgl/callback.html
 *
 * @author 
 *      steffen klatt
 */

#ifndef  LDNDC_CALLBACK_H_
#define  LDNDC_CALLBACK_H_

#include  "crabmeat-common.h"

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"


namespace  ldndc
{

template < typename  _R >
class  abstract_callback_0
{
        public:
        virtual  ~abstract_callback_0() = 0;

                virtual  _R  callback( void) = 0;
};

template < typename  _C, typename  _R >
class  callback_0  :  public  abstract_callback_0< _R >
{
    public:
        typedef  _R  return_type;

        public:
                typedef  _R  (_C::*callback_func)( void);

        public:
                callback_0(
                                _C *  _c_instance,
                                callback_func  _member_func)
                : c_instance_( _c_instance),
                  member_func_( _member_func)
                {
                        CBM_Assert( c_instance_ && member_func_);
                }

        
                _R  callback( void)
                {
                        return  ( c_instance_->*member_func_)();
                }

        private:
                _C *  c_instance_;
                callback_func  member_func_;
};




template < typename  _R, typename  _A >
class  abstract_callback_1
{
        public:
        virtual  ~abstract_callback_1() = 0;

                virtual  _R  callback( _A *) = 0;
};

template < typename  _C, typename  _R, typename  _A >
class  callback_1  :  public  abstract_callback_1< _R, _A >
{
    public:
        typedef  _A  argument_type;
        typedef  _R  return_type;

        public:
                typedef  _R  (_C::*callback_func)( _A *);

        public:
                callback_1(
                                _C *  _c_instance,
                                callback_func  _member_func)
                : c_instance_( _c_instance),
                  member_func_( _member_func)
                {
                        CBM_Assert( c_instance_ && member_func_);
                }

                _R  callback( _A *  _a)
                {
                        return  ( c_instance_->*member_func_)( _a);
                }

        private:
                _C *  c_instance_;
                callback_func  member_func_;
};

#include  "utils/callback.inl"

}  /*  namespace ldndc  */

#endif  /*  !LDNDC_CALLBACK_H  */

