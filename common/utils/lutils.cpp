/*!
 * @author
 *      steffen klatt
 */

#include  "string/lstring-transform.h"
#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"

#include  "containers/lchrll.h"

#include  <limits.h>


namespace cbm
{
        /*!
     * @brief
     *    find first index from left where element matches entry in list and
     *    set @p _index to index in list. if the array @p _c_arr is NULL, try
     *    to convert @p _c_elem to numeric value (no check is performed!) the
     *    search arrays @p _c_arr size @p _n can be omitted (i.e. be invalid)
     *    and a sentinel be used instead, i.e. _c_arr[j] = NULL for some j
     *    smaller than the array size.
     *
     * note:
     *    this function _must_not_ change argument @p _index if the
     *    element @p _c_elem was not found in the array @p _c_arr
     *
     * todo:
     *    offset, stride?
         */
    lerr_t
    find_index_(
            char const *  _c_elem,
            ldndc::charlist_array_base const &  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        /* if no element is given to search for, just pass back _index */
        if ( ! _c_elem)
               {
                        /* *_index = *_index; */
                        return  LDNDC_ERR_OK;
                }

        /* if no source array is given, convert element to index (char to numeric) */
                if ( _c_arr.no_data())
               {
                        cbm::s2n( _c_elem, _index);
                        return  LDNDC_ERR_OK;
                }

        size_t  n(( _n == invalid_size) ? ULONG_MAX : _n);
        /* search position (from left) of element value in array */
                for ( size_t  k = 0;  k < n;  ++k)
               {
            if ( _c_arr[k])
                   {
                if ( cbm::is_equal( _c_elem, _c_arr[k]))
                       {
                    *_index = static_cast< unsigned int >( k);
                    return  LDNDC_ERR_OK;
                }
            }
                   else
                   {
                /* interpreted as sentinel if size is invalid, skipped otherwise */
                if ( _n == invalid_size)
                       {
                    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
                }
            }
        }
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }

    /* char, char version */
    template <>
    lerr_t
    find_index(
            char const *  _c_elem,
            char const * const *  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        return  cbm::find_index_( _c_elem, ldndc::charlist_array< char >( _c_arr), _n, _index);
    }
    /* char, std::string version */
    template < >
    lerr_t
    find_index(
            char const *  _c_elem,
            std::string const *  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        return  cbm::find_index_( _c_elem, ldndc::charlist_array< std::string >( _c_arr), _n, _index);
    }
    /* std::string, char version */
    template < >
    lerr_t
    find_index(
            std::string const *  _c_elem,
            char const * const *  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        return  cbm::find_index_( _c_elem->c_str(), ldndc::charlist_array< char >( _c_arr), _n, _index);
    }
    /* std::string, std::string version */
    template < >
    lerr_t
    find_index(
            std::string const *  _c_elem,
            std::string const *  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        return  cbm::find_index_( _c_elem->c_str(), ldndc::charlist_array< std::string >( _c_arr), _n, _index);
    }
    /* char, ldndc_string_t version */
    template < >
    lerr_t
    find_index(
            char const *  _c_elem,
            ldndc_string_t const *  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        return  cbm::find_index_( static_cast< char const * >( _c_elem), ldndc::charlist_array< ldndc_string_t >( _c_arr), _n, _index);
    }
    /* ldndc_string_t, ldndc_string_t version */
    template < >
    lerr_t
    find_index(
            ldndc_string_t const *  _c_elem,
            ldndc_string_t const *  _c_arr,
            size_t  _n,
            unsigned int *  _index)
    {
        return  cbm::find_index_( static_cast< char const * >( *_c_elem), ldndc::charlist_array< ldndc_string_t >( _c_arr), _n, _index);
    }
    /*
     * same as @fn find_index above, but returns index as function
     * result. if the value was not found, the array size @_n is
     * returned.
     *
     * todo:
     *    offset, stride?
         */
    unsigned int
    find_index(
            char const *  _c_elem,
            char const * const *  _c_arr,
            size_t  _n)
    {
        unsigned int  i((unsigned int)_n);
        cbm::find_index_( _c_elem, ldndc::charlist_array< char >( _c_arr), _n, &i);
        return  i;
    }

    unsigned int
    find_index(
            char const *  _c_elem,
            char const * const *  _c_arr)
    {
        return  cbm::find_index( _c_elem, _c_arr, ldndc::invalid_t< size_t >::value);
    }

}  /*  namespace cbm  */

