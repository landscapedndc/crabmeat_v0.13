/*!
 * @brief
 *    provides various helper methods
 *
 * @author
 *    steffen klatt
 */

#ifndef  CBM_UTILS_H__
#define  CBM_UTILS_H__

#include  "crabmeat-common.h"

#include  "math/lmath-float.h"

#include  "string/lstring-basic.h"
#include  "string/lstring-compare.h"
#include  <cstdarg>

namespace cbm
{
    template < typename _T >
    bool
    is_invalid(
            _T const &  _val)
    {
        return  _val == ldndc::invalid_t< _T >::value;
    }

    template < >
    CBM_API inline bool
    is_invalid< bool >(
            bool const &)
    {
        return  false;
    }
    template < >
    CBM_API inline bool
    is_invalid< float >(
            float const &  _val)
    {
        return  cbm::flt_equal_eps( ldndc::invalid_t< float >::value, _val, (float)0.001);
    }
    template < >
    CBM_API inline bool
    is_invalid< double >(
            double const &  _val)
    {
        return  cbm::flt_equal_eps( ldndc::invalid_t< double >::value, _val, (double)0.0001);
    }
    template < >
    CBM_API inline bool
    is_invalid< std::string >(
            std::string const &  _str)
           {
        return  cbm::is_equal( cbm::to_upper( _str), invalid_str);
    }
    template < >
    CBM_API inline bool
    is_invalid< char const * >(
            char const * const &  _chrl)
    {
        return  !_chrl || is_invalid< std::string >( std::string( _chrl));
    }

    template < typename _T >
    bool
    is_valid(
            _T const &  _val)
    {
        return  ! is_invalid( _val);
    }

    template < typename  _T >
    void
    invalidate(
            _T &  _reg)
    {
        _reg = ldndc::invalid_t< _T >::value;
    }
    template < >
    CBM_API inline void
    invalidate< bool >(
            bool &  _reg)
    {
        _reg = false;
    }


    /*!
     * @brief
     *    fill memory region by setting each element
     *    to the given value. the value defaults to zero
     *
     * @param
     *    pointer to memory region (destination)
     * @param
     *    number of elements
     * @param
     *    fill value (defaults to zero)
     */
    template < typename _T, typename _S >
        void mem_set( _T *  _dst, _S  _n,
                    _T const &  _ini_val = _T( 0))
            { std::fill_n( _dst, _n, _ini_val); }

    /*!
     * @brief
     *    wrapper around memcpy multiplying the size of
     *    the datatype to the element number implicitely
     */
    template < typename _T >
        _T * mem_cpy( _T *  _dst, _T const *  _src, size_t  _n)
        {
            if ( _dst == _src)
                { return  _dst; }
            return  static_cast< _T * >( memcpy( (void*)_dst,
                        (void const *)_src, _n * sizeof( _T)));
        }

    /*!
     * @brief
     *    mem_cpy equivalent for non-POD types
     */
    template < typename _T >
        _T * obj_cpy( _T *  _dst, _T const *  _src, size_t  _n)
        {
            if ( _dst == _src)
                { return  _dst; }

            _T * const  dst = _dst;
            for ( size_t  k = 0;  k < _n;  ++k, ++_dst, ++_src)
                { *_dst = *_src; }
            return  dst;
        }

    /*!
     * @brief
     *    copy memory area from src of size n to
     *    newly allocated buffer and return pointer
     *    to new buffer.
     *
     * @return
     *    returns a pointer to a new block of memory
     *    with the same content as the original. the
     *    user needs to release the memory with free().
     */
    template < typename _T >
        _T * mem_dup( _T const *  _src, size_t  _n)
        {
            _T *  buf = (_T *)malloc( sizeof( _T)*_n);
            if ( buf)
                { cbm::mem_cpy( buf, _src, _n); }
            return  buf;
        }


    template < typename _T >
        void init_array( _T *  _arr, size_t  _size,
            _T const &  _val, size_t  _offset = 0, size_t  _stride = 1)
        {
#ifdef  CRABMEAT_SANITY_CHECKS
            if ( !_arr)
                { return; }
#endif
            for ( size_t  i = _offset;  i < _size;  i+=_stride)
                { _arr[i] = _val; }
        }


    /*!
     * @brief
     *    initializes arbitrary number of equally sized
     *    arrays of identical type
     *
     * @param  _init_val
     *    default value to set for all elements
     * @param  _size
     *    size of arrays (same for all!)
     * @param  _a_cnt
     *    number of arrays given to this function (careful!!!)
     * @param  ...
     *    pointer to arrays of same type and size
     */
    template < typename _T >
        void init_equi_sized_arrays( _T const &  _init_val,
            size_t  _size, size_t  _a_cnt, ...)
        {
            va_list  arrays;
            va_start( arrays, _a_cnt);

            for ( size_t  a = 0;  a < _a_cnt;  ++a)
            {
                _T *  arr = va_arg( arrays, _T *);
                cbm::init_array( arr, _size, _init_val);
            }
            va_end( arrays);
        }


    /* given a char string, find it's index in the char list */
    template < typename _U, typename _V >
    lerr_t
    find_index(
            _U const *,
            _V const *,
            size_t,
            unsigned int *);

    /* char, char version */
    template <>
    CBM_API lerr_t
    find_index(
            char const *,
            char const * const *,
            size_t,
            unsigned int *);
    /* char, std::string version */
    template < >
    CBM_API lerr_t
    find_index(
            char const *,
            std::string const *,
            size_t,
            unsigned int *);
    /* std::string, char version */
    template < >
    CBM_API lerr_t
    find_index(
            std::string const *,
            char const * const *,
            size_t,
            unsigned int *);
    /* std::string, std::string version */
    template < >
    CBM_API lerr_t
    find_index(
            std::string const *,
            std::string const *,
            size_t,
            unsigned int *);
    /* char, ldndc_string_t version */
    template < >
    CBM_API lerr_t
    find_index(
            char const *,
            ldndc_string_t const *,
            size_t,
            unsigned int *);
    /* ldndc_string_t, ldndc_string_t version */
    template < >
    CBM_API lerr_t
    find_index(
            ldndc_string_t const *,
            ldndc_string_t const *,
            size_t,
            unsigned int *);

    /*!
     * @brief
     *    version of find_index returning the result
     *    as return value
     *
     * @param
     *    search key
     * @param
     *    list to search
     * @param
     *    length of list to search
     *
     * @returns
     *    index of key in list
     */
    CBM_API unsigned int
    find_index(
            char const *,
            char const * const *,
            size_t);

    /*!
     * @brief
     *    same as above but with sentinel (list item NULL
     *    terminates search)
     *
     * @param
     *    search key
     * @param
     *    list to search
     *
     * @returns
     *    index of key in list
     */
    CBM_API unsigned int
    find_index(
            char const *,
            char const * const *);


    template < typename  _E >
    lerr_t
    find_enum(
            char const *  _s,
            char const * const *  _l,
            size_t  _l_size,
            _E *  _e_val)
    {
        unsigned int  pos( ldndc::invalid_t< unsigned int >::value);

        lerr_t  rc = find_index( _s, _l, _l_size, &pos);

        if ( pos == ldndc::invalid_t< unsigned int >::value)
        {
            return  rc;
        }
        *_e_val = (_E)pos;

        return  LDNDC_ERR_OK;
    }

    template < typename  _E >
    lerr_t
    find_exp_enum(
            char const *  _s,
            char const * const *  _l,
            size_t  _l_size,
            _E *  _e_val)
    {
        unsigned int  pos( ldndc::invalid_t< unsigned int >::value);

        lerr_t  rc = find_index( _s, _l, _l_size, &pos);

        if ( pos == ldndc::invalid_t< unsigned int >::value)
        {
            return  rc;
        }
        *_e_val = (_E)(((unsigned int)1 << pos) >> 1);

        return  LDNDC_ERR_OK;
    }

    template < typename _T >
        void clear( _T *  _mem, size_t  _size)
            { cbm::mem_set( _mem, _size, _T(0)); }
} /* namespace cbm */


#endif  /*  !CBM_UTILS_H__  */

