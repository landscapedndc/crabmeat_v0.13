
#define  __lunpack_array_type_s(__datatype__)  TOKENPASTE3(unpack_array_,__datatype__,_s)
#define  __lunpack_type(__datatype__)  TOKENPASTE3(ldndc_,__datatype__,_t)

#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
#ifndef  __MUNPACK_TYPE
#  define  __MUNPACK_TYPE  __LUNPACK_TYPE
#endif
static
lerr_t
__lunpack_array_type_s(__LUNPACK_TYPE)(
                char const *  _buf, size_t  _size,
                __lunpack_type(__LUNPACK_TYPE) *  _array, int  _array_rank, int *  _extents)
{
        if ( !_buf || _size == 0 || !_array || !_extents || _array_rank == 0)
        {
                return  LDNDC_ERR_RUNTIME_ERROR;
        }

        int  flat_length = _extents[0];
        for ( int  r = 1;  r < _array_rank;  ++r)
        {
                flat_length *= _extents[r];
        }

        lerr_t  rc_unpack = LDNDC_ERR_OK;

        msgpack_unpacked  msg;
        msgpack_unpacked_init( &msg);

        if ( msgpack_unpack_next( &msg, _buf, _size, NULL))
        {
                msgpack_object  root = msg.data;
                if ( root.type == MSGPACK_OBJECT_ARRAY)
                {
                        int  length = root.via.array.size;
                        //fprintf( stderr, "[DD]  array size = %d (%d)\n", length,  flat_length);
                        if ( length != flat_length)
                        {
                                fprintf( stderr, "[EE]  array sizes do not match  [found=%d,requested=%d]\n", length,  flat_length);
                                rc_unpack = LDNDC_ERR_FAIL;
                        }
                        else for ( int  l = 0; l < length;  ++l)
                        {
                                _array[l] = root.via.array.ptr[l].via.__MUNPACK_TYPE;
                        }
                }
        }

        msgpack_unpacked_destroy( &msg);

        return  rc_unpack;
}
#else
static
lerr_t
__lunpack_array_type_s(__LUNPACK_TYPE)(
                char const *, size_t,
                __lunpack_type(__LUNPACK_TYPE) *, int, int *)
{
    return  LDNDC_ERR_RUNTIME_ERROR;
}
#endif  /*  CRABMEAT_OUTPUTS_SERIALIZE  */

#undef  __lunpack_type
#undef  __lunpack_array_type_s

#ifdef  __LUNPACK_TYPE
#  undef  __LUNPACK_TYPE
#endif
#ifdef  __MUNPACK_TYPE
#  undef  __MUNPACK_TYPE
#endif

