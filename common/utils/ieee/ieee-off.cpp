/*!
 * @brief
 *    provide dummy functions for ieee fpe interface
 *
 * @author
 *    steffen klatt (created on: aug 28, 2012)
 */

#include  "utils/ieee/cbm_ieee.h"


fpu_ctrl_word_t
ldndc_ieee_get_mode()
{
    return  (fpu_ctrl_word_t)0;
}

lerr_t
ldndc_ieee_set_mode( fpu_ctrl_word_t)
{
    return  LDNDC_ERR_OK;
}

int
ldndc_ieee_get_excepts( int)
{
    return  0;
}

lerr_t
ldndc_ieee_clear_excepts( int)
{
    return  LDNDC_ERR_OK;
}

