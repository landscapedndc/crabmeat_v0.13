/*!
 * @brief
 *    set floating point exception trap mask interface
 *
 * @author
 *    gsl,
 *    steffen klatt (created on: aug 28, 2012)
 */

#include  "utils/ieee/cbm_ieee.h"

/* taken from GSL 1.13 */

#if defined(CRABMEAT_IEEE_INTERFACE_GNUC99)
#  include  "utils/ieee/ieee-gnuc99.cpp"
#elif  defined(CRABMEAT_IEEE_INTERFACE_GNUX86)
#  include  "utils/ieee/ieee-gnux86.cpp"
#elif  defined(CRABMEAT_IEEE_INTERFACE_DARWIN)
#  include  "utils/ieee/ieee-darwin.cpp"
#elif  defined(CRABMEAT_IEEE_INTERFACE_DARWIN86)
#  include  "utils/ieee/ieee-darwin86.cpp"
#elif  defined(CRABMEAT_IEEE_INTERFACE_MSWIN)
#  include  "utils/ieee/ieee-mswin.cpp"
#else
#  include  "utils/ieee/ieee-off.cpp"
#endif


