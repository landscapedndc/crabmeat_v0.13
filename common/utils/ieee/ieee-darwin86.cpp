/*!
 * @brief
 *    set floating point exception trap mask on
 *    intel-mac machines
 *
 * @author
 *    gsl,
 *    steffen klatt (created on: aug 28, 2012)
 */

/* ieee-utils/fp-darwin86.c (GSL - gnu scientific library)
 * 
 * Copyright (C) 2006 Erik Schnetter
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */


#include  "utils/ieee/cbm_ieee.h"

#include  <math.h>
#include  <fenv.h>

/* Macros for accessing the hardware control word.

   Note that the use of these macros is no sufficient anymore with
   recent hardware.  Some floating point operations are executed in
   the SSE/SSE2 engines which have their own control and status register.  */
#define _FPU_GETCW(cw) __asm__ __volatile__ ("fnstcw %0" : "=m" (*&cw))
#define _FPU_SETCW(cw) __asm__ __volatile__ ("fldcw %0" : : "m" (*&cw))

#define _FPU_GETMXCSR(cw_sse) asm volatile ("stmxcsr %0" : "=m" (cw_sse))
#define _FPU_SETMXCSR(cw_sse) asm volatile ("ldmxcsr %0" : : "m" (cw_sse))


/* masking of interrupts */
#define _FPU_MASK_IM  0x01
#define _FPU_MASK_DM  0x02
#define _FPU_MASK_ZM  0x04
#define _FPU_MASK_OM  0x08
#define _FPU_MASK_UM  0x10
#define _FPU_MASK_PM  0x20

#define _FPU_RESERVED 0xF0C0  /* Reserved bits in cw */

fpu_ctrl_word_t
ldndc_ieee_get_mode()
{
    fpu_ctrl_word_t  fpu_cw;
    _FPU_GETCW( fpu_cw);

    return  fpu_cw;
}

#define  SET_FE_FLAG(__lflag__,__sflag__)        \
        if ( _except_flags & __lflag__)            \
        {                        \
                mode &= ~(__sflag__);            \
        }                        \
        else                        \
        {                        \
                mode |= __sflag__;            \
        }
#define  SET_FE_FLAG_SSE(__lflag__,__sflag__)        \
        if ( _except_flags & __lflag__)            \
        {                        \
                mode_sse &= ~((__sflag__) << 7);    \
        }                        \
        else                        \
        {                        \
                mode_sse |= (__sflag__) << 7;        \
        }

lerr_t
ldndc_ieee_set_mode( fpu_ctrl_word_t  _except_flags)
{
    /* FPU version */
    fpu_ctrl_word_t  mode( 0);
    _FPU_GETCW( mode);
    mode &= _FPU_RESERVED;

    SET_FE_FLAG(LDNDC_FE_INVALID,_FPU_MASK_IM);
    SET_FE_FLAG(LDNDC_FE_DENORMALIZED,_FPU_MASK_DM);
    SET_FE_FLAG(LDNDC_FE_DIVBYZERO,_FPU_MASK_ZM);
    SET_FE_FLAG(LDNDC_FE_OVERFLOW,_FPU_MASK_OM);
    SET_FE_FLAG(LDNDC_FE_UNDERFLOW,_FPU_MASK_UM);
    /* ignore inexact */
    mode |= _FPU_MASK_PM;

    /* set mode */
    _FPU_SETCW( mode);


    /* SSE version */
    fpu_ctrl_word_t  mode_sse( 0);
    _FPU_GETMXCSR( mode_sse);
    mode_sse &= 0xFFFF0000;

    SET_FE_FLAG_SSE(LDNDC_FE_INVALID,_FPU_MASK_IM);
    SET_FE_FLAG_SSE(LDNDC_FE_DENORMALIZED,_FPU_MASK_DM);
    SET_FE_FLAG_SSE(LDNDC_FE_DIVBYZERO,_FPU_MASK_ZM);
    SET_FE_FLAG_SSE(LDNDC_FE_OVERFLOW,_FPU_MASK_OM);
    SET_FE_FLAG_SSE(LDNDC_FE_UNDERFLOW,_FPU_MASK_UM);
    /* ignore inexact */
    mode_sse |= (fpu_ctrl_word_t)_FPU_MASK_PM << 7;

    /* set mode */
    _FPU_SETMXCSR( mode_sse);


    return  LDNDC_ERR_OK;
}


lerr_t
ldndc_ieee_clear_excepts( int  /*_excepts*/)
{
    /* FIXME  not implemented */
    return  LDNDC_ERR_FAIL;
}

