/*!
 * @brief
 *    set floating point exception trap mask on
 *    i386 machines running ms windows
 *
 * @author
 *    steffen klatt (created on: aug 28, 2012)
 */

#include  "utils/ieee/cbm_ieee.h"
#include  <float.h>

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"

//unsigned long cntrReg;
//_asm
//{
//     stmxcsr [cntrReg]        //Get MXCSR register
//     and [cntrReg], 0FFFFFF7Fh//bit 7  - invalid instruction mask
//                              //bit 9  - divide-by-zero mask
//                              //bit 10 - overflow mask
//                              //bit 11 - underflow mask     
//     ldmxcsr [cntrReg]        //Load MXCSR register
//}

lerr_t
ldndc_ieee_set_mode( fpu_ctrl_word_t  _except_mode)
{
    unsigned int  rc_cfp = _controlfp_s(
            NULL,
                   ~((unsigned int)(
                      (( _except_mode & LDNDC_FE_OVERFLOW) ? EM_OVERFLOW : 0)
                    | (( _except_mode & LDNDC_FE_UNDERFLOW) ? EM_UNDERFLOW : 0)
                    | (( _except_mode & LDNDC_FE_DIVBYZERO) ? EM_ZERODIVIDE : 0)
                    | (( _except_mode & LDNDC_FE_INVALID) ? EM_INVALID : 0))
            ),
            MCW_EM);
    if ( rc_cfp != 0)
    {
        CBM_LogError( "failed to set float control mask");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


fpu_ctrl_word_t
ldndc_ieee_get_mode()
{
    CBM_LogFatal( "implement me");
    return  0;

}


lerr_t
ldndc_ieee_clear_excepts( int  /*_excepts*/)
{
    /* FIXME  not implemented */
    return  LDNDC_ERR_FAIL;
}

