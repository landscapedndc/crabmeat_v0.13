/*!
 * @brief
 *    set floating point exception trap mask on
 *    i386 machines supporting gnu c99
 *
 * @author
 *    steffen klatt (created on: aug 28, 2012)
 */

#define _GNU_SOURCE  1
#define __USE_GNU  1

#include  "utils/ieee/cbm_ieee.h"

#include  <fenv.h>

#define  TRANSL_FE_FLAG(__mode__)  (((LDNDC_ ## __mode__) & _except_flags) ? (__mode__) : 0)
#define  RETRANSL_FE_FLAG(__mode__)  (((__mode__) & _except_flags) ? (LDNDC_ ## __mode__) : 0)


fpu_ctrl_word_t
ldndc_ieee_get_mode()
{
    fpu_ctrl_word_t  _except_flags = fegetexcept();

    return  RETRANSL_FE_FLAG(FE_INVALID)|RETRANSL_FE_FLAG(FE_DIVBYZERO)|RETRANSL_FE_FLAG(FE_OVERFLOW)|RETRANSL_FE_FLAG(FE_UNDERFLOW);
}


lerr_t
ldndc_ieee_set_mode( fpu_ctrl_word_t  _except_flags)
{
    fpu_ctrl_word_t  new_mode = TRANSL_FE_FLAG(FE_INVALID)|TRANSL_FE_FLAG(FE_DIVBYZERO)|TRANSL_FE_FLAG(FE_OVERFLOW)|TRANSL_FE_FLAG(FE_UNDERFLOW);
#ifdef  _HAVE_FUNC_FEENABLEEXCEPT
    fpu_ctrl_word_t  ret = feenableexcept( new_mode);
#elif  _HAVE_FUNC_FESETTRAPENABLE
    fpu_ctrl_word_t  ret = fesettrapenable( new_mode);
#else
#  error  "you should not get here..."
#endif

    if ( ret == -1)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


#include "log/cbm_baselog.h"
int
ldndc_ieee_get_excepts( int  _except_flags)
{
    int  lib_except_flags = TRANSL_FE_FLAG(FE_INVALID)|TRANSL_FE_FLAG(FE_DIVBYZERO)|TRANSL_FE_FLAG(FE_OVERFLOW)|TRANSL_FE_FLAG(FE_UNDERFLOW);
    fetestexcept( lib_except_flags);
    return  RETRANSL_FE_FLAG(FE_INVALID)|RETRANSL_FE_FLAG(FE_DIVBYZERO)|RETRANSL_FE_FLAG(FE_OVERFLOW)|RETRANSL_FE_FLAG(FE_UNDERFLOW);
}

lerr_t
ldndc_ieee_clear_excepts( int  _except_flags)
{
//#ifdef  _HAVE_FUNC_FECLEAREXCEPT
    int  lib_except_flags = TRANSL_FE_FLAG(FE_INVALID)|TRANSL_FE_FLAG(FE_DIVBYZERO)|TRANSL_FE_FLAG(FE_OVERFLOW)|TRANSL_FE_FLAG(FE_UNDERFLOW);
// sk:dbg    LOGDEBUG2( "exc=", _except_flags, "  exc-os=", lib_except_flags);
// sk:dbg    CBM_LogFatal( "ieee-gnu99");
    int  ret = feclearexcept( lib_except_flags);
//#else
//#  error  "huh? don't know how to clear floating pointing exception flags :("
//#endif
    if ( ret == -1)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

