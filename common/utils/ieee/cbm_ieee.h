/*!
 * @brief
 *    set floating point exception trap mask interface
 *
 * @author
 *    gsl,
 *    steffen klatt (created on: aug 28, 2012)
 */

#ifndef  LDNDC_IEEE_INTERFACE_H_
#define  LDNDC_IEEE_INTERFACE_H_

#include  "crabmeat-common.h"

#ifdef  CRABMEAT_FPE_EXCEPTS
   /* mac os x */
#  if  defined(CRABMEAT_OS_MACOSX)
#    if  defined(CRABMEAT_ARCH_x86)
#      define  CRABMEAT_IEEE_INTERFACE_DARWIN86
#    elif  defined(CRABMEAT_ARCH_ppc)
#      define  CRABMEAT_IEEE_INTERFACE_DARWIN
#    else
#      error  "unknown ieee754 interface for apple"
#    endif

   /* ms windows */
#  elif  defined(CRABMEAT_OS_MSWIN)
#    define  CRABMEAT_IEEE_INTERFACE_MSWIN

   /* all remaining unixoids */
#  elif  defined(CRABMEAT_OS_UNIXOID)
#    ifdef  CRABMEAT_HAVE_FENV_H
#      if  defined(_HAVE_FUNC_FEENABLEEXCEPT) || defined(_HAVE_FUNC_FESETTRAPENABLE)
#        define  CRABMEAT_IEEE_INTERFACE_GNUC99
#      else
#        define  CRABMEAT_IEEE_INTERFACE_GNUX86
#      endif
#    endif

   /* ? */
#  else
     /* switch off support for ieee fpe exceptions */
#    error "unknown ieee754 interface"
#  endif
#endif  /*  CRABMEAT_FPE_EXCEPTS  */


#if defined(CRABMEAT_IEEE_INTERFACE_GNUC99)
typedef  int  fpu_ctrl_word_t;
#elif  defined(CRABMEAT_IEEE_INTERFACE_GNUX86)
typedef  int  fpu_ctrl_word_t;
#elif  defined(CRABMEAT_IEEE_INTERFACE_DARWIN)
typedef  unsigned int  fpu_ctrl_word_t;
#elif  defined(CRABMEAT_IEEE_INTERFACE_DARWIN86)
typedef  unsigned int  fpu_ctrl_word_t;
#elif  defined(CRABMEAT_IEEE_INTERFACE_MSWIN)
typedef  int  fpu_ctrl_word_t;
#else
typedef  int  fpu_ctrl_word_t;
#endif

enum
{
        LDNDC_FE_INVALID        = 0x01,
        LDNDC_FE_DIVBYZERO      = 0x02,
        LDNDC_FE_OVERFLOW       = 0x04,
        LDNDC_FE_UNDERFLOW      = 0x08,
        LDNDC_FE_DENORMALIZED   = 0x10,

        LDNDC_FE_ALL            = LDNDC_FE_INVALID|LDNDC_FE_DIVBYZERO|LDNDC_FE_OVERFLOW|LDNDC_FE_UNDERFLOW|LDNDC_FE_DENORMALIZED
};

extern  fpu_ctrl_word_t CBM_API ldndc_ieee_get_mode();

extern  lerr_t CBM_API ldndc_ieee_set_mode( fpu_ctrl_word_t);

//extern  lerr_t CBM_API ldndc_ieee_merge_mode( int);

extern  int CBM_API ldndc_ieee_get_excepts( int);
extern  lerr_t CBM_API ldndc_ieee_clear_excepts( int);

#endif  /*  !LDNDC_IEEE_INTERFACE_H_  */

