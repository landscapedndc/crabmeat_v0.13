/*!
 * @brief
 *    environment and user related functions  (implementation)
 *
 * @author
 *    steffen klatt (created on: may 21, 2013),
 *    edwin haas
 */

#include  "crabmeat-config.h"
#if defined(CRABMEAT_MINGW) && defined(__STRICT_ANSI__)
#  undef  __STRICT_ANSI__
#endif
#include  <stdio.h>
#include  <stdlib.h>

#include  "crabmeat-common.h"
#include  "utils/lutils-env.h"
#include  "string/lstring-compare.h"

char const *
cbm::getenv(
        char const *  _env_var)
{
    return  ::getenv( _env_var);
}

int
cbm::setenv(
        const char *  _env_var,
        const char *  _value,
        int  _overwrite)
{
    if ( _overwrite && cbm::getenv( _env_var))
    {
        cbm::unsetenv( _env_var);
    }

    int  rc_set = 0;
#if  defined(CRABMEAT_COMPILER_MSC)
    CRABMEAT_FIX_UNUSED(_overwrite);
    rc_set = ::_putenv_s( _env_var, _value);
    return  rc_set ? -1 : 0;
#elif  defined(CRABMEAT_MINGW)
    char *  env_entry =
        (char *)malloc( cbm::strlen( _env_var) + cbm::strlen( _value) + 1 + 1);
    if ( !env_entry)
    {
        return  -1;
    }
    sprintf( env_entry, "%s=%s", _env_var, _value);
    rc_set = ::putenv( env_entry);
    if ( rc_set)
    {
        free( env_entry);
    }
    return  rc_set ? -1 : 0;
#else
    rc_set = ::setenv( _env_var, _value, _overwrite);
    return  rc_set ? -1 : 0;
#endif
}
int
cbm::unsetenv(
        const char * _env_var)
{
    int  rc_unset = 0;
#if  defined(CRABMEAT_COMPILER_MSC)
    rc_unset = cbm::setenv( _env_var, "", 1);
    return  rc_unset ? -1 : 0;
#elif  defined(CRABMEAT_MINGW)
    char *  env_entry =
        (char *)malloc( cbm::strlen( _env_var) + 1 + 1);
    if ( !env_entry)
    {
        return  -1;
    }
    sprintf( env_entry, "%s=", _env_var);
    rc_unset = ::putenv( env_entry);
    free( env_entry);
    return  rc_unset ? -1 : 0;
#else
    rc_unset = ::unsetenv( _env_var);
    return  rc_unset ? -1 : 0;
#endif
}

/* portable environment variable query */
static
std::string
home_directory_environment_()
{
    char const *  homedir = cbm::getenv( "HOME");
    if ( homedir)
    {
        return  homedir;
    }
    return  "?";
}

#if  defined(CRABMEAT_OS_MSWIN)
#  include  <windows.h>
#  include  <shlobj.h>
static
std::string
home_directory_windows_()
{
    char  path[MAX_PATH];
    if ( SHGetFolderPathA( NULL, CSIDL_PROFILE, NULL, 0, path) == S_OK)
    {
        return  std::string( path);
    }
    /* fallback */
    return  home_directory_environment_();
}
#endif
#if  defined(CRABMEAT_OS_UNIXOID)
#  include  <unistd.h>
#  include  <sys/types.h>
#  include  <pwd.h>
static
std::string
home_directory_unixoid_()
{
    struct passwd *  pw = getpwuid( getuid());
    if ( pw && pw->pw_dir)
    {
        return  pw->pw_dir;
    }
    /* fallback */
    return  home_directory_environment_();
}
#endif
std::string
cbm::home_directory()
{
#if  defined(CRABMEAT_OS_MSWIN)
    return  home_directory_windows_();
#elif  defined(CRABMEAT_OS_UNIXOID)
    return  home_directory_unixoid_();
#else
    /* try HOME environment variable */
    return  home_directory_environment_();
#endif
}

bool
cbm::home_directory_found()
{
    return  ! ( cbm::is_empty( cbm::home_directory()) || cbm::is_equal( cbm::home_directory(), "?"));
}


#if  defined(_MSC_VER)
//#  include  <Winsock2.h>
#elif  defined(CRABMEAT_HAVE_UNISTD_H)
#  include  <unistd.h>
#endif
#ifdef  HOST_NAME_MAX
#  define  LDNDC_HOST_NAME_MAX  (HOST_NAME_MAX+1)
#else
#  define  LDNDC_HOST_NAME_MAX  256
#endif
std::string
cbm::hostname()
{
#if  defined(_HAVE_FUNC_GETHOSTNAME)
    char  m_hostname[LDNDC_HOST_NAME_MAX];
    int const  rc_hostname =
        ::gethostname( m_hostname, LDNDC_HOST_NAME_MAX);
    if ( rc_hostname == 0)
    {
        return  m_hostname;
    }
#else /* unreliable fallback */
    char const *  m_hostname = cbm::getenv( "HOSTNAME");
    if ( m_hostname)
    {
        return  m_hostname;
    }
#endif
    return  "UNKNOWN_HOSTNAME";
}

#if  defined(_MSC_VER)
//#  include  <Winbase.h>
#elif  defined(CRABMEAT_HAVE_UNISTD_H)
#  include  <unistd.h>
#else
#endif
std::string
cbm::username()
{
#if  defined(_MSC_VER) || defined(CRABMEAT_MINGW)
    /* not implemented */
#elif  defined(_HAVE_FUNC_GETLOGIN)
    return  ::getlogin();
#else
    char const *  m_logname = cbm::getenv( "LOGNAME");
    if ( !m_logname)
    {
        m_logname = cbm::getenv( "USERNAME");
    }
    if ( m_logname)
    {
        return  m_logname;
    }
#endif
    return  "UNKNOWN_USERNAME";
}

#ifdef  CRABMEAT_MINGW
#  include  <process.h>
#endif
pid_t
cbm::get_pid()
{
#if (defined(_MSC_VER)  &&  (_MSC_VER >= 1400)) || defined(CRABMEAT_MINGW)
    return  ::_getpid();
#else
    return  ::getpid();
#endif
}

pid_t
cbm::get_parent_pid()
{
#if  (defined(_MSC_VER)  &&  (_MSC_VER >= 1400)) || defined(CRABMEAT_MINGW)
    return  -1;
#else
    return  ::getppid();
#endif
}

#include  <time.h>
void
cbm::tzset()
{
#if  defined(CRABMEAT_COMPILER_MSC) || defined(CRABMEAT_MINGW)
    ::_tzset();
#else
    ::tzset();
#endif
}

/* see https://stackoverflow.com/questions/6812224/getting-terminal-size-in-c-for-windows */
#if  defined(CRABMEAT_OS_UNIXOID)
#include  <sys/ioctl.h>
#include  <stdio.h>
#include  <unistd.h>
int cbm::terminal_extents( int * _rows, int * _columns)
{
    struct winsize  extents;
    int  rc = ioctl( STDOUT_FILENO, TIOCGWINSZ, &extents);
    if ( rc != -1)
    {
        if ( _rows)
            { *_rows = extents.ws_row; }
        if ( _columns)
            { *_columns = extents.ws_col; }
    }
    return rc;
}
#elif defined(CRABMEAT_OS_MSWIN)
#include <windows.h>
int cbm::terminal_extents( int * _rows, int * _columns)
{
    CONSOLE_SCREEN_BUFFER_INFO  sb;

    int  rc = -1;
    if ( GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE), &sb))
    {
        if ( _rows)
            { *_rows = sb.srWindow.Bottom - sb.srWindow.Top + 1; }
        if ( _columns)
            { *_columns = sb.srWindow.Right - sb.srWindow.Left + 1; }
        rc = 0;
    }
    return rc;
}
#else
#  error no implementation for cbm::terminal_extents
#endif

