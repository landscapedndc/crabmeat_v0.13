/*!
 * @brief
 *    interface for serializing/deserializing data
 *
 * @author
 *    steffen klatt (created on: nov 27, 2014)
 */

#ifndef  CBM_UTILS_SERIALZE_H_
#define  CBM_UTILS_SERIALZE_H_

#include  "crabmeat-common.h"
#include  "cbm_types.h"

namespace cbm
{
    lerr_t CBM_API pack(
            char ** /*packed data*/, size_t * /*packed data size*/,
            void const * /*array*/, int /*array rank*/, int * /*array extents*/, atomic_datatype_t);

    lerr_t CBM_API pack64(
            char ** /*packed data*/, size_t * /*packed data size*/,
            void const * /*array*/, int /*array rank*/, int * /*array extents*/, atomic_datatype_t);

    lerr_t CBM_API unpack(
            char const * /*packed data*/, size_t /*packed data size*/,
            void * /*array*/, int /*array rank*/, int * /*array extents*/, atomic_datatype_t);

    lerr_t CBM_API unpack64(
            char const * /*packed data*/, size_t /*packed data size*/,
            void * /*array*/, int /*array rank*/, int * /*array extents*/, atomic_datatype_t);

    void CBM_API free_pack_buffer( char * /*packed data*/);
    void CBM_API free_pack64_buffer( char * /*packed data*/);
}

#endif  /*  !CBM_UTILS_SERIALZE_H_  */

