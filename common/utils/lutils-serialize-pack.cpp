
#define  __lpack_array_type_s(__datatype__)  TOKENPASTE3(pack_array_,__datatype__,_s)
#define  __lpack_type(__datatype__)  TOKENPASTE3(ldndc_,__datatype__,_t)

#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
#ifndef  __MPACK_TYPE
#  define  __MPACK_TYPE  __LPACK_TYPE
#endif
#define  __lpack_fun(__datatype__)  TOKENPASTE(msgpack_pack_,__datatype__)
static
lerr_t
__lpack_array_type_s(__LPACK_TYPE)(
                char **  _buf, size_t *  _size,
                __lpack_type(__LPACK_TYPE) const *  _array, int  _array_rank, int *  _extents)
{
        if ( !_buf || !_size || !_array || !_extents || _array_rank == 0)
        {
                return  LDNDC_ERR_RUNTIME_ERROR;
        }

        int  flat_length = _extents[0];
        for ( int  r = 1;  r < _array_rank;  ++r)
        {
                flat_length *= _extents[r];
        }

        msgpack_sbuffer  sbuf;
        msgpack_sbuffer_init( &sbuf);

        msgpack_packer  pck;
        msgpack_packer_init( &pck, &sbuf, msgpack_sbuffer_write);
        msgpack_pack_array( &pck, flat_length);
        __lpack_type(__LPACK_TYPE) const *  array = _array;
    __lpack_type(__LPACK_TYPE) const *  array_end = _array+flat_length;
    while ( array != array_end)
        {
                __lpack_fun(__MPACK_TYPE)( &pck, *array);
        ++array;
        }

        *_size = sbuf.size;
        *_buf = (char *)malloc( sbuf.size);
        memcpy( *_buf, sbuf.data, sbuf.size);
        msgpack_sbuffer_destroy( &sbuf);

        return  LDNDC_ERR_OK;
}
#undef  __lpack_fun
#else
static
lerr_t
__lpack_array_type_s(__LPACK_TYPE)(
                char **, size_t *,
                __lpack_type(__LPACK_TYPE) const *, int, int *)
{
    return  LDNDC_ERR_RUNTIME_ERROR;
}
#endif  /*  CRABMEAT_OUTPUTS_SERIALIZE  */

#undef  __lpack_type
#undef  __lpack_array_type_s

#ifdef  __LPACK_TYPE
#  undef  __LPACK_TYPE
#endif
#ifdef  __MPACK_TYPE
#  undef  __MPACK_TYPE
#endif

