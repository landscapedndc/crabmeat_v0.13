/*!
 * @brief
 *    some helper function concerning network stuff
 *    (implementation)
 *
 * @author 
 *    steffen klatt (created on: mar. 25, 2015)
 */

#include  "utils/lutils-net.h"
#include  "string/lstring-compare.h"

#ifdef  CRABMEAT_OS_UNIXOID
#  include  <sys/types.h>
#  include  <ifaddrs.h>
#  include  <netinet/in.h> 
#  include  <arpa/inet.h>
#endif  /*  CRABMEAT_OS_UNIXOID  */


int
cbm::ifaddr( cbm::ifaddr_t *  _if_addr)
{
    int  rc = -1;
#ifdef  CRABMEAT_OS_UNIXOID
    struct ifaddrs *  ifs_addrs = NULL;

    ::getifaddrs( &ifs_addrs);

    for ( struct ifaddrs *  ifa = ifs_addrs;
            ifa != NULL; ifa = ifa->ifa_next)
    {
        if ( !ifa->ifa_addr)
        {
            continue;
        }

        if ( cbm::is_equal( ifa->ifa_name, "lo")
                || cbm::is_equal( ifa->ifa_name, ""))
        {
            continue;
        }

        void const * sockaddr_ifaaddr = ifa->ifa_addr;
        if ( ifa->ifa_addr->sa_family == AF_INET /*v4*/)
        {
            void const * ip_addr =
                &((struct sockaddr_in const *)sockaddr_ifaaddr)->sin_addr;
            ::inet_ntop( AF_INET, ip_addr, _if_addr->addr, INET4_ADDRSTRLEN);
            _if_addr->addr[INET4_ADDRSTRLEN] = '\0';
            _if_addr->version = 4;
            rc = 0;
        }
        else if (ifa->ifa_addr->sa_family == AF_INET6 /*v6*/)
        {
            void const *  ip_addr =
                &((struct sockaddr_in6 const *)sockaddr_ifaaddr)->sin6_addr;
            ::inet_ntop( AF_INET6, ip_addr, _if_addr->addr, INET6_ADDRSTRLEN);
            _if_addr->addr[INET6_ADDRSTRLEN] = '\0';
            _if_addr->version = 6;
            rc = 0;
        }
        else
        {
            /* ignore */
        }

        if ( rc != -1)
        {
            /* use first valid found */
            break;
        }
    }

    if ( ifs_addrs)
    {
        ::freeifaddrs( ifs_addrs);
    }
#endif

    if ( rc == -1)
    {
        _if_addr->addr[0] = '\0';
        _if_addr->version = -1;
    }

    return  rc;
}

