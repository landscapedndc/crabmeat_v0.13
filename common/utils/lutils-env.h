/*!
 * @brief
 *    environment and user related functions
 *
 * @author
 *    steffen klatt (created on: may 21, 2013),
 *    edwin haas
 */

#ifndef  CBM_UTILS_ENVIRONMENT_H_
#define  CBM_UTILS_ENVIRONMENT_H_

#include  "crabmeat-common.h"
#include  "string/lstring-transform.h"

namespace cbm
{
    CBM_API char const *  getenv(
            char const *  /*environment variable name*/);
    CBM_API int  setenv(
            const char * /*environment variable name*/,
                   const char * /*environment variable value*/,
            int /*overwrite*/);
    CBM_API int  unsetenv(
            const char * /*environment variable name*/);

    /* check if we can figure out user's home directory */
    CBM_API bool  home_directory_found();

    /* retrieve user home directory */
    CBM_API std::string  home_directory();

	CBM_API std::string  hostname();
	CBM_API std::string  username();


    CBM_API void  tzset();
}


#if  defined(_MSC_VER)
#  include  <process.h>
typedef  int  pid_t;
#else
#  include  <sys/types.h>
#  include  <unistd.h>
#endif
namespace cbm
{
    /* get process id */
    CBM_API pid_t  get_pid();
    /* get process id of our parent */
    CBM_API pid_t  get_parent_pid();
}

#endif  /*  !CBM_UTILS_ENVIRONMENT_H_  */

namespace cbm
{
    /* query number of visible rows/columns for current terminal window */
    CBM_API int  terminal_extents( int * /*rows*/, int * /*columns*/);
}

