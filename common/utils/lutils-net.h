/*!
 * @brief
 *    some helper function concerning network stuff
 *
 * @author 
 *    steffen klatt (created on: mar. 25, 2015)
 */

#ifndef  CBM_UTILS_NET_H_
#define  CBM_UTILS_NET_H_

#include  "crabmeat-common.h"

#ifndef INET_ADDRSTRLEN
#  define INET_ADDRSTRLEN       16
#  define INET4_ADDRSTRLEN      INET_ADDRSTRLEN
#endif  /*  INET_ADDRSTRLEN  */

#ifndef INET6_ADDRSTRLEN
#  define INET6_ADDRSTRLEN      46
#endif  /*  INET6_ADDRSTRLEN  */

namespace cbm
{
    struct CBM_API ifaddr_t
    {
        enum { max_strlen = INET6_ADDRSTRLEN };

        char  addr[max_strlen+1];
        int  version;

        bool  valid() const { return  this->version != -1; }
    };
    int CBM_API ifaddr( ifaddr_t *);

}

#endif  /*  !CBM_UTILS_NET_H_  */

