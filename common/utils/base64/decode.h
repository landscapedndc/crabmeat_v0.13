// :mode=c++:
/*
decode.h - c++ wrapper for a base64 decoding algorithm

This is part of the libb64 project, and has been placed in the public domain.
For details, see http://sourceforge.net/projects/libb64
*/
#ifndef  CBM_BASE64_DECODE_H_
#define  CBM_BASE64_DECODE_H_

#include <iostream>

namespace cbm { namespace base64
{
    extern "C"
    {
        #include  "utils/base64/cdecode.h"
    }

    struct decoder
    {
        base64_decodestate _state;
// sk:off        int _buffersize;
// sk:off        decoder(int buffersize_in = 1024) : _buffersize(buffersize_in) {}

        int decode(char value_in)
        {
            return base64_decode_value((signed char)value_in);
        }

        static void free_buffer( char* plaintext_out)
        {
            CBM_DefaultAllocator->deallocate( plaintext_out);
        }
        int decode(const char* code_in, const size_t length_in, char** plaintext_out, size_t *length_out)
        {
            char *  decoded_plaintext_out = NULL;
            unsigned int  expected_length_out = length_in;
            if ( expected_length_out > 0)
            {
                decoded_plaintext_out =
                    (char *)CBM_DefaultAllocator->allocate( expected_length_out+1u);
                if ( !decoded_plaintext_out)
                {
                    *plaintext_out = NULL;
                    if ( length_out) { *length_out = 0; }
                    return  -1;
                }
            }
            else
            {
                *plaintext_out = NULL;
                if ( length_out) { *length_out = 0; }
                return  -1;
            }
            base64_init_decodestate( &_state);
            int const  bytes =
                base64_decode_block( code_in, length_in, decoded_plaintext_out, &_state);
            if ( length_out)
            {
                *length_out = bytes;
            }
            *plaintext_out = decoded_plaintext_out;
            return  0;
        }


// sk:off        int decode(const char* code_in, const int length_in, char* plaintext_out)
// sk:off        {
// sk:off            return base64_decode_block(code_in, length_in, plaintext_out, &_state);
// sk:off        }
// sk:off
// sk:off        void decode(std::istream& istream_in, std::ostream& ostream_in)
// sk:off        {
// sk:off            base64_init_decodestate(&_state);
// sk:off            //
// sk:off            const int N = _buffersize;
// sk:off            char* code = new char[N];
// sk:off            char* plaintext = new char[N];
// sk:off            int codelength;
// sk:off            int plainlength;
// sk:off
// sk:off            do
// sk:off            {
// sk:off                istream_in.read((char*)code, N);
// sk:off                codelength = istream_in.gcount();
// sk:off                plainlength = decode(code, codelength, plaintext);
// sk:off                ostream_in.write((const char*)plaintext, plainlength);
// sk:off            }
// sk:off            while (istream_in.good() && codelength > 0);
// sk:off            //
// sk:off            base64_init_decodestate(&_state);
// sk:off
// sk:off            delete [] code;
// sk:off            delete [] plaintext;
// sk:off        }
    };

} /* namespace base64 */
} /* namespace cbm */



#endif  /*  !CBM_BASE64_DECODE_H_ */

