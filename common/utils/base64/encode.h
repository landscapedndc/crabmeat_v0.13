// :mode=c++:
/*
encode.h - c++ wrapper for a base64 encoding algorithm

This is part of the libb64 project, and has been placed in the public domain.
For details, see http://sourceforge.net/projects/libb64
*/
#ifndef  CBM_BASE64_ENCODE_H_
#define  CBM_BASE64_ENCODE_H_

#include  "memory/cbm_mem.h"
#include  <iostream>

namespace cbm { namespace base64
{
    extern "C" 
    {
        #include "utils/base64/cencode.h"
    }

    struct encoder
    {
        base64_encodestate _state;
// sk:off        int _buffersize;
// sk:off        encoder(int buffersize_in = 1024) : _buffersize(buffersize_in) { }

        static size_t encode_size(size_t length_in)
        {
            size_t const  code_padded_size =
                (( length_in + ( ( length_in % 3) ? (3 - ( length_in % 3)) : 0) ) / 3) * 4;
            size_t const  newline_size = ((code_padded_size) / BASE64_CHARS_PER_LINE) * 2;

            return  code_padded_size + newline_size + 8/*being generous..*/;
        }

        static int encode(char value_in)
        {
            return base64_encode_value(value_in);
        }

        static void free_buffer( char* plaintext_out)
        {
            CBM_DefaultAllocator->deallocate( plaintext_out);
        }
        int encode(const char* code_in, size_t length_in, char** plaintext_out, size_t * length_out)
        {
            if ( plaintext_out)
            {
                char *  encoded_plaintext_out = NULL;
                unsigned int  expected_length_out = encoder::encode_size( length_in);
                if ( expected_length_out > 0u)
                {
                    encoded_plaintext_out =
                        (char *)CBM_DefaultAllocator->allocate( expected_length_out+1u);
                    if ( !encoded_plaintext_out)
                    {
                        *plaintext_out = NULL;
                        if ( length_out) { *length_out = 0; }
                        return  -1;
                    }
                }
                else
                {
                    *plaintext_out = NULL;
                    if ( length_out) { *length_out = 0; }
                    return  -1;
                }
                base64_init_encodestate(&_state);
                int  bytes =
                    base64_encode_block( code_in, length_in, encoded_plaintext_out, &_state);
                bytes += base64_encode_blockend( encoded_plaintext_out+bytes, &_state);
                encoded_plaintext_out[bytes] = '\0';
                if ( length_out)
                {
                    *length_out = bytes;
                }
                *plaintext_out = encoded_plaintext_out;
                return  0;
            }
            return  -1;
        }

// sk:off        int encode(const char* code_in, size_t length_in, char* plaintext_out)
// sk:off        {
// sk:off            return base64_encode_block(code_in, length_in, plaintext_out, &_state);
// sk:off        }
// sk:off
// sk:off        int encode_end(char* plaintext_out)
// sk:off        {
// sk:off            return base64_encode_blockend(plaintext_out, &_state);
// sk:off        }
// sk:off
// sk:off        void encode(std::istream& istream_in, std::ostream& ostream_in)
// sk:off        {
// sk:off            base64_init_encodestate(&_state);
// sk:off            //
// sk:off            const int N = _buffersize;
// sk:off            char* plaintext = new char[N];
// sk:off            char* code = new char[2*N];
// sk:off            int plainlength;
// sk:off            int codelength;
// sk:off
// sk:off            do
// sk:off            {
// sk:off                istream_in.read(plaintext, N);
// sk:off                plainlength = istream_in.gcount();
// sk:off                //
// sk:off                codelength = encode(plaintext, plainlength, code);
// sk:off                ostream_in.write(code, codelength);
// sk:off            }
// sk:off            while (istream_in.good() && plainlength > 0);
// sk:off
// sk:off            codelength = encode_end(code);
// sk:off            ostream_in.write(code, codelength);
// sk:off            //
// sk:off            base64_init_encodestate(&_state);
// sk:off
// sk:off            delete [] code;
// sk:off            delete [] plaintext;
// sk:off        }
    };

} /* namespace base64 */
} /* namespace cbm */

#endif /* !CBM_BASE64_ENCODE_H_ */

