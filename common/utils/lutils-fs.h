/*!
 * @brief
 *    filesystem related functions
 *
 * @author
 *    steffen klatt (created on: may 21, 2013),
 *    edwin haas
 */

#ifndef  CBM_UTILS_FILESYSTEM_H_
#define  CBM_UTILS_FILESYSTEM_H_

#include  "crabmeat-common.h"

namespace cbm
{
    CBM_API bool  is_file(
            char const * /*filename*/);
    CBM_API bool  is_dir(
            char const * /*path*/);
    CBM_API bool  is_driveletter(
            char const * /*path*/);

    CBM_API bool  path_exists(
            char const * /*path*/);
    CBM_API bool  is_absolute_path(
            char const * /*path*/);
    CBM_API lerr_t  mkdir(
            char const * /*path*/);
    CBM_API lerr_t  mkdirs(
            char const * /*path*/);

    CBM_API lerr_t  dirname(
            std::string * /*buffer (directoryname)*/,
            char const * /*path*/);

    CBM_API char const *  getcwd();
    CBM_API void  freecwd(
            char const * /*path*/);
}

#endif  /*  !CBM_UTILS_FILESYSTEM_H_  */

