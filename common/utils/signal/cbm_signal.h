/*!
 * @brief
 *    define signal handler API
 *
 * @author
 *    steffen klatt (created on: sep 01, 2012)
 */

#ifndef  CRABMEAT_SIGNAL_INTERFACE_H_
#define  CRABMEAT_SIGNAL_INTERFACE_H_

#include  "crabmeat-common.h"

#ifdef  CRABMEAT_SIGNAL_HANDLING
#  ifndef  _HAVE_FUNC_SIGACTION
#    error  currently only sigaction is supported
#  endif
#endif


extern CBM_API char **  retrieve_backtrace(
        unsigned char /*maximum backtrace symbols*/, unsigned char * /*found backtrace symbols*/,
        bool = true /*demangle?*/);
extern CBM_API void  dump_backtrace(
        char ** /*backtrace symbols*/, unsigned char /*number of backtrace symbols*/);

#ifdef  CRABMEAT_SIGNAL_HANDLING
struct CBM_API signal_info_t
{
    char const *  id;
    int  code;
    char const *  desc;
};

#  define  LDNDC_SIGNALS_CNT  (36)
extern CBM_API signal_info_t const  sig_list[LDNDC_SIGNALS_CNT];
#endif  /*  CRABMEAT_SIGNAL_HANDLING  */

typedef  int  lsignal_t;
extern CBM_API lsignal_t const  LDNDC_SIGHUP;
extern CBM_API lsignal_t const  LDNDC_SIGQUIT;
extern CBM_API lsignal_t const  LDNDC_SIGTRAP;
extern CBM_API lsignal_t const  LDNDC_SIGIOT;
extern CBM_API lsignal_t const  LDNDC_SIGBUS;
extern CBM_API lsignal_t const  LDNDC_SIGKILL;
extern CBM_API lsignal_t const  LDNDC_SIGUSR1;
extern CBM_API lsignal_t const  LDNDC_SIGUSR2;
extern CBM_API lsignal_t const  LDNDC_SIGPIPE;
extern CBM_API lsignal_t const  LDNDC_SIGALRM;
extern CBM_API lsignal_t const  LDNDC_SIGCLD;
extern CBM_API lsignal_t const  LDNDC_SIGCHLD;
extern CBM_API lsignal_t const  LDNDC_SIGCONT;
extern CBM_API lsignal_t const  LDNDC_SIGSTOP;
extern CBM_API lsignal_t const  LDNDC_SIGTSTP;
extern CBM_API lsignal_t const  LDNDC_SIGTTIN;
extern CBM_API lsignal_t const  LDNDC_SIGTTOU;
extern CBM_API lsignal_t const  LDNDC_SIGURG;
extern CBM_API lsignal_t const  LDNDC_SIGXCPU;
extern CBM_API lsignal_t const  LDNDC_SIGXFSZ;
extern CBM_API lsignal_t const  LDNDC_SIGVTALRM;
extern CBM_API lsignal_t const  LDNDC_SIGPROF;
extern CBM_API lsignal_t const  LDNDC_SIGWINCH;
extern CBM_API lsignal_t const  LDNDC_SIGPOLL;
extern CBM_API lsignal_t const  LDNDC_SIGIO;
extern CBM_API lsignal_t const  LDNDC_SIGSYS;
extern CBM_API lsignal_t const  LDNDC_SIGSTKFLT;
extern CBM_API lsignal_t const  LDNDC_SIGPWR;
extern CBM_API lsignal_t const  LDNDC_SIGABRT;
extern CBM_API lsignal_t const  LDNDC_SIGFPE;
extern CBM_API lsignal_t const  LDNDC_SIGILL;
extern CBM_API lsignal_t const  LDNDC_SIGINT;
extern CBM_API lsignal_t const  LDNDC_SIGSEGV;
extern CBM_API lsignal_t const  LDNDC_SIGTERM;


extern CBM_API lerr_t ldndc_signal_traps_init();

extern CBM_API lerr_t ldndc_have_signal_trap( lsignal_t);


#endif  /*  !CRABMEAT_SIGNAL_INTERFACE_H_  */

