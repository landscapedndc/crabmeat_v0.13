/*!
 * @brief
 *    define signals (wraps system signal names)
 *
 * @author
 *    steffen klatt (created on: sep 01, 2012)
 */

#include  "utils/signal/cbm_signal.h"
#ifdef  CRABMEAT_SIGNAL_HANDLING
#  ifdef  CRABMEAT_HAVE_SIGNAL_H
#    include  <signal.h>
#  else
#    error  "no signal.h found. giving up."
#  endif  /*  CRABMEAT_HAVE_SIGNAL_H  */
#endif  /*  CRABMEAT_SIGNAL_HANDLING  */

#ifdef  CRABMEAT_SIGNAL_HANDLING
#  ifdef  CRABMEAT_OS_UNIXOID
lsignal_t const  LDNDC_SIGHUP = SIGHUP;
#    define  LDNDC_SIGHUP_defined
lsignal_t const  LDNDC_SIGQUIT = SIGQUIT;
#    define  LDNDC_SIGQUIT_defined
lsignal_t const  LDNDC_SIGTRAP = SIGTRAP;
#    define  LDNDC_SIGTRAP_defined
lsignal_t const  LDNDC_SIGIOT = SIGIOT;
#    define  LDNDC_SIGIOT_defined
lsignal_t const  LDNDC_SIGBUS = SIGBUS;
#    define  LDNDC_SIGBUS_defined
lsignal_t const  LDNDC_SIGKILL = SIGKILL;
#    define  LDNDC_SIGKILL_defined
lsignal_t const  LDNDC_SIGUSR1 = SIGUSR1;
#    define  LDNDC_SIGUSR1_defined
lsignal_t const  LDNDC_SIGUSR2 = SIGUSR2;
#    define  LDNDC_SIGUSR2_defined
lsignal_t const  LDNDC_SIGPIPE = SIGPIPE;
#    define  LDNDC_SIGPIPE_defined
lsignal_t const  LDNDC_SIGALRM = SIGALRM;
#    define  LDNDC_SIGALRM_defined
lsignal_t const  LDNDC_SIGCLD = SIGCLD;
#    define  LDNDC_SIGCLD_defined
lsignal_t const  LDNDC_SIGCHLD = SIGCHLD;
#    define  LDNDC_SIGCHLD_defined
lsignal_t const  LDNDC_SIGCONT = SIGCONT;
#    define  LDNDC_SIGCONT_defined
lsignal_t const  LDNDC_SIGSTOP = SIGSTOP;
#    define  LDNDC_SIGSTOP_defined
lsignal_t const  LDNDC_SIGTSTP = SIGTSTP;
#    define  LDNDC_SIGTSTP_defined
lsignal_t const  LDNDC_SIGTTIN = SIGTTIN;
#    define  LDNDC_SIGTTIN_defined
lsignal_t const  LDNDC_SIGTTOU = SIGTTOU;
#    define  LDNDC_SIGTTOU_defined
lsignal_t const  LDNDC_SIGURG = SIGURG;
#    define  LDNDC_SIGURG_defined
lsignal_t const  LDNDC_SIGXCPU = SIGXCPU;
#    define  LDNDC_SIGXCPU_defined
lsignal_t const  LDNDC_SIGXFSZ = SIGXFSZ;
#    define  LDNDC_SIGXFSZ_defined
lsignal_t const  LDNDC_SIGVTALRM = SIGVTALRM;
#    define  LDNDC_SIGVTALRM_defined
lsignal_t const  LDNDC_SIGPROF = SIGPROF;
#    define  LDNDC_SIGPROF_defined
lsignal_t const  LDNDC_SIGWINCH = SIGWINCH;
#    define  LDNDC_SIGWINCH_defined
lsignal_t const  LDNDC_SIGPOLL = SIGPOLL;
#    define  LDNDC_SIGPOLL_defined
lsignal_t const  LDNDC_SIGIO = SIGIO;
#    define  LDNDC_SIGIO_defined
lsignal_t const  LDNDC_SIGSYS = SIGSYS;
#    define  LDNDC_SIGSYS_defined

#    ifndef  CRABMEAT_OS_MACOSX
lsignal_t const  LDNDC_SIGSTKFLT = SIGSTKFLT;
#    define  LDNDC_SIGSTKFLT_defined
lsignal_t const  LDNDC_SIGPWR = SIGPWR;
#    define  LDNDC_SIGPWR_defined
#    endif  /*  !CRABMEAT_OS_MACOSX  */

#  endif  /*  CRABMEAT_OS_UNIXOID  */

/* least common denominator (so far...) */
lsignal_t const  LDNDC_SIGABRT = SIGABRT;
#  define  LDNDC_SIGABRT_defined
lsignal_t const  LDNDC_SIGFPE = SIGFPE;
#  define  LDNDC_SIGFPE_defined
lsignal_t const  LDNDC_SIGILL = SIGILL;
#  define  LDNDC_SIGILL_defined
lsignal_t const  LDNDC_SIGINT = SIGINT;
#  define  LDNDC_SIGINT_defined
lsignal_t const  LDNDC_SIGSEGV = SIGSEGV;
#  define  LDNDC_SIGSEGV_defined
lsignal_t const  LDNDC_SIGTERM = SIGTERM;
#  define  LDNDC_SIGTERM_defined

#endif  /*  CRABMEAT_SIGNAL_HANDLING  */



#ifndef  LDNDC_SIGHUP_defined
lsignal_t const  LDNDC_SIGHUP = 0;
#endif
#ifndef  LDNDC_SIGQUIT_defined
lsignal_t const  LDNDC_SIGQUIT = 0;
#endif
#ifndef  LDNDC_SIGTRAP_defined
lsignal_t const  LDNDC_SIGTRAP = 0;
#endif
#ifndef  LDNDC_SIGIOT_defined
lsignal_t const  LDNDC_SIGIOT = 0;
#endif
#ifndef  LDNDC_SIGBUS_defined
lsignal_t const  LDNDC_SIGBUS = 0;
#endif
#ifndef  LDNDC_SIGKILL_defined
lsignal_t const  LDNDC_SIGKILL = 0;
#endif
#ifndef  LDNDC_SIGUSR1_defined
lsignal_t const  LDNDC_SIGUSR1 = 0;
#endif
#ifndef  LDNDC_SIGUSR2_defined
lsignal_t const  LDNDC_SIGUSR2 = 0;
#endif
#ifndef  LDNDC_SIGPIPE_defined
lsignal_t const  LDNDC_SIGPIPE = 0;
#endif
#ifndef  LDNDC_SIGALRM_defined
lsignal_t const  LDNDC_SIGALRM = 0;
#endif
#ifndef  LDNDC_SIGCLD_defined
lsignal_t const  LDNDC_SIGCLD = 0;
#endif
#ifndef  LDNDC_SIGCHLD_defined
lsignal_t const  LDNDC_SIGCHLD = 0;
#endif
#ifndef  LDNDC_SIGCONT_defined
lsignal_t const  LDNDC_SIGCONT = 0;
#endif
#ifndef  LDNDC_SIGSTOP_defined
lsignal_t const  LDNDC_SIGSTOP = 0;
#endif
#ifndef  LDNDC_SIGTSTP_defined
lsignal_t const  LDNDC_SIGTSTP = 0;
#endif
#ifndef  LDNDC_SIGTTIN_defined
lsignal_t const  LDNDC_SIGTTIN = 0;
#endif
#ifndef  LDNDC_SIGTTOU_defined
lsignal_t const  LDNDC_SIGTTOU = 0;
#endif
#ifndef  LDNDC_SIGURG_defined
lsignal_t const  LDNDC_SIGURG = 0;
#endif
#ifndef  LDNDC_SIGXCPU_defined
lsignal_t const  LDNDC_SIGXCPU = 0;
#endif
#ifndef  LDNDC_SIGXFSZ_defined
lsignal_t const  LDNDC_SIGXFSZ = 0;
#endif
#ifndef  LDNDC_SIGVTALRM_defined
lsignal_t const  LDNDC_SIGVTALRM = 0;
#endif
#ifndef  LDNDC_SIGPROF_defined
lsignal_t const  LDNDC_SIGPROF = 0;
#endif
#ifndef  LDNDC_SIGWINCH_defined
lsignal_t const  LDNDC_SIGWINCH = 0;
#endif
#ifndef  LDNDC_SIGPOLL_defined
lsignal_t const  LDNDC_SIGPOLL = 0;
#endif
#ifndef  LDNDC_SIGIO_defined
lsignal_t const  LDNDC_SIGIO = 0;
#endif
#ifndef  LDNDC_SIGSYS_defined
lsignal_t const  LDNDC_SIGSYS = 0;
#endif
#ifndef  LDNDC_SIGSTKFLT_defined
lsignal_t const  LDNDC_SIGSTKFLT = 0;
#endif
#ifndef  LDNDC_SIGPWR_defined
lsignal_t const  LDNDC_SIGPWR = 0;
#endif
#ifndef  LDNDC_SIGABRT_defined
lsignal_t const  LDNDC_SIGABRT = 0;
#endif
#ifndef  LDNDC_SIGFPE_defined
lsignal_t const  LDNDC_SIGFPE = 0;
#endif
#ifndef  LDNDC_SIGILL_defined
lsignal_t const  LDNDC_SIGILL = 0;
#endif
#ifndef  LDNDC_SIGINT_defined
lsignal_t const  LDNDC_SIGINT = 0;
#endif
#ifndef  LDNDC_SIGSEGV_defined
lsignal_t const  LDNDC_SIGSEGV = 0;
#endif
#ifndef  LDNDC_SIGTERM_defined
lsignal_t const  LDNDC_SIGTERM = 0;
#endif

#ifdef  CRABMEAT_SIGNAL_HANDLING
/* from bits/signum.h */
signal_info_t const  sig_list[LDNDC_SIGNALS_CNT] =
{
        /* fallback if signal was not found */
        { "?", 0, "unsupported signal"},

        { "SIGHUP", LDNDC_SIGHUP, "Hangup (POSIX)"},
        { "SIGQUIT", LDNDC_SIGQUIT, "Quit (POSIX)"},
        { "SIGTRAP", LDNDC_SIGTRAP, "Trace trap (POSIX)"},
        { "SIGIOT", LDNDC_SIGIOT, "IOT trap (4.2 BSD)"},
        { "SIGBUS", LDNDC_SIGBUS, "BUS error (4.2 BSD)"},
        { "SIGKILL", LDNDC_SIGKILL, "Kill, unblockable (POSIX)"},
        { "SIGUSR1", LDNDC_SIGUSR1, "User-defined signal 1 (POSIX)"},
        { "SIGUSR2", LDNDC_SIGUSR2, "User-defined signal 2 (POSIX)"},
        { "SIGPIPE", LDNDC_SIGPIPE, "Broken pipe (POSIX)"},
        { "SIGALRM", LDNDC_SIGALRM, "Alarm clock (POSIX)"},
        { "SIGCLD", LDNDC_SIGCLD, "Same as SIGCHLD (System V)"},
        { "SIGCHLD", LDNDC_SIGCHLD, "Child status has changed (POSIX)"},
        { "SIGCONT", LDNDC_SIGCONT, "Continue (POSIX)"},
        { "SIGSTOP", LDNDC_SIGSTOP, "Stop, unblockable (POSIX)"},
        { "SIGTSTP", LDNDC_SIGTSTP, "Keyboard stop (POSIX)"},
        { "SIGTTIN", LDNDC_SIGTTIN, "Background read from tty (POSIX)"},
        { "SIGTTOU", LDNDC_SIGTTOU, "Background write to tty (POSIX)."},
        { "SIGURG", LDNDC_SIGURG, "Urgent condition on socket (4.2 BSD)"},
        { "SIGXCPU", LDNDC_SIGXCPU, "CPU limit exceeded (4.2 BSD)"},
        { "SIGXFSZ", LDNDC_SIGXFSZ, "File size limit exceeded (4.2 BSD)"},
        { "SIGVTALRM", LDNDC_SIGVTALRM, "Virtual alarm clock (4.2 BSD)"},
        { "SIGPROF", LDNDC_SIGPROF, "Profiling alarm clock (4.2 BSD)."},
        { "SIGWINCH", LDNDC_SIGWINCH, "Window size change (4.3 BSD, Sun)"},
        { "SIGPOLL", LDNDC_SIGIO, "Pollable event occurred (System V)"},
        { "SIGIO", LDNDC_SIGIO, "I/O now possible (4.2 BSD)"},
        { "SIGSYS", LDNDC_SIGSYS, "Bad system call"},
        { "SIGSTKFLT", LDNDC_SIGSTKFLT, "Stack fault."},
        { "SIGPWR", LDNDC_SIGPWR, "Power failure restart (System V)"},

        /* least common denominator (so far...) */
        { "SIGABRT", LDNDC_SIGABRT, "Abort (ANSI)"},
        { "SIGFPE", LDNDC_SIGFPE, "Floating-point exception (ANSI)"},
        { "SIGILL", LDNDC_SIGILL, "Illegal instruction (ANSI)"},
        { "SIGINT", LDNDC_SIGINT, "Interrupt (ANSI)"},
        { "SIGSEGV", LDNDC_SIGSEGV, "Segmentation violation (ANSI)"},
        { "SIGTERM", LDNDC_SIGTERM, "Termination (ANSI)"},

        /* sentinel */
        { NULL, -1, NULL}
};
#endif  /*  CRABMEAT_SIGNAL_HANDLING */

