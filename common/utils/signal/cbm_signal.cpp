/*!
 * @brief
 *    define signal handler
 *
 * @author
 *    steffen klatt (created on: sep 01, 2012)
 */

#include  "utils/signal/cbm_signal.h"
#include  "log/cbm_baselog.h"

#include  "utils/signal/cbm_signals.cpp"

#ifdef  CRABMEAT_SIGNAL_HANDLING
#  ifdef  CRABMEAT_HAVE_SIGNAL_H
#    include  <signal.h>
#  else
#    error  "no signal.h found. giving up."
#  endif  /*  CRABMEAT_HAVE_SIGNAL_H  */


/* install our signal handler */
static  struct sigaction  __ldndc_sigaction;

extern void ldndc_signal_handler( int, siginfo_t *, void *);

lerr_t
ldndc_signal_traps_init()
{
    __ldndc_sigaction.sa_sigaction = &ldndc_signal_handler;
    sigemptyset( &__ldndc_sigaction.sa_mask);
    __ldndc_sigaction.sa_flags = SA_RESTART|SA_SIGINFO;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc_have_signal_trap( lsignal_t  _signum)
{
    (void)sigaction( _signum, &__ldndc_sigaction, NULL);

    return  LDNDC_ERR_OK;
}
#else
lerr_t
ldndc_signal_traps_init()
{
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc_have_signal_trap( lsignal_t)
{
    return  LDNDC_ERR_OK;
}

#endif  /*  CRABMEAT_SIGNAL_HANDLING  */



#ifdef  CRABMEAT_PROVIDES_BACKTRACE
#  include  <stdlib.h>
#  include  <stdio.h>
#  include  <execinfo.h>
#  include  <cxxabi.h>

/* obtain a backtrace and optionally demangle names */
char **
retrieve_backtrace( unsigned char  _bt_buf_size, unsigned char *  _bt_buf_real_size, bool  _demangle)
{
    if ( _bt_buf_size == 0)
    {
        return  (char **)NULL;
    }

    void **  bt_buf = new void*[_bt_buf_size];
    if ( !bt_buf)
    {
        return  (char **)NULL;
    }

    size_t  bt_size = backtrace( bt_buf, _bt_buf_size);
    *_bt_buf_real_size = (unsigned char)bt_size;

    char **  bt_syms = backtrace_symbols( bt_buf, static_cast< int >( bt_size));

    if ( !bt_syms)
    {
        return  (char**)NULL;
    }

    if ( !_demangle)
    {
        return  bt_syms;
    }

    for ( size_t  k = 0;  k < bt_size;  ++k)
    {
        char *  s( bt_syms[k]);
        
        size_t  j( 0), s_beg( 0), s_end( 0);

        while ( s && s[j] != '\0')
        {
            if ( s[j] == '(')
            {
                s_beg = j+1;
            }
            else if ( s[j] == '+')
            {
                s_end = j;
                s[j] = '\0';
                break;
            }
            ++j;
        }

        if ( s  &&  s_beg  &&  s_end)
        {
            int  rc( -1);
            char *  s_demang = abi::__cxa_demangle( s+s_beg, NULL, NULL, &rc);

            if (( rc != 0)  &&  s_demang)
            {
                free( s_demang);
                /* keep old symbol, if demangling failed */
            }
            else
            {
                if ( s_demang)
                {
                    /* replace original symbol
                     *
                     * from the backtrace_symbols manpage:
                     *
                     * "The strings pointed to by the array of pointers
                     * need not and should not be freed"
                     *
                     * so we don't  ...  //free( bt_syms[k]);
                     */
                    bt_syms[k] = s_demang;
                }
                else
                {
                    bt_syms[k] = s+s_beg;
                }
            }
        }
    }

    return  bt_syms;
}
void
dump_backtrace( char **  _bt, unsigned char  _bt_size)
{
    if ( !_bt)
    {
        return;
    }
    CBM_LogWrite_( "call stack (",static_cast< int >( _bt_size),"):");
#define  LDNDC_SKIP_CALLS  2
    char **  bt = _bt + LDNDC_SKIP_CALLS;
    for ( unsigned char  k = LDNDC_SKIP_CALLS;  k < _bt_size;  ++k, ++bt)
    {
        CBM_LogWrite_( ( *bt) ? *bt : "?");
    }
#undef  LDNDC_SKIP_CALLS
}
#else
/* dummy backtrace function */
char **
retrieve_backtrace( unsigned char, unsigned char *, bool)
{
    return  (char **)NULL;
}
void
dump_backtrace( char **, unsigned char)
{
    /* no op */
}
#endif

#ifdef  _HAVE_FUNC_SIGACTION
#  define  __SIG_BLOCK_ALL                    \
    /* block all signals originating from handler */    \
    sigset_t  block_mask_prev = __ldndc_sigaction.sa_mask;    \
    sigaddset( &__ldndc_sigaction.sa_mask, SIGFPE);
    /*sigfillset( __ldndc_sigaction.sa_mask);*/

#  define  __SIG_UNBLOCK_ALL                    \
    __ldndc_sigaction.sa_mask = block_mask_prev;
#else
#  define  __SIG_BLOCK_ALL
#  define  __SIG_UNBLOCK_ALL
#endif  /*  _HAVE_FUNC_SIGACTION  */



#ifdef  CRABMEAT_SIGNAL_HANDLING
#  include  "cbm_rtcfg.h"
#  include  "time/cbm_clock.h"
#  ifdef  CRABMEAT_FPE_EXCEPTS
#    include  "utils/ieee/cbm_ieee.h"
#  endif  /*  CRABMEAT_FPE_EXCEPTS  */
void
ldndc_signal_handler( int  _sig_num, siginfo_t *  _sig_info, void *  /*_secret*/)
{
    __SIG_BLOCK_ALL

    int  sig_pos( 0);
    for ( int  k = 1;  sig_list[k].id;  ++k)
    {
        if ( _sig_num == sig_list[k].code)
        {
            sig_pos = k;
            break;
        }
    }

    switch ( _sig_num)
    {
        case  LDNDC_SIGSEGV:
        {
            CBM_LogFatal( "segmentation fault:  faulty address @",_sig_info->si_addr);
            break;
        }
        case  LDNDC_SIGFPE:
        {
#  ifdef  CRABMEAT_FPE_EXCEPTS
            /* unset fpe flag bit */
            ldndc_ieee_clear_excepts( LDNDC_FE_ALL);
#  endif  /*  CRABMEAT_FPE_EXCEPTS  */

            /* reset blocked signals */
            __SIG_UNBLOCK_ALL

            /* do not abort */
            return;
        }
        case  LDNDC_SIGUSR1:
        {
            fprintf( stderr, "simulation time: %s\n",
                           ( CBM_LibRuntimeConfig.clk) ? CBM_LibRuntimeConfig.clk->to_string().c_str() : "<not running>");
            return;
        }
        case  LDNDC_SIGINT:
            fprintf( stderr, "received SIGINT at simulation time %s .. attempting to shut down cleanly\n",
                    ( CBM_LibRuntimeConfig.clk) ? CBM_LibRuntimeConfig.clk->now().to_string().c_str() : "?");
            CBM_LibRuntimeConfig.sc.request_term = 1;
            if ( CBM_LibRuntimeConfig.clk && ( CBM_LibRuntimeConfig.sc.clean_term == 1))
            {
                CBM_LibRuntimeConfig.sc.clean_term = 0;
                return;
            }
            if ( !CBM_LibRuntimeConfig.clk)
            {
                fprintf( stderr, "no. not started, yet .. brutally killing me then\n");
            }
            else
            {
                fprintf( stderr, "no. user told me not to .. brutally killing me then\n");
            }
            break;
        default:
        {
            CBM_LogError( "caught signal  [signal=",sig_list[sig_pos].id,"context=",sig_list[sig_pos].desc,"]");
            /* do not abort */
            return;
        }
    }

    abort();
}
#endif  /*  CRABMEAT_SIGNAL_HANDLING  */

