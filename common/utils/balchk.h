/*!
 * @brief
 *        provides facility for balance checks
 *
 * @author
 *        steffen klatt
 */

#ifndef  CBM_BALANCE_CHECK_H_
#define  CBM_BALANCE_CHECK_H_

#include  "crabmeat-common.h"
#include  <string>

namespace cbm {

        /*!
         * @brief
         *    simple array/scalar item container that sums up all
         *    single values and stores it for later comparison after
         *    possible changes to items stored in this container.
         *
         * @note
         *    scarcely tested ;-)
         */
        template < typename  _T >
        class  balance_check_t  {
                template < typename  _U >
                struct  lbalance_vector  {
                        _U const *  vec;
                        size_t  size;

                        _U  scale;

                        std::string  name;
                };

                public:
                        balance_check_t() 
                                : sum_( 0), dirty_( false),
                                  tol_( LDNDC_EPS(_T)),
                                  pos_( 0), size_( 0u), data_( NULL),
                                  input_( 0), loss_( 0),
                                  desc_( "")
                        {
                        }
                        balance_check_t( _T  _tolerance, char const *  _desc = "<no info given>")
                                : sum_( 0), dirty_( false),
                                  tol_( _tolerance),
                                  pos_( 0), size_( 1u), data_( NULL),
                                  input_( 0), loss_( 0),
                                  desc_( _desc)
                        {
                                data_ = new  lbalance_vector< _T >[size_];
                        }

                        balance_check_t( balance_check_t const &  _other)
                                : pos_( 0), size_( 1u), data_( NULL)
                        {
                                this->operator=( _other);
                        }

                        balance_check_t &  operator=( balance_check_t const &  _other)
                        {
                                if ( &_other != this) {
                                        sum_ = _other.sum_;
                                        dirty_ = _other.dirty_;
                                        tol_ = _other.tol_;
                                        pos_ = _other.pos_;
                                        size_ = _other.size_;
                                        desc_ = _other.desc_;

                                        input_ = _other.input_;
                                        loss_ = _other.loss_;

                                        if ( _other.data_) {
                                                if ( data_) { delete[]  data_; }
                                                data_ = new  lbalance_vector< _T >[size_];
                                                for ( size_t  k = 0;  k < size_;  ++k) {
                                                        data_[k] = _other.data_[k];
                                                }
                                        } else {
                                                if ( data_) { delete[]  data_; }
                                        }
                                }
                                return  *this;
                        }

                        ~balance_check_t()
                        {
                                if ( data_) delete[] data_;
                        }

                        _T  tolerance() const { return  tol_; }
                        bool  is_balanced()
                        const
                        {
                                //CBM_LogDebug1( "running check...   source", desc_);
                                if ( size_ == 0)
                                        return  true;

                                if ( dirty_) {
                                        CBM_LogError( "balance check sum out of date: call 'update()'");
                                        return  false;
                                }

                                return  cbm::flt_equal_eps( sum_ + input_ - loss_, sum(), tol_);
                        }

                        _T  update();
                        _T  sum() const;
                        _T  diff() const { return  fabs( sum_ + input_ - loss_ - sum()); }

                        void  add( _T const *, _T  _scale = (_T)1.0, char const * = NULL);
                        void  add( _T const *, size_t, _T  _scale = (_T)1.0, char const * = NULL);

                        void  set_input( _T const &  _input) { input_ = _input; }
                        void  set_loss( _T const &  _loss) { loss_ = _loss; }

                        void  warn( unsigned long int  _time = 0) const
                        {
                                CBM_LogWarn( "imbalance detected: '"+desc_
                                                +"'  [timestep="+(( _time == 0) ? "<unknown>" : cbm::n2s(_time))
                                                +",diff="+cbm::n2s(diff(), 10)+"]");
                        }
#ifdef  _DEBUG
                        void  dump() const { CBM_LogDebug4( "sum", sum_, "sum(cur)", sum(), "input", input_, "loss", loss_); }
#endif

                private:
                        _T  sum_;
                        bool  dirty_;

                        _T  tol_;

                        size_t  pos_;
                        size_t  size_;
                        lbalance_vector< _T > *  data_;

                        _T  input_, loss_;

                        std::string  desc_;

                        //balance_check_t( balance_check_t const &);
        };

        template < typename  _T >
        _T
        balance_check_t< _T >::update()
        {
                input_ = loss_ = 0;

                if ( size_) {
                        sum_ = const_cast< balance_check_t< _T > * >( this)->sum();
                        dirty_ = false;
                }
                return  sum_;
        }
        template < typename  _T >
        _T
        balance_check_t< _T >::sum()
        const
        {
                if ( size_) {
                        _T  s( 0);
                        for ( size_t  k = 0;  k < pos_;  ++k) {

                                if ( !data_[k].vec) { continue; }

                                double  s_k( 0.0);
                                for ( size_t  j = 0;  j < data_[k].size;  ++j) {
                                        s_k += data_[k].vec[j];
                                }
                                s += data_[k].scale * s_k;
                                //CBM_LogDebug2( "item", data_[k].name, "\tsum", s_k);
                        }
                        return  s;
                }
                return  sum_;
        }

        template < typename  _T >
        void
        balance_check_t< _T >::add( _T const *  _d, _T  _scale, char const *  _item_name)
        {
                this->add( _d, 1, _scale, _item_name);
        }

        template < typename  _T >
        void
        balance_check_t< _T >::add( _T const *  _d, size_t  _s, _T  _scale, char const *  _item_name)
        {
                dirty_ = true;

                /* realloc */
                if ( pos_ == size_) {
                        size_ *= 2;
                        lbalance_vector< _T > *  tmp = new  lbalance_vector< _T >[size_];
                        for ( size_t  k = 0;  k < pos_;  ++k) {
                                tmp[k] = data_[k];
                        }
                        delete[] data_;
                        data_ = tmp;
                }
                data_[pos_].vec = _d;
                data_[pos_].size = _s;
                data_[pos_].scale = _scale;
                data_[pos_].name = std::string(( _item_name) ? _item_name : "<none>");
                ++pos_;
        }

}  /* namespace cbm*/



#ifdef  CRABMEAT_BALANCE_CHECKS

/* add scalar to balance container (and scaled version) */
#  define  BALANCE_ADD_SCALAR(__bobj__,__item__)                { (__bobj__).add( __item__, 1.0 /*,#__item__*/); }
#  define  BALANCE_ADD_SCALAR_S(__bobj__,__item__,__scale__)    { (__bobj__).add( __item__, __scale__ /*,#__item__*/); }
/* add array to balance container (and scaled version) */
#  define  BALANCE_ADD(__bobj__,__item__,__size__)                { (__bobj__).add( __item__, __size__, 1.0 /*,#__item__*/); }
#  define  BALANCE_ADD_S(__bobj__,__item__,__size__,__scale__)    { (__bobj__).add( __item__, __size__, __scale__ /*,#__item__*/); }
/* refresh sum and reset input and loss */
#  define  BALANCE_UPDATE(__bobj__)                                { (__bobj__).update(); }
/* add additional input to be considered */
#  define  BALANCE_INPUT(__bobj__,__input__)                    { (__bobj__).set_input( __input__); }
/* add additional loss to be considered */
#  define  BALANCE_LOSS(__bobj__,__loss__)                        { (__bobj__).set_loss( __loss__); }
/* check balance, relative to last update */
#  define  BALANCE_CHECK_FAIL(__bobj__)                            ( ! (__bobj__).is_balanced())
#else
#  define  BALANCE_ADD_SCALAR(__bobj__,__item__)                CRABMEAT_NOOP
#  define  BALANCE_ADD_SCALAR_S(__bobj__,__item__,__scale__)    CRABMEAT_NOOP
#  define  BALANCE_ADD(__bobj__,__item__,__size__)              CRABMEAT_NOOP
#  define  BALANCE_ADD_S(__bobj__,__item__,__size__,__scale__)  CRABMEAT_NOOP
#  define  BALANCE_UPDATE(__bobj__)                                CRABMEAT_NOOP
#  define  BALANCE_INPUT(__bobj__,__input__)                    CRABMEAT_NOOP
#  define  BALANCE_LOSS(__bobj__,__loss__)                        CRABMEAT_NOOP
#  define  BALANCE_CHECK_FAIL(__bobj__)                            0
#endif

#endif  /* CBM_BALANCE_CHECK_H_ */

