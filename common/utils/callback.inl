/*! 
 * @brief
 *      provides callback mechanisms
 *
 * @see
 *      http://www.codeguru.com/cpp/cpp/cpp_mfc/callbacks/article.php/c4129
 *      http://www.tutok.sk/fastgl/callback.html
 *
 * @author 
 *      Steffen Klatt
 */


template < typename  _R >
ldndc::abstract_callback_0< _R >::~abstract_callback_0< _R >()
{
}


template < typename  _R, typename  _A >
ldndc::abstract_callback_1< _R, _A >::~abstract_callback_1< _R, _A >()
{
}

