/*!
 * @brief
 *    swap values of two variables of identical type
 *
 * @author
 *    steffen klatt (created on: jul 12, 2013)
 */

#ifndef  CBM_UTILS_SWAP_H_
#define  CBM_UTILS_SWAP_H_

#include  "crabmeat-common.h"

namespace cbm
{
    template < typename _T >
    void  swap_values( _T &  _a, _T &  _b)
    {
        if ( &_a != &_b)
        {
            _T  a_tmp = _a;
            _a = _b;
            _b = a_tmp;
        }
    }

}  /* namespace cbm */


#endif  /*  !CBM_UTILS_SWAP_H_  */

