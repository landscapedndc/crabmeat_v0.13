/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: may 12, 2012)
 *    edwin haas
 */

#ifndef  LDNDC_ABSTRACT_DATAFILTER_H_
#define  LDNDC_ABSTRACT_DATAFILTER_H_

#include  "crabmeat-common.h"

template < typename _E >
class  data_filter_t
{
        public:
        data_filter_t();

        virtual  ~data_filter_t() = 0;


        virtual  lerr_t  filter( 
                size_t  _n,
                _E *  _dst, size_t  _dst_stride,
                _E const *  _src, size_t  _src_aggr_size, size_t  _src_stride) const = 0;

    private:
        data_filter_t(
                data_filter_t const &);
        data_filter_t &  operator=(
                data_filter_t const &);
};


#include  "datafilters/filter.inl"

#endif  /*  !LDNDC_ABSTRACT_DATAFILTER_H_  */

