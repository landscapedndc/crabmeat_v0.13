/*!
 * @brief
 *    averages values in array not considering
 *    invalids and zeros
 *
 * @author
 *    steffen klatt (created on: aug 18, 2013)
 *    edwin haas
 */

#ifndef  LDNDC_DATAFILTER_AVERAGE_NONZERO_H_
#define  LDNDC_DATAFILTER_AVERAGE_NONZERO_H_

#include  "datafilters/filter.h"

template < typename _E >
class  data_filter_average_nonzero_t  :  public  data_filter_t< _E >
{
    public:
        typedef  _E  value_type;

        public:
        data_filter_average_nonzero_t();
        ~data_filter_average_nonzero_t();

        lerr_t  filter(
                size_t,
                _E *, size_t,
                _E const *, size_t,
                size_t) const;
};


#include  "datafilters/filter-average-nonzero.inl"

#endif  /*  !LDNDC_DATAFILTER_AVERAGE_NONZERO_H_  */

