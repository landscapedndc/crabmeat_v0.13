/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 02, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_ABSTRACT_DATAFILTER_AGGREGATE_H_
#define  LDNDC_ABSTRACT_DATAFILTER_AGGREGATE_H_

#include  "datafilters/filter.h"


template < typename _E >
class  data_aggregate_t
{
    public:
        data_aggregate_t(
                data_filter_t< _E > const ** /*list of datafilters*/,
                size_t /*record size*/);

        virtual  ~data_aggregate_t();


        lerr_t  filter(
                size_t /*buffer size in units of record*/,
                _E * /*destination buffer*/, size_t /*destination record memory size*/,
                _E const * /*source buffer*/, size_t /*source record memory size*/,
                size_t /*aggregate count*/) const;

    private:
        /* not owner */
        data_filter_t< _E > const **  data_filters_;
        /* record size (matching number data filters in data filter list */
        size_t  record_size_;

        /* hide */
        data_aggregate_t(
                data_aggregate_t const &);
        data_aggregate_t &  operator=(
                data_aggregate_t const &);
};


#include  "datafilters/aggregator.inl"

#endif  /*  !LDNDC_ABSTRACT_DATAFILTER_AGGREGATE_H_  */

