/*!
 * @brief
 *    sums values in array not considering invalids
 *
 * @author
 *    steffen klatt (created on: jul 01, 2012)
 *    edwin haas
 */

#ifndef  LDNDC_DATAFILTER_SUM_H_
#define  LDNDC_DATAFILTER_SUM_H_

#include  "crabmeat-common.h"
#include  "datafilters/filter.h"

template < typename _E >
class  data_filter_sum_t  :  public  data_filter_t< _E >
{
    public:
        typedef  _E  value_type;

        public:
        data_filter_sum_t();
        ~data_filter_sum_t();

        lerr_t  filter(
                size_t,
                _E *, size_t,
                _E const *, size_t,
                size_t) const;
};


#include  "datafilters/filter-sum.inl"

#endif  /*  !LDNDC_DATAFILTER_SUM_H_  */

