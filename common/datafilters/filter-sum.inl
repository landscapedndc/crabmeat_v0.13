/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: jul 01, 2012)
 *    edwin haas
 */

#include  "log/cbm_baselog.h"

template < typename _E >
data_filter_sum_t< _E >::data_filter_sum_t()
    : data_filter_t< _E >()
{
}

template < typename _E >
data_filter_sum_t< _E >::~data_filter_sum_t()
{
}


#include  "utils/lutils.h"
template < typename _E >
lerr_t
data_filter_sum_t< _E >::filter(
        size_t  _n,
        _E *        _dst, size_t  _dst_stride,
        _E const *  _src, size_t  _src_stride,
        size_t  _src_aggr_size)
const
{
    size_t  q( _src_stride*_src_aggr_size);
    _E const *  s = _src;
    for ( size_t  k = 0;  k < _n;  ++k)
    {
        int  all_invalid = 1;

        *_dst = (_E)0;

        s += q;
        for ( ; _src < s;  _src += _src_stride)
        {
// sk:dbg            CBM_LogDebug( "k=", k, "  s[j]=", *_src, "  d-stride=",_dst_stride, "  s-stride=", _src_stride, "  aggr-size=",_src_aggr_size);
            if ( cbm::is_valid( *_src))
            {
                all_invalid = 0;
                *_dst += *_src;
            }
        }
        if ( all_invalid)
        {
            *_dst = ldndc::invalid_t< _E >::value;
        }
        _dst += _dst_stride;
    }

    return  LDNDC_ERR_OK;
}

