/*!
 * @brief
 *    convenience header pulling all data filters
 *
 * @author
 *    steffen klatt (created on: mar 02, 2013)
 *    edwin haas
 */

#ifndef  LDNDC_ABSTRACT_DATAFILTERS_H_
#define  LDNDC_ABSTRACT_DATAFILTERS_H_

#include  "datafilters/filter-average.h"
#include  "datafilters/filter-average-nonzero.h"
#include  "datafilters/filter-copy.h"
#include  "datafilters/filter-minimum.h"
#include  "datafilters/filter-maximum.h"
#include  "datafilters/filter-sum.h"

#endif  /*  !LDNDC_ABSTRACT_DATAFILTERS_H_  */

