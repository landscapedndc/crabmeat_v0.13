/*!
 * @brief
 *    null filter: just copies from source to destination
 *
 * @author
 *    steffen klatt (created on: jul 01, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_DATAFILTER_COPY_H_
#define  LDNDC_DATAFILTER_COPY_H_

#include  "datafilters/filter.h"

template < typename _E >
class  data_filter_copy_t  :  public  data_filter_t< _E >
{
    public:
        typedef  _E  value_type;

        public:
        data_filter_copy_t();
        ~data_filter_copy_t();

        lerr_t  filter(
                size_t,
                _E *, size_t,
                _E const *, size_t,
                size_t) const;
};


#include  "datafilters/filter-copy.inl"

#endif  /*  !LDNDC_DATAFILTER_COPY_H_  */

