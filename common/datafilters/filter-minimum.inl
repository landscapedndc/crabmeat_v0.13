/*!
 * @brief
 *
 * @author
 *    david kraus (created on: feb, 2021)
 */


template < typename _E >
data_filter_minimum_t< _E >::data_filter_minimum_t()
    : data_filter_t< _E >()
{
}

template < typename _E >
data_filter_minimum_t< _E >::~data_filter_minimum_t()
{
}


#include  "utils/cbm_utils.h"
template < typename _E >
lerr_t
data_filter_minimum_t< _E >::filter(
        size_t  _n,
        _E *        _dst, size_t  _dst_stride,
        _E const *  _src, size_t  _src_stride,
        size_t  _src_aggr_size)
const
{
    int  c = 0;
    size_t  q = _src_stride*_src_aggr_size;
    _E const *  s = _src;

    for ( size_t  k = 0;  k < _n;  ++k)
    {
        *_dst = *_src;

        s += q;
        for ( ; _src < s;  _src += _src_stride)
        {
            if ( cbm::is_valid( *_src))
            {
                if ( *_src < *_dst)
                {
                    *_dst = *_src;
                }
                ++c;
            }
        }

        if ( c)
        {
            c = 0;
        }
        else
        {
            *_dst = ldndc::invalid_t< _E >::value;
        }

        _dst += _dst_stride;
    }

    return  LDNDC_ERR_OK;
}
