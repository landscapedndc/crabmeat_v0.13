/*!
 * @brief
 *    minimize values in array not considering
 *    invalids
 *
 * @author
 *    david kraus (created on: feb, 2021)
 */

#ifndef  LDNDC_DATAFILTER_MAXIMUM_H_
#define  LDNDC_DATAFILTER_MAXIMUM_H_

#include  "datafilters/filter.h"

template < typename _E >
class  data_filter_maximum_t  :  public  data_filter_t< _E >
{
    public:
        typedef  _E  value_type;

        public:
        data_filter_maximum_t();
        ~data_filter_maximum_t();

        lerr_t  filter(
                size_t,
                _E *, size_t,
                _E const *, size_t,
                size_t) const;
};


#include  "datafilters/filter-maximum.inl"

#endif  /*  !LDNDC_DATAFILTER_MAXIMUM_H_  */

