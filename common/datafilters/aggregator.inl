/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 02, 2013),
 *    edwin haas
 */

#include  "datafilters/filters.h"
#include  "log/cbm_baselog.h"


template < typename _E >
data_aggregate_t< _E >::data_aggregate_t(
        data_filter_t< _E > const **  _data_filters,
        size_t  _record_size)
        : data_filters_( _data_filters),
          record_size_( _record_size)
{
}

template < typename _E >
data_aggregate_t< _E >::~data_aggregate_t()
{
}


template< typename _E >
lerr_t
data_aggregate_t< _E >::filter(
        size_t  _buf_size,
        _E *        _dst_buf, size_t  _dst_mem_size,
        _E const *  _src_buf, size_t  _src_mem_size,
        size_t  _a)
const
{
    for ( size_t  r = 0;  r < this->record_size_;  ++r)
    {
// sk:dbg        CBM_LogDebug( "[r=", r, "  d-buf=",_dst_buf+r, "  s-buf=",_src_buf+r, "]");
        if ( !this->data_filters_[r])
        {
            /* assume slot is unused */
            continue;
        }
        lerr_t  rc_filt = this->data_filters_[r]->filter(
                _buf_size,
                _dst_buf + r, _dst_mem_size,
                _src_buf + r, _src_mem_size,
                _a);
        RETURN_IF_NOT_OK(rc_filt);
    }
// sk:dbg    CBM_LogDebug( "filtered data records  [n=", _buf_size, "  stride=",_a, "  src-mem-size=",_src_mem_size, "  dst-mem-size=",_dst_mem_size, "]");

    return  LDNDC_ERR_OK;
}

