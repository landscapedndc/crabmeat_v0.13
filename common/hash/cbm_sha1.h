/*
 Copyright (c) 2011, Micael Hildenborg
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Micael Hildenborg nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY Micael Hildenborg ''AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Micael Hildenborg BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef  CBM_HASH_SHA1_H_
#define  CBM_HASH_SHA1_H_

#include  "crabmeat-common.h"

namespace cbm {  namespace hash
{
#define  SHA1_RESULT_SIZE  (5*sizeof(unsigned int))
    typedef  unsigned char  sha1_result_t[SHA1_RESULT_SIZE];
    typedef  char  sha1_hexstring_t[41];
    void  CBM_API  sha1(
            void const * /*source*/, int /*bytelength*/,
            sha1_result_t /*hash*/);

    ldndc_uint64_t  CBM_API sha1_uint64(
            void const * /*source*/, int /*bytelength*/);

    /**
      @param hash is 20 bytes of sha1 hash. This is the same data that is the result from the calc function.
      @param hexstring should point to a buffer of at least 41 bytes of size for storing the hexadecimal representation of the hash. A zero will be written at position 40, so the buffer will be a valid zero ended string.
      */
    void  CBM_API sha1_as_hexstring(
            unsigned char const * /*hash*/,
	        sha1_hexstring_t /*hexstring*/);
    void  CBM_API sha1hex(
	        void const * /*source*/, int /*bytelength*/,
            sha1_hexstring_t /*hexstring*/);

} /* namespace hash */ } /* namespace cbm */

#endif /*  !CBM_HASH_SHA1_H_  */

