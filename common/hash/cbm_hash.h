/*! 
 * @brief
 *  hashing functions
 *
 * @author 
 *  steffen klatt (created on: nov. 19, 2016)
 */

#ifndef  CBM_HASH_H_
#define  CBM_HASH_H_

#include  "crabmeat-common.h"

#include  "hash/cbm_djb2.h"
#include  "hash/cbm_murmur3.h"
#include  "hash/cbm_sha1.h"

#endif /* !CBM_HASH_H_ */

