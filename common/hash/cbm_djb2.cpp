/* 
 * @brief
 *  string hashing function
 */

#include  "hash/cbm_djb2.h"

/* found at http://www.cse.yorku.ca/~oz/hash.html */
unsigned long int
cbm::hash::djb2( char const *  _str)
{
    unsigned char const *  str =
        (unsigned char const *)_str;
    unsigned long int  hash = 5381;
    int  c = 0;

    while (( c = *str++) != '\0')
    {
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash;
}

