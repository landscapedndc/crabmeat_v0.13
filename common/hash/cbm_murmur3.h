/*! 
 * @brief
 *  hashing functions
 *
 * @author 
 *  steffen klatt (created on: jan. 12, 2017)
 */

#ifndef  CBM_MURMUR3_H_
#define  CBM_MURMUR3_H_

#include  "crabmeat-common.h"

namespace cbm{
  namespace hash {

/* from code.google.com/p/smhasher/wiki/MurmurHash3 */
inline uint32_t CBM_API  murmur3( uint32_t  _i32)
{
	_i32 ^= _i32 >> 16;
	_i32 *= 0x85ebca6b;
	_i32 ^= _i32 >> 13;
	_i32 *= 0xc2b2ae35;
	_i32 ^= _i32 >> 16;
	return  _i32;
}

/* from code.google.com/p/smhasher/wiki/MurmurHash3 */
inline uint64_t CBM_API  murmur3( uint64_t  _i64)
{
	_i64 ^= _i64 >> 33;
	_i64 *= 0xff51afd7ed558ccd;
	_i64 ^= _i64 >> 33;
	_i64 *= 0xc4ceb9fe1a85ec53;
	_i64 ^= _i64 >> 33;
	return  _i64;
}

  } /*namespace hash*/
} /*namespace cbm*/

#endif /* !CBM_MURMUR3_H_ */

