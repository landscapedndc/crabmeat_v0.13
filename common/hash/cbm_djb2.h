/*! 
 * @brief
 *  string hashing function (DJB2)
 *
 * @author 
 *  steffen klatt
 */

#ifndef  CBM_HASH_DJB2_H_
#define  CBM_HASH_DJB2_H_

#include  "crabmeat-common.h"

namespace cbm{
  namespace hash {

    unsigned long int CBM_API djb2(
        char const * /* \0 terminated string*/);

  } /*namespace hash*/
} /*namespace cbm*/

#endif /*  !CBM_HASH_DJB2_H_  */

