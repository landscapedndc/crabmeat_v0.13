/*!
 * @brief
 *    wrapper class for rather simple models
 *    with callback for configurability
 *
 * @author
 *    steffen klatt (created on: jun 27, 2016)
 */

#ifndef  CBM_TINYMODELCOLLECTION_H_
#define  CBM_TINYMODELCOLLECTION_H_

#include  "crabmeat-common.h"

namespace cbm {

template < typename object_type, typename return_type, typename argument_type >
struct TinyModelCollection
{
    typedef  return_type  (object_type::*EvaluateFunction)( argument_type) const;
    TinyModelCollection()
        : f_eval( 0) { }
    virtual ~TinyModelCollection()
        { }

    virtual return_type  evaluate(
        argument_type, char const * = 0 /*kind*/) const = 0;
    protected:
        EvaluateFunction  f_eval;
        virtual EvaluateFunction  evaluate_function(
                    char const * = 0 /*kind*/) const = 0;
};
} /* namespace cbm */

#endif  /* !CBM_TINYMODELCOLLECTION_H_ */

