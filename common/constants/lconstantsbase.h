/*!
 * @brief
 *    contains base definitions for all constants declarations
 *
 * @author
 *    steffen klatt ( created on: 2009),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_BASE_H_
#define CBM_CONSTANTS_BASE_H_


/* powers of 10 */
#define  GIGA   1.0e+09
#define  MEGA   1.0e+06
#define  KILO   1.0e+03
#define  HECTO  1.0e+02
#define  DECA   1.0e+01
#define  DECI   1.0e-01
#define  CENTI  1.0e-02
#define  MILLI  1.0e-03
#define  MICRO  1.0e-06
#define  NANO   1.0e-09


#endif  /*  !CBM_CONSTANTS_BASE_H_  */

