/*!
 * @brief
 *    contains mathematical constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_MATH_H_
#define CBM_CONSTANTS_MATH_H_

#include  <math.h>

namespace cbm
{
    /*! number pi */
    static const double  PI = 3.14159265358979323846;

    /*! system wide allowable deviation for balance checks */
    static const double  DIFFMAX = 1.0e-09;

} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_MATH_H_  */

