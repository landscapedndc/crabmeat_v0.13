/*!
 * @brief
 *    contains air related constants, etc.
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_METEO_H_
#define CBM_CONSTANTS_METEO_H_

namespace cbm
{
    static const double  D_O2_AIR       = 0.205e-4;        //!< air diffusion coefficient of O2 at T = 293.15 K in [m2 s-1]    (Paul 'Soil Microbiology and Biochemistry')
    static const double  D_CO2_AIR      = 0.161e-4;        //!< air diffusion coefficient of CO2 at T = 293.15 K in [m2 s-1]    (Paul 'Soil Microbiology and Biochemistry')
    static const double  D_N2_AIR       = 0.170e-4;        //!< air diffusion coefficient of N2 at T = 293.15 K in [m2 s-1]    (Paul 'Soil Microbiology and Biochemistry')
    static const double  D_N2O_AIR      = 0.206e-4;        //!< air diffusion coefficient of N2O at T = 293.15 K in [m2 s-1]    (Paul 'Soil Microbiology and Biochemistry')
    static const double  D_NO_AIR       = 0.206e-4;        //!< air diffusion coefficient of NO set to D_N2O_AIR in [m2 s-1] as long as there is no better value found
    static const double  D_CH4_AIR      = 0.205e-4;        //!< set to air diffusion coefficient of O2 as long as there is no better value found
    static const double  D_H2_AIR       = 0.205e-4;        //!< set to air diffusion coefficient of O2 as long as there is no better value found
    static const double  D_NH3_AIR      = 0.205e-4;        //!< set to air diffusion coefficient of O2 as long as there is no better value found
    
    static const double  D_O2_WATER     = 2.0e-9;          //!< water diffusion coefficient of O2 [m2 s-1]        http://bionumbers.hms.harvard.edu//bionumber.aspx?id=104440&ver=6
    static const double  D_CO2_WATER    = 1.9e-9;          //!< water diffusion coefficient of CO2 [m2 s-1]    http://bionumbers.hms.harvard.edu//bionumber.aspx?id=102625&ver=8
    static const double  D_NO3_WATER    = 1.7e-9;          //!< water diffusion coefficient of NO3 [m2 s-1]    http://bionumbers.hms.harvard.edu//bionumber.aspx?id=104439&ver=4
    static const double  D_NO2_WATER    = 1.7e-9;          //!< water diffusion coefficient of NO2 [m2 s-1]    http://bionumbers.hms.harvard.edu//bionumber.aspx?id=104439&ver=2
    static const double  D_NH4_WATER    = 1.9e-9;          //!< water diffusion coefficient of NH4 [m2 s-1]    http://bionumbers.hms.harvard.edu//bionumber.aspx?id=104437&ver=3
    
    static const double  D_DOC_WATER    = 4.586e-10;       //!< water diffusion coefficient of DOC (saccharose) at T = 293.15 K [m2 s-1]    (Atkins 'Physical Chemistry')
    static const double  D_N2O_WATER    = 0.211e-8;        //!< water diffusion coefficient of N2O at T = 293.15 K in [m2 s-1]    (Paul 'Soil Microbiology and Biochemistry')
    static const double  D_NO_WATER     = 0.211e-8;        //!< water diffusion coefficient of NO set to D_N2O_WATER in [m2 s-1] as long as there is no better value found
    static const double  D_CH4_WATER    = 1.9e-9;          //!< set to water diffusion coefficient of CO2 until better value is found

    static const double  CO2REF         = 370;             //!< ambient co2 reference concentration

    static const double  ELEV0          = 8434.5;          //!< scale height of the Rayleigh atmosphere near the earth surface (Rigollier et al. 2000, cit in Wang et al. 2006).
    static const double  PO2            = 0.208;           //!< volumentric percentage of oxygen in the canopy air
    static const double  PN2            = 0.78;            //!< volumentric percentage of N2 in air
    static const double  PN2O           = 3.3e-7;          //!< volumentric percentage of nitrous oxide (N2O) in the atmosphere
    static const double  PRESS0         = 1013.25;         //!< sea level standard pressure (mbar)
    static const double  RS             = 287.05;          //!< gas constant for dry air [J.kg-1.K-1]
    static const double  RV             = 461.495;         //!< specific gas constant for water vapor [J.kg-1.K-1]

    static const double  H_CH4          = 1.27e-5;         //!< Henry coefficient for CH4 at T = 298.15 K in [mol Pa-1 m-3] (Scheffer/Schachtschabel 'Lehrbuch der Bodenkunde')
    static const double  H_CO2          = 3.35e-4;         //!< Henry coefficient for CO2 at T = 298.15 K in [mol Pa-1 m-3] (Scheffer/Schachtschabel 'Lehrbuch der Bodenkunde')
    static const double  H_N2           = 6.25e-6;         //!< Henry coefficient for N2 at T = 298.15 K in [mol Pa-1 m-3] (Scheffer/Schachtschabel 'Lehrbuch der Bodenkunde')
    static const double  H_O2           = 1.24e-5;         //!< Henry coefficient for O2 at T = 298.15 K in [mol Pa-1 m-3] (Scheffer/Schachtschabel 'Lehrbuch der Bodenkunde')
    static const double  H_NH3          = 5.63e-1;         //!< Henry coefficient for NH3 at T = 298.15 K in [mol Pa-1 m-3] (Scheffer/Schachtschabel 'Lehrbuch der Bodenkunde')
    static const double  H_N2O          = 2.54e-4;         //!< Henry coefficient for N2O at T = 298.15 K in [mol Pa-1 m-3] (Scheffer/Schachtschabel 'Lehrbuch der Bodenkunde')
    static const double  H_NO           = 1.90e-5;         //!< Henry coefficient for NO at T = 298.15 K in [mol Pa-1 m-3] (Warneck and Williams, 2012)
} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_METEO_H_  */

