/*!
 * @brief
 *    contains physical constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_PHYS_H_
#define CBM_CONSTANTS_PHYS_H_

namespace cbm
{
    /* physical constants */
    static const double  AVG_SOLAR_CONSTANT    = 1367.0;      //!< average solar constant  [W m-2]
    static const double  CP            = 1005.0;              //!< specific heat for dry air  [J kg-1 K-1]
    static const double  EXC           = 0.033;               //!< earth excentricity factor
    static const double  G             = 9.81;                //!< earth acceleration  [m s-2]
    static const double  KARMAN        = 0.41;                //!< Karman constant (Hogstrom 1985)
    static const double  LAMBDA        = 2.5;                 //!< latent heat of vaporization at 0 oC  [MJ kg-1]

    static const double  RD            = 287.04;              //!< gas constant for dry air  [J kg-1 K-1]
    static const double  RGAS          = 8.3143;              //!< general gas constant  [J mol-1 K-1]
    static const double  SIGMA         = 5.6705e-08;          //!< Stefan-Boltzmann constant in SI standard units
    static const double  SIGMA_MJ      = 4.903e-09;           //!< Stefan-Boltzmann constant  [MJ M-2 K-4 -d]
    static const double  VGAS          = 22.414;              //!< gas volume of one mol under standard conditions  [l mol-1]


    static const double  ETOM          = 0.4085;              //!< ETOM (mm water)/(MJ/m2) using Lv 2448 MJ/Mg and density of water 1 Mg/m3 = 1E3 mm/m / (2448 MJ/Mg * 1 Mg/m3)
    static const double  WTOMJ         = 0.0864;              //!< WTOMJ (MJ m-2 d-1)/(watt/m2) = 86400 s/d * .000001 MJ/J

    static const double  GAMMA         = 0.0662;              //!< GAMMA CONSTANTE PSYCHROMETRIQUE EN KPA PAR DEGRE K
    static const double  LF            = 0.335;               //!< heat of fusion of water, MJ m-2 mm-1
    static const double  LS            = 2.824;               //!< latent heat of sublimation of snow, MJ m-2 mm-1
    
    /* heat capacities [J:K^-1:kg^-1] */

    static const double  CAIR        = 1005.0;                //!< Heat capacity of air
    static const double  CWAT        = 4180.0;                //!< Heat capacity of water   (Richter 1986)
    static const double  CICE        = 2100.0;                //!< Heat capacity of ice (at 300K)
    static const double  CMIN        = 753.0;                 //!< Heat capacity of mineral soil    (Richter 1986)
    static const double  CORG        = 1884.0;                //!< Heat capacity of soil organic matter (Richter 1986)
    
    /* densities [kg:dm^-3] */

    static const double  DAIR        = 1.2e-3;                //!< Density of air
    static const double  DWAT        = 1.0;                   //!< Density of water
    static const double  DICE        = 0.917;                 //!< Density of ice
    static const double  DSNO        = 0.2;                   //!< Density of snow (actually ranges from 0.1-0.4, e.g. Mellander et al. 2007)
    static const double  DMIN        = 2.65;                  //!< Density of mineral substances (quartz) (Ruehlmann et al., 2006)
    static const double  DSAN        = 2.65;                  //!< Density of sand [Prof. Dr.-Ing. Rolf Katzenbach, scribt Geotechnik]
    static const double  DSIL        = 2.69;                  //!< Density of silt [Prof. Dr.-Ing. Rolf Katzenbach, scribt Geotechnik]
    static const double  DCLA        = 2.75;                  //!< Density of clay [Prof. Dr.-Ing. Rolf Katzenbach, scribt Geotechnik]
    static const double  DORG        = 1.34;                  //!< Density of soil organic matter (Scheffer and Schachtschnabel 1992: 1.34; Adams 1973: 1.298)
    
    /* heat conductivities [J K-1 cm-1 s-1] */

    static const double  KAIR        = 0.00025;               //!< Heat conductivity of air (Scheffer and Schachtschnabel 2010)
    static const double  KWAT        = 0.0057;                //!< Heat conductivity of water   (Richter 1986)
    static const double  KICE        = 0.021;                 //!< Heat conductivity of ice (Richter 1986)
    static const double  KSNO        = 0.0005;                //!< Heat conductivity of snow (this is only a rough estimate)
    static const double  KMIN        = 0.0290;                //!< Heat conductivity of mineral soil    (Richter 1986)
    static const double  KORG        = 0.0149;                //!< Heat conductivity of soil organic matter (Richter 1986)
    static const double  KHUM        = 0.0025;                //!< Heat conductivity of humus   (Scheffer and Schachtschnabel 2010)

    static const double VISCOSITY_AIR = 18.5e-6;              // Viscosity of air (25 oC, 1 bar) [Pa:s]
} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_PHYS_H_  */

