/*!
 * @brief
 *    contains conversion constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_CONV_H_
#define CBM_CONSTANTS_CONV_H_

#include  "constants/lconstantsbase.h"


/*! conversion of degree celsius into kelvin */
#define  CELSIUS_TO_KELVIN(__c__)    ((double)(__c__) + cbm::D_IN_K)
/*! conversion of degree kelvin into celsius */
#define  KELVIN_TO_CELSIUS(__c__)    ((double)(__c__) - cbm::D_IN_K)

/*!
 * @brief
 *    unit conversion factors
 *
 * @note
 *    read conversion factors X_IN_Y like "how many X fit into Y",
 *    that is  aY = aX * X_IN_Y, where a is some constant
 *
 *    example:
 *      2cm to m would be 2cm = 2*M_IN_CM m = 2*(1/CM_IN_M) m = 2/100 m = 0.02m
 */
namespace  cbm
{
    /* declaration of general conversion factors */

    static const double  CM2_IN_M2        = 1.0e+04;                //!<
    static const double  M2_IN_CM2        = 1.0 / CM2_IN_M2;        //!<
    static const double  DM3_IN_M3        = 1.0e+03;                //!< conversion factor dm3 into m3
    static const double  M3_IN_DM3        = 1.0 / DM3_IN_M3;        //!<
    static const double  G_IN_KG          = 1.0e+03;                //!< 0.001 kg per g
    static const double  KG_IN_G          = 1.0 / G_IN_KG;          //!<
    static const double  BAR_IN_PA        = 1.0e+05;                //!<
    static const double  PA_IN_BAR        = 1.0 / BAR_IN_PA;        //!<
    static const double  MBAR_IN_KPA      = 1.0e+01;                //!< kilo pascal to milli bar
    static const double  KPA_IN_MBAR      = 1.0 / MBAR_IN_KPA;      //!< milli bar to kilo pascal
    static const double  KPA_IN_MPA       = 1.0e+03;                //!< kilo pascal per mega pascal
    static const double  MPA_IN_KPA       = 1.0 / KPA_IN_MPA;       //!< mega pascal per kilo pascal
    static const double  PA_IN_KPA        = 1.0e+03;                //!<
    static const double  KPA_IN_PA        = 1.0 / PA_IN_KPA;        //!<
    static const double  MBAR_IN_PA       = 1.0e+02;                //!<
    static const double  PA_IN_MBAR       = 1.0 / MBAR_IN_PA;       //!<
    static const double  PA_IN_MH2O       = 1.0e+4;                 //!< pascal in meter water height
    static const double  MH2O_IN_PA       = 1.0 / PA_IN_MH2O;       //!< meter water height in pascal
    static const double  M2_IN_HA         = 1.0e+04;                //!< 10000 m2 per ha
    static const double  HA_IN_M2         = 1.0 / M2_IN_HA;         //!< 0.0001 ha per m2
    static const double  MG_IN_KG         = 1.0e+06;                //!<
    static const double  KG_IN_MG         = 1.0 / MG_IN_KG;         //!< 0.000001 kg per mg
    static const double  CM_IN_M          = 1.0e+02;                //!<
    static const double  M_IN_CM          = 1.0 / CM_IN_M;          //!< 0.01 m per cm
    static const double  J_IN_KJ          = 1.0e+03;                //!< 0.001 J per MJ
    static const double  KJ_IN_J          = 1.0 / J_IN_KJ;          //!<
    static const double  J_IN_MJ          = 1.0e+06;                //!< 0.000001 J per MJ
    static const double  MJ_IN_J          = 1.0 / J_IN_MJ;          //!<
    static const double  MM_IN_CM         = 1.0e+01;                //!< 10 mm per cm
    static const double  CM_IN_MM         = 1.0 / MM_IN_CM;         //!< 0.1 cm per mm
    static const double  MM_IN_M          = 1.0e+03;                //!< 1000 mm per m
    static const double  M_IN_MM          = 1.0 / MM_IN_M;          //!< 0.001 m per mm
    static const double  PMOL_IN_MOL      = 1.0e+12;                //!<
    static const double  MOL_IN_PMOL      = 1.0 / PMOL_IN_MOL;      //!< conversion from picomol in mol
    static const double  NG_IN_UG         = 1.0e+03;                //!< conversion factor from nano to micro (gramm)
    static const double  UG_IN_NG         = 1.0 / NG_IN_UG;         //!<

    static const double  C_IN_CO2         = 12.0/44.0;              //!< conversion factor for carbon in co2 molecule
    static const double  CO2_IN_C         = 44.0/12.0;              //!< conversion factor for carbon in co2 molecule

    static const double  M_IN_FT          = 0.3048;                 //!< conversion factor for m in foot
    static const double  IN_IN_M          = 39.37;                  //!< conversion factor for m in inch

    static const double  D_IN_K           = 273.15;                 //!< kelvin at zero degree celsius

    static const double  UMOL_IN_W        = 4.57;                   //!< conversion factor from Watt in umol PAR (Cox et al. 1998)
    static const double  W_IN_UMOL        = 1.0 / UMOL_IN_W;        //!< conversion factor from umol PAR in Watt (Cox et al. 1998)

    static const double  FPAR             = 0.45;                   //!< conversion factor for global into PAR (Monteith 1965, Meek et al. 1984)
    static const double  PPB_IN_KG        = 0.0054;                 //!< term conversion from 1000ppb NO in kgN ha-1 and soil layer // Remde XXXX,S.56
    static const double  PPM              = 1.0e-6;                 //!< parts per million

    /*! gN per gDW into percent dryweigth */
    static const double  PRO_IN_GG        = HECTO;
    /*! conversion factor for percent in m3 volume air space per m-3 soil */
    static const double  PRO_IN_M3        = KILO;

    /*! ton to kg */
    static const double  KG_IN_T          = KILO;
    /*! kg to ton */
    static const double  T_IN_KG          = 1.0 / KG_IN_T;

    /*! watt per m2 to joule per day and cm2 */
    static const double  JDCM_IN_WM       = 8.64e4 * M2_IN_CM2;
    /*! joule per day and cm2 to watt per m2 */
    static const double  WM_IN_JDCM       = 1.0 / JDCM_IN_WM;

    /*! mmol to mol */
    static const double  MOL_IN_MMOL      = MILLI;
    /*! mol to mmol */
    static const double  MMOL_IN_MOL      = 1.0 / MOL_IN_MMOL;

    /*! umol to mol */
    static const double  MOL_IN_UMOL      = MICRO;
    /*! mol to umol */
    static const double  UMOL_IN_MOL      = 1.0 / MOL_IN_UMOL;

    /*! nmol to umol */
    static const double  NMOL_IN_UMOL     = KILO;
    /*! umol to nmol */
    static const double  UMOL_IN_NMOL     = 1.0 / NMOL_IN_UMOL;
}  /*  namespace cbm  */


#endif  /*  !CBM_CONSTANTS_CONV_H_  */

