/*!
 * @brief
 *    contains chemical constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_CHEM_H_
#define CBM_CONSTANTS_CHEM_H_

namespace cbm
{
    /* molar weights */
    static const double  MH            = 1.0;             //!< molar weight of hydrogen  [g mol-1]
    static const double  MC            = 12.0;            //!< molar weight of carbon  [g mol-1]
    static const double  MN            = 14.0;            //!< molar weight of nitrogen  [g mol-1]
    static const double  MO            = 16.0;            //!< molar weight of oxygen  [g mol-1]
    static const double  MS            = 32.0;            //!< molar weight of S  [g mol-1]
    static const double  MMN           = 55.0;            //!< molar weight of Mn  [g mol-1]
    static const double  MFE           = 55.8;            //!< molar weight of Fe  [g mol-1]
    static const double  MCH2O         = MC+2.0*MH+MO;    //!< molar weight of dry weight unit  [g mol-1]
    static const double  MH2O          = 2.0*MH+MO;       //!< molar weight of water  [g mol-1]
    static const double  MCH4          = MC+4.0*MH;       //!< molar weight of CH4  [g mol-1]
    static const double  MCO2          = MC+2.0*MO;       //!< molar weight of CO2  [g mol-1]
    static const double  MC5H8         = MC*5.0+MH*8.0;   //!< molar weight of C5H8 (Isoprene)  [g mol-1]
    static const double  MC10H16       = MC*10.0+MH*16.0; //!< molar weight of C10H16 (Monoterpenes)  [g mol-1]

    /* number of carbons */
    static const double  C_ISO         = 5.0;             //!< number of carbons in Isoprene (C5H8)
    static const double  C_MONO        = 10.0;            //!< number of carbons in Monoterpene (C10H16)

} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_CHEM_H_  */

