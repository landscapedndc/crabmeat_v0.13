/*!
 * @brief
 *    contains soil related constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_SOIL_H_
#define CBM_CONSTANTS_SOIL_H_

namespace cbm
{
    /*!  carbon content of soil organic matter (litter) */
    static const double  CCORG        = 0.5;
    /*!  mol weight of carbon required for 4 nitrogen molecules (5 carbon x 12) (g mol-1) according to Ingwersen paper SSSAJ 1999 eq. 2 */
    static const double  CNEFF        = 60.0;
    /*!  fraction of fine roots in the litter layer (Hoeglwald data, Kreutzer et al. 1991) */
    static const double  FLIT        = 0.02;
    /*!  Melting enthalpy for water (J kg-1)(6010 kJ mol-1 / 0.0180152 kg mol-1)\n */
    static const double  MELT        = 333607.0;
    /*!  mol weight of nitrogen (4x14) (g mol-1) according to Ingwersen paper SSSAJ 1999 eq. 2 */
    static const double  MWN        = 56.0;

} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_SOIL_H_  */

