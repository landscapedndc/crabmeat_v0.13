/*!
 * @brief
 *    (temporary) convenience header pulling all constants
 *    header files.
 *
 * @author
 *    steffen klatt (created on: 2009)
 */

#ifndef CBM_CONSTANTS_H_
#define CBM_CONSTANTS_H_


#include  "constants/lconstants-chem.h"
#include  "constants/lconstants-comp.h"
#include  "constants/lconstants-conv.h"
#include  "constants/lconstants-math.h"
#include  "constants/lconstants-meteo.h"
#include  "constants/lconstants-phys.h"
#include  "constants/lconstants-plant.h"
#include  "constants/lconstants-soil.h"
#include  "constants/lconstants-time.h"


#endif  /*  !CBM_CONSTANTS_H_  */

