/*!
 * @brief
 *    contains time-related constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_TIME_H_
#define CBM_CONSTANTS_TIME_H_

namespace cbm
{
    /*! minute to seconds */
    static const unsigned int  SEC_IN_MIN        = 60;
    /*! hour to minutes */
    static const unsigned int  MIN_IN_HR        = 60;
    /*! day to hours */
    static const unsigned int  HR_IN_DAY        = 24;
    /*! year to months */
    static const unsigned int  MONTHS_IN_YEAR    = 12;

    /*! hour to seconds */
    static const unsigned int  SEC_IN_HR        = ( SEC_IN_MIN * MIN_IN_HR);

    /*! day to minutes */
    static const unsigned int  MIN_IN_DAY        = ( MIN_IN_HR * HR_IN_DAY);
    /*! day to seconds */
    static const unsigned int  SEC_IN_DAY        = ( SEC_IN_HR * HR_IN_DAY);

} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_TIME_H_  */

