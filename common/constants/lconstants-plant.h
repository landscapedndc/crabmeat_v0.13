/*!
 * @brief
 *    contains plant related constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013),
 *    edwin haas
 */

#ifndef CBM_CONSTANTS_PLANT_H_
#define CBM_CONSTANTS_PLANT_H_

namespace cbm
{
    /*!  relative carbon content of plant dry matter */
    static const double  CCDM        = 0.45;
    /*!  diameter at breast height, above which crown diameter functions are applied */
    static const double  DBHLIMIT    = 0.01;
    /*!  minimum thickness of canopy layers (m) */
    static const double  H_FLMIN     = 0.02;
    /*!  preliminary assumed height above which stem form functions are applied; NEEDS TO BE LARGER THAN 1.3 m */
    static const double  HLIMIT      = 2.6;
    /*!  average molecular weight of monoterpenes (g mol-1), Fuentes et al. 2000 */
    static const double  MMONO       = 136.24;

} /* namespace cbm */


#endif  /*  !CBM_CONSTANTS_PLANT_H_  */

