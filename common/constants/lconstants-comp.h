/*!
 * @brief
 *    contains computer architecture related constants
 *
 * @author
 *    steffen klatt (created on: feb 01, 2013)
 */

#ifndef CBM_CONSTANTS_COMP_H_
#define CBM_CONSTANTS_COMP_H_


/*! size of byte in terms of bits */
#define  BITS_IN_BYTE        ((size_t)8)


#endif  /*  !CBM_CONSTANTS_COMP_H_  */

