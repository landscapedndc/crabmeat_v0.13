/*!
 * @brief
 *    declares identifiers for atomic data types
 *
 * @author 
 *    steffen klatt  (created on: nov 02, 2013),
 *    edwin haas
 */

#ifndef  CRABMEAT_ATOMICDATATYPES_H_
#define  CRABMEAT_ATOMICDATATYPES_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"
#include  "cbm_nan.h"

#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  include  <inttypes.h>
#endif

#if  defined(CRABMEAT_MINGW)
#  if  defined(LDNDC_HAVE_FLOAT128)
#    undef  LDNDC_HAVE_FLOAT128
#  endif
#else
//#  define  LDNDC_HAVE_FLOAT128
#endif
typedef  int  atomic_datatype_t;

enum  atomic_datatype_e
{
    LDNDC_ATOMIC_DATATYPE_NONE,

    LDNDC_ATOMIC_DATATYPE_BOOL,

    LDNDC_ATOMIC_DATATYPE_INT8,
    LDNDC_ATOMIC_DATATYPE_UINT8,
    LDNDC_ATOMIC_DATATYPE_INT16,
    LDNDC_ATOMIC_DATATYPE_UINT16,
    LDNDC_ATOMIC_DATATYPE_INT32,
    LDNDC_ATOMIC_DATATYPE_UINT32,
    LDNDC_ATOMIC_DATATYPE_INT64,
    LDNDC_ATOMIC_DATATYPE_UINT64,

    LDNDC_ATOMIC_DATATYPE_FLOAT32,
    LDNDC_ATOMIC_DATATYPE_FLOAT64,
#ifdef  LDNDC_HAVE_FLOAT128
    LDNDC_ATOMIC_DATATYPE_FLOAT128,
#endif

    LDNDC_ATOMIC_DATATYPE_CHAR,
    LDNDC_ATOMIC_DATATYPE_STRING,

    LDNDC_ATOMIC_DATATYPE_COMPOUND,

    LDNDC_ATOMIC_DATATYPE_RAW,

    LDNDC_ATOMIC_DATATYPE_CNT,


    LDNDC_ATOMIC_DATATYPE_INVALID = -1
};
extern
CBM_API char const *  LDNDC_ATOMIC_DATATYPE_NAMES[LDNDC_ATOMIC_DATATYPE_CNT];
extern
CBM_API size_t  get_atomic_datatype_size( atomic_datatype_t);


typedef bool ldndc_bool_t;
#ifndef  LDNDC_PRINTF_SEQ_BOOL
#  define  LDNDC_PRINTF_SEQ_BOOL  "%d"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_BOOL;

typedef int8_t ldndc_int8_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_INT8  "%" PRId8
#endif
#ifndef  LDNDC_PRINTF_SEQ_INT8
#  define  LDNDC_PRINTF_SEQ_INT8  "%hhd"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_INT8;
typedef uint8_t ldndc_uint8_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_UINT8  "%" PRIu8
#endif
#ifndef  LDNDC_PRINTF_SEQ_UINT8
#  define  LDNDC_PRINTF_SEQ_UINT8  "%hhu"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_UINT8;
typedef int16_t ldndc_int16_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_INT16  "%" PRId16
#endif
#ifndef  LDNDC_PRINTF_SEQ_INT16
#  define  LDNDC_PRINTF_SEQ_INT16  "%hd"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_INT16;
typedef uint16_t ldndc_uint16_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_UINT16  "%" PRIu16
#endif
#ifndef  LDNDC_PRINTF_SEQ_UINT16
#  define  LDNDC_PRINTF_SEQ_UINT16  "%hu"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_UINT16;
typedef int32_t ldndc_int32_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_INT32  "%" PRId32
#endif
#ifndef  LDNDC_PRINTF_SEQ_INT32
#  define  LDNDC_PRINTF_SEQ_INT32  "%d"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_INT32;
typedef uint32_t ldndc_uint32_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_UINT32  "%" PRIu32
#endif
#ifndef  LDNDC_PRINTF_SEQ_UINT32
#  define  LDNDC_PRINTF_SEQ_UINT32  "%u"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_UINT32;
typedef int64_t ldndc_int64_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_INT64  "%" PRId64
#endif
#ifndef  LDNDC_PRINTF_SEQ_INT64
#  if  __WORDSIZE == 32 || defined(CRABMEAT_OS_MACOSX) /*they seem to always use long long int*/
#    define  LDNDC_PRINTF_SEQ_INT64  "%lld"
#  elif  __WORDSIZE == 64
#    define  LDNDC_PRINTF_SEQ_INT64  "%ld"
#  else
    /* heeeelp */
#  endif
#endif
extern CBM_API atomic_datatype_t const  LDNDC_INT64;
typedef uint64_t ldndc_uint64_t;
#ifdef  CRABMEAT_HAVE_INTTYPES_H
#  define  LDNDC_PRINTF_SEQ_UINT64  "%" PRIu64
#endif
#ifndef  LDNDC_PRINTF_SEQ_UINT64
#  if  __WORDSIZE == 32 || defined(CRABMEAT_OS_MACOSX) /*they seem to always use unsigned long long int*/
#    define  LDNDC_PRINTF_SEQ_UINT64  "%llu"
#  elif  __WORDSIZE == 64
#    define  LDNDC_PRINTF_SEQ_UINT64  "%lu"
#  else
    /* heeeelp */
#  endif
#endif
extern CBM_API atomic_datatype_t const  LDNDC_UINT64;

typedef float ldndc_flt32_t;
#ifndef  LDNDC_PRINTF_SEQ_FLT32
#  define  LDNDC_PRINTF_SEQ_FLT32  "%e"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_FLOAT32;
typedef double ldndc_flt64_t;
#ifndef  LDNDC_PRINTF_SEQ_FLT64
#  define  LDNDC_PRINTF_SEQ_FLT64  "%e"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_FLOAT64;
#ifdef  LDNDC_HAVE_FLOAT128
typedef long double ldndc_flt128_t;
#  ifndef  LDNDC_PRINTF_SEQ_FLT128
#    define  LDNDC_PRINTF_SEQ_FLT128  "%Le"
#  endif
extern CBM_API atomic_datatype_t const  LDNDC_FLOAT128;
#endif

typedef char ldndc_char_t;
typedef ldndc_char_t ldndc_byte_t;
#ifndef  LDNDC_PRINTF_SEQ_CHAR
#  define  LDNDC_PRINTF_SEQ_CHAR  "%c"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_CHAR;
extern CBM_API atomic_datatype_t const  LDNDC_BYTE;
#define  sizeof_CBM_STRING  (CBM_STRING_SIZE+1)
typedef char ldndc_string_t[sizeof_CBM_STRING];
#ifndef  LDNDC_PRINTF_SEQ_STRING
#  define  LDNDC_PRINTF_SEQ_STRING  "%s"
#endif
extern CBM_API atomic_datatype_t const  LDNDC_STRING;

/* declared in used context */
extern CBM_API atomic_datatype_t const  LDNDC_COMPOUND;

typedef char * ldndc_raw_t;
extern CBM_API atomic_datatype_t const  LDNDC_RAW;

/* rather fatal */
extern CBM_API atomic_datatype_t const  LDNDC_DTYPE_INVALID;
/* ignore me */
extern CBM_API atomic_datatype_t const  LDNDC_DTYPE_NONE;

#endif  /*  !CRABMEAT_ATOMICDATATYPES_H_  */

