
#ifndef  CRABMEAT_COMMON_H_
#define  CRABMEAT_COMMON_H_


#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  <stddef.h>

/* defines common atomic data types */
#include  "cbm_types.h"

/* ldndc object ID */
#include  "cbm_id.h"

/* representation of a flag list */
#include  "cbm_flags.h"

/* not-a-number for ldndc */
#include  "cbm_nan.h"

/* defines error codes */
#include  "cbm_errors.h"

/* defines "special" base class */
#include  "cbm_object.h"

#endif  /*  !CRABMEAT_COMMON_H_  */

