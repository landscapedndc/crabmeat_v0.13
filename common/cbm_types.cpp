/*!
 * @brief
 *    defines identifiers for atomic data types
 *
 * @author 
 *    steffen klatt  (created on: nov 02, 2013),
 */

#include  "cbm_types.h"
#include  "cbm_nan.h"

atomic_datatype_t const  LDNDC_BOOL = LDNDC_ATOMIC_DATATYPE_BOOL;

atomic_datatype_t const  LDNDC_INT8 = LDNDC_ATOMIC_DATATYPE_INT8;
atomic_datatype_t const  LDNDC_UINT8 = LDNDC_ATOMIC_DATATYPE_UINT8;
atomic_datatype_t const  LDNDC_INT16 = LDNDC_ATOMIC_DATATYPE_INT16;
atomic_datatype_t const  LDNDC_UINT16 = LDNDC_ATOMIC_DATATYPE_UINT16;
atomic_datatype_t const  LDNDC_INT32 = LDNDC_ATOMIC_DATATYPE_INT32;
atomic_datatype_t const  LDNDC_UINT32 = LDNDC_ATOMIC_DATATYPE_UINT32;
atomic_datatype_t const  LDNDC_INT64 = LDNDC_ATOMIC_DATATYPE_INT64;
atomic_datatype_t const  LDNDC_UINT64 = LDNDC_ATOMIC_DATATYPE_UINT64;

atomic_datatype_t const  LDNDC_FLOAT32 = LDNDC_ATOMIC_DATATYPE_FLOAT32;
atomic_datatype_t const  LDNDC_FLOAT64 = LDNDC_ATOMIC_DATATYPE_FLOAT64;
#ifdef  LDNDC_HAVE_FLOAT128
atomic_datatype_t const  LDNDC_FLOAT128 = LDNDC_ATOMIC_DATATYPE_FLOAT128;
#endif

atomic_datatype_t const  LDNDC_CHAR = LDNDC_ATOMIC_DATATYPE_CHAR;
atomic_datatype_t const  LDNDC_BYTE = LDNDC_ATOMIC_DATATYPE_CHAR;
atomic_datatype_t const  LDNDC_STRING = LDNDC_ATOMIC_DATATYPE_STRING;

atomic_datatype_t const  LDNDC_COMPOUND = LDNDC_ATOMIC_DATATYPE_COMPOUND;
atomic_datatype_t const  LDNDC_RAW = LDNDC_ATOMIC_DATATYPE_RAW;

atomic_datatype_t const  LDNDC_DTYPE_INVALID = LDNDC_ATOMIC_DATATYPE_INVALID;
atomic_datatype_t const  LDNDC_DTYPE_NONE = LDNDC_ATOMIC_DATATYPE_NONE;

char const *  LDNDC_ATOMIC_DATATYPE_NAMES[LDNDC_ATOMIC_DATATYPE_CNT] =
{
    "none",
    "bool",

    "int8",
    "uint8",
    "int16",
    "uint16",
    "int32",
    "uint32",
    "int64",
    "uint64",

    "float32",
    "float64",
#ifdef  LDNDC_HAVE_FLOAT128
    "float128",
#endif

    "char",
    "string",

    "pointer",
    "raw"
};
static const size_t  ldndc_atomic_datatype_sizes[LDNDC_ATOMIC_DATATYPE_CNT] =
{
    0, /*dummy*/
    sizeof( ldndc_bool_t),

    sizeof( ldndc_int8_t),
    sizeof( ldndc_uint8_t),
    sizeof( ldndc_int16_t),
    sizeof( ldndc_uint16_t),
    sizeof( ldndc_int32_t),
    sizeof( ldndc_uint32_t),
    sizeof( ldndc_int64_t),
    sizeof( ldndc_uint64_t),

    sizeof( ldndc_flt32_t),
    sizeof( ldndc_flt64_t),
#ifdef  LDNDC_HAVE_FLOAT128
    sizeof( ldndc_flt128_t),
#endif

    sizeof( ldndc_char_t),
    sizeof( ldndc_string_t),

    sizeof( char*),
    (size_t)-1
};
size_t
get_atomic_datatype_size(
        atomic_datatype_t  _datatype)
{
    if ( LDNDC_ATOMIC_DATATYPE_NONE < _datatype)
    {
        return  ldndc_atomic_datatype_sizes[_datatype];
    }
    return  0;
}

