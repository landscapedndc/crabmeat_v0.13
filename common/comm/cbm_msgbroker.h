/*!
 * @brief
 *    message delivery
 *
 * @author 
 *    steffen klatt (created on: nov 28, 2014)
 */

#ifndef  CBM_MSGBROKER_H_
#define  CBM_MSGBROKER_H_

#include  "crabmeat-common.h"
#include  "comm/cbm_uri.h"
#include  "comm/cbm_msg.h"
#include  "comm/cbm_msghandler.h"

#include  "string/cbm_string.h"

#include  <list>
#include  <vector>

#define  MESSAGEBROKER_MAX_RECIPIENTS  100000u

namespace  cbm {
class CBM_API msgbroker_t
{
    struct  recipient_t
    {
        cbm::uri_t  comp;
		lid_t  r_id;
        void *  compobj;
        cbm::message_handler_function_t  callback;
    };
    public:
        msgbroker_t();
        ~msgbroker_t();

    public:
        lid_t  register_message_handler(
                cbm::string_t const & /*uri*/,
                cbm::message_handler_function_t /*callback*/, void * /*object*/);
        void  deregister_message_handler(
                lid_t const & /*internal id*/);

        lerr_t  send_message(
                cbm::msg_t * /*message*/, cbm::reply_t * = NULL);
        lerr_t  bcast_message(
                cbm::msg_t * /*message*/, int * /*#receivers*/);

    private:
        std::list< cbm::msg_t >  undelivered_messages;
        std::vector< recipient_t >  recipients;

        lerr_t  send_message(
                cbm::msg_t * /*message*/, cbm::reply_t * /*reply*/, int * /*#receivers*/);
        lerr_t  send_message_to(
                cbm::msg_t * /*message*/, cbm::reply_t * /*reply*/, lid_t const & /*receiver id*/);
        lid_t  find_recipient_by_path(
                cbm::string_t const & /*uri*/);
        lerr_t  send_undelivered_messages(
                cbm::uri_t const *);
};

extern CBM_API msgbroker_t *  kmessage_broker;

} /* namespace cbm */

#endif  /* !CBM_MSGBROKER_H_ */

