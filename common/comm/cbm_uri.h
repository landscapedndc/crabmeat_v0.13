/*!
 * @brief
 *    URI (uniform resource identifier)
 *
 *    example:
 *      cbm://hoeglwald/volume/PinusSylvestris?unit=m^3&interpolate=linear#corwood
 *
 * @author 
 *    steffen klatt (created on: nov 25, 2014)
 *
 * @todo
 *    merge with cbm::source_descriptor_t
 */

#ifndef  CBM_URI_H_
#define  CBM_URI_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

namespace cbm {
class CBM_API uri_t
{
    public:
        static cbm::string_t  make_uri(
                char const * /*uri*/, lid_t const & /*id*/);

        enum  parts_e
        {   URI_SCHEME = 0x1,
            URI_PATH = 0x2,
            URI_QUERY = 0x4,
            URI_FRAGMENT = 0x8,
       
            URI_ALLPARTS = URI_SCHEME|URI_PATH|URI_QUERY|URI_FRAGMENT };

    public:
        uri_t( char const * /*uri*/ = NULL);
        uri_t( cbm::string_t const & /*uri*/);
        uri_t( uri_t const & /*uri*/);
        uri_t &  operator=( uri_t const & /*uri*/);

        ~uri_t();

        bool  is_valid() const
            { return  this->m_path != NULL; }

        cbm::string_t  uri( int /*parts*/ = URI_ALLPARTS) const;
        cbm::string_t  str(
                    int  _parts = URI_ALLPARTS) const
            { return  this->uri( _parts); }
        char const *  scheme() const
            { return  this->m_scheme; }
        cbm::string_t  path_stripped() const;
        size_t  path_length() const;
        size_t  path( char * /*buffer*/, size_t /*buffer size*/) const;
        char const *  subpath( int /*index*/) const;
        cbm::string_t  query() const;
        char const *  subquery( int /*index*/) const;
        char const *  query( char const * /*key*/, char const ** = NULL /*query*/) const;
        char const *  fragment() const
            { return  this->m_fragment; }

        int  drop_headpaths( int /*count*/);

        typedef  ldndc_uint64_t  hash_t;
        hash_t  hashify() const;

        uri_t  new_child( char const *  _path,
                    char const *  _query = NULL)
        {
            cbm::string_t  new_uri =
                this->uri( uri_t::URI_SCHEME|uri_t::URI_PATH);
            new_uri += '/';
            new_uri += _path;
            if ( _query)
            {
                new_uri += '?';
                new_uri += _query;
            }
            return  uri_t( new_uri.c_str());
        }

        bool  operator==( uri_t const &  _rhs) const
            { return  this->str() == _rhs.str(); }
        bool  operator!=( uri_t const &  _rhs) const
            { return  ! operator==( _rhs); }

    private:
        int  parse( char const * /*uri*/);

    private:
		char *  m_scheme;
		char *  m_path;
        char *  m_path_end;
		char *  m_query;
		char *  m_query_end;
		char *  m_fragment;
};
} /* namespace cbm */

#endif  /* !CBM_URI_H_ */

