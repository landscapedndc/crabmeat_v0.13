/*!
 * @brief
 *    message delivery
 *
 * @author 
 *    steffen klatt (created on: nov 28, 2014)
 */

#include  "comm/cbm_msgbroker.h"
#include  "regex/cbm_regex.h"
#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

cbm::msgbroker_t::msgbroker_t()
    { }
cbm::msgbroker_t::~msgbroker_t()
    { }

lid_t
cbm::msgbroker_t::register_message_handler(
        cbm::string_t const &  _uri,
        cbm::message_handler_function_t  _callback, void *  _compobj)
{
    CBM_LogDebug( "register_message_handler():", " \"",_uri,"\"");
    if ( this->recipients.size() > MESSAGEBROKER_MAX_RECIPIENTS)
    {
        CBM_LogFatal( "Too many clients for message broker",
                  "    [#clients=",this->recipients.size(),"]");
        return  invalid_lid;
    }

    lid_t  new_id = this->recipients.size();
    for ( size_t  r = 0;  r < new_id;  ++r)
    {
        recipient_t *  R = &this->recipients[r];
        if ( !R->comp.is_valid())
        {
            R->comp = cbm::uri_t( _uri);
			R->r_id = r;
            R->compobj = _compobj;
            R->callback = _callback;
            return  r;
        }
        else if ( R->comp.uri() == _uri)
        {
            R->compobj = _compobj;
            R->callback = _callback;

            crabmeat_assert(( R->r_id == r) && R->compobj);
            return  r;
        }
        else
        {
            /* fall through */
        }
    }

    recipient_t  R;
    R.comp = cbm::uri_t( _uri);
	R.r_id = new_id;
    R.compobj = _compobj;
    R.callback = _callback;
    this->recipients.push_back( R);

    /* deliver messages arriving for this recipient
     * before its registration */
    this->send_undelivered_messages( &R.comp);

    CBM_LogDebug( "registering message handler for \"",_uri,"\", giving ID ",new_id);
    return  new_id;
}

void
cbm::msgbroker_t::deregister_message_handler(
        lid_t const &  _id)
{
    if ( this->recipients.size() > _id)
    {
        this->recipients[_id].comp = cbm::uri_t( "");
        this->recipients[_id].compobj = NULL;
        this->recipients[_id].callback = NULL;
    }
}

lerr_t
cbm::msgbroker_t::send_message(
        cbm::msg_t *  _message, cbm::reply_t *  _reply)
{
    return  this->send_message( _message, _reply, NULL);
}
lerr_t
cbm::msgbroker_t::bcast_message(
        cbm::msg_t *  _message, int *  _n_receivers)
{
    _message->flags |= MSG_BCAST;
    _message->resendcnt = 0;

    lerr_t  rc = LDNDC_ERR_OK;
    cbm::reply_t  bcast_reply;

    int  recv_ok = 1;
    while ( recv_ok)
    {
        _message->resendcnt += 1;
        bcast_reply.resend = 0;

        lerr_t const  rc_recv =
            this->send_message( _message, &bcast_reply, _n_receivers);
        if ( rc_recv)
        {
            rc = LDNDC_ERR_FAIL;
            recv_ok = 0;
        }
        else if ( bcast_reply.status != 0)
        {
            rc = LDNDC_ERR_RUNTIME_ERROR;
            recv_ok = 0;
        }
        else if ( bcast_reply.resend == 0)
        {
            rc = LDNDC_ERR_OK;
            recv_ok = 0;
        }
        else
        {
            /* no op */
        }
    }

    return  rc;
}

lerr_t
cbm::msgbroker_t::send_message(
        cbm::msg_t *  _message, cbm::reply_t *  _reply, int * _n_receivers)
{
    if ( !_message)
    {
        CBM_LogError( "Message must not be NULL",
                  "  [sender=",((_message->sender)?_message->sender->uri():"null","]"));
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
// sk:dbg    CBM_LogDebug( "message broker received message from \"",
// sk:dbg            _message->sender.uri(),"\" for \"",_message->receiver.uri(),"\"");

// sk:todo  bottleneck
    if ( _message->flags & MSG_BCAST)
    {
// sk:dbg        CBM_LogVerbose( "broadcasting message ... ");
        int  n_resends = 0, n_errors = 0;
        int  n_receivers = 0;
        cbm::string_t const  R_regex = _message->receiver.path_stripped();
        for ( size_t  r = 0;  r < this->recipients.size();  ++r)
        {
            recipient_t *  R = &this->recipients[r];
            cbm::string_t const  R_name = R->comp.path_stripped();
            size_t const  n_R_name = R_name.length();
// sk:dbg            CBM_LogDebug( "checking recipient for match: ",R_name, "  #=",n_R_name, " {",R_regex,"}  |m|=",
// sk:dbg                    cbm::regex_match( R_regex.c_str(), R_name.c_str(), n_R_name, NULL, 0));
// sk:??            cbm::string_t const  S_name = _message->sender.path_stripped();
            if ( /* sk:??  !cbm::is_equal( S_name.c_str(), R_name.c_str())
                && */ cbm::regex_match( R_regex.c_str(), R_name.c_str(), n_R_name, NULL, 0)
                    == static_cast< int >( n_R_name))
            {
                /* TODO  error handling */
// sk:dbg                CBM_LogDebug( "recipient matched  [path=",R_name,"]");
                this->send_message_to( _message, _reply, static_cast< lid_t>( r));
                if ( _reply)
                {
                    if ( _reply->resend)
                        { ++n_resends; }
                    if ( _reply->status != 0)
                        { ++n_errors; }
                }
                ++n_receivers;
            }
            else
            {
// sk:dbg                CBM_LogDebug( "no match  [path=",R_name,"]");
            }
        }
        if ( _n_receivers) { *_n_receivers = n_receivers; }
        if ( _reply)
        {
            _reply->resend = n_resends ? 1 : 0;
            _reply->status = n_errors ? 1 : 0;
        }
    }
    else
    {
        lid_t const  receiver_id =
            this->find_recipient_by_path( _message->receiver.path_stripped());
        if (( receiver_id == invalid_lid)
                || !this->recipients[receiver_id].comp.is_valid())
        {
            CBM_LogError( "Sender not registered with message broker",
                    "  [sender=",((_message->sender)?_message->sender->uri():"null","]"));
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        this->send_message_to( _message, _reply, receiver_id);
    }

    return  _message->status ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

lerr_t
cbm::msgbroker_t::send_message_to(
		cbm::msg_t *  _message, cbm::reply_t *  _reply, lid_t const &  _receiver_id)
{
    if ( cbm::is_valid( _receiver_id))
    {
        recipient_t *  R = &this->recipients[_receiver_id];
        if ( R->callback)
        {
            _message->status =
                (*R->callback)( R->compobj, _message, _reply);
        }
// sk:wtf        if ( _message->flags & MSG_ACK)
// sk:wtf        {
// sk:wtf            _message->flags &= ~(int)MSG_ACK;
// sk:wtf            lid_t const  sender_id = _message->sender.id();
// sk:wtf            recipient_t *  S = &this->recipients[sender_id];
// sk:wtf            if ( S->callback)
// sk:wtf            {
// sk:wtf                _message->status =
// sk:wtf                    (*S->callback)( S->compobj, _message, _reply);
// sk:wtf            }
// sk:wtf        }
    }
    else
    {
        /* receiver not yet registered */
        this->undelivered_messages.push_back( *_message);
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    return  _message->status ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

lid_t
cbm::msgbroker_t::find_recipient_by_path(
        cbm::string_t const &  _path)
{
    for ( size_t  r = 0;  r < this->recipients.size();  ++r)
    {
        recipient_t *  R = &this->recipients[r];
// sk:dbg        CBM_LogDebug( "checking recipient \"",R->comp.path_stripped(),"\"  [",_path,"]");
        if ( R->comp.is_valid() &&
                ( R->comp.path_stripped() == _path))
        {
            return  static_cast< lid_t >( r);
        }
    }
    return  invalid_lid;
}

lerr_t
cbm::msgbroker_t::send_undelivered_messages(
        cbm::uri_t const *  _receiver)
{
    CBM_LogVerbose( "checking for undelivered messages for \"",_receiver->uri(),"\"");
    lerr_t  rc = LDNDC_ERR_OK;

    std::list< cbm::msg_t >::iterator  msg = this->undelivered_messages.begin();
    while ( msg != this->undelivered_messages.end())
    {
        if ( msg->receiver.path_stripped() == _receiver->path_stripped())
        {
            lerr_t  rc_send = this->send_message( &(*msg));
            if ( rc_send)
            {
/* FIXME  continuing may be a bad idea .. "out-of-order" delivery */
                rc = LDNDC_ERR_FAIL;
            }
            msg = this->undelivered_messages.erase( msg);
        }
        else
        {
            ++msg;
        }
    }
    return  rc;
}


/* global message broker */
static cbm::msgbroker_t  __kmessage_broker;
cbm::msgbroker_t *  cbm::kmessage_broker = &__kmessage_broker;

