/*!
 * @brief
 *    [OBSOLETE]
 *    inter-kernel messages
 *
 * @author 
 *    steffen klatt (created on: nov 25, 2014)
 */

#ifndef  CBM_MSG_H_
#define  CBM_MSG_H_

#include  "crabmeat-common.h"
#include  "comm/cbm_uri.h"

namespace cbm
{

enum  msg_flag_e
{
    MSG_ACK    = 0x1,
    MSG_BCAST    = 0x2
};

struct CBM_API msg_t
{
    msg_t()
        : sender( NULL), timestamp( 0), flags( 0),
        status( 0), resendcnt( 0) { }

    cbm::uri_t *  sender;
    cbm::uri_t  receiver;

    unsigned int  timestamp;

    cbm::string_t  command;
    cbm::string_t  data;
    int  flags;

    int  status; /* { 0: ok; else: error } */
    unsigned int  resendcnt;
};
struct CBM_API request_t
{
    cbm::string_t  command;
    cbm::uri_t  uri;
    cbm::string_t  data;

    int  status;

// sk:todo    lprotocol_e  protocol() const
// sk:todo    {
// sk:todo        return  this->uri.protocol();
// sk:todo    }

    request_t  subrequest() const
    {
        request_t  req = *this;
        int  rc_drop = req.uri.drop_headpaths( 1);
        if ( rc_drop) { req.status = -1; }
        return  req;
    }
    cbm::string_t  get_uri()
    const 
    { return  this->uri.uri(); }
};
struct CBM_API reply_t
{
    reply_t()
        : status( 0), resend( 0), more( NULL) { }

    cbm::string_t  data;
    int  status; /* { 0: ok; else: error } */
    int  resend; /* { 0: no; else: yes }*/
    struct reply_t *  more;
};

} /* namespace cbm */

#endif  /* !CBM_MSG_H_ */

