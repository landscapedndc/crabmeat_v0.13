/*!
 * @brief
 *    message handler callback
 *
 * @author 
 *    steffen klatt (created on: feb 14, 2015)
 */

#ifndef  CBM_MSGHANDLER_H_
#define  CBM_MSGHANDLER_H_

#include  "crabmeat-common.h"

namespace cbm
{

struct  msg_t;
struct  reply_t;
typedef  int (*message_handler_function_t)( void *, cbm::msg_t const * , cbm::reply_t *);

} /* namespace cbm */

#endif  /* !CBM_MSGHANDLER_H_ */

