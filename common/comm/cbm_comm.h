/*!
 * @brief
 *    convenience header pulling all "communication" headers
 *
 * @author 
 *    steffen klatt (created on: jan 04, 2017)
 */

#ifndef  CBM_COMM_H_
#define  CBM_COMM_H_

#include  "comm/cbm_uri.h"
#include  "comm/cbm_msg.h"
#include  "comm/cbm_msgbroker.h"

#endif /* !CBM_COMM_H_ */

