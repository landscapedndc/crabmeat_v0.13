/*!
 * @brief
 *    URI
 *
 * @author 
 *    steffen klatt (created on: apr 05, 2015)
 */

#include  "comm/cbm_uri.h"
#include  "utils/cbm_utils.h"

#include  "hash/cbm_hash.h"
#include  "log/cbm_baselog.h"

cbm::string_t
cbm::uri_t::make_uri(
        char const *  _uri, lid_t const &  _id)
{
    cbm::string_t  uri = "/";
    uri << _uri;
    uri << '.';
    uri << _id;
    return  uri;
}

cbm::uri_t::uri_t( char const *  _uri)
    : m_scheme( NULL), m_path( NULL), m_path_end( NULL),
    m_query( NULL), m_query_end( NULL), m_fragment( NULL)
{
    if ( _uri)
        { this->parse( _uri); }
}
cbm::uri_t::uri_t( cbm::string_t const &  _uri)
    : m_scheme( NULL), m_path( NULL), m_path_end( NULL),
    m_query( NULL), m_query_end( NULL), m_fragment( NULL)
{
    if ( _uri.c_str())
        { this->parse( _uri.c_str()); }
}

cbm::uri_t::uri_t(
        cbm::uri_t const &  _rhs)
    : m_scheme( NULL), m_path( NULL), m_path_end( NULL),
      m_query( NULL), m_query_end( NULL), m_fragment( NULL)
{
// sk:dbg    CBM_LogError( "== copy constructor  >> =================================================");
// sk:dbg    CBM_LogError( "c:uri=", _rhs.uri());
// sk:dbg    CBM_LogError( "-------------------------------------------------------------------------");
    if ( _rhs.m_scheme)
    {
        this->m_scheme = cbm::strdup( _rhs.m_scheme);
// sk:dbg		CBM_LogError( "c:scheme='", this->m_scheme, "' # '", _rhs.m_scheme, "'");
    }
    if ( _rhs.m_path)
    {
        this->m_path = cbm::mem_dup(
                _rhs.m_path, _rhs.m_path_end - _rhs.m_path + 1);
        this->m_path_end =
           this->m_path + ( _rhs.m_path_end - _rhs.m_path);
// sk:dbg        int i = 0;
// sk:dbg        while ( this->subpath( i))
// sk:dbg        {
// sk:dbg            CBM_LogError( "c:[",i,"] subpath='", this->subpath( i), "' # '", _rhs.subpath(i),
// sk:dbg                "' |p-end|=",this->m_path_end-this->m_path, " # ", _rhs.m_path_end - _rhs.m_path);
// sk:dbg            ++i;
// sk:dbg        }
    }
    if ( _rhs.m_query)
    {
        this->m_query = cbm::mem_dup(
                _rhs.m_query, _rhs.m_query_end - _rhs.m_query + 1);
        this->m_query_end =
            this->m_query + ( _rhs.m_query_end - _rhs.m_query);
// sk:dbg        int i = 0;
// sk:dbg        while ( this->subquery( i))
// sk:dbg        {
// sk:dbg            CBM_LogError( "c:[",i,"] subquery='", this->subquery(i), "' # '", _rhs.subquery(i),
// sk:dbg                "' |q-end|=",this->m_query_end-this->m_query, " # ", _rhs.m_query_end - _rhs.m_query);
// sk:dbg            ++i;
// sk:dbg        }
    }
    if ( _rhs.m_fragment)
    {
        this->m_fragment = cbm::strdup( _rhs.m_fragment);
// sk:dbg		CBM_LogError( "c:fragment='", this->m_fragment, "' # '", _rhs.m_fragment,"'");
    }
// sk:dbg    CBM_LogError( "================================================ <<  copy constructor ==");
}

cbm::uri_t &
cbm::uri_t::operator=(
        cbm::uri_t const &  _rhs)
{
    if ( &_rhs == this)
    {
        return  *this;
    }

// sk:dbg    CBM_LogError( "=:uri=", _rhs.uri());
    if ( this->m_scheme) { free( this->m_scheme); }
    if ( _rhs.m_scheme)
    {
        this->m_scheme = cbm::strdup( _rhs.m_scheme);
// sk:dbg		CBM_LogError( "=:scheme='", this->m_scheme, "' # '", _rhs.m_scheme, "'");
    }
    else
    {
        this->m_scheme = NULL;
    }
    if ( this->m_path) { free( this->m_path); }
    if ( _rhs.m_path)
    {
        this->m_path = cbm::mem_dup(
                _rhs.m_path, _rhs.m_path_end - _rhs.m_path + 1);
        this->m_path_end =
            this->m_path + ( _rhs.m_path_end - _rhs.m_path);
// sk:dbg        int i = 0;
// sk:dbg        while ( this->subpath( i))
// sk:dbg        {
// sk:dbg            CBM_LogError( "=:[",i,"] subpath='", this->subpath( i), "' # '", _rhs.subpath(i),
// sk:dbg                "' |p-end|=",this->m_path_end-this->m_path, " # ", _rhs.m_path_end - _rhs.m_path);
// sk:dbg            ++i;
// sk:dbg        }
    }
    else
    {
        this->m_path = NULL;
        this->m_path_end = NULL;
    }
    if ( this->m_query) { free( this->m_query); }
    if ( _rhs.m_query)
    {
        this->m_query = cbm::mem_dup(
                _rhs.m_query, _rhs.m_query_end - _rhs.m_query + 1);
        this->m_query_end =
			this->m_query + ( _rhs.m_query_end - _rhs.m_query);
// sk:dbg        int i = 0;
// sk:dbg        while ( this->subquery( i))
// sk:dbg        {
// sk:dbg            CBM_LogError( "=:[",i,"] subquery=", this->subquery(i), " # ", _rhs.subquery(i),
// sk:dbg                " |q-end|=",this->m_query_end-this->m_query, " # ", _rhs.m_query_end - _rhs.m_query);
// sk:dbg            ++i;
// sk:dbg        }
    }
    else
    {
        this->m_query = NULL;
        this->m_query_end = NULL;
    }
    if ( this->m_fragment) { free( this->m_fragment); }
    if ( _rhs.m_fragment)
    {
        this->m_fragment = cbm::strdup( _rhs.m_fragment);
// sk:dbg		CBM_LogError( "=:fragment='", this->m_fragment, "' # '", _rhs.m_fragment, "'");
    }
    else
    {
        this->m_fragment = NULL;
    }

    return  *this;
}

cbm::uri_t::~uri_t()
{
    if ( this->m_scheme)
    {
        free( this->m_scheme);
        this->m_scheme = NULL;
    }
    if ( this->m_path)
    {
        free( this->m_path);
        this->m_path = NULL;
        this->m_path_end = NULL;
    }
    if ( this->m_query)
    {
        free( this->m_query);
        this->m_query = NULL;
        this->m_query_end = NULL;
    }
    if ( this->m_fragment)
    {
        free( this->m_fragment);
        this->m_fragment = NULL;
    }
}

cbm::string_t
cbm::uri_t::uri( int  _parts)
const
{
    cbm::string_t  uri_uri;
    if ( this->m_scheme && ( _parts & URI_SCHEME))
    {
        uri_uri += this->m_scheme;
        uri_uri += "://";
    }
    if ( this->m_path && ( _parts & URI_PATH))
    {
        if ( !this->m_scheme || ( ! (_parts & URI_SCHEME)))
        {
            uri_uri += '/';
        }
        uri_uri += this->path_stripped();
    }
    if ( this->m_query && ( _parts & URI_QUERY))
    {
		uri_uri += '?';
		uri_uri += this->query();
    }
    if ( this->m_fragment && ( _parts & URI_FRAGMENT))
    {
        uri_uri += '#';
        uri_uri += this->m_fragment;
    }

    return  uri_uri;
}
cbm::string_t
cbm::uri_t::path_stripped()
const
{
    if ( this->m_path)
    {
        cbm::string_t const  uri_path(
                this->m_path, this->m_path_end-this->m_path, "\0", "/", 1);
// sk:dbg		CBM_LogDebug( "uri-path=", uri_path,
// sk:dbg				"  |path|=",this->m_path_end-this->m_path);
        return  uri_path;
    }
    return  cbm::string_t();
}
size_t
cbm::uri_t::path_length() const
    { return  1 + this->m_path_end - this->m_path; }
size_t
cbm::uri_t::path(
    char *  _path, size_t  _path_sz) const
{
    CBM_Assert(_path);
    CBM_Assert(_path_sz>1);

    if ( _path_sz == 0)
        { /* no op */ }
    else if ( _path_sz == 1)
        { *_path = '\0'; }
    else
    {
        *_path = '/';
        char *  d = _path+1;
        char const *  p = this->m_path;
        char const *  pe = this->m_path_end;
        char const *  e = std::min( p+_path_sz-2, pe);
        while ( p < e)
        {
            *d++ = ( *p=='\0') ? '/' : *p;
            ++p;
        }
        *d = '\0';
    }
    return  this->path_length();
}
static char const *
uri_subitem_s(
        char const *  _itemlist, char const *  _itemlist_end, int  _index)
{
    if ( _index == 0)
    {
        return  _itemlist;
    }
    else if ( _index > 0)
    {
        char const *  p = _itemlist;
		int  i = 0;
        while ( p < _itemlist_end)
        {
            if (( *p++ == '\0') && ( ++i == _index))
                { return  p; }
        }
    }
    return  NULL;
}
char const *
cbm::uri_t::subpath( int  _index)
const
{
    return  uri_subitem_s(
        this->m_path, this->m_path_end, _index);
}

cbm::string_t
cbm::uri_t::query()
const
{
    if ( this->m_query)
    {
        cbm::string_t const  uri_queries(
                this->m_query, this->m_query_end-this->m_query, "\0", "&", 1);
// sk:dbg		CBM_LogDebug( "uri-query=", uri_queries,
// sk:dbg				"  |query|=",this->m_query_end-this->m_query);
        return  uri_queries;
    }
    return  cbm::string_t();
}
char const *
cbm::uri_t::subquery( int  _index)
const
{
    return  uri_subitem_s(
        this->m_query, this->m_query_end, _index);
}
char const *
cbm::uri_t::query( char const *  _key, char const **  _query)
const
{
    if ( !this->m_query)
        { return  NULL; }

    size_t const  key_len = cbm::strlen( _key);
    char const *  q = this->m_query;
    while ( q <= this->m_query_end)
    {
        if ( cbm::has_prefix( q, _key)
                && ( q[key_len] == '='))
        {
            if ( _query)
                { *_query = q; }
            return  q+key_len+1; }

        while ( *q++ != '\0') { }
    }
    return  NULL;
}

int
cbm::uri_t::drop_headpaths(
        int  _count)
{
    char const *  p = this->subpath( _count);
    if ( p)
    {
        char *  new_path =
            cbm::mem_dup( p, this->m_path_end - p + 1);
        free( this->m_path);
        this->m_path = new_path;
        this->m_path_end =
            new_path + ( this->m_path_end - p);

        return  _count;
    }
    return  -1;
}


    cbm::uri_t::hash_t
cbm::uri_t::hashify()
const
{
    cbm::string_t const  m_uri = this->uri();
    return  cbm::hash::sha1_uint64(
            m_uri.c_str(), m_uri.size());
}

/* somewhat borrowed from apr_uri.c (the Apache project) */
int
cbm::uri_t::parse( char const *  _uri)
{
    if ( !_uri)
        { return  -1; }

    char const *  u0 = _uri;
    char const *  u = u0;

    if ( *u == '/')
    {
luri_parse_path:
        char const *  up = u+1;
        /* at path */
        while ( *u != '\0' && *u != '?' && *u != '#')
        {
            ++u;
        }
        if ( u != up)
        {
            this->m_path = cbm::strndup( up, u-up);
        }
        if ( *u == '\0')
        {
            /* path only */
        }
        else if ( *u == '?')
        {
            ++u;
            char const *  uf = strchr( u, '#');
            if ( uf)
            {
                this->m_fragment = cbm::strdup( uf+1);
                this->m_query = cbm::strndup( u, uf-u);
            }
            else
            {
                this->m_query = cbm::strdup( u);
            }
        }
        else
        {
            this->m_fragment = cbm::strdup( u+1);
        }
    }
    else
    {
        while ( *u != '\0' && *u != ':')
        {
            ++u;
        }
        if ( *u == ':' && u != u0)
        {
            if ( u[1] != '\0' && u[1] == '/' && u[2] != '\0' && u[2] == '/')
            {
                this->m_scheme = cbm::strndup( u0, u-u0);
                u += 2;
                goto  luri_parse_path;
            }
        }
        else
            { return  -1; }
    }

// sk:dbg    CBM_LogDebug( "scheme='",this->m_scheme?this->m_scheme:"null",
// sk:dbg            "'  path='",this->m_path?this->m_path:"null",
// sk:dbg            "'  query='",this->m_query?this->m_query:"null",
// sk:dbg            "'  fragment='",this->m_fragment?this->m_fragment:"null","'");
    /* replace path separators '/' by NUL character '\0' */
    if ( this->m_path)
    {
        char *  p = this->m_path;
        while ( *p != '\0')
        {
            if ( *p == '/')
            {
                *p = '\0';
            }
            ++p;
        }
        this->m_path_end = p;
    }
    if ( this->m_query)
    {
        char *  q = this->m_query;
        while ( *q != '\0')
        {
            if ( *q == '&')
            {
                *q = '\0';
            }
            ++q;
        }
        this->m_query_end = q;
    }
    return  0;
}

