/*!
 * @brief
 *    The simulation clock
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#ifndef  CBM_CLOCK_H_
#define  CBM_CLOCK_H_

#include  "crabmeat-common.h"
#include  "time/cbm_date.h"

#include  <list>

namespace cbm {

class sclock_t;
struct CBM_API sclock_callees_t
{
    typedef  int (*callback_t)(
        void * /*object*/, cbm::sclock_t const * /*clock*/);
    struct callee_t
    {
        void *  obj;
        callback_t  func;
    };

    typedef  std::list< callee_t >::iterator  iterator_t;
    iterator_t  begin()
        { return  this->callees.begin(); }
    iterator_t  end()
        { return  this->callees.end(); }
    void  insert( callee_t const &  _callee)
        { this->callees.push_back( _callee); }
    void  remove( iterator_t  _callee)
        { this->callees.erase( _callee); }
    void  clear()
        { this->callees.clear(); }
    std::list< callee_t >  callees;
};

class CBM_API sclock_t
{
    public:
        typedef  sclock_callees_t::callback_t  callback_t;

    public:
        sclock_t();
        sclock_t( ltime_t const &);

        /*! checks whether timer has reached or exceeded simulation end */
        inline bool  run() const
            { return  !this->is_final(); }

        /*! return last time step */
        inline cbm::td_scalar_t  sse_final() const
            { return  this->m_final; }
        /*! check if last or post last time step */
        inline bool  is_final() const
            { return  this->sse_final() < this->sse(); }

        /*! reset internal timer to beginning */
        void  reset();
        /*! add smallest time step unit to current time (updates position) */
        int  advance();
        int  advance( cbm::td_scalar_t /*dT*/);

        /*! or'd flags of time positions, i.e. day boundary, year boundary, etc... */
        inline unsigned int  position() const
            { return  this->m_tpos; }
        std::string  position_names(
            unsigned int = TMODE_NONE /*timemodes*/) const;
        inline bool  is_position( timemode_e  _p) const
            { return  ( this->m_tpos & _p) != 0; }

        ltime_t const &  schedule() const
            { return  this->m_date0; }
        ltime_t const &  fromto() const /*alias for schedule*/
            { return  this->schedule(); }
        ltime_t  time() const
            { return  ltime_t( this->now(), this->m_date0.to()); }
        ldate_t  now() const
            { return  ldate_t( this->year(), this->month(),
                this->day(), this->subday(),
                    this->time_resolution()); }

        /*! return clocks time resolution */
        inline ldate_t::tr_scalar_t  time_resolution() const
            { return  this->m_date0.from().time_resolution(); }
        inline double  day_fraction() const
            { return  this->m_date0.from().day_fraction(); }
        /*! return current time delta in time components */
// TODO        inline cbm::tp_t  dT() const
// TODO            { return  this->m_tp; }
    /*TRICKY*/ /*! return current time delta in seconds */
        inline ldate_t::td_scalar_t  dt() const
            { return  this->m_dt; }

    /*DEPRECATED*/
        /*! return start subday */
        inline ldate_t::tm_scalar_t  subday_zero() const
            { return  this->m_date0.from().subday(); }
        /*! return current subday */
        inline ldate_t::tm_scalar_t  subday() const
            { return  1 + ( this->m_tm.tm_sec + this->m_tm.tm_min*60
                + this->m_tm.tm_hour*3600) / this->m_dt; }


        /*! return start second */
        inline ldate_t::tm_scalar_t  second_zero() const
            { return  this->m_date0.from().second(); }
        /*! return current second */
        inline ldate_t::tm_scalar_t  second() const
            { return  this->m_tm.tm_sec; }
        /*! return start minute */
        inline ldate_t::tm_scalar_t  minute_zero() const
            { return  this->m_date0.from().minute(); }
        /*! return current minute */
        inline ldate_t::tm_scalar_t  minute() const
            { return  this->m_tm.tm_min; }
        /*! return start hour */
        inline ldate_t::tm_scalar_t  hour_zero() const
            { return  this->m_date0.from().hour(); }
        /*! return current hour */
        inline ldate_t::tm_scalar_t  hour() const
            { return  this->m_tm.tm_hour; }

        /*! return start day of month */
        inline ldate_t::tm_scalar_t  day_zero() const
            { return  this->m_date0.from().day(); }
        /*! return current day of month */
        inline ldate_t::tm_scalar_t  day() const
            { return  this->m_tm.tm_day+1; }
        /*! return start month */
        inline ldate_t::tm_scalar_t  month_zero() const
            { return  this->m_date0.from().month(); }
        /*! return current month */
        inline ldate_t::tm_scalar_t  month() const
            { return  this->m_tm.tm_month+1; }
        /*! return start year */
        inline ldate_t::tm_scalar_t  year_zero() const
            { return  this->m_date0.from().year(); }
        /*! return current year */
        inline ldate_t::tm_scalar_t  year() const
            { return  this->m_tm.tm_year; }

        /*! return current day of year */
        inline ldate_t::tm_scalar_t  yearday_zero() const
            { return  this->m_date0.from().yearday(); }
        /*! return current day of year */
        inline ldate_t::tm_scalar_t  yearday() const
            { return  cbm::tm_yearday( &this->m_tm)+1; }


        /*! return number of cycles, i.e., calls to @fn advance() */
        inline ldate_t::tm_scalar_t  cycles() const
            { return  this->m_cycles; }
        /*! number of seconds the simulation is running */
        inline ldate_t::td_scalar_t  seconds() const
            { return this->m_tm.td_seconds
                        - this->m_date0.from().sse(); }
        /*! number of seconds the simulation runs */
        inline ldate_t::td_scalar_t  runtime() const
            { return  this->m_date0.diff( 1); }


        /*! currently in leap year ? */
        inline bool  is_leap_year() const
            { return  cbm::tm_isleapyear( &this->m_tm) != 0; }
        /*! convenience function */
        inline ldate_t::tm_scalar_t  days_in_month() const
            { return  cbm::tm_daysinmonth( &this->m_tm); }
        /*! convenience function */
        inline ldate_t::tm_scalar_t  days_in_year() const
            { return  cbm::tm_daysinyear( &this->m_tm); }

        /*! string representation of current clock time */
        std::string  to_string() const;

        /* return seconds since epoch for current time */
        inline ldate_t::td_scalar_t  seconds_since_epoch() const
            { return  this->m_tm.td_seconds; }
        inline ldate_t::td_scalar_t  sse() const
            { return  this->seconds_since_epoch(); }

    public:
        int  register_event( char const * /*event*/,
                    cbm::sclock_t::callback_t /*callback function*/,
                        void * /*object*/, char const * /*caller*/) const;
        int  unregister_event( char const * /*event*/,
                    cbm::sclock_t::callback_t /*callback function*/,
                        void * /*object*/, char const * /*caller*/) const;
    private:
        /* event: "enter-timestep" */
        mutable cbm::sclock_callees_t  on_enter_timestep;
        /* event: "leave-timestep" */
        mutable cbm::sclock_callees_t  on_leave_timestep;

        cbm::sclock_callees_t *  m_select_event_slot( char const * /*event*/) const;
        int  dispatch_on_event( cbm::sclock_callees_t * /*event slot*/,
                    bool = false /*if true, unregister all callbacks*/);

    private:
        ltime_t  m_date0;
        cbm::tm_t  m_tm;
// TODO        cbm::tp_t  m_tp;
        cbm::td_scalar_t  m_dt;

        /* caches the current time step position */
        unsigned int  m_tpos;
        /* keeps track of tic toc events */
        cbm::tm_scalar_t  m_cycles;
        /* flag that signals if final timestep has been reached */
        cbm::td_scalar_t  m_final;

    private:
        lerr_t  m_setclock( ltime_t const &);

        /* set current time position flags */
        void  m_update_timeposition();
};

} /* namespace cbm */

std::ostream CBM_API &
operator<<( std::ostream &, cbm::sclock_t const &);

#endif  /*  !CBM_CLOCK_H_  */

