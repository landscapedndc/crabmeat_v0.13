/*!
 * @brief
 *    wrapper for system time
 *
 * @author
 *    steffen klatt (created on: mar 07, 2013)
 */

#ifndef  CBM_LTIME_H_
#define  CBM_LTIME_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

#include  <time.h>

namespace  cbm
{
    lerr_t CBM_API current_time_as_string(
            std::string *, char const * = NULL);
}

#endif  /*  !CBM_LTIME_H_  */

