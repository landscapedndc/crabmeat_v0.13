/*!
 * @author
 *    steffen klatt (created on: jan 05, 2012)
 */

#include  "log/cbm_baselog.h"
#include  "time/cbm_date.h"
#include  "time/scanner.h"

ldate_scanner_t::ldate_scanner_t(
                ldate_attrib_t &  _ldate_attr)
        : ldate_scanner_FlexLexer(),
                  buf( _ldate_attr)
{
}


lerr_t
ldate_scanner_t::from_string(
                std::string const &  _t,
        ltime_t const *  _ref)
{
        return  this->from_string( _t.c_str(), _ref);
}

// this is actually the parser
lerr_t
ldate_scanner_t::from_string(
                char const *  _t,
        ltime_t const *  _ref)
{
    std::istringstream  d( _t);
    switch_streams( &d, NULL);

    lerr_t  rc_settime = LDNDC_ERR_FAIL;

    while ( 1)
    {
        switch ( yylex())
        {
            case  ltime_t::token::TIME_ERROR:
                {
                    CBM_LogError( "failed token=\"",this->YYText(),"\"");
                    rc_settime = LDNDC_ERR_FAIL;
                }
                break;

            case  ltime_t::token::TIME_EOF:
                {
                    /* in case no or incomplete "-> xxx" was given
                     * (NOTE  only works when "from" is read before "to"...)
                     */
                    this->buf.to = this->buf.to.merge( this->buf.from);
                }
                return  LDNDC_ERR_OK;

            case  ltime_t::token::TIME_FROM:
                {
                    rc_settime = this->set_time( this->buf.from,
                        this->buf.from_rel, this->YYText(), (( _ref) ? &_ref->from() : NULL));
                    this->buf.to_rel = this->buf.from_rel;
                }
                break;

            case  ltime_t::token::TIME_TO:
                if ( this->YYText()[0] == lperiod_t::PERIOD_TAG)
                {
                    rc_settime = this->set_time(
                        this->buf.to, this->buf.to_rel, this->YYText(), NULL);
                }
                else
                {
                    std::ostringstream  yyt;
                    yyt << this->YYText() << ldate_t::TR_SEP << this->buf.from.time_resolution();
                    rc_settime = this->set_time( this->buf.to,
                        this->buf.to_rel, yyt.str().c_str(), &this->buf.from);
                }
                break;

            default:
                CBM_LogError( "lexer: <default>, impossible to get here it is..  [text=",this->YYText(),"]");
                rc_settime = LDNDC_ERR_FAIL;
        }

        if ( rc_settime)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    /* should not get here */
}

#include  <stdexcept>
lerr_t
ldate_scanner_t::set_time(
        ldate_t &  _d, bool &  _rel, char const *  _s, ldate_t const *  _ref)
{
    lerr_t  rc = LDNDC_ERR_OK;

    if ( _s[0] == lperiod_t::PERIOD_TAG)
    {
        _d = this->buf.from + lperiod_t( _s);
        _rel = true;
    }
    else
    {
        try
        {
            _d = ldate_t( _s, ((_ref) ? *_ref : _d));
        }
        catch ( std::exception &  ldate_err)
        {
            CBM_LogError( "ldate_scanner_t::set_time: ", ldate_err.what());
            rc = LDNDC_ERR_FAIL;
        }
        _rel = false;
    }

    return  rc;
}

