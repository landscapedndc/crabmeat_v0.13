/*!
 * @brief
 *    convenience wrapper
 *
 * @author
 *    steffen klatt (created on: jan 06, 2012)
 */


#ifndef  LDNDC_TIME_SCANNER_WRAPPER_H_
#define  LDNDC_TIME_SCANNER_WRAPPER_H_

#ifdef  FlexLexer
#  error  oops, who defined FlexLexer?!
#endif
#define  FlexLexer  flexlexer
#ifndef  YY_NO_UNISTD_H
#  define  YY_NO_UNISTD_H
#endif
#include  "time/g_scanner.h"
#undef  FlexLexer


#endif /* LDNDC_TIME_SCANNER_WRAPPER_H_ */

