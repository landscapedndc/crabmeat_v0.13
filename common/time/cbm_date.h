/*!
 * @brief
 *    custom time period, time and timespan
 *    representation for simulation system.
 *
 * @author
 *    steffen klatt
 */

#ifndef  CBM_DATE_H_
#define  CBM_DATE_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "cbm_types.h"
#include  "cbm_errors.h"
#include  "cbm_flags.h"

#include  "math/cbm_math.h"
#include  "string/cbm_string.h"
#include  "time/timemodes.h"

#include  "constants/cbm_const.h"

#include  "time/cbm_cal.h"

#define  TRES_BITS  16
typedef  cbm::td_scalar_t  ldndc_timestamp_t;

class  ldate_t;
class  CBM_API  lperiod_t
{
    public:
        typedef  cbm::tp_scalar_t  tp_scalar_t;
        static  char const  PERIOD_TAG;
        static  char const *  PERIOD_TO;

    public:
        lperiod_t();
        lperiod_t( cbm::tp_t const &);
        lperiod_t( char const *);

        /* returns seconds */
        inline tp_scalar_t  seconds() const
            { return  this->m_tp.tp_sec; }
        inline bool  seconds_given() const
            { return  this->m_tp.tp_set.sec; }
        /* returns minutes */
        inline tp_scalar_t  minutes() const
            { return  this->m_tp.tp_min; }
        inline bool  minutes_given() const
            { return  this->m_tp.tp_set.min; }
        /* returns days */
        inline tp_scalar_t  hours() const
            { return  this->m_tp.tp_hour; }
        inline bool  hours_given() const
            { return  this->m_tp.tp_set.hour; }

        /* returns days */
        inline tp_scalar_t  days() const
            { return  this->m_tp.tp_day; }
        inline bool  days_given() const
            { return  this->m_tp.tp_set.day; }
        /* returns months */
        inline tp_scalar_t  months() const
            { return  this->m_tp.tp_month; }
        inline bool  months_given() const
            { return  this->m_tp.tp_set.month; }
        /* returns years */
        inline tp_scalar_t  years() const
            { return  this->m_tp.tp_year; }
        inline bool  years_given() const
            { return  this->m_tp.tp_set.year; }

    /* DEPRECATED */
        /* returns subdays */
        inline tp_scalar_t  subdays() const
            { return  this->m_tp.tr_fd; }
        inline bool  subdays_given() const
            { return  this->m_tp.tp_set.fd; }


        lperiod_t &  operator*=( int /*multiple*/);
        lperiod_t  operator*( int /*multiple*/) const;

        cbm::tp_t const &  as_tp() const
            { return  this->m_tp; }

        /* string representation */
        std::string  to_string() const;

    private:
        cbm::tp_t  m_tp;
};

struct CBM_API period_oneday
{
    typedef  lperiod_t period_t;
    static  period_t const &  one_day();
};
#define  lperiod_one_day period_oneday::one_day()


class  CBM_API  ldate_t
{
    public:
        /* time component separator */
        static  char const  TC_SEP;
        /* time component separator (time resolution) */
        static  char const  TR_SEP;

        /* time components type */
        typedef  cbm::tm_scalar_t  tm_scalar_t;
        /* time components type */
        typedef  cbm::tr_scalar_t  tr_scalar_t;
        /* time difference type */
        typedef  cbm::td_scalar_t  td_scalar_t;

        /*!
         * @brief
         *    flags to control some of ldate's behavior
         */
        enum
        {
            /*! none flag */
            LDATE_FLAG_NONE  = 0,
            /*!  */
            LDATE_FLAG_SHORT  = 1 << 0,
            /*! when constructing string representation of date omit time resolution */
            LDATE_FLAG_OMIT_TRES  = 1 << 1,

            LDATE_FLAG_DEFAULTS  = LDATE_FLAG_NONE
        };

    public:
        /*! short cut for number of days in this date's month */
        inline tm_scalar_t  days_in_month() const
            { return  cbm::tm_daysinmonth( &this->m_tm); }

        /*! short cut for number of days in this date's year */
        inline tm_scalar_t  days_in_year() const
            { return  cbm::tm_daysinyear( &this->m_tm); }

        /*!
         * @brief
         *    combined time resolution
         */
        static tr_scalar_t  ctime_resolution(
                ldate_t const &, ldate_t const &);

    public:
        ldate_t();
        /*!
         * @brief
         *    ...
         * @param
         *    year, month, day, subday, time resolution
         */
        ldate_t( tm_scalar_t /*year*/, tm_scalar_t /*month*/,
            tm_scalar_t /*day*/, tm_scalar_t /*subday*/,
                tr_scalar_t /*resolution*/);
        /*!
         * @brief
         *    ...
         * @param
         *    year, julian day, subday, time resolution
         */
        ldate_t( tm_scalar_t /*year*/, tm_scalar_t /*year day*/,
            tm_scalar_t /*subday*/, tr_scalar_t  /*resolution*/);
        /*!
         * @brief
         *    parse time string and optionally use reference
         *    date's components for missing components
         */
        ldate_t( char const *, ldate_t const & = ldate_t());

        /*! copy constructor */
        ldate_t( ldate_t const &);

        /*! merge date with a reference date, i.e.,
         *  replace all unset time components with
         *  values from reference date.
         */
        ldate_t  merge( ldate_t const &);

    /*DEPRECATED*/
        inline tm_scalar_t  subday() const
            { return  1 + ( this->m_tm.tm_sec + this->m_tm.tm_min*60
                + this->m_tm.tm_hour*3600) / this->dt(); }
        inline tm_scalar_t  subday_given() const
            { return  this->m_tm.tm_set.fd; }

        /* time component access methods */

        inline tm_scalar_t  second() const
            { return  this->m_tm.tm_sec; }
        inline tm_scalar_t  second_given() const
            { return  this->m_tm.tm_set.sec; }

        inline tm_scalar_t  minute() const
            { return  this->m_tm.tm_min; }
        inline tm_scalar_t  minute_given() const
            { return  this->m_tm.tm_set.min; }

        inline tm_scalar_t  hour() const
            { return  this->m_tm.tm_hour; }
        inline tm_scalar_t  hour_given() const
            { return  this->m_tm.tm_set.hour; }

        inline tm_scalar_t  day() const
            { return  this->m_tm.tm_day+1; }
        inline tm_scalar_t  day_given() const
            { return  this->m_tm.tm_set.day; }

        inline tm_scalar_t  month() const
            { return  this->m_tm.tm_month+1; }
        inline tm_scalar_t  month_given() const
            { return  this->m_tm.tm_set.month; }

        inline tm_scalar_t  year() const
            { return  this->m_tm.tm_year; }
        inline tm_scalar_t  year_given() const
            { return  this->m_tm.tm_set.year; }



        inline bool  is_leap_year() const
            { return  cbm::tm_isleapyear( &this->m_tm) != 0; }
        /* returns date's year day */
        inline tm_scalar_t  yearday() const
            { return  cbm::tm_yearday( &this->m_tm)+1; }
        inline tm_scalar_t  julian_day() const
            { return  this->yearday(); }


        /* time resolution */
        inline tr_scalar_t  time_resolution() const
            { return  this->m_tres; }
        inline tm_scalar_t  dt() const
            { return  ( 24*60*60) / this->m_tres; }
        inline bool  time_res_given() const
            { return  this->m_tres_given; }
        /* 1 over time resolution */
        inline double  day_fraction() const
            { return  this->m_itres; }

        inline td_scalar_t  seconds_since_epoch() const
            { return  this->m_tm.td_seconds; }
        inline td_scalar_t  sse() const
            { return  this->seconds_since_epoch(); }

        /*!
         * @brief
         *    difference between two dates in number of subdays. if
         *    their time resolutions do not match they are "normalized"
         *    by using lcm( t_res_0, t_res_1), lcm = least common
         *    multiple.
         */
        cbm::td_t  operator-( ldate_t const &) const;

        ldate_t &  operator+=( lperiod_t const &);
        ldate_t  operator+( lperiod_t const &) const;

// sk:todo        ldate_t &  operator-=( lperiod_t const &);
// sk:todo        ldate_t  operator-( lperiod_t const &) const;

        bool  operator<( ldate_t const &) const;
        bool  operator<=( ldate_t const &) const;
        bool  operator==( ldate_t const &) const;
        bool  operator>=( ldate_t const &) const;
        bool  operator>( ldate_t const &) const;

        ldate_t &  operator=( ldate_t const &);
    
        cbm::tm_t const &  as_tm() const
            { return  this->m_tm; }

        ldndc_timestamp_t  to_scalar() const;
        lerr_t  from_scalar(
                ldndc_timestamp_t const /*scalar*/);

        /* string representation of ldndc date */
        std::string  to_string( lflags_t const & = ldate_t::LDATE_FLAG_DEFAULTS) const;
        std::string  as_iso8601() const;

    private:
        cbm::tm_t  m_tm;

        /* number of subday units a day is split, time resolution */
        tr_scalar_t  m_tres;
        int  m_tres_given;
        /* day fraction, that is 1/m_tres */
        double  m_itres;

    private:
        /* initializes object's members */
        lerr_t  m_setdate( tm_scalar_t = TM_unset /*year*/, tm_scalar_t = TM_unset /*month*/,
            tm_scalar_t = TM_unset /*day*/,
            tm_scalar_t = TM_unset /*subday*/, tr_scalar_t = TR_unset /*resolution*/,
            tm_scalar_t = TM_unset /*hour*/, tm_scalar_t = TM_unset /*minute*/, tm_scalar_t = TM_unset /*second*/);

        /* comparison helper: equal */
        inline  bool  m_isequal( ldate_t const &  _tm) const
            { return  this->m_tm.td_seconds == _tm.m_tm.td_seconds; }
        /* comparison helper: less than */
        inline  bool  m_isless(  ldate_t const &  _tm) const
            { return  this->m_tm.td_seconds < _tm.m_tm.td_seconds; }
};


/*!
 * @brief
 *    components of a full time frame specification
 *
 * @note
 *    time @p from must strictly be earlier or same
 *    as time @p to.
 */
struct  ldate_attrib_t
{
    /* time interval boundary 'beginning' */
    ldate_t  from;
    bool  from_rel;
    /* time interval boundary 'end' */
    ldate_t  to;
    bool  to_rel;

    /* time difference between from and to */
    ldate_t::td_scalar_t  diff;
};
class  CBM_API  ltime_t
{
    public:
        typedef  ldate_t::tr_scalar_t  tr_scalar_t;
        typedef  ldate_t::td_scalar_t  td_scalar_t;
        typedef  ldate_t::tm_scalar_t  tm_scalar_t;

        struct  token
        {
            enum
            {
                TIME_EOF = 0, /* returned when scanner is finished */
                TIME_ERROR,

                TIME_FROM, TIME_TO
            };
        };
    public:
        ltime_t();

        ltime_t( ldate_t const & /*from*/, ldate_t const & /*to*/);
        ltime_t( char const *, ltime_t const & = ltime_t());

        /* @brief
         *    return new ltime with changed resolution
         *
         * @note
         *    this is rather "dangerous" since it
         *    needs to drop the subday information
         *    @n
         *    only use if you know exactly what you are doing!
         */
        ltime_t( ltime_t const & /*ltime*/,
                tr_scalar_t  /*time resolution*/);

        bool  is_set() const;

        /*!
         * @brief
         *    stl string version of function below
         */
        lerr_t  from_string( std::string const &,
            ltime_t const & = ltime_t() /*reference time*/);
        /*!
         * @brief
         *    stl string pointer version of function below
         */
        lerr_t  from_string( std::string const *,
            ltime_t const * /*reference time*/);
        /*!
         * @brief
         *    pointer version of function below
         */
        lerr_t  from_string( char const *,
            ltime_t const * /*reference time*/);
        /*!
         * @brief
         *    given an ldndc date string representation, this
         *    method sets up this object with time components
         *    found in the string. as many components are
         *    set as possible. components that can not be
         *    determined (e.g. year from "01-01") remains
         *    unchanged. you may use @fn merge or @fn
         *    operator= to set a default beforehand.
         *
         *    @n
         *
         *    use a reference date to fill gaps in the string
         *    representation.
         */
        lerr_t  from_string( char const *,
            ltime_t const & = ltime_t() /*reference time*/);

        ltime_t &  parse( cbm::string_t const &);

        /*!
         * @brief
         *    merges from reference date object unspecified
         *    items in this date object
         */
        void  merge( ltime_t const &);
        /*!
         * @brief
         *    merges only 'from' part
         */
        void  merge_from( ldate_t const &);
        /*!
         * @brief
         *    merges only 'to' part
         */
        void  merge_to( ldate_t const &);

        /*!
         * @brief
         *    return string representation of time. if
         *    start and end date are the same omit
         *    end date.
         */
        std::string  to_string(
            lflags_t = ldate_t::LDATE_FLAG_DEFAULTS) const;
        /*!
         * @brief
         *    same as @fn to_string but returns period
         *    representation even if start date is the
         *    same as the end date.
         */
        std::string  to_string_long(
            lflags_t = ldate_t::LDATE_FLAG_DEFAULTS) const;


        /*!
         * @brief
         *    return start time of time period
         */
        ldate_t &  from()
            { return  this->data_.from; }
        /*!
         * @brief
         *    return start time of time period
         *    (const version)
         */
        ldate_t const &  from() const
            { return  this->data_.from; }
        /*!
         * @brief
         *    return end time of time period
         */
        ldate_t &  to()
            { return  this->data_.to; }
        /*!
         * @brief
         *    return end time of time period
         *    (const version)
         */
        ldate_t const &  to() const
            { return  this->data_.to; }

        /*!
         * @brief
         *    return time resolution
         */
        ltime_t::tr_scalar_t  time_resolution() const;

        /*!
         * @brief
         *    return period, that is the difference between
         *    end time and start time in units of the time
         *    resolution.
         *
         * @todo
         *    introduce "period" and "difference". period
         *    should be difference plus one time unit. the
         *    reason for this is time specifications like
         *    "A -> A" which must not yield zero but one.
         */
        inline ltime_t::td_scalar_t  diff( tm_scalar_t  _unit) const
            { return  this->data_.diff / _unit; }
        inline ltime_t::td_scalar_t  period( tm_scalar_t _unit) const
            { return  this->diff( _unit) + 1; }

    private:
        ldate_attrib_t  data_;
};

std::ostream CBM_API &
operator<<( std::ostream &, lperiod_t const &);
std::ostream CBM_API &
operator<<( std::ostream &, ldate_t const &);
std::ostream CBM_API &
operator<<( std::ostream &, ltime_t const &);

namespace cbm 
{
    /* ISO 8601 with zero padding
     *  [yyyy]yyyy-mm-ddTHH:MM:SS */
#define  CBM_ISO8601_MAXLEN  32
    int CBM_API to_iso8601( cbm::tm_t const &,
        char * /*buffer*/, size_t /*buffer size*/);
}

#endif  /*  !CBM_DATE_H_  */

