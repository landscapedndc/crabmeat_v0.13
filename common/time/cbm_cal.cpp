/*-
 * Copyright (c) 1997 Wolfgang Helbig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "cbm_cal.h"

/*
 * For each month tabulate the number of days elapsed in a year before the
 * month. This assumes the internal date representation, where a year
 * starts on March 1st. So we don't need a special table for leap years.
 * But we do need a special table for the year 1582, since 10 days are
 * deleted in October. This is month1s for the switch from Julian to
 * Gregorian calendar.
 */
static int const month1[] =
    {0, 31, 61, 92, 122, 153, 184, 214, 245, 275, 306, 337}; 
   /*  M   A   M   J    J    A    S    O    N    D    J */
static int const month1s[]=
    {0, 31, 61, 92, 122, 153, 184, 214, 235, 265, 296, 327}; 

typedef struct whelbig_date whelbig_date;

/* The last day of Julian calendar, in internal and ndays representation */
static int nswitch;	/* The last day of Julian calendar */
static whelbig_date jiswitch = {1582, 7, 3};

static whelbig_date	*date2idt(whelbig_date *idt, whelbig_date *dt);
static whelbig_date	*idt2date(whelbig_date *dt, whelbig_date *idt);
static int	 ndaysji(whelbig_date *idt);
static int	 ndaysgi(whelbig_date *idt);
static int	 firstweek(int year);

/*
 * Compute the Julian date from the number of days elapsed since
 * March 1st of year zero.
 */
whelbig_date *
jdate(int ndays, whelbig_date *dt)
{
	whelbig_date    idt;		/* Internal date representation */
	int     r;		/* hold the rest of days */

	/*
	 * Compute the year by starting with an approximation not smaller
	 * than the answer and using linear search for the greatest
	 * year which does not begin after ndays.
	 */
	idt.y = ndays / 365;
	idt.m = 0;
	idt.d = 0;
	while ((r = ndaysji(&idt)) > ndays)
		idt.y--;
	
	/*
	 * Set r to the days left in the year and compute the month by
	 * linear search as the largest month that does not begin after r
	 * days.
	 */
	r = ndays - r;
	for (idt.m = 11; month1[idt.m] > r; idt.m--)
		;

	/* Compute the days left in the month */
	idt.d = r - month1[idt.m];

	/* return external representation of the date */
	return (idt2date(dt, &idt));
}

/*
 * Return the number of days since March 1st of the year zero.
 * The date is given according to Julian calendar.
 */
int
ndaysj(whelbig_date *dt)
{
	whelbig_date    idt;		/* Internal date representation */

	if (date2idt(&idt, dt) == NULL)
		return (-1);
	else
		return (ndaysji(&idt));
}

/*
 * Same as above, where the Julian date is given in internal notation.
 * This formula shows the beauty of this notation.
 */
static int
ndaysji(whelbig_date * idt)
{

	return (idt->d + month1[idt->m] + idt->y * 365 + idt->y / 4);
}

/*
 * Compute the date according to the Gregorian calendar from the number of
 * days since March 1st, year zero. The date computed will be Julian if it
 * is older than 1582-10-05. This is the reverse of the function ndaysg().
 */
whelbig_date   *
gdate(int ndays, whelbig_date *dt)
{
	int const *montht;	/* month-table */
	whelbig_date    idt;		/* for internal date representation */
	int     r;		/* holds the rest of days */

	/*
	 * Compute the year by starting with an approximation not smaller
	 * than the answer and search linearly for the greatest year not
	 * starting after ndays.
	 */
	idt.y = ndays / 365;
	idt.m = 0;
	idt.d = 0;
	while ((r = ndaysgi(&idt)) > ndays)
		idt.y--;

	/*
	 * Set ndays to the number of days left and compute by linear
	 * search the greatest month which does not start after ndays. We
	 * use the table month1 which provides for each month the number
	 * of days that elapsed in the year before that month. Here the
	 * year 1582 is special, as 10 days are left out in October to
	 * resynchronize the calendar with the earth's orbit. October 4th
	 * 1582 is followed by October 15th 1582. We use the "switch"
	 * table month1s for this year.
	 */
	ndays = ndays - r;
	if (idt.y == 1582)
		montht = month1s;
	else
		montht = month1;

	for (idt.m = 11; montht[idt.m] > ndays; idt.m--)
		;

	idt.d = ndays - montht[idt.m]; /* the rest is the day in month */

	/* Advance ten days deleted from October if after switch in Oct 1582 */
	if (idt.y == jiswitch.y && idt.m == jiswitch.m && jiswitch.d < idt.d)
		idt.d += 10;

	/* return external representation of found date */
	return (idt2date(dt, &idt));
}

/*
 * Return the number of days since March 1st of the year zero. The date is
 * assumed Gregorian if younger than 1582-10-04 and Julian otherwise. This
 * is the reverse of gdate.
 */
int
ndaysg(whelbig_date *dt)
{
	whelbig_date    idt;		/* Internal date representation */

	if (date2idt(&idt, dt) == NULL)
		return (-1);
	return (ndaysgi(&idt));
}

/*
 * Same as above, but with the Gregorian date given in internal
 * representation.
 */
static int
ndaysgi(whelbig_date *idt)
{
	int     nd;		/* Number of days--return value */

	/* Cache nswitch if not already done */
	if (nswitch == 0)
		nswitch = ndaysji(&jiswitch);

	/*
	 * Assume Julian calendar and adapt to Gregorian if necessary, i. e.
	 * younger than nswitch. Gregori deleted
	 * the ten days from Oct 5th to Oct 14th 1582.
	 * Thereafter years which are multiples of 100 and not multiples
	 * of 400 were not leap years anymore.
	 * This makes the average length of a year
	 * 365d +.25d - .01d + .0025d = 365.2425d. But the tropical
	 * year measures 365.2422d. So in 10000/3 years we are
	 * again one day ahead of the earth. Sigh :-)
	 * (d is the average length of a day and tropical year is the
	 * time from one spring point to the next.)
	 */
	if ((nd = ndaysji(idt)) == -1)
		return (-1);
	if (idt->y >= 1600)
		nd = (nd - 10 - (idt->y - 1600) / 100 + (idt->y - 1600) / 400);
	else if (nd > nswitch)
		nd -= 10;
	return (nd);
}

/*
 * Compute the week number from the number of days since March 1st year 0.
 * The weeks are numbered per year starting with 1. If the first
 * week of a year includes at least four days of that year it is week 1,
 * otherwise it gets the number of the last week of the previous year.
 * The variable y will be filled with the year that contains the greater
 * part of the week.
 */
int
week(int nd, int *y)
{
	whelbig_date    dt;
	int     fw;		/* 1st day of week 1 of previous, this and
				 * next year */
	gdate(nd, &dt);
	for (*y = dt.y + 1; nd < (fw = firstweek(*y)); (*y)--)
		;
	return ((nd - fw) / 7 + 1);
}
		
/* return the first day of week 1 of year y */
static int
firstweek(int y)
{
	whelbig_date idt;
	int nd, wd;

	idt.y = y - 1;   /* internal representation of y-1-1 */
	idt.m = 10;
	idt.d = 0;

	nd = ndaysgi(&idt);
	/*
	 * If more than 3 days of this week are in the preceding year, the
	 * next week is week 1 (and the next monday is the answer),
	 * otherwise this week is week 1 and the last monday is the
	 * answer.
	 */
	if ((wd = weekday(nd)) > 3)
		return (nd - wd + 7);
	else
		return (nd - wd);
}

/* return the weekday (Mo = 0 .. Su = 6) */
int
weekday(int nd)
{
	whelbig_date dmondaygi = {1997, 8, 16}; /* Internal repr. of 1997-11-17 */
	static int nmonday;             /* ... which is a monday        */ 

	/* Cache the daynumber of one monday */
	if (nmonday == 0)
		nmonday = ndaysgi(&dmondaygi);

	/* return (nd - nmonday) modulo 7 which is the weekday */
	nd = (nd - nmonday) % 7;
	if (nd < 0)
		return (nd + 7);
	else
		return (nd);
}

/*
 * Convert a date to internal date representation: The year starts on
 * March 1st, month and day numbering start at zero. E. g. March 1st of
 * year zero is written as y=0, m=0, d=0.
 */
static whelbig_date *
date2idt(whelbig_date *idt, whelbig_date *dt)
{

	idt->d = dt->d - 1;
	if (dt->m > 2) {
		idt->m = dt->m - 3;
		idt->y = dt->y;
	} else {
		idt->m = dt->m + 9;
		idt->y = dt->y - 1;
	}
	if (idt->m < 0 || idt->m > 11 || idt->y < 0)
		return (NULL);
	else
		return idt;
}

/* Reverse of date2idt */
static whelbig_date *
idt2date(whelbig_date *dt, whelbig_date *idt)
{

	dt->d = idt->d + 1;
	if (idt->m < 10) {
		dt->m = idt->m + 3;
		dt->y = idt->y;
	} else {
		dt->m = idt->m - 9;
		dt->y = idt->y + 1;
	}
	if (dt->m < 1)
		return (NULL);
	else
		return (dt);
}


static cbm::td_scalar_t  s_NbDays( cbm::tm_t const *  _tm)
{
    struct whelbig_date  d;
    d.y = _tm->tm_year;
    d.m = _tm->tm_month+1;
    d.d = _tm->tm_day+1;

    return  ndaysg( &d);
}

cbm::td_scalar_t cbm::sse_to_date( cbm::td_scalar_t  _sse, cbm::tm_t *  _tm)
{
    _tm->tm_sec = _sse%60;
    _tm->tm_min = (_sse%3600)/60;
    _tm->tm_hour = (_sse%86400)/3600;
    cbm::td_scalar_t const  days = _sse/86400;

    whelbig_date  d;
    whelbig_date *  dp = gdate( days, &d);
    if ( !dp)
        { return -1; }

    _tm->tm_day = d.d-1;
    _tm->tm_month = d.m-1;
    _tm->tm_year = d.y;

    _tm->td_seconds = _sse;

    return _tm->td_seconds;
}
cbm::td_scalar_t cbm::date_to_sse( cbm::tm_t const *  _tm)
{
    cbm::td_scalar_t const  days = s_NbDays( _tm);
    return days*86400 + _tm->tm_hour*3600 + _tm->tm_min*60 + _tm->tm_sec;
}

cbm::td_scalar_t  cbm::td_daysdiff(
        cbm::tm_t const *  _tm1, cbm::tm_t const *  _tm2)
{
    return  s_NbDays( _tm2) - s_NbDays( _tm1);
}

cbm::tm_t::tm_t()
    { cbm::tm_clear( this); }
cbm::tm_t::tm_t( cbm::tm_scalar_t _year, cbm::tm_scalar_t _month,
        cbm::tm_scalar_t _day, cbm::tm_scalar_t _hour,
        cbm::tm_scalar_t _minute, cbm::tm_scalar_t _seconds)
        : tm_year( _year), tm_month( _month), tm_day( _day),
        tm_hour( _hour), tm_min( _minute), tm_sec( _seconds)
{
    this->tm_set.year = 1;
    this->tm_set.month = 1;
    this->tm_set.day = 1;
    this->tm_set.hour = 1;
    this->tm_set.min = 1;
    this->tm_set.sec = 1;

    this->tm_set.fd = 0;

    this->td_seconds = cbm::date_to_sse( this);
}
void cbm::tm_clear( cbm::tm_t *  _tm)
{
    _tm->tm_sec = 0;
    _tm->tm_set.sec = 0;

    _tm->tm_min = 0;
    _tm->tm_set.min = 0;

    _tm->tm_hour = 0;
    _tm->tm_set.hour = 0;

    _tm->tm_day = 0;
    _tm->tm_set.day = 0;

    _tm->tm_month = 2;
    _tm->tm_set.month = 0;

    _tm->tm_year = 0;
    _tm->tm_set.year = 0;

    _tm->td_seconds = TD_unset;


/*DEPRECATED*/
    _tm->tm_set.fd = 0;
}


cbm::tm_scalar_t  cbm::tm_daysinyear( cbm::tm_t const *  _tm)
{
    cbm::tm_t  t1( _tm->tm_year, 0, 0);
    cbm::tm_t  t2( _tm->tm_year + 1, 0, 0);
    return cbm::td_daysdiff( &t1, &t2);
}
cbm::tm_scalar_t  cbm::tm_daysinmonth( cbm::tm_t const *  _tm)
{
    cbm::tm_t  t1( _tm->tm_year, _tm->tm_month, 0);
    cbm::tm_t  t2( t1.tm_year + ( t1.tm_month==11 ? 1 : 0),
                    t1.tm_month==11 ? 0 : (t1.tm_month+1), 0);
    return  cbm::td_daysdiff( &t1, &t2);
}
cbm::tm_scalar_t  cbm::tm_yearday( cbm::tm_t const *  _tm)
{
    cbm::tm_t  t0( _tm->tm_year, 0, 0);
    return  cbm::td_daysdiff( &t0, _tm);
}
void  cbm::tm_iyearday( cbm::tm_scalar_t  _yearday, cbm::tm_t *  _tm)
{
    _tm->tm_month = 0;
    _tm->tm_day = 0;

    cbm::tm_scalar_t  yearday = _yearday+1;
    cbm::tm_scalar_t  dm = 0;
    while ( yearday > ( dm = cbm::tm_daysinmonth( _tm)))
    {
        yearday -= dm;
        _tm->tm_month += 1;
    }
    _tm->tm_day = yearday-1;
}
int  cbm::tm_isleapyear( cbm::tm_t const *  _tm)
{
    return  !( _tm->tm_year % 400)
        || ( !( _tm->tm_year & 3) && ( _tm->tm_year % 100));
}

cbm::td_scalar_t cbm::tm_mktime( cbm::tm_t *  _tm)
{
    crabmeat_assert( _tm);

    _tm->tm_min += _tm->tm_sec / 60;
    _tm->tm_sec = _tm->tm_sec % 60;

    _tm->tm_hour += _tm->tm_min / 60;
    _tm->tm_min = _tm->tm_min % 60;

    _tm->tm_day += _tm->tm_hour / 24;
    _tm->tm_hour = _tm->tm_hour % 24;

    _tm->tm_year += _tm->tm_month / 12;
    _tm->tm_month = _tm->tm_month % 12;

    whelbig_date  t_D;
    t_D.y = _tm->tm_year;
    t_D.m = _tm->tm_month+1;
    t_D.d = 1;
    cbm::td_scalar_t const  D = ndaysg( &t_D) + _tm->tm_day;

    whelbig_date *  t_Dp = gdate( D, &t_D);
    if ( !t_Dp)
        { return -1; }

    _tm->tm_day = t_D.d-1;
    _tm->tm_month = t_D.m-1;
    _tm->tm_year = t_D.y;

    _tm->td_seconds = cbm::date_to_sse( _tm);

    return  _tm->td_seconds;
}


cbm::tm_t *  cbm::tm_addperiod( cbm::tm_t *  _tm, cbm::tp_t const *  _tp)
{
    crabmeat_assert( _tm);
    crabmeat_assert( _tp);

    _tm->tm_sec += _tp->tp_sec % 86400;
    _tm->tm_min += _tp->tp_min;
    _tm->tm_hour += _tp->tp_hour;

    _tm->tm_day += _tp->tp_day + _tp->tp_sec/86400;
    _tm->tm_month += _tp->tp_month;
    _tm->tm_year += _tp->tp_year;

    tm_mktime( _tm);

    return  _tm;
}

cbm::tp_t::tp_t()
    { cbm::tp_clear( this); }
void cbm::tp_clear( cbm::tp_t *  _tp)
{
    crabmeat_assert( _tp);

    _tp->tp_sec = 0;
    _tp->tp_min = 0;
    _tp->tp_hour = 0;

    _tp->tp_day = 0;
    _tp->tp_month = 0;
    _tp->tp_year = 0;

    _tp->tr_fd = 0;

    _tp->tp_set.sec = 0;
    _tp->tp_set.min = 0;
    _tp->tp_set.hour = 0;

    _tp->tp_set.day = 0;
    _tp->tp_set.month = 0;
    _tp->tp_set.year = 0;

    _tp->tp_set.fd = 0;
}

