/*!
 * @brief
 *    defines valid time modes (i.e. points in discrete time)
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 */

#ifndef  LDNDC_TIME_MODES_H_
#define  LDNDC_TIME_MODES_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "math/lmath-bits.h"

/*!
 * @brief
 *      flags to set frequency for certain actions (e.g. running a module)
 *
 * @note
 *      these trigger-specifiers can be combined
 *
 * @note
 *    must be contiguous powers of two!
 */
enum  timemode_e
{
    /*! silent */
    TMODE_NONE  = 0U,

    /*! (pre) subdaily */
    TMODE_SUBDAILY  = 1U << 0,
    /*! alias for the smallest stepping interval */
    TMODE_SMALLEST  = TMODE_SUBDAILY,

    /*! pre daily */
    TMODE_PRE_DAILY  = 1U << 1,
    /*! post daily */
    TMODE_POST_DAILY  = 1U << 2,

    /*! pre yearly */
    TMODE_PRE_MONTHLY  = 1U << 3,
    /*! post yearly */
    TMODE_POST_MONTHLY  = 1U << 4,

    /*! pre yearly */
    TMODE_PRE_YEARLY  = 1U << 5,
    /*! post yearly */
    TMODE_POST_YEARLY  = 1U << 6,

    /*! alias for largest stepping interval */
    TMODE_LARGEST  = TMODE_POST_YEARLY,

    /* number of time modes */
    POWEROF2_ENUM_CNT(TMODE),

    /* if timemode is required but none given, default to this one */
    TMODE_DEFAULT  = TMODE_POST_DAILY
};
namespace  ldndc
{
    namespace  time
    {
        extern CBM_API char const *  TMODE_NAME;
        extern CBM_API char const *  TMODE_NAMES[TMODE_CNT+1];

        extern CBM_API char const *  timemode_name( timemode_e);
        extern CBM_API timemode_e timemode_by_name( char const *);
    }
}

#endif  /*  !LDNDC_TIME_MODES_H_  */

