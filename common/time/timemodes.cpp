/*!
 * @brief
 *    time modes related helper functions
 *
 * @author 
 *    steffen klatt  (created on: jun 21, 2013),
 */

#include  "time/timemodes.h"

char const *  ldndc::time::TMODE_NAME = "timemode";
char const *  ldndc::time::TMODE_NAMES[TMODE_CNT+1] =
{
    "none",
    "subdaily",
    "predaily", "daily",
    "premonthly", "monthly",
    "preyearly", "yearly",
   
    NULL /*sentinel*/
}; 

char const *
ldndc::time::timemode_name(
        timemode_e  _tm)
{
    int  l = 0, j = _tm;
    while ( j)
    {
        j >>= 1;
        ++l;
    }

    return  TMODE_NAMES[l];
}

#include  "utils/lutils.h"
timemode_e
ldndc::time::timemode_by_name(
        char const *  _timemode_name)
{
    if ( !_timemode_name)
    {
        return  TMODE_NONE;
    }

    timemode_e  timemode( TMODE_NONE);
    cbm::find_exp_enum< timemode_e >(
        _timemode_name, ldndc::time::TMODE_NAMES, invalid_size, &timemode);

    return  timemode;
}

