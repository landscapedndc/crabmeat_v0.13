/*-
 * Copyright (c) 1997 Wolfgang Helbig
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: src/lib/libcalendar/calendar.h,v 1.4 1999/08/28 00:04:04 peter Exp $
 */

#ifndef  CBM_CALENDAR_H_
#define  CBM_CALENDAR_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "log/cbm_baselog.h"

struct whelbig_date
{
	int y;	/* year */
	int m;	/* month */
	int d;	/* day */
};

struct whelbig_date	*gdate(int _nd, struct whelbig_date *_dt);
struct whelbig_date	*jdate(int _nd, struct whelbig_date *_dt);
int		 ndaysg(struct whelbig_date *_dt);
int		 ndaysj(struct whelbig_date *_dt);
int		 week(int _nd, int *_year);
int		 weekday(int _nd);


namespace cbm {

/* time components type */
typedef  uint32_t  tm_scalar_t;
#define  TM_unset ((cbm::tm_scalar_t)(-1))
#define  TM_FORMAT_SEQUENCE  LDNDC_PRINTF_SEQ_UINT32
/* time period components type */
typedef   int64_t  tp_scalar_t;
#define  TP_unset (cbm::tp_scalar_t)((~(uint64_t)0) >> 1)
#define  TP_FORMAT_SEQUENCE  LDNDC_PRINTF_SEQ_INT32
/* time resolution type */
typedef   int32_t  tr_scalar_t;
#define  TR_unset (0)
#define  TR_FORMAT_SEQUENCE  LDNDC_PRINTF_SEQ_INT32
/* time difference type */
typedef   int64_t  td_scalar_t;
#define  TD_unset (cbm::td_scalar_t)((~(uint64_t)0) >> 1)
#define  TD_FORMAT_SEQUENCE  LDNDC_PRINTF_SEQ_INT64

/* time */
struct CBM_API tm_t
{
    tm_t();
    tm_t( tm_scalar_t /*year*/, tm_scalar_t /*month*/,
            tm_scalar_t /*day*/, tm_scalar_t = 0 /*hour*/,
            tm_scalar_t = 0 /*minute*/, tm_scalar_t = 0 /*seconds*/);
    struct _tm_given_t
    {
        bool sec:1;
        bool min:1;
        bool hour:1;

        bool day:1;
        bool month:1;
        bool year:1;

        bool fd:1;
    } tm_set;

    tm_scalar_t tm_year;     /* Year (0-2^38) */
    tm_scalar_t tm_month;    /* Month (0-11) */
    tm_scalar_t tm_day;      /* Day of the month (0-30) */

    tm_scalar_t tm_hour;     /* Hours (0-23) */
    tm_scalar_t tm_min;      /* Minutes (0-59) */
    tm_scalar_t tm_sec;      /* Seconds (0-59) */

    td_scalar_t td_seconds;  /* Seconds since year 0 */
};

/* time period */
struct CBM_API tp_t
{
    tp_t();
    struct _tp_given_t
    {
        bool sec:1;
        bool min:1;
        bool hour:1;

        bool day:1;
        bool month:1;
        bool year:1;

        bool fd:1;
    } tp_set;

    tp_scalar_t tp_sec;    /* Seconds */
    tp_scalar_t tp_min;    /* Minutes */
    tp_scalar_t tp_hour;   /* Hours */

    tp_scalar_t tp_day;    /* Days */
    tp_scalar_t tp_month;  /* Months */
    tp_scalar_t tp_year;   /* Years */

/* DEPRECATED */
    tr_scalar_t tr_fd;    /* "fractional days" */
};

/* time difference */
struct CBM_API td_t
{
    td_scalar_t td_sec; /* Seconds between two tm times */
};


CBM_API cbm::td_scalar_t  date_to_sse( cbm::tm_t const *);
CBM_API cbm::td_scalar_t  sse_to_date( cbm::td_scalar_t /*sse*/, cbm::tm_t *);

/* return d2-d1 in days */
CBM_API cbm::td_scalar_t  td_daysdiff(
        cbm::tm_t const * /*d1*/, cbm::tm_t const * /*d2*/);

void tm_clear( cbm::tm_t *);
CBM_API cbm::tm_scalar_t  tm_daysinyear( cbm::tm_t const *);
CBM_API cbm::tm_scalar_t  tm_daysinmonth( cbm::tm_t const *);
CBM_API cbm::tm_scalar_t  tm_yearday( cbm::tm_t const *);
CBM_API void  tm_iyearday( cbm::tm_scalar_t /*yearday(off by 0)*/, cbm::tm_t *);
CBM_API int  tm_isleapyear( cbm::tm_t const *);

cbm::td_scalar_t tm_mktime( cbm::tm_t *);

CBM_API cbm::tm_t *  tm_addperiod( cbm::tm_t *, cbm::tp_t const *);
void tp_clear( cbm::tp_t *);

}

#endif  /* !CBM_CALENDAR_H_ */

