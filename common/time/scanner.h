/*!
 * @brief
 *      lexer wrapper class
 *
 * @author
 *    steffen klatt (created on: jan 05, 2012)
 */

#ifndef  LDNDC_TIME_SCANNER_H_
#define  LDNDC_TIME_SCANNER_H_

#include  "time/w_scanner.h"

#include  <string>

class  ldate_t;
class  ltime_t;

class  ldate_scanner_t  :  public  ldate_scanner_FlexLexer
{
        public:
        ldate_scanner_t( ldate_attrib_t &);

        lerr_t  from_string( std::string const &, ltime_t const * = NULL);
        lerr_t  from_string( char const *, ltime_t const * = NULL);

    private:
        ldate_attrib_t &  buf;

        lerr_t  set_time(
                ldate_t &, bool &, char const *, ldate_t const *);
};

#endif  /*  !LDNDC_TIME_SCANNER_H_  */

