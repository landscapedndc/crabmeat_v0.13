/*!
 * @author
 *    steffen klatt (created on: jul 02, 2017)
 */

#include  "time/cbm_time.h"

static int _cbm_canonicalize_dt( cbm::dT_t * _dT)
{
    if ( _dT->unit=='s') /*second*/
        { /* all good */ }
    else if ( _dT->unit=='m') /*minute*/
        { _dT->q *= 60; _dT->unit='s'; }
    else if ( _dT->unit=='h') /*hour*/
        { _dT->q *= 60*60; _dT->unit='s'; }
    else if ( _dT->unit=='d') /*day*/
        { _dT->q *= 24*60*60; _dT->unit='s'; }
    else if ( _dT->unit=='w') /*week*/
        { _dT->q *= 7*24*60*60; _dT->unit='s'; }
    else if ( _dT->unit=='M') /*month*/
        { /* all good */ }
    else if ( _dT->unit=='Y') /*year*/
        { _dT->q *= 12; _dT->unit = 'M'; }
    else if ( _dT->unit=='D') /*decade*/
        { _dT->q *= 12*10; _dT->unit = 'M'; }
    else if ( _dT->unit=='C') /*century*/
        { _dT->q *= 12*100; _dT->unit = 'M'; }
    else
        { return -1; }
    return 0;
}

cbm::dT_t  cbm::resolve_dt( char const * _dt)
{
    cbm::dT_t dT;
    dT.q = 0;
    dT.unit = 's';

    char const * dt = _dt; /*find first non-whitespace*/
    while ( *dt!='\0' && *dt==' ')
        { ++dt; }

    char const * e = dt; /*find first non-digit*/
    while ( *e!='\0')
    {
        if ( *e<'0' || *e>'9')
            { break; }
        ++e;
    }
    if ( e == dt)
        { return dT; }
    char const * i = dt; /*convert char to digits*/
    while ( i < e)
    {
        dT.q = 10*dT.q + *i-'0';
        ++i;
    }
    while ( *e!='\0' && *e==' ') /*find first non-whitespace*/
        { ++e; }
    if ( *e!='\0') /*interpret unit*/
    {
        dT.unit = *e;
        int rc_cano = _cbm_canonicalize_dt( &dT);
        if ( rc_cano)
        {
            dT.unit = '?';
            dT.q = -1;
        }
    }
    return dT;
}

cbm::td_scalar_t  cbm::add_dt( cbm::td_scalar_t _now, cbm::dT_t const * _dT)
{
    cbm::dT_t dT = *_dT;
    td_scalar_t now_plus_dt = -1;

    int rc_cano = _cbm_canonicalize_dt( &dT);
    if ( rc_cano == 0)
    {
        if ( dT.unit == 's')
            { now_plus_dt = _now + dT.q; }
        else if ( dT.unit == 'M')
        {
            cbm::tm_t tm;
            cbm::sse_to_date( _now, &tm);

            cbm::tp_t tp;
            tp.tp_month = dT.q;

            cbm::tm_t *  tm_p = cbm::tm_addperiod( &tm, &tp);
            if ( !tm_p)
                { now_plus_dt = -1; }
            else
                { now_plus_dt = tm.td_seconds; }
        }
        else
            { /*ouch*/ CBM_Assert( false); }
    }
    return now_plus_dt;
}                

