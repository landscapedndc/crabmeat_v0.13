/*!
 * @author
 *    steffen klatt (created on: dec 13, 2012)
 */

#include  "time/cbm_date.h"
#include  "time/cbm_cal.h"
#include  "time/scanner.h"

#include  "constants/cbm_const.h"
#include  "math/cbm_math.h"
#include  "log/cbm_baselog.h"

#include  "string/cbm_string.h"

#include  <stdexcept>

/* ************  time period  ************ */

char const  lperiod_t::PERIOD_TAG = '+';
char const *  lperiod_t::PERIOD_TO = "->";

lperiod_t::lperiod_t()
    { }
lperiod_t::lperiod_t( cbm::tp_t const &  _tp)
        : m_tp( _tp)
    { }
lperiod_t::lperiod_t( char const *  _period)
{
    crabmeat_assert( _period && ( _period[0] == PERIOD_TAG));

    cbm::tokenizer  tc( _period+1, ldate_t::TC_SEP);
    if ( !tc.is_ok())
    {
        CBM_LogError( "illegal time period string");
        throw  std::runtime_error( "illegal time period string");
    }

    cbm::tp_clear( &this->m_tp);

    switch ( static_cast< unsigned int >( tc.token_cnt()))
    {
        case 1:
            this->m_tp.tp_day = atoi( tc[0]);
            break;
        case 2:
            this->m_tp.tp_month = atoi( tc[0]);
            this->m_tp.tp_day = atoi( tc[1]);
            break;
        case 3:
            this->m_tp.tp_year = atoi( tc[0]);
            this->m_tp.tp_month = atoi( tc[1]);
            this->m_tp.tp_day = atoi( tc[2]);
            break;
        case 4:
            this->m_tp.tp_year = atoi( tc[0]);
            this->m_tp.tp_month = atoi( tc[1]);
            this->m_tp.tp_day = atoi( tc[2]);
            this->m_tp.tr_fd = atoi( tc[3]);
            break;
        default:
            CBM_LogError( "illegal time period string  [period=",_period,"]");
            throw  std::runtime_error( "illegal time period string");
    }
}

lperiod_t &
lperiod_t::operator*=( int  _mul)
{
    this->m_tp.tp_sec *= _mul;
    this->m_tp.tp_min *= _mul;
    this->m_tp.tp_hour *= _mul;
    this->m_tp.tp_day *= _mul;
    this->m_tp.tp_month *= _mul;
    this->m_tp.tp_year *= _mul;

    this->m_tp.tr_fd *= _mul;

    return  *this;
}

lperiod_t
lperiod_t::operator*( int  _mul) const
{
    lperiod_t  this_period = *this;
    return  this_period.operator*=( _mul);
}

/*TODO*/
std::string
lperiod_t::to_string() const
{
    return  PERIOD_TAG +           cbm::n2s( this->m_tp.tp_year)
               + ldate_t::TC_SEP + cbm::n2s( this->m_tp.tp_month)
               + ldate_t::TC_SEP + cbm::n2s( this->m_tp.tp_day)
               + ldate_t::TC_SEP + cbm::n2s( this->m_tp.tr_fd);
}

period_oneday::period_t const &  period_oneday::one_day()
{
    cbm::tp_t  p_1d;
    cbm::tp_clear( &p_1d);

    p_1d.tp_day = 1;
    p_1d.tp_set.day = 1;

    static lperiod_t _one_day = lperiod_t( p_1d);
    return _one_day;
}


/* ************  date/time  ************ */

char const  ldate_t::TC_SEP = '-';
char const  ldate_t::TR_SEP = '/';

static void s_iyearday( cbm::tm_scalar_t  _yearday, cbm::tm_scalar_t  _year,
        cbm::tm_scalar_t *  _m, cbm::tm_scalar_t *  _d)
{
    cbm::tm_t  t( _year, 0, 0);

    cbm::tm_iyearday( _yearday-1, &t);
    *_m = t.tm_month+1;
    *_d = t.tm_day+1;
}

ldate_t::tr_scalar_t
ldate_t::ctime_resolution(
        ldate_t const &  _d1, ldate_t const &  _d2)
{
    return  cbm::lcm(
                (signed int)_d1.time_resolution(),
                (signed int)_d2.time_resolution());
}

ldate_t::ldate_t()
{
    lerr_t const  rc_setdate = this->m_setdate();
    if ( rc_setdate)
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }
}

ldate_t::ldate_t( cbm::tm_scalar_t  _year, cbm::tm_scalar_t  _month,
        cbm::tm_scalar_t  _day, cbm::tm_scalar_t  _subday, tr_scalar_t  _t_res)
{
    lerr_t const  rc_setdate =
        this->m_setdate( _year, _month, _day, _subday, _t_res);
    if ( rc_setdate)
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }
}

ldate_t::ldate_t( cbm::tm_scalar_t  _year, cbm::tm_scalar_t  _julian_day,
        cbm::tm_scalar_t  _subday, cbm::tr_scalar_t  _t_res)
{
    cbm::tm_scalar_t  mo, da;
    s_iyearday( _julian_day, _year, &mo, &da);
    lerr_t const  rc_setdate =
        this->m_setdate( _year, mo, da, _subday, _t_res);
    if ( rc_setdate)
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }
}

ldate_t::ldate_t( ldate_t const &  _d)
{
    lerr_t const  rc_setdate = this->m_setdate(
        _d.year(), _d.month(), _d.day(),
            _d.subday(), _d.m_tres,
        _d.hour(), _d.minute(), _d.second());
    if ( rc_setdate)
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }
}

/* parse date string YYYY-MM-DD-SS/RR */
ldate_t::ldate_t( char const *  _ldate,
        ldate_t const &  _ref)
{
    crabmeat_assert( _ldate && ( _ldate[0] != lperiod_t::PERIOD_TAG));

    cbm::tr_scalar_t  t_res = 1;
    bool  tr_given = false;

    cbm::tokenizer  tc_r( _ldate, ldate_t::TR_SEP);
    if ( ! tc_r.is_ok())
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }

    cbm::tokenizer  tc( tc_r[0], ldate_t::TC_SEP);
    if ( ! tc.is_ok())
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }
    if ( tc_r.token_cnt() > 1u/*== 2*/)
    {
        if ( tc_r.token_cnt() != 2u)
        {
            CBM_LogError( "Illegal time string  [time=", _ldate, "]");
            throw  std::runtime_error( "illegal time string");
        }

        t_res = atoi( tc_r[1]);
        tr_given = true;
    }

    lerr_t  rc = LDNDC_ERR_FAIL;
/*DEPRECATED*/
    cbm::tm_scalar_t  s( _ref.subday());

    cbm::tm_scalar_t  m( _ref.month());
    cbm::tm_scalar_t  y( _ref.year());

    cbm::tm_scalar_t  H( _ref.hour());
    cbm::tm_scalar_t  M( _ref.minute());
    cbm::tm_scalar_t  S( _ref.second());
/*TODO*/
    if (( s!=1) || (t_res>1))
        { H = TM_unset; M = TM_unset; S = TM_unset; }

    cbm::tr_scalar_t  r( tr_given ? t_res : _ref.time_resolution());

    switch ( tc.token_cnt())
    {
        case 1:
            rc = this->m_setdate( y, m, atoi( tc[0]), s, r, H, M, S);
            break;
        case 2:
            m = atoi( tc[0]);
            rc = this->m_setdate( y, m, atoi( tc[1]), s, r, H, M, S);
            break;
        case 3:
            y = atoi( tc[0]);
            m = atoi( tc[1]);
            rc = this->m_setdate( y, m, atoi( tc[2]), s, r, H, M, S);
            break;
        case 4:
            y = atoi( tc[0]);
            m = atoi( tc[1]);
            s = atoi( tc[3]);
            rc = this->m_setdate( y, m, atoi( tc[2]), s, r, H, M, S);
            break;
        default:
            rc = LDNDC_ERR_FAIL;
            CBM_LogError( "illegal time string.");
    }

    if ( rc != LDNDC_ERR_OK)
    {
        CBM_LogError( "error parsing date");
        throw  std::runtime_error( "error parsing date");
    }
}

/*TODO*/
ldate_t
ldate_t::merge(
        ldate_t const &  _ref)
{
    cbm::tm_scalar_t  d_subday(( this->subday_given()) ? this->subday() : _ref.subday());
    cbm::tm_scalar_t     d_day(( this->day_given()) ? this->day() : _ref.day());
    cbm::tm_scalar_t   d_month(( this->month_given()) ? this->month() : _ref.month());
    cbm::tm_scalar_t    d_year(( this->year_given()) ? this->year() : _ref.year());

    cbm::tr_scalar_t  d_time_resolution((
            this->time_res_given()) ? this->time_resolution() : _ref.time_resolution());

    /* NOTE  we no longer merge the given components indicators */
    return  ldate_t( d_year, d_month, d_day, d_subday, d_time_resolution);
}

cbm::td_t
ldate_t::operator-( ldate_t const &  _rhs)
const
{
    cbm::td_scalar_t const  t0 = cbm::date_to_sse( &this->m_tm);
    cbm::td_scalar_t const  t1 = cbm::date_to_sse( &_rhs.m_tm);

    cbm::td_t  td;
    td.td_sec = t0 - t1;

    return  td;
}

ldate_t &
ldate_t::operator+=( lperiod_t const &  _p)
{
/*FIXME*/
    cbm::tp_t const &  tp = _p.as_tp();
    cbm::tm_addperiod( &this->m_tm, &tp);

    return  *this;
}
ldate_t
ldate_t::operator+( lperiod_t const &  _p)
const
{
    ldate_t  this_date = *this;
    this_date.operator+=( _p);
    return  this_date;
}

bool
ldate_t::operator<( ldate_t const &  _t) const
    { return  this->m_isless( _t); }
bool
ldate_t::operator<=( ldate_t const &  _t) const
    { return  this->m_isless( _t) || this->m_isequal( _t); }
bool
ldate_t::operator==( ldate_t const &  _t) const
    { return  this->m_isequal( _t); }
bool
ldate_t::operator>=( ldate_t const &  _t) const
    { return  ( !this->m_isless( _t)) || this->m_isequal( _t); }
bool
ldate_t::operator>( ldate_t const &  _t) const
    { return  !( this->m_isless( _t) || this->m_isequal( _t)); }

ldate_t &
ldate_t::operator=( ldate_t const &  _t)
{
    if ( &_t != this)
    {
        this->m_tm = _t.m_tm;
        this->m_tres = _t.m_tres;
        this->m_tres_given = _t.m_tres_given;
        this->m_itres = _t.m_itres;
    }
    return  *this;
}


ldndc_timestamp_t ldate_t::to_scalar() const
{
    crabmeat_assert( (int)this->m_tres <= (int)cbm::SEC_IN_DAY);

    ldndc_timestamp_t const  t_sse =
        static_cast< ldndc_timestamp_t >( cbm::date_to_sse( &this->m_tm)) << TRES_BITS;
    ldndc_timestamp_t const  t_res =
        static_cast< ldndc_timestamp_t >( this->m_tres) & (( ldndc_timestamp_t( 1) << TRES_BITS) - 1);

    return  t_sse|t_res;
}

lerr_t ldate_t::from_scalar( ldndc_timestamp_t const  _scalar)
{
    tm_clear( &this->m_tm);
    ldndc_timestamp_t const  t_res =
        _scalar & (( ldndc_timestamp_t( 1) << TRES_BITS) - 1);;
    this->m_tres = t_res;

    ldndc_timestamp_t const  t_sse = _scalar >> TRES_BITS;
    td_scalar_t  sec = cbm::sse_to_date( t_sse, &this->m_tm);
    if ( sec != t_sse)
        { CBM_LogFatal( "[BUG]"); } 

    return  LDNDC_ERR_OK;
}

std::string
ldate_t::to_string(
        lflags_t const &  _flags)
const
{
    std::ostringstream  date_string( "");
    date_string.fill( '0');

    date_string
        << this->m_tm.tm_year
        << TC_SEP << std::setw(2) << this->m_tm.tm_month+1
        << TC_SEP << std::setw(2) << this->m_tm.tm_day+1;

    if ( this->m_tres != 1)
    {
        if (( _flags & LDATE_FLAG_SHORT) && ( this->subday() == 1u))
            { /* no op */ }
        else
        {
            int  tres_width = static_cast< int >( cbm::n2s( this->time_resolution()).size());
            date_string << TC_SEP << std::setw(tres_width) << this->subday();
        }
    }
    if ( ! ( _flags & LDATE_FLAG_OMIT_TRES))
    {
        date_string << TR_SEP << this->m_tres;
    }


    return  date_string.str();
}

std::string
ldate_t::as_iso8601()
const
{
    char  isodate[CBM_ISO8601_MAXLEN];
    cbm::to_iso8601( this->m_tm, isodate, CBM_ISO8601_MAXLEN);

    return  std::string( isodate);
}

lerr_t
ldate_t::m_setdate(
        cbm::tm_scalar_t  _year, cbm::tm_scalar_t  _month, cbm::tm_scalar_t  _day,
        cbm::tm_scalar_t  _subday, cbm::tr_scalar_t  _dt,
        cbm::tm_scalar_t  _hour, cbm::tm_scalar_t  _minute, cbm::tm_scalar_t  _second)
{
// sk:off    cbm::setenv( "TZ", "", 1);
// sk:off    cbm::tzset();

    tm_clear( &this->m_tm);

    if ( _year != TM_unset)
        { this->m_tm.tm_year = _year; }
    this->m_tm.tm_set.year = _year != TM_unset;

    if ( _month != TM_unset)
        { this->m_tm.tm_month = _month-1; }
    this->m_tm.tm_set.month = _month != TM_unset;

    if ( _day != TM_unset)
        { this->m_tm.tm_day = _day-1; }
    this->m_tm.tm_set.day = _day != TM_unset;

    if ( _hour != TM_unset)
        { this->m_tm.tm_hour = _hour; }
    this->m_tm.tm_set.hour = _hour != TM_unset;

    if ( _minute != TM_unset)
        { this->m_tm.tm_min = _minute; }
    this->m_tm.tm_set.min = _minute != TM_unset;

    if ( _second != TM_unset)
        { this->m_tm.tm_sec = _second; }
    this->m_tm.tm_set.sec = _second != TM_unset;

    if ( _dt != TR_unset)
        { this->m_tres = _dt; }
    else
        { this->m_tres = 1; }
    this->m_tres_given = _dt != TR_unset;
    crabmeat_assert(( this->m_tres > 0) && ( this->m_tres < ( 1<<TRES_BITS)));
    this->m_itres = 1.0 / (double)this->m_tres;

/*DEPRECATED*/ /*TODO*/
    if ( _subday != TM_unset)
    {
        /* subday is in { 0, ..., R-1}, (R:=time resolution) */
        if ( _subday > (tm_scalar_t)this->m_tres)
        {
            CBM_LogError( "subday must not be greater than day resolution.",
                "  [time object's resolution r=", this->m_tres, ",getting r'=", _subday, "]");
            return  LDNDC_ERR_FAIL;
        }

        this->m_tm.tm_set.fd = true;

        if ( !this->m_tm.tm_set.hour
                && !this->m_tm.tm_set.min
                && !this->m_tm.tm_set.sec)
        {
            tm_scalar_t  fd_sec = this->dt() * (_subday-1);
            this->m_tm.tm_hour = fd_sec / 3600;
            fd_sec %= 3600;
            this->m_tm.tm_min = fd_sec / 60;
            fd_sec %= 60;
            this->m_tm.tm_sec = fd_sec;
        }
    }
    else
        { this->m_tm.tm_set.fd = false; }

    cbm::tm_mktime( &this->m_tm);
// sk:dbg    CBM_LogDebug( "this=", (void*)this, "  y=",this->m_tm.tm_year, " m=",this->m_tm.tm_month, " d=",this->m_tm.tm_day, " s=",this->subday()-1,
// sk:dbg    "  H=",this->m_tm.tm_hour, " M=",this->m_tm.tm_min, " S=",this->m_tm.tm_sec, "  R=",this->m_tres, " sse=",this->m_tm.td_seconds);


    /* time resolution cannot be zero */
    if ( this->m_tres == 0)
    {
        CBM_LogError( "day resolution must be greater than 0.");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}




/* ************  time frame  ************ */

ltime_t::ltime_t()
{
    data_.diff = ldndc::invalid_t< ldate_t::td_scalar_t >::value;

    this->data_.from_rel = false;
    this->data_.to_rel = false;
}

ltime_t::ltime_t(
        ldate_t const &  _from,
        ldate_t const &  _to)
{
    if ( _from.time_resolution() != _to.time_resolution())
    {
        CBM_LogError( "time resolutions not matching",
                    "  [from=",_from.to_string(), ",to=", _to.to_string(),"]");
        throw  std::runtime_error( "time resolutions not matching");
    }
    if ( _from > _to)
    {
        CBM_LogError( "end of period before beginning",
                    "  [from=",_from.to_string(), ",to=", _to.to_string(),"]");
        throw  std::runtime_error( "time resolutions not matching");
    }

    this->data_.from = _from;
    this->data_.from_rel = false;
    this->data_.to = _to;
    this->data_.to_rel = false;

    cbm::td_t  dt = this->data_.to - this->data_.from;
    this->data_.diff = dt.td_sec;
}

ltime_t::ltime_t(
    ltime_t const &  _ltime, tr_scalar_t  _t_res)
{
    /* by definition we only allow this for subday values being 1 */
    if ( _ltime.time_resolution() != _t_res)
    {
        if (( _ltime.from().subday() > 1) || ( _ltime.to().subday() > 1))
        {
            CBM_LogError( "i told you, but you didn't listen ..");
            throw  std::runtime_error( "subday greater 1");
        }

        ldate_t const &  b = _ltime.from();
        this->data_.from = ldate_t( b.year(), b.month(), b.day(), 1, _t_res);
        this->data_.from_rel = false;
        ldate_t const &  e = _ltime.to();
        this->data_.to = ldate_t( e.year(), e.month(), e.day(), 1, _t_res);
        this->data_.to_rel = false;

        cbm::td_t  dt = this->data_.to - this->data_.from;
        this->data_.diff = dt.td_sec;
    }
    else
    {
        *this = _ltime;
    }
}
ltime_t::ltime_t(
        char const *  _t, ltime_t const &  _ref)
    { this->from_string( _t, _ref); }


bool ltime_t::is_set() const
{
    return  this->data_.diff != ldndc::invalid_t< ldate_t::td_scalar_t >::value;
}


lerr_t
ltime_t::from_string(
        std::string const &  _t, ltime_t const &  _ref)
{
    return  this->from_string( _t.c_str(), &_ref);
}
lerr_t
ltime_t::from_string(
        std::string const *  _t, ltime_t const *  _ref)
{
    return  this->from_string( _t->c_str(), _ref);
}
lerr_t
ltime_t::from_string(
        char const *  _t, ltime_t const &  _ref)
{
    return  this->from_string( _t, &_ref);
}
lerr_t
ltime_t::from_string(
        char const *  _t, ltime_t const *  _ref)
{
    if ( !_t || cbm::is_empty( _t))
        { return  LDNDC_ERR_FAIL; }

    ldate_scanner_t  lexer_( this->data_);
    lerr_t  rc = lexer_.from_string( _t, _ref);
    if ( rc != LDNDC_ERR_OK)
        { return  rc; }

    if ( this->data_.to < this->data_.from)
    {
        CBM_LogError( "time interval not valid.",
            "  ['end="+this->data_.to.to_string()+"' earlier then",
           " 'beginning="+this->data_.from.to_string()+"']");
        this->data_.diff = -1;
        return  LDNDC_ERR_FAIL;
    }
    cbm::td_t  td = this->data_.to - this->data_.from;
    this->data_.diff = td.td_sec;

    return  LDNDC_ERR_OK;
}

ltime_t &
ltime_t::parse( cbm::string_t const &  _t)
{
    this->from_string( _t.c_str(), ltime_t());
    return  *this;
}

void
ltime_t::merge_from( ldate_t const &  _ref_from)
    { this->data_.from =
            this->data_.from.merge( _ref_from); }
void
ltime_t::merge_to( ldate_t const &  _ref_to)
    { this->data_.to =
            this->data_.to.merge( _ref_to); }
void
ltime_t::merge( ltime_t const &  _ref)
{
    this->merge_from( _ref.from());
    this->merge_to( _ref.to());

    cbm::td_t  dt =
        this->data_.to - this->data_.from;
    this->data_.diff = dt.td_sec;
}


ltime_t::tr_scalar_t
ltime_t::time_resolution()
const
{
    crabmeat_assert(
        this->data_.from.time_resolution()
            == this->data_.to.time_resolution());

    return  this->data_.from.time_resolution();
}


std::string
ltime_t::to_string( lflags_t  _lflags)
const
{
    if ( this->data_.from == this->data_.to)
        { return  this->data_.from.to_string( _lflags); }

    return  this->data_.from.to_string( _lflags)
        + std::string( lperiod_t::PERIOD_TO)
        + this->data_.to.to_string( _lflags|ldate_t::LDATE_FLAG_OMIT_TRES);
}
std::string
ltime_t::to_string_long( lflags_t  _lflags)
const
{
    return  this->data_.from.to_string( _lflags)
        + std::string( lperiod_t::PERIOD_TO)
        + this->data_.to.to_string( _lflags|ldate_t::LDATE_FLAG_OMIT_TRES);
}


std::ostream &
operator<<( std::ostream &  _out,
        lperiod_t const &  _lperiod)
{
    _out << _lperiod.to_string();
    return  _out;
}
std::ostream &
operator<<( std::ostream &  _out,
        ldate_t const &  _ldate)
{
    _out << _ldate.to_string();
    return  _out;
}
std::ostream &
operator<<( std::ostream &  _out,
        ltime_t const &  _ltime)
{
    _out << _ltime.to_string();
    return  _out;
}

int
cbm::to_iso8601( cbm::tm_t const &  _tm,
        char *  _buffer, size_t  _buffer_sz)
{
    return  cbm::snprintf( _buffer, _buffer_sz,
        "%u-%02u-%02u %02u:%02u:%02u",
        _tm.tm_year, _tm.tm_month+1, _tm.tm_day+1,
        _tm.tm_hour, _tm.tm_min, _tm.tm_sec);
}

