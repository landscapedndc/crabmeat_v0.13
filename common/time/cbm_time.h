/*!
 * @brief
 *    Simulation time and World time related functions
 *
 * @author
 *    steffen klatt (created on: dec 01, 2016)
 */

#ifndef  CBM_TIME_H_
#define  CBM_TIME_H_

#include  "crabmeat-common.h"

#include  "time/cbm_clock.h"
#include  "time/cbm_date.h"
#include  "time/cbm_rtime.h"
#include  "time/timemodes.h"

namespace cbm {

    struct dT_t
    {
        td_scalar_t  q; /*amount of time units*/
        char  unit; /*time unit: second, month, ..*/
    };

    CBM_API dT_t  resolve_dt( char const * /*dt*/);
    CBM_API td_scalar_t  add_dt( td_scalar_t /*base*/, dT_t const * /*dt*/);

} /*namespace cbm*/

#endif  /*  !CBM_TIME_H_  */

