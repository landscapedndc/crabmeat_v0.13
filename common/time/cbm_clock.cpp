/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: jan 05, 2012),
 *    edwin haas
 */

#include  "time/cbm_clock.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"


cbm::sclock_t::sclock_t()
        : m_dt( 0)
{
    if ( this->m_setclock( this->m_date0) != LDNDC_ERR_OK)
        { CBM_LogFatal( "error setting timer"); }
}

cbm::sclock_t::sclock_t(
        ltime_t const &  _date0)
        : m_date0( _date0),
          m_dt( _date0.from().dt())
{
    if ( this->m_setclock( this->m_date0) != LDNDC_ERR_OK)
        { CBM_LogFatal( "error setting timer"); }
}


void
cbm::sclock_t::reset()
    { this->m_setclock( this->m_date0); }

int
cbm::sclock_t::advance()
    { return this->advance( this->m_dt); }
int
cbm::sclock_t::advance( cbm::td_scalar_t  _dt)
{
    /* execute all registered callbacks at end of timestep */
    int  rc_onleave = 0;
    if ( _dt > 0)
        { rc_onleave = this->dispatch_on_event( &this->on_leave_timestep); }

    this->m_tm.tm_sec += _dt;
    cbm::tm_mktime( &this->m_tm);

    /* increment cycle counter */
    this->m_cycles += 1;

    /* precompute new time step position */
    this->m_update_timeposition();

    /* execute all registered callbacks at start of timestep */
    int  rc_onenter = 0;
    if ( _dt > 0)
        { rc_onenter = this->dispatch_on_event( &this->on_enter_timestep); }

    if ( rc_onleave)
        { return -1; }
    if ( rc_onenter)
        { return  -1; }
    return  0;
}

std::string
cbm::sclock_t::to_string()
const
{
    return  this->now().to_string()
        + lperiod_t::PERIOD_TO
        + this->m_date0.to().to_string( ldate_t::LDATE_FLAG_OMIT_TRES);
}

std::string
cbm::sclock_t::position_names( unsigned int  _timemodes)
const
{
    unsigned int  timemodes = _timemodes;
    if ( timemodes == TMODE_NONE)
    {
        timemodes = this->position();
    }

    std::string  cur_timemodenames;
    static char const *  comma = ",";
    char const *  delim = "";
    for ( int  t = 0;  t < TMODE_CNT;  ++t)
    {
        timemode_e  tm = (timemode_e)( 1 << t);
        if ( tm & timemodes)
        {
            cur_timemodenames += delim;
            cur_timemodenames += ldndc::time::timemode_name( tm);
            delim = comma;
        }
    }
    return  cur_timemodenames;
}

lerr_t
cbm::sclock_t::m_setclock( ltime_t const &  _date)
{
    ldate_t const &  f = _date.from();
    ldate_t const &  t = _date.to();

    this->m_cycles = 0;
    this->m_final = t.sse();
    this->m_dt = f.dt();

    cbm::tm_clear( &this->m_tm);
    this->m_tm.tm_sec = f.second();
    this->m_tm.tm_min = f.minute();
    this->m_tm.tm_hour = f.hour();
    this->m_tm.tm_day = f.day()-1;
    this->m_tm.tm_month = f.month()-1;
    this->m_tm.tm_year = f.year();

    cbm::tm_mktime( &this->m_tm);

    this->m_update_timeposition();

    return  LDNDC_ERR_OK;
}


void
cbm::sclock_t::m_update_timeposition()
{
    this->m_tpos = TMODE_SUBDAILY;

    if ( this->m_tm.tm_hour == 0 && this->m_tm.tm_min == 0
            && this->m_tm.tm_sec == 0)
    {
        this->m_tpos |= TMODE_PRE_DAILY;

        if ( this->m_tm.tm_month == 0 && this->m_tm.tm_day == 0)
        {
            this->m_tpos |= TMODE_PRE_YEARLY;
        }
    }

    if (( this->m_tm.tm_hour*60*60 + this->m_tm.tm_min*60
            + this->m_tm.tm_sec + this->m_dt) == 60*60*24)
    {
        this->m_tpos |= TMODE_POST_DAILY;
        if ( this->m_tm.tm_month == 11 && this->m_tm.tm_day == 30)
        {
            this->m_tpos |= TMODE_POST_YEARLY;
        }
    }
}

cbm::sclock_callees_t *
cbm::sclock_t::m_select_event_slot( char const *  _event)
const
{
    sclock_callees_t *  event_slot = NULL;

    if ( cbm::is_equal( "on-enter-timestep", _event))
        { event_slot = &this->on_enter_timestep; }
    else if ( cbm::is_equal( "on-leave-timestep", _event))
        { event_slot = &this->on_leave_timestep; }
    else
        { /* no such event */ }

    return event_slot;
}

int
cbm::sclock_t::register_event( char const *  _event,
             cbm::sclock_t::callback_t  _callback,
                void *  _object, char const *  _caller)
const
{
    CRABMEAT_FIX_UNUSED(_caller);
// sk:dbg    CBM_LogDebug( "Clock: Registered callback for \"", _caller, "\"");

    crabmeat_assert( _callback);
    crabmeat_assert( _object);

    sclock_callees_t  * event_slot = this->m_select_event_slot( _event);
    if ( !event_slot )
        { return -1; }

    sclock_callees_t::iterator_t  c_k = event_slot->begin();
    /* check if callee is already registered */
    for ( ; c_k != event_slot->end();  ++c_k)
    {
        sclock_callees_t::callee_t *  c = &(*c_k);
        if (( c->obj == _object) && ( c->func == _callback))
        {
            return  -1;
        }
    }

    sclock_callees_t::callee_t  c = { _object, _callback};
    event_slot->insert( c);

    return  0;
}
int
cbm::sclock_t::unregister_event( char const *  _event,
                    cbm::sclock_t::callback_t  _callback,
                void *  _object, char const *  _caller)
const
{
    CRABMEAT_FIX_UNUSED(_caller);

    crabmeat_assert( _callback);
    crabmeat_assert( _object);

    sclock_callees_t *  event_slot = this->m_select_event_slot( _event);
    if ( !event_slot)
        { return -1; }

    /* check if callee is already registered */
    sclock_callees_t::iterator_t  c_k = event_slot->begin();
    for ( ; c_k != event_slot->end();  ++c_k)
    {
        sclock_callees_t::callee_t *  c = &(*c_k);
        if (( c->obj == _object) && ( c->func == _callback))
        {
            event_slot->remove( c_k);
            return  0;
        }
    }
    return  1;
}
int
cbm::sclock_t::dispatch_on_event(
            cbm::sclock_callees_t *  _event_slot,
                bool  _unregister_all_callbacks)
{
    int  rc = 0;

    sclock_callees_t::iterator_t  c_k = _event_slot->begin();
    for ( ; c_k != _event_slot->end();  ++c_k)
    {
        sclock_callees_t::callee_t *  c = &(*c_k);
        if ( c->func)
        {
            int  rc_func = c->func( c->obj, this);
            if ( rc_func)
            {
                rc = rc_func;
            }
        }
    }
    if ( _unregister_all_callbacks)
    {
// sk:dbg        CBM_LogDebug( "unregistering all callbacks");
        _event_slot->clear();
    }
    return  rc;
}


std::ostream & operator<<( std::ostream &  _out,
    cbm::sclock_t const &  _clock)
{
    _out << _clock.to_string();
    return  _out;
}

