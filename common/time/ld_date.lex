
%{
#include  "cbm_clock.h"
%}

/*** Flex Declarations and Options ***/
%option outfile="g_scanner.cpp"
%option header-file="g_scanner.h"

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "ldate_scanner_FlexLexer" */
%option prefix="ldate_scanner_"

/* do not include unix specific header */
%option nounistd
/* never exspect source to be a tty */
%option never-interactive


/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug


%x  sc_timespan

digit		[0-9]
int		0|([1-9]{digit}*)
int_wo0		[1-9]{digit}*
int_l0		{digit}+
int_wo0_l0	0*{int_wo0}
ws		[ \t]
drop		[ \t\r\n]+
timeto		->

time		({int_wo0_l0}{ws}*-{ws}*){0,3}{int_wo0_l0}
period		[+]{ws}*({int_l0}{ws}*-{ws}*){0,3}{int_l0}

%%


<INITIAL>{
        {timeto} {
                //printf( "time span discovered\n");
                BEGIN(sc_timespan);
        }

        /* time components YYYY-MM-DD-SS */

        {time}({ws}*"/"{ws}*{int_wo0}){0,1} {
                //printf( "time(1)=%s\n", yytext);
                return  ldate_t::token::TIME_FROM;
        }

	{period} {
		//LOGERROR( "period not allowed in this context");
                //return  ldate_t::token::TIME_FROM;
		return  ldate_t::token::TIME_ERROR;
	}
}

<sc_timespan>{
	{time} {
		//printf( "time(2)=%s\n", yytext);
                /* indicates that we found valid time span */
		BEGIN(INITIAL);
		return  ldate_t::token::TIME_TO;
	}
	{period} {
		//printf( "period(2): %s\n", yytext);
                /* indicates that we found valid time span */
		BEGIN(INITIAL);
		return  ldate_t::token::TIME_TO;
	}
	<<EOF>> {
		//fprintf( stderr, "missing time span end\n");
		return  ldate_t::token::TIME_ERROR;
	}
}

<INITIAL,sc_timespan>{
	{drop}+ {
		/* no op */
	}

	. {
		//fprintf( stderr, "illegal time specification\n");
		return  ldate_t::token::TIME_ERROR;
	}
}


<<EOF>> {
	return  ldate_t::token::TIME_EOF;
}

%%

int
ldate_scanner_FlexLexer::yywrap()
{
	return  1;
}

