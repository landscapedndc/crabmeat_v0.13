/*!
 * @brief
 *    wrapper for system time (implementation)
 *
 * @author
 *    steffen klatt (created on: mar 07, 2013)
 */

#include  "time/cbm_rtime.h"
#include  <stdio.h>

lerr_t
cbm::current_time_as_string(
                std::string *  _t_str,
                char const *  _fmt)
{
    if ( !_t_str)
        { return  LDNDC_ERR_INVALID_ARGUMENT; }

    /* e.g. Fri Mar  8 00:11:19 CET 2013 */
    static char const *  _fmt_dflt = "%a %b %d %H:%M:%S %Z %Y";
    static const size_t  _t_str_max_size = 200;
    char  t_str[_t_str_max_size];

    time_t  t;
    struct  tm *  t_tm;

    t = ::time( NULL);
    t_tm = ::localtime( &t);

    if ( t_tm == NULL)
    {
        ::perror( "localtime");
        return  LDNDC_ERR_FAIL;
    }

    if ( ::strftime( t_str, _t_str_max_size, ( _fmt ? _fmt : _fmt_dflt), t_tm) == 0)
    {
        fprintf( stderr, "strftime returned 0\n");
        return  LDNDC_ERR_FAIL;
    }


    *_t_str = t_str;
    return  LDNDC_ERR_OK;
}

