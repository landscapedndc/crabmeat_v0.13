/*!
 * @brief
 *    basic math related helper functions 
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_BASIC_H_
#define  CBM_MATH_BASIC_H_

#include  "crabmeat-common.h"
#include  <cmath>
#include  <algorithm>

namespace  cbm
{

    /*!
     * @brief
     *    sign of number x.
     *
     * @return
     *    -1 (x<0), 1 (x>0), 0 (otherwise)
     */
        template < typename  _T >
        inline int sign( _T const &  _x)
        {
            return  ( _x > 0.0) ? 1 : (( _x < 0.0) ? -1 : 0);
        }


        template < typename  _T >
        inline _T abs( _T const &  _a)
        {
            return  std::abs( _a);
        }

        template < >
        inline signed char abs( signed char const &  _a)
        {
            return  ( _a < 0) ? (signed char)(-_a) : _a;
        }
        template < >
        inline signed short int abs(
                signed short int const &  _a)
        {
            return  ( _a < 0) ? static_cast< short int >(-_a) : _a;
        }
        template < >
        inline signed int abs( signed int const &  _a)
        {
            return  ( _a < 0) ? -_a : _a;
        }
        template < >
        inline signed long int abs(
                signed long int const &  _a)
        {
            return  ( _a < 0) ? -_a : _a;
        }
        template < >
        inline signed long long int abs(
                signed long long int const &  _a)
        {
            return  ( _a < 0) ? -_a : _a;
        }


    /*!
     * @brief
     *    integer division. given number a and b
     *    the function computes
     *    @n
     *    m = a mod b
     *
     *    and
     *
     *    d = ( a - ( sgn( a) * m)) / b
     *
     * @param
     *    a
     * @param
     *    b != 0
     * @param
     *    div (pointer to result d), if NULL d
     *    is not calculated.
     * @param
     *    mod (pointer to result m), if NULL m
     *    is not calculated.
     */
    template < typename  _T >
    void
    div_mod(
            _T  _a,  _T  _b,
            _T *  _div,  _T *  _mod)
    {
        crabmeat_assert( _b != 0);
        if ( _b == 0)
        {
            return;
        }
        if ( _div) *_div = _a / _b;
        if ( _mod) *_mod = _a % _b;
    }


    /*!
     * @brief
     *    return greatest common divisor
     */
    template < typename  _T >
    _T
    gcd(
            _T  _a, _T  _b)
    {
        _T  c;
        if (( _a == 0) && ( _b == 0))
        {
            return  (_T)1;
        }
        if ( _a == 0)
        {
            return  _b;
        }
        else
        {
            if ( _b == 0)
            {
                return  _a;
            }
        }

        _a = cbm::abs( _a);
        _b = cbm::abs( _b);
        if ( _a == _b)
        {
            return  _a;
        }

        /* swap numbers, if _a < _b */
        if ( _a < _b)
        {
            _T  a_tmp( _a);
            _a = _b;
            _b = a_tmp;
        }

        while ( 1)
        {
            c = _a % _b;
            if ( c == 0)
            {
                return  _b;
            }

            _a = _b;
            _b = c;
        }

        /* error: we should never get here (by definition gcd is always greater 0) */
        //return  (_T)0;
    }


    /*!
     * @brief
     *    return least common multiple
     */
    template < typename  _T >
    _T
    lcm(
        _T  _a, _T  _b)
    {
        return  ( _a * _b) / cbm::gcd( _a, _b);
    }

    /*!
     * @brief
     *    find equivalent fraction with smallest numbers
     */
    template < typename  _T >
    lerr_t
    simplify_fraction(
            _T *  _n /*numerator*/, _T *  _d /*denominator*/)
    {
        _T  this_gcd = cbm::gcd( *_n, *_d);
        if ( this_gcd == 1 || this_gcd == 0)
        {
            return  LDNDC_ERR_OK;
        }
        *_n /= this_gcd;
        *_d /= this_gcd;

        return  LDNDC_ERR_OK;
    }
}  /*  end namespace cbm  */


#endif  /*  !CBM_MATH_BASIC_H_  */

