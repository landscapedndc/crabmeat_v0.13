/*!
 * @brief
 *    random number related helper functions 
 *
 * @author
 *    steffen klatt,
 *    david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_RANDOM_H_
#define  CBM_MATH_RANDOM_H_

#include  "crabmeat-common.h"

#include  <limits>

namespace  cbm
{
    /*!
     * @brief
     *    namespace holding random number generators
     */
    namespace  random
    {
        extern  unsigned int CBM_API const  invalid_random_seed;
        /*!
         * @brief
         *    seed the random generator
         */
        void CBM_API  seed(
                unsigned int  _seed = invalid_random_seed);


        template < typename  _T >
        class rng_base_t
        {
            public:
                virtual ~rng_base_t() = 0;

                /*! return integral value of type _T */
                virtual _T  rand() = 0;
                /*! return floating point value in [0, 1] */
                virtual double  frand() = 0;

                /*! return maximum integral value */
                virtual _T  max() const = 0;
        };
        template < typename  _T >
        rng_base_t< _T >::~rng_base_t() {}

        /* George Marsaglia, 2003. Xorshift RNGs. Journal of Statistical Software 8, 14, 1-6 */
        class CBM_API xorshift32 : public rng_base_t< ldndc_uint32_t >
        {
            ldndc_uint32_t  x;
            public:
                xorshift32( ldndc_uint32_t  _seed=314159265u)
                    : rng_base_t< ldndc_uint32_t >(), x( _seed) {}

                ldndc_uint32_t  rand();
                double  frand();
                ldndc_uint32_t  max() const
                    { return std::numeric_limits< ldndc_uint32_t >::max(); }
        };
        class CBM_API xorshift64 : public rng_base_t< ldndc_uint64_t >
        {
            ldndc_uint64_t  x;
            public:
                xorshift64( ldndc_uint64_t  _seed=31415926535897932ull)
                    : rng_base_t< ldndc_uint64_t >(), x( _seed) {}

                ldndc_uint64_t  rand();
                double  frand();
                ldndc_uint64_t  max() const
                    { return std::numeric_limits< ldndc_uint64_t >::max(); }
        };
        class CBM_API xorshift128 : public rng_base_t< ldndc_uint32_t >
        {
            ldndc_uint32_t  x, y, z, w;
            public:
                xorshift128(
                    ldndc_uint32_t  _seedx=141592653u, ldndc_uint32_t  _seedy=897932384u,
                    ldndc_uint32_t  _seedz=264338327u, ldndc_uint32_t  _seedw=502884197u)
                    : rng_base_t< ldndc_uint32_t >(),
                      x( _seedx), y( _seedy), z( _seedz), w( _seedw) {}

                ldndc_uint32_t  rand();
                double  frand();
                ldndc_uint32_t  max() const
                    { return std::numeric_limits< ldndc_uint32_t >::max(); }
        };

        typedef  xorshift128  rng_uniform_t;
    }
}  /*  end namespace cbm  */



#endif  /*  !CBM_MATH_RANDOM_H_  */

