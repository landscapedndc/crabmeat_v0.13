/*!
 * @brief
 *    random number related helper functions (implementation)
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#include  "math/lmath-random.h"
#include  "log/cbm_baselog.h"

unsigned int const  cbm::random::invalid_random_seed =
    ldndc::invalid_t< unsigned int >::value;

#include  <time.h>
#include  <stdlib.h>
void
cbm::random::seed(
        unsigned int  _seed)
{
    unsigned int  this_seed = _seed;
    if ( this_seed == invalid_random_seed)
    {
        this_seed = static_cast< unsigned int >( ::time( NULL));
    }
    srand( this_seed);
}

ldndc_uint32_t
cbm::random::xorshift32::rand()
{
    this->x ^= this->x << 13;
    this->x ^= this->x >> 17;
    this->x ^= this->x << 5;

    return  this->x;
}
double
cbm::random::xorshift32::frand()
{
    return (double)this->rand() / (double)this->max();
}

ldndc_uint64_t
cbm::random::xorshift64::rand()
{
    this->x ^= this->x << 13;
    this->x ^= this->x >> 7;
    this->x ^= this->x << 17;

    return  this->x;
}
double
cbm::random::xorshift64::frand()
{
    return (double)this->rand() / (double)this->max();
}

ldndc_uint32_t
cbm::random::xorshift128::rand()
{
    ldndc_uint32_t const  t = this->x ^ ( this->x << 11);
    this->x = this->y;
    this->y = this->z;
    this->z = this->w;
    this->w ^= ( this->w >> 19) ^ t ^ ( t >> 8);

    return  this->w;
}
double
cbm::random::xorshift128::frand()
{
    return (double)this->rand() / (double)this->max();
}

