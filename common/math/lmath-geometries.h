/*!
 * @brief
 *  math related helper functions for geometry
 *
 * @authors
 *  steffen klatt,
 *  david kraus,
 *  edwin haas
 */

#ifndef  CBM_MATH_GEOMETRIES_H_
#define  CBM_MATH_GEOMETRIES_H_

#include  "crabmeat-common.h"
#include  "math/lmath-poly.h"
#include  "constants/lconstants-math.h"
#include  "log/cbm_baselog.h"


namespace  cbm
{
    /*!
     * @brief
     *    Returns cylinder volume from height and diameter
     */
    template < typename  _T >
    inline
    _T
    cylinder_volume(
                 _T const &  _d /* diameter */,
                 _T const &  _h /* height */)
    {
        return  cbm::sqr( _d) * cbm::PI * 0.25 * _h;
    }

    /*!
     * @brief
     *    Returns cone volume from height and diameter
     */
    template < typename  _T >
    inline
    _T
    conus_volume(
            _T const &  _d /* diameter */,
            _T const &  _h /* height */)
    {
        return  cbm::cylinder_volume(_d, _h) / 3.0;
    }

    /*!
     * @brief
     *    Returns cone height from diameter and volume
     */
    template < typename  _T >
    inline
    _T
    conus_height(
            _T const &  _d /* diameter */,
            _T const &  _v /* volume */)
    {
        return  _v / ( cbm::PI / 12.0 * cbm::sqr( _d));
    }

    /*!
     * @brief
     *    Returns cone diameter from height and volume
     */
    template < typename  _T >
    inline
    _T
    conus_diameter(
            _T const &  _h /* height */,
            _T const &  _v /* volume */)
    {
        return cbm::sqrt( _v / (cbm::PI / 12.0 * _h));
    }
}  /*  end namespace cbm  */


#endif  /*  !CBM_MATH_GEOMETRIES_H_  */

