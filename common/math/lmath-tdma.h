/*!
 * @brief 
 *    tridiagonal matrix solver
 *
 * @author
 *    David Kraus,
 *    Steffen Klatt (created on: nov 07, 2011),
 *    Edwin Haas
 */

#ifndef  CBM_MATH_SOLVER_TDMA_H_
#define  CBM_MATH_SOLVER_TDMA_H_

#include  "crabmeat-common.h"

/*!
 * @brief
 *    solve Ax = b for A tridiagonal
 *    @n
 *    Llewellyn Thomas algorithm (TDMA)
 */
class  CBM_API  solver_tridiagonal_t
{
    public:
        solver_tridiagonal_t(
                size_t);
        ~solver_tridiagonal_t();

// sk:todo        lerr_t  set_A(
// sk:todo                double *, double *, double *);
// sk:todo        lerr_t  set_b(
// sk:todo                double *);

        lerr_t  set_row(
                size_t,
                double, double, double,
                double);

        lerr_t  solve(
                double * = NULL) { return  LDNDC_ERR_FAIL; }
        lerr_t  solve_in_place(
                double * = NULL, size_t = 0);

        double  x(
                size_t) const;

        size_t  size() const;

    public:
        /* A, matrix diagonals ( A = diag( a0, a1, a2), where a0 is below main (=a1) diagonal) */
        double *  a0, *  a1, *  a2;
        /* b, rhs */
        double *  b;
    
    private:
        /* matrix size */
        size_t  n_;
        /* allocated memory (size 5n) */
        double *  mem_;
        /* x, result */
        double * x_;
};


#endif  /*  !CBM_MATH_SOLVER_TDMA_H_  */

