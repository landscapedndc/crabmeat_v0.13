/*!
 * @brief
 *    data type limits
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_LIMITS_H_
#define  CBM_MATH_LIMITS_H_

#include  "crabmeat-common.h"
#include  <limits>
#include  <climits>
#include  <cfloat>

#endif  /*  !CBM_MATH_LIMITS_H_  */

