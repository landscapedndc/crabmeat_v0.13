/*!
 * @brief
 *    computing harmonic and arithmetic means for
 *    various amounts of numbers
 *
 * @author
 *        david kraus,
 *        steffen klatt,
 *        edwin haas
 */

#ifndef  CBM_MATH_MEANS_H_
#define  CBM_MATH_MEANS_H_

#include  "crabmeat-common.h"
#include  "math/lmath-float.h"
#include  "math/lmath-array.h"
#include  "log/cbm_baselog.h"

namespace  cbm
{
    /*!
     * @brief
     *    returns weigthed harmonic mean of two numbers
     *
     * @param
     *    x1, x2 of same type
     * @param
     *    w1, w2 of same type
     */
    template < typename _T >
    _T
    harmonic_mean_weighted2(
            _T const &  _x1, _T const &  _x2,
            _T const &  _w1, _T const &  _w2)
    {
        if ( flt_greater_zero( _x1) && flt_greater_zero( _x2))
        {
            _T const  w_sum( _w1 + _w2);
            return flt_greater_zero( w_sum) ? ( w_sum / ( _w1 / _x1 + _w2 / _x2)) : 0.0;
        }
        else
        {
            return 0.0;
        }
    }
    /*!
     * @brief
     *    return harmonic mean of two numbers
     *
     * @param
     *    x1, x2 of same type
     */
    template < typename _T >
    _T
        harmonic_mean2(
            _T const &  _x1, _T const &  _x2)
    {
        return  harmonic_mean_weighted2( _x1, _x2, (_T)1, (_T)1);
    }

    /*!
     * @brief
     *    return weigthed harmonic mean of three numbers
     *
     * @param
     *    x1, x2, x3 of same type
     * @param
     *    w1, w2, w3 of same type
     */
    template < typename _T >
    _T
        harmonic_mean_weighted3(
            _T const &  _x1, _T const &  _x2, _T const &  _x3,
            _T const &  _w1, _T const &  _w2, _T const &  _w3)
    {
        crabmeat_assert( ((_x1<0.0) || (_x1>0.0)) && ((_x2<0.0) || (_x2>0.0)) && ((_x3<0.0) || (_x3>0.0)));
        _T const  w_sum( _w1 + _w2 + _w3);
        return  flt_greater_zero( w_sum) ? (w_sum / ( _w1/_x1 + _w2/_x2 + _w3/_x3)) : 0.0;
    }
    
    /*!
     * @brief
     *    return harmonic mean of three numbers
     *
     * @param
     *    x1, x2, x3 of same type
     */
    template < typename _T >
    _T
        harmonic_mean3(
            _T const &  _x1, _T const &  _x2, _T const &  _x3)
    {
        return  harmonic_mean_weighted3( _x1, _x2, _x3, (_T)1, (_T)1, (_T)1);
    }

    /*!
     * @brief
     *    return weigthed harmonic mean of four numbers
     *
     * @param
     *    x1, x2, x3, x4 of same type
     * @param
     *    w1, w2, w3, w4 of same type
     */
    template < typename _T >
    _T
        harmonic_mean_weighted4(
            _T const &  _x1, _T const &  _x2, _T const &  _x3, _T const &  _x4,
            _T const &  _w1, _T const &  _w2, _T const &  _w3, _T const &  _w4)
    {
        crabmeat_assert( ((_x1<0.0) || (_x1>0.0)) && ((_x2<0.0) || (_x2>0.0)) && ((_x3<0.0) || (_x3>0.0)) && ((_x4<0.0) || (_x4>0.0)));
        _T const  w_sum( _w1 + _w2 + _w3 + _w4);
        return  flt_greater_zero( w_sum) ? ( w_sum / ( _w1/_x1 + _w2/_x2 + _w3/_x3 + _w4/_x4)) : 0.0;
    }

    /*!
     * @brief
     *    return harmonic mean of four numbers
     *
     * @param
     *    x1, x2, x3, x4 of same type
     */
    template < typename _T >
    _T
        harmonic_mean4(
            _T const &  _x1, _T const &  _x2, _T const &  _x3, _T const &  _x4)
    {
        return  harmonic_mean_weighted4( _x1, _x2, _x3, _x4, (_T)1, (_T)1, (_T)1, (_T)1);
    }

    /*!
     * @brief
     *    return weigthed harmonic mean of n numbers
     *
     * @param
     *    array of numbers @p x of size @p n
     * @param
     *    array of weights @p w of size @p n
     * @param
     *    size of arrays @p x and @p w
     */
    template < typename _T >
    _T
    harmonic_mean_weighted(
            _T const *  _x,
            _T const *  _w,
            size_t  _n,
            _T const &  _eps = LDNDC_EPS(_T))
    {
        crabmeat_assert( _n);
        crabmeat_assert( _x);
        crabmeat_assert( _w);

        /* if the numerator is zero skip the rest */
        _T const  w_sum( ::cbm::sum( _w, _n));
        if ( ::cbm::flt_equal_zero( w_sum, _eps))
        {
            return  0.0;
        }

        /* calculate denominator */
        double  w_over_x_sum((_T)0);
        for ( size_t  k = 0;  k < _n;  ++k)
        {
            crabmeat_assert( _x[k] > 0.0);
            w_over_x_sum += (_T)_w[k] / _x[k];
        }

        /* previous checks imply denominator greater zero */
        return  w_sum / w_over_x_sum;
    }
    /*!
     * @brief
     *    return harmonic mean of n numbers
     *
     * @param
     *    array of numbers @p x of size @pn
     */
    template < typename _T >
    _T
    harmonic_mean(
            _T const *  _x,
            size_t  _n)
    {
        crabmeat_assert( _n);
        crabmeat_assert( _x);

        double  w_over_x_sum((_T)0);
        for ( size_t  k = 0;  k < _n;  ++k)
        {
            crabmeat_assert( _x[k] > 0.0);
            w_over_x_sum += (_T)1 / _x[k];
        }
        return  (_T)_n / w_over_x_sum;
    }



    
    /*!
     * @brief
     *    returns weigthed arithmetic mean of two numbers
     *
     * @param
     *    x1, x2 of same type
     */
    template < typename _T >
    _T
    arithmetic_mean_weighted2(
                   _T const &  _x1, _T const &  _x2,
                   _T const &  _w1, _T const &  _w2)
    {
        crabmeat_assert( flt_greater_zero( _w1+_w2));
        return  ( _w1*_x1 + _w2*_x2) / ( _w1 + _w2); 
    }
    /*!
     * @brief
     *    returns arithmetic mean of two numbers
     *
     * @param
     *    x1, x2 of same type
     */
    template< class _T >
    _T
    arithmetic_mean2(
                   _T const &  _x1, _T const &  _x2)
    {
        return  arithmetic_mean_weighted2( _x1, _x2, (_T)1, (_T)1);
    }
    
    /*!
     * @brief
     *    returns weigthed arithmetic mean of n numbers
     *
     * @param
     *    array of numbers @p x of size @p n
     * @param
     *    array of weights @p w of size @p n
     * @param
     *    size of arrays @p x and @p w
     */
    template < typename _T >
    _T
    arithmetic_mean_weighted(
                   _T const *  _x,
                   _T const *  _w,
                   size_t  _n)
    {
        crabmeat_assert( _n);
        crabmeat_assert( _x);
        crabmeat_assert( _w);
        
        _T const  w_sum( cbm::sum( _w, _n));
        if ( flt_equal_zero( w_sum))
        {
            /* FIXME  is this the correct limit? */
            return  cbm::sum( _x, _n) / (_T)_n;
        }

        _T  wx_sum( 0.0);
        for ( size_t  k = 0;  k < _n;  ++k)
        {
            wx_sum += _w[k]*_x[k];
        }
        
        return  wx_sum / w_sum; 
    }
    /*!
     * @brief
     *    returns arithmetic mean of n numbers
     *
     * @param
     *    array of numbers @p x of size @p n
     * @param
     *    size of arrays @p x
     */
    template < typename _T >
    _T
    arithmetic_mean_weighted(
                   _T const *  _x,
                   size_t  _n)
    {
        crabmeat_assert( _n);
        crabmeat_assert( _x);
        
        return  cbm::sum( _x, _n) / (_T)_n;
    }
}  /*  end namespace cbm  */


#endif  /*  !CBM_MATH_MEANS_H_  */

