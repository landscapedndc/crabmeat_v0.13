/*!
 * @brief
 *    bit twiddeling related helper functions 
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_BITS_H_
#define  CBM_MATH_BITS_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "cbm_types.h"

/*! shortcut to have an entry count in a power-of-2 enumeration
 *  e.g.: enum { e1=0, e2=1, e3=2, e4=4, e5=8, e_cnt=5 }
 */
#define  POWEROF2_ENUM_CNT(__nameprefix__)  \
    __##__nameprefix__##_sentinel, __nameprefix__##_CNT = cbm::log2_t< __##__nameprefix__##_sentinel - 1 >::LOG2 + 1 /*including 0*/ + 1 /*shift*/

namespace  cbm
{
    /*!
     * @brief
     *    find least significant bit in integer. rightmost
     *    position is m=1, m=0 means no bit set
     *
     * @note
     *    we assume bit order "msb .. lsb"
     *
     * @note
     *      see ffs, ffsl, ffsll
     */
    template < typename  _I >
        inline
        unsigned int
        lsb_pos(
                _I  _n)
        {
            unsigned int  m( 0);

            while ( _n)
            {
                ++m;
                if ( _n & (_I)1)
                {
                    break;
                }
                _n >>= 1;
            }
            return  m;
        }

    /*!
     * @brief
     *    find most significant bit in integer. rightmost
     *    position is m=1, m=0 means no bit set.
     *
     * @note
     *    we assume bit order "msb .. lsb"
     */
    template < typename  _I >
        inline
        unsigned int
        msb_pos(
                _I  _n)
        {
            unsigned int  m( 0);

            while ( _n)
            {
                ++m;
                _n >>= 1;
            }
            return  m;
        }


    /*!
     * @brief
     *    returns number of non-zero bits in integer
     */
    template < typename  _I >
        inline
        unsigned int
        bits_set(
                _I  _n)
        {
            unsigned int  m( 0);

            if ( _n)
            {
                while ( _n)
                {
                    if ( _n & (_I)1)
                    {
                        ++m;
                    }
                    _n >>= 1;
                }
            }
            return  m;
        }

    /*!
     * @brief
     *    return the first position where both given
     *    integer's bits are set.
     */
    template < typename  _I >
        inline
        unsigned int
        lsb_first_matching(
                _I  _n0, _I  _n1)
        {
            unsigned int  m( 0);

            _I  n( _n0);

            if ( _n0 & _n1)
            {
                while ( n)
                {
                    if (( _n0 & ((_I)1 << m)) && (( _n1 & ((_I)1 << m))))
                    {
                        return  m+1;
                    }
                    ++m;
                    n >>= 1;
                }
            }
            return  m;
        }

    /*!
     * @brief
     *      given a positive integer \f$n\f$ @fn int_log2
     *      computes the position (off by zero) of the most
     *      significant bit. in the special case \f$n = 2^m\f$,
     *      @fn int_log2 computes \f$m\f$.
     *
     * @param
     *    n, n>0
     * @note
     *      off by 0
     */
    inline
        unsigned int int_log2( unsigned int);

    /*!
     * @brief
     *    return log2 of a double precision floating
     *    point number
     */
    inline
        double log2( double);

    template < size_t  _K, size_t  _L, bool _GT >
        struct  __minmax_t
        { };
    template < size_t  _K, size_t  _L >
        struct  __minmax_t< _K, _L, true >
        {
            enum { MAX = _L, MIN = _K };
        };
    template < size_t  _K, size_t  _L >
        struct  __minmax_t< _K, _L, false >
        {
            enum { MAX = _K, MIN = _L };
        };
    template < size_t _K, size_t _L >
        struct  minmax_t
        {
            enum {
                MAX = __minmax_t< _K, _L, _K < _L >::MAX,
                MIN = __minmax_t< _K, _L, _K < _L >::MIN };
        };

    /*!
     * @brief
     *    compute log2(N) for constants at compile time
     */
    template < unsigned int N >
        struct  log2_t
        {
            enum { LOG2 = 1 + log2_t< N / 2 >::LOG2 };
        };

    template < >
        struct  log2_t< 1 >
        {
            enum { LOG2 = 0 };
        };

    template < >
        struct  log2_t< 0 >
        {
            enum { LOG2 = -1 };
        };

    /*!
     * @brief
     *    compute 2^N for constants at compile time
     */
    template < unsigned int N >
        struct  exp2_t
        {
            enum { EXP2 = 2 * exp2_t< N - 1 >::EXP2 };
        };

    template < >
        struct  exp2_t< 0 >
        {
            enum { EXP2 = 1 };
        };


    CBM_API char const *  __to_hex32_8( char * /*buffer*/, ldndc_uint32_t /*number*/);
    CBM_API char const *  __to_hex32_16( char * /*buffer*/, ldndc_uint32_t /*number*/);
    CBM_API char const *  __to_hex32_32( char * /*buffer*/, ldndc_uint32_t /*number*/);
    CBM_API char const *  __to_hex64_8( char * /*buffer*/, ldndc_uint64_t /*number*/);
    CBM_API char const *  __to_hex64_16( char * /*buffer*/, ldndc_uint64_t /*number*/);
    CBM_API char const *  __to_hex64_32( char * /*buffer*/, ldndc_uint64_t /*number*/);
    CBM_API char const *  __to_hex64_64( char * /*buffer*/, ldndc_uint64_t /*number*/);

    template < typename _T, size_t _W/*width of datatype*/, size_t _N/*bytes to convert*/ >
        struct  __to_hex_t
        {
            /* nothing here */
        };
    /* 32 bits */
    template < typename _T >
        struct  __to_hex_t< _T, 4, 1 >
        {
            static char const *  convert( char *  _buffer, _T  _value)
                { return  __to_hex32_8( _buffer /*2 bytes*/, _value); }
        };
    template < typename _T >
        struct  __to_hex_t< _T, 4, 2 >
        {
            static char const *  convert( char *  _buffer, _T  _value)
                { return  __to_hex32_16( _buffer /*4 bytes*/, _value); }
        };
    template < typename _T >
        struct  __to_hex_t< _T, 4, 4 >
        {
            static char const *  convert( char *  _buffer, _T  _value)
                { return  __to_hex32_32( _buffer /*8 bytes*/, _value); }
        };
    /* 64 bits */
    template < typename _T >
        struct  __to_hex_t< _T, 8, 1 >
        {
            static char const *  convert( char *  _buffer /*2 bytes*/, _T  _value)
                { return  __to_hex64_8( _buffer, _value); }
        };
    template < typename _T >
        struct  __to_hex_t< _T, 8, 2 >
        {
            static char const *  convert( char *  _buffer /*4 bytes*/, _T  _value)
                { return  __to_hex64_16( _buffer, _value); }
        };
    template < typename _T >
        struct  __to_hex_t< _T, 8, 4 >
        {
            static char const *  convert( char *  _buffer /*8 bytes*/, _T  _value)
                { return  __to_hex64_32( _buffer, _value); }
        };
    template < typename _T >
        struct  __to_hex_t< _T, 8, 8 >
        {
            static char const *  convert( char *  _buffer /*16 bytes*/, _T  _value)
                { return  __to_hex64_64( _buffer, _value); }
        };

    /*!
     * @brief
     *  convert integral number to string. no trailing
     *  nul-byte is appended.
     *
     * @return
     *  number of written bytes
     */
    template < size_t _N = 1024 /*force sizeof(_T) */ >
        struct  stringify_t
        {
            template < typename _T >
                static size_t  to_hex( char *  _buffer, _T  _value)
                {
                    __to_hex_t< _T, sizeof(_T),
                        minmax_t< _N, sizeof(_T) >::MIN >::convert( _buffer, _value);
                    return  2*minmax_t< _N, sizeof(_T) >::MIN;
                }
        };
}  /*  end namespace cbm  */

namespace cbm {

    /* from https://github.com/preshing/CompareIntegerMaps/blob/master/util.h */
    inline uint32_t  upper_power_of_two( uint32_t  _v)
    {
        _v--;
        _v |= _v >> 1;
        _v |= _v >> 2;
        _v |= _v >> 4;
        _v |= _v >> 8;
        _v |= _v >> 16;
        _v++;
        return  _v;
    }

    /* from https://github.com/preshing/CompareIntegerMaps/blob/master/util.h */
    inline uint64_t  upper_power_of_two( uint64_t  _v)
    {
        _v--;
        _v |= _v >> 1;
        _v |= _v >> 2;
        _v |= _v >> 4;
        _v |= _v >> 8;
        _v |= _v >> 16;
        _v |= _v >> 32;
        _v++;
        return  _v;
    }
} /* namespace cbm */


#endif  /*  !CBM_MATH_BITS_H_  */

