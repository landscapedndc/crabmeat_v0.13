/*!
 * @brief
 *    math related helper functions for arrays
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_ARRAY_H_
#define  CBM_MATH_ARRAY_H_

#include  "crabmeat-common.h"

namespace  cbm
{
    /* c-array convenience functions */
        template < typename  _T/*,  bool  sorted = false*/ >
        _T const &
        min(
                        _T const *  _arr,
                        size_t  _fin,
                        size_t  _offs = 0,
                        size_t  _stride = 1)
    {
        if (( _fin == 0u) || ( _offs >= _fin) || ( _stride == 0))
        {
            return  ldndc::invalid_t< _T >::value;
        }

        size_t  l = _offs;
        for ( size_t  k = _offs+1;  k < _fin;  k+=_stride)
        {
            if ( _arr[l] > _arr[k])
            {
                l = k;
            }
        }
        return  _arr[l];
    }

        template < typename  _T/*,  bool  sorted = false*/ >
        _T const &
        max(
                        _T const * _arr,
                        size_t  _fin,
                        size_t  _offs = 0,
                        size_t  _stride = 1)
    {
        if (( _fin == 0u) || ( _offs >= _fin) || ( _stride == 0))
        {
            return  ldndc::invalid_t< _T >::value;
        }

        size_t  l = _offs;
        for ( size_t  k = _offs+1;  k < _fin;  k+=_stride)
        {
            if ( _arr[l] < _arr[k])
            {
                l = k;
            }
        }
        return  _arr[l];
    }

    /*!
     * @brief
     *    sum elements of an array
     *
     * @return
     *    sum of array
     *
     * @param  _arr
     * @param  _size
     * @param  _offset
     * @param  _stride
     */
        template < typename  _T/*,  bool  sorted = false*/ >
        _T
        sum(
                        _T const * _arr,
                        size_t  _fin,
                        size_t  _offs = 0,
                        size_t  _stride = 1)
    {
        if (( _fin == 0u) || ( _offs >= _fin) || ( _stride == 0))
        {
            return  ldndc::invalid_t< _T >::value;
        }

        _T  a_sum( _T( 0));
        for ( size_t  k = _offs;  k < _fin;  k+=_stride, _arr+=_stride)
        {
            a_sum += *_arr;
        }
        return  a_sum;
    }

}  /*  end namespace cbm  */


#endif  /*  !CBM_MATH_ARRAY_H_  */

