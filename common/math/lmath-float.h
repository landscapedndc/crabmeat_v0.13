/*!
 * @brief
 *    math related helper functions dealing with
 *    floating point numbers
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_FLOATS_H_
#define  CBM_MATH_FLOATS_H_

#include  "math/lmath-basic.h"
#include  "math/lmath-limits.h"
#include  "log/cbm_baselog.h"

#define  LDNDC_EPS(__T__)  std::numeric_limits< __T__ >::epsilon()

namespace  cbm
{
    /*!
     * @brief
     *    check if a floating point number f1 is within
     *    a range around f2. size of range is given by
     *    an epsilon.
     *
     * @return
     *    true, if |f1 - f2| < eps, false otherwise
     */
    template < typename  _T >
    bool flt_equal_eps(
        _T  _f1, _T  _f2, _T  _eps = LDNDC_EPS(_T))
    {
        return  (( fabs( _f1) < _eps) && ( fabs( _f2) < _eps))
            || fabs(( _T)( _f1 - _f2)) < _eps;
    }

    /*!
     * @brief
     *    same as @fn flt_equal_eps except instead of
     *    providing an epsilon, a multiplier for the
     *    machine epsilon is used.
     *
     * @todo
     *    remove?
     */
    template < typename  _T >
    bool flt_equal(
        _T  _f1, _T  _f2, _T  _mul = (_T)10.0)
    {
        return  flt_equal_eps( _f1, _f2, _mul * LDNDC_EPS(_T));
    }

    /*!
     * @brief
     *    checks if f1 is "significantly" greater than f2.
     *
     * @return
     *    true, if f1 > f2+eps, false otherwise.
     */
    template < typename  _T >
    bool flt_greater(
        _T  _f1, _T  _f2, _T  _eps = (_T)0)
    {
        return  _f1 > ( _f2 + _eps);
    }

    /*!
     * @brief
     *    checks if f1 is "significantly" greater or
     *    float-equal to f2
     */
    template < typename  _T >
    bool flt_greater_equal(
        _T  _f1, _T  _f2, _T  _eps = LDNDC_EPS(_T))
    {
        return  flt_greater( _f1, _f2, 0.0) || flt_equal_eps( _f1, _f2, _eps);
    }

    /*!
     * @brief
     *    checks if f1 is not float-greater-equal than f2
     */
    template < typename  _T >
    bool flt_less(
        _T  _f1, _T  _f2, _T  _eps = (_T)0)
    {
        return  _f1 < ( _f2 - _eps);
    }

    /*!
     * @brief
     *    checks if f1 is not float-greater than f2
     */
    template < typename  _T >
    bool flt_less_equal(
        _T  _f1, _T  _f2, _T  _eps = LDNDC_EPS(_T))
    {
        return  flt_less( _f1, _f2, 0.0) || flt_equal_eps( _f1, _f2, _eps);
    }

    /*!
     * @brief
     *    alias for float-equal where f2=0
     */
    template < typename  _T >
    bool flt_equal_zero(
        _T  _f, _T  _eps = LDNDC_EPS(_T))
    {
        return  flt_equal_eps(( _T)0.0, _f, _eps);
    }

    /*!
     * @brief
     *    alias for not-float-equal where f2=0
     */
    template < typename  _T >
    bool flt_not_equal_zero(
        _T  _f, _T  _eps = LDNDC_EPS(_T))
    {
        return  ! flt_equal_eps(( _T)0.0, _f, _eps);
    }

    /*!
     * @brief
     *    alias for float-greater where f2=0
     */
    template < typename  _T >
    bool flt_greater_zero(
        _T  _f, _T  _eps = LDNDC_EPS(_T))
    {
        return  flt_greater( _f, 0.0, _eps);
    }

    /*!
     * @brief
     *    TODO
     */
    template < typename  _T >
    bool flt_greater_equal_zero( _T  _f)
    {
        /* 0.0 == -0.0 */
        int const  f_sgn( ::cbm::sign( _f));
        return  ( f_sgn != (int)-1) || (( f_sgn == (int)-1) && (_f >= 0.0));
    }

    /*!
     * @brief
     *    check if floating point number f is within certain bounds
     *    (exclusively/exclusively)
     *
     * @return
     *    true, if f in (lb, ub), false otherwise
     */
    template < typename  _T >
    bool flt_in_range(
        _T  _lb, _T  _f, _T  _ub)
    {
        return  ( _f > _lb) && ( _ub > _f);
    }

    /*!
     * @brief
     *    check if floating point number f is within certain bounds
     *    (inclusively/exclusively)
     *
     * @return
     *    true, if f in [lb, ub), false otherwise
     */
    template < typename  _T >
    bool flt_in_range_l(
        _T  _lb, _T  _f, _T  _ub)
    {
        return  ( _f >= _lb) && ( _ub > _f);
    }

    /*!
     * @brief
     *    check if floating point number f is within certain bounds
     *    (exclusively/inclusively)
     *
     * @return
     *    true, if f in (lb, ub], false otherwise
     */
    template < typename  _T >
    bool flt_in_range_u(
        _T  _lb, _T  _f, _T  _ub)
    {
        return  ( _f > _lb) && ( _ub >= _f);
    }

    /*!
     * @brief
     *    check if floating point number f is within certain bounds
     *    (inclusively/inclusively)
     *
     * @return
     *    true, if f in [lb, ub], false otherwise
     */
    template < typename  _T >
    bool flt_in_range_lu(
        _T  _lb, _T  _f, _T  _ub)
    {
        return  ( _f >= _lb) && ( _ub >= _f);
    }


    /*!
     * @brief
     *    round towards nearest integral value
     */
    template < typename _T >
    inline _T round( _T const &  x)
    {
        // !!! NOTE expect wrong results when _T is integer type
        //CBM_Assert( abs( x) > (_T)LONG_MAX);

        //long int  n( x);

        /* rounding up if  x > floor( x) + 0.5  ( taken from old model mxxx) */
        //return  (double)( n + (( x - (double)(n) > 0.5) ? n + 1 : n));

        /* fails when number to large */
        //return  (double)((long int)( x + 0.5));

        /* rounds up if  x >= floor( x) + 0.5 */
        return  std::floor( x + 0.5);
    }


    /*!
     * @brief
     *    return the greater of the two values. this
     *    function adds verbosity to the process of
     *    limiting a numbers value.
     */
    template < typename  _T >
    inline _T const & bound_min(
        _T const &  _min, _T const &  _val)
    {
        return  ( _val < _min) ? _min : _val;
    }

    /*!
     * @brief
     *    return the smaller of two values. this
     *    function adds verbosity to the process of
     *    limiting a numbers value.
     */
    template < typename  _T >
    inline _T const & bound_max(
        _T const &  _val, _T const &  _max)
    {
        return  ( _val > _max) ? _max : _val;
    }

    /*!
     * @brief
     *    if the number @p val is out of bounds [@p min,
     *    @p max], return either @p min or @p max depending
     *    on what side @p val left the range. return @p val
     *    otherwise.
     */
    template < typename  _T >
    inline _T const & bound(
        _T const &  _min, _T const &  _val, _T const &  _max)
    {
        CBM_Assert( _max >= _min);
        return  bound_min( _min, bound_max( _val, _max));
    }

    /*!
     * @brief
     *    if the number @p val is less than @p limit, return
     *    @p lower. return @p val otherwise.
     *
     * @note
     *    if returning @p lower then l < v < L
     */
    template < typename  _T >
    inline _T const & round_to_lower(
        _T const &  _lower, _T const &  _val, _T const &  _limit)
    {
        CBM_Assert( _lower <= _limit);
        return  ( _val < _limit) ? _lower : _val;
    }

    /*!
     * @brief
     *    if the number @p val is greater than @p limit, return
     *    @p upper. return @p val otherwise.
     *
     * @note
     *    if returning @p upper then l < v < u
     */
    template < typename  _T >
    inline _T const & round_to_upper(
        _T const &  _limit, _T const &  _val, _T const &  _upper)
    {
        CBM_Assert( _upper >= _limit);
        return  ( _val > _limit) ? _upper : _val;
    }

    /*!
     * @brief
     *    computes
     *        _from := _from - _amount
     *    and
     *        _to := _to + _amount;
     */
    template < typename _T >
    void transfer(
        _T *  _from, _T *  _to, _T const &  _amount)
    {
        if ( _from != _to)
        {
            if ( cbm::flt_equal( *_from, _amount))
            {
                *_from = 0;
            }
            else
            {
                *_from -= _amount;
            }
            if ( cbm::flt_equal( *_to, -_amount))
            {
                *_to = 0;
            }
            else
            {
                *_to += _amount;
            }
        }
    }


}  /*  end namespace cbm  */


#endif  /*  !CBM_MATH_FLOATS_H_  */

