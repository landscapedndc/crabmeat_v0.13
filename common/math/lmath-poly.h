/*!
 * @brief
 *    math related helper functions for polynomials
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_POLYNOMIALS_H_
#define  CBM_MATH_POLYNOMIALS_H_

#include  "crabmeat-common.h"
#include  "log/cbm_baselog.h"

namespace  cbm
{
    /*!
     * @brief
     *    return the square of a number
     */
        template < typename  _T >
        inline
        _T
        sqr(
                        _T const &  _n)
        {
                return  _n * _n;
        }

    /*!
     * @brief
     *    return the square root of a number
     */
        template < typename  _T >
        inline
        _T
        sqrt(
                        _T const &  _n)
        {
                return  std::sqrt( _n);
        }


    /*!
     * @brief
     *    returns the value of a second order polynomial
     *    evaluated at point x. coefficients c_j are given
     *    starting at zero (c0).
     * @n
     *    f(x) = sum( x^j * c_j) : j = 0..2
     */
        template < typename  _T >
        inline
        _T
        poly2(
                        _T const  _x,
                        _T const  _c0,
                        _T const  _c1,
                        _T const  _c2)
        {
                return  _c0 + _x * ( _c1 + _x * _c2);
        }

    /*!
     * @brief
     *    array version of poly2
     */
    template < typename  _T >
    inline
    _T
    poly2(
            _T const  _x,
                        _T const *  _c)
        {
        crabmeat_assert( _c);
                return  cbm::poly2( _x, _c[0], _c[1], _c[2]);
        }


    /*!
     * @brief
     *    returns the value of a third order polynomial
     *    evaluated at point x. coefficients c_j are given
     *    starting at zero (c0).
     * @n
     *    f(x) = sum( x^j * c_j) : j = 0..3
     */
    template < typename  _T >
    inline
    _T
    poly3(
            _T const  _x,
                        _T const  _c0,
                        _T const  _c1,
                        _T const  _c2,
                        _T const  _c3)
        {
                return  _c0 + _x * ( _c1 + _x * ( _c2 + _x * _c3));
        }


    /*!
     * @brief
     *    array version of poly3
     */
    template < typename  _T >
    inline
    _T
    poly3(
            _T const  _x,
                        _T const *  _c)
    {
        crabmeat_assert( _c);
        return  cbm::poly3( _x, _c[0], _c[1], _c[2], _c[3]);
    }


    /*!
     * @brief
     *    returns the value of a fourth order polynomial
     *    evaluated at point x. coefficients c_j are given
     *    starting at zero (c0).
     * @n
     *    f(x) = sum( x^j * c_j) : j = 0..4
     */
    template < typename  _T >
    inline
    _T
    poly4(
            _T const  _x,
                        _T const  _c0,
                        _T const  _c1,
                        _T const  _c2,
                        _T const  _c3,
                        _T const  _c4)
        {
                return  _c0 + _x * ( _c1 + _x * ( _c2 + _x * ( _c3 + _x * _c4)));
        }


    /*!
     * @brief
     *    array version of poly4
     */
    template < typename  _T >
    inline
    _T
    poly4(
            _T const  _x,
                        _T const *  _c)
    {
        crabmeat_assert( _c);
        return  cbm::poly4( _x, _c[0], _c[1], _c[2], _c[3], _c[4]);
    }


    /*!
     * @brief
     *    returns the value of an m=n-1 order polynomial
     *    evaluated at point x. coefficients c_j are given
     *    starting at zero (c0).
     * @n
     *    f(x) = sum( x^j * c_j) : j = 0..m
     */
    template < typename  _T >
    inline
    _T
    polyn(
            _T const  _x,
                        _T const *  _c,
            size_t  _n)
    {
        crabmeat_assert( _n);
        crabmeat_assert( _c);

        _T  res( 0);
        int  j( static_cast< int >( _n));

        while ( --j)
        {
            res = _c[j] + _x * res;
        }

        return  _c[0] + _x * res;
    }
}  /*  end namespace cbm  */


#endif  /*  !CBM_MATH_POLYNOMIALS_H_  */

