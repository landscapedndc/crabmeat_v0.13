/*!
 * @brief
 *    math related helper functions, convenience header
 *    pulling all math headers except solvers.
 *
 * @author
 *      steffen klatt,
 *      david kraus,
 *    edwin haas
 */

#ifndef  CBM_MATH_H_
#define  CBM_MATH_H_

#include  "crabmeat-common.h"

#include  "math/lmath-array.h"
#include  "math/lmath-basic.h"
#include  "math/lmath-bits.h"
#include  "math/lmath-float.h"
#include  "math/lmath-geometries.h"
#include  "math/lmath-limits.h"
#include  "math/lmath-means.h"
#include  "math/lmath-poly.h"
#include  "math/lmath-random.h"
#include  "math/lmath-tdma.h"


#endif  /*  !CBM_MATH_H_  */

