/*!
 * @author
 *    David Kraus,
 *    Steffen Klatt (created on: nov 07, 2011),
 *    Edwin Haas
*/

#include  "math/lmath-tdma.h"

#include  "log/cbm_baselog.h"
#include  "memory/cbm_mem.h"


solver_tridiagonal_t::solver_tridiagonal_t(
                        size_t  _n)
                : a0( NULL), a1( NULL), a2( NULL),
                  b( NULL),
                  n_( _n),
                  mem_( NULL),
                  x_( NULL)
{
    /* allocate memory for all arrays at once and assign chunks later */
    mem_ = CBM_DefaultAllocator->allocate_init_type< double >( 5*this->n_, 0.0);
    if ( !mem_)
    {
        CBM_LogFatal( "TDMA solver failed to allocate memory");
    }

    /* assign chunks from previously allocated buffer */
    a0 = mem_;
    a1 = a0 + n_;
    a2 = a1 + n_;
    x_ = a2 + n_;
    b = x_ + n_;
}

solver_tridiagonal_t::~solver_tridiagonal_t()
{
        CBM_DefaultAllocator->deallocate( mem_);
}


lerr_t
solver_tridiagonal_t::set_row(
                size_t  _j,
                double  _a0, double  _a1, double  _a2,
                double  _b)
{
    if ( _j < n_)
    {
        b[_j]  = _b;
        a0[_j] = _a0;
        a1[_j] = _a1;
        a2[_j] = _a2;
    }
    else
    {
        CBM_LogFatal( "[BUG] tdma solver update: index too large");
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
solver_tridiagonal_t::solve_in_place(
                                     double *  _x, size_t _n)
{
    double *  x_loc(( _x) ? _x : x_);
    size_t n( (_n == 0) ? n_ : _n);
    
    /* crash on _a1 = 0 */
    b[0] = b[0] / a1[0];
    a2[0] = a2[0] / a1[0];
    

    for ( size_t  k = 0, j = 1;  j < n;  ++k, ++j)
    {
        a2[j] = a2[j] / ( a1[j] - ( a2[k] * a0[j]));
        b[j]  = ( b[j] - (b[k]*a0[j])) / ( a1[j] - ( a2[k]*a0[j]));
    }

    
    /* back substitution */
    x_loc[n-1] = b[n-1];
    for ( size_t  k = 1;  k < n;  ++k)
    {
        size_t const  l( n - ( k + 1));
        x_loc[l] = b[l] - ( a2[l] * x_loc[l+1]);
    }
    
    return  LDNDC_ERR_OK;
}


double
solver_tridiagonal_t::x(
                size_t  _j)
const
{
        return  x_[_j];
}


size_t
solver_tridiagonal_t::size()
const
{
        return  n_;
}

