/*!
 * @brief
 *  provides interface to python interpreter engine
 *
 * @author
 *  steffen klatt (created on: nov. 09, 2016)
 *
 * @TODO
 *  function pointer handles
 *  lock ref-count, automatic release on destruct
 */

#ifndef  CRABMEAT_PYTHON_H_
#define  CRABMEAT_PYTHON_H_

#include  <string>

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

class CBM_API cbm_python_t
{
public:
    static int  initialize( char * /*progam name*/);
    static void  finalize();

    explicit  cbm_python_t( char const * /*name*/);
    ~cbm_python_t();

    int  execute( char const * /*script*/);
    /* using json formatted arguments */
    int  call_function( char const * /*function name*/,
        char const * /*argument buffer*/, char ** /*result buffer*/);
    int  execute_function( char const * /*script*/,
            char const * /*function name*/,
        char const * /*argument buffer*/, char ** /*result buffer*/);

//    template < typename _Ret, typename _Arg >
//        int  execute( char const * /*script*/,
//            char const * /*function name*/,
//            _Ret * /*return value*/, _Arg /*arg*/);
//    template < typename _Ret, typename _Arg1, typename _Arg2 >
//        int  execute( char const * /*script*/,
//            _Ret * /*return value*/, _Arg1 /*arg1*/, _Arg2 /*arg2*/);

private:
    std::string  m_name;
    void *  m_pyengine;
private:
    /* hide */
    cbm_python_t( cbm_python_t const &);
    cbm_python_t &  operator=( cbm_python_t const &);
};


#endif  /*  !CRABMEAT_PYTHON_H_  */

