/*!
 * @brief
 */

#include  <Python.h>

#include  "scripting/cbm_python.h"
#include  "log/cbm_baselog.h"
#include  "memory/cbm_mem.h"


static PyThreadState *  CBM_PyMainThread;
#define  AsPyThreadState(ptr) ((PyThreadState*)(ptr))
int
cbm_python_t::initialize( char *  _program_name)
{
    Py_SetProgramName( _program_name);
    Py_InitializeEx( 1);
    PyEval_InitThreads();
    CBM_PyMainThread = PyEval_SaveThread();
    CBM_LogDebug( "Python mainthread @",(void*)CBM_PyMainThread);
    return  Py_IsInitialized() ? 0 : -1;
}
void
cbm_python_t::finalize()
{
    PyEval_RestoreThread( CBM_PyMainThread);
    Py_Finalize();
}

cbm_python_t::cbm_python_t( char const *  _name)
    : m_name( _name ? _name : ":noname:")
{
    PyEval_AcquireLock();
    this->m_pyengine = Py_NewInterpreter();
    PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
    if ( !this->m_pyengine)
        { CBM_LogError( "Failed to create new interpreter"); }
//    CBM_LogDebug( "Python path: ",Py_GetPath());
}

cbm_python_t::~cbm_python_t()
{
    PyEval_AcquireThread( AsPyThreadState(this->m_pyengine));
    Py_EndInterpreter( AsPyThreadState(this->m_pyengine));
    this->m_pyengine = 0;
    PyEval_ReleaseLock();
}

int
cbm_python_t::execute( char const *  _script)
{
    CBM_Assert( _script);

    PyEval_AcquireThread( AsPyThreadState(this->m_pyengine));
    int  rc = PyRun_SimpleString( _script);
    PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));

    return  rc;
}

static int
cbm_python_call_function( char const * _function_name,
    char const *  _args, PyObject **  _ret_value)
{
    CBM_Assert( _function_name);

    PyObject *  main_module = PyImport_AddModule( "__main__");
    if ( !main_module)
    {
        CBM_LogError( "Failed to add module \"__main__\"");
        return  -1;
    }
    PyObject *  fn_ptr =
        PyObject_GetAttrString( main_module, _function_name);
    if ( !fn_ptr)
    {
        CBM_LogError( "Failed to get pointer to",
            " function \"", _function_name, "\"");
        Py_XDECREF( fn_ptr);
        return  -1;
    }
    CBM_LogDebug( "Got function pointer [",fn_ptr,"]",
            " for \"",_function_name, "\"");

    PyObject *  py_args = NULL;
    if ( _args)
    {
        py_args = PyTuple_New( 1);
        PyObject *  py_arg = Py_BuildValue( "s", _args);
        PyTuple_SetItem( py_args, 0, py_arg);
    }
    *_ret_value =
            PyObject_CallObject( fn_ptr, py_args);
    Py_XDECREF( py_args);
    Py_XDECREF( fn_ptr);
    if ( *_ret_value == NULL)
    {
        CBM_LogError( "Return value is NULL");
        return  -1;
    }
    return  0;
}
        
int
cbm_python_t::call_function( char const * _function_name,
    char const *  _args, char **  _rv)
{
    CBM_Assert( _function_name);

    PyEval_AcquireThread( AsPyThreadState(this->m_pyengine));

    PyObject *  ret_value = NULL;
    int  rc_call = cbm_python_call_function(
            _function_name, _args, &ret_value);
    if ( rc_call)
    {
        if ( ret_value != NULL)
            { Py_DECREF( ret_value); }
        PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
        return  -1;
    }
    else if ( _rv)
    {
        size_t const  result_sz =
                strlen( PyString_AsString( ret_value));
//        CBM_LogDebug( "Result [",result_sz,"]: ", PyString_AsString( ret_value));
        *_rv = (char *)CBM_Allocate( result_sz+1);
        if ( !*_rv)
        {
            CBM_LogError( "Failed to allocate result buffer");
            Py_DECREF( ret_value);
            PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
            return  -1;
        }

        memcpy( *_rv, PyString_AsString( ret_value), result_sz);
        (*_rv)[result_sz] = '\0';
    }
    if ( ret_value != NULL)
        { Py_DECREF( ret_value); }

    PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
    return  0;
}

int
cbm_python_t::execute_function( char const * _script,
            char const *  _function_name,
    char const *  _args, char **  _rv)
{
    int  rc_exe = this->execute( _script);
    if ( rc_exe)
        { return  -1; }
    int  rc_callfn = this->call_function(
                _function_name, _args, _rv);
    if ( rc_callfn)
        { return  -1; }
    return  0;
}


/* http://stackoverflow.com/questions/3789881/create-and-call-python-function-from-string-via-c-api */
// sk:off int
// sk:off cbm_python_t::execute( char const *  _script,
// sk:off     char const * _function_name,
// sk:off     char const *  /*_args*/, char **  _rv)
// sk:off {
// sk:off     CBM_Assert( _script);
// sk:off     CBM_Assert( _function_name);
// sk:off 
// sk:off     PyEval_AcquireThread( AsPyThreadState(this->m_pyengine));
// sk:off 
// sk:off     PyObject *  main_module = PyImport_AddModule( "__main__");
// sk:off     PyObject *  global_dict = PyModule_GetDict( main_module);
// sk:off     if ( !global_dict)
// sk:off     {
// sk:off         CBM_LogError( "Failed to create global dictionary");
// sk:off         PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off         return  -1;
// sk:off     }
// sk:off 
// sk:off     PyObject *  local_module = PyModule_New( _function_name);
// sk:off     if ( !local_module)
// sk:off     {
// sk:off         CBM_LogError( "Failed to create Module \"",
// sk:off                 _function_name, "\"");
// sk:off         PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off         return  -1;
// sk:off     }
// sk:off     PyModule_AddStringConstant( local_module, "__file__", "");
// sk:off     PyObject *  local_dict = PyModule_GetDict( local_module);
// sk:off     if ( !local_dict)
// sk:off     {
// sk:off         CBM_LogError( "Failed to get Module \"",
// sk:off                _function_name, "\" dictionary");
// sk:off         PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off         return  -1;
// sk:off     }
// sk:off 
// sk:off     PyObject *  py_value = PyRun_String( _script,
// sk:off             Py_file_input, global_dict, local_dict);
// sk:off     Py_DECREF( py_value);
// sk:off 
// sk:off     PyObject *  fn_ptr =
// sk:off         PyObject_GetAttrString( local_module, _function_name);
// sk:off     if ( !fn_ptr)
// sk:off     {
// sk:off         CBM_LogError( "Failed to get pointer to",
// sk:off             " function \"", _function_name, "\"");
// sk:off         PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off         return  -1;
// sk:off     }
// sk:off     CBM_LogDebug( "Got function pointer [",fn_ptr,"]",
// sk:off             " for \"",_function_name, "\"");
// sk:off 
// sk:off //  TODO
// sk:off     PyObject *  args = NULL;
// sk:off //    PyObject *  args = PyTuple_New( 1);
// sk:off //    py_value = ... convert argument
// sk:off //    PyTuple_SetItem( args, 0, arg0);
// sk:off 
// sk:off     py_value = PyObject_CallObject( fn_ptr, args);
// sk:off     if ( py_value == NULL)
// sk:off     {
// sk:off         CBM_LogError( "Return value is NULL");
// sk:off         PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off         return  -1;
// sk:off     }
// sk:off     size_t const  result_sz =
// sk:off             strlen( PyString_AsString( py_value));
// sk:off     CBM_LogDebug( "Result [",result_sz,"]: ", PyString_AsString( py_value));
// sk:off     *_rv = (char *)CBM_Allocate( result_sz+1);
// sk:off     if ( !*_rv)
// sk:off     {
// sk:off         CBM_LogError( "Failed to allocate result buffer");
// sk:off         PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off         return  -1;
// sk:off     }
// sk:off 
// sk:off     memcpy( *_rv, PyString_AsString( py_value), result_sz);
// sk:off     Py_DECREF( py_value);
// sk:off 
// sk:off //  TODO
// sk:off //    Py_DECREF( args);
// sk:off 
// sk:off     Py_XDECREF( fn_ptr);
// sk:off     Py_DECREF( local_module);
// sk:off 
// sk:off 
// sk:off     PyEval_ReleaseThread( AsPyThreadState(this->m_pyengine));
// sk:off     return  0;
// sk:off }

