/*!
 * @author
 *    steffen klatt  (created on: jul 10, 2017)
 */

#include  "stats/cbm_stats.h"

int cbm::have_resources()
#ifdef  CRABMEAT_MONITOR_RESOURCES
    { return 1; }
#else
    { return 0; }
#endif

