/*!
 * @author 
 *    steffen klatt (created on: sep 04, 2013),
 *    edwin haas
 */

#include  "stats/resmon.h"


cbm::time_monitor_t::time_monitor_t()
{
    this->reset();
}
cbm::time_monitor_t::~time_monitor_t()
{ }

void
cbm::time_monitor_t::reset()
{
    this->ts.nb = 0;
    this->ts.tic = 0;
    this->ts.to = 0;
    this->ts.mi = 0.0;
    this->ts.ma = 0.0;
}


int
cbm::time_monitor_t::update( time_type  _t)
{
    this->ts.nb += 1;
    this->ts.to += _t;

    if ( this->ts.nb == 1)
    {
        this->ts.mi = _t;
        this->ts.ma = _t;
        return 0;
    }

    if ( this->ts.mi > _t)
    {
        this->ts.mi = _t;
        return -1;
    }
    else if ( this->ts.ma < _t)
    {
        this->ts.ma = _t;
        return 1;
    }
    else
        { /* no op */ }

    return -2;
}




#include  "stats/cputime.h"
void
cbm::cputime_monitor_t::tic()
{
    this->ts.tic = cbm::cputime_nanosecond();
}

void
cbm::cputime_monitor_t::toc()
{
    this->update( cbm::cputime_nanosecond() - this->ts.tic);
}



    
#include  "stats/walltime.h"
void
cbm::walltime_monitor_t::tic()
{
    this->ts.tic = cbm::walltime_nanosecond();
}

void
cbm::walltime_monitor_t::toc()
{
    this->update( cbm::walltime_nanosecond() - this->ts.tic);
}




cbm::resources_statistics_t::resources_statistics_t()
{
    this->reset();
}
void
cbm::resources_statistics_t::reset()
{
    this->ct.reset();
    this->wt.reset();

    this->tictocs = 0;
}


void
cbm::resources_statistics_t::tic()
{
    this->ct.tic();
    this->wt.tic();
}

void
cbm::resources_statistics_t::toc()
{
    this->tictocs += 1;

    this->ct.toc();
    this->wt.toc();
}

