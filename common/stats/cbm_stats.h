/*!
 * @author
 *    steffen klatt  (created on: may 13, 2017)
 */

#ifndef  CBM_STATS_H_
#define  CBM_STATS_H_

#include  "crabmeat-common.h"
namespace cbm {
    CBM_API extern int have_resources(); }

#ifdef  CRABMEAT_MONITOR_RESOURCES

#  include  "stats/cputime.h"
#  include  "stats/memusage.h"
#  include  "stats/walltime.h"
#  include  "stats/resmon.h"

#  define TIC(__reg__) \
    double  __reg__( cbm::cputime_nanosecond())/*;*/

#  define TOC(__reg__) \
    __reg__ = (((double)cbm::cputime_nanosecond() - (__reg__)) * 1.0e-09)/*;*/

#else
#  define TIC(__reg__) double  __reg__( -1.0)/*;*/
#  define TOC(__reg__) CRABMEAT_FIX_UNUSED(__reg__)/*;*/

#endif  /*  CRABMEAT_MONITOR_RESOURCES  */

#endif  /* !CBM_STATS_H_ */

