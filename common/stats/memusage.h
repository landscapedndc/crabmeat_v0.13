/*!
 * @brief
 *    query memory usage of ldndc process
 *
 * @author
 *    steffen klatt  (created on: sep 11, 2013)
 */

#ifndef  CBM_STATS_MEMORY_H_
#define  CBM_STATS_MEMORY_H_

#include  "crabmeat-common.h"

namespace cbm
{
struct CBM_API memory_usage_t
{
    long int size;          // total program size
    long int resident;      // resident set size
    long int share;         // shared pages
    long int text;          // text (code)
    long int lib;           // library
    long int data;          // data/stack
};

extern void CBM_API memory_usage( struct memory_usage_t *);
 
} /*namespace cbm*/

#endif  /* !CBM_STATS_MEMORY_H_ */

