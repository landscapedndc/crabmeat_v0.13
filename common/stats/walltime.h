/*!
 * @brief
 *    query wall time
 *
 * @author
 *    steffen klatt  (created on: sep 11, 2013)
 */

#ifndef  CBM_STATS_WALLTIME_H_
#define  CBM_STATS_WALLTIME_H_

#include  "crabmeat-common.h"
#include  "cbm_types.h"

namespace cbm {

typedef  ldndc_int64_t  walltime_t;
#define  WALLTIME_FORMAT  LDNDC_PRINTF_SEQ_INT64

CBM_API extern walltime_t  walltime_nanosecond();

} /*namespace cbm*/

#endif  /* !CBM_STATS_WALLTIME_H_ */

