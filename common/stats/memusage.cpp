/*!
 * @brief
 *    query memory usage of ldndc process
 *
 * @author
 *    steffen klatt  (created on: sep 11, 2013)
 */

#include  "stats/memusage.h"
#include  <sys/types.h>
#include  <unistd.h>
#include  <stdio.h>

void
cbm::memory_usage( struct cbm::memory_usage_t * _m)
{
    char  buf[30];
    snprintf( buf, 30, "/proc/%u/statm", (unsigned)getpid());
    FILE *  pf = fopen( buf, "r");
    if ( pf)
    {
        int  rc_fscanf = fscanf( pf, "%ld %ld %ld %ld %ld %ld",
            &(_m->size), &(_m->resident), &(_m->share), &(_m->text), &(_m->lib), &(_m->data));
        CRABMEAT_FIX_UNUSED(rc_fscanf);
        fclose( pf);
    }
} 

