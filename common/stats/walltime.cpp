/*!
 * @brief
 *    query wall time
 *
 * @author
 *    steffen klatt  (created on: sep 11, 2013)
 */

#include  "stats/walltime.h"
#include  <time.h>

cbm::walltime_t
cbm::walltime_nanosecond()
{
    struct timespec  ts;
    ::clock_gettime( CLOCK_REALTIME, &ts);
    ldndc_int64_t const  tv_sec = (ldndc_int64_t)ts.tv_sec;
    ldndc_int64_t const  tv_nsec = (ldndc_int64_t)ts.tv_nsec;
    return  tv_sec * 1000000000 + tv_nsec;
}

