/*!
 * @brief
 *    query cpu time
 *
 * @author
 *    steffen klatt  (created on: sep 11, 2013)
 */

#ifndef  CBM_STATS_CPUTIME_H_
#define  CBM_STATS_CPUTIME_H_

#include  "crabmeat-common.h"
#include  "cbm_types.h"

namespace cbm {

typedef  ldndc_int64_t  cputime_t;
#define  CPUTIME_FORMAT  LDNDC_PRINTF_SEQ_INT64

CBM_API extern cputime_t  cputime_nanosecond();

} /*namespace cbm*/

#endif  /* !CBM_STATS_CPUTIME_H_ */

