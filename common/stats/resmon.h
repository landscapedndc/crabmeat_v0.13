/*!
 * @brief
 *    declares structure holding and monitoring various
 *    kernel related statistics (e.g. memory usage and
 *    cpu time)
 *
 * @author 
 *    steffen klatt (created on: sep 04, 2013),
 *    edwin haas
 */

#ifndef  CBM_STATS_RESMON_H_
#define  CBM_STATS_RESMON_H_

#include  "crabmeat-common.h"

namespace cbm
{
class CBM_API time_monitor_t
{
public:
    typedef  double  time_type;

public:
    time_monitor_t();
    virtual  ~time_monitor_t() = 0;
    void  reset();

    virtual  void  tic() = 0;
    virtual  void  toc() = 0;

    time_type  min() const
        { return this->ts.mi; }
    time_type  max() const
        { return this->ts.ma; }
    time_type  total() const
        { return this->ts.to; }

    int  nb_samples() const
        { return this->ts.nb; }

    int  update( time_type);

protected:
    struct  time_stats_t
    {
        /* minimum */
        time_type  mi;
        /* maximum */
        time_type  ma;
        /* total */
        time_type  to;

        /* nb of updates */
        int  nb;

        /* temporary */
        time_type  tic;
    }  ts;
};
class CBM_API cputime_monitor_t : public time_monitor_t
{
public:
    void  tic();
    void  toc();
};

class CBM_API walltime_monitor_t : public time_monitor_t
{
public:
    void  tic();
    void  toc();
};


class CBM_API memory_monitor_t
{ };


class CBM_API message_transaction_monitor_t
{ };


class CBM_API resources_statistics_t
{
public:
    typedef  time_monitor_t::time_type  time_type;

public:
    resources_statistics_t();
    void  reset();

    void  tic();
    void  toc();

    time_type  cpu_min() const
        { return ( this->tictocs > 0) ? this->ct.min() : 0.0; }
    time_type  cpu_max() const
        { return ( this->tictocs > 0) ? this->ct.max() : 0.0; }
    time_type  cpu_avg() const
        { return ( this->tictocs > 0) ? this->ct.total() / (time_type)this->tictocs : 0.0; }
    time_type  cpu_total() const
        { return this->ct.total(); }

    time_type  wall_min() const
        { return ( this->tictocs > 0) ? this->wt.min() : 0.0; }
    time_type  wall_max() const
        { return ( this->tictocs > 0) ? this->wt.max() : 0.0; }
    time_type  wall_avg() const
        { return ( this->tictocs > 0) ? this->wt.total() / (time_type)this->tictocs : 0.0; }
    time_type  wall_total() const
        { return this->wt.total(); }

private:
    int  tictocs;
    /* cpu time */
    cputime_monitor_t  ct;
    /* wall time */
    walltime_monitor_t  wt;

    /* memory usage */
// sk:later        memory_monitor_t  mem;

    /* transmission count */
// sk:later        message_transaction_monitor_t  tn;

};
} /*namespace cbm*/


#endif  /* !CBM_STATS_RESMON_H_ */

