/*!
 * @author
 *    steffen klatt  (created on: jun 26, 2021)
 *
 * @note
 *    see https://github.com/marler8997/tiny-regex-c for documentation
 */
#ifndef CBM_REGEX_H_
#define CBM_REGEX_H_

#include  "crabmeat-common.h"

namespace cbm {

extern CBM_API int  regex_match( char const * /*regexp*/,
        char const * /*buf*/, int /*buf_len*/,
            int* /*match index*/, int /*(unused)*/ );

}

#endif  /* !CBM_REGEX_H_ */

