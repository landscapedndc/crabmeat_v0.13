/*!
 * @author
 *    steffen klatt  (created on: jun 26, 2021),
 */

#include "regex/cbm_regex.h"
#include "regex/tiny-regex/re.h"
#include "string/cbm_string.h"

#define WORDBUF_SZ 64

CBM_API int  cbm::regex_match( char const *regexp,
        char const *buf, int buf_len,
            int* match_index, int )
{
	char const * word = buf;
	size_t word_len = buf_len;
	if ( word_len <= 0 ) {
		word_len = strlen( buf );
	}

	struct re_context cxt = { 0, RE_ERROR_NONE, NULL };
	re_length_t matchindex = 0;

    if ( re_match( &cxt, regexp, word, word+word_len, &matchindex ) )
	{
		if ( match_index ) { *match_index = matchindex; }
		return cxt.match_length;
	}
	return -1;
}

