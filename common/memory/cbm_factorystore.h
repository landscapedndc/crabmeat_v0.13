/*!
 * @brief
 *    object factory store
 *
 * @author
 *    steffen klatt (created on: feb 07, 2015)
 *
 * @note
 *    based on D. Kharrat and S. S. Qadri "Self-registering
 *    plug-ins: an architecture for extensible software"
 */

#ifndef  LDNDC_OBJECTFACTORYSTORE_H_
#define  LDNDC_OBJECTFACTORYSTORE_H_

#include  <string>
#include  <map>

#include  "memory/cbm_singleton.h"
#include  "log/cbm_baselog.h"

namespace cbm
{

template < typename _T >
struct  factory_registry_t
{
    typedef  std::map< std::string, _T * >  type_t;
};

template < typename  _T >
struct  factorystoreregister_t
{
    public:
        typedef  typename factory_registry_t< _T >::type_t  registry_t;
        typedef  typename registry_t::iterator  iterator_t;

        typedef  typename ldndc::singleton_t< registry_t > registry_store_t;

    public:
        factorystoreregister_t() {}
        virtual  ~factorystoreregister_t();

        static  void  register_factory(
                char const *  _prefix, char const *  _objname, _T *  _factory)
        {
            std::string  objname( factory_name( _prefix, _objname));
            if ( objname != "")
            {
                registry_store_t::get_instance()->insert( std::make_pair(
                        objname, static_cast< _T * >( _factory)));
            }
        }

        static  registry_t const *  registry()
        {
            return  registry_store_t::get_instance();
        }

        static  std::string  factory_name(
                char const *  _prefix, char const *  _object_name)
        {
            std::string  object_name;
            if ( _prefix && ( std::string( _prefix) != ""))
            {
                object_name = _prefix;
                object_name += '.';
            }
            if ( _object_name && ( std::string( _object_name) != ""))
            {
                object_name += _object_name;
            }
            else
            {
                object_name = "";
            }
            return  object_name;
        }
};
}

template < typename  _T >
cbm::factorystoreregister_t< _T >::~factorystoreregister_t< _T >() {}

namespace cbm {

template < typename  _T >
struct  factorystore_t
{
    public:
        typedef  typename factory_registry_t< _T >::type_t  registry_t;
        typedef  typename registry_t::const_iterator  iterator_t;

    public:
        factorystore_t( registry_t const *  _registry) : m_registry( _registry) {}
        virtual  ~factorystore_t();

        size_t  size() { return  this->m_registry->size(); }
        char const *  resolve_name( iterator_t const &  _it) { return  _it->second->name(); }
        iterator_t  begin() { return  this->m_registry->begin(); }
        iterator_t  end() { return  this->m_registry->end(); }

        _T const *  factory(
                char const *  _prefix, char const *  _objname)
        {
            std::string  objname( factorystoreregister_t< _T >::factory_name( _prefix, _objname));
            iterator_t  ri =
			   	this->m_registry->find( objname);
            if ( ri == this->m_registry->end())
            {
                return  NULL;
            }
            return  ri->second;
        }

    private:
        registry_t const *  m_registry;
};
}

template < typename  _T >
cbm::factorystore_t< _T >::~factorystore_t< _T >() {}

#endif  /*  !LDNDC_OBJECTFACTORYSTORE_H_  */

