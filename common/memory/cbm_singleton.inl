/*!
 * @brief
 *    singleton wrapper
 *
 * @author
 *    steffen klatt (created on: feb 25, 2015)
 * @note
 *    heavily borrowed from boost (1.36)
 */

#ifndef  CRABMEAT_MEMORY_SINGLETON_INL_
#define  CRABMEAT_MEMORY_SINGLETON_INL_

#include  "crabmeat-dllexport.h"

template < typename _S >
void  ldndc::singleton_t< _S >::use( _S const &) {}

template < typename _S >
_S *  ldndc::singleton_t< _S >::get_instance()
{
    static _S  s;
    /* refer to instance, causing it to be instantiated (and
     * initialized at startup on working compilers)
     */
    use( instance);
    return  &s;
}

template < typename _S >
_S & ldndc::singleton_t< _S >::instance = *ldndc::singleton_t< _S >::get_instance();

#endif  /*  !CRABMEAT_MEMORY_SINGLETON_INL_  */

