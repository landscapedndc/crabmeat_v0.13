/*!
 * @brief
 *    SLAB based memory cache
 *
 *    Bonwick, Jeff. "The Slab Allocator: An Object-Caching Kernel Memory Allocator."
 *    USENIX summer. Vol. 16. 1994.
 *
 * @author
 *    steffen klatt (created on: jan 16, 2017)
 */

#include  "memory/cbm_slaba.h"
#include  "memory/cbm_mem.h"
#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct CBM_MemCacheChunk
{
    struct CBM_MemCacheChunk *  next;
} CBM_MemCacheChunk;
typedef struct CBM_MemCacheSlab
{
    int  ref_count;

    void *  buf;
    struct CBM_MemCacheChunk *  chunks;
    struct CBM_MemCacheChunk *  chunks_free;

    struct CBM_MemCacheSlab *  next;
    struct CBM_MemCacheSlab *  previous;
} CBM_MemCacheSlab;

typedef struct CBM_MemCacheData
{
    CBM_MemCacheSlab *  slabs;
    CBM_MemCacheSlab *  slab_cur;
    int  nb_slabs;

    unsigned int  slab_sz;
    unsigned int  chunk_sz;

    CBM_Constructor  constructor;
    CBM_Destructor  destructor;

    void * (*alloc)( size_t /*bytes*/);
    void  (*free)( void * /*data ptr*/);

    char *  name;
} CBM_MemCacheData;

static void *  _cbm_alloc( size_t  _bytes)
    { return  CBM_Allocate( _bytes); }
static void  _cbm_free( void *  _mem)
    { CBM_Free( _mem); }

static int  CBM_MemCacheNewSlab( CBM_MemCacheData *);
static int  CBM_MemCacheReleaseSlab( CBM_MemCacheData *,
                CBM_MemCacheSlab *);

CBM_MemCache  CBM_MemCacheCreate( char const *  _name,
            size_t  _slab_sz, size_t  _chunk_sz, int  /*alignment*/,
        CBM_Constructor  _constructor, CBM_Destructor  _destructor)
{
    CBM_MemCache  mc;
    mc.mem = NULL;

    if ( _slab_sz < _chunk_sz)
        { return  mc; }

    CBM_MemCacheData *  memcache =
        (CBM_MemCacheData *)CBM_Allocate( sizeof( CBM_MemCacheData));
    if ( !memcache)
        { return  mc; }

    memcache->slab_sz = _slab_sz;
    memcache->chunk_sz = _chunk_sz;

    memcache->constructor = _constructor;
    memcache->destructor = _destructor;

    memcache->alloc = _cbm_alloc;
    memcache->free = _cbm_free;

    memcache->slabs = NULL;
    memcache->nb_slabs = 0;
    memcache->slab_cur = NULL;

    if ( _name)
        { memcache->name = cbm::strdup( _name); }
    else
        { memcache->name = cbm::strdup( "memcache"); }
    CBM_LogDebug( "Created object cache '", memcache->name,"'",
        " [slab-sz=",memcache->slab_sz,",chunk-sz=",memcache->chunk_sz,"]");

    mc.mem = memcache;
    return  mc;
}

void  CBM_MemCacheDestroy( CBM_MemCache  _mc)
{
    if ( _mc.mem)
    {
        CBM_MemCacheData *  memcache = (CBM_MemCacheData *)_mc.mem;
        while ( memcache->slabs )
        {
            CBM_MemCacheReleaseSlab( memcache, memcache->slabs);
        }
        CBM_Assert( memcache->nb_slabs==0);

        if ( memcache->name)
            { free( memcache->name); }

        CBM_Free( memcache);
    }
}

void *  CBM_MemCacheAlloc( CBM_MemCache  _mc, int /*flags*/)
{
    CBM_MemCacheData *  memcache = (CBM_MemCacheData *)_mc.mem;
    if (( memcache->slab_cur == NULL) || ( memcache->slab_cur->chunks_free == NULL))
    {
        int  sz_newslab = CBM_MemCacheNewSlab( memcache);
        if ( sz_newslab <= 0)
        {
            CBM_LogError( "Failed to create new slab  [cache=",memcache->name,"]");
            return  NULL;
        }
    }
    CBM_Assert( memcache->slab_cur!=0);

    /* use index in chunks structure to find memory buffer */
    CBM_MemCacheChunk *  chunks = memcache->slab_cur->chunks_free;
    size_t const  chunk_index = chunks - memcache->slab_cur->chunks;
	/* allocated memory chunk */
    char *  chunk = (char*)memcache->slab_cur->buf + memcache->chunk_sz*chunk_index;

    /* unlink first item */
    memcache->slab_cur->chunks_free = memcache->slab_cur->chunks_free->next;
    memcache->slab_cur->ref_count += 1;

    /* attempt to find new serve slab */
    if ( memcache->slab_cur->chunks_free == NULL)
    {
        CBM_MemCacheSlab *  slab = memcache->slabs;
        while ( slab)
        {
            if ( slab->chunks_free != NULL)
            {
                memcache->slab_cur = slab;
                break;
            }
            slab = slab->next;
        }
    }

    return  chunk;
}

void  CBM_MemCacheFree( CBM_MemCache  _mc, void *  _chunk)
{
    CBM_MemCacheData *  memcache = (CBM_MemCacheData *)_mc.mem;

    /* find slab for buffer ( TODO can we do better? )*/
    CBM_MemCacheSlab *  slab = NULL;
    if ( memcache->slab_cur->buf <= _chunk
            && _chunk <= (char*)memcache->slab_cur->buf+memcache->slab_sz)
        { slab = memcache->slab_cur; } /*slab_cur is somewhat likely...*/
    else
    {
        slab = memcache->slabs;
        while ( slab )
        {
            if ( slab->buf <= _chunk
                    && _chunk <= (char*)slab->buf+memcache->slab_sz)
                { break; }
            slab = slab->next;
        }
    }
    CBM_Assert(slab); /*buffer not alloced via this memcache*/
    size_t const  chunk_index =
            ( (char*)_chunk - (char*)slab->buf) / memcache->chunk_sz;
    CBM_MemCacheChunk *  chunks = slab->chunks + chunk_index;

    chunks->next = slab->chunks_free;
    slab->chunks_free = chunks;

    slab->ref_count -= 1;
    /* serve from slab that just grew if other slab is full */
    if ( memcache->slab_cur->chunks_free == NULL)
        { memcache->slab_cur = slab; }
    /* TODO slab eviction policy */
}

static int CBM_MemCacheNewSlab( CBM_MemCacheData *  _memcache)
{
    CBM_MemCacheSlab *  slab =
            (CBM_MemCacheSlab *)_memcache->alloc(
                sizeof(CBM_MemCacheSlab));
    if ( !slab)
        { return  -1; }

    slab->ref_count = 0;
    slab->buf = _memcache->alloc( _memcache->slab_sz);
    if ( !slab->buf)
    {
        _memcache->free( slab);
        return  -1;
    }
    if ( _memcache->constructor)
    {
        char *  buf_end = ((char *)slab->buf)+_memcache->slab_sz;
        for ( char *  chunk = (char*)slab->buf;
                chunk < buf_end;  chunk+=_memcache->chunk_sz)
            { _memcache->constructor( (void *)chunk, 0); }
    }

    size_t const  item_count = _memcache->slab_sz / _memcache->chunk_sz;
    slab->chunks = (CBM_MemCacheChunk *)_memcache->alloc(
                sizeof(CBM_MemCacheChunk) * item_count);
    if ( !slab->chunks)
    {
        _memcache->free( slab->buf);
        _memcache->free( slab);
        return  -1;
    }
    for ( size_t  i = 0;  i < item_count-1;  ++i)
    {
        slab->chunks[i].next = &slab->chunks[i+1];
    }
    slab->chunks[item_count-1].next = NULL;
    slab->chunks_free = &slab->chunks[0];
    CBM_Assert(slab->chunks == slab->chunks_free);

    /* prepend to slab list and link */
    slab->previous = NULL;
    if ( _memcache->slabs == NULL)
    {
        _memcache->slabs = slab;
        slab->next = NULL;
    }
    else
    {
        _memcache->slabs->previous = slab;
        slab->next = _memcache->slabs;
        _memcache->slabs = slab;
    }
    /* serve from new slab */
    _memcache->slab_cur = _memcache->slabs;
    _memcache->nb_slabs += 1;

    return  _memcache->nb_slabs;
}
static int  CBM_MemCacheReleaseSlab( CBM_MemCacheData *  _memcache,
                CBM_MemCacheSlab *  _slab)
{
    if ( !_slab)
        { return 0; }
    CBM_Assert( _memcache->nb_slabs > 0);

    if ( _slab->ref_count != 0)
       { CBM_LogError( "Reference count for slab not zero",
              " [",_slab->ref_count,"] when releasing it",
                " [cache=",_memcache->name,"]"); }

    if ( _memcache->destructor)
    {
        char *  buf_end = ((char *)_slab->buf)+_memcache->slab_sz;
        for ( char *  chunk = (char*)_slab->buf;
                chunk < buf_end;  chunk+=_memcache->chunk_sz)
            { _memcache->destructor( (void *)chunk, 0); }
    }

    if ( _memcache->slabs == _slab)
        { _memcache->slabs = _slab->next; }
    else
    {
        CBM_MemCacheSlab *  slab = _memcache->slabs;
        while ( slab != _slab) /*exists by definition*/
            { slab = slab->next; }
        CBM_Assert( slab);
        slab->previous->next = slab->next;
    }
    if ( _memcache->slab_cur == _slab)
        { _memcache->slab_cur = _memcache->slabs; }

    _memcache->free( _slab->buf);
    _memcache->free( _slab->chunks);
    _memcache->free( _slab);

    _memcache->nb_slabs -= 1;
    return  _memcache->nb_slabs;
}

#if defined(__cplusplus)
}
#endif

