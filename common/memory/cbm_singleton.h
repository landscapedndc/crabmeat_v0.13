/*!
 * @brief
 *    singleton wrapper
 *
 * @author
 *    steffen klatt (created on: feb 25, 2015)
 * @note
 *    heavily borrowed from boost (1.36)
 */

#ifndef  CRABMEAT_MEMORY_SINGLETON_H_
#define  CRABMEAT_MEMORY_SINGLETON_H_

#include  "crabmeat-dllexport.h"

namespace  ldndc
{
    template < typename _S >
        class singleton_t
    {
        /* include this to provoke instantiation at pre-execution time */
        static void  use( _S const &);

        protected:
            static _S &  instance;
        public:
            static _S *  get_instance();
};

}  /* namespace ldndc */
#include  "memory/cbm_singleton.inl"

#endif  /*  !CRABMEAT_MEMORY_SINGLETON_H_  */

