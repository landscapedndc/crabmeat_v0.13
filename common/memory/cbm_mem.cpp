/*!
 * @brief
 *    memory manager
 *
 * @author
 *    steffen klatt (created on: apr 18, 2013)
 */

#include  "memory/cbm_mem.h"

cbm::allocator_base_t::allocator_base_t()
{
}

cbm::allocator_base_t::~allocator_base_t()
{
}


cbm::allocator_default_t::allocator_default_t()
        : allocator_base_t(),
          object_cntr_( 0),
          object_allocs_( 0),
          bytes_( 0)
{
}

cbm::allocator_t *  cbm::DefaultAllocator()
{
    static  cbm::allocator_t  global_CBM_DefaultAllocator;
// sk:dbg    fprintf( stderr, "(%p) created default allocator '%s'..\n",
// sk:dbg        (void*)&global_CBM_DefaultAllocator, global_CBM_DefaultAllocator.name());
    return  &global_CBM_DefaultAllocator;
}

