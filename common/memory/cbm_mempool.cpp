/*!
 * @brief
 *    simple memory pool
 *
 * @author
 *    steffen klatt  (created on: jan 11, 2017)
 */

#include  <string.h>

#include  "memory/cbm_mem.h"
#include  "memory/cbm_mempool.h"
#include  "math/cbm_math.h"
#include  "log/cbm_baselog.h"

#if defined(__cplusplus)
extern "C" {
#endif

static void *  _cbm_alloc( size_t  _bytes)
    { return  CBM_Allocate( _bytes); }
static void  _cbm_free( void *  _mem)
    { CBM_Free( _mem); }

typedef struct CBM_MemPoolData
{
    CBM_MemPool  parent;

    void * (*alloc)( size_t /*bytes*/);
    void  (*free)( void * /*data ptr*/);

    char *  buf_head;

    char *  buf_free;
    char *  buf_tail;
    unsigned int  block_sz;
    unsigned int  align;

    unsigned int  item_count;
} CBM_MemPoolData;

#define  CBM_BLOCK_SIZE_DEFAULT  (8*1024)
static CBM_MemPoolData _CBM_MemPoolRoot =
    { { NULL }, _cbm_alloc, _cbm_free,
            NULL, NULL, NULL, CBM_BLOCK_SIZE_DEFAULT, 8, 0 };

CBM_MemPool CBM_MemPoolNone = { NULL };
CBM_MemPool CBM_MemPoolRoot = { &_CBM_MemPoolRoot };


static char *  CBM_MemPoolNewBlock( CBM_MemPoolData *  _mempool)
{
    char *  buf = (char*)_mempool->alloc( _mempool->block_sz+sizeof(char*));
    if ( buf)
    {
        CBM_LogDebug( (void*)_mempool," == Allocated new block [",(void*)buf,"]",
               " of size ",_mempool->block_sz);
        /*make pointer-size explicit (gcc -Wsizeof-pointer-memaccess) */
        size_t const  ptr_sz = sizeof(char*);
        /* prepend to existing buffer */
        if ( _mempool->buf_head)
            { memcpy( buf+_mempool->block_sz,
                &_mempool->buf_head, ptr_sz); }
        else
            { memset( buf+_mempool->block_sz, 0, ptr_sz); }
        _mempool->buf_head = buf;
        _mempool->buf_free = buf;
        _mempool->buf_tail = buf+_mempool->block_sz;

        /* grow block size */
        _mempool->block_sz <<= 1;
    }
    return  buf;
}
CBM_MemPool  CBM_MemPoolCreate(
        CBM_MemPool  _parent, size_t  _ini_block_sz)
{
    CBM_MemPool  mp;
    mp.mem = NULL;

    CBM_MemPoolData *  mempool =
        (CBM_MemPoolData *)CBM_Allocate( sizeof(CBM_MemPoolData));
    if ( !mempool)
        { return  mp; }

    mempool->parent = _parent;
    CBM_MemPoolData *  mempool_parent =
            (CBM_MemPoolData *)(_parent.mem);
    if ( mempool_parent == NULL)
        { mempool_parent = &_CBM_MemPoolRoot; }

    mempool->alloc = mempool_parent->alloc;
    mempool->free = mempool_parent->free;

    mempool->block_sz = ( _ini_block_sz == 0)
        ? mempool_parent->block_sz : _ini_block_sz;
    mempool->block_sz = cbm::upper_power_of_two( mempool->block_sz);
    CBM_Assert(mempool->block_sz>0);

    mempool->align = mempool_parent->align;

    mempool->buf_head = NULL;
    mempool->buf_free = NULL;
    mempool->buf_tail = NULL;
    mempool->item_count = 0;

    mp.mem = mempool;
    CBM_LogDebug( (void*)mp.mem," == Created MemoryPool",
            "  [bs=",mempool->block_sz," (",_ini_block_sz,")]");
    return  mp;
}
void  CBM_MemPoolDestroy( CBM_MemPool  _mempool)
{
    CBM_MemPoolData *  mempool = (CBM_MemPoolData *)(_mempool.mem);
    CBM_Assert(mempool);

    char *  buf = mempool->buf_head;
    while ( buf )
    {
        char *  free_buf = buf;
        mempool->block_sz >>= 1;
        memcpy( &buf, buf+mempool->block_sz, sizeof(char*));
        CBM_LogDebug( (void*)mempool," == Free block [",(void*)free_buf,"]",
               " for bs ", mempool->block_sz);

        mempool->free( free_buf);
    }
    CBM_Free( mempool);
}

void *  CBM_MAlloc( CBM_MemPool  _mempool,
            size_t  _nb_bytes, int  _flags)
{
    if ( _nb_bytes==0)
        { return  NULL; }

    CBM_MemPoolData *  mempool = (CBM_MemPoolData *)(_mempool.mem);
    CBM_Assert(mempool);

    size_t const  align = mempool->align;
    size_t const  nb_bytes = (( _nb_bytes+align-1) / align) * align;
// sk:dbg    CBM_LogWrite( "Use alloc-size ", nb_bytes, "  ~",_nb_bytes);
    if (( mempool->buf_free == NULL)
            || (( mempool->buf_free+nb_bytes) > mempool->buf_tail))
    {
        if ( CBM_MemPoolNewBlock( mempool) == NULL)
            { return  NULL; }
        /* grow until buffer is large enough */
        return  CBM_MAlloc( _mempool, _nb_bytes, _flags);
    }

    char *  alloc_buf = mempool->buf_free;
    mempool->buf_free += nb_bytes;
    mempool->item_count += 1;
    return  alloc_buf;

}
void *  CBM_CAlloc( CBM_MemPool  _mempool,
            size_t  _nb_bytes, int  _flags)
{
    char *  alloc_buf = (char *)CBM_MAlloc( _mempool, _nb_bytes, _flags);
    memset( alloc_buf, 0, _nb_bytes);
    return  alloc_buf;
}

int CBM_MemPoolSize( CBM_MemPool  _mempool)
{
    CBM_MemPoolData *  mempool = (CBM_MemPoolData *)_mempool.mem;
    if ( mempool)
        { return  mempool->item_count; }
    return  -1;
}

#if defined(__cplusplus)
}
#endif

