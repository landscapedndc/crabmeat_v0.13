/*!
 * @brief
 *    memory manager
 *
 * @author
 *    steffen klatt (created on: apr 18, 2013)
 */

#ifndef  CBM_MEM_H_
#define  CBM_MEM_H_

#include  "crabmeat-config.h"
#include  "crabmeat-common.h"
#if  defined(_DEBUG) && !defined(CRABMEAT_COMPILER_MSC)
#  include  <stdio.h>
#endif

#include  <new>

#define  CBM_Allocate(bytes) CBM_DefaultAllocator->allocate(bytes)
#define  CBM_Free(ptr) CBM_DefaultAllocator->deallocate(ptr)
#define  CBM_Construct(type) CBM_DefaultAllocator->construct< type >()
#define  CBM_Destroy(ptr) CBM_DefaultAllocator->destroy(ptr)

namespace  cbm
{
    /* allocator base class */
    class  CBM_API  allocator_base_t
    {
        public:
            virtual  char const *  name() = 0;

        public:
            virtual  void *  allocate( size_t) = 0;

            virtual  void  deallocate( void *) = 0;

        protected:
            allocator_base_t();
            virtual  ~allocator_base_t() = 0;
    };

    /*** allocators are singletons ***/

    /* default allocator (using operator new) */

    class  CBM_API  allocator_default_t
        :  public  allocator_base_t
    {
//		static void  use( allocator_default_t const &);
//
//        protected:
//            static allocator_default_t &  instance;
//        public:
//            static allocator_default_t *  get_instance();

        static char const *  name_() { return  "DefaultAllocator"; }
        public:
            char const *  name() { return  allocator_default_t::name_(); }

        public:
            void *  allocate( size_t  _bytes)
            {
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (object_cntr_)
#  pragma omp atomic
#endif
                ++this->object_cntr_;
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (object_allocs_)
#  pragma omp atomic
#endif
                ++this->object_allocs_;
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (bytes_)
#  pragma omp atomic
#endif
                this->bytes_ += _bytes;
                return  ::operator new( _bytes, std::nothrow);
            }

            template < typename  _T >
            _T *
            allocate_type( size_t  _n = 1)
            {
                if ( _n == 0)
                {
                    return  NULL;
                }
                return  reinterpret_cast< _T * >( this->allocate( _n*sizeof( _T)));
            }
            template < typename  _T >
            _T *
            allocate_init_type( size_t  _n = 1, _T const &  _ini = _T())
            {
                if ( _n == 0)
                {
                    return  NULL;
                }
                _T *  mem = reinterpret_cast< _T * >( this->allocate( _n*sizeof( _T)));
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  mem_0 = mem;
                while ( _n--)
                {
                    *mem = _ini;
                    ++mem;
                }
                return  mem_0;
            }
            template < typename  _T >
            _T *
            construct( size_t  _n = 1)
            {
                _T *  mem = this->allocate_type< _T >( _n);
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  obj = mem;
                while ( _n--)
                {
                    new ( obj++) _T();
                }
                return  mem;
            }
            template < typename  _T, typename  _T1 >
            _T *
            construct_args( size_t  _n, _T1 &  _arg1)
            {
                _T *  mem = this->allocate_type< _T >( _n);
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  obj = mem;
                while ( _n--)
                {
                    new ( obj++) _T( _arg1);
                }
                return  mem;
            }
            template < typename  _T, typename  _T1, typename  _T2 >
            _T *
            construct_args( size_t  _n, _T1  _arg1, _T2  _arg2)
            {
                _T *  mem = this->allocate_type< _T >( _n);
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  obj = mem;
                while ( _n--)
                {
                    new ( obj++) _T( _arg1, _arg2);
                }
                return  mem;
            }
            template < typename  _T, typename  _T1, typename  _T2, typename  _T3 >
            _T *
            construct_args( size_t  _n, _T1  _arg1, _T2  _arg2, _T3  _arg3)
            {
                _T *  mem = this->allocate_type< _T >( _n);
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  obj = mem;
                while ( _n--)
                {
                    new ( obj++) _T( _arg1, _arg2, _arg3);
                }
                return  mem;
            }
            template < typename  _T, typename  _T1, typename  _T2, typename  _T3, typename  _T4 >
            _T *
            construct_args( size_t  _n, _T1  _arg1, _T2  _arg2, _T3  _arg3, _T4  _arg4)
            {
                _T *  mem = this->allocate_type< _T >( _n);
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  obj = mem;
                while ( _n--)
                {
                    new ( obj++) _T( _arg1, _arg2, _arg3, _arg4);
                }
                return  mem;
            }
            template < typename  _T, typename  _T1, typename  _T2, typename  _T3, typename  _T4, typename  _T5 >
            _T *
            construct_args( size_t  _n, _T1  _arg1, _T2  _arg2, _T3  _arg3, _T4  _arg4, _T5  _arg5)
            {
                _T *  mem = this->allocate_type< _T >( _n);
                if ( !mem)
                {
                    return  NULL;
                }
                _T *  obj = mem;
                while ( _n--)
                {
                    new ( obj++) _T( _arg1, _arg2, _arg3, _arg4, _arg5);
                }
                return  mem;
            }
            void  deallocate( void *  _mem)
            {
                if ( _mem)
                {
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (object_allocs_)
#  pragma omp atomic
#endif
                    --this->object_allocs_;
                    ::operator delete( _mem, std::nothrow);
                }
            }
            void  deallocate( void const *  _mem)
            {
                this->deallocate( const_cast< void * >( _mem));
            }
            template < typename  _T >
            void  destroy( _T *  _mem, size_t  _n = 1)
            {
                if ( _mem)
                {
                    if ( _n == 0)
                    {
                        return;
                    }

                    _T *  obj = _mem;
                    while ( _n--)
                    {
                        (obj++)->~_T();
                    }

                    this->deallocate( _mem);
                }
            }
            template < typename  _T >
            void  destroy( _T const *  _mem, size_t  _n = 1)
            {
                this->destroy( const_cast< _T * >( _mem), _n);
            }

        public:
            allocator_default_t();
            ~allocator_default_t()
            {
#if  defined(_DEBUG) && !defined(CRABMEAT_COMPILER_MSC)
                if ( this->object_allocs_)
                {
                    fprintf( stderr, "%s: there were %lu non-freed objects at allocator destruct time\n",
                        this->name(), (long unsigned)this->object_allocs_);
                }
                fprintf( stderr, "%s: total number of allocated objects:"
                    " %lu  memory allocated: %lu%s\n", this->name(), (long unsigned)this->object_cntr_,
                    (long unsigned)((this->bytes_<8192) ? this->bytes_ : (this->bytes_>>10)), ((this->bytes_<8192) ? "B" : "KB"));
#endif
            }

        private:
            volatile size_t  object_cntr_;
            volatile int  object_allocs_;
            volatile size_t  bytes_;
    };

    /* choose cbm allocator at build time */
    typedef  allocator_default_t  allocator_t;
}

/* memory allocator */
namespace cbm {
    extern CBM_API cbm::allocator_t *  DefaultAllocator(); }
#define  CBM_DefaultAllocator  cbm::DefaultAllocator()

namespace cbm {

    template < typename  _T >
    class  allocatable_t
    {
        public:
            allocatable_t();
            virtual  ~allocatable_t();

            static  _T *  allocate_instance();

            static  _T *  new_instance();
            static  _T *  new_instance(
                    lid_t const &  _object_id);

            static  void  delete_instance( _T *);

            static  cbm::allocator_t *  get_allocator()
            {
                return  CBM_DefaultAllocator;
            }
    };
}

template < typename  _T >
cbm::allocatable_t< _T >::allocatable_t()
{
}
template < typename  _T >
cbm::allocatable_t< _T >::~allocatable_t< _T >()
{
}

template < typename  _T >
_T *
cbm::allocatable_t< _T >::allocate_instance()
{
    return  static_cast< _T * >( CBM_DefaultAllocator->allocate( sizeof( _T)));
}
template < typename  _T >
_T *
cbm::allocatable_t< _T >::new_instance()
{
    _T *  instance = allocate_instance();
    if ( instance)
    {
        new (instance) _T();
    }

    return  instance;
}

template < typename  _T >
_T *
cbm::allocatable_t< _T >::new_instance(
        lid_t const &  _object_id)
{
    _T *  instance = allocatable_t< _T >::new_instance();
    if ( instance)
    {
        instance->set_object_id( _object_id);
    }
    return  instance;
}

template < typename  _T >
void
cbm::allocatable_t< _T >::delete_instance(
        _T *  _obj)
{
    _obj->~_T();
    CBM_DefaultAllocator->deallocate( _obj);
}

#endif /* !CBM_MEM_H_ */

