/*!
 * @brief
 *    simple memory pool
 *
 * @author
 *    steffen klatt  (created on: jan 11, 2017)
 */

#ifndef  CBM_MEMPOOL_H_
#define  CBM_MEMPOOL_H_

#include  "crabmeat-common.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef struct CBM_MemPool
{
    void *  mem;
} CBM_MemPool;
CBM_API extern CBM_MemPool CBM_MemPoolNone;
CBM_API extern CBM_MemPool CBM_MemPoolRoot;

CBM_API CBM_MemPool  CBM_MemPoolCreate( CBM_MemPool /*parent*/,
        size_t /*initial block size (rounded up to next 2^N)*/);
CBM_API void  CBM_MemPoolDestroy( CBM_MemPool);

CBM_API void *  CBM_MAlloc( CBM_MemPool,
            size_t /*bytes*/, int /*flags*/);
CBM_API void *  CBM_CAlloc( CBM_MemPool,
            size_t /*bytes*/, int /*flags*/);

CBM_API int CBM_MemPoolSize( CBM_MemPool);

#if defined(__cplusplus)
}
#endif


#endif /* !CBM_MEMPOOL_H_ */

