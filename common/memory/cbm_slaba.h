/*!
 * @brief
 *    SLAB based memory cache
 *
 *    Bonwick, Jeff. "The Slab Allocator: An Object-Caching Kernel Memory Allocator."
 *    USENIX summer. Vol. 16. 1994.
 *
 * @author
 *    steffen klatt (created on: jan 16, 2017)
 */

#ifndef  CBM_SLABA_H_
#define  CBM_SLABA_H_

#include  "crabmeat-config.h"
#include  "crabmeat-common.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef void (*CBM_Constructor)( void * /*object*/, int /*flags*/);
typedef void (*CBM_Destructor)( void * /*object*/, int /*flags*/);

typedef struct CBM_MemCache
{
    void *  mem;
} CBM_MemCache;
CBM_API extern CBM_MemCache CBM_MemCacheNone;

CBM_API CBM_MemCache  CBM_MemCacheCreate(
        char const * /*name*/, size_t /*slab size*/, size_t /*chunk size*/,
        int  /*alignment*/, CBM_Constructor, CBM_Destructor);
CBM_API void  CBM_MemCacheDestroy( CBM_MemCache);

CBM_API void *  CBM_MemCacheAlloc( CBM_MemCache, int /*flags*/);
CBM_API void  CBM_MemCacheFree( CBM_MemCache, void * /*buffer*/);

#if defined(__cplusplus)
}
#endif


#endif /* !CBM_SLABA_H_ */

