/*!
 * @brief
 *    interface to json document
 *
 * @author
 *    steffen klatt (created on: jan 09, 2017)
 */

#ifndef  CBM_JSONQUERY_H_
#define  CBM_JSONQUERY_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

namespace cbm {

struct jquery_t;

template < typename _T >
struct _jquery_t
{ };
template < >
struct CBM_API _jquery_t<char const *>
{
    static char const *  get( cbm::jquery_t * /*jquery*/,
                char const * /*path*/, char const * /*default*/);
};
template < >
struct CBM_API _jquery_t<bool>
{
    static bool  get( cbm::jquery_t * /*jquery*/,
                char const * /*path*/, bool /*default*/);
};
template < >
struct CBM_API _jquery_t<int>
{
    static int  get( cbm::jquery_t * /*jquery*/,
                char const * /*path*/, int /*default*/);
};
template < >
struct CBM_API _jquery_t<double>
{
    static double  get( cbm::jquery_t * /*jquery*/,
                char const * /*path*/, double /*default*/);
};
template < >
struct CBM_API _jquery_t<float>
{
    static float  get( cbm::jquery_t * /*jquery*/,
                char const * /*path*/, float /*default*/);
};


struct CBM_API jquery_t
{
    jquery_t( char const * /*json string*/, size_t = 0 /*json string size*/,
            char const * = NULL /*base path, i.e. subtree*/);
    jquery_t( jquery_t const &);
    jquery_t &  operator=( jquery_t const &);

    ~jquery_t();

    char const *  nested_string( char const * /*path*/) const;
    jquery_t  nested_object( char const * /*path*/) const;

    bool  parse_ok() const;

    bool  exists( char const * /*path*/) const;
    bool  exists( cbm::string_t const & /*path*/) const;

    int  query_size( char const * /*path*/) const;

    /* query strings */
    char const *  query_string(
        char const * /*path*/, char const * /*default*/) const;
    cbm::string_t  query_string(
        cbm::string_t const & /*path*/, char const * /*default*/) const;

    bool  query_bool(
        char const * /*path*/, bool /*default*/) const;
    bool  query_bool(
        cbm::string_t const & /*path*/, bool /*default*/) const;

    int  query_int(
        char const * /*path*/, int /*default*/) const;
    int  query_int(
        cbm::string_t const & /*path*/, int /*default*/) const;

    //casting to double if integer given
    double  query_double_and_int(
        char const * /*path*/, double /*default*/) const;
    double  query_double_and_int(
        cbm::string_t const & /*path*/, double /*default*/) const;
    
    double  query_double(
        char const * /*path*/, double /*default*/) const;
    double  query_double(
        cbm::string_t const & /*path*/, double /*default*/) const;
    int  query_doubles( char const * /*path*/,
            double * /*data*/, size_t = 0 /*data size*/) const;
    int  query_doubles( cbm::string_t const & /*path*/,
            double * /*data*/, size_t = 0 /*data size*/) const;

    float  query_float(
        char const * /*path*/, float /*default*/) const;
    float  query_float(
        cbm::string_t const & /*path*/, float /*default*/) const;
    int  query_floats( char const * /*path*/,
            float * /*data*/, size_t = 0 /*data size*/) const;
    int  query_floats( cbm::string_t const & /*path*/,
            float * /*data*/, size_t = 0 /*data size*/) const;


    cbm::string_t  query_any(
        char const * /*path*/, char const * /*default*/) const;
    cbm::string_t  query_any(
        cbm::string_t const & /*path*/, char const * /*default*/) const;

    template < typename _T >
        _T  get( char const *  _path, _T  _default)
            { return _jquery_t<_T>::get( this, _path, _default); }

    char const *  serialize() const
        { return this->m_str; }

private:
    char *  m_str;
    size_t  m_str_sz;

    void *  m_data;
};

} /* namespace cbm */

#endif /* !CBM_JSONQUERY_H_ */

