/*!
 * @brief
 *    interface to construct json document
 *
 * @author
 *    steffen klatt (created on: mar 15, 2017)
 */

#ifndef  CBM_JSONBUILD_H_
#define  CBM_JSONBUILD_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

namespace cbm {

#define  CBM_JBUILD_IGNOREINVALID 0x00000001

struct jbuild_t;

template < typename _T >
struct _jbuild_t
{ };
template < >
struct CBM_API _jbuild_t<char const *>
{
    static lerr_t  put( cbm::jbuild_t * /*jbuilder*/,
                char const * /*path*/, char const * /*value*/);
};
template < >
struct CBM_API _jbuild_t<bool>
{
    static lerr_t  put( cbm::jbuild_t * /*jbuilder*/,
                char const * /*path*/, bool /*value*/);
};
template < >
struct CBM_API _jbuild_t<int>
{
    static lerr_t  put( cbm::jbuild_t * /*jbuilder*/,
                char const * /*path*/, int /*value*/);
};
template < >
struct CBM_API _jbuild_t<double>
{
    static lerr_t  put( cbm::jbuild_t * /*jbuilder*/,
                char const * /*path*/, double /*value*/);
};
template < >
struct CBM_API _jbuild_t<float>
{
    static lerr_t  put( cbm::jbuild_t * /*jbuilder*/,
                char const * /*path*/, float /*value*/);
};


struct CBM_API jbuild_t
{
    jbuild_t( int = 0 /*flags*/);
private: /*TODO*/
    jbuild_t( jbuild_t const &);
    jbuild_t &  operator=( jbuild_t const &);
public:
    ~jbuild_t();

    lerr_t  put_string( char const * /*path*/,
            char const * /*value*/, size_t = -1 /*value size*/);
    lerr_t  put_string( cbm::string_t const & /*path*/,
            char const * /*value*/, size_t = -1 /*value size*/);

    lerr_t  put_bool( char const * /*path*/, bool /*value*/);
    lerr_t  put_bool( cbm::string_t const & /*path*/, bool /*value*/);

    lerr_t  put_int( char const * /*path*/, int /*value*/);
    lerr_t  put_int( cbm::string_t const & /*path*/, int /*value*/);

    lerr_t  put_double( char const * /*path*/, double /*value*/);
    lerr_t  put_double( cbm::string_t const & /*path*/, double /*value*/);

    lerr_t  put_float( char const * /*path*/, float /*value*/);
    lerr_t  put_float( cbm::string_t const & /*path*/, float /*value*/);

    template < typename _T >
        lerr_t  put( char const *  _path, _T  _value)
            { return _jbuild_t<_T>::put( this, _path, _value); }

    /* returns stringyfied JSON document */
    char const *  serialize( size_t * /*size(bytes)*/ = NULL) const;

private:
    void *  m_data;
    void *  m_buffer;

    int  m_flags;
    mutable int  m_isdirty; /*only stringify when document changed*/
};

} /* namespace cbm */

#endif /* !CBM_JSONBUILD_H_ */

