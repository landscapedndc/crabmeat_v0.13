/*!
 * @brief
 *      JSON API
 *
 * @author
 *      steffen klatt (created on: jan 24, 2015)
 */

#ifndef  LDNDC_JSONAPI_H_
#define  LDNDC_JSONAPI_H_

#include  "crabmeat-common.h"
#include  "global/datatypes.h"

#include  "log/cbm_baselog.h"
#include  "string/lstring.h"

#include  "json.h"
#include  <vector>


namespace ldndc
{
    template < typename _T >
        struct  json_value_translate_int_t
        {
            static
            _T  get( struct json_object  *  jobj)
            {
                return  static_cast< _T >( json_object_get_int( jobj));
            }
            static
            struct json_object *  set( _T  _value)
            {
                return  json_object_new_int( _value);
            }
            static _T  invalid()
            {
                return  invalid_t< _T >::value;
            }
            static json_type  type()
            {
                return  json_type_int;
            }
        };

    template < typename _T >
        struct  json_value_translate_t {};
    template < >
        struct  json_value_translate_t< ldndc_int8_t >
        : public json_value_translate_int_t< ldndc_int8_t > {};
    template < >
        struct  json_value_translate_t< ldndc_uint8_t >
        : public json_value_translate_int_t< ldndc_uint8_t > {};
    template < >
        struct  json_value_translate_t< ldndc_int16_t >
        : public json_value_translate_int_t< ldndc_int16_t > {};
    template < >
        struct  json_value_translate_t< ldndc_uint16_t >
        : public json_value_translate_int_t< ldndc_uint16_t > {};
    template < >
        struct  json_value_translate_t< ldndc_int32_t >
        : public json_value_translate_int_t< ldndc_int32_t > {};
    template < >
        struct  json_value_translate_t< ldndc_uint32_t >
        : public json_value_translate_int_t< ldndc_uint32_t > {};

    template < typename _T >
        struct  json_value_translate_int64_t
        {
            static
            _T  get( struct json_object  *  jobj)
            {
                return  static_cast< _T >( json_object_get_int64( jobj));
            }
            static
            struct json_object *  set( _T  _value)
            {
                return  json_object_new_int64( _value);
            }
            static _T  invalid()
            {
                return  invalid_t< _T >::value;
            }
            static json_type  type()
            {
                return  json_type_int;
            }
        };

    template < >
        struct  json_value_translate_t< ldndc_int64_t >
        : public json_value_translate_int64_t< ldndc_int64_t > {};
    template < >
        struct  json_value_translate_t< ldndc_uint64_t >
        : public json_value_translate_int64_t< ldndc_uint64_t > {};

    template < typename _T >
        struct  json_value_translate_float_t
        {
            static
            _T  get( struct json_object  *  jobj)
            {
                return  static_cast< _T >( json_object_get_double( jobj));
            }
            static
            struct json_object *  set( _T  _value)
            {
                return  json_object_new_double( _value);
            }
            static _T  invalid()
            {
                return  invalid_t< _T >::value;
            }
            static json_type  type()
            {
                return  json_type_double;
            }
        };
    template < >
        struct  json_value_translate_t< ldndc_flt32_t >
        : public json_value_translate_float_t< ldndc_flt32_t > {};
    template < >
        struct  json_value_translate_t< ldndc_flt64_t >
        : public json_value_translate_float_t< ldndc_flt64_t > {};

    template < typename _T >
        struct  json_value_translate_string_t
        {
            static
            _T  get( struct json_object  *  jobj)
            {
                return  json_object_get_string( jobj);
            }
            static
            struct json_object *  set( _T  _value)
            {
                return  json_object_new_string( _value);
            }
            static _T  invalid()
            {
                return  ldndc_empty_cstring;
            }
            static json_type  type()
            {
                return  json_type_string;
            }
        };
template < >
struct  json_value_translate_t< char const * >
	: public json_value_translate_string_t< char const * > {};
template < >
struct  json_value_translate_t< std::string >
	: public json_value_translate_string_t< char const * > {};

class CBM_API json_document_t
{
    public:
        json_document_t( char const *  _json)
            : obj( NULL), parent( NULL)
        {
            struct json_tokener *  jtokener = json_tokener_new();
            if ( jtokener)
            {
                this->obj =
                    json_tokener_parse_ex( jtokener, _json, cbm::strlen( _json));
                if ( !this->obj || ( json_object_get_type( this->obj) != json_type_object))
                {
                    CBM_LogError( "errors while parsing json document");
                    enum json_tokener_error  jerr =
                        json_tokener_get_error( jtokener);
                    CBM_LogError( "json error: ", json_tokener_error_desc( jerr));
                    this->obj = json_tokener_parse_verbose( _json, &jerr);
                }
            }
            else
            {
                CBM_LogError( "nomem");
            }
        }
        json_document_t()
            : obj( NULL), parent( NULL)
        {
            this->obj = json_object_new_object();
        }
        ~json_document_t()
        {
            /*TODO check reference counter */
			json_object_put( this->obj);
        }

        int  is_good() const { return  this->obj != NULL; }
        int  length() const
        {
            if ( this->obj)
            {
                return  json_object_object_length( this->obj);
            }
            return  0;
        }

		void  clear( char const *  _key = NULL)
		{
			if ( _key)
			{
				json_object_object_del( this->obj, _key);
			}
			else if ( this->parent == NULL)
			{
				json_object_put( this->obj);
				this->obj = json_object_new_object();
			}
		}

        lerr_t  parse( char const *  _json, char const * _key = NULL)
        {
            if ( this->obj)
            {
                json_object_put( this->obj);
            }
            if ( _json)
            {
                if ( !this->parent || ( this->parent && _key))
                {
                    this->obj = json_tokener_parse( _json);
                }
                if ( this->parent && this->obj)
                {
                    json_object_object_add( this->parent, _key, this->obj);
                }
                return  this->obj ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
            }
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        json_document_t  set_node( char const *  _key)
        {
            return  json_document_t( this->obj, _key);
        }
        json_document_t  get_node( char const *  _key)
        {
            struct json_object *  jobj = NULL;
            json_object_object_get_ex( this->obj, _key, &jobj);

            return  json_document_t( jobj, parent);
        }

    public:
        template < typename _T >
        lerr_t  set_scalar( char const *  _key, _T  _value)
        {
            struct json_object *  jsonvalue =
                json_value_translate_t< _T >::set( _value);
            if ( jsonvalue)
            {
                json_object_object_add( this->obj, _key, jsonvalue);
                return  LDNDC_ERR_OK;
            }
            return  LDNDC_ERR_FAIL;
        }
        lerr_t  set_int8( char const *  _key, ldndc_int8_t  _value)
        {
            return  this->set_scalar< ldndc_int8_t >( _key, _value);
        }
        lerr_t  set_uint8( char const *  _key, ldndc_uint8_t  _value)
        {
            return  this->set_scalar< ldndc_uint8_t >( _key, _value);
        }
        lerr_t  set_int16( char const *  _key, ldndc_int16_t  _value)
        {
            return  this->set_scalar< ldndc_int16_t >( _key, _value);
        }
        lerr_t  set_uint16( char const *  _key, ldndc_uint16_t  _value)
        {
            return  this->set_scalar< ldndc_uint16_t >( _key, _value);
        }
        lerr_t  set_int32( char const *  _key, ldndc_int32_t  _value)
        {
            return  this->set_scalar< ldndc_int32_t >( _key, _value);
        }
        lerr_t  set_uint32( char const *  _key, ldndc_uint32_t  _value)
        {
            return  this->set_scalar< ldndc_uint32_t >( _key, _value);
        }
        lerr_t  set_int64( char const *  _key, ldndc_int64_t  _value)
        {
            return  this->set_scalar< ldndc_int64_t >( _key, _value);
        }
        lerr_t  set_uint64( char const *  _key, ldndc_uint64_t  _value)
        {
            return  this->set_scalar< ldndc_uint64_t >( _key, _value);
        }
        lerr_t  set_float32( char const *  _key, ldndc_flt32_t  _value)
        {
            return  this->set_scalar< ldndc_flt32_t >( _key, _value);
        }
        lerr_t  set_float64( char const *  _key, ldndc_flt64_t  _value)
        {
            return  this->set_scalar< ldndc_flt64_t >( _key, _value);
        }
        lerr_t  set_string( char const *  _key, char const *  _value)
        {
            return  this->set_scalar< char const * >( _key,  _value);
        }
        lerr_t  set_string( char const *  _key, std::string const &  _value)
        {
            return  this->set_scalar< char const * >( _key,  _value.c_str());
        }
        lerr_t  set_string( char const *  _key, cbm::string_t const &  _value)
        {
            return  this->set_scalar< char const * >( _key,  _value.c_str());
        }

        lerr_t  set_memaddr( char const *  _key, void *  _memaddr)
        {
            return  this->set_scalar< ldndc_uint64_t >(
						_key, reinterpret_cast< ldndc_uint64_t >( _memaddr));
        }
        lerr_t  set_memaddr( char const *  _key, void const *  _memaddr)
        {
            return  this->set_scalar< ldndc_uint64_t >(
						_key, reinterpret_cast< ldndc_uint64_t >( _memaddr));
        }


    public:
        template < typename _T >
        _T  get_scalar( char const *  _key, bool *  _found = NULL)
        const
        {
            struct json_object *  jobj = NULL;
            json_bool  jkey_found =
                json_object_object_get_ex( this->obj, _key, &jobj);
            if ( jkey_found && jobj)
            {
                enum json_type  jtype = json_object_get_type( jobj);
                if ( jtype == json_value_translate_t< _T >::type())
                {
                    if ( _found) { *_found = true; }
                    return  json_value_translate_t< _T >::get( jobj);
                }
            }
            if ( _found) *_found = false;
            return  json_value_translate_t< _T >::invalid();
        }
        ldndc_int8_t  get_int8( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_int8_t >( _key, _found);
        }
        ldndc_uint8_t  get_uint8( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_uint8_t >( _key, _found);
        }
        ldndc_int16_t  get_int16( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_int16_t >( _key, _found);
        }
        ldndc_uint16_t  get_uint16( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_uint16_t >( _key, _found);
        }
        ldndc_int32_t  get_int32( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_int32_t >( _key, _found);
        }
        ldndc_uint32_t  get_uint32( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_uint32_t >( _key, _found);
        }
        ldndc_int64_t  get_int64( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_int64_t >( _key, _found);
        }
        ldndc_uint64_t  get_uint64( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_uint64_t >( _key, _found);
        }
        ldndc_flt32_t  get_float32( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_flt32_t >( _key, _found);
        }
        ldndc_flt64_t  get_float64( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< ldndc_flt64_t >( _key, _found);
        }
        char const *  get_string( char const *  _key, bool *  _found = NULL)
        const
        {
            return  this->get_scalar< char const * >( _key, _found);
        }

        template < typename _T >
        _T *  get_memaddr( char const *  _key, bool *  _found = NULL)
        const
        {
            bool  found = false;
            _T *  memaddr = reinterpret_cast< _T * >(
                    this->get_scalar< ldndc_uint64_t >( _key, &found));
            if ( _found) { *_found = found; }
            if ( found)
            {
                return  memaddr;
            }
            return  NULL;
        }
 
    public:
        size_t  get_array_size(
                char const *  _key, bool *  _found = NULL)
        {
            struct json_object *  jobj = NULL;
            json_bool  jkey_found =
                json_object_object_get_ex( this->obj, _key, &jobj);
            if ( jkey_found && jobj)
            {
                enum json_type  jtype = json_object_get_type( jobj);
                if ( jtype == json_type_array)
                {
                    if ( _found) { *_found = true; }
                    size_t const  arr_length =
                        json_object_array_length( jobj);
                    return  arr_length;
                }
            }

            if ( _found) { *_found = false; }
            return  0;
        }

        template < typename _T >
        struct  array_t
        {
            typedef  std::vector< _T >  type_t;
        };

        template < typename _T >
        typename array_t< _T >::type_t  get_array(
                char const *  _key, bool *  _found = NULL)
        {
            struct json_object *  jobj = NULL;
            json_bool  jkey_found =
                json_object_object_get_ex( this->obj, _key, &jobj);
            if ( jkey_found && jobj)
            {
                enum json_type  jtype = json_object_get_type( jobj);
                if ( jtype == json_type_array)
                {
                    if ( _found) { *_found = true; }
                    size_t const  arr_length =
                        json_object_array_length( jobj);
                    if ( arr_length)
                    {
                        typename array_t< _T >::type_t  arr( arr_length);
                        for ( size_t  k = 0;  k < arr_length;  ++k)
                        {
                            arr[k] = json_value_translate_t< _T >::get(
                                    json_object_array_get_idx( jobj, k));
                        }
                        return  arr;
                    }
                }
                else
                {
                    if ( _found) { *_found = false; }
                }
            }
            else
            {
                if ( _found) { *_found = false; }
            }
            return  typename array_t< _T >::type_t( 0);
        }
        typedef  array_t< ldndc_int8_t >::type_t  int8_array_t;
        int8_array_t  get_int8_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_int8_t >( _key, _found);
        }
        typedef  array_t< ldndc_uint8_t >::type_t  uint8_array_t;
        uint8_array_t  get_uint8_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_uint8_t >( _key, _found);
        }
        typedef  array_t< ldndc_int16_t >::type_t  int16_array_t;
        int16_array_t  get_int16_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_int16_t >( _key, _found);
        }
        typedef  array_t< ldndc_uint16_t >::type_t  uint16_array_t;
        uint16_array_t  get_uint16_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_uint16_t >( _key, _found);
        }
        typedef  array_t< ldndc_int32_t >::type_t  int32_array_t;
        int32_array_t  get_int32_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_int32_t >( _key, _found);
        }
        typedef  array_t< ldndc_uint32_t >::type_t  uint32_array_t;
        uint32_array_t  get_uint32_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_uint32_t >( _key, _found);
        }
        typedef  array_t< ldndc_int64_t >::type_t  int64_array_t;
        int64_array_t  get_int64_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_int64_t >( _key, _found);
        }
        typedef  array_t< ldndc_uint64_t >::type_t  uint64_array_t;
        uint64_array_t  get_uint64_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_uint64_t >( _key, _found);
        }
        typedef  array_t< ldndc_flt32_t >::type_t  flt32_array_t;
        flt32_array_t  get_flt32_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_flt32_t >( _key, _found);
        }
        typedef  array_t< ldndc_flt64_t >::type_t  flt64_array_t;
        flt64_array_t  get_flt64_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< ldndc_flt64_t >( _key, _found);
        }

        typedef  array_t< std::string >::type_t  string_array_t;
        string_array_t  get_string_array( char const *  _key, bool *  _found = NULL)
        {
            return  this->get_array< std::string >( _key, _found);
        }


        template < typename _T >
        _T  get_array_at(
                char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            struct json_object *  jobj = NULL;
            json_bool  jkey_found =
                json_object_object_get_ex( this->obj, _key, &jobj);
            if ( jkey_found && jobj)
            {
                enum json_type  jtype = json_object_get_type( jobj);
                if ( jtype == json_type_array)
                {
                    if ( _found) { *_found = true; }
                    size_t const  arr_length =
                        json_object_array_length( jobj);
                    if ( _idx < arr_length)
                    {
                            return  json_value_translate_t< _T >::get(
                                    json_object_array_get_idx( jobj, _idx));
		    }
		    return  invalid_t< _T >::value;
                }
            }
            else
            {
                if ( _found) { *_found = false; }
            }
            return  invalid_t< _T >::value;
        }

        ldndc_int8_t  get_int8_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_int8_t >( _key, _idx, _found);
        }
        ldndc_uint8_t  get_uint8_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_uint8_t >( _key, _idx, _found);
        }
        ldndc_int16_t  get_int16_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_int16_t >( _key, _idx, _found);
        }
        ldndc_uint16_t  get_uint16_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_uint16_t >( _key, _idx, _found);
        }
        ldndc_int32_t  get_int32_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_int32_t >( _key, _idx, _found);
        }
        ldndc_uint32_t  get_uint32_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_uint32_t >( _key, _idx, _found);
        }
        ldndc_int64_t  get_int64_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_int64_t >( _key, _idx, _found);
        }
        ldndc_uint64_t  get_uint64_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_uint64_t >( _key, _idx, _found);
        }
        ldndc_flt32_t  get_flt32_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_flt32_t >( _key, _idx, _found);
        }
        ldndc_flt64_t  get_flt64_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< ldndc_flt64_t >( _key, _idx, _found);
        }

        char const *  get_string_array_at(
                        char const *  _key, size_t  _idx, bool *  _found = NULL)
        {
            return  this->get_array_at< char const * >( _key, _idx, _found);
        }


        char const *  to_stream()
        const
        {
            if ( this->obj)
            {
                char const *  jstr =
                    json_object_to_json_string( this->obj);
				return  jstr;
            }
            return  NULL;
        }
        std::string  to_string()
        const
        {
            if ( this->obj)
            {
                char const *  jstr = this->to_stream();
                if ( jstr)
                {
                    return  std::string( jstr);
                }
            }
            return  std::string( "{}");
        }

    private:
        struct json_object *  obj;
        struct json_object *  parent;

        json_document_t(
                struct json_object *  _parent,
                char const *  _key)
            : obj( NULL), parent( _parent)
        {
            this->obj = json_object_new_object();
            if ( this->obj)
            {
                json_object_object_add( this->parent, _key, this->obj);
            }
        }
        json_document_t(
                struct json_object *  _obj,
                struct json_object *  _parent)
            : obj( _obj), parent( _parent)
        {
            if ( this->obj)
            {
                json_object_get( this->obj);
            }
        }

	public:
		json_document_t( json_document_t const &  _json)
				: obj( _json.obj), parent( _json.parent)
		{
			if ( this->obj)
			{
				json_object_get( this->obj);
			}
		}
		json_document_t &  operator=( json_document_t const &  _json)
		{
			if ( &_json != this)
			{
				this->obj = _json.obj;
				this->parent = _json.parent;

				if ( this->obj)
				{
					json_object_get( this->obj);
				}
			}
			return  *this;
		}
};

} /* namespace ldndc end */

#endif  /*  LDNDC_JSONAPI_H_  */

