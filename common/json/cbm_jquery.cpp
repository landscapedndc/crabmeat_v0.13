
/*!
 * @author
 *    steffen klatt (created on: dec 01, 2016)
 *
 * TODO  out-source to cbm_json.h/cbm_json.cpp
 */

#include  <json/rapidjson/document.h>
#include  <json/rapidjson/stringbuffer.h>
#include  <json/rapidjson/writer.h>

#include  "json/cbm_jquery.h"
#include  "log/cbm_baselog.h"

#define  to_jsondoc(doc) static_cast< rapidjson::Document * >( doc)

char const *  cbm::_jquery_t<char const *>::get( cbm::jquery_t *  _jquery,
                char const *  _path, char const *  _default)
    { return _jquery->query_string( _path, _default); }

bool  cbm::_jquery_t<bool>::get( cbm::jquery_t *  _jquery,
                char const *  _path, bool  _default)
    { return _jquery->query_bool( _path, _default); }

int  cbm::_jquery_t<int>::get( cbm::jquery_t *  _jquery,
                char const *  _path, int  _default)
    { return _jquery->query_int( _path, _default); }

double  cbm::_jquery_t<double>::get( cbm::jquery_t *  _jquery,
                char const *  _path, double  _default)
    { return _jquery->query_double( _path, _default); }

float  cbm::_jquery_t<float>::get( cbm::jquery_t *  _jquery,
                char const *  _path, float  _default)
    { return _jquery->query_float( _path, _default); }

static
rapidjson::Value const *
cbm_find_path( cbm::string_t const &  _path, void *  _data)
{
    if ( _path.is_empty() || ( _path[0] != '/'))
    {
        CBM_LogError( "Invalid path  [\"",_path,"\"]");
        return NULL;
    }
    cbm::string_t::cbmstring_array_t const  path = _path.split( '/');
    if ( path.size() < 2 || path[1].is_empty())
        { return  NULL; }
    rapidjson::Value::ConstMemberIterator  i =
        to_jsondoc(_data)->FindMember( path[1].c_str());
    if ( i == to_jsondoc(_data)->MemberEnd())
        { return  NULL; }

    rapidjson::Value const *  v = &i->value;

    int const  K = static_cast< int >( path.size());
    int  n = -1;
    for ( int  k = 2;  k < K;  ++k)
    {
        /* ignore empty path element */
        if ( path[k].size() == 0)
            { continue; }

        char const *  p_k = path[k].c_str();

        if ( v->GetType() == rapidjson::kNullType)
            { return  NULL; }
        if ( !v->IsArray() && ( path[k] == "0"))
            { continue; }
        else if ( v->IsArray())
        {
            lerr_t  rc_slot = cbm::s2n( p_k, &n);
            if ( rc_slot)
                { return  NULL; }
            if ( v->GetArray().Size() < (rapidjson::SizeType)n)
                { return  NULL; }
            v = &(*v)[rapidjson::SizeType(n)];
            continue;
        }
        else if ( v->IsObject())
            { /* all ok */ }
        else
            { return  NULL; }

        i = v->FindMember( p_k);
        if ( i == v->MemberEnd())
            { return  NULL; }
        v = &i->value;
    }
    return  v;
}


cbm::jquery_t::jquery_t( char const *  _data,
            size_t  _data_sz, char const *  _base)
    : m_str( NULL), m_str_sz( 0), m_data( NULL)
{
    if ( _data)
    {
        this->m_data = new rapidjson::Document();
        if ( this->m_data)
        {
            size_t  data_sz = _data_sz;
            if ( data_sz == 0)
                { data_sz = cbm::strlen( _data); }
            this->m_str = cbm::strndup( _data, data_sz);
            this->m_str_sz = data_sz;

            rapidjson::ParseResult  parseok =
                to_jsondoc(this->m_data)->Parse( this->m_str);
            if ( parseok && _base && !cbm::is_empty( _base))
            {
                char const *  nested = this->nested_string( _base);
                if ( nested)
                {
                    size_t  nested_sz = cbm::strlen( nested);
                    if ( nested_sz > 0)
                    {
                        cbm::strfree( this->m_str);
                        this->m_str = cbm::strndup( nested, nested_sz);
                        this->m_str_sz = nested_sz;

                        parseok = to_jsondoc(this->m_data)->Parse( this->m_str);
                    }
                }
            }
            if ( !parseok)
            {
                delete to_jsondoc(this->m_data);
                this->m_data = NULL;
                cbm::strfree( this->m_str);
                this->m_str = NULL;
                this->m_str_sz = 0;
            }
        }
    }
    else
        { CBM_Assert( _data_sz == 0); }
}

cbm::jquery_t::jquery_t( cbm::jquery_t const &  _rhs)
    : m_str( NULL), m_str_sz( 0), m_data( NULL)
{
    if ( _rhs.m_str)
    {
        this->m_str = cbm::strndup( _rhs.m_str, _rhs.m_str_sz);
        this->m_str_sz = _rhs.m_str_sz;

        this->m_data = new rapidjson::Document();
        to_jsondoc(this->m_data)->Parse( this->m_str);
    }
}

cbm::jquery_t &
cbm::jquery_t::operator=( cbm::jquery_t const &  _rhs)
{
    if ( this != &_rhs)
    {
        this->m_str_sz = _rhs.m_str_sz;

        if ( this->m_str)
            { cbm::strfree( this->m_str); }
        if ( _rhs.m_str)
        {
            this->m_str = cbm::strndup( _rhs.m_str, _rhs.m_str_sz);

            if ( this->m_data)
                { delete to_jsondoc(this->m_data); }
            this->m_data = new rapidjson::Document();
            to_jsondoc(this->m_data)->Parse( this->m_str);
        }
        else
            { this->m_data = NULL; }
    }
    return  *this;
}

cbm::jquery_t::~jquery_t()
{
    if ( this->m_data)
    {
        delete to_jsondoc(this->m_data);
        this->m_data = NULL;
    }
    if ( this->m_str)
    {
        cbm::strfree( this->m_str);
        this->m_str = NULL;
    }
    this->m_str_sz = 0;
}

static rapidjson::StringBuffer  jstrbuf;
char const *
cbm::jquery_t::nested_string( char const *  _path) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsObject())
    {
        jstrbuf.Clear(); /*NOTE beware of shared buffer*/
        rapidjson::Writer<rapidjson::StringBuffer>  jwriter( jstrbuf);
        v->Accept( jwriter);
        return jstrbuf.GetString();
    }
    return NULL;
}
cbm::jquery_t
cbm::jquery_t::nested_object( char const *  _path) const
{
    char const *  nested = nested_string( _path);
    if ( nested)
        { return  jquery_t( nested, 0, NULL); }
    return  jquery_t( NULL, 0);
}

bool  cbm::jquery_t::parse_ok() const
    { return  this->m_data != NULL; }

bool cbm::jquery_t::exists( char const *  _path) const
{
    return  this->exists( cbm::string_t( _path));
}
bool cbm::jquery_t::exists( cbm::string_t const &  _path) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    return  v != NULL;
}

int cbm::jquery_t::query_size( char const *  _path) const
{
    CBM_Assert( _path);
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v)
    {
        if ( v->IsArray())
            { return  v->GetArray().Size(); }
        else
            { return  1; }
    }
    return  -1;
}


char const * cbm::jquery_t::query_string( char const *  _path,
        char const *  _default) const
{
    CBM_Assert( _path);
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsString())
        { return  v->GetString(); }
    return  _default;
}
cbm::string_t cbm::jquery_t::query_string( cbm::string_t const &  _path,
        char const *  _default) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsString())
        { return  v->GetString(); }
    return  _default;
}


bool cbm::jquery_t::query_bool(
        char const *  _path, bool  _default) const
{
    CBM_Assert( _path);
    return this->query_bool( cbm::string_t( _path), _default);
}
bool cbm::jquery_t::query_bool(
    cbm::string_t const &  _path, bool  _default) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v)
    {
        if ( v->IsBool())
            { return  v->GetBool(); }
        else if ( v->IsInt())
        {
           if ( v->GetInt() == 0)
               { return false; }
           else if ( v->GetInt() == 1)
               { return true; }
           /* fall through */
        }
        else if ( v->IsString())
        {
            bool  asbool;
            lerr_t  rc_tobool =
                cbm::s2n( v->GetString(), &asbool);
            if ( rc_tobool)
                { /* fall through */ }
            return  asbool;
        }
    }
    return  _default;
}


int cbm::jquery_t::query_int(
        char const *  _path, int  _default) const
{
    CBM_Assert( _path);
    return this->query_int( cbm::string_t( _path), _default);
}
int cbm::jquery_t::query_int(
    cbm::string_t const &  _path, int  _default) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsInt())
        { return  v->GetInt(); }
    return  _default;
}

double cbm::jquery_t::query_double_and_int(
    char const *  _path, double  _default) const
{
    CBM_Assert( _path);
    return this->query_double_and_int( cbm::string_t( _path), _default);
}
double cbm::jquery_t::query_double_and_int(
    cbm::string_t const &  _path, double  _default) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsDouble())
        { return  static_cast< double >(v->GetDouble()); }
    else if ( v && v->IsInt())
        { return  static_cast< double >(v->GetInt()); }
    else if ( v && v->IsUint())
        { return  static_cast< double >(v->GetUint()); }
    else if ( v && v->IsInt64())
        { return  static_cast< double >(v->GetInt64()); }
    else if ( v && v->IsUint64())
        { return  static_cast< double >(v->GetUint64()); }
    return  _default;
}

double cbm::jquery_t::query_double(
    char const *  _path, double  _default) const
{
    CBM_Assert( _path);
    return this->query_double( cbm::string_t( _path), _default);
}
double cbm::jquery_t::query_double(
    cbm::string_t const &  _path, double  _default) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsDouble())
        { return  v->GetDouble(); }
    return  _default;
}

int cbm::jquery_t::query_doubles( char const *  _path,
        double *  _buf, size_t  _buf_size) const
{
    CBM_Assert( _path);
    return this->query_doubles( cbm::string_t( _path), _buf, _buf_size);
}
int cbm::jquery_t::query_doubles( cbm::string_t const &  _path,
        double *  _buf, size_t  _buf_size) const
{
    int  buf_size = _buf_size;
    if ( buf_size == 0)
        { buf_size = this->query_size( _path.c_str()); }
    if ( buf_size < 0)
        { /* invalid */ }
    else if ( buf_size > 0)
    {
        rapidjson::Value const *  v =
            cbm_find_path( _path, this->m_data);
        if ( v && v->IsArray())
        {
            /* allow truncating data */
            buf_size = ((int)v->Size() < buf_size) ? v->Size() : buf_size;
            for ( int  k = 0;  k < buf_size;  ++k)
                { _buf[k] = (*v)[rapidjson::SizeType(k)].GetDouble(); }
           
            return  buf_size;
        }
    }
    else
        { return  0; }
    return  -1;
}

float cbm::jquery_t::query_float(
    char const *  _path, float  _default) const
{
    CBM_Assert( _path);
    return this->query_float( cbm::string_t( _path), _default);
}
float cbm::jquery_t::query_float(
    cbm::string_t const &  _path, float  _default) const
{
    return  static_cast< float >( this->query_double( _path, _default));
}

int cbm::jquery_t::query_floats( char const *  _path,
        float *  _buf, size_t  _buf_size) const
{
    CBM_Assert( _path);
    return this->query_floats( cbm::string_t( _path), _buf, _buf_size);
}
#include  "memory/cbm_mem.h"
int cbm::jquery_t::query_floats( cbm::string_t const &  _path,
        float *  _buf, size_t  _buf_size) const
{
    int  buf_size = _buf_size;
    if ( buf_size == 0)
        { buf_size = this->query_size( _path.c_str()); }
    if ( buf_size < 0)
        { /* invalid */ }
    else if ( buf_size > 0)
    {
        double *  pbuf = (double *)CBM_Allocate( sizeof(double)*buf_size);
        if ( pbuf)
        {
            int  nb_doubles = this->query_doubles( _path, pbuf, buf_size);
            CBM_Assert( nb_doubles<=buf_size);
            if ( nb_doubles > 0)
            {
                float *  buf_f = _buf;
                float const *  buf_f_end = buf_f + nb_doubles;
                double *  buf_d = pbuf;
                while ( buf_f < buf_f_end)
                    { *buf_f++ = static_cast< float >( *buf_d++); }
            }
            CBM_Free( pbuf);
            return  nb_doubles;
        }
    }
    else
        { return  0; }
    return  -1;
}


cbm::string_t cbm::jquery_t::query_any( char const *  _path,
        char const *  _default) const
{
    CBM_Assert( _path);
    return this->query_any( cbm::string_t( _path), _default);
}

cbm::string_t cbm::jquery_t::query_any( cbm::string_t const &  _path,
        char const *  _default) const
{
    rapidjson::Value const *  v =
        cbm_find_path( _path, this->m_data);
    if ( v && v->IsObject())
    {
        rapidjson::Value::ConstMemberIterator  i = v->MemberBegin();
        rapidjson::Value::ConstMemberIterator  e = v->MemberEnd();
        cbm::string_t  kv;
        v = NULL;
        while ( i != e)
        {
            if ( !i->name.IsString())
                { continue; }

            if ( v != NULL)
                { kv << ';'; }

            kv << i->name.GetString() << '=';
            if ( i->value.IsString())
                { kv << i->value.GetString(); }
            else if ( i->value.IsDouble())
                { kv << i->value.GetDouble(); }
            else if ( i->value.IsFloat())
                { kv << i->value.GetFloat(); }
            else if ( i->value.IsBool())
                { kv << ( i->value.GetBool() ? "true" : "false"); }
            else if ( i->value.IsInt())
                { kv << i->value.GetInt(); }
            else if ( i->value.IsUint())
                { kv << i->value.GetUint(); }
            else if ( i->value.IsInt64())
                { kv << i->value.GetInt64(); }
            else if ( i->value.IsUint64())
                { kv << i->value.GetUint64(); }
            else if ( i->value.IsArray())
            {
                CBM_LogWarn( "seeing array in kernel setup data: TODO");
                kv << "";
            }
            else /* "ignore" */
                { kv << ""; }

            ++i;
            v = &i->value;
        }
        return  kv;
    }
    return  _default;
}

#undef  to_jsondoc

