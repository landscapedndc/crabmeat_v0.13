/*!
 * @author
 *    steffen klatt (created on: mar 15, 2017)
 */

#include  <json/rapidjson/document.h>
#include  <json/rapidjson/stringbuffer.h>
#include  <json/rapidjson/writer.h>

#include  "json/cbm_jbuild.h"
#include  "log/cbm_baselog.h"

#define  to_jsondoc(doc) static_cast< rapidjson::Document * >( doc)
#define  to_jsonbuffer(buf) static_cast< rapidjson::StringBuffer * >( buf)

lerr_t  cbm::_jbuild_t<char const *>::put( cbm::jbuild_t *  _jbuilder,
                char const *  _path, char const *  _value)
    { return _jbuilder->put_string( _path, _value); }

lerr_t  cbm::_jbuild_t<bool>::put( cbm::jbuild_t *  _jbuilder,
                char const *  _path, bool  _value)
    { return _jbuilder->put_bool( _path, _value); }

lerr_t  cbm::_jbuild_t<int>::put( cbm::jbuild_t *  _jbuilder,
                char const *  _path, int  _value)
    { return _jbuilder->put_int( _path, _value); }

lerr_t  cbm::_jbuild_t<double>::put( cbm::jbuild_t *  _jbuilder,
                char const *  _path, double  _value)
    { return _jbuilder->put_double( _path, _value); }

lerr_t  cbm::_jbuild_t<float>::put( cbm::jbuild_t *  _jbuilder,
                char const *  _path, float  _value)
    { return _jbuilder->put_float( _path, _value); }


cbm::jbuild_t::jbuild_t( int  _flags)
    : m_data( NULL), m_buffer( NULL),
        m_flags( _flags), m_isdirty( 1)
{
    this->m_data = new rapidjson::Document();
    if ( this->m_data)
    {
        to_jsondoc(this->m_data)->SetObject();
        this->m_buffer = new rapidjson::StringBuffer();
    }
}

cbm::jbuild_t::jbuild_t( jbuild_t const &  _rhs)
        : m_data( NULL), m_flags( _rhs.m_flags), m_isdirty( 1)
{
    /*TODO*/
}

cbm::jbuild_t &
cbm::jbuild_t::operator=( cbm::jbuild_t const &  _rhs)
{
    if ( this == &_rhs)
        { return *this; }

    /*TODO*/

    return *this;
}

cbm::jbuild_t::~jbuild_t()
{
    if ( this->m_data)
    {
        delete to_jsondoc(this->m_data);
        this->m_data = NULL;
    }
    if ( this->m_buffer)
    {
        delete to_jsonbuffer(this->m_buffer);
        this->m_buffer = NULL;
    }
    this->m_flags = 0;
}

#include  "string/lstring-basic.h"
static char const *  cbm_key_from_path( char const *  _path)
{
    return strrchr( _path, '/') + 1;
}
static rapidjson::Value *  cbm_find_path(
    cbm::string_t const &  _path, void *  _data)
{
    if ( _path.is_empty() || ( _path[0] != '/'))
    {
        CBM_LogError( "Invalid path  [\"",_path,"\"]");
        return NULL;
    }
    cbm::string_t::cbmstring_array_t const  path = _path.split( '/');
    if ( path.size() < 2 || path[1].is_empty())
        { return NULL; }
    else if ( path.size() == 2)
        { return to_jsondoc(_data); }

    rapidjson::Value::MemberIterator  i;
    rapidjson::Value *  v = to_jsondoc(_data);

    int const  K = static_cast< int >( path.size()) - 1;
    int  n = -1;
    for ( int  k = 1;  k < K;  ++k)
    {
        /* ignore empty path element */
        if ( path[k].size() == 0)
            { continue; }

        char const *  p_k = path[k].c_str();

        i = v->FindMember( p_k);
        if ( i == v->MemberEnd())
        {
// sk:dbg            CBM_LogDebug( "'", path[k], "' does not exist.. creating it..");
            rapidjson::Value  new_key( p_k, to_jsondoc(_data)->GetAllocator());
            rapidjson::Value  new_object( rapidjson::kObjectType);
            v->AddMember( new_key, new_object,
                    to_jsondoc(_data)->GetAllocator());

            i = v->FindMember( p_k);
            v = &i->value;
        }
        else
        {
            v = &i->value;

            if ( !v || v->GetType() == rapidjson::kNullType)
                { return NULL; }
            if ( !v->IsArray() && ( path[k] == "0"))
                { continue; }
            else if ( v->IsArray())
            {
                lerr_t  rc_slot = cbm::s2n( p_k, &n);
                if ( rc_slot)
                    { return NULL; }
                if ( v->GetArray().Size() < (rapidjson::SizeType)n)
                    { return NULL; }
                v = &(*v)[rapidjson::SizeType(n)];
                continue;
            }
            else if ( v->IsObject())
                { /* all ok */ }
            else
                { return NULL; }
        }
    }
    return  v;
}

#include  "utils/cbm_utils.h"
template < typename _T >
static
lerr_t  _put( void *  _data, int  _flags, int *  _dirty,
                char const *  _path, _T  _value)
{
    if (( _flags & CBM_JBUILD_IGNOREINVALID) && cbm::is_invalid<_T>( _value))
        { return LDNDC_ERR_OK; }

    rapidjson::Value *  v = cbm_find_path( _path, _data);
    if ( v && v->IsObject())
    {
        v->AddMember( rapidjson::StringRef( cbm_key_from_path( _path)),
                _value, to_jsondoc(_data)->GetAllocator());

        *_dirty = 1;
        return LDNDC_ERR_OK;
    }
    return LDNDC_ERR_FAIL;
}

lerr_t  cbm::jbuild_t::put_string( char const *  _path,
                char const *  _value, size_t  _value_sz)
{
    if (( this->m_flags & CBM_JBUILD_IGNOREINVALID) &&
            (( _value_sz==static_cast<size_t>(-1) /*implies '\0' terminated*/) && cbm::is_invalid( _value)))
        { return LDNDC_ERR_OK; }

    rapidjson::Value *  v = cbm_find_path( _path, this->m_data);
    if ( v && v->IsObject())
    {
        size_t  value_sz = _value_sz;
        if ( _value_sz == static_cast<size_t>(-1))
            { value_sz = cbm::strlen( _value); }
        rapidjson::Value  string_value( _value, value_sz,
                        to_jsondoc(this->m_data)->GetAllocator());
        v->AddMember( rapidjson::StringRef( cbm_key_from_path( _path)),
                string_value, to_jsondoc(this->m_data)->GetAllocator());

        this->m_isdirty = 1;
        return LDNDC_ERR_OK;
    }
    return LDNDC_ERR_FAIL;

}
lerr_t  cbm::jbuild_t::put_string(
                cbm::string_t const &  _path,
            char const *  _value, size_t  _value_sz)
{
    return this->put_string( _path.c_str(), _value, _value_sz);
}

lerr_t  cbm::jbuild_t::put_bool(
    char const *  _path, bool  _value)
{
    return _put<bool>( this->m_data,
        this->m_flags, &this->m_isdirty, _path, _value);
}
lerr_t  cbm::jbuild_t::put_bool(
    cbm::string_t const &  _path, bool  _value)
{
    return this->put_bool( _path.c_str(), _value);
}

lerr_t  cbm::jbuild_t::put_int(
    char const *  _path, int  _value)
{
    return _put<int>( this->m_data,
        this->m_flags, &this->m_isdirty, _path, _value);
}
lerr_t  cbm::jbuild_t::put_int(
    cbm::string_t const &  _path, int  _value)
{
    return this->put_int( _path.c_str(), _value);
}

lerr_t  cbm::jbuild_t::put_double(
    char const *  _path, double  _value)
{
    return _put<double>( this->m_data,
        this->m_flags, &this->m_isdirty, _path, _value);
}
lerr_t  cbm::jbuild_t::put_double(
    cbm::string_t const &  _path, double  _value)
{
    return this->put_double( _path.c_str(), _value);
}

lerr_t  cbm::jbuild_t::put_float(
    char const *  _path, float  _value)
{
    return _put<float>( this->m_data,
        this->m_flags, &this->m_isdirty, _path, _value);
}
lerr_t  cbm::jbuild_t::put_float(
    cbm::string_t const &  _path, float  _value)
{
    return this->put_float( _path.c_str(), _value);
}

char const *  cbm::jbuild_t::serialize( size_t *  _size) const
{
    if ( this->m_isdirty)
    {
        to_jsonbuffer(this->m_buffer)->Clear();
        rapidjson::Writer< rapidjson::StringBuffer >
                json_writer( *to_jsonbuffer(this->m_buffer));
        to_jsondoc(this->m_data)->Accept( json_writer);

        this->m_isdirty = 0;
    }
    if ( _size)
        { *_size = to_jsonbuffer(this->m_buffer)->GetSize(); }
    return  to_jsonbuffer(this->m_buffer)->GetString();
}

