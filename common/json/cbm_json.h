/*!
 * @brief
 *      JSON API
 *
 * @author
 *      steffen klatt (created on: jan 24, 2015)
 */

#ifndef  CBM_JSONAPI_H_
#define  CBM_JSONAPI_H_

#include  <cstdio>
#include  <json/rapidjson/document.h>
#include  <json/rapidjson/filereadstream.h>
#include  <json/rapidjson/stringbuffer.h>
#include  <json/rapidjson/writer.h>

#include  "json/cbm_jquery.h"
#include  "json/cbm_jbuild.h"

#endif  /*  CBM_JSONAPI_H_  */

