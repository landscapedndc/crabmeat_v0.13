/*!
 * @brief
 *    defines not-a-number constants
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 *    edwin haas
 */

#include  "crabmeat-common.h"
#include  "cbm_nan.h"


signed char const  invalid_schr = (signed char)-99;
unsigned char const  invalid_uchr = (unsigned char)-1;
signed short int const  invalid_sint = (short int)-9999;
unsigned short int const  invalid_suint = (unsigned short int)-1;
signed int const  invalid_int = (int)-9999;
unsigned int const  invalid_uint = (unsigned int)-1;
signed long int const  invalid_lint = (long int)invalid_int;
unsigned long int const  invalid_luint = (unsigned long int)-1;
signed long long int const  invalid_llint = (long long int)invalid_int;
unsigned long long int const  invalid_lluint = (unsigned long long int)-1;

const size_t  invalid_size = (size_t)-1;

double const  invalid_dbl = (double)-99.99;
float const  invalid_flt = (float)invalid_dbl;

char const  invalid_chr = '\0';
char const *  invalid_str = "NONE";


#define  LDNDC_INVALID_VAL_DEFN(__type__,__value__)                \
    __type__ const  ldndc::invalid_t< __type__ >::value = __value__;


LDNDC_INVALID_VAL_DEFN(signed char,invalid_schr)
LDNDC_INVALID_VAL_DEFN(unsigned char,invalid_uchr)
LDNDC_INVALID_VAL_DEFN(signed short int,invalid_sint)
LDNDC_INVALID_VAL_DEFN(unsigned short int,invalid_suint)
LDNDC_INVALID_VAL_DEFN(signed int,invalid_int)
LDNDC_INVALID_VAL_DEFN(unsigned int,invalid_uint)
LDNDC_INVALID_VAL_DEFN(signed long int,invalid_lint)
LDNDC_INVALID_VAL_DEFN(unsigned long int,invalid_luint)
LDNDC_INVALID_VAL_DEFN(signed long long int,invalid_llint)
LDNDC_INVALID_VAL_DEFN(unsigned long long int,invalid_lluint)

LDNDC_INVALID_VAL_DEFN(float,invalid_flt)
LDNDC_INVALID_VAL_DEFN(double,invalid_dbl)

LDNDC_INVALID_VAL_DEFN(char,invalid_chr)
LDNDC_INVALID_VAL_DEFN(std::string,invalid_str)

