/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: apr 20, 2014),
 *    edwin haas
 */

#ifndef  CBM_KEYVALUEBASE_H_
#define  CBM_KEYVALUEBASE_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"
#include  "string/lstring-compare.h"

#include  "containers/ltinyhashmap.h"

#include  <stdio.h>

namespace cbm
{
    template < size_t  _tbl_sz, size_t  _key_max_sz, size_t  _value_max_sz >
    class keyvaluebase_t
    {
        typedef  tinyhashmap_t< _tbl_sz, char, _value_max_sz, _key_max_sz >  container_t;
        public:
            typedef  typename container_t::key_type  key_type;
            typedef  typename container_t::key_list_t  key_list_t;
            enum
            {
                KEY_MAXSIZE = _key_max_sz,
                VALUE_MAXSIZE = _value_max_sz
            };

            void  dump() const;

            bool  exists(
                    char const * /*key*/) const;
            bool  exists(
                    char const * /*section*/,
                    char const * /*key*/) const;

            int  insert(
                    char const *,
                    char const *, char const *);

            char const *  find(
                    char const * /*key*/) const;
            char const *  find(
                    char const * /*section*/,
                    char const * /*key*/) const;
            template < typename  _T >
            lerr_t  query(
                    char const *,
                    _T *) const;
            template < typename  _T >
            lerr_t  query(
                    char const *,
                    char const *,
                    _T *) const;
        
            size_t  find_num(
                    char const * /*regex key*/) const;
            size_t  find_matching(
                    key_list_t * /*buffer holding matching keys*/,
                    char const * /*regex key*/) const;

            static int  make_key(
                    key_type *, char const * /*format*/, ...);
            static size_t  max_keysize() { return  KEY_MAXSIZE; }
            static size_t  max_valuesize() { return  VALUE_MAXSIZE; }

            int  get_number_of_entries() const { return  this->cntr.get_number_of_entries(); }

        protected:
            void  remove_all() { this->cntr.remove_all(); }

        private:
            container_t  cntr;
    };
}

#include  "string/lstring-convert.h"
template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
template < typename  _T >
lerr_t
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::query(
        char const *  _key, _T *  _value)
const
{
    char const *  val = this->cntr.get( _key);
    if ( !val)
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    return  cbm::s2n< _T >( val, _value);
}
template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
template < typename  _T >
lerr_t
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::query(
        char const *  _section, char const *  _key,
        _T *  _value)
const
{
    typename  container_t::key_type  key;
    container_t::make_item_key( &key, "%s.%s", _section, _key);
    return  this->query( key.name, _value);
}

#include  "containers/lkeyvalbase.inl"

#endif /* !CBM_KEYVALUEBASE_H_ */

