/*!
 * @brief 
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */

#define  __LINKARRAY_OPS_BUFFER_HEADER_SIZE  static_record_size_t<1>::__BUFFER_HEADER_SIZE
#define  __LINKARRAY_OPS_BUFFER_RECORD_LINK_SIZE  static_record_size_t<1>::__BUFFER_RECORD_LINK_SIZE

#define  __LINKARRAY_OPS_RECSIZE(__rsize__)     \
        ((__rsize__) + (( _flags & LINKARRAY_LINK_RECORDS) ? ((_flags & LINKARRAY_SIZE_FINAL) ? 0 : __LINKARRAY_OPS_BUFFER_RECORD_LINK_SIZE) : 0))

template < typename  _E >
ldndc::linkarray_operations_t< _E  >::linkarray_operations_t(
                record_size_t  _record_size,
                linkarray_flag_t  _flags)
                : record_size_( __LINKARRAY_OPS_RECSIZE(_record_size)),
                  BUFFER_HEADER_SIZE( __LINKARRAY_OPS_RECSIZE(_record_size)*((__LINKARRAY_OPS_BUFFER_HEADER_SIZE/__LINKARRAY_OPS_RECSIZE(_record_size)) + ((__LINKARRAY_OPS_BUFFER_HEADER_SIZE==((__LINKARRAY_OPS_BUFFER_HEADER_SIZE/__LINKARRAY_OPS_RECSIZE(_record_size))*__LINKARRAY_OPS_RECSIZE(_record_size))) ? 0 : 1))),
                  BUFFER_RECORD_LINK_SIZE( __LINKARRAY_OPS_BUFFER_RECORD_LINK_SIZE)
{
// sk:dbg        CBM_LogDebug( "record-size=", _record_size, "  buffer-header-size=", BUFFER_HEADER_SIZE,  "  buffer-record-link-size=",BUFFER_RECORD_LINK_SIZE);
    this->flags.link_records = ( _flags & LINKARRAY_LINK_RECORDS);
    this->flags.size_final = ( _flags & LINKARRAY_SIZE_FINAL);
}


template < typename  _E >
size_t
ldndc::linkarray_operations_t< _E >::capacity(
        element_type const *  _buf)
const
{
    return  ((buffer_header_t*)(_buf-this->BUFFER_HEADER_SIZE))->capacity;
}


template < typename  _E >
size_t
ldndc::linkarray_operations_t< _E >::record_size()
const
{
    return  (size_t)this->record_size_;
}

#include  "memory/cbm_mem.h"
template < typename  _E >
typename ldndc::linkarray_operations_t< _E >::element_type *
ldndc::linkarray_operations_t< _E >::allocate(
                size_t  _size, header_data_t const *  _hx)
{
    element_type *  buf = CBM_DefaultAllocator->allocate_type< element_type >(
            (this->record_size_*_size)+this->BUFFER_HEADER_SIZE);
    if ( buf)
    {
        buffer_header_t  b_hdr;
        if ( _hx)
        {
            b_hdr.hx = *_hx;
        }

        b_hdr.lnk.buf_lo = NULL;
        b_hdr.lnk.buf_hi = NULL;

        b_hdr.capacity = _size;

        this->write_buffer_header( buf+this->BUFFER_HEADER_SIZE, &b_hdr);

        return  buf+this->BUFFER_HEADER_SIZE;
    }
    return  NULL;
}
template < typename  _E >
void
ldndc::linkarray_operations_t< _E >::deallocate(
                element_type *  _buf)
{
    CBM_DefaultAllocator->deallocate( _buf-this->BUFFER_HEADER_SIZE);
}
 
#include  "utils/lutils.h"
template < typename  _E >
void
ldndc::linkarray_operations_t< _E  >::mem_set(
                element_type *  _buf,
                size_t  _n,
                element_type const &  _value)
{
    if ( !_buf)
    {
        return;
    }

    if ( this->flags.link_records)
    {
        element_type *  buf = _buf;
        for ( size_t  k = 0;  k < _n;  ++k, buf+=this->record_size_)
        {
            cbm::mem_set( buf, this->record_size_ - this->BUFFER_RECORD_LINK_SIZE, _value);
        }
    }
    else
    {
        cbm::mem_set( _buf, _n * this->record_size_, _value);
    }
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::read_buffer_header(
        buffer_header_t *  _hdr, element_type const *  _buf)
{
    if ( !_hdr || !_buf)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    memcpy( (void*)_hdr, (void const *)(_buf-this->BUFFER_HEADER_SIZE), sizeof( buffer_header_t));
    return  LDNDC_ERR_OK;
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::write_buffer_header(
        element_type *  _buf, buffer_header_t const *  _hdr)
{
    if ( !_buf || !_hdr)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    memcpy( (void*)(_buf-this->BUFFER_HEADER_SIZE), (void const *)_hdr, sizeof( buffer_header_t));
    return  LDNDC_ERR_OK;
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::link(
                element_type *  _buf_0,  element_type *  _buf_1)
{
    if ( !_buf_0 || !_buf_1)
    {
        return  LDNDC_ERR_FAIL;
    }

    buffer_header_t  b_hdr_0;
    this->read_buffer_header( &b_hdr_0, _buf_0);
    buffer_header_t  b_hdr_1;
    this->read_buffer_header( &b_hdr_1, _buf_1);

    b_hdr_0.lnk.buf_hi = _buf_1;
    this->write_buffer_header( _buf_0, &b_hdr_0);
    b_hdr_1.lnk.buf_lo = _buf_0;
    this->write_buffer_header( _buf_1, &b_hdr_1);

    return  LDNDC_ERR_OK;
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::read_record_link(
        buffer_record_link_t *  _rlnk, element_type const *  _buf)
{
    if ( !_rlnk || !_buf)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    memcpy( (void*)_rlnk, (void const *)(_buf+this->record_size_-this->BUFFER_RECORD_LINK_SIZE), sizeof( buffer_record_link_t));
    return  LDNDC_ERR_OK;
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::write_record_link(
        element_type *  _buf, buffer_record_link_t const *  _rlnk)
{
    if ( !_buf || !_rlnk)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    memcpy( (void *)(_buf+this->record_size_-this->BUFFER_RECORD_LINK_SIZE), (void const *)_rlnk, sizeof( buffer_record_link_t));
    return  LDNDC_ERR_OK;
}



template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::link_records(
                element_type *  _buf_0, element_type *  _buf_1,
                int  _size, int  _stride)
{
    if ( _size == 0)
    {
        return  LDNDC_ERR_OK;
    }
    if ( !_buf_0 || !_buf_1 || !_stride)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    element_type *  buf_lo = ( _stride > 0) ? _buf_0 : _buf_1;
    element_type *  buf_hi = ( _stride > 0) ? _buf_1 : _buf_0;

    size_t const  S = _size;
    size_t const  R = ( _stride < 0) ? -_stride : _stride; 
    for ( size_t  s = 0;  s < S;  ++s)
    {
        buffer_record_link_t  r_lnk_l;
        this->read_record_link( &r_lnk_l, buf_lo);
        r_lnk_l.buf_hi = buf_hi;
        this->write_record_link( buf_lo, &r_lnk_l);

        buffer_record_link_t  r_lnk_h;
        this->read_record_link( &r_lnk_h, buf_hi);
        r_lnk_h.buf_lo = buf_lo;
        for ( size_t  r = 0;  r < R;  ++r)
        {
            this->write_record_link( buf_hi, &r_lnk_h);
            buf_hi += this->record_size_;
        }

        buf_lo += this->record_size_;
    }
    return  LDNDC_ERR_OK;
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::find_lower(
                element_type **  _buf_l,
                element_type const *  _buf)
{
    if ( !_buf)
    {
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }
    buffer_header_t  b_hdr;
    this->read_buffer_header( &b_hdr, _buf);
    if ( _buf_l)
    {
        *_buf_l = b_hdr.lnk.buf_lo;
    }
    if ( b_hdr.lnk.buf_lo)
    {
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
}


template < typename  _E >
lerr_t
ldndc::linkarray_operations_t< _E >::find_higher(
                element_type **  _buf_h,
                element_type const *  _buf)
{
    if ( !_buf)
    {
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }
    buffer_header_t  b_hdr;
    this->read_buffer_header( &b_hdr, _buf);
    if ( _buf_h)
    {
        *_buf_h = b_hdr.lnk.buf_hi;
    }
    if ( b_hdr.lnk.buf_hi)
    {
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
}

