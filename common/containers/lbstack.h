/*!
 * @brief 
 *    bounded stack. bounded means maximum size
 *    is known at instantiation time.
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */
#ifndef  LDNDC_BOUNDED_STACK_H_
#define  LDNDC_BOUNDED_STACK_H_

#include  "crabmeat-common.h"

namespace  ldndc
{

template < class  _E >
class  bounded_stack
{
    public:
        typedef  _E  element_type;

    public:
        bounded_stack(
                size_t = 1,
                element_type const & = element_type(),
                element_type * = NULL);

        bounded_stack(
                bounded_stack< element_type > const &);

        bounded_stack &  operator=(
                bounded_stack< element_type > const &);

        ~bounded_stack();

        size_t  size()
        const;

        size_t  fillsize()
        const;

        bool  is_empty()
        const;

        bool  is_full()
        const;

        void  clear();

        element_type const &  push( element_type const &);

        element_type const &  peek();
        element_type const &  pop();

    private:
        /* current position in stack */
        size_t  p_;
        /* maximum size of stack */
        size_t  size_;

        /* indicator for empty stack */
        bool  is_empty_;

        /* buffer to hold items */
        element_type *  buf_;
        /* indicates if object is owner of the buffer */
        bool  owner_;
};
}  /*  namespace ldndc  */

#include  "containers/lbstack.inl"

#endif  /*  !LDNDC_BOUNDED_STACK_H_  */

