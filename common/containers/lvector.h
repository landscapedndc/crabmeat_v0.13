/*!
 * @brief
 *    array wrapper with support for multi-dimensional
 *    indexing currently using blitz++ as back-end.
 *
 * @author 
 *    steffen klatt (created on: may 14, 2012, recreated on: dec 15, 2013),
 *    edwin haas
 */

#ifndef  CBM_VECTOR_H__
#define  CBM_VECTOR_H__

#include  "crabmeat-common.h"
#include  "log/cbm_baselog.h"
#include  "blitz-backend.h"

#if  defined(_DEBUG) && !defined(CRABMEAT_OPENMP)
//#  define  _INTL_HAVE_USAGE_COUNTER
#endif

template < typename _E, unsigned int _R >
class  lvector_t;

/*!
 * @brief
 *    base class for array wrapper classes, it contains various
 *    convenience functions
 *
 * @param
 *    element type
 * @param
 *    rank
 */
template < typename _E, unsigned int _R >
class  lvector_interface_t
{
    public:
        typedef  _E  value_type;
        typedef  int  extent_type;
        typedef  size_t  size_type;

        typedef  blitz::Array< _E, _R >  vector_buffer_t;
        typedef  lvector_t< _E, _R >  vector_t;

    public:
        virtual  ~lvector_interface_t() = 0;

        /*!
         * @brief
         *    query objects rank
         */
        size_type  rank() const;

        /*!
         * @brief
         *    query (sub)vector's full capacity
         */
        virtual  size_type  capacity() const = 0;

        /*!
         * @brief
         *    query (sub)vector's full capacity in bytes
         */
        virtual  size_type  bytes() const = 0;

// sk:off    public:
// sk:off        /*!
// sk:off         * @brief
// sk:off         *    dump vector elements to log stream
// sk:off         */
// sk:off        void  dump_to_log(
// sk:off                char const *  _name = NULL,
// sk:off                char const *  _delim = ";")
// sk:off        const;

// sk:?? #ifdef  _DEBUG
// sk:??     public:
// sk:??         int  name_id;
// sk:?? #endif

    public:
        virtual  void  resize_and_preserve(
                extent_type const *,
                value_type const & /*initial value for new memory*/) = 0;
        virtual  void  resize(
                extent_type const *) = 0;

    protected:
        /*!
         * @brief
         *    allows the base to access a derived object's data
         */
        virtual  value_type *  data() = 0;
        virtual  value_type const *  data() const = 0;

        virtual  vector_buffer_t *  vector() = 0;
        virtual  vector_buffer_t const *  vector() const = 0;

    public:
        virtual  void  copy_to_carray(
                value_type *) const;

        virtual  void  copy_from_carray(
                value_type const *);

    /* various convenience functions */
    public:
        /*! array reduction operations */
        value_type  min() const;
        value_type  max() const;
        value_type  mean() const;
        value_type  prod() const;
        value_type  sum() const;

        /*! unary operators */
        vector_t  operator-() const;
        vector_t  operator~() const;
        vector_t  operator!() const;

        /*! binary operators */
        vector_t  operator+( value_type const &) const;
        vector_t  operator+( vector_t const &) const;
        vector_t  operator-( value_type const &) const;
        vector_t  operator-( vector_t const &) const;
        vector_t  operator*( value_type const &) const;
        vector_t  operator*( vector_t const &) const;
        vector_t  operator/( value_type const &) const;
        vector_t  operator/( vector_t const &) const;

        /*! assignment operators */
        vector_t &  operator+=( value_type const &);
        vector_t &  operator+=( vector_t const &);

        vector_t &  operator-=( value_type const &);
        vector_t &  operator-=( vector_t const &);

        vector_t &  operator*=( value_type const &);
        vector_t &  operator*=( vector_t const &);

        vector_t &  operator/=( value_type const &);
        vector_t &  operator/=( vector_t const &);
};

template < typename _E, unsigned int _R >
lvector_interface_t< _E, _R >::~lvector_interface_t()
{
}

template < typename _E, unsigned int _R >
lvector_t< _E, _R >
operator+(
        _E const &  _lhs,
        lvector_t< _E, _R > const &  _rhs)
{
    return  _rhs + _lhs;
}

template < typename _E, unsigned int _R >
lvector_t< _E, _R >
operator*(
        _E const &  _lhs,
        lvector_t< _E, _R > const &  _rhs)
{
    return  _rhs * _lhs;
}


template < typename _E, unsigned int _R = 1 >
class  lvector_t  :  public  lvector_interface_t< _E, _R >
{
    public:
        /*! vector buffer object type */
        typedef  blitz::Array< _E, _R >  vector_buffer_t;
        /*! this objects type */
        typedef  lvector_t< _E, _R >  vector_t;
        /*! type of elements of this vector object */
        typedef  lvector_t< _E, _R-1 >  sub_vector_t;
        /*! type of elements of this vector object */
// sk:?? #ifdef  CRABMEAT_COMPILER_GCC
// sk:??         typedef  lvector_t< _E /*const?*/, _R-1 > const  const_sub_vector_t;
// sk:?? #else
        typedef  lvector_t< _E, _R-1 >  const_sub_vector_t;
// sk:?? #endif

        typedef  typename lvector_interface_t< _E, _R >::value_type  value_type;
        typedef  typename lvector_interface_t< _E, _R >::extent_type  extent_type;
        typedef  typename lvector_interface_t< _E, _R >::size_type  size_type;


        /*! dimension of vector */
        static size_type const  RANK = _R;

    protected:
        vector_buffer_t  v_buf_;

        public:
        /*!
         * @brief
         *    default constructor
         */
        lvector_t();
        /*!
         * @brief
         *    constructor initializing with @p initial_value
         */
        lvector_t(
                extent_type const [] /*extents*/,
                value_type const & = value_type()/*initial value*/);
        lvector_t(
                extent_type const [] /*extents*/,
                value_type * /*preexisting buffer*/);
        lvector_t(
                extent_type const [] /*extents*/,
                value_type const * /*preexisting buffer*/);

        /*!
         * @brief
         *    copy constructor, shallow copy
         */
        lvector_t(
                vector_t const &);
        lvector_t(
                vector_buffer_t const &);

        /*!
         * @brief
         *    assignment operator, shallow copy if empty vector, element-wise copy
         *    if vector has attached buffer and matching capacity.
         *
         *    none-matching capacities is not allowed, unless destination buffer
         *    is empty (i.e. NULL)
         */
        lvector_t &  operator=(
                vector_t const &);
        lvector_t &  operator=(
                value_type const &);


        void  reference(
                vector_t &);

        virtual  ~lvector_t();

        /*!
         * @brief
         *    total size of vector
         */
        size_type  capacity() const;
        /*!
         * @brief
         *    size of leading dimension
         */
        size_type  size() const;
        extent_type  extent(
                extent_type) const;
        lvector_t< typename lvector_t< _E, _R >::extent_type, 1u >  shape() const;
        /*!
         * @brief
         *    total size of vector in bytes
         */
        size_type  bytes() const;


        void  resize_and_preserve(
                extent_type const *,
                value_type const & /*initial value for new memory*/);
        void  resize(
                extent_type const *);

        inline
        const_sub_vector_t operator[](
                extent_type) const;

        inline
        sub_vector_t  operator[](
                extent_type);

    public:
#ifdef  _INTL_HAVE_USAGE_COUNTER
        size_t  usage_cntr() const { return  (a_buf_) ? a_buf_->usage_cntr_ : 0u; }
#endif

        value_type const *  data() const { return  this->v_buf_.data(); };
        vector_buffer_t const *  vector() const { return  &this->v_buf_; }

    protected:
        value_type *  data() { return  this->v_buf_.data(); }
        vector_buffer_t *  vector() { return  &this->v_buf_; }
};


template < typename _E, unsigned int _R >
inline
typename lvector_t< _E, _R >::const_sub_vector_t
lvector_t< _E, _R >::operator[](
        extent_type  _k)
const
{
    return  lvector_t< _E, _R - 1 >( this->v_buf_.shape().data()+1, &this->v_buf_(_k));
}


template < typename _E, unsigned int _R >
inline
typename lvector_t< _E, _R >::sub_vector_t
lvector_t< _E, _R >::operator[](
        extent_type  _k)
{
    return  lvector_t< _E, _R - 1 >( this->v_buf_.shape().data()+1, &this->v_buf_(_k));
}



/* illegal  */
template < typename _E >
class  lvector_t< _E, 0 >
{
};

template < typename _E >
class  lvector_t< _E, 1 >  :  public  lvector_interface_t< _E, 1 >
{
    public:
        typedef  typename blitz::Array< _E, 1 >  vector_buffer_t;
        typedef  lvector_t< _E, 1 >  vector_t;

        typedef  typename lvector_interface_t< _E, 1 >::value_type  value_type;
        typedef  typename lvector_interface_t< _E, 1 >::extent_type  extent_type;
        typedef  typename lvector_interface_t< _E, 1 >::size_type  size_type;


        /*! dimension of vector, special case RANK = 1 */
        static size_type const  RANK = 1;

    protected:
        /*!
         * @brief
         *    see above
         */
        vector_buffer_t  v_buf_;

        public:
        /*!
         * @brief
         *    default constructor
         */
        lvector_t();

        /*!
         * @brief
         *    constructor with scalar size parameter
         */
                lvector_t(
                extent_type,
                value_type const & = value_type());
        lvector_t(
                extent_type const [] /*extents*/,
                value_type * /*preexisting buffer*/);
        lvector_t(
                extent_type const [] /*extents*/,
                value_type const * /*preexisting buffer*/);

        /*!
         * @brief
         *    copy constructor, shallow copy
         */
        lvector_t(
                vector_t const &);
        lvector_t(
                vector_buffer_t const &);

        /*!
         * @brief
         *    see above
         */
        lvector_t &  operator=(
                vector_t const &);
        lvector_t &  operator=(
                value_type const &);

        void  reference(
                vector_t &);

        virtual  ~lvector_t();


        void  resize_and_preserve(
                extent_type,
                value_type const & /*initial value for new memory*/);
        void  resize_and_preserve(
                extent_type const *,
                value_type const & /*initial value for new memory*/);
        void  resize(
                extent_type);
        void  resize(
                extent_type const *);


// sk:chg        void  set_value( 
// sk:chg                value_type const &);

        /* alias for size in 1d case */
        size_type  capacity() const;
        size_type  size() const;
        extent_type  extent(
                extent_type) const;
        lvector_t< extent_type, 1 >  shape() const;

        /*!
         * @brief
         *    total size of vector in bytes
         */
        size_type  bytes() const;


        inline
        value_type &  operator[](
                extent_type);
        inline
        value_type const &  operator[](
                extent_type) const;

    public:
        void  set_range(
                value_type const &, extent_type /*first*/, extent_type /*last*/);
        using lvector_interface_t< _E, 1 >::min;
        using lvector_interface_t< _E, 1 >::max;
        using lvector_interface_t< _E, 1 >::mean;
        using lvector_interface_t< _E, 1 >::prod;
        using lvector_interface_t< _E, 1 >::sum;
        value_type  sum(
                extent_type /*limit*/) const;

        void  shift_left( value_type const &);
        void  cyclic_shift_left();
        void  shift_right( value_type const &);
        void  cyclic_shift_right();

    public:
#ifdef  _INTL_HAVE_USAGE_COUNTER
        size_t  usage_cntr() const { return  a_buf_->usage_cntr_; }
        //size_t  usage_cntr() const { return  (a_buf_) ? a_buf_->usage_cntr_ : 0u; }
#endif

        value_type const *  data() const { return  this->v_buf_.data(); }
        vector_buffer_t const *  vector() const { return  &this->v_buf_; }

    protected:
        value_type *  data() { return  this->v_buf_.data(); }
        vector_buffer_t *  vector() { return  &this->v_buf_; }
};


template < typename _E >
inline
typename lvector_t< _E, 1 >::value_type &
lvector_t< _E, 1 >::operator[](
        extent_type  _k)
{
#ifdef  _INTL_HAVE_USAGE_COUNTER
    ++(this->a_buf_->usage_cntr_);
#endif
    return  this->v_buf_(_k);
}


template < typename _E >
inline
typename lvector_t< _E, 1 >::value_type const &
lvector_t< _E, 1 >::operator[](
        extent_type  _k)
const
{
#ifdef  _INTL_HAVE_USAGE_COUNTER
    ++(this->a_buf_->usage_cntr_);
#endif
    return  this->v_buf_(_k);
}

namespace cbm
{
    template < typename _E, unsigned int _R, template < typename, unsigned int > class _V >
    inline void
    invalidate(
            _V< _E, _R > &  _vec)
    {
        _vec = ldndc::invalid_t< _E >::value;
    }
}


#include  "containers/lvector.inl"

template < typename  _A >
struct  lvector_memsize_t
{
    static size_t const RANK = _A::RANK;


    static uintptr_t const SIZE_ELEMENT = sizeof( typename _A::value_type);

    static uintptr_t const SIZE_VECTOR = sizeof( _A);

    static uintptr_t const SIZE_BUFFER = sizeof( typename _A::vector_buffer_t);

    static uintptr_t const SIZE = SIZE_VECTOR + SIZE_BUFFER;
};


/* common vector types */
typedef  lvector_t< float, 1u >  vector_flt_t;
typedef  lvector_t< double, 1u >  vector_dbl_t;
typedef  lvector_t< int, 1u >  vector_int_t;
typedef  lvector_t< unsigned int, 1u >  vector_uint_t;
typedef  lvector_t< unsigned long int, 1u >  vector_luint_t;

typedef  lvector_t< float, 2u >  matrix_2d_flt_t;
typedef  lvector_t< double, 2u >  matrix_2d_dbl_t;
typedef  lvector_t< int, 2u >  matrix_2d_int_t;
typedef  lvector_t< unsigned int, 2u >  matrix_2d_uint_t;
typedef  lvector_t< unsigned long int, 2u >  matrix_2d_luint_t;


#endif  /*  CBM_VECTOR_H__  */

