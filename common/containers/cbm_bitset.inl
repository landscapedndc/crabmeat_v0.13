/*!
 * @brief
 *    bit set implementation
 * 
 * @author 
 *      steffen klatt,
 *    edwin haas
 */

#include  "math/cbm_math.h"
#include  "memory/cbm_mem.h"

#include  "log/cbm_baselog.h"


template < typename  _B >
ldndc::bitset_integral_t< _B >::bitset_integral_t( 
                size_type  _size,
                bitset_base_t  _ini)
                : bitset_( NULL), size_( _size), bits_set_( 0)
{
        if ( _size > 0)
        {
                if ( _size > base_size)
                {
                        this->bitset_ = CBM_DefaultAllocator->allocate_type< bitset_base_t >( __BITSET_SIZE(_size,base_size));
            this->clear();
                }
                else
                {
                        this->bitset_ = &bitset_fast_buf_;
                }
                /* in many cases this is sufficient */
                this->bitset_[0] = _ini;
                this->bits_set_ = this->count_bits_( _ini);
        }
        else
        {
                crabmeat_assert( !_ini);
        }
}


template < typename  _B >
ldndc::bitset_integral_t< _B >::~bitset_integral_t< _B >()
{
        if (( this->size_ > base_size) && this->bitset_)
        {
                CBM_DefaultAllocator->deallocate( this->bitset_);
        }
}


template < typename  _B >
typename ldndc::bitset_integral_t< _B >::size_type
ldndc::bitset_integral_t< _B >::size()
const
{
        return  this->size_;
}


template < typename  _B >
typename ldndc::bitset_integral_t< _B >::size_type
ldndc::bitset_integral_t< _B >::bits_set()
const
{
        return  this->bits_set_;
}


template < typename  _B >
void
ldndc::bitset_integral_t< _B >::clear()
{
    for ( size_type  k = 0;  k < __BITSET_SIZE(size_,base_size);  ++k)
    {
        this->bitset_[k] = 0;
    }
}

template < typename  _B >
std::string
ldndc::bitset_integral_t< _B >::to_string()
const
{
        std::string  s( this->size(), '0');
        int  b( static_cast< int >( this->bits_set_));
        size_type  k( 0);
        while ( b)
        {
                if ( this->test( k))
                {
                        s[this->size()-k-1] = '1';
                        --b;
                }
                ++k;
        }
        return  s;
}


template < typename  _B >
void
ldndc::bitset_integral_t< _B >::fail_check_(
                size_type  _ms,
                size_type  _rs)
const
{
        if ( _ms < _rs)
        {
                CBM_LogFatal( "bitset: index or requested size too large [cur=",(int)_ms,",req=",(int)_rs,"]");
        }
}


template < typename  _B >
typename ldndc::bitset_integral_t< _B >::size_type
ldndc::bitset_integral_t< _B >::count_bits_(
                bitset_base_t  _v)
{
        size_type  b( 0);
        while ( _v)
        {
                b += ( _v & (bitset_base_t)1) ? 1 : 0;
                _v >>= 1u;
        }

        return  b;
}

