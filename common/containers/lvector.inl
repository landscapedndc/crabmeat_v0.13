/*!
 * @brief
 *    array wrapper implementation
 *
 * @author 
 *    steffen klatt (created on: oct 29, 2012),
 *    edwin haas
 */

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"
#include  "math/cbm_math.h"

/* more verbose error messages */
#ifdef  _DEBUG
// sk:todo #  include  "state/state_info.h"
#endif


/* ldndc vector interface type */

template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::size_type
lvector_interface_t< _E, _R >::rank()
const
{
    return  static_cast< size_type >( _R);
}


template < typename _E, unsigned int _R >
void
lvector_interface_t< _E, _R >::copy_to_carray(
        value_type *  _buf)
const
{
    ::memcpy( _buf, this->data(), this->capacity()*sizeof( value_type));
}

template < typename _E, unsigned int _R >
void
lvector_interface_t< _E, _R >::copy_from_carray(
        value_type const *  _buf)
{
    ::memcpy( this->data(), _buf, this->capacity()*sizeof( value_type));
}

/* various convenience functions */
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::value_type
lvector_interface_t< _E, _R >::min()
const
{
    return  blitz::min( *this->vector());
}

template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::value_type
lvector_interface_t< _E, _R >::max()
const
{
    return  blitz::max( *this->vector());
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::value_type
lvector_interface_t< _E, _R >::mean()
const
{
    return  blitz::mean( *this->vector());
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::value_type
lvector_interface_t< _E, _R >::prod()
const
{
    return  blitz::product( *this->vector());
}

template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::value_type
lvector_interface_t< _E, _R >::sum()
const
{
    return  blitz::sum( *this->vector());
}


/* unary */
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator-()
const
{
    return  vector_t( static_cast< vector_buffer_t >( -(*this->vector())));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator~()
const
{
    return  vector_t( static_cast< vector_buffer_t >( ~(*this->vector())));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator!()
const
{
    return  vector_t( static_cast< vector_buffer_t >( !(*this->vector())));
}

/* binary */
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator+(
        value_type const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator+( *this->vector(), _rhs)));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator+(
        vector_t const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator+( *this->vector(), *_rhs.vector())));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator-(
        value_type const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator-( *this->vector(), _rhs)));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator-(
        vector_t const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator-( *this->vector(), *_rhs.vector())));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator*(
        value_type const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator*( *this->vector(), _rhs)));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator*(
        vector_t const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator*( *this->vector(), *_rhs.vector())));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator/(
        value_type const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator/( *this->vector(), _rhs)));
}
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t
lvector_interface_t< _E, _R >::operator/(
        vector_t const &  _rhs)
const
{
    return  vector_t( static_cast< vector_buffer_t >( blitz::operator/( *this->vector(), *_rhs.vector())));
}


/* assignments */
template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator+=(
        value_type const &  _rhs)
{
    *this->vector() += _rhs;
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator+=(
        vector_t const &  _rhs)
{
    *this->vector() += *_rhs.vector();
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator-=(
        value_type const &  _rhs)
{
    *this->vector() -= _rhs;
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator-=(
        vector_t const &  _rhs)
{
    *this->vector() -= *_rhs.vector();
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator*=(
        value_type const &  _rhs)
{
    *this->vector() *= _rhs;
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator*=(
        vector_t const &  _rhs)
{
    *this->vector() *= *_rhs.vector();
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator/=(
        value_type const &  _rhs)
{
    *this->vector() /= _rhs;
    return  static_cast< vector_t & >( *this);
}


template < typename _E, unsigned int _R >
typename lvector_interface_t< _E, _R >::vector_t &
lvector_interface_t< _E, _R >::operator/=(
        vector_t const &  _rhs)
{
    *this->vector() /= *_rhs.vector();
    return  static_cast< vector_t & >( *this);
}


/* ldndc vector for rank > 1 */

template < typename _E, unsigned int _R >
lvector_t< _E, _R >::lvector_t()
        : lvector_interface_t< _E, _R >()
{
}
template < typename _E, unsigned int _R >
lvector_t< _E, _R >::lvector_t(
        extent_type const  _extents[],
        value_type const &  _initial_value)
        : lvector_interface_t< _E, _R >()
{
    blitz::TinyVector< extent_type, _R >  the_shape( _extents);
    this->v_buf_.resize( the_shape);
    this->v_buf_ = _initial_value;
}
template < typename _E, unsigned int _R >
lvector_t< _E, _R >::lvector_t(
        extent_type const  _extents[],
        value_type *  _buf)
{
    blitz::TinyVector< extent_type, _R > const  the_shape( _extents);
    this->v_buf_.resize( the_shape);
           this->v_buf_ = vector_buffer_t( _buf, the_shape, blitz::neverDeleteData);
}
template < typename _E, unsigned int _R >
lvector_t< _E, _R >::lvector_t(
        extent_type const  _extents[],
        value_type const *  _buf)
{
    blitz::TinyVector< extent_type, _R > const  the_shape( _extents);
    this->v_buf_.resize( the_shape);
    this->v_buf_ = vector_buffer_t( const_cast< value_type * >( _buf), the_shape, blitz::neverDeleteData);
}
template < typename _E, unsigned int _R >
lvector_t< _E, _R >::lvector_t(
        vector_t const &  _rhs)
        : lvector_interface_t< _E, _R >(),
          v_buf_( _rhs.v_buf_)
{
// sk:?? #ifdef  _DEBUG
// sk:??     this->name_id = _rhs.name_id;
// sk:?? #endif
}
template < typename _E, unsigned int _R >
lvector_t< _E, _R >::lvector_t(
        vector_buffer_t const &  _rhs)
        : lvector_interface_t< _E, _R >(),
          v_buf_( _rhs)
{
}


template < typename _E, unsigned int _R >
lvector_t< _E, _R > &
lvector_t< _E, _R >::operator=(
        vector_t const &  _rhs)
{
    if ( &_rhs != this)
    {
        this->v_buf_ = _rhs.v_buf_;
    }
    return  *this;
}
template < typename _E, unsigned int _R >
lvector_t< _E, _R > &
lvector_t< _E, _R >::operator=(
        value_type const &  _rhs)
{
    this->v_buf_.operator=( _rhs);
    return  *this;
}

template < typename _E, unsigned int _R >
void
lvector_t< _E, _R >::reference(
        vector_t &  _rhs)
{
    this->v_buf_.reference( _rhs.v_buf_);
}

template < typename _E, unsigned int _R >
lvector_t< _E, _R >::~lvector_t()
{
}


template < typename _E, unsigned int _R >
typename lvector_t< _E, _R >::size_type
lvector_t< _E, _R >::capacity()
const
{
    return  this->size();
}
template < typename _E, unsigned int _R >
typename lvector_t< _E, _R >::size_type
lvector_t< _E, _R >::size()
const
{
    return  this->v_buf_.size();
}
template < typename _E, unsigned int _R >
typename lvector_t< _E, _R >::extent_type
lvector_t< _E, _R >::extent(
        extent_type  _r)
const
{
    crabmeat_assert( static_cast< size_type >( _r) < _R);
    return  this->v_buf_.extent( _r);
}
template < typename _E, unsigned int _R >
lvector_t< typename lvector_t< _E, _R >::extent_type, 1u >
lvector_t< _E, _R >::shape()
const
{
    typename lvector_t< _E, 1 >::extent_type  the_extent[1] =
        { static_cast< typename lvector_t< _E, 1 >::extent_type >( _R)};
    return  lvector_t< typename lvector_t< _E, 1 >::extent_type, 1 >( the_extent, this->v_buf_.shape().data());
}
template < typename _E, unsigned int _R >
typename lvector_t< _E, _R >::size_type
lvector_t< _E, _R >::bytes()
const
{
    return  this->capacity() * sizeof( value_type);
}

template < typename _E, unsigned int _R >
void
lvector_t< _E, _R >::resize_and_preserve(
        extent_type const *  _extents,
        value_type const &  _initial_value)
{
    blitz::TinyVector< extent_type, _R > const  old_shape( this->v_buf_.shape().data());
    blitz::TinyVector< extent_type, _R > const  new_shape( _extents);
    if ( blitz::any( new_shape == 0) || blitz::all( new_shape == old_shape))
    {
// sk:dbg        CBM_LogDebug( "\tno resize needed  [",old_shape,"->",new_shape,"]");
        return;
    }

    this->v_buf_.resizeAndPreserve( new_shape);

    if ( blitz::any( old_shape == 0))
    {
        this->v_buf_ = _initial_value;
    }
    else
    {
        blitz::TinyVector< extent_type, _R >  ub;
        blitz::TinyVector< extent_type, _R >  lb;
        for ( size_t  r = 0;  r < _R;  ++r)
        {
            if ( old_shape(r) >= new_shape(r))
            {
                continue;
            }

            for ( size_t  s = 0;  s < _R;  ++s)
            {
                lb(s) = ( r == s) ? old_shape(s) : 0;
                ub(s) = this->v_buf_.ubound(s);
// sk:dbg                CBM_LogDebug( "o-shape=",old_shape, "  n-shape=",new_shape, "  rect-(",r,",",s,")=",lb(s),",",ub(s));
            }

            blitz::RectDomain< _R >  rect_domain( lb, ub);
            this->v_buf_(rect_domain) = _initial_value;
        }
    }
}
template < typename _E, unsigned int _R >
void
lvector_t< _E, _R >::resize(
        extent_type const *  _extents)
{
    blitz::TinyVector< extent_type, _R > const  old_shape( this->v_buf_.shape().data());
    blitz::TinyVector< extent_type, _R > const  new_shape( _extents);
    if ( blitz::any( new_shape == 0) || blitz::all( new_shape == old_shape))
    {
        return;
    }
    this->v_buf_.resize( new_shape);
}


template < typename _E, unsigned int _R >
std::ostream &
operator<<(
                std::ostream &  _out,
        lvector_t< _E, _R > const &  _vec)
{
    size_t const  this_size = static_cast< size_t >( _vec.extent( 0));
    _out << '[';
    for ( size_t  k = 0;  k < this_size;  ++k)
    {
        _out << _vec[k];
    }
    _out << ']' << ';';
        return  _out;
}


/* ldndc vector for rank R=1 */

template < typename _E >
lvector_t< _E, 1 >::lvector_t()
        : lvector_interface_t< _E, 1 >()
{
}
template < typename _E >
lvector_t< _E, 1 >::lvector_t(
        extent_type  _extent,
        value_type const &  _initial_value)
        : lvector_interface_t< _E, 1 >()
{
    this->v_buf_.resize( _extent);
    this->v_buf_ = _initial_value;
}
template < typename _E >
lvector_t< _E, 1 >::lvector_t(
        extent_type const  _extent[],
        value_type *  _buf)
{
    blitz::TinyVector< extent_type, 1 > const  the_shape( *_extent);
    vector_buffer_t  v_tmp( _buf, the_shape, blitz::neverDeleteData);
    this->v_buf_.reference( v_tmp);
}
template < typename _E >
lvector_t< _E, 1 >::lvector_t(
        extent_type const  _extent[],
        value_type const *  _buf)
{
    blitz::TinyVector< extent_type, 1 > const  the_shape( *_extent);
    vector_buffer_t  v_tmp( const_cast< value_type * >( _buf), the_shape, blitz::neverDeleteData);
    this->v_buf_.reference( v_tmp);
}
template < typename _E >
lvector_t< _E, 1 >::lvector_t(
        vector_t const &  _rhs)
        : lvector_interface_t< _E, 1 >(),
          v_buf_( _rhs.v_buf_)
{
// sk:?? #ifdef  _DEBUG
// sk:??     this->name_id = _rhs.name_id;
// sk:?? #endif
}
template < typename _E >
lvector_t< _E, 1 >::lvector_t(
        vector_buffer_t const &  _rhs)
        : lvector_interface_t< _E, 1 >(),
          v_buf_( _rhs)
{
}


template < typename _E >
lvector_t< _E, 1 > &
lvector_t< _E, 1 >::operator=(
        vector_t const &  _rhs)
{
    if ( &_rhs != this)
    {
        this->v_buf_.operator=( _rhs.v_buf_);
    }
    return  *this;
}
template < typename _E >
lvector_t< _E, 1 > &
lvector_t< _E, 1 >::operator=(
        value_type const &  _rhs)
{
    this->v_buf_.operator=( _rhs);
    return  *this;
}

template < typename _E >
void
lvector_t< _E, 1 >::reference(
        vector_t &  _rhs)
{
    this->v_buf_.reference( _rhs.v_buf_);
}


template < typename _E >
lvector_t< _E, 1 >::~lvector_t()
{
}


template < typename _E >
typename lvector_t< _E, 1 >::size_type
lvector_t< _E, 1 >::capacity()
    const
{
    return  size();
}
template < typename _E >
typename lvector_t< _E, 1 >::size_type
lvector_t< _E, 1 >::size()
const
{
    return  this->v_buf_.size();
}
template < typename _E >
typename lvector_t< _E, 1 >::extent_type
lvector_t< _E, 1 >::extent(
        extent_type)
const
{
    return  this->size();
}
template < typename _E >
lvector_t< typename lvector_t< _E, 1 >::extent_type, 1 >
lvector_t< _E, 1 >::shape()
const
{
    return  lvector_t< extent_type, 1 >( 1, this->size());
}
template < typename _E >
typename lvector_t< _E, 1 >::size_type
lvector_t< _E, 1 >::bytes()
const
{
    return  this->capacity() * sizeof( value_type);
}


template < typename _E >
void
lvector_t< _E, 1 >::resize_and_preserve(
        extent_type  _extent,
        value_type const &  _initial_value)
{
    if ( _extent == 0 || ( _extent == this->extent(0)))
    {
// sk:dbg        CBM_LogDebug( "\tno resize needed  [",this->extent(0),"->",_extent,"]");
        return;
    }
    extent_type const  extent_0 = this->extent(0);
    this->v_buf_.resizeAndPreserve( _extent);
    if ( _extent > extent_0)
    {
        this->set_range( _initial_value, extent_0, _extent-1);
    }
}
template < typename _E >
void
lvector_t< _E, 1 >::resize_and_preserve(
        extent_type const *  _extent,
        value_type const &  _initial_value)
{
    if ( *_extent == 0 || ( *_extent == this->extent(0)))
    {
// sk:dbg        CBM_LogDebug( "\tno resize needed  [",this->extent(0),"->",*_extent,"]");
        return;
    }
    
    extent_type const  extent_0 = this->extent(0);
    this->v_buf_.resizeAndPreserve( *_extent);
    if ( *_extent > extent_0)
    {
        this->set_range( _initial_value, extent_0, *_extent-1);
    }
}
template < typename _E >
void
lvector_t< _E, 1 >::resize(
        extent_type  _extent)
{
    if ( _extent == 0)
    {
        return;
    }
    this->v_buf_.resize( _extent);
}
template < typename _E >
void
lvector_t< _E, 1 >::resize(
        extent_type const *  _extent)
{
    if ( *_extent == 0)
    {
        return;
    }
    this->v_buf_.resize( *_extent);
}


template < typename _E >
void
lvector_t< _E, 1 >::set_range(
        value_type const &  _value,
               extent_type  _first, extent_type  _last)
{
    this->v_buf_( blitz::Range( _first, _last)) = _value;
}

template < typename _E >
typename lvector_t< _E, 1 >::value_type
lvector_t< _E, 1 >::sum(
        extent_type _limit)
const
{
    return  blitz::sum( this->v_buf_( blitz::Range( 0, _limit-1)));
}

template < typename _E >
void
lvector_t< _E, 1 >::shift_left(
        value_type const &  _new)
{
    for ( size_type  k = 1;  k < this->extent(0);  ++k)
    {
        this->v_buf_(k-1) = this->v_buf_(k);
    }
    this->v_buf_(this->extent(0)-1) = _new;
}
template < typename _E >
void
lvector_t< _E, 1 >::cyclic_shift_left()
{
    this->shift_left( this->v_buf_(0));
}

template < typename _E >
void
lvector_t< _E, 1 >::shift_right(
        value_type const &  _new)
{
    size_type const  v_size = this->extent(0) - 1;
    for ( size_type  k = v_size;  k > 0;  --k)
    {
        this->v_buf_(k) = this->v_buf_(k-1);
    }
    this->v_buf_(0) = _new;
}
template < typename _E >
void
lvector_t< _E, 1 >::cyclic_shift_right()
{
    this->shift_right( this->v_buf_(this->extent(0)-1));
}


template < typename _E >
std::ostream &
operator<<(
                std::ostream &  _out,
        lvector_t< _E > const &  _vec)
{
    _out << '[';
    if ( _vec.size() > 0)
    {
        char const  delim = ',';
        _E const *  this_data = _vec.data();
        size_t const  this_capacity = static_cast< size_t >( _vec.capacity());

        _out << *this_data;
        for ( size_t  k = 1;  k < this_capacity;  ++k)
        {
            _out << delim << *(++this_data);
        }
    }
    _out << ']' << ';';


        return  _out;
}

