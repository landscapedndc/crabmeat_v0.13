/*!
 * @brief 
 *    stack implementation.
 *
 * @author 
 *      steffen klatt (created on: dec 17, 2013),
 *    edwin haas
 */

#include  "log/cbm_baselog.h"
#include  "math/lmath-limits.h"

template < typename _E >
size_t
ldndc::stack< _E >::size()
const
{
        return  static_cast< size_t >( std::numeric_limits< typename std::stack< _E >::size_type >::max());
}


template < typename _E >
size_t
ldndc::stack< _E >::fillsize()
const
{

    return  this->stack_.size();
}


template < typename _E >
bool
ldndc::stack< _E >::is_empty()
const
{
        return  this->stack_.empty();
}


template < typename _E >
bool
ldndc::stack< _E >::is_full()
const
{
    return  this->fillsize() < this->size();
}

template < typename _E >
void
ldndc::stack< _E >::clear()
{
    while ( !this->stack_.empty())
    {
        this->stack_.pop();
    }
}


template < typename _E >
typename ldndc::stack< _E >::element_type const &
ldndc::stack< _E >::push(
                element_type const &  _e)
{
    this->stack_.push( _e);
    return  _e;
}

template < typename _E >
typename ldndc::stack< _E >::element_type const &
ldndc::stack< _E >::peek()
{
        if ( this->is_empty())
        {
        CBM_LogFatal( "stack: attempt to peek at empty stack");
        }
        return  this->stack_.top();
}
template < typename _E >
typename ldndc::stack< _E >::element_type
ldndc::stack< _E >::pop()
{
        if ( this->is_empty())
        {
        CBM_LogFatal( "stack: attempt to pop from empty stack");
        }
    element_type  e = this->peek();
        this->stack_.pop();
    return  e;
}

