/*!
 * @brief 
 *    implements silently growing array (yes,
 *    sort of like a vector but without resizing
 *    it explicitly)
 *
 * @author 
 *      steffen klatt
 */
#ifndef  LDNDC_GROWABLE_ARRAY_H_
#define  LDNDC_GROWABLE_ARRAY_H_

#include  "crabmeat-common.h"
#include  <vector>

#define  __GROWABLE_ARRAY_SIZE(__size__,__inc__)    \
    (__inc__) * (( __size__) / (__inc__)) + ((( __size__) % (__inc__)) ? 1 : 0) * (__inc__)

namespace  ldndc
{
/*!
 * @brief
 *    very tolerant array, it grows automatically linearly up to 
 *    maximum size when element with index larger than current
 *    size is    requested.
 *    
 *    if not otherwise specified, the array will grow in increments
 *    of 1.
 */
template < class  _E /* element type */, unsigned int  _C /* max. size */, unsigned short int  _I = 1 /* increment */>
class  growable_array
{
    typedef  std::vector< _E >  container_type;
    typedef  char (*fail_type)[_C-1];
    public:
        enum
              {
            MAX_SIZE = _C
        };
        typedef  _E  element_type;

    public:
        growable_array(
                unsigned int = 0,
                _E const & = _E());

        size_t  size() const;

        _E const &  operator[]( size_t) const;

        _E &  operator[]( size_t);

        lerr_t  resize( size_t);

    private:
        container_type  buf_;

        _E  dflt_val_;

        _E &  access_op_( size_t);
};


template < class  _E, unsigned int  _C >
struct  growable_array< _E, _C, 0 >
{
    int  fail[_C - (_C + 1)];
};


}  /*  namespace ldndc  */

#include  "containers/lgrowarray.inl"


#endif  /*  !LDNDC_GROWABLE_ARRAY_H_  */

