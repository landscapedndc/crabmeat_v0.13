/*!
 * @brief
 *    iterable hash map
 *
 *    1) hash map with linked elements
 *    2) hash map with look-up-table
 *
 * @author
 *    steffen klatt  (created on: jan 10, 2017)
 */

#ifndef  CBM_SET_H_
#define  CBM_SET_H_

#include  "crabmeat-common.h"
#include  "memory/cbm_mempool.h"

#if defined(__cplusplus)
extern "C" {
#endif

//#define  CBM_SET_RNDACCESS  0x00000001

typedef struct CBM_SetNodeIterator
{
    void *  value;
    void *  next;
} CBM_SetNodeIterator;
#define  CBM_SetNodeIteratorIsEnd(node) ((node).value==NULL)
CBM_API extern CBM_SetNodeIterator const  CBM_SetNodeIteratorEnd;

typedef struct  CBM_Set
{
    void *  mem;
} CBM_Set;

CBM_API CBM_Set  CBM_SetCreate( CBM_MemPool, size_t /*node value size*/,
        int /*expected #nodes*/, int /*flags*/);
CBM_API void  CBM_SetDestroy( CBM_Set);

CBM_API int  CBM_SetSize( CBM_Set);
CBM_API int  CBM_SetIsEmpty( CBM_Set);

CBM_API void *  CBM_SetInsert( CBM_Set, char const * /*key*/, void * /*data*/);
CBM_API void *  CBM_SetRemove( CBM_Set, char const * /*key*/);
CBM_API void *  CBM_SetGetValue( CBM_Set, char const * /*key*/);
CBM_API void  CBM_SetClear( CBM_Set);

CBM_API CBM_SetNodeIterator  CBM_SetFind( CBM_Set, char const * /*key*/);
CBM_API CBM_SetNodeIterator  CBM_SetFindHead( CBM_Set);
CBM_API CBM_SetNodeIterator  CBM_SetFindNext( CBM_SetNodeIterator);

#if defined(__cplusplus)
}
#endif

#endif  /* !CBM_SET_H_ */

