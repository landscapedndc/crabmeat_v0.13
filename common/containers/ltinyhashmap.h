/*!
 * @brief
 *    object allowing to quickly store, retrieve
 *    and remove values of simple type and limited
 *    size by using a key, much like a map:
 *
 *    "simple" means no copy construction is performed
 *
 * @author
 *    steffen klatt  (created on: mar 15, 2014),
 *    edwin haas
 */

#ifndef CBM_TINYHASHMAP_H_
#define CBM_TINYHASHMAP_H_

#include  "crabmeat-common.h"
#include  <list>
#include  <vector>

#include  <cstdarg>

namespace cbm {

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE = 63 >
struct  tinyhashmap_t
{
    typedef  _V  value_type;
    enum
    {
        TABLE_SIZE = _TABLE_SIZE,

        KEY_MAXSIZE = _KEY_MAXSIZE,
        VALUE_MAXSIZE = _VALUE_MAXSIZE
    };
    struct  key_type
    {
        char  name[KEY_MAXSIZE+1];
    };
    struct  key_list_t
    {
        std::vector< char const * >  names;
        char const *  operator[]( size_t  _i) const
        {
            if ( _i < names.size())
            {
                return  this->names[_i];
            }
            return  NULL;
        }
        int  count() const { return  this->names.size(); }
    };
    static int  make_item_key(
            key_type *,
            char const * /*format*/, ...);
    static int  vmake_item_key(
            key_type *,
            char const * /*format*/, va_list /*values*/);
    private:
    static int  make_item_key(
            char[KEY_MAXSIZE+1] /*buffer(key) with size _KEY_MAXSIZE+1*/,
            char const * /*format*/, ...);
    static int  vmake_item_key(
            char[KEY_MAXSIZE+1] /*buffer(key) with size _KEY_MAXSIZE+1*/,
            char const * /*format*/, va_list /*values*/);

    public:
    /*!
     * make container empty: this effectively removes
     * all items from the container leaving it with
     * a size of zero.
     */
    void  remove_all();
    /*!
     * check if named variable exists in container
     */
    bool  variable_exists(
            char const * /*name*/) const;
    /*!
     * check if named variable exist in container
     * starting with the given prefix
     */
    bool  variable_prefix_exists(
            char const * /*name prefix*/) const;
    /*!
     * get number of items stored in this container
     */
    int  get_number_of_entries() const;
    /*!
     * get value of named variable:
     * if the variable does not exist in the container
     * the value of @p buffer is left unchanged.
     *
     * if @p buffer is NULL it just checks variable's
     * existence.
     *
     * note, that we do not check for empty names.
     */
    value_type const *  get(
            char const * /*name*/) const;
    value_type const *  get(
            char const * /*name*/,
            value_type * /*buffer*/, size_t /*size of buffer*/) const;

    int  get_number_of_entries_matching(
            char const * /*reg ex*/) const;

    int  get_matching(
            char const * /*reg ex*/,
            char const ** /*buffer*/) const;

    int  get_keys_matching(
            key_list_t * /*matching key list*/,
            char const * /*reg ex*/) const;

    /*!
     * set value of named variable. if a variable with
     * the same name already exists it is overwritten.
     *
     * note, that we do not check for empty names.
     */
    int  set(
            char const * /*name*/, value_type const & /*value*/);
    int  set(
            key_type const * /*name*/, value_type const & /*value*/);
    int  set(
            char const * /*name*/,
            value_type const * /*value*/, size_t /*number of values*/);
    int  set(
            key_type const * /*name*/,
            value_type const * /*value*/, size_t /*number of values*/);

    /*!
     * like @fn get but also removes the variable if
     * it exists.
     *
     * if @p buffer is NULL it just removes the variable
     * from the container if it exists.
     */
    int  get_and_remove(
            char const * /*name*/,
            value_type * /*buffer*/, size_t = 1 /*size of buffer*/);
    /*!
     * remove the variable if it exists
     */
    void  remove(
            char const * /*name*/);

    private:
        struct  hashmap_entry_t
        {
            /*key*/
            char  key[KEY_MAXSIZE+1];
            /*value*/
            value_type  data[VALUE_MAXSIZE+1];
        };
        typedef  std::list< hashmap_entry_t >  table_item_t;
        table_item_t  buf[TABLE_SIZE];


        size_t  find_table_position(
                char const * /*name*/) const;
        unsigned long int  make_hash(
                char const * /*name*/) const;

        int  set_data(
                hashmap_entry_t * /*table item*/,
                char const * /*key*/,
                value_type const * /*value*/, size_t /*elements*/);
        int  copy_data(
                _V * /*destination*/, _V const * /*source*/, size_t /*elements*/) const;
};

} /* namespace cbm */

#include  "containers/ltinyhashmap.inl"

#endif /* !CBM_TINYHASHMAP_H_ */

