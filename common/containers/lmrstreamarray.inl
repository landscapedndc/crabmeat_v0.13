/*!
 * @brief 
 *  implements stream array (sliding window in
 *  streamed input data)
 *
 * @author 
 *  steffen klatt,
 *  edwin haas
 */

#include  "log/cbm_baselog.h"

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::multiresolution_stream_array_t()
                : load_size_( 0),
                  fill_size_( 0),
                  buf_displ_( 0),
                  reload_notifier_( NULL)
{
    this->reset_status_flags_();
    this->allocate_buffers_( NULL, 0, NULL, 0, 1, ldndc::invalid_t< element_type >::value);
}

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::multiresolution_stream_array_t(
                _CB *  _reload_notifier,
        int const *  _res_lo, int  _cnt_lo,
        int const *  _res_hi, int  _cnt_hi,
        int  _reference_resolution,
                element_type const &  _init_val)
                : load_size_( 0),
                  fill_size_( 0),
                  buf_displ_( 0),
                  reload_notifier_( _reload_notifier)
{
    this->reset_status_flags_();
    if ((sizeof(element_type)*( RECORD_SIZE_REAL - RECORD_SIZE)) < sizeof(typename linkarray_operations_t< _E >::buffer_record_link_t))
    {
        CBM_LogFatal( "[PROBLEM]  lacking space for storing links",
              "  [needed=",sizeof(typename linkarray_operations_t< _E >::buffer_record_link_t),",available=",(RECORD_SIZE_REAL - RECORD_SIZE),"]");
    }

    lerr_t  rc_allocate_buffers = this->allocate_buffers_( _res_lo, _cnt_lo, _res_hi, _cnt_hi, _reference_resolution, _init_val);
    if ( rc_allocate_buffers)
    {
        CBM_LogError( "allocating stream data buffers failed");
        this->bad_state();
    }
    else
    {
        lerr_t  rc_format_buffers = this->format_buffers_();
        if ( rc_format_buffers)
        {
            CBM_LogError( "formatting stream data buffers failed");
            this->bad_state();
        }
    }

    if ( !_reload_notifier)
    {
        CBM_LogFatal( "[BUG]  need to supply reload callback");
    }
}

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::reset_status_flags_()
{
    this->m.bad_state = 0;

    this->m.replicate = 0;
    this->m.is_replicated = 0;

    this->m.has_lo = 0;
    this->m.has_hi = 0;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::~multiresolution_stream_array_t< _E, _CB, _R, _N >()
{
    linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
    element_type *  buf_p = NULL;

    {
    element_type *  buf = n_BUFDATA(this->buf_);
    buf_ops.find_lower( &buf, buf);
    while ( buf)
    {
        buf_ops.find_lower( &buf_p, buf);
        buf_ops.deallocate( buf);

        buf = buf_p;
    }
    }
    {
    element_type *  buf = n_BUFDATA(this->buf_);
    buf_ops.find_higher( &buf, buf);
    while ( buf)
    {
        buf_ops.find_higher( &buf_p, buf);
        buf_ops.deallocate( buf);

        buf = buf_p;
    }
    }
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::clear(
        element_type const &  _value)
{
    this->load_size_ = 0;
    this->fill_size_ = 0;
    this->buf_displ_ = 0;

    this->mem_set_all( _value);

    return  LDNDC_ERR_OK;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::allocate_buffers_(
        int const *  _res_lo, int  _cnt_lo,
        int const *  _res_hi, int  _cnt_hi,
        int  _reference_resolution,
        element_type  _init_val)
{
    crabmeat_assert(( _cnt_lo >= 0) && ( _cnt_hi >= 0));
    {
        size_t  blck_size = (size_t)(_reference_resolution * (( _cnt_lo > 0) ? _res_lo[_cnt_lo-1] : 1));
        if ( this->capacity() < blck_size)
        {
            CBM_LogError( "internal buffer to small, requested (low) resolution cannot be handeled",
                  "  [minimum capacity=",blck_size,"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        CBM_LogDebug( "capacity=",this->capacity(),"  block-size=",blck_size);
    }

    linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);

    buffer_header_t  b_hdr;
    element_type *  buf_d = n_BUFDATA(this->buf_);

    /* header data for internal "fast/native" buffer */
    b_hdr.hx.res = _reference_resolution;
    b_hdr.capacity = _reference_resolution*( this->capacity()/_reference_resolution);
    b_hdr.lnk.buf_lo = NULL;
    b_hdr.lnk.buf_hi = NULL;

    buf_ops.mem_set( buf_d, b_hdr.capacity, _init_val);
    lerr_t  rc_write_header = buf_ops.write_buffer_header( buf_d, &b_hdr);
    if ( rc_write_header)
    {
        return  rc_write_header;
    }

    /* allocate, initialize and link "low" resolution buffers */
    for ( size_t  k = 0;  k < (size_t)_cnt_lo;  ++k)
    {
        size_t const  lo_capacity =
                ( b_hdr.capacity * _res_lo[k]) / _reference_resolution;
        if ( lo_capacity == 0)
        {
            CBM_LogError( "internal buffer to small, requested (low) resolution cannot be handeled",
                  "  [resolution=",_res_lo[k],"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        element_type *  buf_l = buf_ops.allocate( lo_capacity);
        if ( !buf_l)
        {
            CBM_LogError( "failed to get memory");
            return  LDNDC_ERR_NOMEM;
        }
        buf_ops.mem_set( buf_l, lo_capacity, _init_val);

        buffer_header_t  buf_lo;
        buf_ops.read_buffer_header( &buf_lo, buf_l);
        buf_lo.hx.res = _res_lo[k];
        buf_ops.write_buffer_header( buf_l, &buf_lo);
        buf_ops.link( buf_l, buf_d);

#ifdef  _DEBUG
        buffer_header_t  hl, hh;
        buf_ops.read_buffer_header( &hl, buf_l);
        buf_ops.read_buffer_header( &hh, buf_d);
        CBM_LogDebug( "created buffer  b^L[",buf_lo.hx.res,",",lo_capacity,"]",
                "  buf-l=",hl.lnk.buf_lo,"<-",buf_l,"->",hl.lnk.buf_hi,
                "  buf-h=",hh.lnk.buf_lo,"<-",buf_d,"->",hh.lnk.buf_hi);
#endif
        buf_d = buf_l;
    }

    buf_d = n_BUFDATA(this->buf_);

    /* allocate, initialize and link "high" resolution buffers */
    for ( size_t  k = 0;  k < (size_t)_cnt_hi;  ++k)
    {
        size_t const  hi_capacity =
                ( b_hdr.capacity * _res_hi[k]) / _reference_resolution;
        element_type *  buf_h = buf_ops.allocate( hi_capacity);
        if ( !buf_h)
        {
            CBM_LogError( "nomem");
            return  LDNDC_ERR_NOMEM;
        }
        buf_ops.mem_set( buf_h, hi_capacity, _init_val);

        buffer_header_t  buf_hi;
        buf_ops.read_buffer_header( &buf_hi, buf_h);
        buf_hi.hx.res = _res_hi[k];
        buf_ops.write_buffer_header( buf_h, &buf_hi);
        buf_ops.link( buf_d, buf_h);

#ifdef  _DEBUG
        buffer_header_t  hl, hh;
        buf_ops.read_buffer_header( &hl, buf_d);
        buf_ops.read_buffer_header( &hh, buf_h);
        CBM_LogDebug( "created buffer  b^H[",buf_hi.hx.res,",",hi_capacity,"]",
              "  buf-l=",hl.lnk.buf_lo,"<-",buf_d,"->",hl.lnk.buf_hi,
              "  buf-h=",hh.lnk.buf_lo,"<-",buf_h,"->",hh.lnk.buf_hi);
#endif
        buf_d = buf_h;
    }

    this->m.has_lo = ( _cnt_lo) ? 1 : 0;
    this->m.has_hi = ( _cnt_hi) ? 1 : 0;

    return  LDNDC_ERR_OK;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::format_buffers_()
{
    {
        /* link "lower" */

        linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
        element_type *  buf = n_BUFDATA(this->buf_);
        element_type *  buf_l = NULL;
        while ( buf_ops.find_lower( &buf_l, buf) == LDNDC_ERR_OK)
        {
            int const  c = static_cast< int >( buf_ops.capacity( buf));
            int const  c_l = static_cast< int >( buf_ops.capacity( buf_l));

            buf_ops.link_records( buf_l, buf, c_l, c/c_l);
            buf = buf_l;
        }
    }

    {
        /* link "higher" */

        linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
        element_type *  buf = n_BUFDATA(this->buf_);
        element_type *  buf_h = NULL;
        while ( buf_ops.find_higher( &buf_h, buf) == LDNDC_ERR_OK)
        {
            int const  c = static_cast< int >( buf_ops.capacity( buf));
            int const  c_h = static_cast< int >( buf_ops.capacity( buf_h));

            buf_ops.link_records( buf, buf_h, c, c_h/c);
            buf = buf_h;
        }
    }

    return  LDNDC_ERR_OK;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::mem_set(
        element_type *  _buf,
        size_t  _n,
        element_type const &  _value)
{
    if ( !_buf)
    {
        return;
    }

    linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL,
        linkarray_operations_t< _E >::LINKARRAY_LINK_RECORDS|linkarray_operations_t< _E >::LINKARRAY_SIZE_FINAL);
    buf_ops.mem_set( _buf, _n, _value);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::mem_set_all(
        element_type const &  _value)
{
    this->mem_set( n_BUFDATA(this->buf_), this->capacity(), _value);
    {
        linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
        element_type *  buf = n_BUFDATA(this->buf_);
        while ( buf_ops.find_lower( &buf, buf) == LDNDC_ERR_OK)
        {
            this->mem_set( buf, buf_ops.capacity( buf), _value);
        }
    }

    {
        linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
        element_type *  buf = n_BUFDATA(this->buf_);
        while ( buf_ops.find_higher( &buf, buf) == LDNDC_ERR_OK)
        {
            this->mem_set( buf, buf_ops.capacity( buf), _value);
        }
    }
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::current_fill_cnt()
const
{
    return  this->fill_size_;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::capacity()
const
{
    return  CAPACITY;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::record_size()
const
{
    return  RECORD_SIZE;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::find_by_resolution(
        element_type **  _buf,
        int  _res, int  _dir)
{
    linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
    /* catch jumping between two resolutions: R0 < R < R1 */
    int  dir = 0;
    element_type *  buf = n_BUFDATA(this->buf_);
    element_type *  buf_0 = NULL;
    buffer_header_t  b_hdr;
    while ( buf)
    {
        buf_0 = buf;
        buf_ops.read_buffer_header( &b_hdr, buf);
        if ( b_hdr.hx.res == _res)
        {
            dir = 0;
            break;
        }
        else if ( b_hdr.hx.res > _res)
        {
            /* keep going down */
            if ( dir == 1)
            {
                break;
            }
            dir = -1;
            buf = b_hdr.lnk.buf_lo;
        }
        else if ( b_hdr.hx.res < _res)
        {
            /* keep going up */
            if ( dir == -1)
            {
                break;
            }
            dir = 1;
            buf = b_hdr.lnk.buf_hi;
        }
    }

    if ( _buf)
    {
        if ( dir == 0)
        {
            *_buf = buf_0;
        }
        else
        {
            *_buf = ( dir == 1) ? (( _dir == 1) ? buf : buf_0) : ( /*dir == -1*/( _dir == 1) ? buf_0 : buf);
        }
    }
    return  LDNDC_ERR_OK;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::element_type *
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::operator[](
                typename _CB::argument_type const *  _cb_arg)
{
    return  this->access_( _cb_arg);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::element_type const *
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::operator[](
                typename _CB::argument_type const *  _cb_arg)
const
{
    return  const_cast< ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N > * >( this)->access_( _cb_arg);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::try_trigger_refresh(
                typename _CB::argument_type const *  _cb_arg)
{
    if ( this->access_( _cb_arg))
	{
		return  LDNDC_ERR_OK;
	}
	return  LDNDC_ERR_INPUT_EXHAUSTED;
}


#define  p_l_BUFHDR(__buf__) p_BUFHDR(((buffer_header_t *)(__buf__))->lnk.buf_lo)
#define  p_h_BUFHDR(__buf__) p_BUFHDR(((buffer_header_t *)(__buf__))->lnk.buf_hi)
 
#define  p_l_RECORD(__rec__) (((buffer_record_link_t *)((__rec__)+RECORD_SIZE_REAL-this->BUFFER_RECORD_LINK_SIZE))->buf_lo)
#define  p_h_RECORD(__rec__) (((buffer_record_link_t *)((__rec__)+RECORD_SIZE_REAL-this->BUFFER_RECORD_LINK_SIZE))->buf_hi)

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::reload_constraints(
        typename _CB::argument_type *  _cb_arg,
        int  _resolution, int  _dir)
{
    if ( !_cb_arg)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    lerr_t  rc_find_buffer = this->find_by_resolution( &_cb_arg->buf, _resolution, _dir);
    if ( rc_find_buffer)
    {
        return  rc_find_buffer;
    }
    if ( _cb_arg->buf)
    {
        linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
        buffer_header_t  b_hdr;
        buf_ops.read_buffer_header( &b_hdr, _cb_arg->buf);

        /* calculate maximum number of records to reload */
		if (( _cb_arg->n == 0) ||
                ( _cb_arg->n == ldndc::invalid_t< typename _CB::argument_type::size_type >::value))
        {
            _cb_arg->n = b_hdr.capacity;
        }
        else
        {
            _cb_arg->n = std::min( b_hdr.capacity, _cb_arg->n);
        }

        /* "usual" stride */
        _cb_arg->s = RECORD_SIZE_REAL;

        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
}

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::element_type *
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::access_(
        typename _CB::argument_type const *  _cb_arg)                
{
    crabmeat_assert( _cb_arg);
    if ( this->fill_size_ && ( _cb_arg->t < this->load_size_))
    {
        cast_header_t  hb;
        hb.buf = this->buf_;
        int const  R = hb.hdr->hx.res;
        int const  r = _cb_arg->r;
        if ( !_cb_arg->node && ( r == R))
        {
            return  n_BUFDATA(this->buf_) + ((( _cb_arg->t + this->buf_displ_) << RECORD_SIZE_LOG) & FULL_MASK);
        }
        else
        {
            typename _CB::argument_type  cb_arg( *_cb_arg);
            if ( !_cb_arg->node)
            {
                cb_arg.node = this->buf_;
                cb_arg.buf = n_BUFDATA(this->buf_) + ((( _cb_arg->t + this->buf_displ_) << RECORD_SIZE_LOG) & FULL_MASK);
            }

            cast_header_t  hn;
            hn.buf = (element_type *)cb_arg.node;
            int const  Rx = hn.hdr->hx.res;
            if ( cb_arg.r < Rx)
            {
                /* trying again with "low" node */
                cb_arg.node = p_l_BUFHDR(cb_arg.node);

                if ( !cb_arg.node)
                {
                    // TODO  throw exception?!
                    return  (element_type *)NULL;
// sk:off                    CBM_LogFatal( "attempting to access non-existing buffer  [resolution=",cb_arg.r,"]");
                }
                cb_arg.buf = p_l_RECORD(cb_arg.buf);
                return  this->access_( &cb_arg);
            }
            else if ( cb_arg.r == Rx)
            {
                /* found record */
                return  cb_arg.buf;
            }
            else /* if ( cb_arg.r > Rx) */
            {
                CBM_LogFatal( "currently not supported: requires additional offset!");
                /* trying again with "high" node */
                cb_arg.node = p_h_BUFHDR(cb_arg.node);
                if ( !cb_arg.node)
                {
                    // TODO  throw exception?!
                    return  (element_type *)NULL;
// sk:off                    CBM_LogFatal( "attempting to access non-existing buffer  [resolution=",cb_arg.r,"]");
                }
                cb_arg.buf = p_h_RECORD(cb_arg.buf);
                return  this->access_( &cb_arg);
            }
        }
    }
    else
    {
        /* update buffer displacement */
        this->buf_displ_ = ( this->buf_displ_ + (this->capacity() - this->fill_size_)) & MASK;

        /* we try to get the requested position (might imply multiple reloads) */
        typename _CB::argument_type  cb_arg( *_cb_arg);

        /* trigger reload */
// sk:dbg       CBM_LogDebug( "fill-size=",this->fill_size_,"  requested=",_cb_arg->n,"  requested'=",cb_arg.n,"  stride=",cb_arg.s, "  displacement=",this->buf_displ_);
        int  reload_result = this->reload_notifier_->callback( &cb_arg);
// sk:dbg       CBM_LogDebug( "fill-size=",this->fill_size_,"  requested=",_cb_arg->n,"  requested'=",cb_arg.n,"  received=",reload_result,"  stride=",cb_arg.s, "  displacement=",this->buf_displ_);

        if ( reload_result > 0)
        {
            if ( !this->m.is_replicated && ( reload_result > static_cast< int >( cb_arg.n)))
            {
                CBM_LogWarn( "got more data than requested  ",
                        "[requested=",cb_arg.n,",received=",reload_result,",loaded=",this->load_size_,"]");
            }
            if ( cb_arg.buf)
            {
                linkarray_operations_t< _E >  buf_ops( RECORD_SIZE_REAL);
                buffer_header_t  c_hdr;
                buf_ops.read_buffer_header( &c_hdr, n_BUFDATA(this->buf_));
                int const  c0 = c_hdr.hx.res;
                buf_ops.read_buffer_header( &c_hdr, cb_arg.buf);
                int const  c1 = c_hdr.hx.res;

// sk:dbg                CBM_LogDebug( "c0=",c0," c1=",c1,"  reload_result=",reload_result, "  reload_result'=",(reload_result*c0)/c1);
                reload_result = ( reload_result*c0)/c1;
            }

            /* all fine */
            size_t const  reload_size = static_cast< size_t >( reload_result);
            this->fill_size_  = reload_size;
            this->load_size_ += reload_size;

            /* if buffer content replication was requested.. */
            if ( this->m.replicate)
            {
                size_t  this_fill_size =
                    const_cast< ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N > * >( this)->do_replicate_();
                if ( this_fill_size == static_cast< size_t >( -1))
                {
                    return  (element_type *)NULL;
                }
            }
        }
        else
        {
            /* error occured */
            CBM_LogError( "reading past end of data");

            cb_arg.t = static_cast< size_t >( -1);
        }
        return  ( cb_arg.t == static_cast< size_t >( -1)) ? (element_type *)NULL : this->access_( &cb_arg);
    }
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::request_replicate()
{
    this->m.replicate = 1;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::multiresolution_stream_array_t< _E, _CB, _R, _N >::do_replicate_()
{
    /* only the "native" buffer needs to be replicated. since pointers to
     * higher/lower resolution buffers are replicated, we automagically
     * reuse existing records
     */
    if ( this->m.replicate)
    {
        crabmeat_assert( this->fill_size_ > 0);

        this->m.replicate = 0;
        this->m.is_replicated = 1;

        /* also replicate pointers into other buffers */
        size_t  q = this->fill_size_ * RECORD_SIZE_REAL * sizeof( element_type);
        size_t  c = ( FULL_CAPACITY * sizeof( element_type) / q) - 1;

        this->load_size_ += c*this->fill_size_;
        this->fill_size_ = (c+1)*this->fill_size_;

        char *  buf = (char *)n_BUFDATA(this->buf_);
        char *  p_buf = buf;
        while ( c--)
        {
            buf += q;
            memcpy( buf, p_buf, q);
        }
        return  this->fill_size_;
    }
    return  static_cast< size_t >( -1);
}

