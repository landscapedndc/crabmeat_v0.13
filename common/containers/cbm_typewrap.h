/*!
 * @brief 
 *    implements type wrapper object
 *
 * @author 
 *      steffen klatt
 */
#ifndef  LDNDC_TYPE_WRAPPER_H_
#define  LDNDC_TYPE_WRAPPER_H_

#include  "crabmeat-common.h"

#include  "log/cbm_baselog.h"
#include  "string/lstring-convert.h"
#include  "utils/lutils.h"


#ifdef  _DEBUG
#  define  LTYPEWRAP_SET_TYPE(__type_char__)        type_ = __type_char__;
#else
#  define  LTYPEWRAP_SET_TYPE(__type_char__)
#endif

namespace  ldndc
{
    template < typename  _T >
    struct  type_identifier
    {
            typedef  _T  type;
    };

/*!
 * @brief
 *    oh boy...
 *
 * @note
 *    it is garantueed that an object of this class can be bitwise copied.
 *
 * @note
 *    the last template parameter (@p _id) is used to distinguish
 *    ltypewrap_t types that are identical with respect to the
 *    first three template parameters
 */
template < typename  _float_type, typename  _int_type = int, typename  _uint_type = unsigned int, int  _id = 0 >
class  ltypewrap_t
{
    private:
        enum
              {
            ID = _id
        };
    public:
        typedef  _float_type  float_type;
        typedef  _int_type  int_type;
        typedef  _uint_type  uint_type;
        typedef  bool  bool_type;

    private:
        typedef  union
              {
            bool_type  b;    /* 'b' */
            //char  c;    /* 'c' */
            int_type  i;    /* 'i' */
            uint_type  u;    /* 'u' */
            float_type  f;    /* 'f' */
        }  mixed_type;

#ifdef  _DEBUG
    private:
        /* data type specifier, some kind of guard (if you find a way to use it such 
         * that get_value<_type_>() works without giving _type_ let me know)
         */
        char  type_;

        /* return type specifier or 'U' for not valid */
        template < typename _T >
        inline static char  type_id_() { return  'U'; }

        inline static char type_id_( type_identifier< bool >) { return  'b'; }
        inline static char type_id_( type_identifier< int_type >) { return  'i'; }
        inline static char type_id_( type_identifier< uint_type >) { return  'u'; }
        inline static char type_id_( type_identifier< float_type >) { return  'f'; }

        template < typename _T >
        static
        _T &  fail_(
                char _t_from, char  _t_to, char const *  _errmsg)
        {
            static _T  foo;
            CBM_LogFatal( _errmsg, "  [",_t_from,"-->",_t_to,"]");

            /* compiler ... you know. */
            return  (_T&)(foo);
        }
#endif

    public:
        ltypewrap_t()
#ifdef  _DEBUG
            : type_( 'U')
#endif
        {
            this->data_.u = ldndc::invalid_t< uint_type >::value;
        }

                template < typename _T >
        ltypewrap_t(
                                _T  _val)
#ifdef  _DEBUG
            : type_( this->type_id_( type_identifier< _T >()))
#endif
        {
                        this->operator=( _val);
        }

        template < typename _T >
        inline
        ltypewrap_t &  operator=(
                _T  _val)
        {
#ifdef  _DEBUG
            if (( this->type_ != 'U') && ( this->type_ != this->type_id_( type_identifier< _T >())))
            {
                this->fail_<_T>( this->type_, this->type_id_( type_identifier< _T >()), "ltypewrap_t: invalid change of data type");
                                return  *this;
                        }
#endif
                        this->assign_( _val);

            return  *this;
        }

                /* cast operator */
                template < typename _T >
                inline
                operator  _T()
                const
                {
                        return  this->get_value< _T >();
                }


        template < typename _T >
        inline
        _T const &  get_value()
        const
        {
#ifdef  _DEBUG
            if (( this->type_ == 'U') || ( this->type_ != this->type_id_( type_identifier< _T >())))
            {
                return  this->fail_<_T>( this->type_, this->type_id_( type_identifier< _T >()), "ltypewrap_t: type mismatch in value request");
            }
#endif
            return  this->get_value_( type_identifier< _T >());
               }

        lerr_t  get(
                   void *  _buf,  size_t *  _bytes, char  _type)
        const
        {
            switch ( _type)
            {
                case 'd':
                case 'f':
                    *(float_type*)(_buf) = this->get_value_( type_identifier< float_type >());
                    if ( _bytes) { *_bytes = sizeof( float_type); }
                    break;
                case 'b':
                    *(bool_type*)(_buf) = this->get_value_( type_identifier< bool_type >());
                    if ( _bytes) { *_bytes = sizeof( bool_type); }
                    break;
                case 'i':
                    *(int_type*)(_buf) = this->get_value_( type_identifier< int_type >());
                    if ( _bytes) { *_bytes = sizeof( int_type); }
                    break;
                case 'u':
                    *(uint_type*)(_buf) = this->get_value_( type_identifier< uint_type >());
                    if ( _bytes) { *_bytes = sizeof( uint_type); }
                    break;

                default:
                    CBM_LogFatal( "unknown type identifier  [type-id='",_type,"']");
                    return  LDNDC_ERR_FAIL;
            }
            return  LDNDC_ERR_OK;
               }

        std::string  get_value_as_string(
                char  _type)
        const
        {
            switch ( _type)
            {
                case 'b':
                    return  cbm::n2s< bool_type > ( this->get_value_( type_identifier< bool_type >()));
                case 'i':
                    return  cbm::n2s< int_type >( this->get_value_( type_identifier< int_type >()));
                case 'u':
                    return  cbm::n2s< uint_type >( this->get_value_( type_identifier< uint_type >()));
                case 'd':
                case 'f':
                    return  cbm::n2s< float_type >( this->get_value_( type_identifier< float_type >()), 12);

                default:
                    // falling through
                    break;
            }
            CBM_LogFatal( "unknown type identifier  [type-id='",_type,"']");
            return  "nan";
               }


        bool  is_set()
        const
               {
                   return  !this->is_unset();
        }
        bool  is_unset()
        const
        {
            return  ldndc::invalid_t< uint_type >::value == this->data_.u;
        }

        bool  is_valid(
                char  _type)
        const
        {
            if ( this->is_unset())
            {
                return  false;
            }
            switch ( _type)
            {
                case 'b':
                    return  true;
                case 'i':
                    return  cbm::is_valid< int_type >( this->get_value_( type_identifier< int_type >()));
                case 'u':
                    return  cbm::is_valid< uint_type >( this->get_value_( type_identifier< uint_type >()));
                case 'd':
                case 'f':
                    return  cbm::is_valid< float_type >( this->get_value_( type_identifier< float_type >()));

                default:
                    // falling through
                    break;
            }
            CBM_LogFatal( "unknown type identifier  [type-id='",_type,"']");
            return  false;
               }

        private:
        /* data item */
        mixed_type  data_;

        /* assigment specializations */
        template < typename _T >
        inline
        _T &  assign_( _T _val)
                {
                        LTYPEWRAP_SET_TYPE( this->type_id_( type_identifier< _T >()));
                        return  const_cast< _T & >( this->get_value_( type_identifier< _T >())) = _val;
                }

        /* assigment specializations */
        template < typename _T >
        inline
        _T const &  get_value_( _T) const { /* noop (see template assign_) */}

        inline bool_type const &  get_value_( type_identifier< bool_type >)
        const
        {
            return  this->data_.b;
        }
        inline int_type const &  get_value_( type_identifier< int_type >)
        const
        {
            return  this->data_.i;
        }
        inline uint_type const &  get_value_( type_identifier< uint_type >)
        const
        {
            return  this->data_.u;
        }
        inline float_type const &  get_value_( type_identifier< float_type >)
        const
        {
            return  this->data_.f;
        }
};
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_TYPE_WRAPPER_H_  */

