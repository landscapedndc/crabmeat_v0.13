/*!
 * @brief 
 *    implements stream array (sliding window in
 *    streamed input data)
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
ldndc::stream_array< _E, _CB, _R, _N >::stream_array(
                _CB *  _reload_notifier,
                element_type const &  _init_val)
                : load_size_( 0),
                  fill_size_( 0),
                  buf_displ_( 0),
                  blck_size_( 1),
                  replicate_( 0),
                  adopt_as_blck_size_( 0),
                  reload_notifier_( _reload_notifier)
{
    if ( !_reload_notifier)
    {
        CBM_LogFatal( "BUG:  need to supply reload callback");
    }

    this->mem_set( _init_val);

        CBM_LogDebug( "full_capacity=",(size_t)FULL_CAPACITY, ",  log2(",_R,")=", (unsigned)RECORD_SIZE_LOG);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::stream_array< _E, _CB, _R, _N >::mem_set(
        element_type const &  _value,
        size_t  _n,
        size_t  _off)
{
    crabmeat_assert( _n <= CAPACITY);
    cbm::mem_set( this->buf_+(_off<<RECORD_SIZE_LOG), (( _n == 0) ? FULL_CAPACITY : ( _n<<RECORD_SIZE_LOG)), _value);
}
template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::stream_array< _E, _CB, _R, _N >::mem_set(
        element_type *  _buf,
        size_t  _n,
        element_type const &  _value)
{
    crabmeat_assert( _n <= CAPACITY);
    cbm::mem_set( _buf, _n<<RECORD_SIZE_LOG, _value);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::stream_array< _E, _CB, _R, _N >::current_fill_cnt()
const
{
        return  this->fill_size_;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::stream_array< _E, _CB, _R, _N >::capacity()
const
{
        return  CAPACITY;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::stream_array< _E, _CB, _R, _N >::record_size()
const
{
        return  RECORD_SIZE;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::stream_array< _E, _CB, _R, _N >::get_block_size()
const
{
    return  this->blck_size_;
}
template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::stream_array< _E, _CB, _R, _N >::set_block_size(
        size_t  _blck_size)
{
    if (( _blck_size == 0) || ( _blck_size > CAPACITY))
    {
        CBM_LogError( "invalid block size, must be in {1,...",this->capacity(),"}  [blck-size=",_blck_size,"]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    this->blck_size_ = _blck_size;
    return  LDNDC_ERR_OK;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::stream_array< _E, _CB, _R, _N >::element_type *
ldndc::stream_array< _E, _CB, _R, _N >::data_record(
        size_t _j)
{
    crabmeat_assert( _j < CAPACITY);
        return  this->buf_ + (_j << RECORD_SIZE_LOG);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::stream_array< _E, _CB, _R, _N >::element_type *
ldndc::stream_array< _E, _CB, _R, _N >::operator[](
                typename _CB::argument_type const &  _cb_arg)
{
        return  this->access_( _cb_arg);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::stream_array< _E, _CB, _R, _N >::element_type const *
ldndc::stream_array< _E, _CB, _R, _N >::operator[](
                typename _CB::argument_type const &  _cb_arg)
const
{
        return  const_cast< ldndc::stream_array< _E, _CB, _R, _N > * >( this)->access_( _cb_arg);
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
lerr_t
ldndc::stream_array< _E, _CB, _R, _N >::try_trigger_refresh(
                typename _CB::argument_type const &  _cb_arg)
{
        if ( this->access_( _cb_arg))
        {
                return  LDNDC_ERR_OK;
        }
        return  LDNDC_ERR_INPUT_EXHAUSTED;
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
typename ldndc::stream_array< _E, _CB, _R, _N >::element_type *
ldndc::stream_array< _E, _CB, _R, _N >::access_(
        typename _CB::argument_type const &  _cb_arg)                
{
        if ( this->fill_size_ && ( _cb_arg.t < this->load_size_))
        {
                return  this->buf_ + ((( _cb_arg.t + this->buf_displ_) << RECORD_SIZE_LOG) & FULL_MASK);
        }
        else
        {
                /* we try to get the requested position (might imply multiple reloads) */
                typename _CB::argument_type  cb_arg = _cb_arg;

        /* "usual" stride */
        cb_arg.s = RECORD_SIZE_REAL;

        /* buffer was completely filled, reclaim it */
        if ( this->capacity() == this->fill_size_)
        {
            this->fill_size_ = 0;
        }

        /* calculate maximum number of records to reload */
                if (( _cb_arg.n == 0) || ( _cb_arg.n == ldndc::invalid_t< typename _CB::argument_type::size_type >::value))
        {
            cb_arg.n = this->capacity() - this->fill_size_;
        }
        else
        {
            cb_arg.n = std::min( this->capacity() - this->fill_size_, _cb_arg.n);
        }
        if ( cb_arg.n < this->blck_size_)
        {
            this->buf_displ_ = ( this->buf_displ_ + cb_arg.n) & MASK;
            cb_arg.n = this->blck_size_;
            this->fill_size_ = 0;
        }
        /* set buffer offset */
                cb_arg.buf = const_cast< ldndc::stream_array< _E, _CB, _R, _N > * >( this)->buf_ + ( this->fill_size_ << RECORD_SIZE_LOG);

                /* trigger reload */
                int const  reload_result = this->reload_notifier_->callback( cb_arg);

        if ( reload_result > 0)
                {
                        /* all fine */
                        size_t const  reload_size = static_cast< size_t >( reload_result);
                        this->fill_size_ += reload_size;
                        this->load_size_ += reload_size;

            /* if buffer content replication was requested.. */
            if ( this->replicate_)
            {
                size_t  this_fill_size = const_cast< ldndc::stream_array< _E, _CB, _R, _N > * >( this)->do_replicate_();
                /* keep rotating */
                if ( this->adopt_as_blck_size_)
                {
                    lerr_t  rc_blk_size = const_cast< ldndc::stream_array< _E, _CB, _R, _N > * >( this)->set_block_size( this_fill_size);
                    if ( rc_blk_size)
                    {
                        cb_arg.t = ldndc::invalid_t< typename _CB::argument_type::time_type >::value;
                    }
                }
            }
            else
            {
                if ( reload_result > static_cast< int >( cb_arg.n))
                {
                    CBM_LogWarn( "got more data than requested  [requested=",cb_arg.n,",received=",reload_result,"]");
                }
            }
        }
                else if ( reload_result == 0)
                {
                        /* reinvoke reload */
                }
                else
                {
                        /* error occured */
                        LSTAT_INIT_ERRNO(lstat,LDNDC_ERR_INPUT_EXHAUSTED,LSTAT_CONTINUE|LSTAT_BACKTRACE|LSTAT_ONCE);
                        LSTAT_QUEUE(lstat,"reading past end of data");

                        cb_arg.t = ldndc::invalid_t< typename _CB::argument_type::time_type >::value;
                }

                return  ( cb_arg.t == ldndc::invalid_t< typename _CB::argument_type::time_type >::value) ? (element_type *)NULL : this->access_( cb_arg);
        }
}


template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
void
ldndc::stream_array< _E, _CB, _R, _N >::request_replicate(
        typename _CB::argument_type *  _cb_arg,
        streamarray_flag_e  _flag)
{
    this->replicate_ = 1;
    this->buf_displ_ = ( this->capacity() - this->fill_size_) & MASK;

    if ( _flag & WBUF_ADOPT_AS_BLOCK_SIZE)
    {
        this->adopt_as_blck_size_ = 1;
    }

    this->fill_size_ = 0;

    crabmeat_assert( _cb_arg);
    _cb_arg->n = this->capacity();
    _cb_arg->buf = this->buf_;
}

template < typename  _E, typename  _CB, size_t  _R, size_t  _N >
size_t
ldndc::stream_array< _E, _CB, _R, _N >::do_replicate_()
{
    if ( this->replicate_)
    {
        this->replicate_ = 0;

        size_t  q = this->fill_size_ * RECORD_SIZE_REAL * sizeof( element_type);
        size_t  c = ( FULL_CAPACITY * sizeof( element_type) / q) - 1;

        this->load_size_ += c*this->fill_size_;
        this->fill_size_ = (c+1)*this->fill_size_;

        char *  buf = (char *)this->buf_;
        while ( c--)
        {
            buf += q;
            memcpy( buf, this->buf_, q);
        }

        return  this->fill_size_;
    }

    return  ldndc::invalid_t< typename _CB::argument_type::size_type >::value;
}

