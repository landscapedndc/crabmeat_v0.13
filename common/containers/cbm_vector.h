/*!
 * @brief
 *    import from future..
 *
 * @author 
 *    steffen klatt (created on: mar 01, 2017)
 */

#ifndef  CBM_VECTOR_H_ 
#define  CBM_VECTOR_H_

#include  "containers/lvector.h"

#define  CBM_Vector lvector_t

#endif  /*  CBM_VECTOR_H_  */

