/*!
 * @brief
 *    bit set declaration
 * 
 * @author 
 *      steffen klatt,
 *    edwin haas
 */
#ifndef  LDNDC_BITSET_H_
#define  LDNDC_BITSET_H_

#include  "crabmeat-common.h"
#include  "constants/cbm_const.h"
#include  "math/cbm_math.h"

#define  __BITSET_SIZE(__size__,__base_size__)   ((__size__/__base_size__)+(((__size__) % (__base_size__)) ? 1 : 0))

namespace  ldndc
{

/*!
 * @brief
 *    bitset implementation
 */
template < typename  _bitset_base_t >
class  bitset_integral_t
{
    public:
        /*!
         * @brief
         *    base element block for bitset buffer
         *
         *    the bitset is composed of as many base element
         *    blocks that are required to provide the requested
         *    bitset size. this depends on the bitwidth of the
         *    base element block data type.
         */
        typedef  _bitset_base_t  bitset_base_t;
    
        /* we assume sizes of data types are powers of two ;-) */
        /*! size in bits of each block (base element) in buffer */
        static bitset_base_t const  base_size = BITS_IN_BYTE * sizeof( bitset_base_t);
        /*! log2 of base element size */
        static bitset_base_t const  base_log = cbm::log2_t< base_size >::LOG2;

        typedef  size_t  size_type;

    public:
        bitset_integral_t( 
                size_type,
                bitset_base_t = (bitset_base_t)0);
        ~bitset_integral_t();

        size_type  size() const;

        size_type  bits_set() const;


        void  clear();

        inline
        bool  test( size_type) const;

        inline
        void  set( size_type);

        /*!
         * @brief
         *    check if all set bits are matching
         */
        inline
        bool  all() const;

            /*!
         * @brief
         *    check if all set bits are matching
         */
        inline
        bool  all( bitset_integral_t const &) const;

        /*!
         * @brief
         *    check if at least one bit is set
         */
        inline
        bool  any() const;

        /*!
         * @brief
         *    check if at least one set bit is matching
         */
        inline
        bool  any( bitset_integral_t const &) const;

        //flip, toggle, clear, ...

        std::string  to_string() const;
        
    private:
        /* buffer */
        bitset_base_t *  bitset_;
        /* most significant bit in bitset */
        size_type  size_;
        /* have one stack element for small bitsets (most of them are small) */
        bitset_base_t  bitset_fast_buf_;

        /* number of bits currently set, i.e. have value "1" */
        size_type  bits_set_;

        void  fail_check_( size_type, size_type) const;

        size_type  count_bits_( bitset_base_t);

        /* hide them until we need them... */
        bitset_integral_t( bitset_integral_t const &);
        bitset_integral_t &  operator=( bitset_integral_t const &);

};
}  /*  namespace ldndc  */

#include  "containers/cbm_bitset.inl"

template < typename  _B >
inline
bool
ldndc::bitset_integral_t< _B >::test(
                size_type  _bit)
const
{
        fail_check_( this->size_, _bit);

        if ( this->size_ > base_size)
        {
                /* shift by base_log determines appropriate base element block */
                size_t const  b( _bit>>base_log);
                return  ( this->bitset_[b] & ((bitset_base_t)1 << (_bit - (b*base_size)))) != (bitset_base_t)0;
        }
        /* size =< base_size */
        return  ( this->bitset_[0] & ((bitset_base_t)1 << _bit)) != (bitset_base_t)0;
}


template < typename  _B >
inline
void
ldndc::bitset_integral_t< _B >::set(
                size_type  _bit)
{
        fail_check_( this->size_, _bit);

        bitset_base_t  b_block( this->bitset_[0]);
        size_type  b( 0);

        if ( this->size_ > base_size)
        {
                /* shift by base_log determines appropriate base element block */
                b = _bit >> base_log;
                b_block = this->bitset_[b];

                this->bitset_[b] |= ((bitset_base_t)1 << (_bit - (b*base_size)));
        }
        else /* size_ =< base_size */
        {
                this->bitset_[0] |= ((bitset_base_t)1 << _bit);
        }
        this->bits_set_ += ( b_block != this->bitset_[b]) ? (size_type)1 : (size_type)0;
}


template < typename  _B >
inline
bool
ldndc::bitset_integral_t< _B >::all()
const
{
        return  this->bits_set_ == this->size_;
}


template < typename  _B >
inline
bool
ldndc::bitset_integral_t< _B >::all(
                bitset_integral_t< _B > const &  _b)
const
{
        if ( this->bits_set_ < _b.bits_set())
        {
                return  false;
        }
        /* highest bit set would be sufficient */
        size_type  bs_len( std::min( this->size_, _b.size()));
        size_type  r_all( _b.bits_set());
        for ( size_type  r = 0, k = 0;  ( k < bs_len) && ( r < r_all);  ++k)
        {
                if ( _b.test( k))
                {
                        if ( this->test( k))
                        {
                                ++r;
                        }
                        else
                        {
                                return  false;
                        }
                }
        }
        return  true;
}


template < typename  _B >
inline
bool
ldndc::bitset_integral_t< _B >::any()
const
{
        return  this->bits_set_ > 0u;
}


template < typename  _B >
inline
bool
ldndc::bitset_integral_t< _B >::any(
                bitset_integral_t< _B > const &  _b)
const
{
        if ( _b.bits_set() == 0)
        {
                return  true;
        }
        if ( this->bits_set_ == (size_type)0)
        {
                return  false;
        }
        size_type  bs_len( std::min( this->size_, _b.size()));
        size_type  r_all( _b.bits_set());
        for ( size_type  r = 0, k = 0;  ( k < bs_len) && ( r < r_all);  ++k)
        {
                if ( _b.test( k))
                {
                        if ( this->test( k))
                        {
                                return  true;
                        }
                        ++r;
                }
        }
        return  false;
}


template < typename _B >
std::ostream &
operator<<( std::ostream &  _out,
        ldndc::bitset_integral_t< _B > const &  _bitset)
{
    _out << _bitset.to_string();
        return  _out;
}


namespace  ldndc
{
    typedef  ldndc::bitset_integral_t< unsigned int >  bitset;
}


#endif  /*  !LDNDC_BITSET_H_  */
