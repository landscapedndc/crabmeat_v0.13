/*!
 * @brief 
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */
#ifndef  LDNDC_LINKARRAY_H_
#define  LDNDC_LINKARRAY_H_

#include  "crabmeat-common.h"


#define  p_BUFHDR(__buf__) ((buffer_header_t *)(((element_type*)(__buf__))-BUFFER_HEADER_SIZE))

namespace  ldndc
{

template < typename  _E >
class  linkarray_operations_t
{
    public:
        typedef  typename _E::element_type  element_type;
        typedef  typename _E::header_data_t  header_data_t;
        typedef  size_t  record_size_t;

        struct  buffer_record_link_t
        {
            /* links to buffers with next lower/higher resolution */
            element_type *  buf_lo, *  buf_hi;
        };
        struct  buffer_header_t
        {
            buffer_record_link_t  lnk;
            /* capacity of buffer in units of elements */
            size_t  capacity;

            /* buffer's header data */
            header_data_t  hx;
        };

        enum  linkarray_flag_e
        {
            LINKARRAY_NONE = 0x0,
            /* use trailing record elements to link record lines */
            LINKARRAY_LINK_RECORDS = 0x1,
            /* treat record size as final */
            LINKARRAY_SIZE_FINAL = 0x2
        };
        typedef  lflags_t  linkarray_flag_t;

        template < size_t  _R >
        struct  static_record_size_t
        {
            enum
            {
                /* buffer header size in units of element-type */
                __BUFFER_HEADER_SIZE = sizeof(buffer_header_t)/sizeof(element_type) + ((sizeof(buffer_header_t) == ((sizeof(buffer_header_t)/sizeof(element_type))*sizeof(element_type))) ? 0 : 1),
                /* buffer header size in units of records */
                BUFFER_HEADER_SIZE = _R * ((__BUFFER_HEADER_SIZE/_R) + (( __BUFFER_HEADER_SIZE == ((__BUFFER_HEADER_SIZE/_R)*_R)) ? 0 : 1)),

                /* buffer record link size in units of element-type */
                __BUFFER_RECORD_LINK_SIZE = sizeof(buffer_record_link_t)/sizeof(element_type) + ((sizeof(buffer_record_link_t) == ((sizeof(buffer_record_link_t)/sizeof(element_type))*sizeof(element_type))) ? 0 : 1),
                BUFFER_RECORD_LINK_SIZE = __BUFFER_RECORD_LINK_SIZE
            };
        };

    public:
        /*!
         * @brief
         *    read header from buffer
         */
        lerr_t  read_buffer_header(
                buffer_header_t *, element_type const *);
        /*!
         * @brief
         *    write header from buffer
         */
        lerr_t  write_buffer_header(
                element_type *, buffer_header_t const *);


        /*!
         * @brief
         *    link buffer b0 with b1,
         *
         *    link(b0,b1) = link(b1,b0)
         */
        lerr_t  link(
                element_type * /*b0*/, element_type * /*b1*/);

        /*!
         * @brief
         *    read record link from buffer record
         */
        lerr_t  read_record_link(
                buffer_record_link_t * /*record link*/,
                element_type const * /*buffer*/);
        /*!
         * @brief
         *    write record link to buffer record
         */
        lerr_t  write_record_link(
                element_type * /*buffer*/,
                buffer_record_link_t const * /*record link*/);

        /*!
         * @brief    
         *    link buffer records. stride less than zero makes b0
         *    the smaller buffer and vice versa. the size is interpreted
         *    to belong to the smaller buffer
         *
         *    link_records(b0,b1,n,s) = link(b1,b0,n,-s)
         */
        lerr_t  link_records(
                element_type * /*buffer b0*/, element_type * /*buffer b1*/,
                int /*size*/, int /*stride*/);

    public:
        linkarray_operations_t(
                record_size_t /*record size*/,
                linkarray_flag_t = LINKARRAY_NONE);

        /*!
         * @brief
         *    get buffer size in units of records
         */
        size_t  capacity(
                element_type const * /*buffer*/) const;

        /*!
         * @brief
         *    number of elements per record
         */
        size_t  record_size() const;

        /*!
         * @brief
         *    allocate memory for given number of records. this
         *    method additionally allocates enough memory to
         *    store the header in front of the data portion
         */
        element_type *  allocate(
                size_t /*size in units of records*/, header_data_t const * = NULL /*header data*/);
        /*!
         * @brief
         *    frees the memory allocated via this class'
         *    allocate method, i.e. it effectively frees
         *    the memory starting at the given buffers
         *    header.
         */
        void  deallocate(
                element_type *);

        /*!
         * @brief
         *    fill own buffer with constant value
         *
         * @param
         *    mem-setting in units of records (keeping
         *    record-links) starting at given offset.
         * @param
         *    number of records to set
         * @param
         *    value to set
         */
        void  mem_set(
                element_type * /*offset*/,
                       size_t /*number of records*/,
                element_type const & = ldndc::invalid_t< element_type >::value);

        /*!
         * @brief
         *    find buffer "left" of buffer b0
         */
        lerr_t  find_lower(
                element_type ** /*pointer to (left) lower buffer*/,
                element_type const * /*buffer b0*/);

        /*!
         * @brief
         *    find buffer "right" of buffer b0
         */
        lerr_t  find_higher(
                element_type ** /*pointer to (right) higher buffer*/,
                element_type const * /*buffer b0*/);

    private:
        record_size_t  record_size_;
        struct
        {
            bool  link_records:1;
            bool  size_final:1;
        } flags;
    public:
        record_size_t const  BUFFER_HEADER_SIZE;
        record_size_t const  BUFFER_RECORD_LINK_SIZE;
};
}  /*  namespace ldndc  */

#include  "containers/linkarray.inl"


#endif  /*  !LDNDC_LINKARRAY_H_  */

