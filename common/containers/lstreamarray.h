/*!
 * @brief 
 *    implements stream array (sliding window in
 *    streamed input data)
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */
#ifndef  LDNDC_STREAM_ARRAY_H_
#define  LDNDC_STREAM_ARRAY_H_

#include  "crabmeat-common.h"
#include  "math/cbm_math.h"

#include  "utils/callback.h"

namespace  ldndc
{
/* 
 * @brief
 *    array having n+1 records, record size is m+1
 *
 *    array layout:
 *        | 0_0 0_1 ... 0_m | 1_0 1_1 ... 1_m | ... | n_1 n_2 ... n_m |
 *
 * @todo
 *    specialize for _R = 1?
 */
template < typename  _E /* array element type */, typename  _CB /* callback argument type */, size_t  _R /* record size */, size_t  _N /* array size (2^_N), i.e. number of records */ >
class  stream_array
{
    public:
        static size_t const  RECORD_SIZE = _R;
        static size_t const  RECORD_SIZE_LOG = cbm::log2_t< _R >::LOG2 + ((_R > cbm::exp2_t< cbm::log2_t<_R>::LOG2>::EXP2) ? 1 : 0);
        static size_t const  RECORD_SIZE_REAL = cbm::exp2_t< RECORD_SIZE_LOG >::EXP2;

        static size_t const  CAPACITY_LOG = _N;

        static size_t const  CAPACITY = cbm::exp2_t< CAPACITY_LOG >::EXP2;
        static size_t const  FULL_CAPACITY = cbm::exp2_t< CAPACITY_LOG + RECORD_SIZE_LOG >::EXP2;

        static size_t const  MASK = CAPACITY - (size_t)1;
        static size_t const  FULL_MASK = FULL_CAPACITY - (size_t)1;

        typedef  _E  element_type;

        typedef  _CB  reload_notify_t;

    private:
        /* data buffer */
        element_type  buf_[FULL_CAPACITY];

    public:
        /*!
         * @brief
         *    constructor
         *
         * @param
         *    call back function (used to reload data)
         * @param
         *    default element (used to initialize buffer)
         */
        stream_array(
                _CB *,
                element_type const & = 0);

        /*!
         * @brief
         *    fill own buffer with constant value
         *
         * @param
         *    value to set
         * @param
         *    number of records to set
         * @param
         *    offset, in number of records, in own buffer
         */
        void  mem_set(
                element_type const & = ldndc::invalid_t< element_type >::value,
                       size_t = 0,
                size_t = 0);
        /*!
         * @brief
         *    fill own buffer with constant value
         *
         * @param
         *    "offset", i.e. use given pointer to buffer
         *    rather than own but memset in units of records
         * @param
         *    number of records to set
         * @param
         *    value to set
         */
        void  mem_set(
                element_type *,
                       size_t,
                element_type const & = ldndc::invalid_t< element_type >::value);

        /*!
         * @brief
         *    number of records we got after last
         *    reload
         */
        size_t  current_fill_cnt() const;

        /*!
         * @brief
         *    number of elements this stream array
         *    can hold in terms of records
         */
        size_t  capacity() const;

        /*!
         * @brief
         *    number of elements per record
         */
        size_t  record_size() const;

        /*!
         * @brief
         *    like access operator without index
         *    translation and reload mechanism. it
         *    simply returns the k-th record of
         *    internal data buffer
         *
         * @param
         *    index of data record, 0 <= index < CAPACITY
         */
        element_type *  data_record( size_t /*index*/);

        /* note
         *    what about reading backwards? no reading backward.
         *    this thing is called stream array and that's
         *    exactly what it is.
         *
         * TODO benchmark different access methods
         *        - bit operations (needs special array element alignments)
         *        - simple arithmetic (memory friendly)
         *        - lut (no good on cache miss .. probably)
         */
        element_type *  operator[](
                typename reload_notify_t::argument_type const &);
        /*!
         * @brief
         *    const version of access operator
         */
        element_type const *  operator[](
                typename reload_notify_t::argument_type const &) const;

        /*!
         * @brief
         *    triggers reload if needed determined
         *    by given index.
         *
         * @param
         *    index
         *
         * @note
         *    with new approach of updating internal states
         *    of input classes, this becomes a bit of overkill...
         *    we keep it for a while :-)
         */
        lerr_t  try_trigger_refresh(
                typename reload_notify_t::argument_type const &);

    public:
        /*!
         * @brief
         *    get currently set block size
         */
        size_t  get_block_size() const;
        /*!
         * @brief
         *    set minimum number of records that need to fit into
         *    buffer during reload.
         */
        lerr_t  set_block_size(
                size_t /*block size*/);

    public:
        enum  streamarray_flag_e
        {
            WBUF_NONE            = 0,
            /*! when requesting replication, set fill size as block size */
            WBUF_ADOPT_AS_BLOCK_SIZE    = 1 << 0
        };
        /*!
         * @brief
         *    requests to duplicate the content in the buffer as
         *    often as capacity permits. actual replication is
         *    postponed until after return from callback.
         */
        void  request_replicate(
                   typename reload_notify_t::argument_type *,
                          streamarray_flag_e = WBUF_NONE);
    private:
        size_t  do_replicate_();

    private:
        /* number of records read so far (packed on left side) */
        size_t  load_size_;
        /* number of records currently stored in buffer */
        size_t  fill_size_;
        /* keeps track of introduced gaps in buffer */
        size_t  buf_displ_;
        /* holds block size for this stream array */
        size_t  blck_size_;
        /* holds buffer stride, determines offset of next record */
        size_t  buf_stride_;

        unsigned char  replicate_;
        unsigned char  adopt_as_blck_size_;

        /* reload request callback (the owner of this array knows what to do) */
        _CB  *  reload_notifier_;

        /*
         * second parameter switches on fail detection (could be a bool)
         */
        element_type *  access_(
                typename reload_notify_t::argument_type const &);
};
}  /*  namespace ldndc  */

#include  "containers/lstreamarray.inl"


#endif  /*  !LDNDC_STREAM_ARRAY_H_  */

