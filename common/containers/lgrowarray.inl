/*!
 * @brief 
 *    implements silently growing array (yes,
 *    sort of like a vector but without resizing
 *    it explicitly)
 *
 * @author 
 *      steffen klatt
 */

#include  "containers/lgrowarray.h"
#include  "log/cbm_baselog.h"


template < class  _E, unsigned int  _C, unsigned short int  _I >
ldndc::growable_array< _E, _C, _I >::growable_array(
                unsigned int  _initial_size,
                _E const &  _default_val)
                : buf_( container_type( __GROWABLE_ARRAY_SIZE(_initial_size,_I))),
                  dflt_val_( _default_val)
{
        crabmeat_assert( _initial_size <= MAX_SIZE);
}


template < class  _E, unsigned int  _C, unsigned short int  _I >
size_t
ldndc::growable_array< _E, _C, _I >::size()
const
{
        return  buf_.size();
}

 
template < class  _E, unsigned int  _C, unsigned short int  _I >
_E const &
ldndc::growable_array< _E, _C, _I >::operator[](
                size_t  _k)
const
{
        if ( _k < buf_.size())
        {
                return  buf_[_k];
        }
        CBM_LogFatal( "access violation, index out of bounds");
        return  buf_[0];
}
 

template < class  _E, unsigned int  _C, unsigned short int  _I >
_E &
ldndc::growable_array< _E, _C, _I >::operator[](
                size_t  _k)
{
        this->resize( _k+1);
        return  buf_[_k];
}


template < class  _E, unsigned int  _C, unsigned short int  _I >
lerr_t
ldndc::growable_array< _E, _C, _I >::resize(
                size_t  _size)
{
        if ( _size > buf_.size())
        {
                size_t  new_size( __GROWABLE_ARRAY_SIZE(_size,_I));
                if ( new_size > MAX_SIZE)
                {
                        CBM_LogError( "exceeded maximum number of elements allowed for growable_array");
                        return  LDNDC_ERR_REQUEST_MISMATCH;
                }
                for ( size_t  l = buf_.size();  l < new_size;  ++l)
                {
                        buf_.push_back( dflt_val_);
                }
        }
        return  LDNDC_ERR_OK;
}


template < class  _E, unsigned int  _C, unsigned short int  _I >
_E &
ldndc::growable_array< _E, _C, _I >::access_op_(
                size_t  _k)
{
        return  buf_[_k];
}




template < class  _E, unsigned int  _C>
std::ostream &
operator<<(
                std::ostream &  _out,
                ldndc::growable_array< _E, _C > const &  _g_arr)
{
    _out << "growable array [size=" << _g_arr.size() << "]";
    return  _out;
}

