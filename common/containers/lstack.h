/*!
 * @brief 
 *    stack
 *
 * @author 
 *      steffen klatt (created on: dec 17, 2013),
 *    edwin haas
 */
#ifndef  LDNDC_STACK_H_
#define  LDNDC_STACK_H_

#include  "crabmeat-common.h"
#include  <stack>

namespace  ldndc
{

template < class  _E >
class  stack
{
    public:
        typedef  _E  element_type;

    public:
        size_t  size()
        const;

        size_t  fillsize()
        const;

        bool  is_empty()
        const;

        bool  is_full()
        const;

        void  clear();

        element_type const &  push( element_type const &);

        element_type const &  peek();
        element_type  pop();

    private:
        std::stack< _E >  stack_;
};
}  /*  namespace ldndc  */

#include  "containers/lstack.inl"

#endif  /*  !LDNDC_STACK_H_  */

