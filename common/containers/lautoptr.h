/*!
 * @brief
 *    auto pointers take ownership of memory pointed
 *    to by pointer and destroy it as soon as it
 *    goes out of scope
 *
 * @author 
 *      steffen klatt
 */
#ifndef  LDNDC_AUTO_PTR_H_
#define  LDNDC_AUTO_PTR_H_

#include  "crabmeat-common.h"

namespace  ldndc
{
    /*!
     * @brief
     *    STL's auto_ptr (deprecated) clone
     */
    template < class  _T >
    struct  auto_ptr
    {
        auto_ptr( _T *);
        auto_ptr( _T const *);

        virtual  ~auto_ptr();

        operator  _T *()
        {
            return  this->ptr_;
        }
        operator _T const *()
        const
        {
            return  this->ptr_;
        }


        _T &  operator*();
        _T *  operator->();

        _T const &  operator*() const;
        _T const *  operator->() const;


        protected:
            _T *  ptr_;
    };

    /*!
     * @brief
     *    We just overwrite the destructor here: delete[]
     */
    template < class  _T >
    struct  auto_ptr_array  :  public  ldndc::auto_ptr< _T >
    {
        auto_ptr_array( _T *);
        auto_ptr_array( _T const *);
        virtual  ~auto_ptr_array();
    };

}  /*  namespace ldndc  */

#include  "lautoptr.inl"


#endif  /*  !LDNDC_AUTO_PTR_H_  */

