/*!
 * @brief
 *    auto pointers take ownership of memory pointed
 *    to by pointer and destroy it as soon as it
 *    goes out of scope
 *
 * @author 
 *      steffen klatt
 */

#include  "lautoptr.h"

template < typename  _T >
ldndc::auto_ptr< _T >::auto_ptr(
        _T *  _ptr)
        : ptr_( _ptr)
{
}
template < typename  _T >
ldndc::auto_ptr< _T >::auto_ptr(
        _T const *  _ptr)
        : ptr_( const_cast< _T * >( _ptr))
{
}

template < typename  _T >
ldndc::auto_ptr< _T >::~auto_ptr()
{
    if ( this->ptr_)
    {
        delete  this->ptr_;
        this->ptr_ = NULL;
    }
}

template < typename  _T >
_T &
ldndc::auto_ptr< _T >::operator*()
{
    if ( this->ptr_)
    {
        return  *this->ptr_;
    }
    CBM_LogFatal( "dereferencing NULL-pointer...");
    return  *this->ptr_;
}
template < typename  _T >
_T *
ldndc::auto_ptr< _T >::operator->()
{
    return  this->ptr_;
}


template < typename  _T >
_T const &
ldndc::auto_ptr< _T >::operator*()
const
{
    return  const_cast< auto_ptr< _T > * >( this)->operator*();
}
template < typename  _T >
_T const *
ldndc::auto_ptr< _T >::operator->()
const
{
    return  const_cast< auto_ptr< _T > * >( this)->operator->();
}


/** array version **/

template < typename  _T >
ldndc::auto_ptr_array< _T >::auto_ptr_array(
        _T *  _obj)
        : ldndc::auto_ptr< _T >( _obj)
{
}

template < typename  _T >
ldndc::auto_ptr_array< _T >::auto_ptr_array(
        _T const *  _obj)
        : ldndc::auto_ptr< _T >( _obj)
{
}


template < typename _T >
ldndc::auto_ptr_array< _T >::~auto_ptr_array()
{
    if ( this->ptr_)
    {
        delete[]  this->ptr_;
        this->ptr_ = NULL;
    }
}

