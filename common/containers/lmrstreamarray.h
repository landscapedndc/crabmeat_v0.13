/*!
 * @brief 
 *    implements multi-resolution stream array (sliding
 *    window in streamed input data with multiple resolutions)
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */
#ifndef  LDNDC_MULTIRESOLUTION_STREAM_ARRAY_H_
#define  LDNDC_MULTIRESOLUTION_STREAM_ARRAY_H_

#include  "crabmeat-common.h"
#include  "math/cbm_math.h"

#include  "utils/callback.h"
#include  "containers/linkarray.h"


#define  n_BUFDATA(__buf__) (((element_type *)(__buf__))+BUFFER_HEADER_SIZE)
#define  p_BUFHDR(__buf__) ((buffer_header_t *)(((element_type*)(__buf__))-BUFFER_HEADER_SIZE))

namespace  ldndc
{
/* 
 * @brief
 *    array having n+1=2^_N records, record size _R <= m+1 = 2^k
 *    (k is minimum to satify condition)
 *
 *    array layout:
 *        | 0_0 0_1 ... 0_m | 1_0 1_1 ... 1_m | ... | n_0 n_1 ... n_m |
 */
template < typename  _E /* array info type */, typename  _CB /* callback type */, size_t  _R /* record size */, size_t  _N /* array size (2^_N), i.e. number of records */ >
class  multiresolution_stream_array_t
{
    public:
        static size_t const  RECORD_SIZE = _R;
        static size_t const  RECORD_SIZE_LOG = cbm::log2_t< _R >::LOG2 + ((_R > cbm::exp2_t< cbm::log2_t<_R>::LOG2>::EXP2) ? 1 : 0);
        static size_t const  RECORD_SIZE_REAL = cbm::exp2_t< RECORD_SIZE_LOG >::EXP2;

        static size_t const  CAPACITY_LOG = _N;

        static size_t const  CAPACITY = cbm::exp2_t< CAPACITY_LOG >::EXP2;
        static size_t const  FULL_CAPACITY = cbm::exp2_t< CAPACITY_LOG + RECORD_SIZE_LOG >::EXP2;

        static size_t const  MASK = CAPACITY - (size_t)1;
        static size_t const  FULL_MASK = FULL_CAPACITY - (size_t)1;

        typedef  typename _E::element_type  element_type;

        typedef  _CB  reload_notify_t;

        typedef  typename linkarray_operations_t< _E >::buffer_record_link_t  buffer_record_link_t;
        typedef  typename linkarray_operations_t< _E >::buffer_header_t  buffer_header_t;

        typedef  typename linkarray_operations_t< _E >::template static_record_size_t< RECORD_SIZE_REAL >  BUFFER_RECORD_LINK_SIZE_t;
        static size_t const  BUFFER_RECORD_LINK_SIZE = BUFFER_RECORD_LINK_SIZE_t::BUFFER_RECORD_LINK_SIZE;
        typedef  typename linkarray_operations_t< _E >::template static_record_size_t< RECORD_SIZE_REAL >  BUFFER_HEADER_SIZE_t;
        static size_t const  BUFFER_HEADER_SIZE = BUFFER_HEADER_SIZE_t::BUFFER_HEADER_SIZE;

    private:
        typedef  union
        {
            buffer_header_t *  hdr;
            element_type *  buf;
        }  cast_header_t;
        /* data buffer */
        element_type  buf_[FULL_CAPACITY+BUFFER_HEADER_SIZE];


        /*! @brief
         *    allocate and link buffers if applicable
         */
        lerr_t  allocate_buffers_(
                int const *, int,
                int const *, int,
                int,
                element_type);
        /*! @brief
         *    make links between buffer records
         */
        lerr_t  format_buffers_();

        /*!
         * @brief
         *    find buffer by resolution.
         *
         *    assumes that buffers are sorted with respect
         *    to resolution -N_0, ..., N_k
         *
         * @param
         *    pointer to hold found buffer
         * @param
         *    requested resolution
         * @param
         *    find exact match (0), find largest resolution
         *    less than or equal to requested (-1), find smallest
         *    resolution greater than or equal to requested (1)
         */
        lerr_t  find_by_resolution(
                element_type ** /*pointer to buffer*/,
                int /*resolution*/,
                int = 0 /*match*/);

    public:
        multiresolution_stream_array_t();
        /*!
         * @brief
         *    constructor
         *
         * @param
         *    call back function (used to reload data)
         * @param
         *    lower resolution array (R_k relative to R_k-1)
         * @param
         *    lower resolution array size
         * @param
         *    higher resolution array (R_k relative to R_k-1)
         * @param
         *    higher resolution array size
         * @param
         *    reference resolution (used to scale L_i,H_k)
         * @param
         *    default element (used to initialize buffer)
         */
        multiresolution_stream_array_t(
                _CB * /*callback*/,
                int const * /*L*/, int /*|L|*/,
                int const * /*H*/, int /*|H|*/,
                int /*reference resolution*/,
                element_type const & = 0 /*default value*/);

        ~multiresolution_stream_array_t();

        /*!
         * @brief
         *    reset all internal record counters and
         *    memset buffers
         */
        lerr_t  clear(
                element_type const & = ldndc::invalid_t< element_type >::value);

        /*!
         * @brief
         *    fill buffer with constant value
         *
         * @param
         *    "offset", i.e. use given pointer to buffer
         *    rather than own but memset in units of records
         * @param
         *    number of records to set
         * @param
         *    value to set
         */
        void  mem_set(
                element_type *,
                       size_t,
                element_type const & = ldndc::invalid_t< element_type >::value);
        /*!
         * @brief
         *    fill all buffers owned by this object with constant value
         *
         * @param
         *    value to set
         */
        void  mem_set_all(
                element_type const & = ldndc::invalid_t< element_type >::value);

        int  is_ok() const { return  this->m.bad_state == 0; }
    protected:
        void  bad_state() { this->m.bad_state = 1; }

    public:

        /*!
         * @brief
         *    number of records we got after last
         *    reload
         */
        size_t  current_fill_cnt() const;

        /*!
         * @brief
         *    number of elements this stream array
         *    can hold in terms of records
         */
        size_t  capacity() const;

        /*!
         * @brief
         *    number of elements per record
         */
        size_t  record_size() const;

        /* note
         *    what about reading backwards? no reading backward.
         *    this thing is called stream array and that's
         *    exactly what it is.
         *
         * TODO benchmark different access methods
         *        - bit operations (needs special array element alignments)
         *        - simple arithmetic (memory friendly)
         *        - lut (no good on cache miss .. probably)
         */
        element_type *  operator[](
                typename reload_notify_t::argument_type const *);
        /*!
         * @brief
         *    const version of access operator
         */
        element_type const *  operator[](
                typename reload_notify_t::argument_type const *) const;

        /*!
         * @brief
         *    triggers reload if needed determined
         *    by given index.
         *
         * @param
         *    index
         *
         * @note
         *    with new approach of updating internal states
         *    of input classes, this becomes a bit of overkill...
         *    we keep it for a while :-)
         */
        lerr_t  try_trigger_refresh(
                typename reload_notify_t::argument_type const *);

        /*!
         * @brief
         *    select appropriate buffer for given resolution
         */
        lerr_t  reload_constraints(
                typename reload_notify_t::argument_type * /*reload info*/,
                int /*resolution*/, int = 0 /*match*/);

    public:
        /*!
         * @brief
         *    requests to duplicate the content in the buffer as
         *    often as capacity permits. actual replication is
         *    postponed until after return from callback.
         */
        void  request_replicate();
    private:
        size_t  do_replicate_();

    private:
        /* number of records read so far (packed on left side) */
        size_t  load_size_;
        /* number of records currently stored in buffer */
        size_t  fill_size_;
        /* keeps track of introduced gaps in buffer */
        size_t  buf_displ_;

        void  reset_status_flags_();
        struct
        {
            bool  bad_state:1;

            bool  replicate:1;
            bool  is_replicated:1;

            bool  has_lo:1;
            bool  has_hi:1;
        } m;

        /* reload request callback (the owner of this array knows what to do) */
        _CB  *  reload_notifier_;

        /*
         * second parameter switches on fail detection (could be a bool)
         */
        element_type *  access_(
                typename reload_notify_t::argument_type const *);

    private:
// TODO
        multiresolution_stream_array_t(
                multiresolution_stream_array_t const &);
        multiresolution_stream_array_t &  operator=(
                multiresolution_stream_array_t const &);


};
}  /*  namespace ldndc  */

#include  "containers/lmrstreamarray.inl"


#endif  /*  !LDNDC_MULTIRESOLUTION_STREAM_ARRAY_H_  */

