/*!
 * @brief 
 *    declares wrappers around char lists and stl strings arrays,
 *    where bracket operator returns char list 
 * 
 * @author 
 *      steffen klatt
 */

#include  "containers/lchrll.h"
#include  "log/cbm_baselog.h"

ldndc::charlist_array_base::~charlist_array_base()
{
}


ldndc::charlist_array< char >::charlist_array(
                char const * const *  _c_arr)
                : charlist_array_base(),
                  d( _c_arr)
{
}


char const *
ldndc::charlist_array< char >::operator[](
                size_t  _k)
const
{
        return  d[_k];
}


bool
ldndc::charlist_array< char >::no_data()
const
{
        return  !d;
}


ldndc::charlist_array< std::string >::charlist_array(
                std::string const *  _arr)
                : charlist_array_base(),
                  d( _arr)
{
}


char const *
ldndc::charlist_array< std::string >::operator[](
                size_t  _k)
const
{
        return  d[_k].c_str();
}


bool
ldndc::charlist_array< std::string >::no_data()
const
{
        return  !d;
}


ldndc::charlist_array< ldndc_string_t >::charlist_array(
                ldndc_string_t const *  _arr)
                : charlist_array_base(),
                  d( _arr)
{
}


char const *
ldndc::charlist_array< ldndc_string_t >::operator[](
                size_t  _k)
const
{
        return  static_cast< char const * >( d[_k]);
}


bool
ldndc::charlist_array< ldndc_string_t >::no_data()
const
{
        return  !d;
}

