/*!
 * @author
 *    steffen klatt  (created on: mar 15, 2014),
 *    edwin haas
 */

#include  "containers/ltinyhashmap.h"
#include  "log/cbm_baselog.h"

#include  "regex/cbm_regex.h"
#include  "hash/cbm_hash.h"
#include  "utils/cbm_utils.h"
#include  "string/cbm_string.h"

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::get_number_of_entries()
const
{
    int  c = 0;
    for ( size_t  i = 0;  i < _TABLE_SIZE;  ++i)
    {
        c += this->buf[i].size();
    }
    return  c;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::get_number_of_entries_matching(
        char const *  _regex)
const
{
    return  this->get_keys_matching( NULL, _regex);
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::make_item_key(
        key_type *  _key, char const *  _format, ...)
{
    va_list  p;
    va_start( p, _format);
    int const  r_size = vmake_item_key( _key->name, _format, p);
    va_end( p);

    return  r_size;
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::vmake_item_key(
        key_type *  _key, char const *  _format, va_list  _p)
{
    return  vmake_item_key( _key->name, _format, _p);
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::make_item_key(
        char  _key[KEY_MAXSIZE+1], char const *  _format, ...)
{
    va_list  p;
    va_start( p, _format);
    int const  r_size = vmake_item_key( _key, _format, p);
    va_end( p);

    return  r_size;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::vmake_item_key(
        char  _key[KEY_MAXSIZE+1], char const *  _format, va_list  _p)
{
    int const  r_size = cbm::vsnprintf( _key, _KEY_MAXSIZE, _format, _p);
    _key[_KEY_MAXSIZE] = '\0';

    if ( r_size > static_cast< int >( _KEY_MAXSIZE))
    {
        CBM_LogError( "tinyhashmap_t::make_item_key(): ",
                "a key was to long  [format=",_format,",key=",_key,",size=",r_size,",maxsize=",_KEY_MAXSIZE,"]");
        return  -1;
    }
    else if ( r_size == 0)
    {
        CBM_LogError( "tinyhashmap_t::make_item_key(): ",
                "empty key");
        return  -2;
    }

    return  r_size;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
void
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::remove_all()
{
    for ( size_t  t = 0;  t < _TABLE_SIZE;  ++t)
    {
        this->buf[t].clear();
    }
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
size_t
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::find_table_position(
        char const *  _key)
const
{
    return  this->make_hash( _key) % _TABLE_SIZE;
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
unsigned long int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::make_hash(
        char const *  _key)
const
{
    return  cbm::hash::djb2( _key);
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
bool
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::variable_exists(
        char const *  _key)
const
{
    size_t const  t_i = this->find_table_position( _key);
    typename  table_item_t::const_iterator  t_e = this->buf[t_i].begin();
    while ( t_e != this->buf[t_i].end())
    {
        if ( cbm::is_equal( t_e->key, _key))
        {
            return  true;
        }
        ++t_e;
    }
    return  false;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
bool
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::variable_prefix_exists(
        char const *  _prefix)
const
{
    for ( size_t  t_i = 0;  t_i < _TABLE_SIZE;  ++t_i)
    {
        typename  table_item_t::const_iterator  t_e = this->buf[t_i].begin();
        while ( t_e != this->buf[t_i].end())
        {
            if ( cbm::has_prefix( t_e->key, _prefix))
            {
                return  true;
            }
            ++t_e;
        }
    }
    return  false;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
void
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::remove(
        char const *  _key)
{
    size_t  t_i = this->find_table_position( _key);
    typename  table_item_t::iterator  t_e = this->buf[t_i].begin();
    while ( t_e != this->buf[t_i].end())
    {
        if ( cbm::is_equal( t_e->key, _key))
        {
            this->buf[t_i].erase( t_e);
            return;
        }
        ++t_e;
    }
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
_V const *
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::get(
        char const *  _key)
const
{
    size_t  t_i = this->find_table_position( _key);
    if ( !this->buf[t_i].empty())
    {
// sk:dbg        CBM_LogDebug( "GET:  key(exists)=\"",_key,"\"  t_i=",t_i, "  collision-count=",this->buf[t_i].size());
        typename  table_item_t::const_iterator  t_e = this->buf[t_i].begin();
        while ( t_e != this->buf[t_i].end())
        {
            if ( cbm::is_equal( t_e->key, _key))
            {
                return  t_e->data;
            }
            ++t_e;
        }
    }
// sk:dbg    CBM_LogDebug( "GET:  key(does not exist)=\"",_key,"\"");
    return  NULL;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
_V const *
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::get(
        char const *  _key,
        _V *  _reg, size_t  _n_reg)
const
{
    _V const *  data = this->get( _key);
    if ( data)
    {
        this->copy_data( _reg, data, _n_reg);
    }
    return  data;
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::get_keys_matching(
        key_list_t *  _key_list,
        char const *  _regex)
const
{
    int  c = 0;
    for ( size_t  i = 0;  i < _TABLE_SIZE;  ++i)
    {
        typename table_item_t::const_iterator  ti = this->buf[i].begin();
        while ( ti != this->buf[i].end())
        {
            char const *  key = (char const *)ti->key;
            size_t  n_key = cbm::strnlen( key, _KEY_MAXSIZE*sizeof(_V));
            if ( regex_match( _regex, key, n_key, NULL, 0) == static_cast< int >( n_key))
            {
                if ( _key_list)
                {
                    _key_list->names.push_back( key);
                }
// sk:dbg                CBM_LogDebug( "\tregex \"",_regex,"\" matches \"",key,"\"");
                c += 1;
            }
            ++ti;
        }
    }
    return  c;
}


template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::set(
        key_type const *  _key, _V const &  _reg)
{
    return  this->set( _key->name, _reg);
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::set(
        char const *  _key, _V const &  _reg)
{
    _V const  v = _reg;
    return  this->set( _key, &v, 1);
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::set(
        key_type const *  _key, _V const *  _reg, size_t  _n)
{
    return  this->set( _key->name, _reg, _n);
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::set(
        char const *  _key, _V const *  _reg, size_t  _n)
{
    size_t  t_i = this->find_table_position( _key);
    typename  table_item_t::iterator  t_e = this->buf[t_i].begin();
    while ( t_e != this->buf[t_i].end())
    {
        if ( cbm::is_equal( t_e->key, _key))
        {
            this->set_data( &(*t_e), NULL, _reg, _n);
// sk:dbg            CBM_LogError( "SET:  key(existed:replace)=\"",_key,/*"\"  value=",_reg,*/"  t_i=",t_i, "  collision-count=",this->buf[t_i].size());
            return  0;
        }
        ++t_e;
    }

    /* not yet in table */
    hashmap_entry_t  s_val;
    int  rc_cpy = this->set_data( &s_val, _key, _reg, _n);
    if ( !rc_cpy)
    {
// sk:dbg        CBM_LogError( "SET:  key=\"",_key,"\"  value=",_reg,"  t_i=",t_i, "  collision-count=",this->buf[t_i].size());
        this->buf[t_i].push_back( s_val);
        return  0;
    }
    return  -1;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::get_and_remove(
        char const *  _key, _V *  _reg, size_t  _n_reg)
{
    _V const *  data = this->get( _key, _reg, _n_reg);
    if ( data != NULL)
    {
        this->copy_data( _reg, data, _n_reg);
        this->remove( _key);
        return  0;
    }
    return  -1;
}

template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::set_data(
        hashmap_entry_t *  _entry,
        char const *  _key,
        _V const *  _data, size_t  _n)
{
    if ( _key)
    {
        cbm::strncpy( _entry->key, _key, _KEY_MAXSIZE);
        _entry->key[_KEY_MAXSIZE] = '\0';
    }
    return  this->copy_data( _entry->data, _data, _n);
}
template < size_t  _TABLE_SIZE, typename  _V, size_t  _VALUE_MAXSIZE, size_t  _KEY_MAXSIZE >
int
cbm::tinyhashmap_t< _TABLE_SIZE, _V, _VALUE_MAXSIZE, _KEY_MAXSIZE >::copy_data(
        _V *  _dst, _V const *  _src, size_t  _n)
const
{
    if ( _n > _VALUE_MAXSIZE)
    {
        return  -1;
    }
    cbm::mem_cpy( _dst, _src, _n);
    return  0;
}

