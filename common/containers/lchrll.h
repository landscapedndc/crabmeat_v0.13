/*!
 * @brief 
 *    declares wrappers around char lists and stl strings arrays,
 *    where bracket operator returns char list 
 *
 * @author 
 *      steffen klatt
 */

#ifndef  LDNDC_CHAR_LIST_LISTS_WRAP_H_
#define  LDNDC_CHAR_LIST_LISTS_WRAP_H_

#include  "crabmeat-common.h"

namespace  ldndc
{
    struct  charlist_array_base
          { 
        virtual  ~charlist_array_base() = 0;

        virtual  char const *  operator[](
                size_t) const = 0;

        virtual  bool  no_data() const = 0;
    };
    

    template < typename  _C >
    struct  charlist_array  { };


    template <>
    class  charlist_array< char >  :  public  charlist_array_base
           {
        public:
            charlist_array(
                    char const * const *);

            char const *  operator[](
                    size_t) const;

            bool  no_data() const;

        private:
            /* _not_ owner */
            char const * const *  d;
    };


    template <>
    class  charlist_array< std::string >  :  public  charlist_array_base
          {
        public:
            charlist_array(
                    std::string const *);

            char const *  operator[](
                    size_t) const;

            bool  no_data() const;

        private:
            /* _not_ owner */
            std::string const *  d;
    };

    template <>
    class  charlist_array< ldndc_string_t >  :  public  charlist_array_base
          {
        public:
            charlist_array(
                    ldndc_string_t const *);

            char const *  operator[](
                    size_t) const;

            bool  no_data() const;

        private:
            /* _not_ owner */
            ldndc_string_t const *  d;
    };

}  /* end namespace ldndc */


#endif  /*  !LDNDC_CHAR_LIST_LISTS_WRAP_H_  */

