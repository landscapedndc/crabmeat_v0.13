/*!
 * @brief
 *    iterable hash map
 *
 * @author
 *    steffen klatt  (created on: jan 11, 2017)
 */

#include  "containers/cbm_set.h"
#include  "memory/cbm_mem.h"
#include  "memory/cbm_mempool.h"
#include  "memory/cbm_slaba.h"
#include  "nosql/cbm_kvstore.h"

#if defined(__cplusplus)
extern "C" {
#endif

#define  as_setdata(set) static_cast< CBM_SetData * >((set).mem)
CBM_SetNodeIterator const  CBM_SetNodeIteratorEnd = { NULL, NULL };

typedef struct CBM_SetNode
{
// sk:unused    char *  key;
// sk:??  unsigned int  keyhash;
    void *  value;
    int  owner; /*indicates if node is value owner*/
    struct CBM_SetNode *  previous;
    struct CBM_SetNode *  next;
} CBM_SetNode;

typedef struct CBM_SetData
{
    int  size;
    CBM_SetNode *  head;
    CBM_SetNode *  tail;

    CBM_MemCache  nmem;
    CBM_MemCache  vmem;

    cbm::kvstore_t  nodes;
    int  flags;
} CBM_SetData;

CBM_Set CBM_SetCreate( CBM_MemPool  /*mempool*/,
    size_t  _value_size, int  _expected_nb_nodes, int  _flags)
{
    CBM_Set  set;
    set.mem = 0;

    CBM_SetData *  setdata = (CBM_SetData *)CBM_Construct( CBM_SetData);
    if ( !setdata)
        { return  set; }

    setdata->flags = _flags;

    setdata->size = 0;
    setdata->head = NULL;
    setdata->tail = NULL;

    int const  expected_nb_nodes =
        _expected_nb_nodes == 0 ? 32 : _expected_nb_nodes;

    setdata->nmem = CBM_MemCacheCreate( ".set.nodes",
        expected_nb_nodes*sizeof(CBM_SetNode), sizeof(CBM_SetNode), 0, NULL, NULL);
    if ( !CBM_IsCreated(setdata->nmem))
    {
        CBM_Destroy( setdata);
        return  set;
    }

    setdata->vmem = CBM_MemCacheCreate( ".set.values",
        expected_nb_nodes*_value_size, _value_size, 0, NULL, NULL);
    if ( !CBM_IsCreated(setdata->vmem))
    {
        CBM_MemCacheDestroy( setdata->nmem);
        CBM_Destroy( setdata);
        return  set;
    }

    CBM_LogDebug( "Created set  ",
        "[#nodes=",_expected_nb_nodes, ",|value|=", _value_size, "]");
    set.mem = setdata;
    return  set; 
}
void CBM_SetDestroy( CBM_Set  _set)
{
    CBM_SetClear( _set);

    CBM_SetData *  setdata = as_setdata(_set);
    CBM_MemCacheDestroy( setdata->nmem);
    CBM_MemCacheDestroy( setdata->vmem);

    CBM_Destroy( setdata);
}

int CBM_SetSize( CBM_Set  _set)
{
    return  as_setdata(_set)->size;
}
int CBM_SetIsEmpty( CBM_Set  _set)
{
    return  as_setdata(_set)->size == 0 ? 1 : 0;
}

void *  CBM_SetInsert( CBM_Set  _set,
        char const *  _key, void *  _value)
{
    CBM_SetData *  setdata = as_setdata(_set);
    CBM_SetNode *  node = (CBM_SetNode *)CBM_MemCacheAlloc( setdata->nmem, 0);
    if ( !node)
        { return  NULL; }

    if ( _value == NULL)
    {
        /*alloc from memory cache */
        node->value = CBM_MemCacheAlloc( setdata->vmem, 0);
        if ( !node->value)
        {
            CBM_MemCacheFree( setdata->nmem, node);
            return  NULL;
        }
        node->owner = 1;
    }
    else
    {
        node->value = _value;
        node->owner = 0;
    }
    /* append at end of list */
    node->next = NULL;
    if ( setdata->head == NULL)
    {
        CBM_Assert( setdata->size == 0);
        CBM_Assert( setdata->tail == NULL);
        setdata->head = node;
    }
    node->previous = setdata->tail;
    if ( setdata->tail)
        { setdata->tail->next = node; }
    setdata->tail = node;

    setdata->size += 1;
    CBM_LogDebug( "Inserted node '", _key, "'  value=@",(void*)node->value,
        "  ",(void*)node->previous, "<-",(void*)node, "->",(void*)node->next,
        "  head=",(void*)setdata->head, "  tail=",(void*)setdata->tail);

    setdata->nodes.store_pod( _key, (size_t)node);
    return  node->value;
}

static void *  CBM_SetRemoveNode(
    CBM_SetData *  _setdata, CBM_SetNode *  _node)
{
    if ( _node)
    {
        _setdata->size -= 1;
        CBM_Assert( _setdata->size >= 0);

        if ( _node == _setdata->head) /* move head */
            { _setdata->head = _node->next; }
        if ( _node == _setdata->tail) /* move tail */
            { _setdata->tail = _node->previous; }
        if ( _node->previous) /* unlink (left) */
            { _node->previous->next = _node->next; }
        if ( _node->next) /* unlink (right) */
            { _node->next->previous = _node->previous; }

        CBM_LogDebug( "Remove node '?' value=@",(void*)_node->value,
            "  ",(void*)_node->previous, "<-",(void*)_node, "->",(void*)_node->next,
            "  head=",(void*)_setdata->head, "  tail=",(void*)_setdata->tail);


        void *  node_value = _node->value;
        if ( _node->owner)
        {
            node_value = NULL;
            CBM_MemCacheFree( _setdata->vmem, _node->value);
        }
        CBM_MemCacheFree( _setdata->nmem, _node);
// sk:dbg        CBM_LogDebug( "Removed node [",(void*)_node,"] from set");
        return  node_value;
    }
    CBM_Assert(0);
    return  NULL;
}

void *  CBM_SetRemove( CBM_Set  _set, char const *  _key)
{
    CBM_SetData *  setdata = as_setdata(_set);
    CBM_SetNode *  node = NULL;
    setdata->nodes.fetch_and_remove_pod( &node, _key);
    return  CBM_SetRemoveNode( setdata, node);
}

void *  CBM_SetGetValue( CBM_Set  _set, char const *  _key)
{
    CBM_SetNodeIterator  node = CBM_SetFind( _set, _key);
    return  node.value;
}

void CBM_SetClear( CBM_Set  _set)
{
    CBM_SetData *  setdata = as_setdata(_set);
    CBM_SetNode *  node = setdata->head;
    while ( node)
    {
        CBM_SetRemoveNode( setdata, node);
        node = node->next;
    }

    setdata->nodes.clear();
}

CBM_SetNodeIterator  CBM_SetFind( CBM_Set  _set, char const *  _key)
{
    CBM_SetData *  setdata = as_setdata(_set);
    if ( setdata->size == 0)
        { return  CBM_SetNodeIteratorEnd; }

    CBM_SetNode *  node = NULL;
    setdata->nodes.fetch_pod( &node, _key);
    if ( node)
    {
        CBM_SetNodeIterator  i;
        i.value = node->value;
        i.next = node->next;
        return  i;
    }
    return  CBM_SetNodeIteratorEnd;
}
CBM_SetNodeIterator  CBM_SetFindHead( CBM_Set  _set)
{
    CBM_SetData *  setdata = as_setdata(_set);
    if ( setdata->size == 0)
        { return  CBM_SetNodeIteratorEnd; }

// sk:dbg    CBM_LogDebug( "IteratorHead: value=",(void*)setdata->head->value,
// sk:dbg            "  next=",(void*)setdata->head->next);
    CBM_SetNodeIterator  i =
        { setdata->head->value, setdata->head->next };
    return  i;
}
CBM_SetNodeIterator  CBM_SetFindNext( CBM_SetNodeIterator  _iterator)
{
// sk:dbg    CBM_LogDebug( "iterator->next=",_iterator.next);
    if ( _iterator.next != NULL)
    {
        CBM_SetNodeIterator const  i =
            { ((CBM_SetNode*)(_iterator.next))->value,
                ((CBM_SetNode*)(_iterator.next))->next };
        return  i;
    }
    return  CBM_SetNodeIteratorEnd;
}

#if defined(__cplusplus)
}
#endif

