/*!
 * @brief 
 *    bounded stack implementation.
 *
 * @author 
 *      steffen klatt,
 *    edwin haas
 */

#include  "memory/cbm_mem.h"
#include  "utils/lutils.h"

#include  "log/cbm_baselog.h"

template < class  _E >
ldndc::bounded_stack< _E >::bounded_stack(
                size_t  _size,
                element_type const &  _init_val,
                element_type *  _buf) 
                : p_( 0), size_( _size), is_empty_( true),
                  buf_( _buf), owner_( false)
{
        if ( !this->buf_)
        {
                this->buf_ = CBM_DefaultAllocator->construct< element_type >( this->size_);
                cbm::mem_set( this->buf_, this->size_, _init_val);
                this->owner_ = true;
        }
}


template < typename _E >
ldndc::bounded_stack< _E >::bounded_stack(
                bounded_stack< element_type > const &  _bs)
                : p_( _bs.p_), size_( _bs.size_), is_empty_( _bs.is_empty_),
                  buf_( _bs.buf_), owner_( true)
{
        this->buf_ = CBM_DefaultAllocator->construct< element_type >( this->size_);

        cbm::obj_cpy( this->buf_, _bs.buf_, _bs.p_);
}


template < typename _E >
ldndc::bounded_stack< _E > &
ldndc::bounded_stack< _E >::operator=(
                bounded_stack< element_type > const &  _bs)
{
        if ( &_bs != this)
        {

                this->p_ = _bs.p_;
                this->size_ = _bs.size_;
                this->is_empty_ = _bs.is_empty_;

                cbm::obj_cpy( this->buf_, _bs.buf_, _bs.p_);
        }
        return  *this;
}


template < typename _E >
ldndc::bounded_stack< _E >::~bounded_stack< _E >()
{
        if ( this->owner_ && this->buf_)
        {
                CBM_DefaultAllocator->destroy( this->buf_, this->size_);
    }
    this->buf_ = NULL;
    this->size_ = 0;
    this->is_empty_ = true;
    this->p_ = 0;
    this->owner_ = false;
}


template < typename _E >
size_t
ldndc::bounded_stack< _E >::size()
const
{
        return  this->size_;
}


template < typename _E >
size_t
ldndc::bounded_stack< _E >::fillsize()
const
{
    if ( this->is_empty())
    {
        return  0;
    }
    return  this->p_;
}


template < typename _E >
bool
ldndc::bounded_stack< _E >::is_empty()
const
{
        return  this->is_empty_;
}


template < typename _E >
bool
ldndc::bounded_stack< _E >::is_full()
const
{
        return  this->p_ == this->size_;
}

template < typename _E >
void
ldndc::bounded_stack< _E >::clear()
{
        this->p_ = 0;
        this->is_empty_ = true;
}


template < typename _E >
typename ldndc::bounded_stack< _E >::element_type const &
ldndc::bounded_stack< _E >::push(
                element_type const &  _e)
{
        if ( this->p_ < this->size_)
        {
                this->buf_[this->p_] = _e;
                ++this->p_;

                this->is_empty_ = false;

                return  _e;
        }
        CBM_LogFatal( "bounded stack: maximum number of elements reached.");
        return  this->buf_[0];
}

template < typename _E >
typename ldndc::bounded_stack< _E >::element_type const &
ldndc::bounded_stack< _E >::peek()
{
        if ( !this->is_empty_)
        {
                return  this->buf_[this->p_];
        }
        CBM_LogFatal( "bounded stack: attempt to peek at empty stack");
        return  this->buf_[0];
}
template < typename _E >
typename ldndc::bounded_stack< _E >::element_type const &
ldndc::bounded_stack< _E >::pop()
{
        if ( !this->is_empty_)
        {
                --this->p_;
                this->is_empty_ = ( this->p_ == 0);
                return  this->buf_[this->p_];
        }
        CBM_LogFatal( "bounded stack: attempt to pop from empty stack");
        return  this->buf_[0];
}

