/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: apr 20, 2014),
 *    edwin haas
 */

#include  "log/cbm_baselog.h"

template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
bool
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::exists(
        char const *  _key)
const
{
    char const *  v = this->cntr.get( _key);
    if ( v)
    {
        return  true;
    }
    return  false;
}
template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
bool
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::exists(
        char const *  _section,
        char const *  _key)
const
{
    typename  container_t::key_type  key;
    container_t::make_item_key( &key, "%s.%s", _section, _key);
    return  this->exists( key.name);
}

template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
char const *
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::find(
        char const *  _key)
const
{
    char const *  v = this->cntr.get( _key);
    if ( v)
    {
        return  v;
    }
    return  ldndc_empty_cstring;
}
template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
char const *
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::find(
        char const *  _section,
        char const *  _key)
const
{
    typename  container_t::key_type  key;
    container_t::make_item_key( &key, "%s.%s", _section, _key);
    return  this->find( key.name);
}
template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
size_t
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::find_num(
        char const *  _regex_key)
const
{
    return  static_cast< size_t >( this->cntr.get_number_of_entries_matching( _regex_key));
}
template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
size_t
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::find_matching(
        key_list_t *  _key_list,
        char const *  _regex_key)
const
{
    return  this->cntr.get_keys_matching( _key_list, _regex_key);
}

template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
int
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::make_key(
        key_type *  _key, char const *  _format, ...)
{
    va_list  p;
    va_start( p, _format);
    int const  keysize = container_t::vmake_item_key( _key, _format, p);
    va_end( p);

    return  keysize;
}

template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
int
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::insert(
        char const *  _section,
        char const *  _key, char const *  _value)
{
    if ( !_key || cbm::is_empty( _key) || !_value)
    {
        CBM_LogError( "keyvaluebase_t::insert(): ",
                "invalid key or value  ",
                "[key=",(_key?_key:"<NULL>"),",value=",(_value?_value:"<NULL>"),"]");
        return  -1;
    }

    int  key_size = 0;
    typename container_t::key_type  key;
    if ( _section && !cbm::is_empty( _section))
    {
        key_size = this->cntr.make_item_key( &key, "%s.%s", _section, _key);
// sk:dbg        CBM_LogDebug( "section=",_section, "  key=",_key, "  key'=",key, "  data=",_value);
    }
    else
    {
        key_size = this->cntr.make_item_key( &key, "%s", _key);
    }
    if ( key_size < 1)
    {
        CBM_LogError( "keyvaluebase_t::insert(): ",
                "invalid key  [key=",_key,"]");
        return  -1;
    }

//    CBM_LogDebug( "key=",_key, "  key'=",key, "  data=",_value, "  |data|=",cbm::strlen( _value), "  |max|=",VALUE_MAXSIZE);
    size_t const  value_size = cbm::strlen( _value);
    if ( value_size > container_t::VALUE_MAXSIZE)
    {
        CBM_LogError( "keyvaluebase_t::insert(): ",
                "value size too large  [size=",value_size,",max=",(int)container_t::VALUE_MAXSIZE,"]");
        return  -2;
    }
    return  this->cntr.set( &key, _value, value_size+1/*null byte*/);
}

template < size_t  _TBL_SZ, size_t  _KEY_SZ, size_t  _VALUE_SZ >
void
cbm::keyvaluebase_t< _TBL_SZ, _KEY_SZ, _VALUE_SZ >::dump()
const
{
    CBM_LogWarn( "TODO");
}

