/*!
 * @brief
 *    defines not-a-number constants
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 *    edwin haas
 */

#ifndef  CRABMEAT_GLOBAL_NAN_H_
#define  CRABMEAT_GLOBAL_NAN_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"
#include  <stddef.h>

extern CBM_API signed char const  invalid_schr;
extern CBM_API unsigned char const  invalid_uchr;
extern CBM_API signed short int const  invalid_sint;
extern CBM_API unsigned short int const  invalid_suint;
extern CBM_API signed int const  invalid_int;
extern CBM_API unsigned int const  invalid_uint;
extern CBM_API signed long int const  invalid_lint;
extern CBM_API unsigned long int const  invalid_luint;
extern CBM_API signed long long int const  invalid_llint;
extern CBM_API unsigned long long int const  invalid_lluint;

extern CBM_API size_t const  invalid_size;

extern CBM_API double const  invalid_dbl;
extern CBM_API float const  invalid_flt;

extern CBM_API char const  invalid_chr;
extern CBM_API char const *  invalid_str;
#define  invalid_string  std::string( invalid_str)


/* template versions: */

#define  LDNDC_INVALID_VAL_DECL(__type__,__value__) \
    namespace ldndc{ template < > \
    struct  CBM_API  invalid_t< __type__ > \
    { \
        static  __type__  const  value; \
    };}

#include  <string>
namespace  ldndc
{
    template < typename  _T >
    struct  invalid_t
    {
        static  _T const  value;
    };

    /* bool has no invalid values */
    template < >
    struct  CBM_API  invalid_t< bool >
    {
        /* nothing here */
    };
} /* namespace ldndc */

LDNDC_INVALID_VAL_DECL(signed char,invalid_schr)
LDNDC_INVALID_VAL_DECL(unsigned char,invalid_uchr)
LDNDC_INVALID_VAL_DECL(signed short int,invalid_sint)
LDNDC_INVALID_VAL_DECL(unsigned short int,invalid_suint)
LDNDC_INVALID_VAL_DECL(signed int,invalid_int)
LDNDC_INVALID_VAL_DECL(unsigned int,invalid_uint)
LDNDC_INVALID_VAL_DECL(signed long int,invalid_lint)
LDNDC_INVALID_VAL_DECL(unsigned long int,invalid_luint)
LDNDC_INVALID_VAL_DECL(signed long long int,invalid_llint)
LDNDC_INVALID_VAL_DECL(unsigned long long int,invalid_lluint)

LDNDC_INVALID_VAL_DECL(float,invalid_flt)
LDNDC_INVALID_VAL_DECL(double,invalid_dbl)

LDNDC_INVALID_VAL_DECL(char,invalid_chr)
LDNDC_INVALID_VAL_DECL(std::string,invalid_str)

#endif  /*  !CRABMEAT_GLOBAL_NAN_H_  */

