/*!
 * @brief
 *    key/value store wrapper
 *
 * @author 
 *    steffen klatt (created on: dec 11, 2014)
 */

#ifndef  LDNDC_NOSQL_KV_H_ 
#define  LDNDC_NOSQL_KV_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"

struct  unqlite;
struct  unqlite_kv_cursor;

namespace  cbm
{
class CBM_API kvstore_t
{
    template < typename _T >
    struct  db_serializer_t
    {
        db_serializer_t() {}
        db_serializer_t( _T const &  _value)
            { cbm::mem_cpy< _T >( &this->value, &_value, 1); }
        static size_t const  n_bytes = sizeof(_T);
        union
        {
            _T  value;
            char  repr[sizeof(_T)];
        };
    };
    public:
        kvstore_t();
        ~kvstore_t();

        bool  is_connected() const { return  this->m_db != NULL; }

        lerr_t  store(
                char const * /*key*/, ldndc_int64_t /*key length*/,
                char const * /*value*/, ldndc_int64_t /*value length*/);
        template < typename _T >
        lerr_t  store_pod(
                char const *  _key, _T const &  _value)
        {
            db_serializer_t< _T >  db_serializer( _value);
            return  this->store(
                    _key, -1, db_serializer.repr, db_serializer.n_bytes);
        }
        lerr_t  store(
                char const *  _key, char const *  _value)
        {
            return  this->store( _key, -1, _value, -1);
        }
        lerr_t  store(
                char const *  _key, std::string const &  _value)
        {
            return  this->store( _key, -1, _value.c_str(), _value.length());
        }
        lerr_t  store(
                char const *  _key, cbm::string_t const &  _value)
        {
            return  this->store( _key, -1, _value.c_str(), _value.length());
        }
        lerr_t  store(
                cbm::string_t const &  _key, cbm::string_t const &  _value)
        {
            return  this->store( _key.c_str(), _key.length(),
                                    _value.c_str(), _value.length());
        }

        lerr_t  append(
                char const * /*key*/, ldndc_int64_t /*key length*/,
                char const * /*value*/, ldndc_int64_t /*value length*/,
                char const * = NULL /*delimiter*/);

        lerr_t  remove(
                char const * /*key*/, ldndc_int64_t = -1 /*key length*/);
        lerr_t  remove(
                cbm::string_t const &  _key)
        {
            return  this->remove( _key.c_str(), _key.length());
        }
        void  clear();

        lerr_t  length(
                char const * /*key*/, ldndc_int64_t /*key length*/,
                ldndc_int64_t * /*value length*/);

        lerr_t  fetch(
                char const * /*key*/, ldndc_int64_t /*key length*/,
                char * /*value*/, ldndc_int64_t /*value length*/) const;
// sk:off        lerr_t  fetch(
// sk:off                cbm::string_t * /*value*/,
// sk:off                char const * /*key*/, ldndc_int64_t = -1 /*key length*/) const;

        template < typename _T >
        lerr_t  fetch_pod(
                _T *  _reg,
                char const *  _key, ldndc_int64_t  _key_length = -1)
        const
        {
            db_serializer_t< _T >  db_serializer;
            lerr_t  rc_fetch = this->fetch(
                    _key, _key_length, db_serializer.repr, db_serializer.n_bytes);
            if ( rc_fetch == LDNDC_ERR_OK)
            {
                if ( _reg) { *_reg = db_serializer.value; }
            }
            return  rc_fetch;
        }

// sk:off        template < typename _T >
// sk:off        lerr_t  fetch_as_array(
// sk:off                typename cbm::string_t::array_t< _T >::type_t *  _arr,
// sk:off                char const *  _key, ldndc_int64_t  _key_length = -1,
// sk:off                char  _delim = ' ')
// sk:off        const
// sk:off        {
// sk:off            cbm::string_t  val;
// sk:off            lerr_t  rc_fetch = this->fetch( &val, _key, _key_length);
// sk:off            if ( rc_fetch == LDNDC_ERR_OK)
// sk:off            {
// sk:off                if ( _arr) { *_arr = val.as_array< _T >( _delim); }
// sk:off            }
// sk:off            return  rc_fetch;
// sk:off        }

        template < typename _T >
        lerr_t  fetch_and_remove_pod(
                _T *  _reg, char const *  _key)
        {
            lerr_t  rc_fetch = this->fetch_pod< _T >( _reg, _key);
            if ( rc_fetch == LDNDC_ERR_OK)
            {
                this->remove( _key);
            }
            return  rc_fetch;
        }
        template < typename _T >
        lerr_t  fetch_and_remove_pod(
                _T *  _reg, char const *  _key, _T const &  _default)
        {
            lerr_t  rc_fetch_remove =
                this->fetch_and_remove_pod< _T >( _reg, _key);
            if ( rc_fetch_remove == LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
            {
                *_reg = _default;
            }
            return  rc_fetch_remove;
        }

        void  dump() const;

        class iterator
        {
        public:
            iterator( unqlite *);
            ~iterator();

            bool next();
            char const *  operator*() const;
            bool  is_valid() const;

        private:
            unqlite * m_db;
            unqlite_kv_cursor *  m_iter;

            char *  m_key;
            int  m_keysize;
        };
        iterator  begin()
            { return iterator( this->m_db); }

    private:
        unqlite *  m_db;
};

struct CBM_API kvstore_shutdown_t
{
    ~kvstore_shutdown_t();
};
extern kvstore_shutdown_t  kvstore_shutdown;

} /* namespace cbm */

#endif  /*  LDNDC_NOSQL_KV_H_  */

