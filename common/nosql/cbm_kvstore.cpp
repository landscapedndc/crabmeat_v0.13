/*!
 * @brief
 *    key/value store wrapper implementation
 *
 * @author 
 *    steffen klatt (created on: dec 11, 2014)
 */

#include  "nosql/cbm_kvstore.h"
#include  "memory/cbm_mem.h"

#include  <unqlite.h>

cbm::kvstore_t::kvstore_t()
    : m_db( NULL)
{
    unqlite_open( &this->m_db, ":mem:", UNQLITE_OPEN_IN_MEMORY);
}

cbm::kvstore_t::~kvstore_t()
{
    if ( this->m_db)
        { unqlite_close( this->m_db); }
}


lerr_t
cbm::kvstore_t::store(
        char const *  _key, ldndc_int64_t  _key_length,
        char const *  _value, ldndc_int64_t  _value_length)
{
    if ( !_key || ( _key_length < -1) || !_value)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  key_length =
            static_cast< unqlite_int64 >( _key_length);
    if ( key_length == -1)
    {
        key_length = cbm::strlen( _key);
    }
    if ( key_length == 0)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  value_length =
            static_cast< unqlite_int64 >( _value_length);
    if ( value_length == -1)
    {
        value_length = cbm::strlen( _value);
    }

    int  rc_store = unqlite_kv_store( this->m_db,
        _key, key_length, _value, value_length);
    if ( rc_store != UNQLITE_OK)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kvstore_t::append(
        char const *  _key, ldndc_int64_t  _key_length,
        char const *  _value, ldndc_int64_t  _value_length,
        char const *  _delimiter)
{
    if ( !_key || ( _key_length < -1) || !_value)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  key_length =
            static_cast< unqlite_int64 >( _key_length);
    if ( key_length == -1)
    {
        key_length = cbm::strlen( _key);
    }
    if ( key_length == 0)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  value_length =
            static_cast< unqlite_int64 >( _value_length);
    if ( value_length == -1)
    {
        value_length = cbm::strlen( _value);
    }

    int  rc_append = UNQLITE_OK;
    if ( _delimiter && ( *_delimiter != '\0'))
    {
        ldndc_int64_t  appvalue_length = 0;
        this->length( _key, key_length, &appvalue_length);
        if ( appvalue_length > 0)
        {
            int  delimiter_length = cbm::strlen( _delimiter);
            rc_append = unqlite_kv_append( this->m_db,
                _key, key_length, _delimiter, delimiter_length);
            if ( rc_append != UNQLITE_OK)
            {
                return  LDNDC_ERR_FAIL;
            }
        }
    }

    rc_append = unqlite_kv_append( this->m_db,
            _key, key_length, _value, value_length);
    if ( rc_append != UNQLITE_OK)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kvstore_t::remove(
        char const *  _key, ldndc_int64_t  _key_length)
{
    if ( !_key || ( _key_length < -1))
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  key_length =
            static_cast< unqlite_int64 >( _key_length);
    if ( key_length == -1)
    {
        key_length = cbm::strlen( _key);
    }
    if ( key_length == 0)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    int const  rc_delete = unqlite_kv_delete(
            this->m_db, _key, key_length);
    if ( rc_delete == UNQLITE_NOTFOUND)
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    else if ( rc_delete != UNQLITE_OK)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kvstore_t::length(
        char const *  _key, ldndc_int64_t  _key_length,
        ldndc_int64_t *  _value_length)
{
    if ( !_key || ( _key_length < -1) || !_value_length)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  key_length =
            static_cast< unqlite_int64 >( _key_length);
    if ( key_length == -1)
    {
        key_length = cbm::strlen( _key);
    }
    if ( key_length == 0)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  value_length = -1;
    int  rc_fetch_len = unqlite_kv_fetch( this->m_db,
            _key, key_length, NULL, &value_length);
    if ( rc_fetch_len == UNQLITE_NOTFOUND)
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    else if ( rc_fetch_len != UNQLITE_OK)
    {
        return  LDNDC_ERR_FAIL;
    }
    *_value_length =
            static_cast< ldndc_int64_t >( value_length);
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kvstore_t::fetch(
        char const *  _key, ldndc_int64_t  _key_length,
        char *  _value, ldndc_int64_t  _value_length)
const
{
    if ( !_key)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  key_length =
            static_cast< unqlite_int64 >( _key_length);
    if ( key_length == -1)
    {
        key_length = cbm::strlen( _key);
    }
    if ( key_length == 0)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    unqlite_int64  value_length =
            static_cast< unqlite_int64 >( _value_length);
    if ( _value && ( value_length < 1))
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    int  rc_fetch = unqlite_kv_fetch( this->m_db,
            _key, key_length, _value, &value_length);
    if ( rc_fetch == UNQLITE_NOTFOUND)
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    else if ( rc_fetch != UNQLITE_OK)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

void
cbm::kvstore_t::clear()
{
    unqlite_kv_cursor *  cur = NULL;
    unqlite_kv_cursor_init( this->m_db, &cur);
    if ( !cur)
        { return; }

    int  rc_cur = unqlite_kv_cursor_first_entry( cur);
    if ( rc_cur == UNQLITE_OK)
    {
        while ( unqlite_kv_cursor_valid_entry( cur))
        {
            unqlite_kv_cursor_delete_entry( cur);
            unqlite_kv_cursor_next_entry( cur);
        }
    }
}

#include  "string/cbm_string.h"
#include  <stdio.h>
static int s_DumpKeyCallback( void const *  _data,
                unsigned int  _data_len, void *  _userdata)
{
    char *  data = NULL;
    if ( _data && ( _data_len > 0))
    {
        data = CBM_DefaultAllocator->allocate_type< char >( _data_len+1);
        if ( data)
        {
            cbm::snprintf( data, _data_len+1, "%s", (char const *)_data);
            fprintf( stdout, "key[%d]=%s\n", *(int *)_userdata, data);

            CBM_DefaultAllocator->deallocate( data);
        }
    }
    else
    {
        fprintf( stdout, "key[%d]=\n", *(int *)_userdata);
    }
    return  0;
}

void
cbm::kvstore_t::dump()
const
{
    unqlite_kv_cursor *  cur = NULL;
    unqlite_kv_cursor_init( this->m_db, &cur);
    if ( !cur)
        { return; }

    int  rc_cur = unqlite_kv_cursor_first_entry( cur);
    if ( rc_cur == UNQLITE_OK)
    {
        int  k = 0;
        while ( unqlite_kv_cursor_valid_entry( cur))
        {
            ++k;

            unqlite_kv_cursor_key_callback( cur, s_DumpKeyCallback, &k);
            unqlite_kv_cursor_next_entry( cur);
        }
    }
}

cbm::kvstore_t::iterator::iterator( unqlite *  _db)
    : m_db( _db), m_iter( NULL), m_key( NULL), m_keysize( 0)
{
    unqlite_kv_cursor_init( this->m_db, &this->m_iter);
}
cbm::kvstore_t::iterator::~iterator()
{
    if ( this->m_iter)
        { unqlite_kv_cursor_release( this->m_db, this->m_iter); }
    if ( this->m_key)
        { delete[]  this->m_key; }
}

bool
cbm::kvstore_t::iterator::next()
{
    unqlite_kv_cursor_next_entry( this->m_iter);
    if ( unqlite_kv_cursor_valid_entry( this->m_iter))
    {
        int  keysize;
        unqlite_kv_cursor_key( this->m_iter, NULL, &keysize);
        if ( !this->m_key || keysize < this->m_keysize)
        {
            delete[]  this->m_key;
            this->m_key = new char[keysize+1];
        }
        if ( this->m_key)
        {
            this->m_keysize = keysize;
            unqlite_kv_cursor_key( this->m_iter, this->m_key, &this->m_keysize);
            this->m_key[keysize] = '\0';
        }
        else
            { this->m_keysize = 0; }

        return  true;
    }
    return  false;
}
char const *
cbm::kvstore_t::iterator::operator*() const
{
    if ( unqlite_kv_cursor_valid_entry( this->m_iter) && this->m_key)
        { return  this->m_key; }
    return  NULL;
}
bool
cbm::kvstore_t::iterator::is_valid() const
    { return this->operator*() != NULL; }


cbm::kvstore_shutdown_t  cbm::kvstore_shutdown;
cbm::kvstore_shutdown_t::~kvstore_shutdown_t()
{
    unqlite_lib_shutdown();
}

