/*!
 * @brief
 *    class that gives derived class an ID
 *
 * @author
 *    steffen klatt (created on: apr 16, 2013),
 *    edwin haas
 */

#ifndef  CBM_OBJECT_H_
#define  CBM_OBJECT_H_

#include  "crabmeat-common.h"

#include  "cbm_id.h"
#include  "memory/cbm_mem.h"

namespace  cbm
{
#define  LDNDC_ERROR_STACK_SIZE  10
struct  CBM_API  object_status_t
{
    object_status_t()// sk:off : err_desc( NULL)
    {
        state.ok = 1;
    }
// sk:off        int  err_cntr;
// sk:off        int  last_error;
// sk:off        lerr_t  err_stack[LDNDC_ERROR_STACK_SIZE];

// sk:off        char const *  err_desc;
    struct  status_t
    {
        bool  ok:1;
// sk:off            bool  init:1;
    } state;
};

class  CBM_API  object_t  :  public  cbm::objectid_owner_t,  public  cbm::object_status_t
{
public:
    object_t()
        : cbm::objectid_owner_t(),
        cbm::object_status_t()
        { /* nothing here */ }
    object_t( lid_t const &  _id)
        : cbm::objectid_owner_t( _id),
        cbm::object_status_t()
        { /* nothing here */ }

public:
    virtual  void  delete_instance() = 0;

// sk:??                private:
// sk:??                        int  ref_cntr_;

// sk:??        protected:
// sk:??            object_status_t  obj_status_;
};

class  CBM_API  server_object_t  :  public  object_t
{
public:
    server_object_t() { }
    server_object_t( lid_t const &  _id)
        : cbm::object_t( _id) { }
};

class  CBM_API  client_object_t  :  public  object_t
{
public:
    client_object_t() { }
    client_object_t( lid_t const &  _id)
        : cbm::object_t( _id) { }
};

}  /*  namespace cbm  */
namespace ldndc {
    typedef cbm::object_t  object_t;
    typedef cbm::client_object_t  client_object_t;
} /* namespace cbm */

#define  LDNDC_OBJECT(__type__)                                                 \
    public:                                                                     \
        static  __type__ *  allocate_instance()                                 \
        {                                                                       \
            return  cbm::allocatable_t< __type__ >::allocate_instance();    \
        }                                                                       \
        static __type__ *  new_instance()                                       \
        {                                                                       \
            return  cbm::allocatable_t< __type__ >::new_instance();         \
        }                                                                       \
        static  __type__ *  new_instance(                                       \
                lid_t const &  _object_id)                                      \
        {                                                                       \
            return  cbm::allocatable_t< __type__ >::new_instance( _object_id); \
        }                                                                       \
/* sk:?? */                                                                     \
        virtual __type__ *  clone_instance()                                    \
        const                                                                   \
        {                                                                       \
            /*CBM_LogFatal( "cloning "#__type__" objects not implemented");*/       \
            __type__ *  this_clone = NULL/*__type__::new_instance( this->object_id())*/;\
            if ( this_clone)                                                    \
            {                                                                   \
                /* most of the assignment operators are still private... */     \
/* sk:off                *this_clone = *this; */                                \
            }                                                                   \
            return  this_clone;                                                 \
        }                                                                       \
                                                                                \
        virtual void  delete_instance()                                         \
        {                                                                       \
            cbm::allocatable_t< __type__ >::delete_instance( this);         \
        }                                                                       \
        virtual void  delete_instance()                                         \
        const                                                                   \
        {                                                                       \
            const_cast< __type__ * >( this)->delete_instance();                 \
        }                                                                       \
    private:

#define  LDNDC_OBJECT_DEFN(__type__)

/* server objects */
#define  LDNDC_SERVER_OBJECT(__type__)  LDNDC_OBJECT(__type__)
#define  LDNDC_SERVER_OBJECT_DEFN(__type__)  LDNDC_OBJECT_DEFN(__type__)
/* client objects */
#define  LDNDC_CLIENT_OBJECT(__type__)  LDNDC_OBJECT(__type__)
#define  LDNDC_CLIENT_OBJECT_DEFN(__type__)  LDNDC_OBJECT_DEFN(__type__)


#endif  /*  !CBM_OBJECT_H_  */

