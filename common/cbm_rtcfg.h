/*!
 * @brief
 *    declares library configuration structure
 *
 * @author 
 *    steffen klatt (created on: sep. 10, 2013),
 *    edwin haas
 */

#ifndef  CRABMEAT_GLOBAL_RUNTIMECONFIG_H_
#define  CRABMEAT_GLOBAL_RUNTIMECONFIG_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "string/cbm_string.h"

#include  "cpl/cbm_cpl.h"

namespace ldndc {
    class project_file_t;
    class config_file_t;
}

namespace cbm {
    class sclock_t;
    struct kernelfactorystore_t;

/* TODO  LDNDC_ stuff.. :-( */
/* environment variables */
#define  LDNDC_ENV_DOT_PATH  "LDNDC_DOT_PATH"
#define  LDNDC_DOT_PATH  "~/.ldndc/"            /*in home directory*/
    extern CBM_API char const *  DOT_PATH;

#define  LDNDC_ENV_INPUT_PATH  "LDNDC_INPUT_PATH"
#define  LDNDC_INPUT_PATH  "."
    extern CBM_API char const *  INPUT_PATH;

#define  LDNDC_ENV_OUTPUT_PATH  "LDNDC_OUTPUT_PATH"
#define  LDNDC_OUTPUT_PATH  "."
    extern CBM_API char const *  OUTPUT_PATH;

#define  LDNDC_ENV_RESOURCES_PATH  "LDNDC_RESOURCES_PATH"
#define  LDNDC_RESOURCES_PATH  LDNDC_DOT_PATH
    extern CBM_API char const *  RESOURCES_PATH;
#define  LDNDC_ENV_RESOURCES_FILENAME  "LDNDC_RESOURCES_FILENAME"
#define  LDNDC_RESOURCES_FILENAME  "Lresources"
    extern CBM_API char const *  RESOURCES_FILENAME;

#define  LDNDC_ENV_DEFAULT_LOGFILE  "LDNDC_DEFAULT_LOGFILE"
#define  LDNDC_LOG_FILE  "%O/%oldndc.log"
    extern CBM_API char const *  LOG_FILE;

#define  LDNDC_CONFIG_FILENAME  "ldndc.conf"
    extern CBM_API char const *  CONFIG_FILENAME;
#define  LDNDC_CONFIG_FILE LDNDC_DOT_PATH LDNDC_CONFIG_FILENAME
    extern CBM_API char const *  CONFIG_FILE;

#define  LDNDC_PROJECT_FILE  "%I/project.xml"
    extern CBM_API char const *  PROJECT_FILE;
#define  LDNDC_ENV_SCENARIO  "LDNDC_SCENARIO"

    extern CBM_API double const  BALANCE_TOLERANCE;


    /* client binary package information */
    class CBM_API package_version_t
    {
        char const *  m_name;
        char const *  m_version_long;
        char const *  m_version;
        public:
            package_version_t();

            char const *  name() const;
            void  set_name( char const * /*name*/);
            char const *  version_long() const;
            void  set_version_long( char const * /*version long*/);
            char const *  version() const;
            void  set_version( char const * /*version*/);
    };
}

/* package user object */
namespace cbm {
    extern CBM_API  package_version_t &  CBM_PackageUser(); }
#define  package_user cbm::CBM_PackageUser()


namespace  cbm
{
    struct CBM_API  libconfig_t
    {
        int  status;
        /* simulation project meta info related configuration settings */
        struct CBM_API  metainfo_t
        {
            std::string  project_name;
            std::string  project_filename;
            std::string  global_source_prefix;
            std::string  global_sink_prefix;

            std::string  scenario_name;
        };
        metainfo_t  pc;

        /* general library related configuration settings */
        struct CBM_API  control_t
        {
            double  math_epsilon;
            int  random_seed;
        };
        control_t  cc;

        /* logging related configuration settings */
        struct CBM_API  logging_t
        {
            int  log_append;
            int  log_buffersize;
        };
        logging_t  lc;

        /* resource monitoring related configuration settings */
        struct CBM_API  resource_monitoring_t
        {
            int  rank;
            long int  pid;
            int  measure_time;

            std::string  ipaddr;
            int  ipaddr_version;
            int  port;

            int  coupler_statistics;
        };
        resource_monitoring_t  rc;

        /* kernel related configuration settings */
        struct CBM_API  kernels_t
        {
            int  balance_check;
            int  balance_tolerance;
        };
        kernels_t  kc;

        /* signal related configuration settings */
        struct CBM_API  signals_t
        {
            int  current_kernel[CRABMEAT_OPENMP_MAXTHREADS];
            int  request_term;
            int  clean_term;
        };
        signals_t  sc;

        /* i/o related configuration settings */
        struct CBM_API  io_t
        {
            std::string  dot_path;

            std::string  current_working_path;

            std::string  input_path;
            std::string  output_path;

            std::string  resources_path;
            std::string  udunits_path;
            
            std::string  service_registry_url;

            bool  have_output;

            bool  create_checkpoint;
            std::string  create_date;
            bool  restore_checkpoint;
            std::string  restore_date;

			std::string  default_sink_format;
        };
        io_t  io;

        /* access to configuration */
        ldndc::config_file_t const *  cfg;
        /* access to project file */
        ldndc::project_file_t const *  prj;
        /* global simulation clock */
        cbm::sclock_t *  clk;
        /* kernel factories */
        cbm::kernelfactorystore_t *  kfactory;
        /* coupler */
        CBM_Coupler  cpl;

        libconfig_t();

        void  set_dot_path();
        void  set_current_working_path();

        void  dump_processed_kernels() const;
        void  dump_clock() const;
        std::string  now_as_iso8601() const;
    };
} /* namespace cbm */

namespace cbm {
    extern CBM_API  libconfig_t &  LibRuntimeConfig(); }
#define CBM_LibRuntimeConfig  cbm::LibRuntimeConfig()

#endif  /*  !CRABMEAT_GLOBAL_RUNTIMECONFIG_H_  */

