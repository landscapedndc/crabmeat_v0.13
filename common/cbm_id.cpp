/*!
 * @brief
 *    object ID
 *
 * @author 
 *    steffen klatt  (created on: jul 08, 2015)
 */

#include  "crabmeat-common.h"
#include  "cbm_id.h"
#include  "cbm_nan.h"

lid_t const  invalid_lid = ldndc::invalid_t< lid_t >::value;

cbm::objectid_owner_interface_t::~objectid_owner_interface_t()
    { }



cbm::objectid_owner_t::objectid_owner_t()
                : cbm::objectid_owner_interface_t(),
          m_objectid( invalid_lid)
    { }

cbm::objectid_owner_t::objectid_owner_t(
                lid_t const &  _object_id)
                : cbm::objectid_owner_interface_t(),
          m_objectid( _object_id)
    { }


cbm::objectid_owner_t::~objectid_owner_t()
    { }


lid_t const &
cbm::objectid_owner_t::object_id() const
    { return  this->m_objectid; }

void
cbm::objectid_owner_t::set_object_id(
        lid_t const &  _object_id)
    { this->m_objectid = _object_id; }

#include  "string/cbm_string.h"
int
cbm::set_source_descriptor(
        cbm::source_descriptor_t *  _source_info,
        lid_t const &  _id,
        char const *  _source_identifier)
{
    if ( !_source_info)
    {
        return  -1;
    }
    _source_info->block_id = _id;
    int  rc_cpy = 0;
    if ( _source_identifier)
    {
        rc_cpy = cbm::as_strcpy( &_source_info->source_identifier, _source_identifier);
    }

    return  rc_cpy;
}
void
cbm::set_source_descriptor_defaults(
        cbm::source_descriptor_t *  _source_info,
        char const *  _iclass,
        lid_t const &  _id)
{
    cbm::set_source_descriptor( _source_info, _id, _iclass);
}

std::ostream &
cbm::operator<<(
        std::ostream &  _out,
        cbm::source_descriptor_t const &  _si)
{
    _out << '[' << _si.source_identifier << ':' << _si.block_id << ']';
    return  _out;
}


