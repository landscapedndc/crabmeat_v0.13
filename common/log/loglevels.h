/*!
 * @brief
 *    defines logging levels and their names
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 */

#ifndef  LDNDC_LOG_LEVELS_H_
#define  LDNDC_LOG_LEVELS_H_

#include  "crabmeat-common.h"

/*!
 * @brief
 *    supported logging levels, sorted by verbosity (ascending)
 *
 * @note
 *    these levels have to be supported in configuration file and
 *    logging interface.
 */
enum  llog_level_e
{
    LOGLEVEL_SILENT,
    LOGLEVEL_ERROR,
    LOGLEVEL_WARN,
    LOGLEVEL_INFO,
    LOGLEVEL_VERBOSE,
#ifdef  _DEBUG
    LOGLEVEL_DEBUG,
#endif

    LOGLEVEL_CNT,
	LOGLEVEL_MAX = LOGLEVEL_CNT - 1
};
extern CBM_API char const *  LOGLEVEL_NAMES[LOGLEVEL_CNT+1];

extern CBM_API char const *  LOGLEVEL_PREFIX[LOGLEVEL_CNT+1];
#ifdef  _HAVE_ASCII_COLOR
extern CBM_API char const *  LOGLEVEL_PREFIX_COLOR[LOGLEVEL_CNT+1];
#else
/* if we do not have color support, just make this an alias for non-colored prefixes */
extern CBM_API char const **  LOGLEVEL_PREFIX_COLOR;
#endif

extern CBM_API unsigned int CBM_LogLevelFromName(
    char const * /*name*/, llog_level_e const & /*fall back*/);

#endif  /*  !LDNDC_LOG_LEVELS_H_  */

