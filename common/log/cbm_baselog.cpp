/*!
 * @brief
 *  contains the implementation of the logging methods.
 *
 * @author
 *  steffen klatt (created on: oct. 30, 2016)
 */

#include  "log/cbm_baselog.h"
#ifdef  CRABMEAT_OPENMP
#  include  <omp.h>
#endif

#ifdef  CRABMEAT_DEBUG
unsigned int  cbm::baselogger_t::log_level = LOGLEVEL_DEFAULT;
#else
unsigned int  cbm::baselogger_t::log_level = LOGLEVEL_ERROR;
#endif
int  cbm::baselogger_t::color = 0;
int  cbm::baselogger_t::terminate_on_fatal_error = 1;
int  cbm::baselogger_t::prepend_filename = 1;
char const **  cbm::baselogger_t::log_level_prefix = LOGLEVEL_PREFIX;

#include  <stdio.h>
void
cbm::baselogger_t::m_logmessage( std::string const &  _msg,
    unsigned int  _msg_loglevel, char const *  _msg_type)
{
    if ( _msg_loglevel < log_level)
    {
#ifdef  CRABMEAT_OPENMP
#  pragma omp critical (CBM_BaseLogger)
#endif
        {
            if ( _msg_type && ( _msg_type[0] != '\0'))
            {
                fprintf( stderr, "[%s] ", _msg_type);
            }
            fprintf( stderr, "%s", _msg.c_str());
        }
    }
}



/** logging stream interface **/

void
cbm::baselogger_t::log_write(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, 0u, _prefix);
}


void
cbm::baselogger_t::log_info(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, LOGLEVEL_INFO-1, _prefix);
}


void
cbm::baselogger_t::log_verbose(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, LOGLEVEL_VERBOSE-1, _prefix);
}


void
cbm::baselogger_t::log_warn(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, LOGLEVEL_WARN-1, _prefix);
}


void
cbm::baselogger_t::log_error(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, LOGLEVEL_ERROR-1, _prefix);
}


#ifdef  CRABMEAT_PROVIDES_BACKTRACE
#  include  "utils/signal/cbm_signal.h"
#endif
#include  <stdlib.h>
void
cbm::baselogger_t::log_fatal(
    std::string const &  _msg, char const *  _prefix)
{
#ifdef  CRABMEAT_OPENMP
#  pragma omp critical (CBM_BaseLogger)
#endif
    {
    if ( terminate_on_fatal_error)
    {
        m_logmessage( "\n======================================================\n\n", 0, NULL);
        m_logmessage( "\noops :-(\n\n\tCrabMEAT encountered an unrecoverable error\n"
                    "\tand will shut down. The error message follows:\n\n", 0, NULL);
    }
    m_logmessage( _msg, 0, _prefix);
    if ( terminate_on_fatal_error)
    {
        m_logmessage( "\n======================================================\n\n", 0, NULL);
    }
    }
    if ( terminate_on_fatal_error)
    {
#ifdef  CRABMEAT_PROVIDES_BACKTRACE
        unsigned char  cs_size = 0;
        char **  log_callstack = retrieve_backtrace( 10, &cs_size);
        dump_backtrace( log_callstack, cs_size);
#endif
        ::exit( LDNDC_ERR_RUNTIME_ERROR);
    }
}


#ifdef  _DEBUG
void
cbm::baselogger_t::log_debug(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, LOGLEVEL_DEBUG-1, _prefix);
}

void
cbm::baselogger_t::log_todo(
    std::string const &  _msg, char const *  _prefix)
{
    m_logmessage( _msg, LOGLEVEL_MAX, _prefix);
}
#endif

#include  <string.h>
char const *
cbm::baselogger_t::format_filename(
        char const *  _filename)
{
    char const *  fn = strrchr( _filename, '/');
    if ( fn)
        {   return  fn + 1; }
    return  _filename;
}

void
cbm::baselogger_t::set_color( int  _have_color)
{
    color = 0;
    if ( _have_color)
        { color = 1; }

    if ( color)
        { log_level_prefix = LOGLEVEL_PREFIX_COLOR; }
}

