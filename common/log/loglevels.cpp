/*!
 * @brief
 *    defines logging levels and their names (implementation)
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 */

#include  "log/loglevels.h"

char const *  LOGLEVEL_NAMES[LOGLEVEL_CNT+1] =
{
    "silent", "error", "warn", "info", "verbose",
#ifdef  _DEBUG
    /*first debug level ->*/ "debug",
#endif
    /* sentinel */
    NULL
};

char const *  LOGLEVEL_PREFIX[LOGLEVEL_CNT+1] = { "  ", "EE", "WW", "II", "II",
#ifdef  _DEBUG
    "DD",
#endif
    "??"};
#ifdef  _HAVE_ASCII_COLOR
char const *  LOGLEVEL_PREFIX_COLOR[LOGLEVEL_CNT+1] = 
    { "  ", "\033[00;31;1mEE\033[00m", "\033[00;33;1mWW\033[00m", "\033[00;32;1mII\033[00m", "\033[00;32;1mII\033[00m",
#  ifdef  _DEBUG
    "\033[00;30;1mDD\033[00m",
#  endif
    "\033[00;36;1m??\033[00m"};
#else
char const **  LOGLEVEL_PREFIX_COLOR = (char const **)LOGLEVEL_PREFIX;
#endif

#include "string/cbm_string.h"
unsigned int  CBM_LogLevelFromName(
        char const *  _name, llog_level_e const &  _use_if_invalid)
{
    if ( !_name)
        { return _use_if_invalid; }

    unsigned int const  nb_loglevel = LOGLEVEL_CNT;
    for ( unsigned int  l = 0; l < nb_loglevel; ++l)
    {
        if ( cbm::is_equal( _name, LOGLEVEL_NAMES[l]))
            { return l; }
    }
    return _use_if_invalid;
}

