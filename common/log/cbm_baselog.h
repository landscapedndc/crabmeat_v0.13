/*!
 * @brief
 *  provides logging facility for crabmeat
 *
 * @author
 *  steffen klatt (created on: oct. 30, 2016)
 */

#ifndef  CRABMEAT_BASELOG_H_
#define  CRABMEAT_BASELOG_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "log/loglevels.h"

#include  <string>
#include  <sstream>

/*!
 * brief
 *      custom made assertion
 *
 * @see
 *      http://www.devarticles.com/c/a/Cplusplus/C-plus-plus-Preprocessor-Always-Assert-Your-Code-Is-Right
 */
#ifdef  CRABMEAT_DEBUG
#include  <assert.h>
#  define  __crabmeat_assert(__condition__,...) \
    if ( ! ( __condition__)) \
    { \
        CBM_LogError( __VA_ARGS__,"\"" #__condition__ "\""); \
        assert(__condition__); \
    }

#  ifdef  CRABMEAT_HAVE_ASSERT
#    define  crabmeat_assert(__condition__)            \
        __crabmeat_assert(__condition__,"assertion failed:")
#  else
#    define  crabmeat_assert(__unused__)      CRABMEAT_NOOP
#  endif  /*  CRABMEAT_HAVE_ASSERT  */
#else
#  define  crabmeat_assert(__unused__)     CRABMEAT_NOOP
#endif  /*  CRABMEAT_DEBUG  */
/* alias */
#define  CBM_Assert(__condition__)  crabmeat_assert(__condition__)

namespace cbm {

struct CBM_API baselogger_t
{
    /*!
     * @brief
     *    used to write informational messages
     */
    static void  log_write(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_write_prefix()
        { return  "  "; }

    /*!
     * @brief
     *    used to write informational messages
     */
    static void  log_info(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_info_prefix()
        { return  log_level_prefix[LOGLEVEL_INFO]; }

    /*!
     * @brief
     *    used to write more informational messages
     */
    static void  log_verbose(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_verbose_prefix()
        { return  log_level_prefix[LOGLEVEL_VERBOSE]; }


#ifdef  CRABMEAT_DEBUG
    /*!
     * @brief
     *    used to write debug level messages
     */
    static void  log_debug(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_debug_prefix()
        { return  log_level_prefix[LOGLEVEL_DEBUG]; }
    /*!
     * @brief 
     *    used to write messages into the log file
     *    when executing code that is tagged as
     *    incomplete.
     */
    static void  log_todo(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_todo_prefix()
        { return  "TT"; }
#endif

    /*!
     * @brief 
     *    used to write warning level messages
     */
    static void  log_warn(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_warn_prefix()
        { return  log_level_prefix[LOGLEVEL_WARN]; }
    /*!
     * @brief 
     *    used to write error level messages
     */
    static void  log_error(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_error_prefix()
        { return  log_level_prefix[LOGLEVEL_ERROR]; }
    /*!
     * @brief 
     *    writes fatal level messages to log stream
     *    and throws run time exception
     */
    static void  log_fatal(
        std::string const & /*message*/, char const * = NULL);
    static char const *  log_fatal_prefix()
        { return  "FF"; }

    /*! @brief
     *    strip leading directories from filename */
    static char const *  format_filename(
            char const * /*filename*/);


    /*! @brief 
     *     holds the current log level */
    static unsigned int  log_level;
    /*! @brief
     *    non-zero if colors should be used */
    static int  color;
    static void  set_color( int);
    /*! @brief
     *    non-zero if abort() is called on fatal errors */
    static int  terminate_on_fatal_error;
    /*! @brief
     *    non-zero if filename is printed */
    static int  prepend_filename;


//    int  messages_count(
//            llog_level_e = LOGLEVEL_CNT) const;

    static char const **  log_level_prefix;

    /* does the actual write-work */
    static void  m_logmessage(
        std::string const & /*logging message*/,
        unsigned int /*logging level*/,
        char const * /*logging message type mnemonic*/);
};

} /* namespace cbm */

#define  CBM_SetLogLevel(__loglevel__) cbm::baselogger_t::log_level = __loglevel__

#define  priv_CBM_LogTypeID(__type__) \
        cbm::baselogger_t::log_ ## __type__ ## _prefix()

/* creating single logging message from various arguments */
#define  priv_CBM_LogLine_(__type__,...) \
    priv_CBM_Log ## __type__(NULL,0,priv_CBM_LogTypeID(__type__),"",__VA_ARGS__)
#ifdef  CRABMEAT_DEBUG
#  define  priv_CBM_LogLine(__type__,...) \
    priv_CBM_Log ## __type__(__FILE__,__LINE__,priv_CBM_LogTypeID(__type__),"",__VA_ARGS__)
#else
#  define  priv_CBM_LogLine(__type__,...) \
    priv_CBM_Log ## __type__(NULL,0,priv_CBM_LogTypeID(__type__),"",__VA_ARGS__)
#endif

/* logging functions (using default logger)
 *    - _TO versions accept alternative logger object
 *    - underscore versions omit source file and line information
 */
#define  CBM_LogWrite(...)  priv_CBM_LogLine(write,__VA_ARGS__)
#define  CBM_LogWrite_(...)  priv_CBM_LogLine_(write,__VA_ARGS__)
#define  CBM_LogInfo(...)  priv_CBM_LogLine(info,__VA_ARGS__)
#define  CBM_LogInfo_(...)  priv_CBM_LogLine_(info,__VA_ARGS__)
#define  CBM_LogVerbose(...)  priv_CBM_LogLine(verbose,__VA_ARGS__)
#define  CBM_LogVerbose_(...)  priv_CBM_LogLine_(verbose,__VA_ARGS__)
#define  CBM_LogWarn(...)  priv_CBM_LogLine(warn,__VA_ARGS__)
#define  CBM_LogWarn_(...)  priv_CBM_LogLine_(warn,__VA_ARGS__)
#define  CBM_LogError(...)  priv_CBM_LogLine(error,__VA_ARGS__)
#define  CBM_LogError_(...)  priv_CBM_LogLine_(error,__VA_ARGS__)
#define  CBM_LogFatal(...)  priv_CBM_LogLine(fatal,__VA_ARGS__)
#define  CBM_LogFatal_(...)  priv_CBM_LogLine_(fatal,__VA_ARGS__)
#ifdef  CRABMEAT_DEBUG
#  define  CBM_LogDebug(...)  priv_CBM_LogLine(debug,__VA_ARGS__);
#  define  CBM_LogDebug_(...)  priv_CBM_LogLine_(debug,__VA_ARGS__);

#  define  CBM_LogTodo(...)  CBM_LogWarn("TODO")
#  define  CBM_LogNoImpl(__id__)  CBM_LogWarn("missing implementation... (",__id__,")")
#else
#  define  CBM_LogDebug(...)  CRABMEAT_NOOP
#  define  CBM_LogDebug_(...)  CRABMEAT_NOOP

#  define  CBM_LogTodo(...)   CRABMEAT_NOOP
#  define  CBM_LogNoImpl(__id__)  CRABMEAT_NOOP
#endif  /*  CRABMEAT_DEBUG  */


/* following noise allows CBM_Log{Write,Info,Debug,...} to be used like
 * print in python for a list of n arguments (n is the highest value
 * seen below (e.g. see 'typename _Tn')
 */

/* maximum number of supported arguments to logging functions */
#define  priv_CBM_LogArgMax  24

/* helpers */
#define  priv_CBM_LogGlue(__e1__,__e2__) __e1__##__e2__

/* expand to "typename _T1, typename _T2, ..., typename _T<priv_CBM_LogArgMax>" */
#define  priv_CBM_LogTemplateArg_1  typename _T1
#define  priv_CBM_LogTemplateArg_2  priv_CBM_LogTemplateArg_1, typename _T2
#define  priv_CBM_LogTemplateArg_3  priv_CBM_LogTemplateArg_2, typename _T3
#define  priv_CBM_LogTemplateArg_4  priv_CBM_LogTemplateArg_3, typename _T4
#define  priv_CBM_LogTemplateArg_5  priv_CBM_LogTemplateArg_4, typename _T5
#define  priv_CBM_LogTemplateArg_6  priv_CBM_LogTemplateArg_5, typename _T6
#define  priv_CBM_LogTemplateArg_7  priv_CBM_LogTemplateArg_6, typename _T7
#define  priv_CBM_LogTemplateArg_8  priv_CBM_LogTemplateArg_7, typename _T8
#define  priv_CBM_LogTemplateArg_9  priv_CBM_LogTemplateArg_8, typename _T9
#define  priv_CBM_LogTemplateArg_10  priv_CBM_LogTemplateArg_9, typename _T10
#define  priv_CBM_LogTemplateArg_11  priv_CBM_LogTemplateArg_10, typename _T11
#define  priv_CBM_LogTemplateArg_12  priv_CBM_LogTemplateArg_11, typename _T12
#define  priv_CBM_LogTemplateArg_13  priv_CBM_LogTemplateArg_12, typename _T13
#define  priv_CBM_LogTemplateArg_14  priv_CBM_LogTemplateArg_13, typename _T14
#define  priv_CBM_LogTemplateArg_15  priv_CBM_LogTemplateArg_14, typename _T15
#define  priv_CBM_LogTemplateArg_16  priv_CBM_LogTemplateArg_15, typename _T16
#define  priv_CBM_LogTemplateArg_17  priv_CBM_LogTemplateArg_16, typename _T17
#define  priv_CBM_LogTemplateArg_18  priv_CBM_LogTemplateArg_17, typename _T18
#define  priv_CBM_LogTemplateArg_19  priv_CBM_LogTemplateArg_18, typename _T19
#define  priv_CBM_LogTemplateArg_20  priv_CBM_LogTemplateArg_19, typename _T20
#define  priv_CBM_LogTemplateArg_21  priv_CBM_LogTemplateArg_20, typename _T21
#define  priv_CBM_LogTemplateArg_22  priv_CBM_LogTemplateArg_21, typename _T22
#define  priv_CBM_LogTemplateArg_23  priv_CBM_LogTemplateArg_22, typename _T23
#define  priv_CBM_LogTemplateArg_24  priv_CBM_LogTemplateArg_23, typename _T24

/* expand to "[...] char const *  _delim, _T1 const &  _m1, ..., _T16 const &  _m<priv_CBM_LogArgMax>" */
#define  priv_CBM_LogDeclArg_1  char const *  _filename, size_t  _lineno, char const *  _prefix, char const *  _delim, _T1 const &  _m1
#define  priv_CBM_LogDeclArg_2  priv_CBM_LogDeclArg_1, _T2 const &  _m2
#define  priv_CBM_LogDeclArg_3  priv_CBM_LogDeclArg_2, _T3 const &  _m3
#define  priv_CBM_LogDeclArg_4  priv_CBM_LogDeclArg_3, _T4 const &  _m4
#define  priv_CBM_LogDeclArg_5  priv_CBM_LogDeclArg_4, _T5 const &  _m5
#define  priv_CBM_LogDeclArg_6  priv_CBM_LogDeclArg_5, _T6 const &  _m6
#define  priv_CBM_LogDeclArg_7  priv_CBM_LogDeclArg_6, _T7 const &  _m7
#define  priv_CBM_LogDeclArg_8  priv_CBM_LogDeclArg_7, _T8 const &  _m8
#define  priv_CBM_LogDeclArg_9  priv_CBM_LogDeclArg_8, _T9 const &  _m9
#define  priv_CBM_LogDeclArg_10  priv_CBM_LogDeclArg_9, _T10 const &  _m10
#define  priv_CBM_LogDeclArg_11  priv_CBM_LogDeclArg_10, _T11 const &  _m11
#define  priv_CBM_LogDeclArg_12  priv_CBM_LogDeclArg_11, _T12 const &  _m12
#define  priv_CBM_LogDeclArg_13  priv_CBM_LogDeclArg_12, _T13 const &  _m13
#define  priv_CBM_LogDeclArg_14  priv_CBM_LogDeclArg_13, _T14 const &  _m14
#define  priv_CBM_LogDeclArg_15  priv_CBM_LogDeclArg_14, _T15 const &  _m15
#define  priv_CBM_LogDeclArg_16  priv_CBM_LogDeclArg_15, _T16 const &  _m16
#define  priv_CBM_LogDeclArg_17  priv_CBM_LogDeclArg_16, _T17 const &  _m17
#define  priv_CBM_LogDeclArg_18  priv_CBM_LogDeclArg_17, _T18 const &  _m18
#define  priv_CBM_LogDeclArg_19  priv_CBM_LogDeclArg_18, _T19 const &  _m19
#define  priv_CBM_LogDeclArg_20  priv_CBM_LogDeclArg_19, _T20 const &  _m20
#define  priv_CBM_LogDeclArg_21  priv_CBM_LogDeclArg_20, _T21 const &  _m21
#define  priv_CBM_LogDeclArg_22  priv_CBM_LogDeclArg_21, _T22 const &  _m22
#define  priv_CBM_LogDeclArg_23  priv_CBM_LogDeclArg_22, _T23 const &  _m23
#define  priv_CBM_LogDeclArg_24  priv_CBM_LogDeclArg_23, _T24 const &  _m24

/* expand to "[...] m << _m1 << _m2 << ... << _m16" */
#define  priv_CBM_LogConcat_1(__not_used__) \
    std::ostringstream  m; \
    int  brace_open = 0; \
    if ( cbm::baselogger_t::prepend_filename && _filename) \
    { \
        m << '{'; \
        brace_open=1; \
        m << cbm::baselogger_t::format_filename( _filename) << " +" << _lineno; \
    } \
    if ( brace_open) { m << "} "; } \
    m << _m1
#define  priv_CBM_LogConcat_2(__delim__)  priv_CBM_LogConcat_1(__delim__) << (__delim__) << _m2
#define  priv_CBM_LogConcat_3(__delim__)  priv_CBM_LogConcat_2(__delim__) << (__delim__) << _m3
#define  priv_CBM_LogConcat_4(__delim__)  priv_CBM_LogConcat_3(__delim__) << (__delim__) << _m4
#define  priv_CBM_LogConcat_5(__delim__)  priv_CBM_LogConcat_4(__delim__) << (__delim__) << _m5
#define  priv_CBM_LogConcat_6(__delim__)  priv_CBM_LogConcat_5(__delim__) << (__delim__) << _m6
#define  priv_CBM_LogConcat_7(__delim__)  priv_CBM_LogConcat_6(__delim__) << (__delim__) << _m7
#define  priv_CBM_LogConcat_8(__delim__)  priv_CBM_LogConcat_7(__delim__) << (__delim__) << _m8
#define  priv_CBM_LogConcat_9(__delim__)  priv_CBM_LogConcat_8(__delim__) << (__delim__) << _m9
#define  priv_CBM_LogConcat_10(__delim__)  priv_CBM_LogConcat_9(__delim__) << (__delim__) << _m10
#define  priv_CBM_LogConcat_11(__delim__)  priv_CBM_LogConcat_10(__delim__) << (__delim__) << _m11
#define  priv_CBM_LogConcat_12(__delim__)  priv_CBM_LogConcat_11(__delim__) << (__delim__) << _m12
#define  priv_CBM_LogConcat_13(__delim__)  priv_CBM_LogConcat_12(__delim__) << (__delim__) << _m13
#define  priv_CBM_LogConcat_14(__delim__)  priv_CBM_LogConcat_13(__delim__) << (__delim__) << _m14
#define  priv_CBM_LogConcat_15(__delim__)  priv_CBM_LogConcat_14(__delim__) << (__delim__) << _m15
#define  priv_CBM_LogConcat_16(__delim__)  priv_CBM_LogConcat_15(__delim__) << (__delim__) << _m16
#define  priv_CBM_LogConcat_17(__delim__)  priv_CBM_LogConcat_16(__delim__) << (__delim__) << _m17
#define  priv_CBM_LogConcat_18(__delim__)  priv_CBM_LogConcat_17(__delim__) << (__delim__) << _m18
#define  priv_CBM_LogConcat_19(__delim__)  priv_CBM_LogConcat_18(__delim__) << (__delim__) << _m19
#define  priv_CBM_LogConcat_20(__delim__)  priv_CBM_LogConcat_19(__delim__) << (__delim__) << _m20
#define  priv_CBM_LogConcat_21(__delim__)  priv_CBM_LogConcat_20(__delim__) << (__delim__) << _m21
#define  priv_CBM_LogConcat_22(__delim__)  priv_CBM_LogConcat_21(__delim__) << (__delim__) << _m22
#define  priv_CBM_LogConcat_23(__delim__)  priv_CBM_LogConcat_22(__delim__) << (__delim__) << _m23
#define  priv_CBM_LogConcat_24(__delim__)  priv_CBM_LogConcat_23(__delim__) << (__delim__) << _m24


#include  "crabmeat-config.h"
template < int _N >
struct  priv_CBM_LogFail
{
#ifndef  CRABMEAT_COMPILER_CLANG
    /* if you get caught here, you used up all
     * available log_<type> interfaces
     *
     * llvm's clang front-end always evaluates this
     * line, which makes it choke here.
     */
    char  invalid_array[priv_CBM_LogArgMax-(_N+1)];
#endif
};
#define  priv_CBM_LogDefnTrap_(__type__)  priv_CBM_LogDefnTrap__(priv_CBM_LogArgMax,__type__)
#define  priv_CBM_LogDefnTrap__(__n__,__type__) \
    template < priv_CBM_LogGlue(priv_CBM_LogTemplateArg_,__n__), typename _Tx > \
    void  priv_CBM_Log ## __type__( \
        priv_CBM_LogGlue(priv_CBM_LogDeclArg_,__n__),_Tx,...) \
    { \
        /* fires at point of instantiation */ \
        priv_CBM_LogFail<__n__>  log_##__type__##__has_too_many_arguments; \
        CRABMEAT_FIX_UNUSED(log_##__type__##__has_too_many_arguments); \
        CRABMEAT_FIX_UNUSED(_prefix); \
        priv_CBM_LogGlue(priv_CBM_LogConcat_,__n__)(_delim); \
        CRABMEAT_FIX_UNUSED(m); \
    }

#define  priv_CBM_LogDefnN_(__n__,__type__) \
    template < priv_CBM_LogGlue(priv_CBM_LogTemplateArg_,__n__) > \
    void  priv_CBM_Log ## __type__( \
        priv_CBM_LogGlue(priv_CBM_LogDeclArg_,__n__)) \
    { \
        CRABMEAT_FIX_UNUSED(_delim); /* for __n__==1 */ \
        priv_CBM_LogGlue(priv_CBM_LogConcat_,__n__)(_delim) << '\n'; \
        cbm::baselogger_t::log_ ## __type__( m.str(), _prefix); \
    }

#define  priv_CBM_LogDefnNRel(__n__)  priv_CBM_LogDefnN_(__n__,write) priv_CBM_LogDefnN_(__n__,info) priv_CBM_LogDefnN_(__n__,verbose) priv_CBM_LogDefnN_(__n__,warn) priv_CBM_LogDefnN_(__n__,error) priv_CBM_LogDefnN_(__n__,fatal)
#define  priv_CBM_LogDefnTrapsRel       priv_CBM_LogDefnTrap_(write) priv_CBM_LogDefnTrap_(info) priv_CBM_LogDefnTrap_(verbose) priv_CBM_LogDefnTrap_(warn) priv_CBM_LogDefnTrap_(error) priv_CBM_LogDefnTrap_(fatal)/*;*/
#ifdef  CRABMEAT_DEBUG
#  define  priv_CBM_LogDefnN(__n__) priv_CBM_LogDefnNRel(__n__) priv_CBM_LogDefnN_(__n__,debug) priv_CBM_LogDefnN_(__n__,todo)
#  define  priv_CBM_LogDefnTraps    priv_CBM_LogDefnTrapsRel priv_CBM_LogDefnTrap_(debug) priv_CBM_LogDefnTrap_(todo)/*;*/
#else
#  define  priv_CBM_LogDefnN(__n__) priv_CBM_LogDefnNRel(__n__)
#  define  priv_CBM_LogDefnTraps    priv_CBM_LogDefnTrapsRel
#endif

/* pull it all together */
#define  priv_CBM_LogDefn  priv_CBM_LogDefnN(1)  priv_CBM_LogDefnN(2)  priv_CBM_LogDefnN(3)  priv_CBM_LogDefnN(4)  priv_CBM_LogDefnN(5)  priv_CBM_LogDefnN(6)  priv_CBM_LogDefnN(7)  priv_CBM_LogDefnN(8)  priv_CBM_LogDefnN(9)  priv_CBM_LogDefnN(10)  priv_CBM_LogDefnN(11)  priv_CBM_LogDefnN(12)  priv_CBM_LogDefnN(13)  priv_CBM_LogDefnN(14)  priv_CBM_LogDefnN(15)  priv_CBM_LogDefnN(16)  priv_CBM_LogDefnN(17)  priv_CBM_LogDefnN(18)  priv_CBM_LogDefnN(19)  priv_CBM_LogDefnN(20)  priv_CBM_LogDefnN(21)  priv_CBM_LogDefnN(22)  priv_CBM_LogDefnN(23)  priv_CBM_LogDefnN(24)  priv_CBM_LogDefnTraps


/* define all available logging methods */
priv_CBM_LogDefn

#endif  /*  !CRABMEAT_BASELOG_H_  */

