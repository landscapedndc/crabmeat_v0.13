/*!
 * @brief
 *    defines various error codes
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012)
 */

#ifndef  CRABMEAT_GLOBAL_ERROR_H_
#define  CRABMEAT_GLOBAL_ERROR_H_


/*! quick return from function without error message */
#define  RETURN_IF_NOT_OK(__reg__)                      \
{                                                       \
    lerr_t const  __RETURN_IF_NOT_OK__value__ =         \
    static_cast< lerr_t >( __reg__);                    \
    if ((__RETURN_IF_NOT_OK__value__) != LDNDC_ERR_OK)  \
        { return  __RETURN_IF_NOT_OK__value__; }        \
}


/* various error/status codes */
enum  lerror_e
{
    /* OK is guaranteed to be 0! */
    LDNDC_ERR_OK = 0,


    LDNDC_ERR_NONE,

    LDNDC_ERR_READER_OPEN_FAILED,
    LDNDC_ERR_READER_LOAD_FAILED,
    LDNDC_ERR_READER_CLOSE_FAILED,

    LDNDC_ERR_INVALID_ID,
    LDNDC_ERR_INVALID_ARGUMENT,
    LDNDC_ERR_INVALID_OPERATION,
    LDNDC_ERR_ATTRIBUTE_NOT_FOUND,

    LDNDC_ERR_OBJECT_CREATE_FAILED,
    LDNDC_ERR_OBJECT_CONFIG_FAILED,
    LDNDC_ERR_OBJECT_INIT_FAILED,
    LDNDC_ERR_OBJECT_INIT_INCOMPLETE,
    LDNDC_ERR_OBJECT_DOES_NOT_EXIST,

    LDNDC_ERR_FILE_OPEN_FAILED,

    LDNDC_ERR_NOMEM,

    LDNDC_ERR_INPUT_EXHAUSTED,
    LDNDC_ERR_CONT_ITERATION,
    LDNDC_ERR_STOP_ITERATION,
    LDNDC_ERR_REQUEST_MISMATCH,

    LDNDC_ERR_EVENT_EMPTY_QUEUE,
    LDNDC_ERR_EVENT_MATCH,

    LDNDC_ERR_RUN_INCOMPLETE,

    LDNDC_ERR_CAUGHT_SIGNAL,
    LDNDC_ERR_FPE,

    LDNDC_ERR_RUNTIME_ERROR,
    LDNDC_ERR_FAIL,

    LDNDC_ERR_CODES_CNT
};
typedef  enum lerror_e   lerr_t;


#endif  /*  !CRABMEAT_GLOBAL_ERROR_H_  */

