/*!
 * @brief
 *    helper class that signals base class <-> subclass relation
 *    by enum value, i.e. at compile time.
 *
 * @note
 *      http://stackoverflow.com/questions/1996703/specializing-a-template-by-a-template-base-class
 */

#ifndef  CBM_MATCHBASE_H_
#define  CBM_MATCHBASE_H_

namespace cbm {

/*!
 * @brief
 *    helper struct to determine if class
 *    _D has base class _B
 */
template < typename  _B /*base*/, typename  _D /*derived*/>
class  match_base_t
{
    struct  has_base_no_t
    {
    };
    struct  has_base_yes_t
    {
        has_base_no_t  no[3];
    }; 

    static  has_base_yes_t  test_( _B const *); /* no implementation */

    static  has_base_no_t  test_( ...); /* no implementation */

        public:
                enum
                {
                        is_match = sizeof( test_( static_cast< _D const * >( 0))) == sizeof( has_base_yes_t)
                }; 

};

} /* namespace cbm */

#endif  /*  !CBM_MATCHBASE_H_  */

