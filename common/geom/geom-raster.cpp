/*!
 * @brief
 *     raster geometry structure
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#include  "geom/geom-raster.h"


size_t
cbm::geom_shape_container_t::size()
const
{
        return  this->shapes_.size();
}

size_t
cbm::geom_shape_container_t::insert_shape(
                cbm::geom_shape_t const *  _shape)
{
        crabmeat_assert( _shape);
        this->shapes_.push_back( *_shape);
        return  this->size() - 1;
}
cbm::geom_shape_t *
cbm::geom_shape_container_t::shape_at(
                size_t  _index)
const
{
        if ( _index < this->size())
        {
                return  this->shapes_[_index];
        }
        return  NULL;
}



cbm::geom_raster_t::geom_raster_t()
                : rasterize_size_( 0.0),
                  is_rasterized_( 0)
{
}

int
cbm::geom_raster_t::raster_from_shapes(
                geom_shape_container_t const *  _shapes,
                geom_dim_t  _raster_size)
{
        int const  r_x = _raster_size.n_x;
        int const  r_y = _raster_size.n_y;

        geom_shape_container_t const *  shapes = _shapes;
        cbm::geom_shape_t const *  shape = NULL;

        for ( size_t  s = 0;  s < shapes->size();  ++s)
        {

        }
}
int
cbm::geom_raster_t::size_x()
const
{
        return  this->dim_.n_x;
}
int
cbm::geom_raster_t::size_y()
const
{
        return  this->dim_.n_y;
}

void
cbm::geom_raster_t::upperleft(
                geom_coord_t *  _upperleft)
const
{
        *_upperleft = this->upperleft_;
}
void
cbm::geom_raster_t::lowerright(
                geom_raster_t *  _lowerright)
const
{
        *_lowerright = this->lowerright_;
}

