/*!
 * @brief
 *     raster geometry structure
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  CBM_GEOM_RASTER_H_
#define  CBM_GEOM_RASTER_H_

#include  "crabmeat-common.h"
#include  "geom/geom.h"

#include  <list>
#include  <vector>

namespace cbm {

class  CBM_API  geom_shape_container_t
{
public:
    size_t  size() const;

    size_t  insert_shape( cbm::geom_shape_t const *);
    cbm::geom_shape_t const *  shape_at(
            size_t /*index*/) const;

private:
    typedef  std::vector< cbm::geom_shape_t >  shape_buffer_t;
    shape_buffer_t  shapes_;
};

class  CBM_API  geom_raster_t
{
public:
    bool  is_rasterized() { return  this->is_rasterized_ == 1; }
    int  from_shapes(
            geom_shape_container_t const * /*shapes*/,
            geom_dim_t /*result size*/);

    int  size_x() const;
    int  size_y() const;

    void  upperleft( geom_coord_t *);
    void  lowerright( geom_coord_t *);

private:
    geom_dim_t  dim_;

    geom_coord_t  upperleft_;
    geom_coord_t  lowerright_;

    int  is_rasterized_;

    typedef  std::list< geom_shape_container_t >  raster_t;
    raster_t  raster_;
};

} /* namespace cbm */

#endif  /*  !CBM_GEOM_RASTER_H_  */

