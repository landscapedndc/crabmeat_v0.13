/*!
 * @brief
 *     geographic orientations
 *
 * @author
 *    steffen klatt (created on: feb 11, 2015)
 */

#include  "geom/geom-orientation.h"
#include  "log/cbm_baselog.h"

char const *  cbm::GEO_ORIENTATION_NAMES[cbm::ORIENT_CNT] =
{
    "none",

    "north",
    "northeast",
    "east",
    "southeast",
    "south",
    "southwest",
    "west",
    "northwest"
};

lerr_t
cbm::geo_orientation_t::mooreboundaries_from_string(
        cbm::string_t const &  _boundaries,
        cbm::geo_orientations_t *  _orientations)
{
    if ( _boundaries == invalid_string)
    {
        if ( _orientations ) { *_orientations = ORIENT_NONE; }
    }
    else
    {
        cbm::string_t  boundaries = _boundaries.strip( " ,");
        cbm::string_t::string_array_t  given_orientations =
            boundaries.as_string_array( ',');

        if (( given_orientations.size() == 0) && ( boundaries.length() != 0))
        {
            if ( _orientations)
            {
                *_orientations = ORIENT_INVALID;
                CBM_LogError( "error when parsing Moore-boundaries list",
                  "  [moore-boundaries=",_boundaries,"]");
            }
            return  LDNDC_ERR_FAIL;
        }

        for ( size_t  o = 0;  o < given_orientations.size();  ++o)
        {
            cbm::geo_orientation_e  dir =
                cbm::geo_orientation_t::as_enum( given_orientations[o].c_str());
            if ( dir == ORIENT_INVALID)
            {
                if ( _orientations)
                {
                    *_orientations = ORIENT_INVALID;
                    CBM_LogError( "invalid geographic orientation for Moore boundary",
                       "  [moore-boundaries=",_boundaries,",orientation=",given_orientations[o],"]");
                }
                return  LDNDC_ERR_FAIL;
            }
            if ( _orientations) { *_orientations |= dir; }
        }
    }
    return  LDNDC_ERR_OK;
}

