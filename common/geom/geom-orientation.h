/*!
 * @brief
 *     geographic orientations
 *
 * @author
 *    steffen klatt (created on: feb 11, 2015)
 */

#ifndef  CBM_GEOGRAPHICORIENTATIONS_H_
#define  CBM_GEOGRAPHICORIENTATIONS_H_

#include  "crabmeat-common.h"

namespace cbm {

enum  geo_orientation_e
{
    ORIENT_NONE = 0,
    
    ORIENT_NORTH = 1,
    ORIENT_NORTHEAST = 2,
    ORIENT_EAST = 4,
    ORIENT_SOUTHEAST = 8,
    ORIENT_SOUTH = 16,
    ORIENT_SOUTHWEST = 32,
    ORIENT_WEST = 64,
    ORIENT_NORTHWEST = 128,

    ORIENT_CNT = 9,
    ORIENT_INVALID = -1,
    ORIENT_ANY = ORIENT_NORTH|ORIENT_NORTHEAST|ORIENT_EAST|ORIENT_SOUTHEAST|ORIENT_SOUTH|ORIENT_SOUTHWEST|ORIENT_WEST|ORIENT_NORTHWEST
};
typedef  int  geo_orientations_t;

extern CBM_API char const *  GEO_ORIENTATION_NAMES[ORIENT_CNT];
} /* namespace cbm */

#include  "string/cbm_string.h"
namespace cbm {
struct CBM_API geo_orientation_t
{
    static  geo_orientation_e  as_enum( char const * _name)
    {
        geo_orientation_e  orientation = ORIENT_INVALID;
        if ( cbm::is_equal_i( _name, "none"))
        {
            orientation = ORIENT_NONE;
        }
        else if ( cbm::is_equal_i( _name, "north"))
        {
            orientation = ORIENT_NORTH;
        }
        else if ( cbm::is_equal_i( _name, "northeast"))
        {
            orientation = ORIENT_NORTHEAST;
        }
        else if ( cbm::is_equal_i( _name, "east"))
        {
            orientation = ORIENT_EAST;
        }
        else if ( cbm::is_equal_i( _name, "southeast"))
        {
            orientation = ORIENT_SOUTHEAST;
        }
        else if ( cbm::is_equal_i( _name, "south"))
        {
            orientation = ORIENT_SOUTH;
        }
        else if ( cbm::is_equal_i( _name, "southwest"))
        {
            orientation = ORIENT_SOUTHWEST;
        }
        else if ( cbm::is_equal_i( _name, "west"))
        {
            orientation = ORIENT_WEST;
        }
        else if ( cbm::is_equal_i( _name, "northwest"))
        {
            orientation = ORIENT_NORTHWEST;
        }
        else
        {
            orientation = ORIENT_INVALID;
        }
        return  orientation;
    }

    static  lerr_t  mooreboundaries_from_string(
            cbm::string_t const &,
            cbm::geo_orientations_t *);
};
} /* namespace cbm */

#endif  /*  !CBM_GEOGRAPHICORIENTATIONS_H_  */

