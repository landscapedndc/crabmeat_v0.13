/*!
 * @brief
 *     geometry related types
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_GEOM_H_
#define  LDNDC_GEOM_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"

/*
 *     z
 *      ^
 *      |
 *      |   .
 *      |    (x,y,z)
 *      |
 *      +---------------> x
 *     /
 *    /
 *   y
 *
 *  3d coordinate system, with point (x,y,z)
 */

namespace cbm {

struct  CBM_API  geom_coord_t
{
    typedef  double  coord_t;
    geom_coord_t();
    geom_coord_t( coord_t, coord_t, coord_t = 0.0);

    bool  is_origin() const;

    bool  operator==( geom_coord_t const &) const;
    bool  operator!=( geom_coord_t const &) const;

    coord_t  x;
    coord_t  y;
    coord_t  z;
};
std::ostream &
operator<<( std::ostream &, geom_coord_t const &);

struct  CBM_API  geom_dim_t
{
    geom_dim_t();
    geom_dim_t( int /*x*/, int /*y*/, int = 1 /*z*/);
    size_t  size()
    const
    {
        if ( cbm::is_invalid( this->n_x)
            || cbm::is_invalid( this->n_y)
            || cbm::is_invalid( this->n_z))
                { return  0; }
        return  this->n_x * this->n_y * this->n_z;
    }

    int  n_x;
    int  n_y;
    int  n_z;
};

struct  geom_shape_t;
struct  CBM_API  geom_bounding_box_t
{
    typedef  double  coord_t;
    geom_bounding_box_t();

    coord_t  dx() const
        { return  this->lr.x-this->ul.x; }
    coord_t  dy() const
        { return  this->lr.y-this->ul.y; }
    coord_t  dz() const
        { return  this->ul.z-this->lr.z; }

    void  init( geom_coord_t const &);
    void  update( geom_coord_t const &);
    void  init( geom_shape_t const &);
    void  update( geom_shape_t const &);
    void  merge( geom_bounding_box_t const &);

    bool  contains( geom_bounding_box_t const &) const;

    std::string  to_string() const;

    /* upper left */
    geom_coord_t  ul;
    /* lower right */
    geom_coord_t  lr;
};

struct  CBM_API  geom_mapinfo_t
{
    geom_mapinfo_t();
    bool  is_valid() const;
    size_t  size() const
        { return  this->dim.size(); }

    geom_bounding_box_t const &  get_bounding_box()
            const { return  this->bb; }

    geom_bounding_box_t  bb;
    geom_dim_t  dim;

    double  extent_x;
    double  extent_y;

    size_t  parts;
};

struct  CBM_API  geom_shape_t
{
    geom_shape_t();
    geom_shape_t( size_t /*vertices*/);
    ~geom_shape_t();

    geom_shape_t( geom_shape_t const &);
    geom_shape_t &  operator=( geom_shape_t const &);

    int  copy_vertices( geom_shape_t const &);
    void  shift( geom_coord_t const &);

    size_t  get_number_of_vertices()
        const { return  this->n_vertices; }
    geom_bounding_box_t  get_bounding_box() const;

    double  get_signed_area() const;

    bool  contains_point( geom_coord_t const &) const;
    bool  orientation_is_clockwise() const;
    bool  orientation_is_counterclockwise() const;
    void  reverse_orientation();

    void  make_orientation_counterclockwise();

    size_t  make_convex_hull();

// sk:TODO    bool  is_simple_polygon() const;

    size_t  make_chain( geom_coord_t *, size_t,
            int (*)( const void*, const void*));

    /* list of coordinates for polygon shape */
    geom_coord_t *  vertices;
    size_t  n_vertices;
};

/*       C
 *       /\
 *      /  \
 *     /    \
 *    A------B
 */
extern CBM_API bool  triangle_contains_point_2d(
        geom_coord_t const & /*point to check*/,
        geom_coord_t const & /*triangle A*/,
        geom_coord_t const & /*triangle B*/,
        geom_coord_t const & /*triangle C*/);

extern CBM_API double  signed_area_of_triangle_2d(
        geom_coord_t const & /*triangle A*/,
        geom_coord_t const & /*triangle B*/,
        geom_coord_t const & /*triangle C*/);

extern CBM_API bool  triangle_has_clockwise_orientation(
        geom_coord_t const & /*triangle A*/,
        geom_coord_t const & /*triangle B*/,
        geom_coord_t const & /*triangle C*/);

extern CBM_API bool  triangle_has_counterclockwise_orientation(
        geom_coord_t const & /*triangle A*/,
        geom_coord_t const & /*triangle B*/,
        geom_coord_t const & /*triangle C*/);

} /* namespace cbm */

#ifdef  __cplusplus
extern "C" {
#endif
int
cmpl(
        const void *, const void *);
int
cmpu(
        const void *, const void *);
#ifdef  __cplusplus
}
#endif

#ifdef  __cplusplus
namespace cbm {
extern "C" {
#endif

#define  FP52_PREC 10
struct  CBM_API  geom_geohash_t
{
    geom_geohash_t( double  /*lat*/, double  /*lon*/);
    ~geom_geohash_t();

    double  latitude() const { return  this->lat;}
    double  longitude() const { return  this->lon;}

    char *  encode( int = FP52_PREC /*precision*/);
    void  decode( char * /*geohash*/);
//    double  encode_fp52();

    private:
    double  lat;
    double  lon;
    char *  hash;
    int  precision;
};
struct  CBM_API  geo_position_t
{
    geo_position_t( double /*lat*/=0.0, double /*lon*/=0.0,  double /*alt*/=0.0);

    char *  geohash( int = FP52_PREC /*precision*/);
//    double  geohash_fp52();

    double  latitude() const { return  this->hash.latitude();}
    double  longitude() const { return  this->hash.longitude();}
    double  elevation() const { return  this->alt;}

    private:
    double  alt;
    geom_geohash_t  hash;
};

#ifdef  __cplusplus
}
} /* namespace cbm */
#endif

#endif  /*  !LDNDC_GEOM_H_  */

