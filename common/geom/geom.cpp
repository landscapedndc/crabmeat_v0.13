/*!
 * @brief
 *     geometry related types
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#include  "geom/geom.h"
#include  "string/cbm_string.h"
#include  "memory/cbm_mem.h"
#include  "log/cbm_baselog.h"

#include  "math/cbm_math.h"

cbm::geom_coord_t::geom_coord_t()
        : x( 0.0), y( 0.0), z(0.0)
{
}
cbm::geom_coord_t::geom_coord_t(
        geom_coord_t::coord_t  _x, geom_coord_t::coord_t  _y, geom_coord_t::coord_t  _z)
        : x( _x), y( _y), z( _z)
{
}
bool
cbm::geom_coord_t::is_origin()
const
{
    cbm::geom_coord_t  p0( 0.0, 0.0, 0.0);
    return  *this == p0;
}
bool
cbm::geom_coord_t::operator==(
        cbm::geom_coord_t const &  _coord)
const
{
    if ( this == &_coord)
        { return  true; }

    return  cbm::flt_equal( _coord.x, this->x)
        && cbm::flt_equal( _coord.y, this->y)
        && cbm::flt_equal( _coord.z, this->z);
}
bool
cbm::geom_coord_t::operator!=(
        cbm::geom_coord_t const &  _coord)
const
{
    return  !this->operator==( _coord);
}

std::ostream &
cbm::operator<<( std::ostream &  _out,
        cbm::geom_coord_t const &  _p)
{
    return  _out << '(' << _p.x << ',' << _p.y << ',' << _p.z << ')';
}


cbm::geom_dim_t::geom_dim_t()
        : n_x( 0), n_y( 0), n_z(0)
{ }
cbm::geom_dim_t::geom_dim_t(
    int  _n_x, int  _n_y, int  _n_z)
        : n_x( _n_x), n_y( _n_y), n_z( _n_z)
{ }


cbm::geom_bounding_box_t::geom_bounding_box_t()
        : ul( 0.0, 0.0, 0.0), lr( 0.0, 0.0, 0.0)
{ }
void
cbm::geom_bounding_box_t::init(
        cbm::geom_coord_t const &  _coord)
{
    this->ul.x = this->lr.x = _coord.x;
    this->ul.y = this->lr.y = _coord.y;
    this->ul.z = this->lr.z = _coord.z;
}
void
cbm::geom_bounding_box_t::update(
        cbm::geom_coord_t const &  _coord)
{
    if ( _coord.x < this->ul.x)
        { this->ul.x = _coord.x; }
    else if ( _coord.x > this->lr.x)
        { this->lr.x = _coord.x; }

    if ( _coord.y < this->ul.y)
        { this->ul.y = _coord.y; }
    else if ( _coord.y > this->lr.y)
        { this->lr.y = _coord.y; }

    if ( _coord.z > this->ul.z)
        { this->ul.z = _coord.z; }
    else if ( _coord.z < this->lr.z)
        { this->lr.z = _coord.z; }
}
void
cbm::geom_bounding_box_t::init(
        geom_shape_t const &  _shape)
{
    if ( _shape.get_number_of_vertices() > 0)
    {
        this->init( _shape.vertices[0]);
        this->update( _shape);
        return;
    }
    CBM_LogWarn( "geom_bounding_box_t::init(): ",
            "no vertices in shape object");
}
void
cbm::geom_bounding_box_t::update(
        geom_shape_t const &  _shape)
{
    if ( _shape.get_number_of_vertices() <= 1)
    {
        return;
    }
    for ( size_t  v = 0;  v < _shape.get_number_of_vertices();  ++v)
    {
        this->update( _shape.vertices[v]);
    }
}
void
cbm::geom_bounding_box_t::merge(
        cbm::geom_bounding_box_t const &  _bb)
{
    this->ul.x = std::min( this->ul.x, _bb.ul.x);
    this->lr.x = std::max( this->lr.x, _bb.lr.x);

    this->ul.y = std::min( this->ul.y, _bb.ul.y);
    this->lr.y = std::max( this->lr.y, _bb.lr.y);

    this->ul.z = std::max( this->ul.z, _bb.ul.z);
    this->lr.z = std::min( this->lr.z, _bb.lr.z);
}

bool
cbm::geom_bounding_box_t::contains(
        cbm::geom_bounding_box_t const &  _bb)
const
{
    return (( this->ul.x <= _bb.ul.x)
        && ( this->ul.y <= _bb.ul.y)
        && ( this->lr.x >= _bb.lr.x)
        && ( this->lr.y >= _bb.lr.y)
        && ( this->ul.z >= _bb.ul.z)
        && ( this->lr.z <= _bb.lr.z));
}
std::string
cbm::geom_bounding_box_t::to_string()
const
{
    std::ostringstream  bb_str;
    bb_str    << '('<< this->ul.x<<','<<this->ul.y<<','<<this->ul.z<<')'
        << '*'
        << '('<< this->lr.x<<','<<this->lr.y<<','<<this->lr.z<<')';

    return  bb_str.str();
}


cbm::geom_mapinfo_t::geom_mapinfo_t()
    : extent_x( 0.0), extent_y( 0.0), parts( 0)
{
}
bool
cbm::geom_mapinfo_t::is_valid()
const
{
    if ( ((this->dim.n_x < 1) != (this->dim.n_y < 1)) 
        || ( this->extent_x <= 0.0) || ( this->extent_y <= 0.0)
        || ( this->parts == 0)
        || ( this->parts > static_cast< size_t >( (this->dim.n_x*this->dim.n_y))))
    {
        return  false;
    }
    return  true;
}


cbm::geom_shape_t::geom_shape_t()
        : vertices( NULL), n_vertices( 0)
{
}
cbm::geom_shape_t::geom_shape_t(
        size_t  _n_vertices)
    : vertices( NULL), n_vertices( _n_vertices)
{
    if ( _n_vertices > 0)
    {
        this->vertices = CBM_DefaultAllocator->construct< cbm::geom_coord_t >( _n_vertices);
    }
}
cbm::geom_shape_t::~geom_shape_t()
{
    if ( this->vertices)
    {
        CBM_DefaultAllocator->destroy( this->vertices);
        this->vertices = NULL;
    }
    this->n_vertices = 0;
}

cbm::geom_shape_t::geom_shape_t(
            cbm::geom_shape_t const &  _rhs)
        : vertices( NULL), n_vertices( _rhs.n_vertices)
{
    int  rc_copy = this->copy_vertices( _rhs);
    if ( rc_copy < 0)
    {
        this->n_vertices = 0;
        crabmeat_assert( !this->vertices);
    }
}
cbm::geom_shape_t &
cbm::geom_shape_t::operator=(
                cbm::geom_shape_t const &  _rhs)
{
    if ( this == &_rhs)
        { return  *this; }

    int  rc_copy = this->copy_vertices( _rhs);
    this->n_vertices = _rhs.n_vertices;
    if ( rc_copy < 0)
    {
        this->n_vertices = 0;
        crabmeat_assert( !this->vertices);
    }

    return  *this;
}

int
cbm::geom_shape_t::copy_vertices(
                cbm::geom_shape_t const &  _rhs)
{
    if ( this->vertices)
    {
        CBM_DefaultAllocator->destroy( this->vertices, this->n_vertices);
        this->vertices = NULL;
    }

    if ( _rhs.n_vertices == 0)
        { return  0; }

    crabmeat_assert( _rhs.vertices);
    this->vertices = CBM_DefaultAllocator->construct< cbm::geom_coord_t >( _rhs.n_vertices);
    if ( !this->vertices)
    {
        CBM_LogError( "geom_shape_t::copy_vertices(): ", "nomem");
        return  -1;
    }

    for ( size_t  v = 0;  v < _rhs.n_vertices;  ++v)
    {
        this->vertices[v] = _rhs.vertices[v];
    }

    return  static_cast< int >( _rhs.n_vertices);
}

void
cbm::geom_shape_t::shift(
        cbm::geom_coord_t const &  _shift_value)
{
    for ( size_t  v = 0;  v < this->n_vertices;  ++v)
    {
        this->vertices[v].x -= _shift_value.x;
        this->vertices[v].y -= _shift_value.y;
        this->vertices[v].z -= _shift_value.z;
    }
}

cbm::geom_bounding_box_t
cbm::geom_shape_t::get_bounding_box()
const
{
    if ( !this->vertices || ( this->n_vertices == 0))
    {
        return  cbm::geom_bounding_box_t();
    }

    cbm::geom_bounding_box_t  bb;
    bb.init( this->vertices[0]);
    for ( size_t  v = 1;  v < this->n_vertices;  ++v)
    {
        bb.update( this->vertices[v]);
    }

    return  bb;
}

#include  "math/lmath-random.h"
/* F. Feito and J.C. Torres and A. Urena
 * "orientation, simplicity and inclusion test for planar polygons"
 * computers and graphics vol 19 no 4 pp 595-600
 */
bool
cbm::geom_shape_t::contains_point(
        cbm::geom_coord_t const &  _q)
const
{
    cbm::geom_coord_t const *  V = this->vertices;
    size_t const  n_V = this->n_vertices;

    if ( !V || ( n_V == 0))
    {
        return  false;
    }
    else if ( n_V == 1)
    {
        return  _q == V[0];
    }

    cbm::geom_coord_t  p0( 0.0, 0.0);
    int  j[2];
    int  k = 0, J = 0;

    if ( triangle_contains_point_2d( _q, p0, V[n_V-1], V[0]))
    {
        j[J] = cbm::sign( signed_area_of_triangle_2d( 
                p0, V[n_V-1], V[0]));
    }
    for ( size_t  v = 1;  v < n_V;  ++v)
    {
        J = 1 - J;
        bool const  is_interior = triangle_contains_point_2d( _q, p0, V[v-1], V[v]);
        if ( is_interior)
        {
            j[J] = cbm::sign( signed_area_of_triangle_2d( p0, V[v-1], V[v]));
        }
        else
        {
            j[J] = 0;
        }

        if ( j[0] != j[1])
        {
            k += j[J];
        }
    }
    return  k > 0;
}

/* http://mathworld.wolfram.com/PolygonArea.html */
double
cbm::geom_shape_t::get_signed_area()
const
{
    double  polygon_area = 0.0;
    cbm::geom_coord_t const *  V = this->vertices;
    for ( size_t  v = 1;  v < this->n_vertices;  ++v)
    {
        polygon_area += ( V[v-1].x * V[v].y) - ( V[v].x * V[v-1].y);
    }

    return  0.5 * polygon_area;
}

#include  "utils/lutils-swap.h"
void
cbm::geom_shape_t::reverse_orientation()
{
    size_t  n_vert = this->n_vertices / 2;
    for ( size_t  v = 0;  v < n_vert;  ++v)
    {
        cbm::swap_values< cbm::geom_coord_t >( this->vertices[v], this->vertices[this->n_vertices-v-1]);
    }
}
bool
cbm::geom_shape_t::orientation_is_clockwise()
const
{
    return  this->get_signed_area() < 0;
}
bool
cbm::geom_shape_t::orientation_is_counterclockwise()
const
{
    return  this->get_signed_area() > 0;
}
void
cbm::geom_shape_t::make_orientation_counterclockwise()
{
    if ( this->orientation_is_clockwise())
    {
        this->reverse_orientation();
    }
}


#include  "utils/lutils.h"
size_t
cbm::geom_shape_t::make_convex_hull()
{
    if ( this->n_vertices == 0)
    {
        return  0;
    }

    cbm::geom_coord_t *  P = CBM_DefaultAllocator->construct< cbm::geom_coord_t >( this->n_vertices+1);
    if ( !P)
    {
        CBM_LogError( "geom_shape_t::make_convex_hull(): nomem");
        return  0;
    }
    cbm::mem_cpy( P, this->vertices, this->n_vertices);

    size_t  u = this->make_chain( P, this->n_vertices, cmpl); /* make lower hull */
    P[this->n_vertices] = P[0];
    u += this->make_chain( P+u, this->n_vertices-u+1, cmpu);  /* make upper hull */

    if ( u > 0)
    {
        CBM_DefaultAllocator->destroy( this->vertices, this->n_vertices);
        this->vertices = CBM_DefaultAllocator->construct< cbm::geom_coord_t >( u);
        if ( !this->vertices)
        {
            CBM_LogError( "geom_shape_t::make_convex_hull(): nomem");
            return  0;
        }
        cbm::mem_cpy( this->vertices, P, u);
    }
    CBM_DefaultAllocator->destroy( P, this->n_vertices+1);
    this->n_vertices = u;

    CBM_LogDebug( "geom_shape_t::make_convex_hull():  u'=",u);
    return  u;
}

#define CMPM(__c__,__p1__,__p2__)                        \
    v = (*(cbm::geom_coord_t*)__p1__).__c__ - (*(cbm::geom_coord_t*)__p2__).__c__;    \
    if (v>0) return 1;                            \
    if (v<0) return -1;

#ifdef  __cplusplus
extern "C" {
#endif
int
cmpl(
        const void *a, const void *b)
{
    double  v; 
    CMPM( x, a, b);
    CMPM( y, b, a);
    return 0;
}

int
cmpu(
        const void *a, const void *b)
{
    return  cmpl( b,a);
}

#ifdef  __cplusplus
}
#endif

#include <stdlib.h>
size_t
cbm::geom_shape_t::make_chain(
        cbm::geom_coord_t *  _V, size_t  _n,
        int (* _cmp)( const void*, const void*))
{
    size_t  s = 1;

    qsort( _V, _n, sizeof(cbm::geom_coord_t), _cmp);
    for ( size_t  i = 2;  i <  _n;  ++i)
           {
        size_t  j = s;
        for (  ;  j >= 1 && triangle_has_counterclockwise_orientation( _V[i], _V[j], _V[j-1]);  --j)
        {
        }

        s = j + 1;
        cbm::swap_values< cbm::geom_coord_t >( _V[s], _V[i]);
    }
    return  s;
}

bool
cbm::triangle_contains_point_2d(
        cbm::geom_coord_t const &  _q,
        cbm::geom_coord_t const &  _p1,
        cbm::geom_coord_t const &  _p2,
        cbm::geom_coord_t const &  _p3)
{
    int const  s12 = cbm::sign( signed_area_of_triangle_2d( _q, _p1, _p2));
    int const  s23 = cbm::sign( signed_area_of_triangle_2d( _q, _p2, _p3));
    int const  s31 = cbm::sign( signed_area_of_triangle_2d( _q, _p3, _p1));

    int const  s_sum = s12 + s23 + s31;
    if ( s_sum == 3 || s_sum == -3 || ((( s12*s23*s31) == 0) && (s_sum != 0)))
    {
        return  true;
    }
    return  false;
}
double
cbm::signed_area_of_triangle_2d(
        cbm::geom_coord_t const &  _p1,
        cbm::geom_coord_t const &  _p2,
        cbm::geom_coord_t const &  _p3)
{
    double const  sgn_area =
          _p1.x * ( _p2.y - _p3.y)
        + _p2.x * ( _p3.y - _p1.y)
        + _p3.x * ( _p1.y - _p2.y);

    return  sgn_area;
}

bool
cbm::triangle_has_clockwise_orientation(
        cbm::geom_coord_t const &  _p1,
        cbm::geom_coord_t const &  _p2,
        cbm::geom_coord_t const &  _p3)
{
    double  a = _p1.x - _p2.x,
        b = _p1.y - _p2.y,
        c = _p3.x - _p2.x,
        d = _p3.y - _p2.y;

    return  (( a * d) - ( b * c)) > 0.0;
}
bool
cbm::triangle_has_counterclockwise_orientation(
        cbm::geom_coord_t const &  _p1,
        cbm::geom_coord_t const &  _p2,
        cbm::geom_coord_t const &  _p3)
{
    return  !triangle_has_clockwise_orientation( _p1, _p2, _p3);
}



#include  "geom/geohash.h"

cbm::geom_geohash_t::geom_geohash_t(
        double  _lat, double  _lon)
    : lat( _lat), lon( _lon), hash( NULL), precision( -1)
{
}
cbm::geom_geohash_t::~geom_geohash_t()
{
    if ( this->hash)
    {
        free( this->hash);
    }
    this->precision = -1;
}
char *
cbm::geom_geohash_t::encode(
        int  _precision)
{
    if ( !this->hash || ( this->precision != _precision))
    {
        if ( this->hash)
        {
            free( this->hash);
        }
        this->hash = ::geohash_encode( this->lat, this->lon, _precision);
    }
    return  this->hash;
}
void
cbm::geom_geohash_t::decode( char *  _geohash)
{
    if ( _geohash)
    {
        GeoCoord  coord = ::geohash_decode( _geohash);
        this->lat = coord.latitude;
        this->lon = coord.longitude;

        if ( this->hash)
        {
            free( this->hash);
        }
        this->hash = cbm::strdup( _geohash);
        this->precision = cbm::strlen( _geohash);
    }
}
//double
//cbm::geom_geohash_t::encode_fp52()
//{
//    if ( !this->hash)
//    {
//        this->encode( FP52_PREC);
//        if ( !this->hash)
//        {
//            return  -1.0;
//        }
//    }
//    for ( int  p = 0;  p < FP52_PREC;  ++p)
//           {
//        char  c = tolower( this->hash[i]);
//        char  c_digit = strchr( geohash_charmap, c) - geohash_charmap;
//        for ( char  i = 0;  i < 5;  ++i)
//        {
//
//    }
//    CBM_LogFatal( "implement");
//    return  0.0;
//}

cbm::geo_position_t::geo_position_t(
        double  _lat, double  _lon,  double  _alt)
        : alt( _alt), hash( _lat, _lon)
{
}
char *
cbm::geo_position_t::geohash( int  _precision)
{
    return  this->hash.encode( _precision);
}
//double
//cbm::geo_position_t::geohash_fp52()
//{
//    return  this->hash.encode_fp52();
//}

