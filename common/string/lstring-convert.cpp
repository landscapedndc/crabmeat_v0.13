/*!
 * @brief
 *    string conversion methods (implementation)
 *
 * @author
 *    Steffen Klatt (created on: feb 06, 2012 (forked from utils.cpp)),
 *    Edwin Haas
 */

#include  "string/lstring.h"
#include  "string/lstring-convert.h"
#include  "string/lstring-compare.h"

#include  "log/cbm_baselog.h"

void
cbm::__fail( char const *  _msg, 
    char const *  _info, bool  _fatal)
{
    if ( _info)
        { CBM_LogError( _msg, "[", _info,"]"); }
    else
        { CBM_LogError( _msg); }
    if ( _fatal)
        { CBM_LogFatal( "fatal error"); }
}

/* convert string to bool */
template < >
lerr_t
cbm::s2n< bool >(
        std::string const &  _str,
        bool *  _b)
{
    if ( cbm::is_bool( _str))
    {
        bool  t( cbm::is_bool_true( _str));
        bool  f( cbm::is_bool_false( _str));

        if ( t ^ f)
        {
            *_b = t;
            return LDNDC_ERR_OK;
        }
    }
    CBM_LogFatal( "invalid boolean string  [string=",_str,"]");
    return  LDNDC_ERR_FAIL;
}

/* convert string to string */
template < >
lerr_t
cbm::s2n< std::string >(
        std::string const &  _str,
        std::string *  _num)
{
    *_num = _str;
    return  LDNDC_ERR_OK;
}

/* convert string to cbm::string_t */
template < >
lerr_t
cbm::s2n< cbm::string_t >(
        std::string const &  _str,
        cbm::string_t *  _lstr)
{
    *_lstr = _str;
    return  LDNDC_ERR_OK;
}

static long int
cbm_strtol(
        char const *  _chrbuf, char **  _endbuf)
{
#if  defined(_HAVE_FUNC_STRTOL)
    return  static_cast< long int >( strtol( _chrbuf, _endbuf, 10));
#else
#  error  "sorry, currently there is no alternative implementation for strtol"
    return  0;
#endif
}
static long long int
cbm_strtoll(
        char const *  _chrbuf, char **  _endbuf)
{
#if  defined(_HAVE_FUNC_STRTOLL)
    return  static_cast< long long int >( strtoll( _chrbuf, _endbuf, 10));
#else
    return  static_cast< long long int >( cbm_strtol( _chrbuf, _endbuf));
#endif
}


template < >
char const *
cbm::s2n_c< char const * >(
        char const *  _chrbuf, char **  _endbuf)
{
    if ( _endbuf)
    {
        *_endbuf = const_cast< char * >( _chrbuf) + cbm::strlen( _chrbuf);
    }
    return  _chrbuf;
}
template < >
ldndc_bool_t
cbm::s2n_c< ldndc_bool_t >(
        char const *  _chrbuf, char **  _endbuf)
{
#define  BOOL_STR_SIZE  8
    char  bool_str_buffer[BOOL_STR_SIZE];
    bool_str_buffer[BOOL_STR_SIZE-1] = '\0';
    char *  bool_str = bool_str_buffer;

    char const *  chrbuf = _chrbuf;
    int  p = 1;
    while ( *chrbuf != '\0' && !is_space(*chrbuf))
    {
        /* strings representing boolean never longer than 7 characters */
        if ( ++p == BOOL_STR_SIZE)
        {
            if ( _endbuf) { *_endbuf = const_cast< char * >( _chrbuf); }
            return  false;
        }
        *bool_str++ = *chrbuf;
        ++chrbuf;
    }
    *bool_str = '\0';
    bool  chrbuf_value = false;
    lerr_t  rc_boolconv = cbm::s2n< bool >( bool_str_buffer, &chrbuf_value);
    if ( rc_boolconv)
    {
        if ( _endbuf ) { *_endbuf = const_cast< char * >( _chrbuf); }
        return  false;
    }
    if ( *chrbuf != '\0')
    {
        ++chrbuf;
    }
    if ( _endbuf) { *_endbuf = const_cast< char * >( chrbuf); }
    return  chrbuf_value;
}
template < >
ldndc_flt32_t
cbm::s2n_c< ldndc_flt32_t >(
        char const *  _chrbuf, char **  _endbuf)
{
#if  defined(_HAVE_FUNC_STRTOF)
    return  static_cast< ldndc_flt32_t >( strtof( _chrbuf, _endbuf));
#else
    return  static_cast< ldndc_flt32_t >( s2n_c< ldndc_flt64_t >( _chrbuf, _endbuf));
#endif
}
template < >
ldndc_flt64_t
cbm::s2n_c< ldndc_flt64_t >(
        char const *  _chrbuf, char **  _endbuf)
{
#if  defined(_HAVE_FUNC_STRTOD)
    return  static_cast< ldndc_flt64_t >( strtod( _chrbuf, _endbuf));
#else
#  error  "sorry, currently there is no alternative implementation for strtod"
#endif
}
#ifdef  LDNDC_HAVE_FLOAT128
template < >
ldndc_flt128_t
cbm::s2n_c< ldndc_flt128_t >(
        char const *  _chrbuf, char **  _endbuf)
{
#if  defined(_HAVE_FUNC_STRTOLD)
    return  static_cast< ldndc_flt128_t >( strtold( _chrbuf, _endbuf));
#else
    return  static_cast< ldndc_flt128_t >( s2n_c< ldndc_flt64_t >( _chrbuf, _endbuf));
#endif
}
#endif
template < >
ldndc_int8_t
cbm::s2n_c< ldndc_int8_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_int8_t >( cbm_strtol( _chrbuf, _endbuf));
}
template < >
ldndc_uint8_t
cbm::s2n_c< ldndc_uint8_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_uint8_t >( cbm_strtol( _chrbuf, _endbuf));
}
template < >
ldndc_int16_t
cbm::s2n_c< ldndc_int16_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_int16_t >( cbm_strtol( _chrbuf, _endbuf));
}
template < >
ldndc_uint16_t
cbm::s2n_c< ldndc_uint16_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_uint16_t >( cbm_strtol( _chrbuf, _endbuf));
}
template < >
ldndc_int32_t
cbm::s2n_c< ldndc_int32_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_int32_t >( cbm_strtol( _chrbuf, _endbuf));
}
template < >
ldndc_uint32_t
cbm::s2n_c< ldndc_uint32_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_uint32_t >( cbm_strtol( _chrbuf, _endbuf));
}
template < >
ldndc_int64_t
cbm::s2n_c< ldndc_int64_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_int64_t >( cbm_strtoll( _chrbuf, _endbuf));
}
template < >
ldndc_uint64_t
cbm::s2n_c< ldndc_uint64_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    return  static_cast< ldndc_uint64_t >( cbm_strtoll( _chrbuf, _endbuf));
}

template < >
ldndc_char_t
cbm::s2n_c< ldndc_char_t >(
        char const *  _chrbuf, char **  _endbuf)
{
    *_endbuf = const_cast< char * >( _chrbuf+1);
    return  static_cast< ldndc_char_t >( *_chrbuf);
}

template < >
std::string
cbm::s2n_c< std::string >(
        char const *  _chrbuf, char **  _endbuf)
{
    char const  *  chrbuf = _chrbuf;
    if ( *_endbuf==NULL)
    {
        while (( *chrbuf != '\0') && !is_space(*chrbuf))
            { ++chrbuf; }
    }
    else
    {
        while (( *chrbuf != '\0') && ( *chrbuf != **_endbuf))
            { ++chrbuf; }
    }
    *_endbuf = const_cast< char * >( chrbuf);

	/* NOTE  is length allowed to be 0 ? */
    return  std::string( _chrbuf, chrbuf-_chrbuf);
}

