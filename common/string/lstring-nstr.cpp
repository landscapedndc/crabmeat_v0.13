/*!
 * @author
 *      steffen klatt (created on: feb 06, 2012 (forked from utils.cpp))
 */

#include  "string/lstring-nstr.h"
#include  "string/lstring-basic.h"
#include  "string/lstring-convert.h"

#include  "log/cbm_baselog.h"

#include  <stdlib.h>


lerr_t
cbm::string_lstrip(
        char **  _s, char const *  _c, unsigned int *  _matches)
{
    return  cbm::__string_strip_in_place__( _s, _c, _matches, SSTRIP_HEAD|SSTRIP_INPLACE);
}
char *
cbm::string_lstrip(
        char const *  _s, char const *  _c, unsigned int *  _matches)
{
    return  cbm::__string_strip__( _s, _c, _matches, SSTRIP_HEAD);
}
lerr_t
cbm::string_rstrip(
        char **  _s, char const *  _c, unsigned int *  _matches)
{
    return  cbm::__string_strip_in_place__( _s, _c, _matches, SSTRIP_TAIL|SSTRIP_INPLACE);
}
char *
cbm::string_rstrip(
        char const *  _s, char const *  _c, unsigned int *  _matches)
{
    return  cbm::__string_strip__( _s, _c, _matches, SSTRIP_TAIL);
}
lerr_t
cbm::string_strip(
        char **  _s, char const *  _c, unsigned int *  _matches)
{
    return  cbm::__string_strip_in_place__( _s, _c, _matches, SSTRIP_HEAD|SSTRIP_TAIL|SSTRIP_INPLACE);
}
char *
cbm::string_strip(
        char const *  _s, char const *  _c, unsigned int *  _matches)
{
    return  cbm::__string_strip__( _s, _c, _matches, SSTRIP_HEAD|SSTRIP_TAIL);
}

char *
cbm::canonical_name_new(
        char const *  _c,
        lflags_t  _flags,
        char const *  _ws)
{
    if ( !_c) { return  NULL; }

    char *  c_can( NULL);
    if (( _flags & CNAME_STRIP_HEAD)  &&  ( _flags & CNAME_STRIP_TAIL))
    {
        c_can = string_strip( _c, _ws);
    }
    else if ( _flags & CNAME_STRIP_HEAD)
    {
        c_can = string_lstrip( _c, _ws);
    }
    else if ( _flags & CNAME_STRIP_TAIL)
    {
        c_can = string_rstrip( _c, _ws);
    }
    else
    {
        c_can = cbm::strdup( _c);
    }

    if ( !c_can)
    {
        CBM_LogError( "failed to strip string [",_c,"]");
    }
    else if (( _flags & CNAME_STRIP_TO_LOWER)  &&  ( _flags & CNAME_STRIP_TO_UPPER))
    {
        CBM_LogError( "cannot uppercase and lowercase at the same time.");
    }
    else if ( _flags & CNAME_STRIP_TO_UPPER)
    {
        to_upper_inplace( c_can);
    }
    else if ( _flags & CNAME_STRIP_TO_LOWER)
    {
        to_lower_inplace( c_can);
    }
    else
    {
        // no conversion
    }

    return  c_can;
}
char *
cbm::canonical_name_new(
        std::string const &  _s,
        lflags_t  _flags,
        char const *  _ws)
{
    return  canonical_name_new( _s.c_str(), _flags, _ws);
}
void
cbm::canonical_name_free(
        char *  _c)
{
    if ( _c)
           {
               free((void *)_c);
           }
}



/***************************/
/***  normalized string  ***/
/***************************/

cbm::nstr::nstr()
        : cnorm_( NULL), cflags_( CNAME_STRIP_NONE), cstrip_( NULL)
{
}
cbm::nstr::nstr(
        std::string const &  _s,
        lflags_t  _cflags,
        char const *  _cstrip)
        : cnorm_( NULL), cflags_( _cflags), cstrip_( _cstrip)
{
    this->cnorm_ = canonical_name_new( _s.c_str(), this->cflags_, this->cstrip_);
}

cbm::nstr::nstr(
        char const *  _c,
        lflags_t  _cflags,
        char const *  _cstrip)
        : cnorm_( NULL), cflags_( _cflags), cstrip_( _cstrip)
{
    if ( _c)
    {
        this->cnorm_ = canonical_name_new( _c, this->cflags_, this->cstrip_);
    }
}

cbm::nstr::nstr(
        nstr const &  _other)
{
    this->cnorm_ = cbm::strdup( _other.cnorm_);
    this->cflags_ = _other.cflags_;
    this->cstrip_ = _other.cstrip_; // copy would be safer!
}

cbm::nstr &
cbm::nstr::operator=(
        cbm::nstr const &  _other)
{
    if ( this == &_other)
           {
        return  *this;
    }

    this->clear_();
    this->cnorm_ = cbm::strdup( _other.cnorm_);
    this->cflags_ = _other.cflags_;
    this->cstrip_ = _other.cstrip_; // copy would be safer!

    return  *this;
}

cbm::nstr &
cbm::nstr::operator=(
        char const *  _c)
{
    if ( this->cnorm_ == _c)
           {
        return  *this;
    }

    this->clear_();
    this->cnorm_ = canonical_name_new( _c, this->cflags_, this->cstrip_);

    return  *this;
}

cbm::nstr &
cbm::nstr::operator=(
        std::string const &  _s)
{
    return  this->operator=( _s.c_str());
}

cbm::nstr::~nstr()
{
    this->clear_();
}

size_t
cbm::nstr::size()
const
{
    return  strlen( this->cnorm_);
}

void
cbm::nstr::clear_()
{
    if ( cnorm_)
           {
               canonical_name_free((char *)this->cnorm_);
           }
}

