/*!
 * @brief
 *
 * @author
 *      steffen klatt (created on: feb 06, 2012 (forked from utils.cpp))
 */

#include  "string/lstring-tokenizer.h"
#include  "string/lstring-basic.h"

#include  "log/cbm_baselog.h"
#include  "memory/cbm_mem.h"

#include  <stdlib.h>

lerr_t
cbm::string_token_new(
        char ***  _l, size_t *  _l_size,
        char const *  _c, char  _delim)
{
    crabmeat_assert( _l);
    crabmeat_assert( _c);

    int  tok_n = 0;
    char const *  c_pos_n = _c;
    while ( *c_pos_n != '\0')
    {
        if ( *c_pos_n == _delim)
        {
            ++tok_n;
        }
        ++c_pos_n;
    }
    *_l = CBM_DefaultAllocator->allocate_init_type< char * >( tok_n+1+( _l_size ? 0 : 1), NULL);
    if ( _l_size)
    {
        *_l_size = tok_n+1;
    }

    int  tok_sz = 0;
    int  tok_j = 0;
    char const *  c_pos_t = _c;
    while ( *c_pos_t != '\0')
    {
        if ( *c_pos_t == _delim)
        {
            (*_l)[tok_j] = CBM_DefaultAllocator->allocate_type< char >( tok_sz+1);
            if ( tok_sz > 0)
            {
                cbm::strncpy( (*_l)[tok_j], c_pos_t-tok_sz, tok_sz);
            }
            ((*_l)[tok_j])[tok_sz] = '\0';

            tok_sz = 0;
            ++tok_j;
        }
        else
        {
            ++tok_sz;
        }
        ++c_pos_t;
    }

    /* final token */
    (*_l)[tok_n] = CBM_DefaultAllocator->allocate_type< char >( tok_sz+1);
    cbm::strncpy( (*_l)[tok_n], c_pos_t-tok_sz, tok_sz);
    ((*_l)[tok_n])[tok_sz] = '\0';

    return  LDNDC_ERR_OK;
}
lerr_t
cbm::string_token_new(
        char ***  _l, size_t *  _l_size,
        std::string const &  _s, char  _delim)
{
    return  cbm::string_token_new( _l, _l_size, _s.c_str(), _delim);
}

void
cbm::string_token_free(
        char **  _l, size_t  _l_size)
{
    if ( !_l) { return; }

    if ( _l_size == 0)
    {
        /* expect sentinel */
        int  k( 0);
        while ( _l[k])
        {
            CBM_DefaultAllocator->deallocate( _l[k]);
            ++k;
        }
    }
    else
    {
        /* expect known size */
        for ( size_t  k = 0;  k < _l_size;  ++k)
        {
            CBM_DefaultAllocator->deallocate( _l[k]);
        }
    }
    CBM_DefaultAllocator->deallocate( _l);
}



/*******************/
/***  tokenizer  ***/
/*******************/

cbm::tokenizer::tokenizer()
        : tokens_( NULL),
          token_cnt_( 0),
          delim_( '\0'),
          cflags_( CNAME_STRIP_DEFAULTS),
          cstrip_( CNAME_WHITESPACE),
          status_( LDNDC_ERR_OK)
{
}
cbm::tokenizer::tokenizer(
        std::string const &  _s,
        char  _delim,
        lflags_t  _cflags,
        char const *  _cstrip)
        : tokens_( NULL),
          token_cnt_( 0),
          delim_( _delim),
          cflags_( _cflags),
          cstrip_( _cstrip)
{
    this->status_ = cbm::string_token_new( &this->tokens_, &this->token_cnt_, _s, this->delim_);
}
cbm::tokenizer::tokenizer(
        char const *  _c,
        char  _delim,
        lflags_t  _cflags,
        char const *  _cstrip)
        : tokens_( NULL),
          token_cnt_( 0),
          delim_( _delim),
          cflags_( _cflags),
          cstrip_( _cstrip)
{
    crabmeat_assert( _c);
    this->status_ = cbm::string_token_new( &this->tokens_, &this->token_cnt_, _c, this->delim_);
}
cbm::tokenizer::tokenizer(
        cbm::tokenizer const &  _other)
        : tokens_( NULL),
          token_cnt_( _other.token_cnt_),
          delim_( _other.delim_),
          cflags_( _other.cflags_),
          cstrip_( _other.cstrip_),
          status_( _other.status_)
{
    this->tokens_ = CBM_DefaultAllocator->allocate_init_type< char * >( this->token_cnt_);
    if ( this->tokens_)
    {
        for ( size_t  k = 0;  k < token_cnt_;  ++k)
        {
            this->tokens_[k] = CBM_DefaultAllocator->allocate_type< char >( cbm::strlen( _other[k]));
            if ( ! this->tokens_[k])
            {
                this->status_ = LDNDC_ERR_OBJECT_INIT_FAILED;
            }
            cbm::strcpy( this->tokens_[k], _other[k]);
        }
    }
    else
    {
        this->status_ = LDNDC_ERR_OBJECT_INIT_FAILED;
    }

}
cbm::tokenizer::tokenizer(
        cbm::nstr const &  _n,
        char  _delim)
        : delim_( _delim),
          cflags_( _n.flags()),
          cstrip_( _n.strip_chars())
{
    this->status_ = cbm::string_token_new( &this->tokens_, &this->token_cnt_, _n.c_str(), _delim);
}


cbm::tokenizer &
cbm::tokenizer::operator=(
        cbm::tokenizer const &  _other)
{
    if ( this != &_other)
           {
        this->clear_();
        this->status_ = _other.status_;

        this->delim_ = _other.delim_;
        this->cflags_ = _other.cflags_;
        this->cstrip_ = _other.cstrip_;

        this->token_cnt_ = _other.token_cnt_;

        this->tokens_ = CBM_DefaultAllocator->allocate_init_type< char * >( this->token_cnt_);
        if ( this->tokens_)
        {
            for ( size_t  k = 0;  k < this->token_cnt_;  ++k)
            {
                this->tokens_[k] = CBM_DefaultAllocator->allocate_type< char >( cbm::strlen( _other[k]));
                if ( ! this->tokens_[k])
                {
                    this->status_ = LDNDC_ERR_OBJECT_INIT_FAILED;
                }
                cbm::strcpy( this->tokens_[k], _other[k]);
            }
        }
        else
        {
            this->status_ = LDNDC_ERR_OBJECT_INIT_FAILED;
        }

    }
    return  *this;
}
cbm::tokenizer &
cbm::tokenizer::operator=(
        cbm::nstr const &  _n)
{
    this->clear_();
    this->cflags_ = _n.flags();
    this->cstrip_ = _n.strip_chars();
    this->status_ = cbm::string_token_new( &this->tokens_, &this->token_cnt_, _n.c_str(), this->delim_);

    return  *this;
}
cbm::tokenizer &
cbm::tokenizer::operator=(
        char const *  _c)
{
    crabmeat_assert( _c);

    this->clear_();
    this->status_ = cbm::string_token_new( &this->tokens_, &this->token_cnt_, _c, this->delim_);

    return  *this;
}
cbm::tokenizer &
cbm::tokenizer::operator=(
        std::string const &  _s)
{
    return  this->operator=( _s.c_str());
}

cbm::tokenizer::~tokenizer()
{
    this->clear_();
}

void
cbm::tokenizer::clear_()
{
    if ( this->tokens_ && ( this->token_cnt_ > 0))
           {
        string_token_free( this->tokens_, this->token_cnt_);
        this->tokens_ = NULL;
        this->token_cnt_ = 0;

        this->status_ = LDNDC_ERR_OK;
    }
}

