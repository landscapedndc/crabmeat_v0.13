/*!
 * @brief
 *    string comparison methods
 *
 * @author
 *    Steffen Klatt (created on: feb 06, 2012 (forked from lutils.h)),
 *    Edwin Haas
 */

#ifndef  CBM_STRING_COMPARE_H_
#define  CBM_STRING_COMPARE_H_

#include  "crabmeat-common.h"

#include  "string/lstring-convert.h"
#include  "string/lstring-basic.h"

namespace  cbm
{
    /* case sensitive string comparison */
    CBM_API bool is_equal(
            std::string const &, std::string const &);
    CBM_API bool is_equal(
            char const *, std::string const &);
    CBM_API bool is_equal(
            std::string const &, char const *);
    CBM_API bool is_equal(
            char const *, char const *);
    template < typename  _L, typename  _R >
    bool is_not_equal(
            _L const &  _l,  _R const  _r)
    {
        return  ! is_equal( _l, _r);
    }

    /* case insensitive string comparison */
    CBM_API bool is_equal_i(
            std::string const &, std::string const &);
    CBM_API bool is_equal_i(
            char const *, std::string const &);
    CBM_API bool is_equal_i(
            std::string const &, char const *);
    CBM_API bool is_equal_i(
            char const *, char const *);
    template < typename  _L, typename  _R >
        bool is_not_equal_i(
            _L const &  _l,  _R const  _r)
        { return  ! is_equal_i( _l, _r); }

    /* case sensitive string prefix comparison */
    CBM_API bool has_prefix(
            std::string const & /*string*/, std::string const & /*prefix*/);
    CBM_API bool has_prefix(
            char const * /*string*/, std::string const & /*prefix*/);
    CBM_API bool has_prefix(
            std::string const & /*string*/, char const * /*prefix*/);
    CBM_API bool has_prefix(
            char const * /*string*/, char const * /*prefix*/,
            size_t = ldndc::invalid_t< size_t >::value /*prefix size*/);
    template < typename  _L, typename  _R >
        bool has_not_prefix(
            _L const &  _l /*string*/,  _R const  _r /*prefix*/)
        { return  ! has_prefix( _l, _r); }

    /* case insensitive string prefix comparison */
    CBM_API bool has_prefix_i(
            std::string const & /*string*/, std::string const & /*prefix*/);
    CBM_API bool has_prefix_i(
            char const * /*string*/, std::string const & /*prefix*/);
    CBM_API bool has_prefix_i(
            std::string const &, char const * /*prefix*/);
    CBM_API bool has_prefix_i(
            char const * /*string*/, char const * /*prefix*/,
            size_t = ldndc::invalid_t< size_t >::value /*prefix size*/);
    template < typename  _L, typename  _R >
        bool has_not_prefix_i(
            _L const &  _l /*string*/,  _R const  _r /*prefix*/)
        { return  ! has_prefix_i( _l, _r); }

    /* case sensitive string prefix comparison */
    CBM_API bool has_suffix(
            std::string const & /*string*/, std::string const & /*prefix*/);
    CBM_API bool has_suffix(
            char const * /*string*/, std::string const & /*prefix*/);
    CBM_API bool has_suffix(
            std::string const & /*string*/, char const * /*prefix*/);
    CBM_API bool has_suffix(
            char const * /*string*/, char const * /*prefix*/,
            size_t = ldndc::invalid_t< size_t >::value /*prefix size*/);
    template < typename  _L, typename  _R >
        bool has_not_suffix(
            _L const &  _l /*string*/,  _R const  _r /*prefix*/)
        { return  ! has_suffix( _l, _r); }


    CBM_API bool is_empty(
            std::string const &);
    CBM_API bool is_empty(
            char const *);
    template < typename _T >
    bool
    is_not_empty( _T const &  _c) { return  ! is_empty( _c); }


    CBM_API bool is_whitespace(
            char const *);
    CBM_API bool is_whitespace(
            std::string const &);


    extern CBM_API char const *  cstring_false;
    extern CBM_API std::string const  lstring_false;
    template < typename  _T >
    inline
    bool  is_bool_true(
            _T const &  _b)
    {
        std::string  b( cbm::to_lower( std::string( _b)));
        return
            (       cbm::is_equal( b, "1")
                 || cbm::is_equal( b, "yes")
                 || cbm::is_equal( b, "true")
                 || cbm::is_equal( b, "set")
                 || cbm::is_equal( b, "on"));
    }

    extern CBM_API char const *  cstring_true;
    extern CBM_API std::string const  lstring_true;
    template < typename _T >
    inline
    bool  is_bool_false(
            _T const &  _b)
    {
        std::string  b( cbm::to_lower( std::string( _b)));
        return
            (       cbm::is_equal( b, "0")
                 || cbm::is_equal( b, "no")
                 || cbm::is_equal( b, "false")
                 || cbm::is_equal( b, "unset")
                 || cbm::is_equal( b, "off"));
    }

    template < typename _T >
    inline
    bool  is_bool(
            _T const &  _b)
    {
        return is_bool_true( _b)  ||  is_bool_false( _b);
    }

}  /* namespace cbm */


#endif  /*  !CBM_STRING_COMPARE_H_  */

