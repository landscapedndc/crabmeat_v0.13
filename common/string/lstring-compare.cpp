/*!
 * @brief
 *    string comparison methods (implementation)
 *
 * @author
 *    Steffen Klatt (created on: feb 06, 2012 (forked from utils.cpp)),
 *    Edwin Haas
 */

#include  "crabmeat-config.h"
#if defined(CRABMEAT_MINGW) && defined(__STRICT_ANSI__)
#  undef  __STRICT_ANSI__
#endif
#include  "string/lstring-compare.h"
#include  <string.h>
#ifdef  CRABMEAT_OS_UNIXOID
#  include  <strings.h>
#endif

char const *  cbm::cstring_true = "1";
std::string const  cbm::lstring_true = cbm::cstring_true;
char const *  cbm::cstring_false = "0";
std::string const  cbm::lstring_false = cbm::cstring_false;

bool
cbm::is_equal(
        std::string const &  _l,
        std::string const &  _r)
{
    return  ( _l == _r);
}

bool
cbm::is_equal(
        char const *  _l,
        char const *  _r)
{
    if ( !_l ^ !_r) { return  false; }
    if ( !_l && !_r) { return  true; }
#if  defined(_HAVE_FUNC_STRCMP)
    return  ( ::strcmp( _l, _r) == 0);
#else
    char const * r = _r, * l = _l;
    for ( ;; ++l, ++r)
    {
        int  lr_diff = *l - *r;
        if (( lr_diff != 0) || ( *r == '\0'))
        {
            return  ( lr_diff) ? false : true;
        }
    }
    return  true;
#endif
}
bool
cbm::is_equal(
        char const *  _l,
        std::string const &  _r)
{
    return  is_equal( _l, _r.c_str());
}
bool
cbm::is_equal(
        std::string const &  _l,
        char const *  _r)
{
    return  is_equal( _l.c_str(), _r);
}


bool
cbm::is_equal_i(
        std::string const &  _l,
        std::string const &  _r)
{
    return  is_equal_i( _l.c_str(), _r.c_str());
}

bool
cbm::is_equal_i(
        char const *  _l,
        char const *  _r)
{
    if ( !_l ^ !_r) { return  false; }
    if ( !_l && !_r) { return  true; }
#if  defined(CRABMEAT_COMPILER_MSC) || defined(CRABMEAT_MINGW)
    return  _stricmp( _l, _r) == 0;
#elif  defined(_HAVE_FUNC_STRCASECMP)
    return  strcasecmp( _l, _r) == 0;
#else
    char const * r = _r, * l = _l;
    for ( ;; ++l, ++r)
    {
        int  lr_diff = tolower(*l) - tolower(*r);
        if (( lr_diff != 0) || ( *r == '\0'))
        {
            return  ( lr_diff) ? false : true;
        }
    }
    return  true;
#endif
}
bool
cbm::is_equal_i(
        char const *  _l,
        std::string const &  _r)
{
    return  is_equal_i( _l, _r.c_str());
}
bool
cbm::is_equal_i(
        std::string const &  _l,
        char const *  _r)
{
    return  is_equal_i( _l.c_str(), _r);
}


bool
cbm::has_prefix(
        std::string const &  _str, std::string const &  _prefix)
{
    return  has_prefix( _str.c_str(), _prefix.c_str(), _prefix.size());
}
bool
cbm::has_prefix(
        char const *  _s, std::string const &  _prefix)
{
    return  has_prefix( _s, _prefix.c_str(), _prefix.size());
}
bool
cbm::has_prefix(
        std::string const &  _str, char const *  _prefix)
{
    return  has_prefix( _str.c_str(), _prefix);
}
bool
cbm::has_prefix(
        char const *  _str, char const *  _prefix,
        size_t  _n)
{
    if ( !_str || !_prefix) { return  false; }

    size_t  n =
        ( _n == ldndc::invalid_t< size_t >::value) ? cbm::strlen( _prefix) : _n;
#if  defined(_HAVE_FUNC_STRNCMP)
    return  ::strncmp( _str, _prefix, n) == 0;
#else
    char const *  s = _str;
    char const *  p = _prefix;
    for ( ;; ++s, ++p, --n)
    {
        int  c = *s - *p;
        if (( c != 0) || ( *s == '\0') || !n)
        {
            return  ( c || n) ? false : true;
        }
    }
    return  true;
#endif
}

bool
cbm::has_prefix_i(
        std::string const &  _str, std::string const &  _prefix)
{
    return  has_prefix_i( _str.c_str(), _prefix.c_str(), _prefix.size());
}
bool
cbm::has_prefix_i(
        char const *  _s, std::string const &  _prefix)
{
    return  has_prefix_i( _s, _prefix.c_str(), _prefix.size());
}
bool
cbm::has_prefix_i(
        std::string const &  _str, char const *  _prefix)
{
    return  has_prefix_i( _str.c_str(), _prefix);
}
bool
cbm::has_prefix_i(
        char const *  _str, char const *  _prefix,
        size_t  _n)
{
    if ( !_str || !_prefix) { return  false; }

    size_t   n =
        ( _n == ldndc::invalid_t< size_t >::value) ? cbm::strlen( _prefix) : _n;
#if  defined(CRABMEAT_COMPILER_MSC) || defined(CRABMEAT_MINGW)
    return  ::_strnicmp( _str, _prefix, n) == 0;
#elif  defined(_HAVE_FUNC_STRNCASECMP)
    return  ::strncasecmp( _str, _prefix, n) == 0;
#else
    char const *  s = _str;
    char const *  p = _prefix;
    for ( ;; ++s, ++p, --n)
    {
        int  c = tolower( *s) - tolower( *p);
        if (( c != 0) || ( *s == '\0') || !n)
        {
            return  ( c || n) ? false : true;
        }
    }
    return  true;
#endif
}

bool
cbm::has_suffix(
        std::string const &  _str, std::string const &  _prefix)
{
    return  has_suffix( _str.c_str(), _prefix.c_str(), _prefix.size());
}
bool
cbm::has_suffix(
        char const *  _s, std::string const &  _prefix)
{
    return  has_suffix( _s, _prefix.c_str(), _prefix.size());
}
bool
cbm::has_suffix(
        std::string const &  _str, char const *  _prefix)
{
    return  has_suffix( _str.c_str(), _prefix);
}
bool
cbm::has_suffix( char const *  _str,
        char const *  _prefix, size_t  _prefix_sz)
{
    if ( !_str || !_prefix)
        { return  false; }

    size_t  p_sz = _prefix_sz;
    if ( p_sz == ldndc::invalid_t< size_t >::value)
        { p_sz = cbm::strlen( _prefix); }
    size_t  s_sz = cbm::strlen( _str);

    if ( p_sz == 0)
        { return true; }
    if ( p_sz > s_sz)
        { return false; }
    if ( p_sz == s_sz)
        { return  is_equal( _str, _prefix); }

    s_sz -= 1;
    char const *  s = _str + s_sz;
    p_sz -= 1;
    char const *  p = _prefix + p_sz;
    for ( ;; --s, --p, --p_sz)
    {
        int  c = *s - *p;
        if (( c != 0) || ( p_sz == 0))
        {
            return  ( c || p_sz) ? false : true;
        }
    }
    return  true;
}


bool
cbm::is_empty(
        std::string const &  _s)
{
    return  _s.empty();
}

bool
cbm::is_empty(
        char const *  _c)
{
    return  cbm::strlen( _c) == 0;
}

bool
cbm::is_whitespace(
        char const *  _c)
{
    if ( !_c)
           {
        return  false;
    }

    while  (( *_c != '\0') && is_space(*_c)) { ++_c; }

    return  *_c == '\0';
}
bool
cbm::is_whitespace(
        std::string const &  _s)
{
    return  is_empty( _s)  ||  is_whitespace( _s.c_str());
}

