/*!
 * @brief
 *
 * @author
 *    Steffen Klatt (created on: feb 06, 2012 (forked from lutils.h)),
 *    Edwin Haas
 */

#ifndef  CBM_STRING_BASIC_H_
#define  CBM_STRING_BASIC_H_

#include  "crabmeat-common.h"

#include  <sstream>

#include  <cstring>
#include  <string>
#include  <cstdio>
#include  <cstdlib>
#include  <cstdarg>

#define  ldndc_txt_newline  '\n'
#define  ldndc_txt_tab  '\t'
#define  ldndc_txt_space  ' '
#define  ldndc_txt_comment  '#'
#define  ldndc_txt_comment2  '!'
#define  ldndc_txt_quote_open  '"'
#define  ldndc_txt_quote_close  '"'
#define  ldndc_txt_assign  '='
#define  ldndc_txt_tag  '%'
#define  ldndc_txt_block_open  '['
#define  ldndc_txt_block_close  ']'

#define  is_space(__chr__)  (( __chr__ == ldndc_txt_space) || ( __chr__ == ldndc_txt_tab) || ( __chr__ == '\v'))
#define  is_newline(__chr__)  /*(*/( __chr__ == ldndc_txt_newline) /*|| ( __chr__ == '\r'))*/
#define  is_comment(__chr__)  (( __chr__ == ldndc_txt_comment) || ( __chr__ == ldndc_txt_comment2))
#define  is_block(__chr__)  ( __chr__ == ldndc_txt_tag)
#define  is_assignment(__chr__)  ( __chr__ == ldndc_txt_assign)
#define  is_quotes_open(__chr__)  ( __chr__ == ldndc_txt_quote_open)
#define  is_quotes_close(__chr__)  ( __chr__ == ldndc_txt_quote_close)
#define  is_block_open(__chr__)  ( __chr__ == ldndc_txt_block_open)
#define  is_block_close(__chr__)  ( __chr__ == ldndc_txt_block_close)


/* the soldered empty string */
extern CBM_API std::string const  ldndc_empty_string;
extern CBM_API char const * const  ldndc_empty_cstring;


/* used internally for now */
enum
{
    /*! strip head of string */
    SSTRIP_HEAD             = 1u << 0,
    /*! strip tail of string */
    SSTRIP_TAIL             = 1u << 1,
    /*! reuse storage space of original string */
    SSTRIP_INPLACE          = 1u << 2
};

enum
{
    /*! empty flag */
    CNAME_STRIP_NONE        = 0u,
    /*! strip head of string */
    CNAME_STRIP_HEAD        = 1u << 0,
    /*! strip tail of string */
    CNAME_STRIP_TAIL        = 1u << 1,
    /*! strip head and tail */
    CNAME_STRIP             = CNAME_STRIP_HEAD|CNAME_STRIP_TAIL,
    /*! convert to lower case */
    CNAME_STRIP_TO_LOWER    = 1u << 2,
    /*! convert to upper case */
    CNAME_STRIP_TO_UPPER    = 1u << 3,

    /*! shortcut */
    CNAME_STRIP_DEFAULTS    = CNAME_STRIP|CNAME_STRIP_TO_UPPER
};

extern CBM_API char const *  CNAME_WHITESPACE;


namespace  cbm
{
    template < typename _S, typename _T >
    size_t
    find(
            _T const &  _s,
            _S const &  _sub)
    {
        std::string const  s( _s);
        return  s.find( _sub);
    }


    /*!
     * @brief
     *    concatenates char pointer list to single string
     */
    CBM_API void cat_list(
            std::string *  /*result string*/,
            char const * [] /*char list*/,
            size_t  /*size of list*/,
            char const * = NULL  /*delimiter*/);


    CBM_API lerr_t  __string_strip_in_place__(
            char **,
            char const *,
                   unsigned int *,
            unsigned char);

    CBM_API char *  __string_strip__(
            char const *,
                   char const *,
            unsigned int *,
            unsigned char);

    CBM_API int  strclrnul( char *, size_t);


    CBM_API size_t  strlen( char const *);
    CBM_API size_t  strnlen( char const *, size_t);

    CBM_API char *  strcpy( char *, char const *);
    CBM_API char *  strncpy( char *, char const *, size_t);

    CBM_API char *  strdup( char const *);
    CBM_API char *  strndup( char const *, size_t);

    CBM_API void  strfree( void const *);

    CBM_API int  snprintf( char * /*result buffer*/,
                    size_t /*size*/, char const * /*format*/, ...);
    CBM_API int  vsnprintf( char * /*result buffer*/,
        size_t /*size*/, char const * /*format*/, va_list /*arguments*/);

    CBM_API int as_strcpy( ldndc_string_t * /*pointer to ldndc string*/, char const * /*string*/);
    CBM_API int as_strcpy( ldndc_string_t * /*pointer to ldndc string*/, std::string const & /*string*/);
    CBM_API int as_strcpy( ldndc_string_t & /*destination ldndc string*/, ldndc_string_t const & /*source ldndc string*/);

    CBM_API bool  as_exceeds_maxsize( char const * /*string*/);
    CBM_API bool  as_exceeds_maxsize( std::string const & /*string*/);

}  /* namespace cbm */


#endif  /*  !CBM_STRING_BASIC_H_  */

