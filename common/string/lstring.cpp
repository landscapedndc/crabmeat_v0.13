/*!
 * @brief
 *    ldndc string class (implementation)
 *
 * @author
 *      steffen klatt (created on: mar 18, 2013),
 *    edwin haas
 */

#include  "lstring.h"
#include  "lstring-transform.h"
#include  "log/cbm_baselog.h"

cbm::string_t const  invalid_lstring = invalid_string;

#include  <stdio.h>
#include  <stdarg.h>
void
cbm::string_t::_vmake(
    char *  _buf, size_t _buf_sz, char const *  _format, va_list  _args)
{
    int  r_size = ::vsnprintf( _buf, _buf_sz-1, _format, _args);
    if ( r_size >= static_cast< int >( _buf_sz))
    {
        _buf[0] = '\0';
    }
    _buf[_buf_sz-1] = '\0';
}

cbm::string_t::string_t()
{ }
cbm::string_t::string_t(
        cbm::string_t const &  _str)
        : buf( _str.buf)
{ }
cbm::string_t::string_t( std::string const &  _str,
        lflags_t const &  _flags) : buf( _str)
{
    if ( _flags & FORMAT_EXPAND)
        { this->format_expand(); }
}
cbm::string_t::string_t( char const *  _str,
        lflags_t const &  _flags) : buf( _str)
{
    if ( _flags & FORMAT_EXPAND)
        { this->format_expand(); }
}

cbm::string_t::string_t(
        char const *  _str, size_t  _length,
        char const *  _set1, char const *  _set2, size_t  n_sets)
{
    this->buf = std::string( _str, _length);
    if ( n_sets == 1)
    {
        for ( size_t  j = 0;  j < this->buf.size();  ++j)
        {
            if ( this->buf[j] == *_set1)
            {
                this->buf[j] = *_set2;
            }
        }
    }
    else
    {
        for ( size_t  j = 0;  j < this->buf.size();  ++j)
        {
            char const *  s1 = _set1;
            char const *  s2 = _set2;
            size_t  k = 0;
            while ( k++ < n_sets)
            {
                if ( this->buf[j] == *s1)
                {
                    this->buf[j] = *s2;
                }
                ++s1;
                ++s2;
            }
        }
    }
}

cbm::string_t::~string_t()
{
}

cbm::string_t &
cbm::string_t::operator=(
        cbm::string_t const &  _str)
{
    if ( this != &_str)
    {
        this->buf = _str.buf;
    }
    return  *this;
}
cbm::string_t &
cbm::string_t::operator=(
        std::string const &  _str)
{
    this->buf = _str;
    return  *this;
}
cbm::string_t &
cbm::string_t::operator=(
        char const *  _str)
{
    this->buf = _str;
    return  *this;
}

cbm::string_t
cbm::string_t::operator+(
        cbm::string_t const &  _str)
{
    return  cbm::string_t( this->buf + _str.buf);
}
cbm::string_t
cbm::string_t::operator+(
        std::string const &  _str)
{
    return  cbm::string_t( this->buf + _str);
}
cbm::string_t
cbm::string_t::operator+(
        char  _chr)
{
    return  cbm::string_t( this->buf + _chr);
}
cbm::string_t
cbm::string_t::operator+(
        char const *  _str)
{
    return  cbm::string_t( this->buf + _str);
}

cbm::string_t &
cbm::string_t::operator+=(
        cbm::string_t const &  _str)
{
    this->buf += _str.buf;
    return  *this;
}
cbm::string_t &
cbm::string_t::operator+=(
        std::string const &  _str)
{
    this->buf += _str;
    return  *this;
}
cbm::string_t &
cbm::string_t::operator+=(
        char  _chr)
{
    this->buf += _chr;
    return  *this;
}
cbm::string_t &
cbm::string_t::operator+=(
        char const *  _str)
{
    this->buf += _str;
    return  *this;
}


lerr_t
cbm::string_t::format_expand()
{
    return  cbm::format_expand( &this->buf);
}
cbm::string_t &
cbm::string_t::expand()
{
    lerr_t rc_exp = this->format_expand();
    if ( rc_exp)
        { this->buf = ""; }
    return  *this;
}



cbm::tokenizer
cbm::string_t::tokenize(
        char  _delim, lflags_t  _flags, char const *  _normalize)
const
{
    return  cbm::tokenizer( this->buf, _delim, _flags, _normalize);
}

#include  "string/lstring-nstr.h"
cbm::string_t
cbm::string_t::strip(
		char const * _chars)
const
{
    char const *  chars = _chars ? _chars : " ";
    cbm::nstr  normstr( this->buf, CNAME_STRIP_HEAD|CNAME_STRIP_TAIL, chars);

    return  normstr.c_str();
}
cbm::string_t
cbm::string_t::lstrip(
		char const * _chars)
const
{
    char const *  chars = _chars ? _chars : " ";
    cbm::nstr  normstr( this->buf, CNAME_STRIP_HEAD, chars);

    return  normstr.c_str();
}
cbm::string_t
cbm::string_t::rstrip(
		char const * _chars)
const
{
    char const *  chars = _chars ? _chars : " ";
    cbm::nstr  normstr( this->buf, CNAME_STRIP_TAIL, chars);

    return  normstr.c_str();
}

#include  "string/lstring-transform.h"
cbm::string_t
cbm::string_t::to_lower()
const
{
    return  cbm::to_lower( this->buf);
}
cbm::string_t
cbm::string_t::to_upper()
const
{
    return  cbm::to_upper( this->buf);
}

bool
cbm::string_t::startswith( char const *  _prefix) const
{
    return  cbm::has_prefix( this->buf.c_str(), _prefix);
}
bool
cbm::string_t::startswith( cbm::string_t const &  _prefix) const
{
    return  this->startswith( _prefix.c_str());
}

bool
cbm::string_t::endswith( char const *  _prefix) const
{
    return  cbm::has_suffix( this->buf.c_str(), _prefix);
}
bool
cbm::string_t::endswith( cbm::string_t const &  _prefix) const
{
    return  this->endswith( _prefix.c_str());
}

int
cbm::string_t::find( char _chr) const
{
    size_t const  pos = this->buf.find( _chr);
    if ( pos == std::string::npos)
        { return  -1; }
    return  pos;
}
int
cbm::string_t::rfind( char _chr) const
{
    size_t const  pos = this->buf.rfind( _chr);
    if ( pos == std::string::npos)
        { return  -1; }
    return  pos;
}


cbm::key_value_t
cbm::as_key_value(
        char const *  _keyvalue)
{
    key_value_t  keyvalue;
    cbm::string_t *  s = &keyvalue.key;
    while ( *_keyvalue != '\0')
    {
        if ( *_keyvalue == '=')
        {
            s = &keyvalue.value;
        }
        else
        {
            *s += *_keyvalue;
        }
        ++_keyvalue;
    }
    return  keyvalue;
}

std::ostream &
cbm::operator<<( std::ostream &  _out,
        cbm::string_t const &  _str)
{
    _out << _str.c_str();
    return  _out;
}

