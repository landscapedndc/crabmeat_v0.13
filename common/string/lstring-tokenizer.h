/*!
 * @author
 *    steffen klatt (created on: feb 06, 2012 (forked from lutils.h))
 */

#ifndef  CBM_STRING_TOKENIZER_H_
#define  CBM_STRING_TOKENIZER_H_

#include  "crabmeat-common.h"

#include  "string/lstring-basic.h"
#include  "string/lstring-nstr.h"

namespace  cbm
{
    /*!
     * @brief
     *    split a string into tokens separated by delimiter
     *
     *    the second argument is set to the number of tokens
     *    the string was split into. if this argument is NULL
     *    it will not be set but a sentinel will be added
     *    instead, i.e. an additional element pointing to NULL
     *    is appended to the resulting array.
     */
    lerr_t CBM_API  string_token_new(
            char ***, size_t *,
            std::string const &, char);
    /*!
     * @brief
     *    char const * version of @fn string_token_new
     */ 
    lerr_t CBM_API  string_token_new(
            char ***, size_t *,
                   char const *, char);
    /*!
     * @brief
     *    convenience function to free arrays allocated by
     *    @fn string_token_new. this ensures the right
     *    combinations of malloc/free, new/delete and
     *    new[]/delete[].
     *
     *    if the array was created with a sentinel call
     *    this routine with size specified as 0
     *
     *    e.g.  string_token_free( c_list, 0);
     */
    void CBM_API  string_token_free(
            char **, size_t);


    /*!
     * @brief
     *    string tokenizer wrapper class. the string
     *    passed to this class is split into substrings
     *    (tokens). string is cut at each occurence of
     *    the delimiter.
     */
    class  CBM_API  tokenizer
          {
        public:
            tokenizer();
            tokenizer(
                    std::string const &,
                    char,
                           lflags_t = CNAME_STRIP_DEFAULTS,
                           char const * = CNAME_WHITESPACE);
            tokenizer(
                    char const *,
                    char,
                           lflags_t = CNAME_STRIP_DEFAULTS,
                           char const * = CNAME_WHITESPACE);
            tokenizer(
                    cbm::tokenizer const &);
            tokenizer(
                    cbm::nstr const &,
                    char);

            tokenizer &  operator=(
                    cbm::tokenizer const &);

            tokenizer &  operator=(
                    cbm::nstr const &);

            tokenizer &  operator=(
                    char const *);

            tokenizer &  operator=(
                    std::string const &);

            ~tokenizer();

            bool  is_ok()
            const
            {
                       return  this->status_ == LDNDC_ERR_OK;
                   }


            std::string  str(
                    size_t  _k)
            const
                   {
                return  std::string( this->tokens_[_k]);
            }
            char const *  c_str(
                    size_t  _k)
            const
            {
                return  this->operator[]( _k);
            }
            cbm::nstr  n_str(
                    size_t  _k, lflags_t  _cflags = CNAME_STRIP_NONE)
            const
            {
                return  cbm::nstr( this->c_str( _k), ( _cflags == CNAME_STRIP_NONE) ? this->cflags_ : _cflags, this->cstrip_);
            }

            char const *  operator[](
                    size_t  _k)
            const
            {
                return  this->tokens_[_k];
            }

            size_t  size() const { return  this->token_cnt(); }
            size_t  token_cnt()
            const
            {
                return  this->token_cnt_;
            }
            char  delimiter()
            const
            {
                return  this->delim_;
            }

// sk:later            lerr_t  disown( char const **, unsigned int  *);

        private:
            char **  tokens_;
            size_t  token_cnt_;
            char  delim_;

            lflags_t  cflags_;
            char const *  cstrip_;

            lerr_t  status_;

            void  clear_();
    };

}  /* namespace cbm */


#endif  /*  !CBM_STRING_TOKENIZER_H_  */

