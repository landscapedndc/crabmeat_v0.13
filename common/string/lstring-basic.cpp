/*!
 * @brief
 *
 * @author
 *    Steffen Klatt (created on: feb 06, 2012 (forked from utils.cpp)),
 *    Edwin Haas,
 */

#include  "crabmeat-config.h"
#if defined(CRABMEAT_MINGW) && defined(__STRICT_ANSI__)
#  undef  __STRICT_ANSI__
#endif
#include  "string/lstring-basic.h"

#include  "math/cbm_math.h"
#include  "log/cbm_baselog.h"

#include  <stdlib.h>

std::string const  ldndc_empty_string = "";
char const * const ldndc_empty_cstring = "";

char const *  CNAME_WHITESPACE = " \t\r\n\f\v";

void
cbm::cat_list(
        std::string *  _cat_list,
        char const *  _list[],
        size_t  _n,
        char const *  _delim)
{
    CBM_Assert( _cat_list);


    _cat_list->clear();
    if (( _n > 0) && _list)
    {
        if ( _n > 1)
        {
            char const *  delim(( _delim) ? _delim : " ");
            std::ostringstream  cat_list;
            size_t const  n = _n - 1;
            for ( size_t  k = 0;  k < n;  ++k)
            {
                cat_list << _list[k] << delim;
            }
            _cat_list->assign( cat_list.str());
        }
        _cat_list->append( _list[_n-1]);
    }
}


lerr_t
cbm::__string_strip_in_place__(
        char **  _s, char const *  _c, unsigned int *  _matches,
        unsigned char  _flags)
{
    if ( !_s || !_c)
           {
        if ( _matches) *_matches = 0;
        return  LDNDC_ERR_OK;
    }

    /* limit to left strip, right strip or both */
    bool  lstrip( _flags & SSTRIP_HEAD), rstrip( _flags & SSTRIP_TAIL);

    int  S( 0);
    /* find size of _s */
    while ( (*_s)[S] != '\0') { ++S; }

    if ( !S) { if ( _matches) *_matches = 0; return  LDNDC_ERR_OK; }

    int  p[2] = { 0, S};
    while (( p[0] < p[1]) && ( lstrip || rstrip))
    {
        {
            /* eat character from left */
            unsigned int  k( 0);
            int  p0( p[0]);
            while ( lstrip && ( _c[k] != '\0'))
                   {
                if ( (*_s)[p0] == _c[k])
                       {
                    ++p[0];
                    break;
                }
                ++k;
            }
            lstrip = (p0 < p[1]/*(int)S*/) && (p0 < p[0]);
        }
        {
            /* eat character from right */
            unsigned int  k( 0);
            int  p1( p[1]-1);
            while ( rstrip && ( _c[k] != '\0'))
                   {
                if ( (*_s)[p1] == _c[k])
                       {
                    --p[1];
                    break;
                }
                ++k;
            }
            rstrip = ( p1 > p[0]/*0*/) && ( p1 == p[1]);
        }
    }

    int  n( std::max( 0, p[1] - p[0]));
    if ( S > n)
    {
        if ( _flags & SSTRIP_INPLACE)
        {
            /* shift characters to front */
            size_t  j( 0), j_e( n);
            while ( j < j_e)
                   {
                (*_s)[j] = (*_s)[p[0]+j];
                ++j;
            }
            (*_s)[j] = '\0';
        }
        else
        {
            /* copy substring, delete old string and attach new one */
            char *  s = cbm::strndup( (*_s)+p[0], n);
            cbm::strfree((void *)(*_s));
            *_s = s;
        }
    }

    /* set matches found */
    if ( _matches) *_matches = S - n;

    return  LDNDC_ERR_OK;
}

char *
cbm::__string_strip__(
        char const *  _s, char const *  _c, unsigned int *  _matches, unsigned char  _flags)
{
    char *  s_tmp = cbm::strdup( _s);
    if ( cbm::__string_strip_in_place__( &s_tmp, _c, _matches, _flags|SSTRIP_INPLACE) != LDNDC_ERR_OK)
    {
        CBM_LogError( "error stripping string", "  [string='",_s,"']");
        if ( s_tmp)
            { cbm::strfree( s_tmp); }
        s_tmp = NULL;
    }

    return  s_tmp;
}


int
cbm::strclrnul(
        char *  _s,
        size_t  _n)
{
    int  m = 0;
    for ( size_t  j = 0;  j < _n;  ++j)
    {
        if ( _s[j] == '\0')
        {
            _s[j] = ' ';
            ++m;
        }
    }
    return  m;
}


size_t
cbm::strlen( char const *  _c)
{
#ifndef  _HAVE_FUNC_STRLEN
    return  ::strlen( _c);
#else
    size_t  n = 0;
    while ( _c[n] != '\0') { ++n; }
    return  n;
#endif
}
size_t
cbm::strnlen( char const *  _c, size_t  _n)
{
#ifdef  _HAVE_FUNC_STRNLEN
    return  ::strnlen( _c, _n);
#else
    size_t  n = 0;
    while (( _c[n] != '\0') && ( n < _n)) { ++n; }
    return  n;
#endif
}

char *
cbm::strcpy(
        char *  _d, char const *  _c)
{
#if  defined(_HAVE_FUNC_STRCPY)
    return  ::strcpy( _d, _c);
#else
    char *  d = _d;
    char const *  c = _c;
    while ( *c != '\0')
    {
        *d = *c;
        ++d;
        ++c;
    }
    *d = '\0';
    return  _d;
#endif
}

char *
cbm::strncpy(
        char *  _d, char const *  _c, size_t  _n)
{
#if  defined(_HAVE_FUNC_STRNCPY)
    return  ::strncpy( _d, _c, _n);
#else
    char *  d = _d;
    char const *  c = _c;
    size_t  n = _n;
    while (( *c != '\0') && n)
    {
        *d = *c;
        ++d;
        ++c;
        --n;
    }
    if ( *c == '\0')
        { *d = '\0'; }
    return  _d;
#endif
}

char *
cbm::strdup(
        char const *  _c)
{
#if  (defined(_MSC_VER) && (_MSC_VER >= 1400)) || defined(CRABMEAT_MINGW)
    return  _strdup( _c);
#elif  defined(_HAVE_FUNC_STRDUP)
    return  ::strdup( _c);
#else
    char *  c = (char *)malloc( sizeof( char) * ( strlen( _c) + 1 /*\0*/));
    return  cbm::strcpy( c, _c);
#endif
}

char *
cbm::strndup(
        char const *  _c, size_t  _n)
{
#if  defined(_HAVE_FUNC_STRNDUP)
    return  ::strndup( _c, _n);
#else
    size_t  n = cbm::strnlen( _c, _n);
    size_t  r = 0;
    char *  s_copy = (char *)malloc((n+1)*sizeof( char));
    if ( s_copy)
    {
        r = 0;
        while ( r < n)
        {
            s_copy[r] = _c[r];
            ++r;
        }
        s_copy[n] = '\0';
    }
    return  s_copy;
#endif
}

void
cbm::strfree(
        void const *  _c)
{
    free((void *)_c);
}

int
cbm::snprintf( char *  _buf, size_t  _size,
                const char *  _format, ...)
{
    va_list  va;
    va_start( va, _format);
    int const  rc =
        cbm::vsnprintf( _buf, _size, _format, va);
    va_end( va);

    return  rc;
}

int
cbm::vsnprintf( char *  _buf, size_t  _size,
                const char *  _format, va_list  _args)
{
#if  defined(_HAVE_FUNC_VSNPRINTF)
    int const  rc =
        ::vsnprintf( _buf, _size, _format, _args);
    return  rc;
#elif  defined(_MSC_VER) && (_MSC_VER >= 1400)
    int const  rc =
        ::vsnprintf_s( _buf, _size, _TRUNCATE, _format, _args);
    return  rc;
#else
#  error  no vsnprintf function implementation available
#endif
}

int
cbm::as_strcpy(
        ldndc_string_t *  _lstring,
        char const *  _string)
{
    CBM_Assert( _lstring);
    if ( !_string)
    {
        return  0;
    }

    cbm::strncpy( (char *)*_lstring, (char const *)_string, (size_t)CBM_STRING_SIZE);
    (*_lstring)[CBM_STRING_SIZE] = '\0';

    if ( cbm::as_exceeds_maxsize( _string))
    {
        CBM_LogWarn( "ldndc_strcpy: string too long, truncated.",
              "  [string=",_string,",maximum=",CBM_STRING_SIZE,"]");
        return  -1;
    }

    return  0;
}
int
cbm::as_strcpy(
        ldndc_string_t *  _lstring,
        std::string const &  _string)
{
    return  cbm::as_strcpy( _lstring, _string.c_str());
}
int
cbm::as_strcpy(
        ldndc_string_t &  _string_d,
        ldndc_string_t const &  _string_s)
{
    return  cbm::as_strcpy( &_string_d, (char const *)_string_s);
}


bool
cbm::as_exceeds_maxsize(
        char const *  _string)
{
    return  cbm::strlen( _string) > CBM_STRING_SIZE;
}
bool
cbm::as_exceeds_maxsize(
        std::string const &  _string)
{
    return  _string.size() > CBM_STRING_SIZE;
}

