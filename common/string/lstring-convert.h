/*!
 * @brief
 *    string conversion methods
 *
 * @author
 *    Steffen Klatt (created on: feb 06, 2012 (forked from lutils.h)),
 *    Edwin Haas
 */

#ifndef  CBM_STRING_CONVERT_H_
#define  CBM_STRING_CONVERT_H_

#include  "crabmeat-common.h"
#include  "log/cbm_baselog.h"

#include  "string/lstring-basic.h"
#include  "string/lstring-transform.h"

#include  <iomanip>

namespace  cbm
{
    void CBM_API __fail( char const * /*message*/, 
            char const * = NULL /*info*/, bool = false /*fatal?*/);

    /*!
     * @brief
     *    convert "anything" to a string
     */
    template < typename _T >
    std::string
    n2s(
            _T  nval,
            size_t  prec = 5)
    {
        std::stringstream  str;
        str.precision((std::streamsize)prec);
        str << nval;

        if ( str.fail())
        {
            __fail( "numeric to string conversion failed.");
        }
        return  str.str();
    }


    /*!
     * @brief
     *    convert a bool to string, i.e. "true" or "false"
     * @note
     *    use second argument for choice? (e.g. "1","set","on","true","yes")
     */
    template < >
    inline
    CBM_API std::string  n2s< bool >(
            bool  nval,
            size_t)
    {
        return  ( nval) ? "true" : "false";
    }


    /*!
     * @brief
     *      converts a std::string to any class
     *      with a proper overload of the >> operator
     */
    template < typename _T >
    lerr_t  s2n( std::string const &  _str, _T *  _num)
    {
        if ( ( _str.empty()) || ( cbm::to_lower( _str) == "na")
            || ( cbm::to_lower( _str) == "nan"))
        {
            *_num = ldndc::invalid_t< _T >::value;
        }
        else
        {
            std::istringstream  val( _str);

            val >> *_num;
            if ( val.fail())
            {
                __fail( "string to numeric conversion failed", _str.c_str());
                return  LDNDC_ERR_FAIL;
            }
            else if ( !val.eof())
            {
                __fail( "string only partially converted", _str.c_str());
                return  LDNDC_ERR_FAIL;
            }
        }
        return  LDNDC_ERR_OK;
    }

    /*!
     * @brief
     *    converts a string to bool
     */
    template < >
    CBM_API lerr_t  s2n< bool >(
            std::string const &,
            bool *);
    /*!
     * @brief
     *    "converts" string to string
     */
    template < >
    CBM_API lerr_t  s2n< std::string >(
            std::string const &,
            std::string *);
    class  string_t;
    /*!
     * @brief
     *    converts a string to a cbm::string_t
     */
    template < >
    CBM_API lerr_t  s2n< cbm::string_t >(
            std::string const &,
            cbm::string_t *);


    template < typename  _T >
    int  as_strset( ldndc_string_t *  _buf, _T const &  _v)
    {
        std::string const  s( n2s< _T >( _v));
        return  as_strcpy( _buf, s);
    }
}  /* namespace cbm */

#include  <vector>
#include  "cbm_types.h"
namespace  cbm
{
    template < typename  _T >
    _T  s2n_c(
            char const *  _buf, char ** /*end pointer*/)
    {
        CRABMEAT_FIX_UNUSED(_buf);
        cbm::__fail( "missing implementation", NULL, true); //  [str=",((_buf)?_buf:"<?>"),"]");
        return  0;
    }

    template < >
    CBM_API  char const *  s2n_c< char const * >(
            char const *, char **);

    template < >
    CBM_API  ldndc_bool_t  s2n_c< ldndc_bool_t >(
            char const *, char **);

    template < >
    CBM_API  ldndc_flt32_t  s2n_c< ldndc_flt32_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_flt64_t  s2n_c< ldndc_flt64_t >(
            char const *, char **);
#ifdef  LDNDC_HAVE_FLOAT128
    template < >
    CBM_API  ldndc_flt128_t  s2n_c< ldndc_flt128_t >(
            char const *, char **);
#endif
    template < >
    CBM_API  ldndc_int8_t  s2n_c< ldndc_int8_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_uint8_t  s2n_c< ldndc_uint8_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_int16_t  s2n_c< ldndc_int16_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_uint16_t  s2n_c< ldndc_uint16_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_int32_t  s2n_c< ldndc_int32_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_uint32_t  s2n_c< ldndc_uint32_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_int64_t  s2n_c< ldndc_int64_t >(
            char const *, char **);
    template < >
    CBM_API  ldndc_uint64_t  s2n_c< ldndc_uint64_t >(
            char const *, char **);

    template < >
    CBM_API  ldndc_char_t  s2n_c< ldndc_char_t >(
            char const *, char **);

    template < >
    CBM_API  std::string  s2n_c< std::string >(
            char const *, char **);

    template < typename  _T >
        struct s2n_properties
            { enum { allow_empty = 0 }; };
    template < >
        struct s2n_properties< std::string >
            { enum { allow_empty = 1 }; };


// sk:off    template < typename  _T >
// sk:off    int  s2n_c(
// sk:off            _T *  _buf, size_t  _n_buf, char *  _chrbuf /*NUL terminated*/)
// sk:off    {
// sk:off        int  j = 0;
// sk:off        int const  n_buf = static_cast< int >( _n_buf);
// sk:off
// sk:off        char const *  b_data = _chrbuf;
// sk:off        char *  b_endptr = _chrbuf;
// sk:off
// sk:off        _T *  d = _buf;
// sk:off        while (( j < n_buf) && b_endptr)
// sk:off        {
// sk:off            *d = static_cast< _T >( s2n_c< _T >( b_data, &b_endptr));
// sk:off            ++d;
// sk:off            if ( b_data != b_endptr)
// sk:off            {
// sk:off                b_data = b_endptr;
// sk:off                while ( *b_data != '\0' && is_space(*b_data)) { ++b_data; }
// sk:off                if ( *b_data == '\0')
// sk:off                {
// sk:off                    b_endptr = NULL;
// sk:off                }
// sk:off            }
// sk:off            else
// sk:off            {
// sk:off                return  -1;
// sk:off            }
// sk:off            ++j;
// sk:off        }
// sk:off
// sk:off        return  j;
// sk:off    }

    template < typename  _T >
    int  string_split_whitespace( std::vector< _T > *  _buf,
            int  _n_buf, char *  _chrbuf /*NUL terminated*/)
    {
        char const *  b_data = _chrbuf;
        char *  b_endptr = _chrbuf;

        int  j = 0;

        while ( b_data && *b_data!='\0')
        {
            while ( *b_data!='\0' && is_space(*b_data))
                { ++b_data; }
            if ( *b_data=='\0')
                { break; }

            b_endptr = NULL; /* delimiter is whitespace */
            _T const  val = s2n_c< _T >( b_data, &b_endptr);
            if ( b_data != b_endptr)
            {
                if ( _n_buf > j)
                    { (*_buf)[j] = val; }
                else
                    { _buf->push_back( val); }
                b_data = b_endptr;
            }
            else
            {
                _buf->clear();
                return  -1;
            }
            ++j;
        }
        return  static_cast< int >( _buf->size());
    }
    template < typename  _T >
    int  string_split( std::vector< _T > *  _buf, int  _n_buf,
            char *  _chrbuf /*NUL terminated*/, char  _delim)
    {
        char  delim = _delim;
        char const *  b_data = _chrbuf;
        char *  b_endptr = _chrbuf;

        int  j = 0;

        while ( b_data && *b_data!='\0')
        {
            /* eat delimiters */
            while (( *b_data!='\0') && ( *b_data==delim))
            {
                /* if empty values are allowed, enqueue them */
                if ( s2n_properties< _T >::allow_empty == 1)
                {
                    if ( _n_buf > j)
                        { (*_buf)[j] = _T(); }
                    else
                        { _buf->push_back( _T()); }
                    ++j;
                }
                else
                {
                    _buf->clear();
                    return -1;
                }
                ++b_data;
            }
            if ( *b_data=='\0')
                { break; }

            b_endptr = &delim;
            _T const  val = s2n_c< _T >( b_data, &b_endptr);
            if (( b_data != b_endptr)
                && (( *b_endptr==delim) || ( *b_endptr=='\0')))
            {
                if ( _n_buf > j)
                    { (*_buf)[j] = val; }
                else
                    { _buf->push_back( val); }
                b_data = b_endptr;
                if ( *b_data==delim)
                    { ++b_data; }
            }
            else
            {
                _buf->clear();
                return  -1;
            }

            ++j;
        }
        return  static_cast< int >( _buf->size());
    }

    template < typename  _T >
    int  s2n_c( std::vector< _T > *  _buf, int  _n_buf,
            char *  _chrbuf /*NUL terminated*/, char  _delim = '\0')
    {
        if ( _delim=='\0')
            { return string_split_whitespace< _T >( _buf, _n_buf, _chrbuf); }
        return  string_split< _T >( _buf, _n_buf, _chrbuf, _delim);
    }
}  /* namespace cbm */


#endif  /*  !CBM_STRING_CONVERT_H_  */

