/*!
 * @brief
 *    canonicalized string
 *
 * @author
 *    steffen klatt (created on: feb 06, 2012 (forked from lutils.h))
 */

#ifndef  CBM_STRING_NSTR_H_
#define  CBM_STRING_NSTR_H_

#include  "crabmeat-common.h"
#include  "string/lstring-basic.h"

namespace  cbm
{
    lerr_t CBM_API  string_lstrip(
            char **, char const *, unsigned int * = NULL);
    char CBM_API  * string_lstrip(
            char const *, char const *, unsigned int * = NULL);
    lerr_t CBM_API  string_rstrip(
            char **, char const *, unsigned int * = NULL);
    char CBM_API  * string_rstrip(
            char const *, char const *, unsigned int * = NULL);
    lerr_t CBM_API  string_strip(
            char **, char const *, unsigned int * = NULL);
    char CBM_API  * string_strip(
            char const *, char const *, unsigned int * = NULL);

    /*!
     * @brief
     *    creates a new string where whitespace is stripped from
     *    head and tail and letters are converted to upper case.
     *
     * @note
     *    the behavior can be controlled by or'd flags:
     *    @li CNAME_STRIP_HEAD        strip from head of string (default)
     *    @li CNAME_STRIP_TAIL        strip from tail of string (default)
     *    @li CNAME_STRIP_TO_LOWER    convert to lower case
     *    @li CNAME_STRIP_TO_UPPER    convert to upper case (default)
     *
     *    (CNAME_STRIP_TO_LOWER and CNAME_STRIP_TO_UPPER are mutually
     *    exclusive)
     *
     *    the list of characters to strip off the string can be 
     *    overwritten.
     *
     * @note
     *    changing the default settings will break other code!
     */
    char CBM_API * canonical_name_new(
            char const *,
                   lflags_t = CNAME_STRIP_DEFAULTS,
            char const * = CNAME_WHITESPACE);
    /*!
     * @brief
     *    same as canonical_name_new( char const *...), this is the 
     *    std::string version
     *
     * @see canonical_name_new
     */
    char CBM_API * canonical_name_new(
            std::string const &,
                   lflags_t = CNAME_STRIP_DEFAULTS,
            char const * = CNAME_WHITESPACE);
    /*!
     * @brief
     *    for readability, we provide the complement, too.
     */
    void CBM_API  canonical_name_free(
            char *  _c);


    /*!
     * @brief
     *    normalized string wrapper class. a string passed
     *    to this class may be stripped or converted. @see
     *    @fn canonical_name_new
     */
    class  CBM_API  nstr
    {
        public:
            nstr();
            nstr(
                    std::string const &,
                           lflags_t = CNAME_STRIP_DEFAULTS,
                           char const * = CNAME_WHITESPACE);
            nstr(
                    char const *,
                           lflags_t = CNAME_STRIP_DEFAULTS,
                           char const * = CNAME_WHITESPACE);
            nstr(
                    cbm::nstr const &);

            nstr &  operator=(
                    cbm::nstr const &);

            nstr &  operator=(
                    char const *);

            nstr &  operator=(
                    std::string const &);

            ~nstr();

            std::string  str()
            const
            {
                return  std::string( this->cnorm_);
            }
            char const *  c_str()
            const
            {
                return  this->cnorm_;
            }

            //bool  operator==( cbm::nstr const &) const;
            //bool  operator==( char const *) const;
            //bool  operator==( std::string const &) const;

            char  operator[](
                    size_t  _k)
            const
            {
                return  this->cnorm_[_k];
            }

            size_t  size() const;
            
            lflags_t  flags()
            const
            {
                return  this->cflags_;
            }
            char const *  strip_chars()
            const
            {
                return  this->cstrip_;
            }

        private:
            char const *  cnorm_;
            lflags_t  cflags_;
            char const *  cstrip_;

            void  clear_();
    };

}  /* namespace cbm */


#endif  /*  !CBM_STRING_NSTR_H_  */

