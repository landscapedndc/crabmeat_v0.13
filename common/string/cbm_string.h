/*!
 * @brief
 *    crabmeat string class
 *
 * @author
 *    steffen klatt (created on: nov 16, 2016)
 */

#ifndef  CBM_STRING_H_
#define  CBM_STRING_H_

#include  "crabmeat-common.h"

#include  "lstring.h"
#include  "lstring-basic.h"
#include  "lstring-compare.h"
#include  "lstring-convert.h"
#include  "lstring-nstr.h"
#include  "lstring-tokenizer.h"
#include  "lstring-transform.h"

#endif  /*  !CBM_STRING_H_  */

