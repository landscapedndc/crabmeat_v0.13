/*!
 * @brief
 *    string transformation methods
 *
 * @author
 *    steffen klatt (created on: feb 06, 2012 (forked from lutils.h))
 */

#ifndef  CBM_STRING_TRANSFORM_H_
#define  CBM_STRING_TRANSFORM_H_

#include  "crabmeat-common.h"
#include  "string/lstring-basic.h"

namespace  cbm
{
    /*!
     * @brief
     *    convert string to lower case string
     */
    std::string  CBM_API  to_lower( std::string const &);
    std::string  CBM_API  to_lower( char const *);
    /*!
     * @brief
     *    convert char array to lower case (in place)
     */
    void  CBM_API  to_lower_inplace( char *);
    
    /*!
     * @brief
     *    convert string to upper case string
     */
    std::string  CBM_API  to_upper( std::string const &);
    std::string  CBM_API  to_upper( char const *);
    /*!
     * @brief
     *    convert char array to upper case (in place)
     */
    void  CBM_API  to_upper_inplace( char *);


    /*!
     * @brief
     *    replace all occurences of character sequence @p repl_src
     *    by character sequence @p repl_dst in string @p s
     *
     * @param
     *    s, string to be modified
     * @param
     *    repl_src, nul-terminated character sequence to replace
     * @param
     *    repl_dst, new nul-terminated character sequence
     *
     * @return
     *    number of replacements in units of @p repl_src
     */
    int  CBM_API  replace_all(
					std::string *, char const *, char const *);

    /*!
     * @brief
     *    expand ldndc specific format sequences in
     *    given string
     *
     * @example
     *    "logfile-%03r.txt" yields "logfile-001.txt" for
     *    node with rank 1
     */
    lerr_t  CBM_API  format_expand(
            std::string *, char const *, size_t = ldndc::invalid_t< size_t >::value);
    lerr_t  CBM_API  format_expand(
            std::string *, std::string const *);
    lerr_t  CBM_API  format_expand(
            std::string *);

    CBM_API std::string format_expand(
            std::string const & /*string*/);

    /* substitute ${<var>} occurences in @p string by the
     * value of environment variables <var> or empty string
     * if no match was found in environment
     */
    CBM_API std::string  expand_envvars(
            std::string const &  /*string*/);

}  /* namespace cbm */


#endif  /*  !CBM_STRING_TRANSFORM_H_  */

