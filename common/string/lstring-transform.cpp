/*!
 * @brief
 *    string transformation methods (implementation)
 *
 * @author
 *      steffen klatt (created on: feb 06, 2012 (forked from utils.cpp))
 */

#include  "string/lstring-transform.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"

#include  <iomanip>

static
void
case_convert(
        std::string * const  _s,
        int (* char_converter)( int))
{
    size_t  n( _s->size());

    for ( size_t  k = 0;  k < n;  ++k)
    {
        (*_s)[k] = static_cast< char >( char_converter( (*_s)[k]));
    }
}


std::string
cbm::to_lower(
        std::string const &  _s)
{
    // two copies ...
    std::string  s( _s);
    case_convert( &s, tolower);

    return  s;
}
std::string
cbm::to_lower(
        char const *  _c)
{
    return  to_lower( std::string( _c));
}
void
cbm::to_lower_inplace(
        char *  _c)
{
    if ( !_c)
        { return; }
    int  k( 0);
    while ( _c[k] != '\0')
    {
        _c[k] = static_cast< char >( tolower( _c[k]));
        ++k;
    }
}

std::string
cbm::to_upper(
        std::string const &  _s)
{
    // two copies ...
    std::string  s( _s);
    case_convert( &s, toupper);

    return  s;
}
std::string
cbm::to_upper(
        char const *  _c)
{
    return  to_upper( std::string( _c));
}
void
cbm::to_upper_inplace(
        char *  _c)
{
    if ( !_c)
        { return; }
    int  k( 0);
    while ( _c[k] != '\0')
    {
        _c[k] = static_cast< char >( toupper( _c[k]));
        ++k;
    }
}


int
cbm::replace_all(
        std::string *  _s,
        char const *  _repl_src,
        char const *  _repl_dst)
{
    crabmeat_assert( _repl_src && _repl_dst);

    size_t  src_len( strlen( _repl_src));
    size_t  dst_len( strlen( _repl_dst));

    size_t  k( 0);
    int  c( 0);
    while ( ( k < _s->size()) && (( k = _s->find( _repl_src, k)) != std::string::npos))
    {
        ++c;
        _s->replace( k, src_len, _repl_dst);
        k += dst_len;
    }

    return  c;
}

template < int  _N >
struct  lstring_format_expand_plan_t
{
	char  in_buf[_N+1];
	char *  in;

	char  out_buf[_N+1];
	char *  out;

	static int const  max_size = _N;

	int  depth;
};

#include  "cbm_rtcfg.h"
#include  "time/cbm_clock.h"
template < int  _N >
struct  lstring_format_expand_percent_t
{
    static int expand(
		lstring_format_expand_plan_t< _N > *  _fplan)
    {
        crabmeat_assert( _fplan);

        int  n_expansions = 0;

        int  expand = 0;
        int  pad_size = 0;
        char  pad_char = ' ';

        char const *  i_str = _fplan->in;
        char *  o_str = _fplan->out;

        int  n_print = 0;
        char const *  o_str_max = o_str + _fplan->max_size;

        while ( *i_str != '\0')
        {
            if ( !expand)
            {
                if ( *i_str == '%')
                {
                    expand = 1;

                    pad_size = 0;
                    pad_char = ' ';

                    ++i_str;
                    ++n_expansions;

                    continue;
                }
                else if (( i_str == _fplan->in) && ( *i_str == '~'))
                {
                    ++n_expansions;

                    /* fall through: expand tilde */
                }
                else
                {
                    /* append character to output */
                    *o_str++ = *i_str;

                    ++i_str;
                    continue;
                }
            }

            switch ( *i_str)
            {
                case  '0':
// sk:off                case  '-':
// sk:off                case  '_':
                    {
                        /*  zero pad number (possibly overwriting previously set pad character) */
                        if ( pad_size == 0)
                        {
                            pad_char = *i_str;
                            break;
                        }
                        implicit_fallthrough;
                    }
                case  '1':
                case  '2':
                case  '3':
                case  '4':
                case  '5':
                case  '6':
                case  '7':
                case  '8':
                case  '9':
                    {

                        pad_size = 10*pad_size + (*i_str-'1'+1);
                        break;
                    }

                case  'r':
                    {
                        char  rank_fmt[8] = "%d";
                        if ( pad_size > 0 && pad_size < 1000 /*insane limit*/)
                        {
                            cbm::snprintf( rank_fmt, 7, "%%%c%dd", pad_char, pad_size);
                        }
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, rank_fmt,
                                static_cast< int >( CBM_LibRuntimeConfig.rc.rank));
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'p':
                    {
                        char  pid_fmt[8] = "%d";
                        if ( pad_size > 0 && pad_size < 1000 /*insane limit*/)
                        {
                            cbm::snprintf( pid_fmt, 7, "%%%c%dd", pad_char, pad_size);
                        }
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, pid_fmt,
                                static_cast< int >( cbm::get_pid()));
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'C':
                    {
                        std::string const  m_dotpath = CBM_LibRuntimeConfig.io.dot_path;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_dotpath.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'I':
                    {
                        std::string const  m_inputpath = CBM_LibRuntimeConfig.io.input_path;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_inputpath.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'O':
                    {
                        std::string const  m_outputpath = CBM_LibRuntimeConfig.io.output_path;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_outputpath.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'R':
                    {
                        std::string const  m_resourcespath = CBM_LibRuntimeConfig.io.resources_path;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_resourcespath.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

                case  'i':
                    {
                        std::string const  m_sourceprefix = CBM_LibRuntimeConfig.pc.global_source_prefix;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_sourceprefix.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'o':
                    {
                        std::string const  m_sinkprefix = CBM_LibRuntimeConfig.pc.global_sink_prefix;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_sinkprefix.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'S':
                    {
                        std::string const  m_scenarioname = CBM_LibRuntimeConfig.pc.scenario_name;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_scenarioname.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

                case  '$':
                    {
                        std::string const  m_projectname = CBM_LibRuntimeConfig.pc.project_name;
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_projectname.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

                case  'h':
                    {
                        std::string const  m_hostname = cbm::hostname();
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_hostname.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

                case  'u':
                    {
                        std::string const  m_username = cbm::username();
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, m_username.c_str());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

                case  'Y': /*current (0000-?) year from global clock*/
                    {
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, "%04d",
                                CBM_LibRuntimeConfig.clk->year());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'M': /*current month (01-12) from global clock*/
                    {
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, "%02d",
                                CBM_LibRuntimeConfig.clk->month());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'D': /*current day (01-31) from global clock*/
                    {
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, "%02d",
                                CBM_LibRuntimeConfig.clk->day());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  's': /*current subday (00001-%T) from global clock*/
                    {
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, "%05d",
                                CBM_LibRuntimeConfig.clk->subday());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }
                case  'T': /*current time resolution (1-86400) from global clock*/
                    {
                        n_print = cbm::snprintf( o_str, o_str_max-o_str, "%d",
                                CBM_LibRuntimeConfig.clk->time_resolution());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

                case  'V':
                    {
                        n_print = cbm::snprintf( o_str,
                            o_str_max-o_str, package_user.version());
                        o_str += n_print;
                        expand = 0;
                        break;
                    }

// sk:troublemaker                case  '%':
// sk:troublemaker                    {
// sk:troublemaker                        *o_str++ = '%';
// sk:troublemaker                        expand = 0;
// sk:troublemaker                        break;
// sk:troublemaker                    }
                case  '~':
                    {
                        if ( i_str == _fplan->in)
                        {
                            if ( cbm::home_directory_found())
                            {
                                std::string const  m_homedir = cbm::home_directory();
                                n_print = cbm::snprintf( o_str, o_str_max-o_str, m_homedir.c_str());
                                o_str += n_print;
                            }
                            else
                            {
                                CBM_LogError( "unable to obtain user's home directory");
                                *o_str++ = '~';
                            }
                        }
                        else
                        {
                            *o_str++ = '~';
                        }
                        expand = 0;
                        break;
                    }

                default:
                    {
                        CBM_LogWarn( "unknown format sequence  [", *i_str, "]");
                        expand = 0;
                        return  -2;
                    }
            }
            ++i_str;

            if ( o_str > o_str_max)
            {
                return  -1;
            }
        }
        *o_str = '\0';

        return  n_expansions;
    }
};

template < int  _N >
struct  lstring_format_expand_envvars_t
{
    static int expand(
		lstring_format_expand_plan_t< _N > *  _fplan)
    {
        crabmeat_assert( _fplan);

        int  n_expansions = 0;
        int  brace_open = 0;

        /* ex:  "foo_${ba${R}}_ba${S}z" -> find "${R}" and "${S}" */
        char *  i_str = _fplan->in, *  I_str = _fplan->in;
        char *  o_str = _fplan->out;
        char *  p_dollar = NULL;

        char const *  o_str_max = o_str + _fplan->max_size;

        while ( *i_str!='\0')
        {
            if ( *i_str=='$' )
            {
                p_dollar = i_str;
                brace_open = 0;
            }
            else if ( *i_str=='{' && p_dollar) // && !brace_open)
            {
                brace_open += 1;
            }
            else if ( *i_str=='}' && brace_open && p_dollar)
            {
                brace_open -= 1;

                if ( p_dollar == (i_str-2))
                {
                    /* empty: "${}" */
                    p_dollar = NULL;
                }
                else if ( !brace_open)
                {
                    /* found inner most expression '${.*}' */
                    n_expansions += 1;

                    *i_str = '\0';
                    *p_dollar = '\0';
                    char const *  envval = cbm::getenv( p_dollar+2);
                    if ( !envval)
                    {
                        envval = ldndc_empty_cstring;
                    }
                    int const  n_print =
                        cbm::snprintf( o_str, o_str_max-o_str, "%s%s", I_str, envval);
                    o_str += n_print;
                    if ( o_str > o_str_max)
                    {
                        return  LDNDC_ERR_RUNTIME_ERROR;
                    }
                    I_str = i_str+1;
                    p_dollar = NULL;
                }
            }
            ++i_str;
        }

        if ( n_expansions > 0)
        {
            int const  n_print =
                cbm::snprintf( o_str, o_str_max-o_str, "%s", I_str);
            o_str += n_print;
            if ( o_str > o_str_max)
            {
                return  LDNDC_ERR_RUNTIME_ERROR;
            }
        }
        return  n_expansions;
    }
};

template < bool _P /*percent sequences*/, bool _E /*expand environment variables*/,
        int  _N = 4095 /*maximum buffer size*/, int  _D = 8 /*maximum "recursion" depth*/ >
struct  lstring_format_expand_t
{
    static
        lerr_t
        expand(
                std::string *  _o_str, char const *  _i_str, size_t  _i_str_size)
        {
            crabmeat_assert( _o_str && _i_str);
            if ( _i_str_size > _N)
            {
                return  LDNDC_ERR_INVALID_ARGUMENT;
            }

            lerr_t  rc_expand = LDNDC_ERR_CONT_ITERATION;

            lstring_format_expand_plan_t< _N >  fplan;
            fplan.in = fplan.in_buf;
            fplan.in[fplan.max_size] = '\0';
            fplan.out = fplan.out_buf;
            fplan.out[fplan.max_size] = '\0';
            fplan.depth = 0;

            cbm::strncpy( fplan.in, _i_str, _i_str_size);
            fplan.in[_i_str_size] = '\0';

            while ( rc_expand == LDNDC_ERR_CONT_ITERATION)
            {
                int const  n_expand_p =
                    !_P ? 0 : lstring_format_expand_percent_t< _N >::expand( &fplan);
                if ( !_P)
                {
                    /* pretend action */
                    cbm::swap_values( fplan.in, fplan.out);
                }
                if ( n_expand_p >= 0)
                {
                    cbm::swap_values( fplan.in, fplan.out);

                    int const  n_expand_e =
                        !_E ? 0 : lstring_format_expand_envvars_t< _N >::expand( &fplan);
                    if ( n_expand_e < 0)
                    {
                        rc_expand = LDNDC_ERR_FAIL;
                    }
                    else if (( n_expand_p == 0) && ( n_expand_e == 0))
                    {
                        rc_expand = LDNDC_ERR_OK;
                    }
                    else if	( ++fplan.depth == _D)
                    {
                        fprintf( stderr, "maximum format expansion depth reached, stopping loop"
                                "  [string=%s]\n", _i_str);
                        rc_expand = LDNDC_ERR_STOP_ITERATION;
                    }
                    else if ( n_expand_e > 0)
                    {
                        cbm::swap_values( fplan.in, fplan.out);
                    }
                }
                else
                {
                    rc_expand = LDNDC_ERR_FAIL;
                }
            }

            *_o_str = fplan.out;
            return  rc_expand;
        }
};
lerr_t
cbm::format_expand(
        std::string *  _o_str, char const *  _i_str, size_t  _i_str_size)
{
    size_t  i_str_size( _i_str_size);
    if ( _i_str_size == ldndc::invalid_t< size_t >::value)
    {
        i_str_size = cbm::strlen( _i_str);
    }
    return  lstring_format_expand_t< true, true >::expand(
                _o_str, _i_str, i_str_size);
}
lerr_t
cbm::format_expand(
            std::string *  _o_str,
            std::string const *  _i_str)
{
    return  lstring_format_expand_t< true, true >::expand(
                _o_str, _i_str->c_str(), _i_str->size());
}
lerr_t
cbm::format_expand(
            std::string *  _str)
{
    std::string  expanded_str;
    lerr_t const  rc_expand =
        lstring_format_expand_t< true, true >::expand(
                &expanded_str, _str->c_str(), _str->size());
    if ( rc_expand == LDNDC_ERR_OK)
    {
        if ( _str)
        {
            *_str = expanded_str;
        }
    }
    return  rc_expand;
}

std::string
cbm::format_expand(
        std::string const &  _str)
{
    std::string  expanded_str;
    int const  rc_expand =
        lstring_format_expand_t< true, true >::expand(
                &expanded_str, _str.c_str(), _str.size());
    if ( rc_expand == LDNDC_ERR_OK)
    {
        return  expanded_str;
    }
    return  _str;
}

std::string
cbm::expand_envvars(
        std::string const &  _str)
{
    std::string  expanded_str;
    int const  rc_expand =
        lstring_format_expand_t< false, true >::expand(
                &expanded_str, _str.c_str(), _str.size());
    if ( rc_expand == LDNDC_ERR_OK)
    {
        return  expanded_str;
    }
    return  _str;
}

