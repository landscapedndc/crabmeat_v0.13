/*!
 * @brief
 *    ldndc string class
 *
 * @author
 *    steffen klatt (created on: mar 18, 2013),
 *    edwin haas
 */

#ifndef  CBM_STRING_T_H_
#define  CBM_STRING_T_H_

#include  "crabmeat-common.h"

#include  "string/lstring-basic.h"
#include  "string/lstring-tokenizer.h"
#include  "string/lstring-compare.h"
#include  "string/lstring-convert.h"

#include  <vector>
#include  <cstdarg>

namespace  cbm
{
    class CBM_API  string_t
    {
        public:
            template < typename _T >
            struct  array_t
            {
                typedef  std::vector< _T >  type_t;
            };

        public:
            enum
            {
                NONE = 0x0,
                FORMAT_EXPAND = 0x1
            };
        private:
            static  void  _vmake( char * /*buffer*/, size_t /*buffer size*/,
                char const * /*format*/, va_list /*args*/);
        public:
            template < size_t _MAXSIZE = 128 >
            struct  format_t
            {
                static  string_t  vmake( char const *  _format, va_list  _args)
                {
                    char  char_array[_MAXSIZE];
                    string_t::_vmake( char_array, _MAXSIZE, _format, _args);
                    return  string_t( char_array);
                }

                static  string_t  make( char const *  _format, ...)
                {
                    va_list  args;
                    va_start( args, _format);
                    string_t const  vstr( vmake( _format, args));
                    va_end( args);

                    return  vstr;
                }
            };

        public:
            string_t();
            string_t( cbm::string_t const &);
            string_t( cbm::string_t const &  _str,
                size_t  _offset, size_t  _length = invalid_size)
            {
				this->buf = std::string( _str.buf, _offset,
                    (_length == invalid_size) ? std::string::npos : _length);
		   	}
            string_t( std::string const &, lflags_t const & = NONE);
            string_t( char const *, lflags_t const & = NONE);
            string_t( char  _chr, size_t  _length)
                { this->buf = std::string( _length, _chr); }
            /* from sequence with replace sets */
            string_t( char const * /*string*/, size_t /*length*/,
                char const * /*set 1*/, char const * /*set 2*/, size_t /*size of sets*/);

            ~string_t();

            template < typename _T >
                string_t &  operator<<( _T const &  _data)
                {
                    std::ostringstream  is;
                    is << this->buf << _data;
                    this->buf = is.str();
                    return *this;
                }
            size_t  assign( char const *  _chararray, size_t  _chararray_sz)
            {
                this->buf.assign( _chararray, _chararray_sz);
                return  this->buf.size();
            }

            bool  is_empty() const { return  this->buf.empty(); }
            size_t  size() const { return  this->buf.size(); }
            size_t  length() const { return  this->buf.size(); }
            void  clear() { this->buf.clear(); }

            string_t &  operator=( cbm::string_t const &);
            string_t &  operator=( std::string const &);
            string_t &  operator=( char const *);

            string_t  operator+( cbm::string_t const &);
            string_t  operator+( std::string const &);
            string_t  operator+( char);
            string_t  operator+( char const *);

            string_t &  operator+=( cbm::string_t const &);
            string_t &  operator+=( std::string const &);
            string_t &  operator+=( char);
            string_t &  operator+=( char const *);

            bool  operator==( cbm::string_t const &  _str) const
                { return  this->buf == _str.buf; }
            bool  operator==( std::string const &  _str) const
                { return  this->buf == _str; }
            bool  operator==( char const *  _str) const
                { return  cbm::is_equal( this->buf.c_str(), _str); }

            bool  operator!=( cbm::string_t const &  _str) const
                { return  !this->operator==( _str); }
            bool  operator!=( std::string const &  _str) const
                { return  !this->operator==( _str); }
            bool  operator!=( char const *  _str) const
                { return  !this->operator==( _str); }

            char &  operator[]( size_t  _k) { return  this->buf[_k]; }
            char const &  operator[]( size_t  _k) const { return  this->buf[_k]; }

            char const *  c_str() const { return  this->buf.c_str(); }
            std::string const &  str() const { return  this->buf; }

        public:
            lerr_t  format_expand();
            string_t &  expand();

            cbm::tokenizer  tokenize(
                    char=' ', lflags_t = CNAME_STRIP, char const * = CNAME_WHITESPACE) const;

            string_t  strip( char const * = NULL /*characters*/) const;
            string_t  lstrip( char const * = NULL /*characters*/) const;
            string_t  rstrip( char const * = NULL /*characters*/) const;

            string_t  to_lower() const;
            string_t  to_upper() const;

            bool  startswith( char const * /*string*/) const;
            bool  startswith( string_t const & /*string*/) const;
            bool  endswith( char const * /*string*/) const;
            bool  endswith( string_t const & /*string*/) const;

            int  find( char) const;
            int  rfind( char) const;

        public:
            template < typename _T >
            _T  as() const { return  cbm::s2n_c< _T >( this->buf.c_str(), NULL); }

            ldndc_bool_t  as_bool() const { return  this->as< ldndc_bool_t >(); }
            ldndc_char_t  as_char() const { return  this->as< ldndc_char_t >(); }
            ldndc_flt32_t  as_float32() const { return  this->as< ldndc_flt32_t >(); }
            ldndc_flt64_t  as_float64() const { return  this->as< ldndc_flt64_t >(); }
            ldndc_int32_t  as_int32() const { return  this->as< ldndc_int32_t >(); }
            ldndc_uint32_t  as_uint32() const { return  this->as< ldndc_uint32_t >(); }
            ldndc_int64_t  as_int64() const { return  this->as< ldndc_int64_t >(); }
            ldndc_uint64_t  as_uint64() const { return  this->as< ldndc_uint64_t >(); }
            template < typename  _T >
            _T *  as_memaddr() const
            {
                return  reinterpret_cast< _T * >( this->as< uintptr_t >());
            }

        public:
            template < typename  _T >
            typename array_t< _T >::type_t  as_array( char  _delim='\0') const
            {
                typename array_t< _T >::type_t  arr;

                if ( this->size() == 0)
                    { return  arr; }

                char *  arr_str = cbm::strdup( this->buf.c_str());
                if ( arr_str)
                {
                    (void)cbm::s2n_c< _T >( &arr, -1, arr_str, _delim);
                    cbm::strfree( arr_str);
                }

                return  arr;
            }

            typedef  array_t< bool >::type_t  bool_array_t;
            bool_array_t  as_bool_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_bool_t >( _delim);
            }
            typedef  array_t< char >::type_t  char_array_t;
            char_array_t  as_char_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_char_t >( _delim);
            }
            typedef  array_t< std::string >::type_t  string_array_t;
            string_array_t  as_string_array( char  _delim='\0') const
            {
                return  this->as_array< std::string >( _delim);
            }
            typedef  array_t< ldndc_flt32_t >::type_t  float32_array_t;
            float32_array_t  as_float32_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_flt32_t >( _delim);
            }
            typedef  array_t< ldndc_flt64_t >::type_t  float64_array_t;
            float64_array_t  as_float64_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_flt64_t >( _delim);
            }
            typedef  array_t< ldndc_int32_t >::type_t  int32_array_t;
            int32_array_t  as_int32_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_int32_t >( _delim);
            }
            typedef  array_t< ldndc_uint32_t >::type_t  uint32_array_t;
            uint32_array_t  as_uint32_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_uint32_t >( _delim);
            }
            typedef  array_t< ldndc_int64_t >::type_t  int64_array_t;
            int64_array_t  as_int64_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_int64_t >( _delim);
            }
            typedef  array_t< ldndc_uint64_t >::type_t  uint64_array_t;
            uint64_array_t  as_uint64_array( char  _delim='\0') const
            {
                return  this->as_array< ldndc_uint64_t >( _delim);
            }

        public:
            typedef  array_t< string_t >::type_t  cbmstring_array_t;
            cbmstring_array_t  split( char  _delim='\0') const
            {
                string_array_t  arr =
                    this->as_array< std::string >( _delim);
                cbmstring_array_t  carr;
                for ( size_t  k = 0;  k < arr.size();  ++k)
                    { carr.push_back( arr[k]); }
                return  carr;
            }

        private:
            std::string  buf;
    };
    struct CBM_API string_array_t
    {
        typedef string_t::array_t< string_t >::type_t array_t;
        string_array_t() { }
        string_array_t( string_t const &  _string, char  _delim='\0')
            { m_array = _string.split( _delim); }

        size_t  size() const
            { return this->m_array.size(); }

        string_t &  operator[]( size_t  _k)
            { return this->m_array[_k]; }
        string_t const &  operator[]( size_t  _k) const
            { return this->m_array[_k]; }

        bool  contains( char const *  _string) const
        {
            for ( size_t  k = 0;  k < this->m_array.size();  ++k)
            {
                if ( cbm::is_equal( this->m_array[k].c_str(), _string))
                    { return true; }
            }
            return false;
        }
        bool  contains( string_t const &  _string) const
            { return this->contains( _string.c_str()); }

    private:
        array_t  m_array;
    };

    struct CBM_API key_value_t
    {
        cbm::string_t  key;
        cbm::string_t  value;
    };
    key_value_t CBM_API as_key_value( char const * /*"key"="value"*/);

    std::ostream CBM_API &
    operator<<( std::ostream &, cbm::string_t const &);

    template < typename _T >
        cbm::string_t
        join( _T const *  _array, size_t  _array_size, 
                    char const *  _delim = "",
                size_t  _offset = 0, size_t  _count = 0)
        {
            int  array_size = _array_size;
            int  offset = _offset;
            int  count = _count + offset;
            if ( _count == 0 || count >= array_size)
                { count = array_size; }

            cbm::string_t  s;
            for ( int  c = offset;  c < count;  ++c)
            {
                if ( c != offset)
                    { s << _delim; }
                s << _array[c];
            }
            return  s;
        }
    template < typename _T >
        cbm::string_t
        join( _T const &  _array, char const *  _delim = "",
                size_t  _offset = 0, size_t  _count = 0)
        {
            return  join( &_array[0], _array.size(),
                        _delim, _offset, _count);
        }

}  /* namespace cbm */
extern CBM_API cbm::string_t const  invalid_lstring;

namespace cbm {
    typedef cbm::string_t string_t; }

#endif  /*  !CBM_STRING_T_H_  */

