
#ifndef  CRABMEAT_CONFIG_H_
#define  CRABMEAT_CONFIG_H_


#include  "crabmeat-config.h.inc"

#if  defined(CRABMEAT_HAVE_INTTYPES_H) && !defined(__STDC_FORMAT_MACROS)
#  define  __STDC_FORMAT_MACROS
#endif
#if  defined(CRABMEAT_HAVE_STDINT_H)
#  include  <stdint.h>
#elif  defined(_MSC_VER)
#  include  "cbm_stdint.h"
#elif  defined(CRABMEAT_HAVE_CRTDEFS_H)
#  include  <crtdefs.h>
#else
  /*  any alternatives? */
#endif

#include  "crabmeat-compiler.h"
#include  "crabmeat-os.h"
#include  "crabmeat-arch.h"

#if defined(__GNUC__) && __GNUC__ >= 7
#  define implicit_fallthrough __attribute__ (( fallthrough ))
#else
#  define implicit_fallthrough ( (void)0 )
#endif /* __GNUC__ >= 7 */

#define  CBM_IsCreated(obj) ((obj).mem!=0)

#endif  /*  !CRABMEAT_CONFIG_H_  */

