/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: oct 01, 2016)
 */

#ifndef  CBM_CPL_CPL_H_
#define  CBM_CPL_CPL_H_

#include  "cbm_cfg.h"

#include  "cbm_type.h"
#include  "cbm_event.h"
#include  "cbm_transform.h"

#include  <map>

#if defined(__cplusplus)
extern "C" {
#endif

/* flags for publish operation [CBM_CouplerPublishField()] */
#define  CBM_PUB_CREATESKELETON   0x00000001
#define  CBM_PUB_TRANSIENT        0x00000002
#define  CBM_PUB_PERSISTENT       0x00000004

/* flags for send operation [CBM_CouplerSendField()] */
// sk:todo #define  CBM_SEND_NOTRANSFORM   0x00000001

/* flags for subscribe operation [CBM_CouplerSubscribeField()] */
// sk:todo #define  CBM_SUB_   0x00000001
#define  CBM_SUB_PERSISTENT       0x00000002

/* flags for receive operation [CBM_CouplerReceiveField()] */
#define  CBM_RECV_CLEAR           0x00000001
// sk:todo #define  CBM_RECV_NOTRANSFORM   0x00000002

/* opaque handle for field */
typedef struct
{
    size_t  ID;
} CBM_Handle;
CBM_API extern CBM_Handle const CBM_None;
#define  CBM_HandleId(handle) ((void*)(handle).ID)
#define  CBM_HandleOk(handle) ((handle).ID!=CBM_None.ID)
#define  CBM_HandleEqual(h1,h2) ((h1).ID==(h2).ID)



typedef struct
{
    /* ID based */
    int  ID;
    /* Location based */
//    double  x,y,z; /*centroid*/
//    double  dx, dy, dz; /*bounding box*/
} CBM_Spatial;
CBM_API extern CBM_Spatial const CBM_SpatialNone;
#define  CBM_SpatialOk(s) ((s).ID!=CBM_SpatialNone.ID)
#define  CBM_SpatialEqual(s1,s2) ((s1).ID==(s2).ID)



typedef struct
{
    int  dT; /*seconds*/
} CBM_Temporal;
CBM_API extern CBM_Temporal const  CBM_TemporalNone;
#define  CBM_TemporalOk(t) ((t).dT!=CBM_TemporalNone.dT)
#define  CBM_TemporalEqual(t1,t2) ((t1).dT==(t2).dT)


typedef struct CBM_CouplerStats
{
    std::map< std::string, CBM_FieldStats >  data;
} CBM_CouplerStats;

typedef struct CBM_CouplerEnvironment
{
    int  allow_skeletons;
    int  collect_stats;
} CBM_CouplerEnvironment;
CBM_API int CBM_CouplerEnvironmentInitialize( CBM_CouplerEnvironment *);


typedef struct CBM_Coupler
{
    void *  mem;
} CBM_Coupler;

CBM_API CBM_Coupler  CBM_CouplerCreate( char const * /*name*/,
        CBM_CouplerEnvironment const *, CBM_CouplerStats *, int * /*rc*/);
CBM_API void  CBM_CouplerDestroy( CBM_Coupler /*coupler*/);

CBM_API CBM_Handle  CBM_CouplerPublishField(
        CBM_Coupler /*coupler*/, char const * /*uri*/,
    void * /*data*/, size_t /*data size*/, CBM_Datatype  /*data type*/,
    int /*flags*/, int * /*rc*/);
CBM_API void  CBM_CouplerUnpublishField(
    CBM_Coupler /*coupler*/, CBM_Handle /*field*/, int * /*rc*/);
CBM_API CBM_Handle  CBM_CouplerSubscribeField(
    CBM_Coupler /*coupler*/, char const * /*uri*/,
    CBM_Transform * /*send-transform*/, CBM_Callback /*send-callback*/,
    int /*flags*/, int * /*rc*/);
CBM_API void  CBM_CouplerUnsubscribeField(
    CBM_Coupler /*coupler*/, CBM_Handle /*field*/, int * /*rc*/);

CBM_API size_t  CBM_CouplerSendField( CBM_Coupler, CBM_Handle,
    void const * /*data*/, size_t /*data size*/,
        CBM_Datatype, int /*flags*/, int * /*rc*/);
CBM_API size_t  CBM_CouplerReceiveField( CBM_Coupler, CBM_Handle,
    void * /*data*/, size_t /*data size*/,
        CBM_Datatype, int /*flags*/, int * /*rc*/);


typedef struct CBM_FieldPingInfo
{
    size_t  size;
    CBM_Datatype  type;
    CBM_Temporal  temporal;
} CBM_FieldPingInfo;
CBM_API extern CBM_FieldPingInfo const CBM_FieldPingInfoNone;

CBM_API int  CBM_CouplerPingField( CBM_Coupler /*coupler*/,
    CBM_Handle /*field*/, CBM_FieldPingInfo *, int * /*rc*/);


CBM_API int  CBM_CouplerNumberOfRegisteredFields( CBM_Coupler);
CBM_API char const *  CBM_CouplerGetFieldUri( CBM_Coupler, CBM_Handle);
CBM_API char const *  CBM_CouplerGetFieldUnit( CBM_Coupler, CBM_Handle, 
        char * /*buffer*/, size_t /*buffer size*/);

CBM_API int  CBM_CouplerWriteSummaryTable( CBM_CouplerStats *, int /*options*/);

#if defined(__cplusplus)
}
#endif

#endif  /* !CBM_CPL_CPL_H_ */

