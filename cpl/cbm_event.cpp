/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 10, 2017)
 */

#include  "cbm_event.h"

#if defined(__cplusplus)
extern "C" {
#endif

CBM_Callback const CBM_CallbackNone = { NULL, NULL };

#if defined(__cplusplus)
}
#endif

