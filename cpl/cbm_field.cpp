/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: jun 29, 2016)
 */

#include  "cbm_field.h"

#include  "log/cbm_baselog.h"
#include  "comm/cbm_comm.h"
#include  "string/cbm_string.h"
#include  "memory/cbm_mem.h"
#include  "hash/cbm_hash.h"


int CBM_FieldUriNormalize(
    CBM_Field *  _field, cbm::uri_t const *  _uri)
{
    CBM_Assert(_field);
    return  CBM_UriNormalize( _field->uri, _uri, 0);
}
int CBM_FieldUriSetSpatial(
    CBM_Field * /*_field*/, cbm::uri_t const * /*_uri*/)
{
    /*CBM_UriQSpatial*/
    return  0;
}
int CBM_FieldUriSetTemporal(
    CBM_Field * /*_field*/, cbm::uri_t const * /*_uri*/)
{
    /*CBM_UriQTemporal*/
    return  0;
}
int CBM_FieldUriSetUnit(
    CBM_Field *  _field, cbm::uri_t const *  _uri)
{
    CBM_Assert(_field);
    CBM_Assert(_uri);

    char const *  unit = _uri->query( CBM_UriQUnit);
    int const  rc_unit =
        CBM_UnitsParse( &_field->unit, unit);
    if ( rc_unit)
        { return  -1; }
    return  0;
}

#if defined(__cplusplus)
extern "C" {
#endif

static void *  _cbm_alloc( CBM_MemPool  _mempool, size_t  _bytes)
    { return  CBM_MAlloc( _mempool, _bytes, 0); }
static void  _cbm_free( void *  /*_mem*/)
    { /* no op */ }

CBM_Field *  CBM_FieldCreate( CBM_MemCache  _memcache,
       char const *  _uri, int *  _rc)
{
    cbm_status(_rc,CBM_RET_OK);

    if ( !_uri)
    {
        cbm_status(_rc,CBM_ERR_EMPTY);
        return  NULL;
    }

    CBM_Field *  field =
        (CBM_Field *)CBM_MemCacheAlloc( _memcache, 0);
    if ( !field)
    { 
        cbm_status(_rc,CBM_ERR_NOMEM);
        return  field;
    }

    strcpy( field->uri, _uri);

    cbm::uri_t  uri( field->uri);
    if ( CBM_FieldUriSetSpatial( field, &uri))
        { CBM_MemCacheFree( _memcache, field); field = NULL; }
    if ( CBM_FieldUriSetTemporal( field, &uri))
        { CBM_MemCacheFree( _memcache, field); field = NULL; }
    if ( CBM_FieldUriSetUnit( field, &uri))
        { CBM_MemCacheFree( _memcache, field); field = NULL; }
    if ( CBM_FieldUriNormalize( field, &uri))
        { CBM_MemCacheFree( _memcache, field); field = NULL; }
    if ( field == NULL)
    {
        cbm_status(_rc,CBM_ERR_PATH);
        CBM_LogError( "Failed to parse URI \"",_uri,"\"");
        return  NULL;
    }

    field->sub_count = 0;
    field->pub_count = 0;

    field->alloc = &_cbm_alloc;
    field->free = &_cbm_free;

    field->head = 0;
    field->next = 0;
    field->send_transform = 0;
    field->recv_transform = 0;
    field->send_callback = CBM_CallbackNone;

    field->buf.p = 0;
    field->buf_size = CBM_FieldMinAllocSize;
    field->msg_size = 0;
    field->type = CBM_VOID;

    field->stats = NULL;

    CBM_Assert(CBM_FieldIsSkeleton(field));
    return  field;
}

void  CBM_FieldDestroy( CBM_MemCache  _memcache,
        CBM_Field *  _field)
{
    CBM_Assert(_field);

    if ( _field->head == 0) /* "invalid" */
        { /*no op*/ }
    else if ( CBM_FieldIsProducer(_field))
    {
        CBM_Field *  consumer_field = _field->next;
        while ( consumer_field)
        {
// sk:dbg            CBM_LogDebug( "consumer .. @", (void*)consumer_field->next);
            /* clear consumer's producer entry */
            consumer_field->head = 0;
            consumer_field = consumer_field->next;
        }
    }
    else if ( CBM_FieldIsConsumer(_field))
    {
        CBM_Field *  consumer_field = _field->head;
        while ( consumer_field->next != _field)
            { consumer_field = consumer_field->next; }
        /* remove myself from consumer list */
        consumer_field->next = consumer_field->next->next;
    }
    else
        { CBM_Assert( 1); }

// sk:dbg        CBM_LogDebug( "\"",_field->uri,"\"  buf_size=",_field->buf_size,
// sk:dbg            "  use-stack=",CBM_FieldUseStack(_field));

    if ( !CBM_FieldIsSkeleton(_field))
        { CBM_FieldUnsetData( _field, NULL); }

    CBM_MemCacheFree( _memcache, _field);
}

void  CBM_FieldSetUnitFromUri( CBM_Field *  _field,
        char const *  _uri, int *  _rc)
{
    cbm::uri_t  uri( _uri);
    int const  rc_unit =
        CBM_FieldUriSetUnit( _field, &uri);
    if ( _rc)
    {
        if ( rc_unit)
            { *_rc = CBM_ERR_INVALID; }
        else
            { *_rc = CBM_RET_OK; }
    }
}

void
CBM_FieldInitializeData( CBM_Field *  _field, void *  _buffer)
{
    if ( _buffer)
    {
// sk:dbg        CBM_LogDebug( "Initializing field storage  [P=",CBM_FieldIsProducer(_field),"]");
        memcpy( CBM_FieldStorage(_field), _buffer, _field->buf_size);
        _field->msg_size = _field->buf_size;
    }
    else
        { memset( CBM_FieldStorage(_field), 0, _field->buf_size); }
}

void  CBM_FieldSetData( CBM_MemPool  _mempool, CBM_Field *  _field, void *  _buffer,
        size_t  _size, CBM_Datatype  _type, int *  _rc)
{
    CBM_Assert(!CBM_TypesAreEqual(_type,CBM_VOID));
    int  rc = CBM_RET_OK;

    _field->msg_size = 0;
    _field->buf_size = CBM_FieldMinAllocSize;
    _field->buf.p = NULL;
    _field->type = _type;

    if ( _size == (size_t)CBM_DynamicSize)
        { _field->msg_size = CBM_DynamicSize; }
    else
    {
        _field->buf_size = _size*_type.size;

        if ( !CBM_FieldUseStack(_field))
        {
            CBM_LogDebug( "Using allocated storage for field \"",_field->uri,"\"",
                    "  [buf_size=",_field->buf_size,"]");
            /* allocate if reserved storage too small */
            _field->buf.p =
                _field->alloc( _mempool, _field->buf_size);
            if ( _field->buf.p == NULL)
                { rc = CBM_ERR_NOMEM; }
        }
        else
        {
            CBM_LogDebug( "Using static storage for field \"",_field->uri,"\"",
                    "  [buf_size=",_field->buf_size,"]");
        }
        if ( rc == CBM_RET_OK)
            { CBM_FieldInitializeData( _field, _buffer); }
    }

    if ( rc)
    {
        CBM_FieldUnsetData( _field, NULL);
    }
    else
        { CBM_Assert(!CBM_FieldIsSkeleton(_field)); }

    if ( _rc)
        { *_rc = rc; }
}

void  CBM_FieldUnsetData( CBM_Field *  _field, int *  _rc)
{
    if ( _rc)
        { *_rc = CBM_RET_OK; }

    if ( !CBM_FieldUseStack(_field)
            && !CBM_FieldIsAlias(_field))
    {
        if ( _field->free && _field->buf.p)
            { _field->free( _field->buf.p); }
    }

    _field->msg_size = 0;
    _field->buf_size = CBM_FieldMinAllocSize;
    _field->buf.p = NULL;
    _field->type = CBM_VOID;

    CBM_Assert(CBM_FieldIsSkeleton(_field));
}

void  CBM_FieldResetData( CBM_MemPool  _mempool,
    CBM_Field *  _field, size_t  _size, int *  _rc)
{
    /* realloc if needed */
    if ( _field->buf_size < ((int)_size)*_field->type.size)
    {
        CBM_Datatype  type = _field->type;

        CBM_FieldUnsetData( _field, NULL);
        CBM_FieldSetData( _mempool, _field, NULL, _size, type, _rc);
    }
    else
        { if ( _rc)
            { *_rc = CBM_RET_OK; } }

    _field->msg_size = 0;
}

int  CBM_FieldsCompatible( CBM_Field *  _field1, CBM_Field *  _field2)
{
    CBM_Assert(_field1 && _field2);

    if (( CBM_TypesAreEqual( _field1->type, _field2->type))
        && ( CBM_UnitsCompatible( _field1->unit, _field2->unit)))
    {
        return  1;
    }
    return  0;
}

char const *  CBM_FieldGetUri( CBM_Field *  _field)
{
    return  _field->uri;
}

int  CBM_FieldPubCount( CBM_Field *  _field)
{
    if ( !_field)
        { return  -1; }
    return  _field->pub_count;
}

int  CBM_FieldSubCount( CBM_Field *  _field)
{
    if ( !_field)
        { return  -1; }
    return  _field->sub_count;
}

CBM_Hash  CBM_FieldHashify( CBM_Field *  _field)
{
    return  cbm::hash::djb2( _field->uri);
}

#if defined(__cplusplus)
} /* extern C */
#endif


int CBM_UriNormalize(
    char *  _urinorm, cbm::uri_t const *  _uri, int  _flags)
{
    CBM_Assert(_urinorm);
    CBM_Assert(_uri);

    char *  urinorm = _urinorm;
    char const *  k_spatial = NULL;
    char const *  spatial = _uri->query( CBM_UriQSpatial, &k_spatial);
    if ( !spatial)
        { return  -1; }
    int  len_max = CBM_UriSize;
    int  len = cbm::snprintf( urinorm, len_max, "/%s", spatial);
    urinorm += len;
    len_max -= len;
    if ( len_max <= 0)
        { return  -1; }
    len = _uri->path( urinorm, len_max);
    urinorm += len;
    len_max -= len;
    if ( len_max <= 0)
        { return  -1; }

    char const *  k_unit = NULL;
    char const *  unit = _uri->query( CBM_UriQUnit, &k_unit);
    char const *  k_temporal = NULL;
    _uri->query( CBM_UriQTemporal, &k_temporal);

    /*TODO sort query keys..?! duplicates? */
    int  k = 0;
    char const *  q_k = _uri->subquery( k);
    while ( q_k)
    {
        if ( q_k==k_spatial || q_k==k_temporal || q_k==k_unit)
            { /*skip*/ }
        else
        {
            len = cbm::snprintf( urinorm, len_max, "/%s", q_k);
            urinorm += len;
            len_max -= len;
            if ( len_max <= 0)
                { return  -1; }
        }
        q_k = _uri->subquery( ++k);
    }

    if ( _flags & CBM_URI_NOUNIT)
        { /*no op*/ }
    else
    {
        if ( !unit || *unit=='\0')
            { unit = CBM_NoUnit; }
        len = cbm::snprintf( urinorm, len_max, "?%s=%s", CBM_UriQUnit, unit);
// sk:off        urinorm += len;
        len_max -= len;
        if ( len_max <= 0)
            { return  -1; }
    }

    return  0;
}

