
#ifndef  CBM_CPL_DATATYPE_H_
#define  CBM_CPL_DATATYPE_H_

#include  "cbm_cfg.h"

#if defined(__cplusplus)
extern "C" {
#endif

#define  CBM_TypesAreEqual( type1, type2) (type1.ID == type2.ID)

typedef struct
{
    int  ID;
    int  size;
} CBM_Datatype;

CBM_API extern CBM_Datatype const CBM_VOID;

typedef float CBM_Float;
CBM_API extern CBM_Datatype const CBM_FLOAT;
typedef double CBM_Double;
CBM_API extern CBM_Datatype const CBM_DOUBLE;

typedef char CBM_Char;
CBM_API extern CBM_Datatype const CBM_CHAR;
typedef unsigned char CBM_UChar;
CBM_API extern CBM_Datatype const CBM_UCHAR;

typedef short int CBM_Short;
CBM_API extern CBM_Datatype const CBM_SHORT;
typedef unsigned short int CBM_UShort;
CBM_API extern CBM_Datatype const CBM_USHORT;

typedef int CBM_Int;
CBM_API extern CBM_Datatype const CBM_INT;
typedef unsigned int CBM_UInt;
CBM_API extern CBM_Datatype const CBM_UINT;

typedef long int CBM_Long;
CBM_API extern CBM_Datatype const CBM_LONG;
typedef unsigned long int CBM_ULong;
CBM_API extern CBM_Datatype const CBM_ULONG;


#if defined(__cplusplus)
}
#endif

template < typename _T >
    struct cbm_type { };

template < > struct CBM_API cbm_type< CBM_Float >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_Double >
    { static CBM_Datatype const  type; };

template < > struct CBM_API cbm_type< CBM_Char >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_UChar >
    { static CBM_Datatype const  type; };

template < > struct CBM_API cbm_type< CBM_Short >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_UShort >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_Int >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_UInt >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_Long >
    { static CBM_Datatype const  type; };
template < > struct CBM_API cbm_type< CBM_ULong >
    { static CBM_Datatype const  type; };

#endif  /* !CBM_CPL_DATATYPE_H_ */
