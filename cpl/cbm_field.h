/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: jun 29, 2016)
 */

#ifndef  CBM_CPL_FIELD_H_
#define  CBM_CPL_FIELD_H_

#include  "cbm_cfg.h"
#include  "cbm_transform.h"
#include  "cbm_event.h"
#include  "cbm_type.h"
#include  "memory/cbm_mempool.h"
#include  "memory/cbm_slaba.h"

#if defined(__cplusplus)
extern "C" {
#endif

#define  CBM_FieldDynamicSize (-1) /*msg_size*/
#define  CBM_FieldAliasSize (-1) /*buf_size*/

#define  CBM_FieldIsCreated(field) ((field)!=NULL)

#define  CBM_FieldIsProducer(field) ((field)->head==(field))
#define  CBM_FieldIsConsumer(field) ((field)->head!=(field))
#define  CBM_FieldIsSkeleton(field) (CBM_TypesAreEqual((field)->type, CBM_VOID))
#define  CBM_FieldIsAlias(field) (CBM_FieldIsProducer(field) && (field)->buf_size == CBM_FieldAliasSize)

#define  CBM_FieldUseStack(field) ((field)->buf_size <= CBM_FieldMinAllocSize)
#define  CBM_FieldStorage(field) (void *)((!CBM_FieldUseStack(field)) ? (field)->buf.p : (field)->buf.d)

typedef struct CBM_Field
{
// sk:??    int seq_count; /*incremented on each buffer update [sse?]*/
    int  buf_size; /*size of data buffer [bytes]*/
    int  msg_size; /*size of last message sent [bytes]*/
    union {
        void *  p;
        CBM_Char  c[CBM_FieldMinAllocSize/sizeof(CBM_Char)];
        CBM_UChar  d[CBM_FieldMinAllocSize/sizeof(CBM_UChar)];
        CBM_Float  f[CBM_FieldMinAllocSize/sizeof(CBM_Float)];
        CBM_Double  lf[CBM_FieldMinAllocSize/sizeof(CBM_Double)];
        CBM_Int  i[CBM_FieldMinAllocSize/sizeof(CBM_Int)];
        CBM_UInt  u[CBM_FieldMinAllocSize/sizeof(CBM_UInt)];
        CBM_Long  li[CBM_FieldMinAllocSize/sizeof(CBM_Long)];
        CBM_ULong  lu[CBM_FieldMinAllocSize/sizeof(CBM_ULong)];
    } buf;
    CBM_Datatype  type;

    struct CBM_Field *  head; /*producer*/
    struct CBM_Field *  next; /*list of consumers/aliases*/
    /* transform consumer buffer during send() operation */
    CBM_Transform *  send_transform;
    /* transform consumer buffer during receive() operation */
    CBM_Transform *  recv_transform;
    /* callback function for incoming data */
    CBM_Callback  send_callback;

    /* count number of publishers/subscribers for field. This
     * only applies for producers, i.e., for consumers both
     * counters are 0 by definition */
    int  pub_count;
    int  sub_count;

    char  uri[CBM_UriSize];
// sk:??    CBM_Spatial  spatial;
// sk:??    CBM_Tag  space_tag;
// sk:??    CBM_Temporal  temporal;
// sk:??    CBM_Tag  time_tag;

    CBM_Unit  unit;


    void * (*alloc)( CBM_MemPool, size_t /*bytes*/);
    void  (*free)( void * /*data ptr*/);

// sk:??    int (*pack)( char const * /*name*/,
// sk:??        void const * /*data*/, size_t /*data size*/,
// sk:??        void ** /*packed data*/);
// sk:??    int (*unpack)( CBM_Hash * /*name magic*/,
// sk:??        void * /*data*/, size_t /*data size*/,
// sk:??        void const * /*packed data*/);

    CBM_FieldStats *  stats; /*collect statistics*/
} CBM_Field;

// sk:?? typedef struct CBM_FieldCollectionData
// sk:?? {
// sk:??     CBM_Field  field;
// sk:?? 
// sk:??     std::vector< CBM_Spatial >  partids;
// sk:??     std::vector< size_t >  partid_offsets;
// sk:?? 
// sk:?? } CBM_FieldCollectionData;

CBM_Field *  CBM_FieldCreate( CBM_MemCache,
        char const * /*uri*/, int * /*rc*/);
void  CBM_FieldDestroy( CBM_MemCache, CBM_Field *);

void  CBM_FieldSetUnitFromUri( CBM_Field *, char const * /*uri*/, int * /*rc*/);
void  CBM_FieldSetData( CBM_MemPool, CBM_Field *,
    void * /*data*/, size_t /*data size*/, CBM_Datatype, int * /*rc*/);
void  CBM_FieldUnsetData( CBM_Field *, int * /*rc*/);
void  CBM_FieldResetData( CBM_MemPool, CBM_Field *,
        size_t /*data size*/, int * /*rc*/);

char const *  CBM_FieldGetUri( CBM_Field *);
size_t  CBM_FieldGetSize( CBM_Field *, int * /*rc*/);

int  CBM_FieldsCompatible( CBM_Field *, CBM_Field *);

int  CBM_FieldPubCount( CBM_Field *);
int  CBM_FieldSubCount( CBM_Field *);

CBM_Hash  CBM_FieldHashify( CBM_Field *);

#if defined(__cplusplus)
}
#endif

#define  CBM_URI_NOUNIT  0x00000001 /*drop unit from uri*/

#include  "comm/cbm_comm.h"
int CBM_UriNormalize( char * /*normalized uri*/,
        cbm::uri_t const * /*uri*/, int /*flags*/);

#endif  /* !CBM_CPL_FIELD_H_ */

