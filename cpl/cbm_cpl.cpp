/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: oct 01, 2016)
 */

#include  <vector>
#include  "nosql/cbm_kvstore.h"

#include  "cbm_cpl.h"
#include  "cbm_field.h"

#include  "log/cbm_baselog.h"
#include  "string/cbm_string.h"
#include  "memory/cbm_mem.h"
#include  "memory/cbm_mempool.h"
#include  "memory/cbm_slaba.h"

#include  <assert.h>

#if defined(__cplusplus)
extern "C" {
#endif

CBM_Handle const CBM_None = { 0 };

CBM_Spatial const CBM_SpatialNone = { -1 };
CBM_Temporal const CBM_TemporalNone = { 0 };


CBM_FieldPingInfo const CBM_FieldPingInfoNone = { 0, CBM_VOID, CBM_TemporalNone };
int const CBM_DynamicSize = CBM_FieldDynamicSize;

#define  CBM_CouplerSize(coupler)  (static_cast< CBM_CouplerData * >( coupler.mem)->fields.nb_fields)
#define  CBM_CouplerFieldCache(coupler) (static_cast< CBM_CouplerData * >( coupler.mem)->fields.mc)
#define  CBM_CouplerFieldBufferPool(coupler) (static_cast< CBM_CouplerData * >( coupler.mem)->fields.mempool)

static
int CBM_AllowOutOfOrderSubscription()
{
#ifdef  CBM_OutOfOrderSubscription
    return  1;
#else
    return  0;
#endif
}

static CBM_CouplerEnvironment const  CBM_DefaultCouplerEnvironment =
{
    CBM_AllowOutOfOrderSubscription() /*allow_skeletons */,
    0 /*collect_stats*/
};
int CBM_CouplerEnvironmentInitialize( CBM_CouplerEnvironment *  _env)
{
    _env->allow_skeletons = CBM_AllowOutOfOrderSubscription();  /*TODO unused*/
    _env->collect_stats = 0;

    return 0;
}

// sk:?? typedef struct CBM_DistributionMap
// sk:?? {
// sk:??     int **  field_ids;
// sk:??     int * targets;
// sk:?? } CBM_DistributionMap;

typedef struct CBM_CouplerFields
{
    CBM_MemCache  mc;
    CBM_MemPool  mempool;
    unsigned int  nb_fields;
} CBM_CouplerFields;

static int  CBM_CouplerFieldsInitialize( CBM_CouplerFields *  _fields,
        char const *  _name, size_t  _slab_sz)
{
    CBM_Assert(_fields);

    _fields->mempool = CBM_MemPoolCreate(
        CBM_MemPoolRoot, CBM_CouplerInitialMemoryPoolBlockSize);
    if ( !CBM_IsCreated(_fields->mempool))
        { return  -1; }

    _fields->mc = CBM_MemCacheCreate(
        _name, _slab_sz, sizeof(CBM_Field), 0, NULL, NULL);
    if ( !CBM_IsCreated(_fields->mc))
        { return  -1; }
    _fields->nb_fields = 0;
    return  0;
}
static void  CBM_CouplerFieldsTerminate( CBM_CouplerFields *  _fields)
{
    CBM_LogDebug( "FieldMemoryPoolSize=", CBM_MemPoolSize( _fields->mempool));
    CBM_MemPoolDestroy( _fields->mempool);
    CBM_MemCacheDestroy( _fields->mc);

    _fields->nb_fields = 0;
}


typedef struct CBM_CouplerData
{
    /* coupler name */
    char const *  name;

    /* registered (i.e., published) fields */
    CBM_CouplerFields  fields;
    cbm::kvstore_t  pubcache;

    CBM_CouplerEnvironment  env;
    CBM_CouplerStats * stats;
} CBM_CouplerData;

static CBM_FieldStats *
CBM_CouplerStatsNewField( CBM_CouplerStats *  _cplstats, CBM_Field const *  _field)
{
    int existed = 1;
    if ( _cplstats->data.find( _field->uri) == _cplstats->data.end())
        { existed = 0; }
    CBM_FieldStats &  fstats = _cplstats->data[_field->uri];

    if ( !existed )
    {
        fstats.publish_count = 0;
        fstats.subscribe_count = 0;
        fstats.send_count = 0;
        fstats.receive_count = 0;

        fstats.unit = _field->unit;
    }

    return &fstats;
}

CBM_Coupler  CBM_CouplerCreate( char const *  _name,
            CBM_CouplerEnvironment const *  _env,
        CBM_CouplerStats *  _stats, int *  _rc)
{
    CBM_LogDebug( "sizeof(field)=",sizeof(CBM_Field));
    if ( _rc)
        { *_rc = CBM_RET_OK; }

    CBM_Coupler  cpl;
    cpl.mem = 0;

    CBM_CouplerData *  cpldata =
        CBM_Construct( CBM_CouplerData);
    if ( !cpldata)
    { 
        if ( _rc)
            { *_rc = CBM_ERR_NOMEM; }
        return  cpl;
    }

    if ( _env)
        { cpldata->env = *_env; }
    else
        { cpldata->env = CBM_DefaultCouplerEnvironment; }

    cpldata->stats = NULL;
    if ( _stats)
        { cpldata->stats = _stats; }

    cpldata->name = 0;
    if ( _name)
    {
        cpldata->name = cbm::strdup( _name);
        if ( !cpldata->name)
        {
            if ( _rc)
                { *_rc = CBM_ERR_NOMEM; }
            CBM_Destroy( cpldata);
            return  cpl;
        }
    }

    int  rc_fields = CBM_CouplerFieldsInitialize(
            &cpldata->fields, _name, 256*1024 /*TODO make setting*/);
    if ( rc_fields)
    {
        if ( cpldata->name)
            { cbm::strfree( cpldata->name); }
        CBM_Destroy( cpldata);
        return  cpl;
    }

    cpl.mem = cpldata;
    return  cpl;
}

void  CBM_CouplerDestroy( CBM_Coupler  _cpl)
{
    if ( _cpl.mem)
    {
        CBM_CouplerData *  cpldata =
            static_cast< CBM_CouplerData * >( _cpl.mem);

        if ( cpldata->name)
            { cbm::strfree( cpldata->name); }

        int const  fields_alive_sz = cpldata->fields.nb_fields;
        if ( fields_alive_sz > 0)
            { CBM_LogWarn( "Coupler: Published/Subscribed fields",
                " [", fields_alive_sz,"] have not been unpublished/unsubscribed."); }
        CBM_CouplerFieldsTerminate( &cpldata->fields);

        CBM_Destroy( cpldata);
    }
}

static CBM_Field *  CBM_CouplerGetField( CBM_Coupler /*_cpl*/,
                CBM_Handle  _handle, CBM_Field **  _master)
{
    CBM_Field *  field = NULL;

    if ( CBM_HandleOk(_handle))
    {
        field = (CBM_Field *)_handle.ID;
        if ( _master)
        {
            if ( CBM_FieldIsCreated(field) && CBM_FieldIsAlias(field))
            {
                *_master = static_cast< CBM_Field * >( field->buf.p);
            }
            else
                { *_master = field; }
        }
    }

    return  field;
}

static CBM_Handle  CBM_CouplerFindHandle(
            CBM_Coupler /*_cpl*/, CBM_Field *  _field)
{
    CBM_Handle  handle = CBM_None;
    if ( !_field)
        { return  handle; }
    handle.ID = (size_t)_field;

    return  handle;
}

static CBM_Handle  CBM_CouplerFindProducer( CBM_Coupler  _cpl,
        char const *  _uri, int *  _req_alias)
{
    CBM_Assert(CBM_IsCreated(_cpl));

    CBM_CouplerData *  cpldata =
        static_cast< CBM_CouplerData * >( _cpl.mem);
    CBM_Handle  handle = CBM_None;

    if ( _req_alias)
        { *_req_alias = 0; }

    /* attempt to find matching field */
    char  nuri[CBM_UriSize];
    cbm::uri_t const  uri( _uri);
    CBM_UriNormalize( nuri, &uri, 0);
    cpldata->pubcache.fetch_pod( &handle.ID, nuri);
    if ( !CBM_HandleOk(handle))
    {
/* FIXME  uri.path?!*/
        CBM_UriNormalize( nuri, &uri, CBM_URI_NOUNIT);
        cpldata->pubcache.fetch_pod( &handle.ID, nuri);
        if ( _req_alias && CBM_HandleOk(handle))
            { *_req_alias = 1; }
    }

    return  handle;
}

static int  CBM_CouplerInsertField( CBM_Coupler  _cpl,
        CBM_Field *  _field, CBM_Handle *  _handle, char  _kind)
{
    CBM_Assert(CBM_IsCreated(_cpl));
    CBM_Assert(CBM_FieldIsCreated(_field));

    CBM_CouplerData *  cpldata =
        static_cast< CBM_CouplerData * >( _cpl.mem);

    _handle->ID = (size_t)_field;

    if ( CBM_HandleOk(*_handle) && ( _kind=='P'))
    {
        if ( !CBM_FieldIsSkeleton(_field))
        {
            cpldata->pubcache.store_pod( CBM_FieldGetUri( _field), _handle->ID);
            CBM_LogDebug( "Inserted slot number [",CBM_HandleId(*_handle),"] for field \"",
                CBM_FieldGetUri(_field),"\" into publisher cache");
        }
        cbm::uri_t const  uri( CBM_FieldGetUri( _field));
        char  uri_path[CBM_UriSize];
        uri.path( uri_path, CBM_UriSize);
        cpldata->pubcache.store_pod( uri_path, _handle->ID);
        CBM_LogDebug( "Inserted slot number [",CBM_HandleId(*_handle),"] for field \"",
                uri_path,"\" into publisher cache");
    }
    CBM_LogDebug( "Inserted field[",CBM_HandleId(*_handle),"] \"", CBM_FieldGetUri(_field), "\"");
    cpldata->fields.nb_fields += 1;
// sk:dbg    CBM_LogDebug( "Insert: #fields=",cpldata->fields.nb_fields);

    return  CBM_RET_OK;
}

static int  CBM_CouplerRemoveField( CBM_Coupler  _cpl,
            CBM_Handle  _handle, char  _kind)
{
    CBM_Field *  field = CBM_CouplerGetField( _cpl, _handle, NULL);
    if ( !CBM_FieldIsCreated(field))
    {
        CBM_LogError( "handle no longer valid  [handle=",CBM_HandleId(_handle),"]");
        return  CBM_ERR_INVALID;
    }

    CBM_CouplerData *  cpldata =
        static_cast< CBM_CouplerData * >( _cpl.mem);
    CBM_Assert(cpldata->fields.nb_fields > 0);

    /* remove cache entries */
    if ( _kind=='P')
    {
        cpldata->pubcache.remove( CBM_FieldGetUri( field));
        CBM_LogDebug( "Removed slot number [",CBM_HandleId(_handle),"] for field \"",
                CBM_FieldGetUri(field),"\" from publisher cache");
        cbm::uri_t const  uri( CBM_FieldGetUri( field));
        char  uri_path[CBM_UriSize];
        uri.path( uri_path, CBM_UriSize);
        cpldata->pubcache.remove( uri_path);
        CBM_LogDebug( "Removed slot number [",CBM_HandleId(_handle),"] for field \"",
                uri_path,"\" from publisher cache");
    }

    CBM_LogDebug( "Removing field[",CBM_HandleId(_handle),"] \"",
                CBM_FieldGetUri(field),"\"",
                "  kind=", (CBM_FieldIsProducer(field)?"producer":"consumer"));
    CBM_FieldDestroy( CBM_CouplerFieldCache(_cpl), field);

    cpldata->fields.nb_fields -= 1;
// sk:dbg    CBM_LogDebug( "Remove: #fields=",cpldata->fields.nb_fields);

    return  CBM_RET_OK;
}

static int CBM_FieldsUnitsCompatible( char const * _uri1, char const * _uri2)
{
    CBM_Field  f1;
    int  rc_unit1 = CBM_RET_OK;
    CBM_FieldSetUnitFromUri( &f1, _uri1, &rc_unit1);
    if ( rc_unit1)
        { return 0; }
    CBM_Field  f2;
    int  rc_unit2 = CBM_RET_OK;
    CBM_FieldSetUnitFromUri( &f2, _uri2, &rc_unit2);
    if ( rc_unit2)
        { return 0; }
    if ( !CBM_UnitsCompatible( f1.unit, f2.unit))
        { return 0; }
    return 1;
}

CBM_Handle  CBM_CouplerPublishField( CBM_Coupler  _cpl,
        char const *  _uri, void *  _data, size_t  _size,
        CBM_Datatype  _type, int  _flags, int *  _rc)
{
    int  rc = CBM_RET_OK;

    CBM_LogDebug( "::CPL::Publishing field \"", _uri,"\"...");

    int const  tsize = static_cast< int >( _size);

    CBM_Field *  pub_field = NULL;
    int  req_alias = 0;
    CBM_Handle  pub_handle = CBM_CouplerFindProducer( _cpl, _uri, &req_alias);
    if ( CBM_HandleOk(pub_handle))
    {
        CBM_Assert(!(_flags & CBM_PUB_CREATESKELETON));

        CBM_Field *  master_field = NULL;
        pub_field = CBM_CouplerGetField( _cpl, pub_handle, &master_field);
        if ( req_alias == 0)
        {
            /*all good: use existing field*/
            CBM_LogDebug( "Field \"",_uri, "\" does exist, using it...");
        }
        else if ( req_alias == 1 && CBM_FieldIsSkeleton(master_field))
        {
            CBM_LogDebug( "Field \"",_uri, "\" does exist, but is skeleton, promoting...");
            pub_field = master_field;
            pub_handle = CBM_None; /*force insert*/
        }
        else if ( req_alias == 1)
        {
            CBM_LogDebug( "Field \"",_uri, "\" does exist but requires alias...");
            CBM_Field *  alias_field = CBM_FieldCreate(
                    CBM_CouplerFieldCache(_cpl), _uri, &rc);
            if ( CBM_FieldIsCreated(alias_field) && ( rc == CBM_RET_OK)
                    && ( /* FIXME CBM_FieldIsSkeleton(master_field) || */
         /*TODO grow buffer?*/ (( tsize*_type.size <= master_field->buf_size)
                        && CBM_TypesAreEqual( _type, master_field->type))))
            {
                alias_field->head = alias_field; /*tag as alias (producer)*/
                alias_field->buf_size = CBM_FieldAliasSize; /*tag as alias*/
                alias_field->buf.p = master_field; /*set master*/

                CBM_Assert(CBM_FieldIsAlias(alias_field));
            }
            else
            {
                CBM_LogError( "Field \"", CBM_FieldGetUri(master_field),"\"",
                       " and its alias are not compatible");
                if ( CBM_FieldIsCreated(alias_field))
                {
                    CBM_FieldDestroy( CBM_CouplerFieldCache(_cpl), alias_field);
                    alias_field = NULL;
                }
            }
            pub_field = alias_field;
            pub_handle = CBM_None; /*force insert*/
        }
        else /*failure: invalidate*/
        {
            CBM_LogError( "Failed to resolve alias settings",
                            " for field \"",_uri,"\" ");
            rc = CBM_ERR_INVALID;
            pub_field = NULL;
            pub_handle = CBM_None;
        }
    }
    else
    {
        CBM_LogDebug( "Field \"",_uri, "\" does not exist, creating it...");
        pub_field = CBM_FieldCreate( CBM_CouplerFieldCache(_cpl), _uri, &rc);
        if ( rc || !CBM_FieldIsCreated(pub_field))
            { pub_field = NULL; }
    }

    if ( CBM_FieldIsCreated(pub_field))
    {
        pub_field->head = pub_field; /*producer*/

        if ( _flags & CBM_PUB_CREATESKELETON )
        {
            CBM_LogDebug( "Field \"",_uri, "\" is a skeleton...");
            pub_field->pub_count = 0; /*make it explicit*/
        }
        else if ( CBM_FieldIsAlias(pub_field))
        {
            pub_field->type = _type;
            /* increase publication counter */
            pub_field->pub_count += 1;
            CBM_LogDebug( "Increase publication counter",
                " for field[",(void*)pub_field,"] \"",pub_field->uri,"\"",
                "  C_p=", pub_field->pub_count, " T_p=", pub_field->type.ID);

            static_cast< CBM_Field * >(
                pub_field->buf.p)->pub_count += 1;
            CBM_LogDebug( "Increase publication counter of alias reference"
                " for field[",(void*)pub_field,"] \"",pub_field->uri,"\"",
                "  C_p=", static_cast< CBM_Field * >(pub_field->buf.p)->pub_count);
        }
        else if ( CBM_FieldIsSkeleton(pub_field))
        {
            if ( CBM_FieldsUnitsCompatible( pub_field->uri, _uri))
            {
                int  rc_unit = CBM_RET_OK;
                CBM_FieldSetUnitFromUri( pub_field, _uri, &rc_unit);
                int  rc_setdata = CBM_RET_OK;
                CBM_FieldSetData( CBM_CouplerFieldBufferPool(_cpl),
                            pub_field, _data, tsize, _type, &rc_setdata);
                if ( rc_unit != CBM_RET_OK || rc_setdata != CBM_RET_OK)
                {
                    pub_field = NULL;
                    pub_handle = CBM_None;
                }
                else
                {
                    /* set all consumers */
                    CBM_Field *  p = pub_field;
                    while ( p->next)
                    {
                        CBM_Field *  sub_field = p->next;
                        CBM_FieldSetData( CBM_CouplerFieldBufferPool(_cpl),
                                    sub_field, _data, tsize, _type, &rc);
                        p = p->next;
                    }

                    /* set all aliases */
                        /* -- nothing to do! */

                    /* increase publication counter */
                    pub_field->pub_count += 1;
                    CBM_LogDebug( "Increase publication counter",
                            " for field[",(void*)pub_field,"] \"",pub_field->uri,"\"",
                            "  C_p=", pub_field->pub_count);
                    CBM_Assert(!CBM_FieldIsSkeleton(pub_field));
                }
            }
            else
            {
                CBM_LogError( "Illegal attempt to assign incompatible unit to field",
                        "[uri=",_uri,"]");
                pub_field = NULL;
                pub_handle = CBM_None;
            }
        }
        else
        {
            /* increase publication counter */
            pub_field->pub_count += 1;
            CBM_LogDebug( "Increase publication counter",
                    " for field[",(void*)pub_field,"] \"",pub_field->uri,"\"",
                    "  C_p=", pub_field->pub_count);
        }

        /* collect statistics */
        CBM_CouplerData *  cpldata =
            static_cast< CBM_CouplerData * >( _cpl.mem);
        if ( pub_field && cpldata->env.collect_stats && cpldata->stats)
        {
            if ( !pub_field->stats)
                { pub_field->stats = CBM_CouplerStatsNewField( cpldata->stats, pub_field); }
            pub_field->stats->publish_count += 1;
        }
    }

    if ( CBM_FieldIsCreated(pub_field)
            && !CBM_HandleOk(pub_handle)) /*needs insert*/
    {
        rc = CBM_CouplerInsertField( _cpl, pub_field, &pub_handle, 'P');
        if ( rc || ( !CBM_HandleOk(pub_handle)))
        {
            CBM_LogError( "Failed to insert field",
                    " \"", CBM_FieldGetUri(pub_field),"\"");
            CBM_FieldDestroy( CBM_CouplerFieldCache(_cpl), pub_field);
            pub_field = NULL;
            pub_handle = CBM_None;
        }
    }
    else
        { /*ok*/ }


    if ( _rc)
        { *_rc = rc; }
    return  pub_handle;
}

static CBM_Handle  CBM_CouplerPublishSkeletonField( CBM_Coupler  _cpl,
        char const *  _uri, int  _flags, int *  _rc)
{
    CBM_LogDebug( "Publishing skeleton \"", _uri, "\"");
    return  CBM_CouplerPublishField( _cpl, _uri,
        NULL, 0, CBM_VOID, _flags|CBM_PUB_CREATESKELETON, _rc);
}


void  CBM_CouplerUnpublishField( CBM_Coupler _cpl,
                CBM_Handle  _handle, int *  _rc)
{
    if ( !CBM_HandleOk(_handle))
    {
        if ( _rc)
            { *_rc = CBM_ERR_INVALID; }
        return;
    }
    if ( _rc)
        { *_rc = CBM_RET_OK; }

    CBM_Field *  field = CBM_CouplerGetField( _cpl, _handle, NULL);
    CBM_LogDebug( "::CPL::Unpublishing field \"", CBM_FieldGetUri(field),"\"...");
    if ( !CBM_FieldIsCreated(field) || !CBM_FieldIsProducer(field))
    {
        CBM_LogDebug( "Errors unpublishing field[",CBM_HandleId(_handle),"]");
        if ( _rc)
            { *_rc = CBM_ERR_INVALID; }
        return;
    }

    if ( !CBM_FieldIsSkeleton(field))
        { field->pub_count -= 1; }
    CBM_Assert(field->pub_count>=0);
    CBM_LogDebug( "Field[",CBM_HandleId(_handle),"] \"", CBM_FieldGetUri(field),"\"",
           " C_p=",field->pub_count, " C_s=",field->sub_count);

    if ( CBM_FieldIsAlias(field))
    {
        CBM_Field *  pub_field =
            static_cast< CBM_Field * >( field->buf.p);
        pub_field->pub_count -= 1;
        CBM_Assert((pub_field->pub_count>0)
            || (pub_field->pub_count==0 && CBM_FieldIsSkeleton(pub_field)));
    }

    if ( field->pub_count == 0)
    {
        if ( field->sub_count > 0)
        {
            CBM_LogDebug( "Making field \"", field->uri,"\" a skeleton,"
                    " there are subscribers left  [#subscribers=",field->sub_count,"]");
            CBM_Assert( !CBM_FieldIsAlias(field));
            CBM_FieldUnsetData( field, _rc);
            /* FIXME  unset consumer too?
             *      no: allow "final" receive after unpublish.. (.. but we disallow receive from skeleton..)
             */
        }
        else
            { CBM_CouplerRemoveField( _cpl, _handle, 'P'); }
    }
}


CBM_Handle  CBM_CouplerSubscribeField( CBM_Coupler  _cpl,
        char const *  _uri, CBM_Transform *  _send_transform,
        CBM_Callback  _send_callback,
        int  _flags, int *  _rc)
{
    if ( _rc)
        { *_rc = CBM_RET_OK; }
    CBM_LogDebug( "::CPL::Subscribing field \"", _uri,"\"...",
        " [transform=",(_send_transform?_send_transform->name:"-"),"]");

    CBM_Handle  pub_handle = CBM_CouplerFindProducer( _cpl, _uri, NULL);
    CBM_Field *  pub_field = NULL;
    if ( CBM_HandleOk(pub_handle))
        { CBM_CouplerGetField( _cpl, pub_handle, &pub_field); }
    if ( !CBM_FieldIsCreated(pub_field))
    {
        if ( CBM_AllowOutOfOrderSubscription())
        {
            int  rc_pub = CBM_RET_OK;
            pub_handle = CBM_CouplerPublishSkeletonField(
                    _cpl, _uri, _flags, &rc_pub);
            if ( rc_pub != CBM_RET_OK)
            {
                if ( _rc)
                    { *_rc = rc_pub; }
                return  CBM_None;
            }
            CBM_Assert(CBM_HandleOk(pub_handle)
                && CBM_FieldIsSkeleton(CBM_CouplerGetField(_cpl,pub_handle,NULL)));
            pub_field = CBM_CouplerGetField( _cpl, pub_handle, NULL);
        }
        else
        {
            CBM_LogError( "No such field  [field=",_uri,"]");
            if ( _rc)
                { *_rc = CBM_ERR_NOTFOUND; }
            return  CBM_None;
        }
    }
    CBM_Assert(!CBM_FieldIsAlias(pub_field));
    CBM_LogDebug( "Found published field[",CBM_HandleId(pub_handle),"]",
            " (",_uri,") with",
            " pub-count=", CBM_FieldPubCount( pub_field),
            " sub-count=", CBM_FieldSubCount( pub_field));

    /* add new field with own buffer */
    int  rc_field = CBM_RET_OK;
    CBM_Field *  sub_field = CBM_FieldCreate(
            CBM_CouplerFieldCache(_cpl), _uri, &rc_field);
    if ( rc_field != CBM_RET_OK)
    {
        if ( CBM_FieldIsCreated(sub_field))
            { CBM_FieldDestroy( CBM_CouplerFieldCache(_cpl), sub_field); }
        CBM_CouplerUnpublishField( _cpl, pub_handle, NULL);
        if ( _rc)
            { *_rc = rc_field; }
        return  CBM_None;
    }
    CBM_Assert(CBM_FieldIsCreated(sub_field));

    if ( !CBM_FieldIsSkeleton(pub_field))
    {
        void *  ini_data = NULL;
        if ( pub_field->msg_size > 0)
            { ini_data = CBM_FieldStorage( pub_field); }
        CBM_FieldSetData( CBM_CouplerFieldBufferPool(_cpl),
                sub_field, ini_data,
                pub_field->buf_size/pub_field->type.size,
                pub_field->type, &rc_field);
        if (( rc_field != CBM_RET_OK)
                || !CBM_FieldsCompatible( pub_field, sub_field))
        {
            CBM_LogError( "Failed to set consumer field",
                   " \"", sub_field->uri,"\"");
            if ( !CBM_FieldsCompatible( pub_field, sub_field))
            {
                CBM_LogError( "Producer/consumer fields \"",
                        sub_field->uri, "\" are not compatible",
                        "  [unit(P)=",CBM_UnitName(pub_field->unit),
                        ",unit(C)=", CBM_UnitName(sub_field->unit), "]");
            }

            CBM_FieldDestroy( CBM_CouplerFieldCache(_cpl), sub_field);
            return  CBM_None;
        }
    }
    else
        { CBM_Assert(CBM_AllowOutOfOrderSubscription()); }

    CBM_Handle  sub_handle = CBM_None;
    rc_field = CBM_CouplerInsertField(_cpl, sub_field, &sub_handle, 'C');
    if (( rc_field != CBM_RET_OK)
        || ( !CBM_HandleOk(sub_handle)))
    {
        sub_handle = CBM_None;
        CBM_FieldDestroy( CBM_CouplerFieldCache(_cpl), sub_field);
    }
    else
    {
        sub_field->send_transform = _send_transform;
        sub_field->send_callback = _send_callback;
        sub_field->pub_count = 0; /*it's a consumer!*/
        sub_field->sub_count = 0; /*it's a consumer!*/
        sub_field->head = pub_field;
        CBM_Field *  p = pub_field;
        while ( p->next)
            { p = p->next; }
        p->next = sub_field;
        CBM_LogDebug( "Enqueue field \"",_uri,"\" into",
            " consumer list of field \"",sub_field->head->uri,"\"");

        pub_field->sub_count += 1;
        CBM_LogDebug( "Field[",CBM_HandleId(sub_handle),"]: \"",_uri, "\" subscribed to ",
                pub_field->sub_count, " times");
    }

    /* collect statistics */
    CBM_CouplerData *  cpldata =
        static_cast< CBM_CouplerData * >( _cpl.mem);
    if ( sub_field && cpldata->env.collect_stats && cpldata->stats)
    {
        if ( !sub_field->stats)
            { sub_field->stats = CBM_CouplerStatsNewField( cpldata->stats, sub_field); }
        sub_field->stats->subscribe_count += 1;
    }

    return  sub_handle;
}

void  CBM_CouplerUnsubscribeField( CBM_Coupler  _cpl,
        CBM_Handle  _handle, int *  _rc)
{
    if ( !CBM_HandleOk(_handle))
    {
        if ( _rc)
            { *_rc = CBM_ERR_INVALID; }
        return;
    }
    if ( _rc)
        { *_rc = CBM_RET_OK; }

    CBM_Field *  field = CBM_CouplerGetField( _cpl, _handle, NULL);
    CBM_LogDebug( "::CPL::Unsubscribing field \"", CBM_FieldGetUri(field), "\"...");
    if ( !CBM_FieldIsCreated(field) || !CBM_FieldIsConsumer( field))
    {
        CBM_LogDebug( "Errors unsubscribing field[",CBM_HandleId(_handle),"]");
        if ( _rc)
            { *_rc = CBM_ERR_INVALID; }
        return;
    }

    CBM_Assert(field->sub_count == 0);
    CBM_Assert(field->pub_count == 0);
    CBM_Assert(field->head->sub_count > 0);

    field->head->sub_count -= 1;
    if ( CBM_FieldIsSkeleton( field->head))
    {
        CBM_Handle  pub_handle =
            CBM_CouplerFindHandle( _cpl, field->head);
        CBM_CouplerUnpublishField( _cpl, pub_handle, _rc);
    }
    CBM_CouplerRemoveField( _cpl, _handle, 'C');
}

int  CBM_CouplerPingField( CBM_Coupler  _cpl,
        CBM_Handle  _handle, CBM_FieldPingInfo *  _finfo, int * /*rc*/)
{
    int  size = -1;
    if ( _finfo)
        { *_finfo = CBM_FieldPingInfoNone; }
    if ( CBM_HandleOk(_handle))
    {
        CBM_Field *  field = NULL;
        CBM_CouplerGetField( _cpl, _handle, &field);
        if ( CBM_FieldIsCreated(field))
        {
            if ( !CBM_FieldIsSkeleton(field))
            {
                size = field->msg_size/field->type.size;
                if ( _finfo)
                {
                    _finfo->size = size;
                    _finfo->type = field->type;
// sk:TODO          _finfo->temporal = field->temporal;
                }
            }
            else
                { size = 0; }
        }
    }

    return  size;
}

size_t  CBM_CouplerSendField( CBM_Coupler  _cpl, CBM_Handle  _handle,
        void const *  _data, size_t  _size,
            CBM_Datatype  _type, int /*flags*/, int *  _rc)
{
    CBM_Assert(CBM_IsCreated(_cpl));
    CBM_Assert(CBM_HandleOk(_handle));
    CBM_Assert(_data);
    CBM_Assert(_size>0);

    CBM_Field *  field = CBM_CouplerGetField( _cpl, _handle, NULL);
    CBM_Assert(CBM_FieldIsCreated(field));
    CBM_Assert(CBM_FieldIsProducer(field));
    CBM_Unit *  unit = NULL;
    if ( CBM_FieldIsAlias(field))
    {
        unit = &field->unit;
        field = static_cast< CBM_Field * >( field->buf.p);
    }
//    CBM_LogDebug( "Sending field \"",field->uri,"\"",
//          "  [size=",_size*field->type.size,",buf_size=",field->buf_size,"]");

    int const  is_dynamic_size = ( field->msg_size == CBM_DynamicSize) ? 1 :0;
    if ( is_dynamic_size)
    {
        CBM_LogDebug( "Field \"", field->uri,"\" has dynamic size: ", _size);
        int  rc_set = CBM_RET_OK;
        CBM_FieldResetData( CBM_CouplerFieldBufferPool(_cpl), field, _size, &rc_set);
        if ( rc_set == CBM_RET_OK)
        {
            field->msg_size = CBM_DynamicSize; /*reset to dynamic size*/
            int  rc_cset = CBM_RET_OK;
            /* set all consumers */
            CBM_Field *  consumer_field = field->next;
            while ( consumer_field)
            {
                CBM_FieldResetData( CBM_CouplerFieldBufferPool(_cpl),
                        consumer_field, _size, &rc_cset);
                if ( rc_cset)
                    { rc_set = rc_cset; }
                consumer_field = consumer_field->next;
            }
        }
        if ( rc_set)
        {
            if ( _rc)
                { *_rc = rc_set; }
            return  0;
        }
    }
    else
        { field->msg_size = _size*field->type.size; }

    int  rc = CBM_RET_OK;
    size_t  copy_sz = 0;
    /*copy to field buffer*/
    if ( field->buf_size < (((int)_size)*field->type.size))
    {
        CBM_LogError( "Send: message buffer size too small",
            "  [size=",_size*field->type.size, ",max=",field->buf_size,"]");
        rc = CBM_ERR_INVALID;
    }
    else if ( unit) /*convert to master unit*/
    {
        if ( !CBM_TypesAreEqual( field->type, _type))
        {
            CBM_LogFatal( "Datatypes of producer and sender buffer do not match;",
                " currently this is not handled.", " Sorry :-(",
                " [type=",field->type.ID, ":",_type.ID, ",uri=",field->uri);
            if ( _rc)
                { *_rc = CBM_ERR_NOTIMPLEMENTED; }
            return  0;
        }
        CBM_LogDebug( "Copying/Converting to master send-buffer \"",field->uri,"\"");
        CBM_TransformConvertUnitData  units;
        /*no check for unit compatibility, must be done at publish/subscribe*/
        units.from = *unit;
        units.to = field->unit;
        /*copy during unit conversion*/
        int  rc_unitconvert =
            CBM_TransformConvertUnit->execute(
                CBM_FieldStorage(field), _size,
                _data, _size, field->type, &units);
        if ( rc_unitconvert == 0)
            { copy_sz = _size; }
        else
            { rc = CBM_ERR_TRANSFORM; }
    }
    else
    {
        memcpy( CBM_FieldStorage(field), _data, _size*field->type.size);
        copy_sz = _size;
    }

    /*update consumer fields*/
    CBM_Field *  consumer_field = field->next;
    if (( rc == CBM_RET_OK) && ( copy_sz > 0))
    {
        while ( consumer_field)
        {
            CBM_Transform *  transform = ( consumer_field->send_transform)
                    ? consumer_field->send_transform : CBM_TransformNoop;

// sk:dbg            CBM_LogDebug( "Update consumer buffer \"",consumer_field->uri,"\"",
// sk:dbg                " on sender-side using transform \"",transform->uri,"\"");

            size_t  msg_tsize = consumer_field->buf_size/consumer_field->type.size;
            if ( msg_tsize < copy_sz)
            {
                CBM_LogWarn( "Send: message truncated for",
                        " field \"",consumer_field->uri,"\"");
            }
            else
                { msg_tsize = copy_sz; }

            CBM_Assert( transform->execute);
            int  rc_transform = transform->execute(
                CBM_FieldStorage(consumer_field), msg_tsize,
                    CBM_FieldStorage(field), msg_tsize,
                        consumer_field->type, transform->state);
            if ( rc_transform == CBM_RET_OK)
                { consumer_field->msg_size = msg_tsize*consumer_field->type.size; }
            else
            {
                consumer_field->msg_size = 0;
                rc = CBM_ERR_TRANSFORM;
            }

            /* call event handler */
            if ( consumer_field->send_callback.fn)
            {
                int  rc_handler = consumer_field->send_callback.fn(
                    CBM_FieldStorage(consumer_field), msg_tsize,
                        consumer_field->send_callback.data);
                if ( rc_handler)
                    { rc = CBM_ERR_CALLBACK; }
                else
                {
                    if ( consumer_field->stats)
                    {
                        consumer_field->stats->receive_count += 1;
                        consumer_field->stats->bytes_received += (unsigned long int)msg_tsize*consumer_field->type.size;
                    }
                }
            }

            consumer_field = consumer_field->next;
        }
    }
    else
    {
        while ( consumer_field)
        {
            consumer_field->msg_size = 0;
            consumer_field = consumer_field->next;
        }
    }

    if ( field && field->stats)
    {
        field->stats->send_count += 1;
        field->stats->bytes_sent += ((unsigned long int)_size)*field->type.size;
    }

    if ( _rc)
        { *_rc = rc; }

    return  copy_sz;
}

size_t  CBM_CouplerReceiveField( CBM_Coupler  _cpl, CBM_Handle  _handle,
    void *  _data, size_t  _size, CBM_Datatype  _type, int  _flags, int *  _rc)
{
    CBM_Assert(CBM_IsCreated(_cpl));
    CBM_Assert(CBM_HandleOk(_handle));
    CBM_Assert(_data);
    CBM_Assert(_size>0);

    CBM_Field *  field = CBM_CouplerGetField( _cpl, _handle, NULL);
    CBM_Assert(CBM_FieldIsCreated(field));
    CBM_Assert(CBM_FieldIsConsumer(field));
    CBM_LogDebug( "Receiving field \"",field->uri,"\"",
          "  [size=",_size*field->type.size,",msg_size=",field->msg_size,"]");

    int  rc = CBM_RET_OK;
    size_t  copy_sz = 0;
    /*copy from field buffer*/
    if ( field->head == 0)
    {
        CBM_LogError( "Receiving data from consumer field",
            " without producer  [field=\"",field->uri,"\"]");
        rc = CBM_ERR_EOF;
    }
    else if ( CBM_FieldIsSkeleton( field->head))
    {
        /* FIXME  this test might consider data availability (seq_count?) */
        CBM_LogDebug( "Consumer without producer  [field=\"",field->uri,"\"]");
        rc = CBM_ERR_INVALID;
    }
    else if ( field->msg_size != (((int)_size)*field->type.size))
    {
        CBM_LogError( "Receive: message buffer sizes disagree for \"",field->uri,"\"",
            "  [size=",_size,",expected=",field->msg_size/field->type.size,"]");
        rc = CBM_ERR_INVALID;
    }
    else
    {
        if ( !CBM_TypesAreEqual( field->type, _type))
        {
            CBM_LogFatal( "Datatypes of consumer and receiver buffer do not match;",
                " currently this is not handled.", " Sorry :-(");
            if ( _rc)
                { *_rc = CBM_ERR_NOTIMPLEMENTED; }
            return  0;
        }
        CBM_LogDebug( "Copying/Converting to receive-buffer \"",field->uri,"\"");
        CBM_TransformConvertUnitData  units;
        units.from = field->head->unit;
        units.to = field->unit;
        /*copy during unit conversion*/
        int  rc_unitconvert =
            CBM_TransformConvertUnit->execute( _data, _size,
                CBM_FieldStorage(field), _size,
                field->type, &units);
        if ( rc_unitconvert == 0)
            { copy_sz = _size; }
        else
            { rc = CBM_ERR_TRANSFORM; }
    }

    if ( _flags & CBM_RECV_CLEAR)
    {
        CBM_LogDebug( "Clearing field[",CBM_HandleId(_handle),"] \"",field->uri,"\"...");
        memset( CBM_FieldStorage(field), 0, field->buf_size);
        field->msg_size = 0;
    }

    if ( field && field->stats)
    {
        field->stats->receive_count += 1;
        field->stats->bytes_received += ((unsigned long int)_size)*field->type.size;
    }

    if ( _rc)
        { *_rc = rc; }

    return  copy_sz;
}

int  CBM_CouplerNumberOfRegisteredFields( CBM_Coupler  _cpl)
{
    if ( !CBM_IsCreated(_cpl))
        { return -1; }
    return  CBM_CouplerSize(_cpl);
}

char const *  CBM_CouplerGetFieldUri(
        CBM_Coupler  _cpl, CBM_Handle  _handle)
{
    CBM_Field *  field =
        CBM_CouplerGetField( _cpl, _handle, NULL);
    if ( field)
        { return  field->uri; }
    return  NULL;
}

char const *  CBM_CouplerGetFieldUnit(
            CBM_Coupler  _cpl, CBM_Handle  _handle,
        char *  _symbols, size_t _symbols_sz)
{
    CBM_Field *  field =
        CBM_CouplerGetField( _cpl, _handle, NULL);
    if ( field)
    {
        cbm::string_t usym = CBM_UnitName( field->unit);
        if ( usym == "ERROR" || usym.length()==0 )
            { /*error*/ }
        else
        {
            size_t L = std::min( usym.length(), _symbols_sz-1);
            for ( size_t i = 0; i < L; ++i)
                { _symbols[i] = usym[i]; }
            _symbols[L] = '\0';
            return  _symbols;
        }
    }
    return  NULL;
}


int  CBM_CouplerWriteSummaryTable( CBM_CouplerStats *  _stats, int /*_options*/)
{
    fprintf( stdout, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
                    "URI", "unit",
                    "publish_count", "subscribe_count",
                    "send_count", "receive_count",
                    "bytes_sent", "bytes_received");
    std::map< std::string, CBM_FieldStats >::iterator  field = _stats->data.begin();
    for ( ; field != _stats->data.end(); ++field)
    {
        CBM_FieldStats const &  s = field->second;
        fprintf( stdout, "%s\t%s\t%d\t%d\t%d\t%d\t%lu\t%lu\n",
                    field->first.c_str(), CBM_UnitName(s.unit).c_str(),
                    s.publish_count, s.subscribe_count,
                    s.send_count, s.receive_count,
                    s.bytes_sent, s.bytes_received);
    }

    return CBM_RET_OK;
}


#if defined(__cplusplus)
} /* extern C */
#endif

