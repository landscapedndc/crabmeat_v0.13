/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: nov 05, 2016)
 */

#include  "cbm_transform.h"
#include  "log/cbm_baselog.h"

#include  <cstring>

#if defined(__cplusplus)
extern "C" {
#endif

/* NoOp "none" */
static int _cbm_transform_noop(
        void *  _recv_data, size_t  _recv_sz,
        void const *  _send_data, size_t  _send_sz,
        CBM_Datatype  _type, void *)
{
    if ( _recv_sz != _send_sz)
        { return  CBM_ERR_INVALID; }

    memcpy( _recv_data, _send_data, _recv_sz*_type.size);
    return  CBM_RET_OK;
}

static CBM_Transform  s_CBM_TransformNoop = { "noop", &_cbm_transform_noop, NULL };
CBM_Transform *  CBM_TransformNoop = &s_CBM_TransformNoop;


/* Dot Sum ".+" */
static int _cbm_transform_dotsum(
        void *  _recv_data, size_t _recv_sz,
        void const *  _send_data, size_t  _send_sz,
        CBM_Datatype  _type, void * /*state*/)
{
    if ( _recv_sz != _send_sz)
        { return  CBM_ERR_INVALID; }

    if ( CBM_TypesAreEqual( _type, CBM_DOUBLE))
    {
        CBM_Double *  r = static_cast< CBM_Double * >( _recv_data);
        CBM_Double const *  s = static_cast< CBM_Double const * >( _send_data);
        CBM_Double const *  S = s+_recv_sz;
        for ( ;  s != S; )
            { *r++ += *s++; }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_FLOAT))
    {
        CBM_Float *  r = static_cast< CBM_Float * >( _recv_data);
        CBM_Float const *  s = static_cast< CBM_Float const * >( _send_data);
        CBM_Float const *  S = s+_recv_sz;
        for ( ;  s != S; )
            { *r++ += *s++; }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_INT))
    {
        CBM_Int *  r = static_cast< CBM_Int * >( _recv_data);
        CBM_Int const *  s = static_cast< CBM_Int const * >( _send_data);
        CBM_Int const *  S = s+_recv_sz;
        for ( ;  s != S; )
            { *r++ += *s++; }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_UINT))
    {
        CBM_UInt *  r = static_cast< CBM_UInt * >( _recv_data);
        CBM_UInt const *  s = static_cast< CBM_UInt const * >( _send_data);
        CBM_UInt const *  S = s+_recv_sz;
        for ( ;  s != S; )
            { *r++ += *s++; }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_LONG))
    {
        CBM_Long *  r = static_cast< CBM_Long * >( _recv_data);
        CBM_Long const *  s = static_cast< CBM_Long const * >( _send_data);
        CBM_Long const *  S = s+_recv_sz;
        for ( ;  s != S; )
            { *r++ += *s++; }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_ULONG))
    {
        CBM_ULong *  r = static_cast< CBM_ULong * >( _recv_data);
        CBM_ULong const *  s = static_cast< CBM_ULong const * >( _send_data);
        CBM_ULong const *  S = s+_recv_sz;
        for ( ;  s != S; )
            { *r++ += *s++; }
    }
    else
    {
        CBM_LogError( "Transform \".+\" not defined for type ",_type.ID);
        return  CBM_ERR_UNKNOWN;
    }
    return  CBM_RET_OK;
}

static CBM_Transform  s_CBM_TransformDotSum = { ".+", &_cbm_transform_dotsum, NULL };
CBM_Transform *  CBM_TransformDotSum = &s_CBM_TransformDotSum;


/* Sum "+" */
static int _cbm_transform_sum(
        void *  _recv_data, size_t _recv_sz,
        void const *  _send_data, size_t  _send_sz,
        CBM_Datatype  _type, void * /*state*/)
{
    if ( _recv_sz > _send_sz)
        { return  CBM_ERR_INVALID; }

    size_t  blk_sz = _send_sz / _recv_sz;

    if ( CBM_TypesAreEqual( _type, CBM_DOUBLE))
    {
        CBM_Double *  r = static_cast< CBM_Double * >( _recv_data);
        CBM_Double const *  s = static_cast< CBM_Double const * >( _send_data);
        CBM_Double const *  R = r+_recv_sz;
        CBM_Double const *  S = s;
        for ( ;  r != R; ++r)
        {
            S += blk_sz;
            for ( ;  s != S; )
                { *r += *s++; }
        }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_FLOAT))
    {
        CBM_Float *  r = static_cast< CBM_Float * >( _recv_data);
        CBM_Float const *  s = static_cast< CBM_Float const * >( _send_data);
        CBM_Float const *  R = r+_recv_sz;
        CBM_Float const *  S = s;
        for ( ;  r != R; ++r)
        {
            S += blk_sz;
            for ( ;  s != S; )
                { *r += *s++; }
        }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_INT))
    {
        CBM_Int *  r = static_cast< CBM_Int * >( _recv_data);
        CBM_Int const *  s = static_cast< CBM_Int const * >( _send_data);
        CBM_Int const *  R = r+_recv_sz;
        CBM_Int const *  S = s;
        for ( ;  r != R; ++r)
        {
            S += blk_sz;
            for ( ;  s != S; )
                { *r += *s++; }
        }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_UINT))
    {
        CBM_UInt *  r = static_cast< CBM_UInt * >( _recv_data);
        CBM_UInt const *  s = static_cast< CBM_UInt const * >( _send_data);
        CBM_UInt const *  R = r+_recv_sz;
        CBM_UInt const *  S = s;
        for ( ;  r != R; ++r)
        {
            S += blk_sz;
            for ( ;  s != S; )
                { *r += *s++; }
        }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_LONG))
    {
        CBM_Long *  r = static_cast< CBM_Long * >( _recv_data);
        CBM_Long const *  s = static_cast< CBM_Long const * >( _send_data);
        CBM_Long const *  R = r+_recv_sz;
        CBM_Long const *  S = s;
        for ( ;  r != R; ++r)
        {
            S += blk_sz;
            for ( ;  s != S; )
                { *r += *s++; }
        }
    }
    else if ( CBM_TypesAreEqual( _type, CBM_ULONG))
    {
        CBM_ULong *  r = static_cast< CBM_ULong * >( _recv_data);
        CBM_ULong const *  s = static_cast< CBM_ULong const * >( _send_data);
        CBM_ULong const *  R = r+_recv_sz;
        CBM_ULong const *  S = s;
        for ( ;  r != R; ++r)
        {
            S += blk_sz;
            for ( ;  s != S; )
                { *r += *s++; }
        }
    }
    else
    {
        CBM_LogError( "Transform \"+\" not defined for type ",_type.ID);
        return  CBM_ERR_UNKNOWN;
    }
    return  CBM_RET_OK;
}

static CBM_Transform  s_CBM_TransformSum = { "+", &_cbm_transform_sum, NULL };
CBM_Transform *  CBM_TransformSum = &s_CBM_TransformSum;

#if defined(__cplusplus)
} /* extern C */
#endif


cbm_units_t *  __cbmunits()
{
    static cbm_units_t  cbmunits;
    return  &cbmunits;
}

template < typename _Data >
struct _CBM_ConvertInt
{
#define STATIC_BUFFER_SIZE 128
    static double double_buffer[STATIC_BUFFER_SIZE];

    static int convert( void * _recv_data, void const * _send_data,
                size_t _sz, CBM_TransformConvertUnitData const * _units)
    {
        _Data const *  sbuf = (_Data*)_send_data;
        _Data *  dbuf = (_Data*)_recv_data;
        CBM_Double *  tbuf = double_buffer;
        if ( _sz > STATIC_BUFFER_SIZE )
            { tbuf = new CBM_Double[_sz]; }

        for ( size_t j = 0; j < _sz; ++j)
            { tbuf[j] = sbuf[j]; }
        __cbmunits()->convert_double( tbuf, _units->to, tbuf, _units->from, _sz);
        for ( size_t j = 0; j < _sz; ++j)
            { dbuf[j] = tbuf[j]; }

        if ( _sz > STATIC_BUFFER_SIZE )
            { delete[] tbuf; }

        return 0;
    }
};
template < typename _Data >
    double _CBM_ConvertInt<_Data>::double_buffer[STATIC_BUFFER_SIZE];


#if defined(__cplusplus)
extern "C" {
#endif

static int _cbm_transform_convertunit(
        void *  _recv_data, size_t _recv_sz,
        void const *  _send_data, size_t  _send_sz,
        CBM_Datatype  _type, void *  _units)
{
    if ( _recv_sz != _send_sz)
        { return  CBM_ERR_INVALID; }

    if ( !_units)
        { return  -1; }

    CBM_TransformConvertUnitData  units =
        *static_cast< CBM_TransformConvertUnitData * >( _units);
    int const  sz = _recv_sz;

    if ( CBM_TypesAreEqual( _type, CBM_DOUBLE))
    {
        __cbmunits()->convert_double( (double *)_recv_data, units.to,
            (double const *)_send_data, units.from, sz);
    }
    else if ( CBM_TypesAreEqual( _type, CBM_FLOAT))
    {
        __cbmunits()->convert_float( (float *)_recv_data, units.to,
            (float const *)_send_data, units.from, sz);
    }
    else if ( CBM_TypesAreEqual( _type, CBM_CHAR)
        || CBM_TypesAreEqual( _type, CBM_UCHAR))
    {
        memcpy( _recv_data, _send_data, sz);
    }
    else if ( CBM_TypesAreEqual( _type, CBM_INT))
    {
        _CBM_ConvertInt< CBM_Int >::convert( _recv_data, _send_data, _recv_sz, &units);
    }
    else if ( CBM_TypesAreEqual( _type, CBM_LONG))
    {
        _CBM_ConvertInt< CBM_Long >::convert( _recv_data, _send_data, _recv_sz, &units);
    }
    else if ( CBM_TypesAreEqual( _type, CBM_SHORT))
    {
        _CBM_ConvertInt< CBM_Short >::convert( _recv_data, _send_data, _recv_sz, &units);
    }
    else
    {
        CBM_LogError( "not implemented");
        CBM_Assert( 1);
        return  -1;
    }
    return  0;
}

static CBM_Transform  s_CBM_TransformConvertUnit = { "?", &_cbm_transform_convertunit, NULL };
CBM_Transform *  CBM_TransformConvertUnit = &s_CBM_TransformConvertUnit;

#if defined(__cplusplus)
} /* extern C */
#endif


int CBM_UnitsParse( CBM_Unit *  _unit,
        char const *  _unit_name)
{
    return  __cbmunits()->parse_unit( _unit, _unit_name);
}

cbm::string_t CBM_UnitName( CBM_Unit const &  _unit)
{
    return  __cbmunits()->symbols( _unit);
}

int CBM_UnitsEqual( CBM_Unit const &  _unit_1,
        CBM_Unit const &  _unit_2)
{
    return  __cbmunits()->equal( _unit_1, _unit_2) ? 1 : 0;
}

int CBM_UnitsCompatible( CBM_Unit const &  _unit_1,
        CBM_Unit const &  _unit_2)
{
    return  __cbmunits()->convertible( _unit_1, _unit_2) ? 1 : 0;
}

