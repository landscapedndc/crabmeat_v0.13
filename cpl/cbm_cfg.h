
#ifndef  CBM_CPL_CONFIG_H_
#define  CBM_CPL_CONFIG_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  <stddef.h>

/* Configuration */

/* Maximum size of an URI */
#define  CBM_UriSize  128 /*terminating \0 inclusive*/

/* Out-of-order subscription allows
 * publish/subscribe request to resolve
 * at a later stage. */
#define  CBM_OutOfOrderSubscription  1

/* Minimum allocation size specifies the
 * size in bytes that shall be "preallocated"
 * not requiring a call to, e.g., malloc. */
#define  CBM_FieldMinAllocSize  ((int)(2*sizeof(double)))

/* Initial size of allocated memory blocks
 * used to serve memory for field data */
#define  CBM_CouplerInitialMemoryPoolBlockSize  32


#include  "units/cbm_units.h"
typedef cbm_unit_t CBM_Unit;
#define  CBM_NoUnit  "-"

#if defined(__cplusplus)
extern "C" {
#endif

CBM_API extern int const CBM_DynamicSize;

typedef unsigned long int CBM_Hash;

/* Error codes adapted from UnQLite (Symisc Systems http://unqlite.org) sources */
#define  CBM_RET_OK       0      /* Not an error */    
#define  CBM_ERR_NOMEM    (-1)   /* Out of memory */
#define  CBM_ERR_IO       (-2)   /* IO error */
#define  CBM_ERR_EMPTY    (-3)   /* Empty field */
#define  CBM_ERR_ORANGE   (-5)   /* Out of range value */
#define  CBM_ERR_NOTFOUND (-6)   /* Item not found */
#define  CBM_ERR_LIMIT    (-7)   /* Limit reached */
#define  CBM_ERR_INVALID  (-9)   /* Invalid parameter */
#define  CBM_ERR_EXISTS   (-11)  /* Item exists */
#define  CBM_ERR_UNKNOWN  (-13)  /* Unknown error */
#define  CBM_ERR_NOTIMPLEMENTED  (-17) /* Operation not implemented */
#define  CBM_ERR_EOF      (-18) /* End of input */
#define  CBM_ERR_PERM     (-19) /* Permission error */
#define  CBM_ERR_OS       (-23) /* System call return an error */
#define  CBM_ERR_CONTINUE (-25) /* Not an error: Operation in progress */
#define  CBM_ERR_SHORT    (-29) /* Buffer too short */
#define  CBM_ERR_PATH     (-30) /* Path error */
#define  CBM_ERR_TIMEOUT  (-31) /* Timeout */
#define  CBM_ERR_BIG      (-32) /* Too big for processing */
#define  CBM_ERR_RETRY    (-33) /* Retry your call */
#define  CBM_ERR_TRANSFORM (-34) /* Data transform error */
#define  CBM_ERR_CALLBACK (-35) /* Data transform error */
#define  CBM_ERR_IGNORE   (-63) /* Ignore */


// sk:?? struct CBM_CommonBase
// sk:?? {
// sk:??     int  err;
// sk:?? };
// sk:?? 
// sk:?? #define  CBM_Error(obj) (CBM_CommonBase *)(obj.mem)->err

#define  cbm_status(reg,rc) if ( reg) *(reg) = rc /*;*/

/* Query keywords in URI */
#define CBM_UriQUnit "unit"
#define CBM_UriQSpatial "dS"
#define CBM_UriQTemporal "dT"
#define CBM_UriQPart "part"

typedef struct CBM_FieldStats
{
    int  publish_count;     /*number of publish calls*/
    int  subscribe_count;   /*number of subscribe calls*/
    int  send_count;        /*number of send calls*/
    int  receive_count;     /*number of receive calls*/

    unsigned long int  bytes_sent;
    unsigned long int  bytes_received;

    CBM_Unit  unit;
} CBM_FieldStats;

#if defined(__cplusplus)
}
#endif

#endif  /*  !CBM_CPL_CONFIG_H_  */

