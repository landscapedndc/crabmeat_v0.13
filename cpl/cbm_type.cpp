
#include  "cbm_type.h"

#if defined(__cplusplus)
extern "C" {
#endif

CBM_Datatype const CBM_VOID = { /*.ID=*/0, /*.size=*/0 };

CBM_Datatype const CBM_FLOAT = { /*.ID=*/1, /*.size=*/sizeof(CBM_Float) };
CBM_Datatype const CBM_DOUBLE = { /*.ID=*/2, /*.size=*/sizeof(CBM_Double) };

CBM_Datatype const CBM_CHAR = { /*.ID=*/3, /*.size=*/sizeof(CBM_Char) };
CBM_Datatype const CBM_UCHAR = { /*.ID=*/4, /*.size=*/sizeof(CBM_UChar) };

CBM_Datatype const CBM_SHORT = { /*.ID=*/5, /*.size=*/sizeof(CBM_Short) };
CBM_Datatype const CBM_USHORT = { /*.ID=*/6, /*.size=*/sizeof(CBM_UShort) };

CBM_Datatype const CBM_INT = { /*.ID=*/7, /*.size=*/sizeof(CBM_Int) };
CBM_Datatype const CBM_UINT = { /*.ID=*/8, /*.size=*/sizeof(CBM_UInt) };

CBM_Datatype const CBM_LONG = { /*.ID=*/9, /*.size=*/sizeof(CBM_Long) };
CBM_Datatype const CBM_ULONG = { /*.ID=*/10, /*.size=*/sizeof(CBM_ULong) };

#if defined(__cplusplus)
}
#endif

CBM_Datatype const  cbm_type< CBM_Float >::type = CBM_FLOAT;
CBM_Datatype const  cbm_type< CBM_Double >::type = CBM_DOUBLE;

CBM_Datatype const  cbm_type< CBM_Char >::type = CBM_CHAR;
CBM_Datatype const  cbm_type< CBM_UChar >::type = CBM_UCHAR;

CBM_Datatype const  cbm_type< CBM_Short >::type = CBM_SHORT;
CBM_Datatype const  cbm_type< CBM_UShort >::type = CBM_USHORT;
CBM_Datatype const  cbm_type< CBM_Int >::type = CBM_INT;
CBM_Datatype const  cbm_type< CBM_UInt >::type = CBM_UINT;
CBM_Datatype const  cbm_type< CBM_Long >::type = CBM_LONG;
CBM_Datatype const  cbm_type< CBM_ULong >::type = CBM_ULONG;

