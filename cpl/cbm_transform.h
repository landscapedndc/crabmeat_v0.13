/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: nov 05, 2016)
 */

#ifndef  CBM_CPL_TRANSFORM_H_
#define  CBM_CPL_TRANSFORM_H_

#include  "cbm_cfg.h"
#include  "cbm_type.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef int (*CBM_TransformFunction)(
        void * /*consumer data*/, size_t /*consumer data size*/,
        void const * /*producer data*/, size_t /*producer data size*/,
        CBM_Datatype /*common data type*/, void * /*state*/);

typedef struct CBM_Transform
{
    char const *  name;

    CBM_TransformFunction  execute;
    void *  state;

} CBM_Transform;



/* no-op transform */
CBM_API extern CBM_Transform *  CBM_TransformNoop;
/* element-wise sum of two arrays */
CBM_API extern CBM_Transform *  CBM_TransformDotSum;
/* summing elements of array */
CBM_API extern CBM_Transform *  CBM_TransformSum;

typedef struct CBM_TransformConvertUnitData
{
    CBM_Unit  from;
    CBM_Unit  to;
} CBM_TransformConvertUnitData;
/* convert units */
CBM_API extern CBM_Transform *  CBM_TransformConvertUnit;

#if defined(__cplusplus)
}
#endif

extern int  CBM_UnitsParse( CBM_Unit * /*unit handle*/,
        char const * /*unit symbols*/);
#include  "string/cbm_string.h"
extern cbm::string_t  CBM_UnitName( CBM_Unit const & /*unit*/);
extern int  CBM_UnitsEqual( CBM_Unit const & /*unit*/,
        CBM_Unit const & /*unit*/);
extern int  CBM_UnitsCompatible( CBM_Unit const & /*unit*/,
        CBM_Unit const & /*unit*/);

#endif  /* !CBM_CPL_TRANSFORM_H_ */

