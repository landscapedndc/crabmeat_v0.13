/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 10, 2017)
 */

#ifndef  CBM_CPL_EVENT_H_
#define  CBM_CPL_EVENT_H_

#include  "cbm_cfg.h"
#include  "cbm_transform.h"
#include  "cbm_type.h"
#include  "memory/cbm_mempool.h"
#include  "memory/cbm_slaba.h"

#if defined(__cplusplus)
extern "C" {
#endif

typedef  int (*CBM_CallbackFn)( void const * /*message*/,
        size_t /*message size*/, void * /*custom data*/);

typedef struct CBM_Callback
{
    CBM_CallbackFn  fn;
    void *  data;
} CBM_Callback;

CBM_API extern CBM_Callback const CBM_CallbackNone;

#if defined(__cplusplus)
}
#endif

#endif /* !CBM_CPL_EVENT_H_ */

