/*!
 * @brief
 *  contains the implementation of the logging methods.
 *  These are logging functionalities on various levels of logging.
 *
 * @author
 *  steffen klatt,
 *  edwin haas
 */

#define  CRABMEAT_GENERATE_GLOBAL_INSTANCES
#include  "logging/cbm_logging.h"
#include  "cbm_rtcfg.h"

#include  "io/default-streams.h"

#include  "utils/cbm_utils.h"
#include  "memory/cbm_mem.h"
#include  "string/cbm_string.h"

#include  "time/cbm_time.h"

#ifdef  CRABMEAT_OPENMP
#  include  <omp.h>
#endif

#include  <stdlib.h>

#include  <iostream>
#include  <fstream>
#include  <sstream>
#include  <stdexcept>


cbm::logger_t::logger_t(
        char const *  _name)
    : m_name( _name ? _name : invalid_str),
      terminate_on_fatal_error( true),
      n_calls_without_initialize( 0),

      targets_mask( 0),

      logfile_stream( NULL),

      logfile_stream_buffer( ""),
      log_buffersize( 0),
      log_prefix( "")
{
    ldndc::standard_sink_buffer( &this->logfile_stream, "stderr");
    this->logfile_stream.clear();

    this->conf.initialized = 0;
    this->conf.initialize_failed = 0;

    this->conf.is_devnull = 0;

    this->set_prepend_filename();
    this->set_color();

    this->set_log_level( LOGLEVEL_DEFAULT);

    for ( int  l = 0;  l < LOGLEVEL_CNT;  ++l)
    {
        this->msg_counter[l] = 0;
    }
}

cbm::logger_t::~logger_t()
{
    if ( this->is_initialized())
        { this->uninitialize(); }
}


lerr_t
cbm::logger_t::initialize( char const *  _log_file,
        unsigned int  _level, char const *  _prefix)
{
    char const *  log_file = _log_file;
    /* choose default if NULL */
    if ( !log_file)
        { log_file = cbm::LOG_FILE; }

    lerr_t  rc_initialize = LDNDC_ERR_OK;
#ifdef  CRABMEAT_OPENMP
#  pragma omp critical (cbm_logger)
#endif
    {
    rc_initialize =
        this->initialize_log( log_file, _level, _prefix);
    if ( rc_initialize)
        { this->conf.initialize_failed = 1; }
    }

    return  rc_initialize;
}

lerr_t
cbm::logger_t::initialize_log( char const *  _log_file,
        unsigned int  _level, char const *  _prefix)
{
    if ( this->conf.initialized || this->conf.initialize_failed)
    {
        /* silently ignore reinitialization */
        if ( this->conf.initialize_failed)
        {
            return  LDNDC_ERR_FAIL;
        }
        return  LDNDC_ERR_OK;
    }

    /* logging level */
    this->conf.log_level = _level;

    /* logging buffer size */
    int const  conf_log_buffersize = CBM_LibRuntimeConfig.lc.log_buffersize;
    this->log_buffersize =
        ( conf_log_buffersize < 0) ? CBM_LOG_BUFFER_SIZE : conf_log_buffersize;


    lerr_t const  rc_rdbuf =
        ldndc::standard_sink_buffer( &this->logfile_stream, _log_file);
    if ( rc_rdbuf == LDNDC_ERR_OK)
    {
        /* overwrite buffer size for standard sink buffers (e.g. stdout, stderr) */
        this->log_buffersize = 0;
        if ( this->logfile_stream.rdbuf() == NULL)
        {
            this->conf.is_devnull = 1;
            this->conf.log_level = LOGLEVEL_SILENT;
        }
    }
    else if ( rc_rdbuf == LDNDC_ERR_OBJECT_DOES_NOT_EXIST)
    {
        std::string  logfile_dirname;
        cbm::dirname( &logfile_dirname, _log_file);
        if ( cbm::path_exists( logfile_dirname.c_str()))
        {
            if ( !cbm::is_dir( logfile_dirname.c_str()))
            {
                return  LDNDC_ERR_OBJECT_INIT_FAILED;
            }
        }
        else
        {
            lerr_t  rc_mkdir = cbm::mkdirs( logfile_dirname.c_str());
            if ( rc_mkdir)
            {
                std::cerr << "LOG: failed to create directory  [directory="
                    << logfile_dirname << "]" << std::endl;
                return  LDNDC_ERR_OBJECT_INIT_FAILED;
            }
        }
        this->logfile_buf.open( _log_file, std::ios::out|((CBM_LibRuntimeConfig.lc.log_append) ? std::ios::app : std::ios::trunc));
        if ( ! this->logfile_buf.is_open())
        {
            std::cerr << "LOG: log file could not be opened  [logfile=" << _log_file << "]" << std::endl;
            return  LDNDC_ERR_OBJECT_INIT_FAILED;
        }
        this->logfile_stream.rdbuf( &this->logfile_buf);
        this->unset_color();
    }
    else
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    this->log_prefix  = std::string(( cbm::is_empty( _prefix) ? "" : cbm::n2s( _prefix)));
    this->log_prefix += cbm::is_empty( this->log_prefix) ? "" : cbm::n2s( "|");

    /* clear buffer stream */
    this->logfile_stream_buffer.str( std::string());

    /* [FIXME move to client] add client package version and current time to beginning of logfile */
    if (( this->conf.log_level != LOGLEVEL_SILENT) && !this->conf.is_devnull)
    {
        pid_t  pid = cbm::get_pid();
        this->logfile_stream_buffer << "[" << pid << "]  "
            << package_user.name() << " "
            << package_user.version_long() << "\n";
        std::string  t;
        cbm::current_time_as_string( &t);
        this->logfile_stream_buffer << "[" << pid << "]  "
            << t << "\n";
        this->logfile_stream_buffer << "[" << pid << "]  "
            << "running on host \"" << cbm::hostname() << "\"";

        if ( cbm::is_valid( CBM_LibRuntimeConfig.rc.ipaddr))
        {
            this->logfile_stream_buffer << " (" << CBM_LibRuntimeConfig.rc.ipaddr << ")";
        }
        this->logfile_stream_buffer << "\n";

        this->flush_logfile_stream();
    }

    this->conf.initialized = 1;


    return  LDNDC_ERR_OK;
}


void
cbm::logger_t::uninitialize()
{
    if ( !this->conf.initialized)
    {
        std::cerr << "LOG: (warning) logging interface deinited without calling initialize().\n";
        return;
    }
    
    this->flush_logfile_stream();

    if ( this->logfile_buf.is_open())
        { this->logfile_buf.close(); }

    if ( this->n_calls_without_initialize > 0)
    {
        std::cerr << "LOG: (warning) " << this->n_calls_without_initialize
            << " messages arrived before calling initialize().\n";
    }

    this->conf.initialized = 0;
}


void
cbm::logger_t::flush_logfile_stream()
{
    /* flush stream buffer to file */
    this->logfile_stream << this->logfile_stream_buffer.str();
    this->logfile_stream.flush();
    /* clear buffer stream */
    this->logfile_stream_buffer.str( "");
}


void
cbm::logger_t::log( lflags_t const &  _log_target, std::string const &  _msg,
        unsigned int  _msg_loglevel, char const *  _msg_type)
{
    if ( this->conf.is_devnull)
        { return; }

    /* check if target is masked */
    if ( _log_target && ( _log_target & this->targets_mask))
        { return; }

    if ( _msg_loglevel < this->conf.log_level)
    {
#ifdef  CRABMEAT_OPENMP
#  pragma omp critical (cbm_logger)
#endif
        {
            if ( _msg_type && ( _msg_type[0] != '\0'))
            {
                this->logfile_stream_buffer << '[' << _msg_type << ']' << ' ';
            }
            this->logfile_stream_buffer << this->log_prefix << 
#ifdef  CRABMEAT_OPENMP
                omp_get_thread_num() << ' ' <<
#endif
                _msg;

            /* flush when buffer is filled */
            int const  log_streambuffersize =
                static_cast< int >( this->logfile_stream_buffer.tellp());
            if (( _msg_loglevel < LOGLEVEL_ERROR)
                    || ( log_streambuffersize > this->log_buffersize))
            {
                this->flush_logfile_stream();
            }
        }
    }
}



/** logging stream interface **/

void
cbm::logger_t::log_write( lflags_t const &  _target,
        std::string const &  _msg, char const *  _prefix)
{
#ifdef  _DEBUG
    CRABMEAT_FIX_UNUSED(_target);
    this->log(       0, _msg, 0u, _prefix);
#else
    this->log( _target, _msg, 0u, _prefix);
#endif
}


void
cbm::logger_t::log_info( lflags_t const &  _target,
        std::string const &  _msg, char const *  _prefix)
{
#ifdef  _DEBUG
    CRABMEAT_FIX_UNUSED(_target);
    this->log(       0, _msg, LOGLEVEL_INFO-1, _prefix);
#else
    this->log( _target, _msg, LOGLEVEL_INFO-1, _prefix);
#endif
#ifdef  CRABMEAT_OPENMP
#  pragma omp atomic
#endif
    this->msg_counter[LOGLEVEL_INFO] += 1;
}


void
cbm::logger_t::log_verbose( lflags_t const &  _target,
        std::string const &  _msg, char const *  _prefix)
{
#ifdef  _DEBUG
    CRABMEAT_FIX_UNUSED(_target);
    this->log(       0, _msg, LOGLEVEL_VERBOSE-1, _prefix);
#else
    this->log( _target, _msg, LOGLEVEL_VERBOSE-1, _prefix);
#endif
#ifdef  CRABMEAT_OPENMP
#  pragma omp atomic
#endif
    this->msg_counter[LOGLEVEL_VERBOSE] += 1;
}


void
cbm::logger_t::log_warn( lflags_t const &,
        std::string const &  _msg, char const *  _prefix)
{
    this->log( 0, _msg, LOGLEVEL_WARN-1, _prefix);
#ifdef  CRABMEAT_OPENMP
#  pragma omp atomic
#endif
    this->msg_counter[LOGLEVEL_WARN] += 1;
}


void
cbm::logger_t::log_error( lflags_t const &,
        std::string const &  _msg, char const *  _prefix)
{
    this->log( 0, _msg, LOGLEVEL_ERROR-1, _prefix);
#ifdef  CRABMEAT_OPENMP
#  pragma omp atomic
#endif
    this->msg_counter[LOGLEVEL_ERROR] += 1;
}


void
cbm::logger_t::log_fatal( lflags_t const &,
        std::string const &  _msg, char const *  _prefix)
{
#ifdef  CRABMEAT_OPENMP
#  pragma omp critical (cbm_logger)
#endif
    {
    llog_level_e const  log_level_now = static_cast< llog_level_e >( this->conf.log_level);
    if ( this->terminate_on_fatal_error)
    {
        this->conf.log_level = LOGLEVEL_ERROR;
        this->log( 0, "\n======================================================\n\n", LOGLEVEL_ERROR-1, NULL);
        this->log( 0, "\noops :-(\n\n\tLDNDC encountered an unrecoverable error\n\tand will shut down. The error message follows:\n\n", LOGLEVEL_ERROR-1, NULL);
    }
    this->log( 0, _msg, LOGLEVEL_ERROR-1, _prefix);
    if ( this->terminate_on_fatal_error)
    {
        this->log( 0, "\n======================================================\n\n", LOGLEVEL_ERROR-1, NULL);
        this->conf.log_level = log_level_now;
    }
    this->msg_counter[LOGLEVEL_ERROR] += 1;
    }
    if ( this->terminate_on_fatal_error)
    {
#ifdef  CRABMEAT_PROVIDES_BACKTRACE
        unsigned char  cs_size = 0;
        char **  log_callstack = retrieve_backtrace( 10, &cs_size);
        dump_backtrace( log_callstack, cs_size);

        CBM_LogWrite( "");
#endif
        CBM_LibRuntimeConfig.dump_processed_kernels();
        CBM_LibRuntimeConfig.dump_clock();

        this->uninitialize();
        ::exit( LDNDC_ERR_RUNTIME_ERROR);
    }
}


#ifdef  _DEBUG
void
cbm::logger_t::log_debug( lflags_t const &  _target,
        std::string const &  _msg, char const *  _prefix)
{
    this->log( _target, _msg, LOGLEVEL_DEBUG-1, _prefix);
#ifdef  CRABMEAT_OPENMP
#  pragma omp atomic
#endif
    this->msg_counter[LOGLEVEL_DEBUG] += 1;
}

void
cbm::logger_t::log_todo( lflags_t const &  _target,
        std::string const &  _msg, char const *  _prefix)
{
    this->log( _target, _msg, LOGLEVEL_MAX, _prefix);
}
#endif


bool
cbm::logger_t::silent()
const
{
    return  this->conf.log_level == LOGLEVEL_SILENT;
}
void
cbm::logger_t::set_silent()
{
    this->conf.log_level = LOGLEVEL_SILENT;
}

llog_level_e
cbm::logger_t::log_level()
const
{
    return  static_cast< llog_level_e >( this->conf.log_level);
}
void
cbm::logger_t::set_log_level(
        llog_level_e  _log_level)
{
    this->conf.log_level = _log_level;
}

bool
cbm::logger_t::prepend_filename() const
{
    return  this->conf.prepend_filename;
}
void
cbm::logger_t::set_prepend_filename()
{
    this->conf.prepend_filename = 1;
}
void
cbm::logger_t::unset_prepend_filename()
{
    this->conf.prepend_filename = 0;
}
#include  "string/cbm_string.h"
char const *
cbm::logger_t::format_filename(
        char const *  _filename) const
{
    return  strrchr( _filename, '/') ? strrchr( _filename, '/') + 1 : _filename;
}

bool
cbm::logger_t::color() const
{
    return  this->conf.color;
}
void
cbm::logger_t::set_color()
{
    this->conf.color = 1;
    this->log_level_prefix = static_cast< char const **>( LOGLEVEL_PREFIX_COLOR);
}
void
cbm::logger_t::unset_color()
{
    this->conf.color = 0;
    this->log_level_prefix = static_cast< char const **>( LOGLEVEL_PREFIX);
}

lflags_t const &
cbm::logger_t::log_targets_mask(
        lflags_t const *  _mask)
{
    if ( _mask)
    {
        this->targets_mask = *_mask;
    }
    return  this->targets_mask;
}

int
cbm::logger_t::messages_count( llog_level_e  _loglevel)
const
{
    int  n_msg = -1;
    if ( _loglevel == LOGLEVEL_CNT)
    {
        n_msg = 0;
        for ( int  l = 0;  l < this->log_level()+1;  ++l)
        {
            n_msg += this->msg_counter[l];
        }
    }
    else
    {
        n_msg = this->msg_counter[_loglevel];
    }
    return  n_msg;
}


/* logger manager */
#include  "log/cbm_baselog.h"
cbm::registered_loggers_t::registered_loggers_t()
{
    this->new_logger( CBM_DEFAULT_LOGGER_NAME);
    CBM_LogDebug( "(", this->loggers.size(),"::",
        (void*)this, ") created default logger '",
            CBM_DEFAULT_LOGGER_NAME, "'");
}
cbm::registered_loggers_t::~registered_loggers_t()
{
    this->shutdown();
}

cbm::logger_t *
cbm::registered_loggers_t::new_logger(
        char const *  _name)
{
    if ( !_name || cbm::is_empty( _name) || cbm::is_invalid( _name))
    {
        return  NULL;
    }

    cbm::logger_t *  this_logger = this->get_logger( _name);
    if ( !this_logger)
    {
        this_logger = CBM_DefaultAllocator->construct< cbm::logger_t >();
        if ( this_logger)
        {
            this_logger->set_name( _name);
            this->loggers.push_back( this_logger);
        }
    }
    return  this_logger;
}

void
cbm::registered_loggers_t::delete_logger(
        char const *  _name)
{
    if ( !_name || cbm::is_empty( _name))
    {
        return;
    }

    cbm::logger_t *  this_logger = this->get_logger( _name);
    if ( !this_logger || ( this_logger == this->default_logger()))
    {
        /* can't delete non-existing or default logger */
        return;
    }

    std::list< cbm::logger_t * >::iterator  llog_it = this->loggers.begin();
    for( ; llog_it != this->loggers.end();  ++llog_it)
    {
        if ( cbm::is_equal( _name, (*llog_it)->name()))
        {
            CBM_DefaultAllocator->destroy( *llog_it);
            this->loggers.erase( llog_it);
            break;
        }
    }
}

cbm::logger_t *
cbm::registered_loggers_t::get_logger(
        char const *  _name)
{
    if ( this->loggers.size() == 0)
    {
        return  NULL;
    }

    if ( !_name || cbm::is_empty( _name))
    {
        return  this->default_logger();
    }

    std::list< cbm::logger_t * >::iterator  llog_it = this->loggers.begin();
    for( ; llog_it != this->loggers.end();  ++llog_it)
    {
        if ( cbm::is_equal( _name, (*llog_it)->name()))
        {
            return  *llog_it;
        }
    }
    return  NULL;
}


cbm::logger_t *
cbm::registered_loggers_t::default_logger()
{
    /* beware of endless loop ;-) */
    return  this->get_logger( CBM_DEFAULT_LOGGER_NAME);
}

void
cbm::registered_loggers_t::set_silent()
{
    std::list< cbm::logger_t * >::iterator  llog_it = this->loggers.begin();
    for( ; llog_it != this->loggers.end();  ++llog_it)
    {
        (*llog_it)->set_silent();
    }
}

void
cbm::registered_loggers_t::broadcast( lflags_t const &  _target,
        std::string const &  _msg, char const *  _prefix)
{
    std::list< cbm::logger_t * >::iterator  llog_it = this->loggers.begin();
    for( ; llog_it != this->loggers.end();  ++llog_it)
    {
        (*llog_it)->log_write( _target, _msg, _prefix);
    }
}

void
cbm::registered_loggers_t::shutdown()
{
    while ( ! this->loggers.empty())
    {
        CBM_DefaultAllocator->destroy( this->loggers.back());
        this->loggers.pop_back();
    }

    this->loggers.clear();
}


cbm::registered_loggers_t &  cbm::RegisteredLoggers()
{
    static  registered_loggers_t  global_CBM_RegisteredLoggers;
    return  global_CBM_RegisteredLoggers;
}

cbm::logger_t *  cbm::DefaultLogger()
{
    static  cbm::logger_t *  global_CBM_DefaultLogger =
                        RegisteredLoggers().default_logger();
    return  global_CBM_DefaultLogger;
}

