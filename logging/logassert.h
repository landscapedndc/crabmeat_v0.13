/*!
 * @brief
 *    defines assertion macro
 *
 * @author
 *    steffen klatt  (created on: jan 07, 2012)
 */

#ifndef  CBM_LOGASSERT_H_
#define  CBM_LOGASSERT_H_

#include  "crabmeat-config.h"
#include  "logging/logvariadic.h"

/*!
 * brief
 *      custom made assertion
 *
 * @see
 *      http://www.devarticles.com/c/a/Cplusplus/C-plus-plus-Preprocessor-Always-Assert-Your-Code-Is-Right
 */
#ifdef  CRABMEAT_DEBUG
#include  "log/cbm_baselog.h"
#include  "cbm_matchbase.h"

/* object id owner */
template < typename  _K, bool >
struct  __llogassert_idowner_t;

#include  "cbm_id.h"
template < typename  _K >
struct  __llogassert_idowner_t< _K, false >
{
    static inline  lid_t const *  object_id( _K const *)
    {
        return  NULL;
    }
};
#include  "cbm_id.h"
template < typename  _K >
struct  __llogassert_idowner_t< _K, true >
{
    static inline  lid_t const *  object_id( _K const *  _obj)
    {
        return  &_obj->object_id();
    }
};

#include  "cbm_id.h"
template < typename  _K >
lid_t const *
__llogassert_get_if_idowner(
                _K const *  _obj)
{
    return  __llogassert_idowner_t< _K, cbm::match_base_t< cbm::objectid_owner_t, _K >::is_match >::object_id( _obj);
}

#  ifdef  CRABMEAT_HAVE_ASSERT
#    define  crabmeat_assert(__condition__)            \
        __crabmeat_assert(__condition__,"assertion failed:")
#  else
#    define  crabmeat_assert(__unused__)      CRABMEAT_NOOP
#  endif  /*  CRABMEAT_HAVE_ASSERT  */
#  ifdef  CRABMEAT_HAVE_KASSERT
#    define  crabmeat_kassert(__condition__)                                                \
        __crabmeat_assert(__condition__,"assertion failed",                                        \
            "  [",                                                        \
            "object-id=",((__llogassert_get_if_idowner(this)?cbm::n2s(*__llogassert_get_if_idowner(this)):"?")),    \
/*            ",timestep=",((__llogassert_get_if_clockowner(this))?__llogassert_get_if_clockowner(this)->now().to_string():"?"),*/\
            ",time=",(cbm::LibRuntimeConfig().clk?cbm::LibRuntimeConfig().clk->now().as_iso8601().c_str():"?"), \
            "]\n")
#  else
#    define  crabmeat_kassert(__unused__)     CRABMEAT_NOOP
#  endif  /*  CRABMEAT_HAVE_KASSERT  */
#else
#  define  crabmeat_assert(__unused__)     CRABMEAT_NOOP
#  define  crabmeat_kassert(__unused__)     CRABMEAT_NOOP
#endif  /*  CRABMEAT_DEBUG  */

#endif /* !CBM_LOGASSERT_H_ */

