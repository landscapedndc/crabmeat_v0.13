/*!
 * @brief
 *  provides logging facility for simulation system
 *
 * @author
 *  steffen klatt,
 *  edwin haas
 */

#ifndef  CBM_LOGGING_H_
#define  CBM_LOGGING_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"

#include  "log/loglevels.h"

#include  "utils/signal/cbm_signal.h"

#include  <string>
#include  <sstream>
#include  <fstream>

#include  "logging/logvariadic.h"
#include  "logging/logassert.h"

#include  "cbm_rtcfg.h"

template < typename _T >
void
__ldndc_unreferenced_variable( _T &/*_not_needed*/)
{
    /* me is empty */
}


#define  ldndc_not_supported(__condition__,__desc__)                    \
    if ( ( __condition__))                                \
    {                                        \
        CBM_LogInfo( __desc__);                            \
        CBM_LogFatal( "program terminated due to request of unsupported feature");    \
    }

namespace cbm
{
class CBM_API logger_t
{
    public:
        /*!
         * @param
         *    logger name
         */
        logger_t( char const * = NULL);
        ~logger_t();

    public:
        char const *  name() const
            { return  this->m_name.c_str(); }
        void  set_name( char const *  _name)
        {
            if ( _name)
                { this->m_name = _name; }
        }
    private:
        std::string  m_name;

    public:
        /*!
         * @brief
         *    opens log file and progress file output streams.
         *
         * @param
         *    log file stream name
         * @param
         *    log level
         * @param
         *    message line prefix
         */
        lerr_t  initialize( char const * = NULL,
            unsigned int = LOGLEVEL_DEFAULT, char const * = "");

        /*!
         * @brief
         *    closes the logging streams.
         */
        void  uninitialize();
        /*!
         * @brief
         *    report initialization status
         */
        bool  is_initialized() const
            { return  this->conf.initialized; }

        /*!
         * @brief
         *    used to write informational messages
         */
        void  log_write( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_write_prefix()
            { return  this->log_level_prefix[LOGLEVEL_SILENT]; }

        /*!
         * @brief
         *    used to write informational messages
         */
        void  log_info( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_info_prefix()
            { return  this->log_level_prefix[LOGLEVEL_INFO]; }

        /*!
         * @brief
         *    used to write more informational messages
         */
        void  log_verbose( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_verbose_prefix()
            { return  this->log_level_prefix[LOGLEVEL_VERBOSE]; }


#ifdef  _DEBUG
        /*!
         * @brief
         *    used to write debug level messages
         */
        void  log_debug( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_debug_prefix()
            { return  this->log_level_prefix[LOGLEVEL_DEBUG]; }
        /*!
         * @brief 
         *    used to write messages into the log file
         *    when executing code that is tagged as
         *    incomplete.
         */
        void  log_todo( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_todo_prefix()
            { return  "TT"; }
#endif

        /*!
         * @brief 
         *    used to write warning level messages
         */
        void  log_warn( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_warn_prefix()
            { return  this->log_level_prefix[LOGLEVEL_WARN]; }
        /*!
         * @brief 
         *    used to write error level messages
         */
        void  log_error( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_error_prefix()
            { return  this->log_level_prefix[LOGLEVEL_ERROR]; }
        /*!
         * @brief 
         *    writes fatal level messages to log stream
         *    and throws run time exception
         */
        void  log_fatal( lflags_t const &,
                std::string const &, char const * = NULL);
        char const *  log_fatal_prefix()
            { return  "FF"; }
        bool  terminate_on_fatal_error;

        /*!
         * @brief
         *    sets the log level to silent
         */
        bool  silent() const;
        void  set_silent();

        /*!
         * @brief
         *    returns the current log level
         */
        llog_level_e  log_level() const;
        /*!
         * @brief
         *    set the current log level
         */
        void  set_log_level( llog_level_e);

        /*!
         * @brief
         *    return if the source code file name is
         *    prepended to each logging message
         */
        bool  prepend_filename() const;
        /*!
         * @brief
         *    request the source code file name to
         *    (not) be prepended to each logging message
         */
        void  set_prepend_filename();
        void  unset_prepend_filename();
        /*!
         * @brief
         *    strip leading directories from filename
         */
        char const *  format_filename(
                char const * /*filename*/) const;

        /*!
         * @brief
         *    return if colors should be used
         */
        bool  color() const;
        /*!
         * @brief
         *    request colors to (not) be used
         */
        void  set_color();
        void  unset_color();
        /*!
         * @brief
         *    returns the current log target mask. if
         *    an argument is passed to this function
         *    the mask is set to the new value.
         */
        lflags_t const &  log_targets_mask(
                lflags_t const * = NULL);

        int  messages_count(
                llog_level_e = LOGLEVEL_CNT) const;

    private:
        /*!
         * @brief
         *    writes the buffered output to stream
         */
        void  flush_logfile_stream();

        struct
        {
            bool  initialized:1;
            bool  initialize_failed:1;

            bool  is_devnull:1;

            bool  prepend_filename:1;
            bool  color:1;
            bool  __dummy__:1;

            unsigned int  log_level:4;
        } conf;
        /* holds number of emitted logging messages */
        int  msg_counter[LOGLEVEL_CNT];

        /* number of messages received without
         * prior initialization */
        int  n_calls_without_initialize;
        lflags_t  targets_mask;

        std::filebuf  logfile_buf;
        std::ostream  logfile_stream;

#ifdef  _DEBUG
#  define  CBM_LOG_BUFFER_SIZE  (0) /* flush right away */
#else
#  define  CBM_LOG_BUFFER_SIZE  (2*1024*1024) /* 2 MB */
#endif
        std::stringstream  logfile_stream_buffer;
        int  log_buffersize;

        std::string  log_prefix;
        char const **  log_level_prefix;

        lerr_t  initialize_log( char const * /*logging stream name*/,
            unsigned int /*logging level*/, char const * /*message prefix*/);

        /* does the actual write-work */
        void  log( lflags_t const & /*logging target*/,
            std::string const & /*logging message*/, unsigned int /*logging level*/,
                char const * /*logging message type mnemonic*/);
};
} /* namespace cbm */

/* define all available logging methods */
LDNDC_LOG_DEFN

#include  <list>
#define  CBM_DEFAULT_LOGGER_NAME  "__default__"
namespace cbm
{
class  CBM_API registered_loggers_t
{
    public:
        explicit  registered_loggers_t();
        virtual ~registered_loggers_t();

    public:
        cbm::logger_t *  new_logger( char const * /*logger name*/);
        void  delete_logger( char const * /*logger name*/);

        cbm::logger_t *  get_logger( char const * /*logger name*/);
        cbm::logger_t *  default_logger();

        void  set_silent();

        /*!
         * @brief
         *    send logging message to all
         *    (registered) logging streams
         */
        void  broadcast(
                lflags_t const & /*flags*/,
                std::string const & /*message*/,
                char const * = "  " /*prefix*/);

        void  shutdown();

    private:
        std::list< cbm::logger_t * >  loggers;

    private:
        registered_loggers_t( registered_loggers_t const &);
        registered_loggers_t &  operator=( registered_loggers_t const &);
};

} /* namespace cbm */

/* global logger manager */
namespace cbm {
    extern CBM_API  registered_loggers_t &  RegisteredLoggers(); }
#define  CBM_RegisteredLoggers  cbm::RegisteredLoggers()

/* global default logger */
namespace cbm {
    extern CBM_API  cbm::logger_t *  DefaultLogger(); }
#define  CBM_DefaultLogger  cbm::DefaultLogger()


#endif /* !CBM_LOGGING_H_ */

