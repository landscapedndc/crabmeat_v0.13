/*!
 * @brief
 *    defines all kinds of logging macros
 *
 * @author
 *    steffen klatt  (created on: jan 07, 2012)
 */

#ifndef  CBM_LOGVARIADIC_H_
#define  CBM_LOGVARIADIC_H_

/* writing logging message to logger */
#define  __LOGLOG(__type__,__logger__,__msg__,__prefix__) \
    (__logger__)->log_ ## __type__( static_cast<lflags_t>( 0), __msg__, __prefix__)

#define  __LOG_PREFIX(__type__,__logger__)  (__logger__)->log_ ## __type__ ## _prefix()

/* creating single logging message from various arguments */
/*    arbitrary logger */
#define  __LOG_LINE_TO_(__type__,__logger__,...)  __ldndc_log_ ## __type__(__logger__,NULL,__LINE__,NULL,NULL,invalid_lid,"","",__VA_ARGS__)
#ifdef  _DEBUG
#  define  __LOG_LINE_TO(__type__,__logger__,...)  __ldndc_log_ ## __type__(__logger__,__FILE__,__LINE__,__LOG_PREFIX(__type__,__logger__),NULL,invalid_lid,CBM_LibRuntimeConfig.now_as_iso8601(),"",__VA_ARGS__)
#else
#  define  __LOG_LINE_TO(__type__,__logger__,...)  __ldndc_log_ ## __type__(__logger__,NULL,__LINE__,__LOG_PREFIX(__type__,__logger__),NULL,invalid_lid,CBM_LibRuntimeConfig.now_as_iso8601(),"",__VA_ARGS__)
#endif
/*    default logger */
#define  __LOG_LINE_(__type__,...)  __LOG_LINE_TO_(__type__,CBM_DefaultLogger,__VA_ARGS__)
#define  __LOG_LINE_ONCE_(__type__,...)  { static int __log_once__ = 1; if ( __log_once__) { __LOG_LINE_TO_(__type__,CBM_DefaultLogger,__VA_ARGS__); } __log_once__ = 0; } CRABMEAT_NOOP
#define  __LOG_LINE(__type__,...)  __LOG_LINE_TO(__type__,CBM_DefaultLogger,__VA_ARGS__)
#define  __LOG_LINE_ONCE(__type__,...)  { static int __log_once__ = 1; if ( __log_once__) { __LOG_LINE_TO(__type__,CBM_DefaultLogger,__VA_ARGS__); } __log_once__ = 0; } CRABMEAT_NOOP

/* logging functions (using default logger)
 *    - _TO versions accept alternative logger object
 *    - underscore versions omit source file and line information
 */
#define  LOGWRITE(...)  __LOG_LINE(write,__VA_ARGS__)
#define  LOGWRITE_ONCE(...)  __LOG_LINE_ONCE(write,__VA_ARGS__)
#define  LOGWRITE_(...)  __LOG_LINE_(write,__VA_ARGS__)
#define  LOGWRITE_ONCE_(...)  __LOG_LINE_ONCE_(write,__VA_ARGS__)
#define  LOGWRITE_TO(__logger__,...)  __LOG_LINE_TO(write,__logger__,__VA_ARGS__)
#define  LOGWRITE_TO_(__logger__,...)  __LOG_LINE_TO_(write,__logger__,__VA_ARGS__)
#define  LOGINFO(...)  __LOG_LINE(info,__VA_ARGS__)
#define  LOGINFO_ONCE(...)  __LOG_LINE_ONCE(info,__VA_ARGS__)
#define  LOGINFO_(...)  __LOG_LINE_(info,__VA_ARGS__)
#define  LOGINFO_ONCE_(...)  __LOG_LINE_ONCE_(info,__VA_ARGS__)
#define  LOGINFO_TO(__logger__,...)  __LOG_LINE_TO(info,__logger__,__VA_ARGS__)
#define  LOGINFO_TO_(__logger__,...)  __LOG_LINE_TO_(info,__logger__,__VA_ARGS__)
#define  LOGVERBOSE(...)  __LOG_LINE(verbose,__VA_ARGS__)
#define  LOGVERBOSE_ONCE(...)  __LOG_LINE_ONCE(verbose,__VA_ARGS__)
#define  LOGVERBOSE_(...)  __LOG_LINE_(verbose,__VA_ARGS__)
#define  LOGVERBOSE_ONCE_(...)  __LOG_LINE_ONCE_(verbose,__VA_ARGS__)
#define  LOGVERBOSE_TO(__logger__,...)  __LOG_LINE_TO(verbose,__logger__,__VA_ARGS__)
#define  LOGVERBOSE_TO_(__logger__,...)  __LOG_LINE_TO_(verbose,__logger__,__VA_ARGS__)
#define  LOGWARN(...)  __LOG_LINE(warn,__VA_ARGS__)
#define  LOGWARN_ONCE(...)  __LOG_LINE_ONCE(warn,__VA_ARGS__)
#define  LOGWARN_(...)  __LOG_LINE_(warn,__VA_ARGS__)
#define  LOGWARN_ONCE_(...)  __LOG_LINE_ONCE_(warn,__VA_ARGS__)
#define  LOGWARN_TO(__logger__,...)  __LOG_LINE_TO(warn,__logger__,__VA_ARGS__)
#define  LOGWARN_TO_(__logger__,...)  __LOG_LINE_TO_(warn,__logger__,__VA_ARGS__)
#define  LOGERROR(...)  __LOG_LINE(error,__VA_ARGS__)
#define  LOGERROR_ONCE(...)  __LOG_LINE_ONCE(error,__VA_ARGS__)
#define  LOGERROR_(...)  __LOG_LINE_(error,__VA_ARGS__)
#define  LOGERROR_ONCE_(...)  __LOG_LINE_ONCE_(error,__VA_ARGS__)
#define  LOGERROR_TO(__logger__,...)  __LOG_LINE_TO(error,__logger__,__VA_ARGS__)
#define  LOGERROR_TO_(__logger__,...)  __LOG_LINE_TO_(error,__logger__,__VA_ARGS__)
/* fatal messages in kernels are redirected to error (no program abort) */
#if  !defined(_DEBUG) && defined(_LDNDC_LOGTARGET_KERNELS)
#  define  LOGFATAL(...)  __LOG_LINE(error,__VA_ARGS__)
#  define  LOGFATAL_(...)  __LOG_LINE_(error,__VA_ARGS__)
#  define  LOGFATAL_TO(__logger__,...)  __LOG_LINE_TO(error,__logger__,__VA_ARGS__)
#  define  LOGFATAL_TO_(__logger__,...)  __LOG_LINE_TO_(error,__logger__,__VA_ARGS__)
#else
#  define  LOGFATAL(...)  __LOG_LINE(fatal,__VA_ARGS__)
#  define  LOGFATAL_(...)  __LOG_LINE_(fatal,__VA_ARGS__)
#  define  LOGFATAL_TO(__logger__,...)  __LOG_LINE_TO(fatal,__logger__,__VA_ARGS__)
#  define  LOGFATAL_TO_(__logger__,...)  __LOG_LINE_TO_(fatal,__logger__,__VA_ARGS__)
#endif
#ifdef  _DEBUG
#  define  LOGDEBUG(...)  __LOG_LINE(debug,__VA_ARGS__);
#  define  LOGDEBUG_ONCE(...)  __LOG_LINE_ONCE(debug,__VA_ARGS__)
#  define  LOGDEBUG_(...)  __LOG_LINE_(debug,__VA_ARGS__);
#  define  LOGDEBUG_ONCE_(...)  __LOG_LINE_ONCE_(debug,__VA_ARGS__)
#  define  LOGDEBUG_TO(__logger__,...)  __LOG_LINE_TO(debug,__logger__,__VA_ARGS__);
#  define  LOGDEBUG_TO_(__logger__,...)  __LOG_LINE_TO_(debug,__logger__,__VA_ARGS__);

#  define  LOGTODO(...)  LOGWARN("TODO")
#  define  LOGNOIMPL(__id__)  LOGWARN("missing implementation... (",__id__,")")
#else
#  define  LOGDEBUG(...)  CRABMEAT_NOOP
#  define  LOGDEBUG_ONCE(...)  CRABMEAT_NOOP
#  define  LOGDEBUG_(...)  CRABMEAT_NOOP
#  define  LOGDEBUG_ONCE_(...)  CRABMEAT_NOOP
#  define  LOGDEBUG_TO(__logger__,...)  CRABMEAT_NOOP
#  define  LOGDEBUG_TO_(__logger__,...)  CRABMEAT_NOOP

#  define  LOGTODO(...)  CRABMEAT_NOOP
#  define  LOGNOIMPL(__id__)  CRABMEAT_NOOP
#endif  /*  _DEBUG  */


/* creating single kernel logging message from various arguments */
/*    arbitrary logger */
#define  __KLOG_LINE_TO_(__type__,__logger__,...)  __ldndc_log_ ## __type__(__logger__,NULL,__LINE__,NULL,NULL,invalid_lid,NULL,"",__VA_ARGS__)
#ifdef  _DEBUG
#  define  __KLOG_LINE_TO(__type__,__logger__,...)  __ldndc_log_ ## __type__(__logger__,__FILE__,__LINE__,__LOG_PREFIX(__type__,__logger__),this->name(),this->object_id(),CBM_LibRuntimeConfig.now_as_iso8601(),"",__VA_ARGS__)
#else
#  define  __KLOG_LINE_TO(__type__,__logger__,...)  __ldndc_log_ ## __type__(__logger__,NULL,__LINE__,__LOG_PREFIX(__type__,__logger__),this->name(),this->object_id(),CBM_LibRuntimeConfig.now_as_iso8601(),"",__VA_ARGS__)
#endif
/*    default logger */
#define  __KLOG_LINE_(__type__,...)  __KLOG_LINE_TO_(__type__,CBM_DefaultLogger,__VA_ARGS__)
#define  __KLOG_LINE_ONCE_(__type__,...)  { static int __log_once__ = 1; if ( __log_once__) { __KLOG_LINE_TO_(__type__,CBM_DefaultLogger,__VA_ARGS__); } __log_once__ = 0; } CRABMEAT_NOOP
#define  __KLOG_LINE(__type__,...)  __KLOG_LINE_TO(__type__,CBM_DefaultLogger,__VA_ARGS__)
#define  __KLOG_LINE_ONCE(__type__,...)  { static int __log_once__ = 1; if ( __log_once__) { __KLOG_LINE_TO(__type__,CBM_DefaultLogger,__VA_ARGS__); } __log_once__ = 0; } CRABMEAT_NOOP

#define  KLOGWRITE(...)  __KLOG_LINE(write,__VA_ARGS__)
#define  KLOGWRITE_ONCE(...)  __KLOG_LINE_ONCE(write,__VA_ARGS__)
#define  KLOGWRITE_(...)  __KLOG_LINE_(write,__VA_ARGS__)
#define  KLOGWRITE_ONCE_(...)  __KLOG_LINE_ONCE_(write,__VA_ARGS__)
#define  KLOGWRITE_TO(__logger__,...)  __KLOG_LINE_TO(write,__logger__,__VA_ARGS__)
#define  KLOGWRITE_TO_(__logger__,...)  __KLOG_LINE_TO_(write,__logger__,__VA_ARGS__)
#define  KLOGINFO(...)  __KLOG_LINE(info,__VA_ARGS__)
#define  KLOGINFO_ONCE(...)  __KLOG_LINE_ONCE(info,__VA_ARGS__)
#define  KLOGINFO_(...)  __KLOG_LINE_(info,__VA_ARGS__)
#define  KLOGINFO_ONCE_(...)  __KLOG_LINE_ONCE_(info,__VA_ARGS__)
#define  KLOGINFO_TO(__logger__,...)  __KLOG_LINE_TO(info,__logger__,__VA_ARGS__)
#define  KLOGINFO_TO_(__logger__,...)  __KLOG_LINE_TO_(info,__logger__,__VA_ARGS__)
#define  KLOGVERBOSE(...)  __KLOG_LINE(verbose,__VA_ARGS__)
#define  KLOGVERBOSE_ONCE(...)  __KLOG_LINE_ONCE(verbose,__VA_ARGS__)
#define  KLOGVERBOSE_(...)  __KLOG_LINE_(verbose,__VA_ARGS__)
#define  KLOGVERBOSE_ONCE_(...)  __KLOG_LINE_ONCE_(verbose,__VA_ARGS__)
#define  KLOGVERBOSE_TO(__logger__,...)  __KLOG_LINE_TO(verbose,__logger__,__VA_ARGS__)
#define  KLOGVERBOSE_TO_(__logger__,...)  __KLOG_LINE_TO_(verbose,__logger__,__VA_ARGS__)
#define  KLOGWARN(...)  __KLOG_LINE(warn,__VA_ARGS__)
#define  KLOGWARN_ONCE(...)  __KLOG_LINE_ONCE(warn,__VA_ARGS__)
#define  KLOGWARN_(...)  __KLOG_LINE_(warn,__VA_ARGS__)
#define  KLOGWARN_ONCE_(...)  __KLOG_LINE_ONCE_(warn,__VA_ARGS__)
#define  KLOGWARN_TO(__logger__,...)  __KLOG_LINE_TO(warn,__logger__,__VA_ARGS__)
#define  KLOGWARN_TO_(__logger__,...)  __KLOG_LINE_TO_(warn,__logger__,__VA_ARGS__)
#define  KLOGERROR(...)  __KLOG_LINE(error,__VA_ARGS__)
#define  KLOGERROR_ONCE(...)  __KLOG_LINE_ONCE(error,__VA_ARGS__)
#define  KLOGERROR_(...)  __KLOG_LINE_(error,__VA_ARGS__)
#define  KLOGERROR_ONCE_(...)  __KLOG_LINE_ONCE_(error,__VA_ARGS__)
#define  KLOGERROR_TO(__logger__,...)  __KLOG_LINE_TO(error,__logger__,__VA_ARGS__)
#define  KLOGERROR_TO_(__logger__,...)  __KLOG_LINE_TO_(error,__logger__,__VA_ARGS__)
/* fatal messages in kernels are redirected to error (no program abort) */
#if  !defined(_DEBUG) && defined(_LDNDC_LOGTARGET_KERNELS)
#  define  KLOGFATAL(...)  __KLOG_LINE(error,__VA_ARGS__)
#  define  KLOGFATAL_(...)  __KLOG_LINE_(error,__VA_ARGS__)
#  define  KLOGFATAL_TO(__logger__,...)  __KLOG_LINE_TO(error,__logger__,__VA_ARGS__)
#  define  KLOGFATAL_TO_(__logger__,...)  __KLOG_LINE_TO_(error,__logger__,__VA_ARGS__)
#else
#  define  KLOGFATAL(...)  __KLOG_LINE(fatal,__VA_ARGS__)
#  define  KLOGFATAL_(...)  __KLOG_LINE_(fatal,__VA_ARGS__)
#  define  KLOGFATAL_TO(__logger__,...)  __KLOG_LINE_TO(fatal,__logger__,__VA_ARGS__)
#  define  KLOGFATAL_TO_(__logger__,...)  __KLOG_LINE_TO_(fatal,__logger__,__VA_ARGS__)
#endif
#ifdef  _DEBUG
#  define  KLOGDEBUG(...)  __KLOG_LINE(debug,__VA_ARGS__);
#  define  KLOGDEBUG_ONCE(...)  __KLOG_LINE_ONCE(debug,__VA_ARGS__);
#  define  KLOGDEBUG_(...)  __KLOG_LINE_(debug,__VA_ARGS__);
#  define  KLOGDEBUG_ONCE_(...)  __KLOG_LINE_ONCE_(debug,__VA_ARGS__);
#  define  KLOGDEBUG_TO(__logger__,...)  __KLOG_LINE_TO(debug,__logger__,__VA_ARGS__);
#  define  KLOGDEBUG_TO_(__logger__,...)  __KLOG_LINE_TO_(debug,__logger__,__VA_ARGS__);

#  define  KLOGTODO(...)  KLOGWARN_ONCE("TODO")
#  define  KLOGNOIMPL(__id__)  KLOGWARN_ONCE("missing implementation... (",__id__,")")
#else
#  define  KLOGDEBUG(...)  CRABMEAT_NOOP
#  define  KLOGDEBUG_ONCE(...)  CRABMEAT_NOOP
#  define  KLOGDEBUG_(...)  CRABMEAT_NOOP
#  define  KLOGDEBUG_ONCE_(...)  CRABMEAT_NOOP
#  define  KLOGDEBUG_TO(__logger__,...)  CRABMEAT_NOOP
#  define  KLOGDEBUG_TO_(__logger__,...)  CRABMEAT_NOOP

#  define  KLOGTODO(...)  CRABMEAT_NOOP
#  define  KLOGNOIMPL(__id__)  CRABMEAT_NOOP
#endif  /*  _DEBUG  */


/* following noise allows LOG{WRITE,INFO,DEBUG,...} to be used like
 * print in python for a list of n arguments (n is the highest value
 * seen below (e.g. see 'typename _Tn')
 */

/* maximum number of supported arguments to logging functions */
#define  __LOG_ARG_MAX__  24

/* helpers */
#define  __log_glue(__e1__,__e2__) __e1__##__e2__

/* expand to "typename _T1, typename _T2, ..., typename _T<__LOG_ARG_MAX__>" */
#define  __LOG_TMPL_ARG_1  typename _T1
#define  __LOG_TMPL_ARG_2  __LOG_TMPL_ARG_1, typename _T2
#define  __LOG_TMPL_ARG_3  __LOG_TMPL_ARG_2, typename _T3
#define  __LOG_TMPL_ARG_4  __LOG_TMPL_ARG_3, typename _T4
#define  __LOG_TMPL_ARG_5  __LOG_TMPL_ARG_4, typename _T5
#define  __LOG_TMPL_ARG_6  __LOG_TMPL_ARG_5, typename _T6
#define  __LOG_TMPL_ARG_7  __LOG_TMPL_ARG_6, typename _T7
#define  __LOG_TMPL_ARG_8  __LOG_TMPL_ARG_7, typename _T8
#define  __LOG_TMPL_ARG_9  __LOG_TMPL_ARG_8, typename _T9
#define  __LOG_TMPL_ARG_10  __LOG_TMPL_ARG_9, typename _T10
#define  __LOG_TMPL_ARG_11  __LOG_TMPL_ARG_10, typename _T11
#define  __LOG_TMPL_ARG_12  __LOG_TMPL_ARG_11, typename _T12
#define  __LOG_TMPL_ARG_13  __LOG_TMPL_ARG_12, typename _T13
#define  __LOG_TMPL_ARG_14  __LOG_TMPL_ARG_13, typename _T14
#define  __LOG_TMPL_ARG_15  __LOG_TMPL_ARG_14, typename _T15
#define  __LOG_TMPL_ARG_16  __LOG_TMPL_ARG_15, typename _T16
#define  __LOG_TMPL_ARG_17  __LOG_TMPL_ARG_16, typename _T17
#define  __LOG_TMPL_ARG_18  __LOG_TMPL_ARG_17, typename _T18
#define  __LOG_TMPL_ARG_19  __LOG_TMPL_ARG_18, typename _T19
#define  __LOG_TMPL_ARG_20  __LOG_TMPL_ARG_19, typename _T20
#define  __LOG_TMPL_ARG_21  __LOG_TMPL_ARG_20, typename _T21
#define  __LOG_TMPL_ARG_22  __LOG_TMPL_ARG_21, typename _T22
#define  __LOG_TMPL_ARG_23  __LOG_TMPL_ARG_22, typename _T23
#define  __LOG_TMPL_ARG_24  __LOG_TMPL_ARG_23, typename _T24

/* expand to "[...] char const *  _delim, _T1 const &  _m1, ..., _T16 const &  _m<__LOG_ARG_MAX__>" */
#define  __LOG_DECL_ARG_1  char const *  _filename, size_t  _lineno, char const *  _prefix, char const *  _obj_name, lid_t const &  _obj_id, std::string const &  _lclock, char const *  _delim, _T1 const &  _m1
#define  __LOG_DECL_ARG_2  __LOG_DECL_ARG_1, _T2 const &  _m2
#define  __LOG_DECL_ARG_3  __LOG_DECL_ARG_2, _T3 const &  _m3
#define  __LOG_DECL_ARG_4  __LOG_DECL_ARG_3, _T4 const &  _m4
#define  __LOG_DECL_ARG_5  __LOG_DECL_ARG_4, _T5 const &  _m5
#define  __LOG_DECL_ARG_6  __LOG_DECL_ARG_5, _T6 const &  _m6
#define  __LOG_DECL_ARG_7  __LOG_DECL_ARG_6, _T7 const &  _m7
#define  __LOG_DECL_ARG_8  __LOG_DECL_ARG_7, _T8 const &  _m8
#define  __LOG_DECL_ARG_9  __LOG_DECL_ARG_8, _T9 const &  _m9
#define  __LOG_DECL_ARG_10  __LOG_DECL_ARG_9, _T10 const &  _m10
#define  __LOG_DECL_ARG_11  __LOG_DECL_ARG_10, _T11 const &  _m11
#define  __LOG_DECL_ARG_12  __LOG_DECL_ARG_11, _T12 const &  _m12
#define  __LOG_DECL_ARG_13  __LOG_DECL_ARG_12, _T13 const &  _m13
#define  __LOG_DECL_ARG_14  __LOG_DECL_ARG_13, _T14 const &  _m14
#define  __LOG_DECL_ARG_15  __LOG_DECL_ARG_14, _T15 const &  _m15
#define  __LOG_DECL_ARG_16  __LOG_DECL_ARG_15, _T16 const &  _m16
#define  __LOG_DECL_ARG_17  __LOG_DECL_ARG_16, _T17 const &  _m17
#define  __LOG_DECL_ARG_18  __LOG_DECL_ARG_17, _T18 const &  _m18
#define  __LOG_DECL_ARG_19  __LOG_DECL_ARG_18, _T19 const &  _m19
#define  __LOG_DECL_ARG_20  __LOG_DECL_ARG_19, _T20 const &  _m20
#define  __LOG_DECL_ARG_21  __LOG_DECL_ARG_20, _T21 const &  _m21
#define  __LOG_DECL_ARG_22  __LOG_DECL_ARG_21, _T22 const &  _m22
#define  __LOG_DECL_ARG_23  __LOG_DECL_ARG_22, _T23 const &  _m23
#define  __LOG_DECL_ARG_24  __LOG_DECL_ARG_23, _T24 const &  _m24

/* expand to "[...] m << _m1 << _m2 << ... << _m16" */
#define  __LOG_CAT_1(__logger__,__not_used__) \
    std::ostringstream  m; \
    int  brace_open = 0; \
    if ( (__logger__)->prepend_filename() && _filename) \
    { \
        m << '{'; \
        brace_open=1; \
        m << (__logger__)->format_filename( _filename) << " +" << _lineno; \
    } \
    if ( _obj_name) \
    { \
        if ( !brace_open) { m << '{'; brace_open=1; } else { m << ';'; } \
        m << _obj_name; \
        if ( _obj_id != invalid_lid) { m << '<' << _obj_id << '>'; } \
    } \
    if ( _lclock.size() > 0) \
    { \
        if ( !brace_open) { m << '{'; brace_open=1; } else { m << ' '; } \
        m << _lclock; \
    } \
    if ( brace_open) { m << "} "; } \
    m << _m1
#define  __LOG_CAT_2(__logger__,__delim__)  __LOG_CAT_1(__logger__,__delim__) << (__delim__) << _m2
#define  __LOG_CAT_3(__logger__,__delim__)  __LOG_CAT_2(__logger__,__delim__) << (__delim__) << _m3
#define  __LOG_CAT_4(__logger__,__delim__)  __LOG_CAT_3(__logger__,__delim__) << (__delim__) << _m4
#define  __LOG_CAT_5(__logger__,__delim__)  __LOG_CAT_4(__logger__,__delim__) << (__delim__) << _m5
#define  __LOG_CAT_6(__logger__,__delim__)  __LOG_CAT_5(__logger__,__delim__) << (__delim__) << _m6
#define  __LOG_CAT_7(__logger__,__delim__)  __LOG_CAT_6(__logger__,__delim__) << (__delim__) << _m7
#define  __LOG_CAT_8(__logger__,__delim__)  __LOG_CAT_7(__logger__,__delim__) << (__delim__) << _m8
#define  __LOG_CAT_9(__logger__,__delim__)  __LOG_CAT_8(__logger__,__delim__) << (__delim__) << _m9
#define  __LOG_CAT_10(__logger__,__delim__)  __LOG_CAT_9(__logger__,__delim__) << (__delim__) << _m10
#define  __LOG_CAT_11(__logger__,__delim__)  __LOG_CAT_10(__logger__,__delim__) << (__delim__) << _m11
#define  __LOG_CAT_12(__logger__,__delim__)  __LOG_CAT_11(__logger__,__delim__) << (__delim__) << _m12
#define  __LOG_CAT_13(__logger__,__delim__)  __LOG_CAT_12(__logger__,__delim__) << (__delim__) << _m13
#define  __LOG_CAT_14(__logger__,__delim__)  __LOG_CAT_13(__logger__,__delim__) << (__delim__) << _m14
#define  __LOG_CAT_15(__logger__,__delim__)  __LOG_CAT_14(__logger__,__delim__) << (__delim__) << _m15
#define  __LOG_CAT_16(__logger__,__delim__)  __LOG_CAT_15(__logger__,__delim__) << (__delim__) << _m16
#define  __LOG_CAT_17(__logger__,__delim__)  __LOG_CAT_16(__logger__,__delim__) << (__delim__) << _m17
#define  __LOG_CAT_18(__logger__,__delim__)  __LOG_CAT_17(__logger__,__delim__) << (__delim__) << _m18
#define  __LOG_CAT_19(__logger__,__delim__)  __LOG_CAT_18(__logger__,__delim__) << (__delim__) << _m19
#define  __LOG_CAT_20(__logger__,__delim__)  __LOG_CAT_19(__logger__,__delim__) << (__delim__) << _m20
#define  __LOG_CAT_21(__logger__,__delim__)  __LOG_CAT_20(__logger__,__delim__) << (__delim__) << _m21
#define  __LOG_CAT_22(__logger__,__delim__)  __LOG_CAT_21(__logger__,__delim__) << (__delim__) << _m22
#define  __LOG_CAT_23(__logger__,__delim__)  __LOG_CAT_22(__logger__,__delim__) << (__delim__) << _m23
#define  __LOG_CAT_24(__logger__,__delim__)  __LOG_CAT_23(__logger__,__delim__) << (__delim__) << _m24


#include  "crabmeat-config.h"
template < int _N >
struct  __log_fail_t
{
#ifndef  CRABMEAT_COMPILER_CLANG
    /* if you get caught here, you used up all
     * available log_<type> interfaces
     *
     * add more __LOG_DECL_n_( n >__LOG_ARG_MAX__)
     *
     * llvm's clang front-end always evaluates this
     * line, which make it choke here.
     */
    char  invalid_array[__LOG_ARG_MAX__-(_N+1)];
#endif
};
#define  __LOG_DEFN_TRAP_(__type__)  __LOG_DEFN_TRAP__(__LOG_ARG_MAX__,__type__)
#define  __LOG_DEFN_TRAP__(__n__,__type__)                    \
    template < __log_glue(__LOG_TMPL_ARG_,__n__), typename _Tx >        \
    void  __ldndc_log_ ## __type__(                        \
                cbm::logger_t *  _logger,                \
                __log_glue(__LOG_DECL_ARG_,__n__),_Tx,...)    \
    {                                    \
        /* fires at point of instantiation */                \
        __log_fail_t<__n__>  log_##__type__##__has_too_many_arguments;    \
        CRABMEAT_FIX_UNUSED(log_##__type__##__has_too_many_arguments);     \
                                        \
        CRABMEAT_FIX_UNUSED(_prefix);                    \
        __log_glue(__LOG_CAT_,__n__)(_logger,_delim);            \
        CRABMEAT_FIX_UNUSED(m);                        \
    }

#define  __LOG_DEFN_n_(__n__,__type__)                        \
    template < __log_glue(__LOG_TMPL_ARG_,__n__) >                \
    void  __ldndc_log_ ## __type__(                        \
                    cbm::logger_t *  _logger,            \
                    __log_glue(__LOG_DECL_ARG_,__n__))    \
    {                                    \
        if ( !_logger)                            \
        {                                \
            fprintf( stderr, "[BUG]  logging message without logger target");\
            return;                            \
        }                                \
        CRABMEAT_FIX_UNUSED(_delim); /* for __n__==1 */            \
        __log_glue(__LOG_CAT_,__n__)(_logger,_delim) << '\n';        \
        __LOGLOG(__type__,_logger,m.str(),_prefix);            \
    }

#define  __LOG_DEFN_n_rel_(__n__)  __LOG_DEFN_n_(__n__,write) __LOG_DEFN_n_(__n__,info) __LOG_DEFN_n_(__n__,verbose) __LOG_DEFN_n_(__n__,warn) __LOG_DEFN_n_(__n__,error) __LOG_DEFN_n_(__n__,fatal)
#define  __LOG_DEFN_TRAPS_rel_       __LOG_DEFN_TRAP_(write) __LOG_DEFN_TRAP_(info) __LOG_DEFN_TRAP_(verbose) __LOG_DEFN_TRAP_(warn) __LOG_DEFN_TRAP_(error) __LOG_DEFN_TRAP_(fatal)/*;*/
#ifdef  _DEBUG
#  define  __LOG_DEFN_n(__n__) __LOG_DEFN_n_rel_(__n__) __LOG_DEFN_n_(__n__,debug) __LOG_DEFN_n_(__n__,todo)
#  define  __LOG_DEFN_TRAPS    __LOG_DEFN_TRAPS_rel_ __LOG_DEFN_TRAP_(debug) __LOG_DEFN_TRAP_(todo)/*;*/
#else
#  define  __LOG_DEFN_n(__n__) __LOG_DEFN_n_rel_(__n__)
#  define  __LOG_DEFN_TRAPS    __LOG_DEFN_TRAPS_rel_
#endif

/* pull it all together */
#define  LDNDC_LOG_DEFN  __LOG_DEFN_n(1)  __LOG_DEFN_n(2)  __LOG_DEFN_n(3)  __LOG_DEFN_n(4)  __LOG_DEFN_n(5)  __LOG_DEFN_n(6)  __LOG_DEFN_n(7)  __LOG_DEFN_n(8)  __LOG_DEFN_n(9)  __LOG_DEFN_n(10)  __LOG_DEFN_n(11)  __LOG_DEFN_n(12)  __LOG_DEFN_n(13)  __LOG_DEFN_n(14)  __LOG_DEFN_n(15)  __LOG_DEFN_n(16)  __LOG_DEFN_n(17)  __LOG_DEFN_n(18)  __LOG_DEFN_n(19)  __LOG_DEFN_n(20)  __LOG_DEFN_n(21)  __LOG_DEFN_n(22)  __LOG_DEFN_n(23)  __LOG_DEFN_n(24)  __LOG_DEFN_TRAPS


#endif /* !CBM_LOGVARIADIC_H_ */

