
#include  "cgo_shell.h"
#include  <stdlib.h>
#include  <stdio.h>

#ifdef  __cplusplus
extern "C"
#endif
int
null_task(
                struct cgo_task_t *  _task, char **  _msgs, int  _n_msgs)
{
        (void)_task;
        (void)_msgs;
        (void)_n_msgs;
// sk:obs        struct cgo_db_op_scalar_t  op;
// sk:obs        cgo_db_op_scalar_init( &op, _task->db, "null", 0);
// sk:obs
// sk:obs        int  k = 0;
// sk:obs        cgo_op_geti( &op, "null_task_counter", &k);
// sk:obs        k += 1;
// sk:obs        cgo_op_seti( &op, "null_task_counter", k);
// sk:obs
// sk:obs        double  f = 0.0;
// sk:obs        cgo_op_setd( &op, "null_pi", 3.141592653589793238462643383279502);
// sk:obs        cgo_op_getd( &op, "null_pi", &f);
// sk:obs        char  fs[128];
// sk:obs        fs[0] = '\0';
// sk:obs        cgo_op_gets( &op, "null_pi", fs, 127);
// sk:obs
// sk:obs        fprintf( stdout, "k=%5d,  pi=%.21f  pi-string=%s\n", k, f, fs);
// sk:obs
// sk:obs        cgo_db_op_scalar_deinit( &op);


        return  0;
}


int  null_ports_scan(
        char const *  _ports, enum cgo_port_type_e  _port_type, struct ports_static_info_t *  _ports_list, size_t *  _n_ports)
{
    size_t  p = 0;
    char *  ports = cgo_strdup( _ports);
    char *  port = strtok( ports, ":");
    while ( port)
    {
        if ( strlen( port) == 0)
        {
            fprintf( stderr, "empty %s-port[%u] of type [double] \"%s\". ignoring.\n",
                    _port_type==PORT_IN?"in":"out", (unsigned)p, port);

            port = strtok( NULL, ":");
            continue;
        }
        if ( _ports_list)
        {
            fprintf( stderr, "adding %s-port[%u] of type [double] \"%s\"\n",
                _port_type==PORT_IN?"in":"out", (unsigned)p, port);

            _ports_list[p].name = cgo_strdup( port + (( port[0]=='+')?1:0));
            _ports_list[p].type = _port_type;
            _ports_list[p].datatype = CGO_DOUBLE;
        }

        port = strtok( NULL, ":");
        ++p;
    }
    free( ports);
    if ( _n_ports)
    {
        *_n_ports = p;
    }
    return  0;
}
size_t  null_ports_count( char const *  _ports, enum cgo_port_type_e  _port_type)
{
    size_t  n_ports = 0;
    int  rc_count = null_ports_scan( _ports, _port_type, NULL, &n_ports);
    if ( rc_count)
    {
        return  0;
    }
    return  n_ports;
}

struct cgo_ports_t *  null_ports_set_from_command_line(
        struct cgo_task_t *  _task, char const *  _ports_out, char const *  _ports_in)
{
    size_t  n_ports_out = null_ports_count( _ports_out, PORT_OUT);
    size_t  n_ports_in = null_ports_count( _ports_in, PORT_IN);
    size_t  n_ports = n_ports_out + n_ports_in;
    fprintf( stderr, "seeing %u ports  [out=%u,in=%u]\n",
            (unsigned)n_ports, (unsigned)n_ports_out, (unsigned)n_ports_in);
    if ( n_ports == 0)
    {
        return  cgo_ports_new( NULL, 0);
    }

    struct ports_static_info_t *  ports =
        (struct ports_static_info_t *)malloc( n_ports*sizeof(struct ports_static_info_t));
    struct cgo_ports_t *  null_ports = NULL;
    if ( ports)
    {
        int  rc_scan_out = null_ports_scan( _ports_out, PORT_OUT, ports, NULL);
        int  rc_scan_in = null_ports_scan( _ports_in, PORT_IN, ports+n_ports_out, NULL);
        if ( rc_scan_out == 0 && rc_scan_in == 0)
        {
            null_ports = cgo_ports_new( ports, n_ports);
            for ( size_t  p = 0;  p < n_ports;  ++p)
            {
                (void)_task;
// sk:??                char const *  substr = strstr( _ports_in, ports[p].name);
// sk:??                if ( _ports_in < substr && substr-1 == '+')
// sk:??                {
// sk:??                    int  offs = 1;
// sk:??                    cgo_task_set_port_property( _task, ports[p].name, "offset", &offs);
// sk:??                }
                fprintf( stderr, "freeing port info[%u/%u] for \"%s\"\n",
                        (unsigned)p, (unsigned)n_ports, ports[p].name);
                free(( void*)ports[p].name);
            }
            free( ports);
        }
    }
    return  null_ports;
}

int
main(
                int  _argc, char **  _argv)
{
    char const *  task_name = ( _argc < 2) ? "C" : _argv[1];
    char const *  ports_out = ( _argc < 3) ? NULL : _argv[2];
    char const *  ports_in = ( _argc < 4) ? NULL : _argv[3];

    struct cgo_task_t *  task = cgo_task_new( task_name);
    task->execute = null_task;

    task->ports = null_ports_set_from_command_line( task, ports_out, ports_in);

    struct cgo_shell_t *  shell = cgo_shell_new( task);
    if ( shell)
    {
        struct cgo_hostaddress_t *  db_addr = cgo_hostaddress_newset( "localhost", 6379);
        int  rc_connect = cgo_shell_connect( shell, db_addr);
        if ( rc_connect)
        {
            cgo_shell_disconnect( shell);
            cgo_shell_destroy( shell);
            shell = NULL;
        }
        cgo_hostaddress_destroy( db_addr);
    }
    if ( shell)
    {
        int  rc_exe = cgo_shell_execute( shell);
        if ( rc_exe)
        {
            fprintf( stderr, "%s: failed with status %d\n", task_name, rc_exe);
            return  1;
        }
        return  0;
    }
    return  2;
}

