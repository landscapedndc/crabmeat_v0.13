
import  os
import  sys
try :
    sys.path.insert( 0, os.environ['CGO_ROOT']+'/shells/python')
except KeyError :
    print "environment variable CGO_ROOT not set"
    sys.exit( 1)

import  cgo_helpers
import  cgo_shell
import  cgo_db

class  null_task( cgo_shell.cgo_task) :
    def  __init__( self, _name, _db, _geoindex, _ports_in, _ports_out) :
	super( null_task, self).__init__( _name, _db)

        self.geoindex = _geoindex

        self.n_exec = 0
        self._register_ports( _ports_in, _ports_out)

    def  _register_ports( self, _ports_in, _ports_out) :
        self.data_consume = {}
        for port in _ports_in :
            self.data_consume[port] = None
            self.register_port( port, "in", self.update_inport)
        self.data_produce = {}
        for port in _ports_out :
            self.data_produce[port] = None
            self.register_port( port, "out", self.update_outport)

## NOTE  vectors only ;-)
    def  update_inport( self, _port) :
        if _port.dir != "in" :
            print "[EE]  port direction mismatch"

        self.data_consume[_port.name] = self.db.read_vector( _port.name, self.geoindex)
        if not self.data_consume[_port.name] is None :
            print '[ in] \"%s\" %s' % ( _port.name, self.data_consume[_port.name])

    def  update_outport( self, _port) :
        if _port.dir != "out" :
            print "[EE]  port direction mismatch"

#        for port in self.data_produce :
#            self.data_produce[port] = self.db.write_vector( port, self.geoindex, data)
#            print '[out] \"%s\" %s' % ( port, self.data_produce[port])

    def  initialize( self) :
        return  0

    def  execute( self) :
        self.n_exec += 1
        print "call #%d" % ( self.n_exec)
	return  0


if  __name__ == '__main__' :

    task_name = "null" if len( sys.argv) < 2 else sys.argv[1]

    ports_out = [] if len( sys.argv) < 3 else sys.argv[2].split( ':')
    ports_in = [] if len( sys.argv) < 4 else sys.argv[3].split( ':')

    db = cgo_db.cgo_db( task_name)
    db.connect( cgo_helpers.cgo_hostaddress( 'localhost', 6379))

    task = null_task( task_name, db, 0, ports_in, ports_out)
    shell = cgo_shell.cgo_shell( task)
    rc_connect = shell.join()
    if  rc_connect :
	print( '%s: failed to join simulation' % ( task_name))

    rc_exe = shell.execute()
    if  rc_exe :
	print( '%s: failed with status %d' % ( task_name, rc_exe))


