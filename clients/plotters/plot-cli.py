
import  os
import  sys
sys.path.insert( 0, os.environ['CGO_ROOT']+'/shells/python')

import  cgo_helpers
import  cgo_shell
import  cgo_db

from collections import deque
from matplotlib import pyplot as plt

class  plotdata_class :
    def  __init__( self, _max_len) :
        self.ax = deque( [0.0] * _max_len)
        #self.ay = deque( [0.0] * _max_len)
        self.max_len = _max_len

    def  add_to_buffer( self, _buf, _val) :
        if  len( _buf) < self.max_len :
            _buf.append( _val)
        else :
            _buf.pop()
            _buf.appendleft( _val)

    # add data
    def  add( self, _data) :
        #assert( len( _data) == 2)
        self.add_to_buffer( self.ax, _data[0])
        #self.add_to_buffer( self.ay, _data[1])

class  plotter_class :
    def  __init__( self,  _data, _ylim):
        # set plot to animated
        plt.ion() 
        self.axline, = plt.plot( _data.ax)
        #self.ayline, = plt.plot( _data.ay)
        plt.ylim([ _ylim[0], _ylim[1]])

    def update_plot(self,  _data):

        self.axline.set_ydata( _data.ax)
        #self.ayline.set_ydata( _data.ay)
        plt.draw()

class  plot_options( object) :
    def  __init__( self, _args) :
        
        self.key = None
        self.scale = 1.0
        self.ylim_lo = -1.0
        self.ylim_hi = 1.0

        args = _args.split( ':')
        if  len( args) >= 1 :
            key_c = args[0].split( ',')
            if  len( key_c) >= 3 :
                self.key = cgo_db.cgo_db_key( key_c[0], int( key_c[1]), key_c[2])
            if  len( key_c) == 4 :
                self.item = key_c[3]
        if  len( args) >= 2 :
            self.scale = float( args[1])
        if  len( args) >= 3 :
            ylim_c = args[2].split( ',')
            if  len( ylim_c) >= 2 :
                self.ylim_lo = float( ylim_c[0])
                self.ylim_hi = float( ylim_c[1])


class  plot_task( cgo_shell.cgo_task) :
    def  __init__( self, _name, _db, _plot_opts) :
	super( plot_task, self).__init__( _name, _db)

        self.plot_opts = _plot_opts

        self.plotdata = None
        self.plotter = None

        self.x = 0.0

    def  initialize( self) :
        if self.plot_opts is None :
            return  0

        self.plotdata = plotdata_class( 2400)
        self.plotter = plotter_class( self.plotdata, [ self.plot_opts.ylim_lo, self.plot_opts.ylim_hi])
        return  0

    def  execute( self) :
        if  self.plot_opts is None :
            print( 'nothing to do..')
            return  0

        #wc_sl = self.db.sgetvd( cgo_db.cgo_db_key( 'soil', 0, 'soil'), 'watercontent')
        #self.plotdata.add( [ self.x, sum( wc_sl)])

#        if  int( self.x) % 24 == 0 :
        plot_entity_value = self.db.getd( self.plot_opts.key)
        print( '%s=%s' % ( self.plot_opts.key.get_key(), str( plot_entity_value)))
        self.plotdata.add( [ float( self.plot_opts.scale * plot_entity_value), 0.0])

        self.plotter.update_plot( self.plotdata)

        self.x += 1.0

	return  0


if  __name__ == '__main__' :

    task_name = "z-plot"
    plot_opts = None
    if  len( sys.argv) >= 2 :
        plot_opts = plot_options( sys.argv[1])

    db = cgo_db.cgo_db()
    db.connect( cgo_helpers.cgo_hostaddress( 'localhost', 6379))

    task = plot_task( task_name, db, plot_opts)
    shell = cgo_shell.cgo_shell( task)
    rc_connect = shell.connect( cgo_helpers.cgo_hostaddress( 'localhost', 5559), cgo_helpers.cgo_hostaddress( 'localhost', 5560))
    if  rc_connect :
	print( 'failed to connect to server')

    rc_exe = shell.execute()
    if  rc_exe :
	print( '%s: failed with status %d' % ( task_name, rc_exe))

    raw_input( "close plot by pressing enter")

