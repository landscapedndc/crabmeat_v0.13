
import  os
import  sys
try :
    sys.path.insert( 0, os.environ['CGO_ROOT']+'/shells/python')
except KeyError :
    print "environment variable CGO_ROOT not set"
    sys.exit( 1)

import  time
import  cgo_helpers
import  cgo_shell
import  cgo_db

import  cmf
import  ldndc

class  cmf_cell( object) :
    def  __init__( self, _cell, _inputs) :
        self.cell = _cell
        self.inputs = _inputs

        self.time_resolution = _inputs.get_timeresolution()
        self.geoindex = _inputs.id()

        self.ET = None
        self.area = self.cell.area

        self.wc_sl = None
        self.ice_sl = None

    ## wait until input is initialized...
    def  initialize( self, _solutes) :

        soil = self.inputs.soilchemistry
        water = self.inputs.watercycle

        depth = 0.0
        for l, h in enumerate( soil.h_sl) :

            depth += h
            Ks = soil.sks_sl[l] * 24.0 * 60.0 / 100.0

            bc = cmf.VanGenuchtenMualem( Ksat=Ks, phi=soil.poro_sl[l])
            bc.fit_w0()

            self.cell.add_layer( depth, bc)
            self.cell.layers[-1].theta = water.wc_sl[l]
            self.cell.layers[-1].icefraction = water.ice_sl[l] / ( water.ice_sl[l] + water.wc_sl[l])

            #print( '\th=%f,  sks=%f,  poro=%f,  water=%f,  theta=%f' % ( h, soil.sks_sl[l], soil.poro_sl[l], water.wc_sl[l], self.cell.layers[-1].theta))

        self.cell.install_connection( cmf.Richards)
        print( '%s: Added %i layers, soildepth is %g m' % ( self.cell, len( self.cell.layers), depth))
        #self.ET = cmf.ShuttleworthWallace.use_for_cell( self.cell)

        SOLUTES = { 'nitrate':soil.no3_sl, 'ammonium':soil.nh4_sl}
        for s in _solutes :
            solute_array = SOLUTES[s.Name]
            for i, l in enumerate( self.cell.layers) :
                l.Solute( s).state = solute_array[i] * 1.0e3 * self.area
            print '[%3d]%s=%s' % ( self.cell.Id, s.Name, [ l.Solute( s).state for l in self.cell.layers])

    def  update( self) :

        ## TODO  vegetation

        ## ice fraction
        if self.ice_sl is not None and self.wc_sl is not None :
            self.cell.layers.icefraction = self.ice_sl / ( self.ice_sl + self.wc_sl)
        else :
            self.cell.layers.icefraction = numpy.array( [ 0.0] * len( self.cell.layers))

        ## meteorological conditions
        self.update_weather()

    def  write_soilwater( self, _port, _db) :
        n_layers = len( self.cell.layers)
        wc_sl = ( self.cell.layers.theta * ( 1.0 - self.cell.layers.ice_fraction))[:n_layers]
        self.inputs.watercycle.wc_sl = wc_sl
        _db.write_vector( _port.name, self.geoindex, wc_sl.tolist())
    def  read_soilwater( self, _port, _db) :
        wc_sl = _db.read_vector( _port.name, self.geoindex)
        if wc_sl is None :
            self.wc_sl = None
        else :
            self.wc_sl = numpy.array( wc_sl)

    def  write_surfacewater( self, _port, _db) :
        _db.write_vector( _port.name, self.geoindex, [self.cell.surfacewater.volume / self.area])
    def  read_surfacewater( self, _port, _db) :
        pass
## FIXME        #self.cell.surfacewater.volume = _db.read_vector( _port.name, self.geoindex)[0] * self.area

    def  write_soilice( self, _port, _db) :
        n_layers = len( self.cell.layers)
        ice_sl = ( self.cell.layers.theta * self.cell.layers.ice_fraction)[:n_layers]
        _db.write_vector( _port.name, self.geoindex, ice_sl.tolist())
    def  read_soilice( self, _port, _db) :
        ice_sl = _db.read_vector( _port.name, self.geoindex)
        if ice_sl is None :
            self.ice_sl = None
        else :
            self.ice_sl = numpy.array( ice_sl)

    def  write_surfaceice( self, _port, _db) :
        surface_ice = 0.0
        if  self.cell.snow :
            surface_ice = self.cell.snow.volume / self.area
        _db.write_vector( _port.name, self.geoindex, [surface_ice])
    def  read_surfaceice( self, _port, _db) :
        pass
## FIXME        if  self.cell.snow :
## FIXME            self.cell.snow.volume = _db.read_vector( _port.name, self.geoindex)[0] * self.area

#    def  write_surfaceflux( self, _port, _db) :
#        _db.write_vector( _port.name, self.geoindex, [self.cell.surfacewater( self.T) * self.time_resolution / self.area])

    def  write_solute( self, _port, _db, _solute) :
        n_layers = len( self.cell.layers)
        _db.write_vector( _port.name, self.geoindex, [ l.Solute( _solute).state/( 1.0e+3 * self.area) for l in self.cell.layers[0:n_layers]])
    def  read_solute( self, _port, _db, _solute) :
        sol = _db.read_vector( _port.name, self.geoindex)
        if sol != [] :
            for i, l in enumerate( self.cell.layers) :
                l.Solute( _solute).state = sol[i] * 1.0e+3 * self.cell.area

    def  update_weather( self) :

        climate = self.inputs.microclimate

        #precip = climate.ts_precipitation * 1000.0 * self.time_resolution
        precip = climate.nd_precipitation * 1000.0 #* self.time_resolution

        temp_avg = climate.ts_airtemperature
        temp_min = climate.ts_minimumairtemperature
        temp_max = climate.ts_maximumairtemperature
        vpd = climate.ts_watervaporsaturationdeficit
        windspeed = climate.ts_windspeed
        radiation = climate.ts_shortwaveradiance * ( 24 * 60 * 60 * 1.0e-6)
        sunshine = 0.5

        w = cmf.Weather( temp_avg, temp_max, temp_min, cmf.rH_from_vpd( temp_avg, vpd), windspeed, sunshine, radiation)
        self.cell.set_weather( w)
        self.cell.set_rainfall( precip)

#        if self.cell.Id == 0 :
#            print( 't_avg=%f, t_max=%f, t_min=%f, rh=%f, precip=%f(%.6f/%.6f)' % ( temp_avg, temp_max, temp_min, vpd, precip, climate.nd_precipitation, climate.ts_precipitation))

class  cmf_hydro( object) :
    def  __init__( self, _solutes, _project) :

        self.T = None
        self.solver = None

        self.timestep = 0
        self.t_0 = time.time() ## assuming we don't chill to long

        self.hydro = cmf.project( _solutes)
        self.biogeochem = ldndc.landscape( _project['project'], _project['configuration'])
        self.hcells = []

        self.dT = cmf.h
        print  self.dT, type( cmf.h)
        self.time_resolution = self.biogeochem.get_timeresolution()

    def  find_hydrocell_by_id( self, _id) :
        for hcell in self.hydro :
            if hcell.Id == _id :
                return  hcell
        return  None

    def  initialize_domain( self) :

        for k in xrange( self.biogeochem.kernelcount()) :
            bcell = self.biogeochem.get_kernel( k)
            print "kernel", k, "object id",bcell.id()
            hcell = self.hydro.NewCell( bcell.setup.x(), bcell.setup.y(), bcell.setup.z(), bcell.setup.area(), True)
            hcell.Id = bcell.id()
            self.hcells.append( cmf_cell( hcell, bcell))

        for hcell in self.hcells :
            hcell.initialize( self.hydro.solutes)

    def  initialize_neighborhood( self) :
        ## set neighbors
        for bcell in self.biogeochem :
            neighbors = bcell.get_neighbors()
            print neighbors
            hcell = None
            if neighbors :
                hcell = self.find_hydrocell_by_id( bcell.id())
            if hcell :
                for n in neighbors :
                    nhcell = self.find_hydrocell_by_id( n)
                    if nhcell :
                        print  "%d:\tadding neighbor %d  [w=%f]" % ( hcell.Id, n, bcell.get_neighbor_intersect( n))
                        hcell.topology.AddNeighbor( nhcell, bcell.get_neighbor_intersect( n))
                    else :
                        print  "%d:\tmissing neighbor %d" % ( hcell.Id, n)
                        return  -1

        print( '%s: size is %i' % ( self.hydro, len( self.hydro)))
        return  0

    def  initialize_outlets( self) :
        lowest_cell = min( self.hydro, key= lambda c: c.z)
        outlet = self.hydro.NewOutlet( name="Outlet", x=lowest_cell.x, y=lowest_cell.y-1.0, z=lowest_cell.z-3.0)
        outlet.potential = lowest_cell.z-3.0
        cmf.Richards( lowest_cell.layers[-1], outlet)
        #lowest_cell.connect_soil_with_node( outlet, cmf.Richards_lateral, 10.0, 5.0)

    def  initialize_solver( self) :

        self.solver = cmf.SoluteWaterIntegrator( self.hydro.solutes, cmf.CVodeIntegrator( 1.0e-08), cmf.ImplicitEuler( 1.0e-08), self.hydro)
        self.solver.t = self.biogeochem.get_modeltime()

        self.T = cmf.AsCMFtime( self.biogeochem.get_modeltime())
        self.dT = cmf.day / self.biogeochem.get_timeresolution()

        print( '%s: solver %s' % ( self.hydro, self.solver))


    def  initialize( self) :

        self.initialize_domain()
        if  len( self.hcells) > 1 :
            rc_neighborhood = self.initialize_neighborhood()
            if  rc_neighborhood != 0 :
                return  -1
        self.initialize_outlets()

        ## subsurface flow
        cmf.connect_cells_with_flux( self.hydro, cmf.Richards_lateral)
        print  'subsurface flow installed ...'
        ## surface runoff
        cmf.connect_cells_with_flux( self.hydro, cmf.KinematicSurfaceRunoff)
        print  'surface runoff installed ...'

        self.initialize_solver()
        print  'initialized solute/water solver ...'

        return  0

    def  execute( self) :

#	print( 'executing %s...' % ( self.name))

        ## update inputs
        self.biogeochem.launch( 1)

        for hcell in self.hcells :
            hcell.update()

        t_s = time.time()
        self.solver.reset()
        try :
            self.solver.integrate_until( cmf.AsCMFtime( self.biogeochem.get_modeltime()))
            #self.solver.t = self.biogeochem.get_modeltime()
        except RuntimeError, runtime_error :
            print 'runtime error: %s' % ( runtime_error)
            return  -1
        t_e = time.time()

        self.timestep += 1

        #print 'theta=', self.hydro[14].layers.theta

	print( '\n[%5d{%s}] T(solve)=%.6f;  T(cycle)=%.6f' % ( self.timestep, str( self.solver.t), t_e-t_s, t_e-self.t_0))
        self.t_0 = t_e

	return  0


    def  update_shared( self, _port, _rw, _db) :
        for hcell in self.hcells :
            cell_fun = getattr( hcell, '%s_%s' % ( _rw, _port.name))
            cell_fun( _port, _db)

    def  write_solute( self, _port, _db, _solute) :
        #t0 = time.time()
        for cell in self.hcells :
            cell.write_solute( _port, _db, _solute)
        #print 'T(send-%s)=%.6f' % ( _port.name, time.time()-t0)
    def  read_solute( self, _port, _db, _solute) :
        #t0 = time.time()
        for cell in self.hcells :
            cell.read_solute( _port, _db, _solute)
        #print 'T(recv-%s)=%.6f' % ( _port.name, time.time()-t0)


class  cmf_task( cgo_shell.cgo_task) :
    def  __init__( self, _name, _db, _solutes=['nitrate','ammonium'], _input={ 'project':None, 'configuration':'~/.ldndc/ldndc.conf'}) :
	super( cmf_task, self).__init__( _name, _db)

        self.model = cmf_hydro( _solutes, _input)

        self._register_ports()

    def  __repr__( self) :
        return  "<cmf-task %d cells>" % ( self.model.hydro.size())

    def  _register_ports( self) :
        ## out ports
        self.register_port( "soilwater", "out", self._write_shared)
        self.register_port( "surfacewater", "out", self._write_shared)
        self.register_port( "soilice", "out", self._write_shared)
        self.register_port( "surfaceice", "out", self._write_shared)

        ## in ports
        self.register_port( "+soilwater", "in", self._read_shared)
        self.register_port( "+soilice", "in", self._write_shared)

        for solute in self.model.hydro.solutes :
            self.register_port( '+%s' % solute.Name, "in", self._read_solute)
            self.register_port( '%s' % solute.Name, "out", self._write_solute)

    def  _read_shared( self, _port) :
        self.model.update_shared( _port, 'read', self.db)
    def  _write_shared( self, _port) :
        self.model.update_shared( _port, 'write', self.db)

    def  _find_solute_by_name( self, _solute_name) :
        for solute in self.model.hydro.solutes :
            if _solute_name == solute.Name :
                return  solute
        return  None

    def  _write_solute( self, _port) :
        solute = self._find_solute_by_name( _port.name)
        if solute :
            self.model.write_solute( _port, self.db, solute)
    def  _read_solute( self, _port) :
        solute = self._find_solute_by_name( _port.name)
        if solute :
            self.model.read_solute( _port, self.db, solute)


    def  initialize( self) :
        return  self.model.initialize()

    def  execute( self) :
        return  self.model.execute()

if  __name__ == '__main__' :

    client_name = "cmf"

    db = cgo_db.cgo_db( client_name)
    db.connect( cgo_helpers.cgo_hostaddress( 'localhost', 6379))

    ## ldndc project file
    setup = None if len( sys.argv) < 2 else sys.argv[1]
    config = '%s/.ldndc/ldndc.conf' % ( os.environ['HOME']) if len( sys.argv) < 3 else sys.argv[2]

    task = cmf_task( client_name, db, 'nitrate ammonium', {'project':setup, 'configuration':config})
    shell = cgo_shell.cgo_shell( task)
    rc_connect = shell.join()
    if  rc_connect :
	print( 'failed to connect to server')

    rc_exe = shell.execute()
    if  rc_exe :
	print( 'failed with status %d' % ( rc_exe))


