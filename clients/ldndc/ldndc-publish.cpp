
#include  "ldndc/ldndc-publish.h"
#include  "kernels/mobile/mobile.h"
#include  "lkernel/io-kcomm.h"
#include  "string/lstring-basic.h"
#include  "time/ldate.h"

#include  "cgo_db.h"

#ifdef  __cplusplus
extern "C"
{
#endif
//static int  ldndc_write_air_temperature( struct cgo_port_t * /*port*/, void *  /*db*/)
//{
//    return 0;
//}
//static int  ldndc_write_number_of_soil_layer( struct cgo_port_t * /*port*/, void *  /*db*/)
//{
//    return 0;
//}
//static int  ldndc_write_soil_layer_height( struct cgo_port_t * /*port*/, void *  /*db*/)
//{
//    return 0;
//}

static int  ldndc_readwrite_soilwater( struct cgo_port_t *  _port, void *  _db)
{
	int  rc = 0;
	landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_port->data)->ldndc;
	if ( !ldndc)
	{
		return  -1;
	}
	for ( size_t  k = 0;  k < ldndc->size();  ++k)
	{
		ldndc::kernels::mobile::lkernel_mobile_t *  K =
			(ldndc::kernels::mobile::lkernel_mobile_t *)ldndc->kernel_at( k);
		if ( !K)
		{
			continue;
		}
		ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
		if ( !K_state)
		{
			continue;
		}

        ldndc::kernels::mobile::substate_watercycle_t *  water =
            K_state->get_substate< ldndc::kernels::mobile::substate_watercycle_t >();


        struct cgo_geospatial_key_t  geospatial_key;
        geospatial_key.portname = _port->name;
        geospatial_key.geoindex = K->object_id(); //K->geo_position().geohash_fp52();

		int  len = water->wc_sl.size();
		if ( _port->dir == PORT_IN)
		{
			rc = cgo_db_read_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&water->wc_sl[0], &len, CGO_DOUBLE);
		}
		else if ( _port->dir == PORT_OUT)
		{
			rc = cgo_db_write_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&water->wc_sl[0], &len, CGO_DOUBLE);
		}
		if ( rc)
		{
			break;
		}
	}
	return  rc;
}
static int  ldndc_read_surfacewater( struct cgo_port_t *  _port, void *  _db)
{
    int  rc = 0;
    landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_port->data)->ldndc;
    if ( !ldndc || ( _port->dir != PORT_IN))
    {
        return  -1;
    }
    for ( size_t  k = 0;  k < ldndc->size();  ++k)
    {
        ldndc::kernels::mobile::lkernel_mobile_t *  K =
            (ldndc::kernels::mobile::lkernel_mobile_t *)ldndc->kernel_at( k);
        if ( !K)
        {
            continue;
        }
        ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
        if ( !K_state)
        {
            continue;
        }

        ldndc::kernels::mobile::substate_watercycle_t *  water =
            K_state->get_substate< ldndc::kernels::mobile::substate_watercycle_t >();


        struct cgo_geospatial_key_t  geospatial_key;
        geospatial_key.portname = _port->name;
        geospatial_key.geoindex = K->object_id(); //K->geo_position().geohash_fp52();

		//fprintf( stdout, "geohash \"%s\"\n", K->geo_position().geohash());
		int  len = 1;
		if ( _port->dir == PORT_IN)
		{
			rc = cgo_db_read_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&water->surface_water, &len, CGO_DOUBLE);
		}
		if ( rc)
		{
			break;
		}
	}
	return  rc;
}
static int  ldndc_readwrite_soilice( struct cgo_port_t *  _port, void *  _db)
{
	int  rc = 0;
	landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_port->data)->ldndc;
	if ( !ldndc)
	{
		return  -1;
	}
	for ( size_t  k = 0;  k < ldndc->size();  ++k)
	{
		ldndc::kernels::mobile::lkernel_mobile_t *  K =
			(ldndc::kernels::mobile::lkernel_mobile_t *)ldndc->kernel_at( k);
		if ( !K)
		{
			continue;
		}
		ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
		if ( !K_state)
		{
			continue;
		}

        ldndc::kernels::mobile::substate_watercycle_t *  water =
            K_state->get_substate< ldndc::kernels::mobile::substate_watercycle_t >();


        struct cgo_geospatial_key_t  geospatial_key;
        geospatial_key.portname = _port->name;
        geospatial_key.geoindex = K->object_id(); //K->geo_position().geohash_fp52();

		int  len = water->ice_sl.size();
		if ( _port->dir == PORT_IN)
		{
			rc = cgo_db_read_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&water->ice_sl[0], &len, CGO_DOUBLE);
		}
		else if ( _port->dir == PORT_OUT)
		{
			rc = cgo_db_write_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&water->ice_sl[0], &len, CGO_DOUBLE);
		}
		if ( rc)
		{
			break;
		}
	}
	return  rc;
}
static int  ldndc_read_surfaceice( struct cgo_port_t *  _port, void *  _db)
{
    int  rc = 0;
    landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_port->data)->ldndc;
    if ( !ldndc || ( _port->dir != PORT_IN))
    {
        return  -1;
    }
    for ( size_t  k = 0;  k < ldndc->size();  ++k)
    {
        ldndc::kernels::mobile::lkernel_mobile_t *  K =
            (ldndc::kernels::mobile::lkernel_mobile_t *)ldndc->kernel_at( k);
        if ( !K)
        {
            continue;
        }
        ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
        if ( !K_state)
        {
            continue;
        }

        ldndc::kernels::mobile::substate_watercycle_t *  water =
            K_state->get_substate< ldndc::kernels::mobile::substate_watercycle_t >();


        struct cgo_geospatial_key_t  geospatial_key;
        geospatial_key.portname = _port->name;
        geospatial_key.geoindex = K->object_id(); //K->geo_position().geohash_fp52();

		//fprintf( stdout, "geohash \"%s\"\n", K->geo_position().geohash());
		int  len = 1;
		if ( _port->dir == PORT_IN)
		{
			rc = cgo_db_read_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&water->surface_ice, &len, CGO_DOUBLE);
		}
		if ( rc)
		{
			break;
		}
	}
	return  rc;
}
static int  ldndc_readwrite_ammonium( struct cgo_port_t *  _port, void *  _db)
{
    int  rc = 0;
    landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_port->data)->ldndc;
    if ( !ldndc || ( _port->dir != PORT_OUT && _port->dir != PORT_IN))
    {
        return  -1;
    }
    for ( size_t  k = 0;  k < ldndc->size();  ++k)
    {
        ldndc::kernels::mobile::lkernel_mobile_t *  K =
            (ldndc::kernels::mobile::lkernel_mobile_t *)ldndc->kernel_at( k);
        if ( !K)
        {
            continue;
        }
        ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
        if ( !K_state)
        {
            continue;
        }

        ldndc::kernels::mobile::substate_soilchemistry_t *  soilchem =
            K_state->get_substate< ldndc::kernels::mobile::substate_soilchemistry_t >();


        struct cgo_geospatial_key_t  geospatial_key;
        geospatial_key.portname = _port->name;
        geospatial_key.geoindex = K->object_id(); //K->geo_position().geohash_fp52();

		//fprintf( stdout, "geohash \"%s\"\n", K->geo_position().geohash());
		int  len = soilchem->nh4_sl.size();
		if ( _port->dir == PORT_IN)
		{
			rc = cgo_db_read_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&soilchem->nh4_sl[0], &len, CGO_DOUBLE);
		}
		else if ( _port->dir == PORT_OUT)
		{
			rc = cgo_db_write_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&soilchem->nh4_sl[0], &len, CGO_DOUBLE);
		}
		if ( rc)
		{
			break;
		}
	}
	return  rc;
}
static int  ldndc_readwrite_nitrate( struct cgo_port_t *  _port, void *  _db)
{
    int  rc = 0;
    landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_port->data)->ldndc;
    if ( !ldndc || ( _port->dir != PORT_OUT && _port->dir != PORT_IN))
    {
        return  -1;
    }
    for ( size_t  k = 0;  k < ldndc->size();  ++k)
    {
        ldndc::kernels::mobile::lkernel_mobile_t *  K =
            (ldndc::kernels::mobile::lkernel_mobile_t *)ldndc->kernel_at( k);
        if ( !K)
        {
            continue;
        }
        ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
        if ( !K_state)
        {
            continue;
        }

        ldndc::kernels::mobile::substate_soilchemistry_t *  soilchem =
            K_state->get_substate< ldndc::kernels::mobile::substate_soilchemistry_t >();


        struct cgo_geospatial_key_t  geospatial_key;
        geospatial_key.portname = _port->name;
        geospatial_key.geoindex = K->object_id(); //K->geo_position().geohash_fp52();

		//fprintf( stdout, "geohash \"%s\"\n", K->geo_position().geohash());
		int  len = soilchem->no3_sl.size();
		if ( _port->dir == PORT_IN)
		{
			rc = cgo_db_read_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&soilchem->no3_sl[0], &len, CGO_DOUBLE);
		}
		else if ( _port->dir == PORT_OUT)
		{
			rc = cgo_db_write_vector( (struct cgo_db_t *)_db, &geospatial_key,
					&soilchem->no3_sl[0], &len, CGO_DOUBLE);
		}
		if ( rc)
		{
			break;
		}
	}
	return  rc;
}
#ifdef  __cplusplus
}
#endif
int  ldndc_set_ports(
        struct cgo_task_t *  _task)
{
	cgo_task_register_port( _task, "soilwater", ldndc_readwrite_soilwater);
	cgo_task_register_port( _task, "surfacewater", ldndc_read_surfacewater);
	cgo_task_register_port( _task, "soilice", ldndc_readwrite_soilice);
	cgo_task_register_port( _task, "surfaceice", ldndc_read_surfaceice);
	cgo_task_register_port( _task, "nitrate", ldndc_readwrite_nitrate);
	int  nitrate_offset = 1;
	cgo_task_set_port_property( _task, "nitrate", "offset", &nitrate_offset);
	cgo_task_register_port( _task, "ammonium", ldndc_readwrite_ammonium);
	int  ammonium_offset = 1;
	cgo_task_set_port_property( _task, "ammonium", "offset", &ammonium_offset);

//	cgo_task_register_port( _task, "canopy_height", &ldndc_write_canopy_height);
//	cgo_task_register_port( _task, "leaf_area_index", &ldndc_write_lai);
//	cgo_task_register_port( _task, "canopy_closure", &ldndc_write_canopy_closure);
//	cgo_task_register_port( _task, "root_fraction", &ldndc_write_root_fraction);

	return  0;
}

// sk:obs int  ldndc_publish_initial(
// sk:obs                 ldndc::llandscapedndc_t *  _ldndc, cgo_db_t *  _db)
// sk:obs {
// sk:obs         struct cgo_db_op_scalar_t  opschedule;
// sk:obs         cgo_db_op_scalar_init( &opschedule, _db, "schedule", 0);
// sk:obs         std::string  schedule = _ldndc->schedule.to_string();
// sk:obs         cgo_op_sets( &opschedule, "time", schedule.c_str(), cbm::strlen( schedule.c_str()));
// sk:obs         cgo_db_op_scalar_deinit( &opschedule);
// sk:obs 
// sk:obs         int *  domain = CBM_DefaultAllocator->allocate_init_type< int >( _ldndc->size(), -1);
// sk:obs         if ( !domain)
// sk:obs         {
// sk:obs                 return  -1;
// sk:obs         }
// sk:obs 
// sk:obs         size_t  domain_size = 0;
// sk:obs         for ( size_t  k = 0;  k < _ldndc->size();  ++k)
// sk:obs         {
// sk:obs                 ldndc::kernels::mobile::lkernel_mobile_t *  K =
// sk:obs                         (ldndc::kernels::mobile::lkernel_mobile_t *)_ldndc->kernel_at( k);
// sk:obs                 if ( !K)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::kernels::mobile::public_state_mobile_t const *  K_state = K->public_state();
// sk:obs                 if ( !K_state)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::io_kcomm_t *  io_kcomm = K->io();
// sk:obs                 if ( !io_kcomm)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs 
// sk:obs                 /* store kernel ID */
// sk:obs                 domain[domain_size] = K->object_id();
// sk:obs                 domain_size += 1;
// sk:obs 
// sk:obs                 ldndc::kernels::mobile::substate_soilchemistry_t const *  soilchem =
// sk:obs                         K_state->get_substate< ldndc::kernels::mobile::substate_soilchemistry_t >();
// sk:obs                 ldndc::kernels::mobile::substate_watercycle_t const *  watercyc =
// sk:obs                         K_state->get_substate< ldndc::kernels::mobile::substate_watercycle_t >();
// sk:obs 
// sk:obs 
// sk:obs                 size_t const  n_soil_layers = soilchem->h_sl.size();
// sk:obs 
// sk:obs                 struct cgo_db_op_struct_t  opsoil_v;
// sk:obs                 cgo_db_op_struct_init( &opsoil_v, _db, "soil", K->object_id(), "soil", n_soil_layers);
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "height", &soilchem->h_sl[0], soilchem->h_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "porosity", &soilchem->poro_sl[0], soilchem->poro_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "sks", &soilchem->sks_sl[0], soilchem->sks_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "watercontent", &watercyc->wc_sl[0], watercyc->wc_sl.size());
// sk:obs                 cgo_db_op_struct_deinit( &opsoil_v);
// sk:obs 
// sk:obs                 struct cgo_db_op_scalar_t  opsoil;
// sk:obs                 cgo_db_op_scalar_init( &opsoil, _db, "soil", K->object_id());
// sk:obs                 cgo_op_seti( &opsoil, "number_of_layers", n_soil_layers);
// sk:obs                 cgo_db_op_scalar_deinit( &opsoil);
// sk:obs 
// sk:obs                 struct cgo_db_op_scalar_t  opwater;
// sk:obs                 cgo_db_op_scalar_init( &opwater, _db, "water", K->object_id());
// sk:obs                 cgo_op_setd( &opwater, "transpiration", watercyc->transp);
// sk:obs                 cgo_op_setd( &opwater, "evaporation:cep", watercyc->evacep);
// sk:obs                 cgo_op_setd( &opwater, "evaporation:soil", watercyc->evasoil);
// sk:obs                 cgo_op_setd( &opwater, "evapotranspiration", watercyc->evapot);
// sk:obs                 cgo_db_op_scalar_deinit( &opwater);
// sk:obs 
// sk:obs 
// sk:obs                 struct cgo_db_op_scalar_t  optopo;
// sk:obs                 cgo_db_op_scalar_init( &optopo, _db, "topology", K->object_id());
// sk:obs 
// sk:obs                 ldndc::setup::input_class_setup_t const *  setup =
// sk:obs                         io_kcomm->get_input_class< ldndc::setup::input_class_setup_t >();
// sk:obs 
// sk:obs                 cgo_op_setd( &optopo, "longitude", setup->longitude());
// sk:obs                 cgo_op_setd( &optopo, "latitude", setup->latitude());
// sk:obs                 cgo_op_setd( &optopo, "elevation", setup->elevation());
// sk:obs                 cgo_op_setd( &optopo, "area", setup->area());
// sk:obs 
// sk:obs                 cgo_db_op_scalar_deinit( &optopo);
// sk:obs 
// sk:obs                 /* TODO  neighbors */
// sk:obs 
// sk:obs 
// sk:obs         }
// sk:obs 
// sk:obs         struct cgo_db_op_struct_t  opdomain;
// sk:obs         cgo_db_op_struct_init( &opdomain, _db, "domain", 0, "cells", domain_size);
// sk:obs         cgo_op_ssetvi( &opdomain, "id", domain, domain_size);
// sk:obs         cgo_db_op_struct_deinit( &opdomain);
// sk:obs 
// sk:obs         CBM_DefaultAllocator->deallocate( domain);
// sk:obs 
// sk:obs         return  0;
// sk:obs }
// sk:obs int  ldndc_publish_meteo(
// sk:obs                 ldndc::llandscapedndc_t *  _ldndc, cgo_db_t *  _db)
// sk:obs {
// sk:obs         for ( size_t  k = 0;  k < _ldndc->size();  ++k)
// sk:obs         {
// sk:obs                 ldndc::kernels::mobile::lkernel_mobile_t *  K =
// sk:obs                         (ldndc::kernels::mobile::lkernel_mobile_t *)_ldndc->kernel_at( k);
// sk:obs                 if ( !K)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::io_kcomm_t *  io_kcomm = K->io();
// sk:obs                 if ( !io_kcomm)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs 
// sk:obs                 lclock_t const &  clk = *_ldndc->lclock();
// sk:obs 
// sk:obs                 ldndc::climate::input_class_climate_t const *  climate =
// sk:obs                         io_kcomm->get_input_class< ldndc::climate::input_class_climate_t >();
// sk:obs 
// sk:obs                 struct cgo_db_op_scalar_t  opmeteo;
// sk:obs                 cgo_db_op_scalar_init( &opmeteo, _db, "climate", K->object_id());
// sk:obs                 cgo_op_setd( &opmeteo, "air_temperature", climate->temp_avg_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "minimum_air_temperature", climate->temp_min_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "maximum_air_temperature", climate->temp_max_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "precipitation", climate->precip_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "relative_humidity", climate->rel_humidity_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "windspeed", climate->wind_speed_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "radiation", climate->ex_rad_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "air_pressure", climate->air_pressure_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "vpd", climate->vpd_subday( clk));
// sk:obs                 cgo_op_setd( &opmeteo, "longwave_radiation", climate->long_rad_subday( clk));
// sk:obs                 cgo_db_op_scalar_deinit( &opmeteo);
// sk:obs         }
// sk:obs         return  0;
// sk:obs }
// sk:obs #include  "utils/lutils-env.h"
// sk:obs int  ldndc_publish_phys(
// sk:obs                 ldndc::llandscapedndc_t *  _ldndc, cgo_db_t *  _db)
// sk:obs {
// sk:obs         for ( size_t  k = 0;  k < _ldndc->size();  ++k)
// sk:obs         {
// sk:obs                 ldndc::kernels::mobile::lkernel_mobile_t *  K =
// sk:obs                         (ldndc::kernels::mobile::lkernel_mobile_t *)_ldndc->kernel_at( k);
// sk:obs                 if ( !K)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::kernels::mobile::public_state_mobile_t const *  K_state = K->public_state();
// sk:obs                 if ( !K_state)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::io_kcomm_t *  io_kcomm = K->io();
// sk:obs                 if ( !io_kcomm)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs 
// sk:obs                 ldndc::species::input_class_species_t const *  c_species =
// sk:obs                         io_kcomm->get_input_class< ldndc::species::input_class_species_t >();
// sk:obs                 ldndc::kernels::mobile::substate_physiology_t const *  phys = NULL;
// sk:obs                 if ( c_species->species_cnt() > 0)
// sk:obs                 {
// sk:obs                         phys = K_state->get_substate< ldndc::kernels::mobile::substate_physiology_t >();
// sk:obs                 }
// sk:obs 
// sk:obs //                if ( _ldndc->lclock()->is_position( TMODE_PRE_DAILY))
// sk:obs                 {
// sk:obs                         struct cgo_db_op_scalar_t  opphys;
// sk:obs             cgo_db_op_scalar_init( &opphys, _db, "phys", K->object_id());
// sk:obs //                        CBM_LogDebug( "t=", _ldndc->lclock()->to_string(),"  mass(foliage)=", (phys ? phys->mFol_vt[0] : 0.0));
// sk:obs                         cgo_op_setd( &opphys, "foliage_mass", phys ? phys->mFol_vt[0] : 0.0);
// sk:obs                         cgo_db_op_scalar_deinit( &opphys);
// sk:obs                 }
// sk:obs         }
// sk:obs         return  0;
// sk:obs }
// sk:obs int  ldndc_publish_soil(
// sk:obs                 ldndc::llandscapedndc_t *  _ldndc, cgo_db_t *  _db)
// sk:obs {
// sk:obs         for ( size_t  k = 0;  k < _ldndc->size();  ++k)
// sk:obs         {
// sk:obs                 ldndc::kernels::mobile::lkernel_mobile_t *  K =
// sk:obs                         (ldndc::kernels::mobile::lkernel_mobile_t *)_ldndc->kernel_at( k);
// sk:obs                 if ( !K)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::kernels::mobile::public_state_mobile_t const *  K_state = K->public_state();
// sk:obs                 if ( !K_state)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs 
// sk:obs                 ldndc::kernels::mobile::substate_soilchemistry_t const *  soilchem =
// sk:obs                         K_state->get_substate< ldndc::kernels::mobile::substate_soilchemistry_t >();
// sk:obs 
// sk:obs //                if ( _ldndc->lclock()->is_position( TMODE_PRE_DAILY))
// sk:obs                 {
// sk:obs                         struct cgo_db_op_scalar_t  opsoil;
// sk:obs                         cgo_db_op_scalar_init( &opsoil, _db, "soil", K->object_id());
// sk:obs //                        CBM_LogDebug( "t=", _ldndc->lclock()->to_string(),"  co2(hetero)=",soilchem->nd_co2_hetero);
// sk:obs                         cgo_op_setd( &opsoil, "co2_hetero", soilchem->ts_co2_hetero_sl.sum());
// sk:obs                         cgo_op_setd( &opsoil, "n2o", soilchem->n2o_sl.sum());
// sk:obs                         cgo_db_op_scalar_deinit( &opsoil);
// sk:obs                 }
// sk:obs 
// sk:obs                 size_t const  n_soil_layers = soilchem->h_sl.size();
// sk:obs 
// sk:obs                 struct cgo_db_op_struct_t  opsoil_v;
// sk:obs                 cgo_db_op_struct_init( &opsoil_v, _db, "soil", K->object_id(), "soil", n_soil_layers);
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "/nh4/", &soilchem->nh4_sl[0], soilchem->nh4_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "/doc/", &soilchem->doc_sl[0], soilchem->doc_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "/no3/", &soilchem->no3_sl[0], soilchem->no3_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "/no2/", &soilchem->no2_sl[0], soilchem->no2_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "/no/", &soilchem->no_sl[0], soilchem->no_sl.size());
// sk:obs                 cgo_op_ssetvd( &opsoil_v, "/n2o/", &soilchem->n2o_sl[0], soilchem->n2o_sl.size());
// sk:obs                 cgo_db_op_struct_deinit( &opsoil_v);
// sk:obs 
// sk:obs         }
// sk:obs         return  0;
// sk:obs }
// sk:obs int  ldndc_read_water(
// sk:obs                 ldndc::llandscapedndc_t *  _ldndc, cgo_db_t *  _db)
// sk:obs {
// sk:obs         for ( size_t  k = 0;  k < _ldndc->size();  ++k)
// sk:obs         {
// sk:obs                 ldndc::kernels::mobile::lkernel_mobile_t *  K =
// sk:obs                         (ldndc::kernels::mobile::lkernel_mobile_t *)_ldndc->kernel_at( k);
// sk:obs                 if ( !K)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs                 ldndc::kernels::mobile::public_state_mobile_t *  K_state = K->public_state();
// sk:obs                 if ( !K_state)
// sk:obs                 {
// sk:obs                         continue;
// sk:obs                 }
// sk:obs 
// sk:obs                 ldndc::kernels::mobile::substate_watercycle_t *  watercyc =
// sk:obs                         K_state->get_substate< ldndc::kernels::mobile::substate_watercycle_t >();
// sk:obs 
// sk:obs                 struct cgo_db_op_struct_t  opwater_v;
// sk:obs                 cgo_db_op_struct_init( &opwater_v, _db, "soil", K->object_id(), "soil", watercyc->wc_sl.size());
// sk:obs                 cgo_op_sgetvd( &opwater_v, "watercontent", &watercyc->wc_sl[0], watercyc->wc_sl.size());
// sk:obs                 cgo_op_sgetvd( &opwater_v, "ice", &watercyc->ice_sl[0], watercyc->ice_sl.size());
// sk:obs                 cgo_db_op_struct_deinit( &opwater_v);
// sk:obs 
// sk:obs //                CBM_LogDebug( "wc=",watercyc->wc_sl);
// sk:obs         }
// sk:obs         return  0;
// sk:obs }

