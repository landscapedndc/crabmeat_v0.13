
#ifndef  CGO_CLIENT_LDNDC_PUBLISH_H_
#define  CGO_CLIENT_LDNDC_PUBLISH_H_

#include  "landscapedndc.h"
#ifdef  __cplusplus
extern "C"
{
#endif

typedef  ldndc::llandscapedndc_t  landscapedndc_t;
struct  llandscapedndc_shell_t
{
    landscapedndc_t *  ldndc;
};
#ifdef  __cplusplus
}
#endif
#include  "cgo_shell.h"

struct cgo_db_t;

struct ports_static_info_t const  PORTS_STATIC_INFO[] =
{
//    { "air_temperature", PORT_OUT, CGO_DOUBLE},
//    { "minimum_air_temperature", PORT_OUT, CGO_DOUBLE},
//    { "maximum_air_temperature", PORT_OUT, CGO_DOUBLE},
//    { "precipitation", PORT_OUT, CGO_DOUBLE},
//    { "relative_humidity", PORT_OUT, CGO_DOUBLE},
//    { "windspeed", PORT_OUT, CGO_DOUBLE},
//    { "radiation", PORT_OUT, CGO_DOUBLE},
//    { "air_pressure", PORT_OUT, CGO_DOUBLE},
//    { "vapor_pressure_deficit", PORT_OUT, CGO_DOUBLE},
//    { "longwave_radiation", PORT_OUT, CGO_DOUBLE},

//    { "latitude", PORT_OUT, CGO_DOUBLE},
//    { "longitude", PORT_OUT, CGO_DOUBLE},
//    { "elevation", PORT_OUT, CGO_DOUBLE},
//    { "area", PORT_OUT, CGO_DOUBLE},

//    { "transpiration", PORT_IN, CGO_DOUBLE},
//    { "evapotranspiration", PORT_IN, CGO_DOUBLE},
//    { "evaporation", PORT_IN CGO_DOUBLE},
//
//    { "saturated_conductivity", PORT_OUT, CGO_DOUBLE},
//    { "porosity", PORT_OUT, CGO_DOUBLE},
//    { "number_of_soil_layer", PORT_OUT, CGO_UNSIGNED_INT},
//    { "soil_layer_height", PORT_OUT, CGO_DOUBLE},

	{ "soilwater", PORT_INOUT, CGO_DOUBLE},
	{ "surfacewater", PORT_IN, CGO_DOUBLE},
	{ "soilice", PORT_INOUT, CGO_DOUBLE},
	{ "surfaceice", PORT_IN, CGO_DOUBLE},

    { "nitrate", PORT_INOUT, CGO_DOUBLE},
    { "ammonium", PORT_INOUT, CGO_DOUBLE}
};
#define  N_PORTS ((size_t)sizeof(PORTS_STATIC_INFO)/sizeof(struct  ports_static_info_t))

extern int  ldndc_set_ports(
        struct cgo_task_t *);

extern int  ldndc_publish_initial(
        ldndc::llandscapedndc_t * /*region*/, cgo_db_t * /*database*/);
extern int  ldndc_publish_meteo(
        ldndc::llandscapedndc_t * /*region*/, cgo_db_t * /*database*/);
extern int  ldndc_publish_phys(
        ldndc::llandscapedndc_t * /*region*/, cgo_db_t * /*database*/);
extern int  ldndc_publish_soil(
        ldndc::llandscapedndc_t * /*region*/, cgo_db_t * /*database*/);

extern int  ldndc_read_water(
        ldndc::llandscapedndc_t * /*region*/, cgo_db_t * /*database*/);

#endif  /*  !CGO_CLIENT_LDNDC_PUBLISH_H_  */

