/*!
 * @brief
 *    client side shell for LandscapeDNDC
 *
 * @author
 *    steffen klatt (created on: jun 23, 2014),
 *    edwin haas,
 *    david kraus
 */

#include  "landscapedndc.h"
#include  "cgo_shell.h"

#ifdef  _HAVE_CMDLINE_PARSING
#  include  "app/ldndc-opt-impl.h"
#endif

#include  "liblog/res.h"

#include  <stdio.h>

// sk:off #ifdef  CRABMEAT_MPI
// sk:off #  include  "io/work-dispatcher/work-dispatcher-roundrobin.h"
// sk:off #else
// sk:off namespace ldndc {
// sk:off class  work_dispatcher_t;
// sk:off class  work_dispatcher_roundrobin_t;
// sk:off }
// sk:off #endif

#include  "ldndc/ldndc-publish.h"
#ifdef  __cplusplus
extern "C"
{
#endif

int
ldndc_task_execute(
        struct cgo_task_t *  _task, char **  /*_msgs*/, int  /*_n_msgs*/)
{
    landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_task->mem)->ldndc;
//    ldndc_publish_meteo( ldndc, _task->db);

//    ldndc_read_water( ldndc, _task->db);

    lerr_t  rc_step = ldndc->step( 1);
    if ( rc_step)
    {
        return  -1;
    }
#define  N_sends  80
    TIC(T_send);
    for ( int i=0; i<N_sends/*/N_sends_chunk*/; ++i)
    {
//        for ( int ii=0;ii<N_sends_chunk;++ii)
        {
//        ldndc_publish_phys( ldndc, _task->db);
//        ldndc_publish_soil( ldndc, _task->db);
        }
    }
    TOC(T_send);
    CBM_LogInfo( "T(send{",N_sends,"}=",T_send);

    return  0;
}
int
ldndc_task_initialize(
        struct cgo_task_t *  _task, char **  /*_msgs*/, int  /*_n_msgs*/)
{
    landscapedndc_t *  ldndc = ((llandscapedndc_shell_t *)_task->mem)->ldndc;

//    int  rc_pubini = ldndc_publish_initial( ldndc, _task->db);
//    if ( rc_pubini)
//    {
//        return  -1;
//    }
//    int  rc_pubsoil = ldndc_publish_soil( ldndc, _task->db);
//    if ( rc_pubsoil)
//    {
//        return  -1;
//    }
    return  0;
}
#ifdef  __cplusplus
}
#endif

llandscapedndc_shell_t *
ldndc_initialize(
        char const *  _project_fname, char const *  _cfg_fname, char const *  _schedule)
{

// sk:off #ifdef  CRABMEAT_MPI
// sk:off     ldndc::work_dispatcher_roundrobin_t  wd( 0);
// sk:off     wd.set_rank( ldndc::mpi::comm_rank( ldndc::mpi::comm_ldndc));
// sk:off     wd.set_size( ldndc::mpi::comm_size( ldndc::mpi::comm_ldndc));
// sk:off     ldndc::work_dispatcher_t *  this_work_dispatcher = &wd;
// sk:off #else
// sk:off     ldndc::work_dispatcher_t *  this_work_dispatcher = NULL;
// sk:off #endif

    printf( "pf=%s  sch=%s  cf=%s\n", _project_fname, _schedule, _cfg_fname);
    llandscapedndc_shell_t *  ldndc_s = (llandscapedndc_shell_t *)malloc( sizeof( llandscapedndc_shell_t));
    if ( !ldndc_s)
    {
        fprintf( stderr, "no mem\n");
        return  NULL;
    }
    ldndc_s->ldndc = new  landscapedndc_t(
            _project_fname, _schedule, _cfg_fname, NULL/*this_work_dispatcher*/);
    if ( !ldndc_s->ldndc || ldndc_s->ldndc->status != 0)
    {
        if ( ldndc_s->ldndc)
        {
            fprintf( stderr,
                "setting up ldndc instance failed with status %d\n", (int)ldndc_s->ldndc->status);
        }
        else
        {
            fprintf( stderr, "no mem\n");
        }

        delete  ldndc_s->ldndc;
        free( ldndc_s);
        return  NULL;
    }

    ldndc::mpi::barrier( ldndc::mpi::comm_ldndc);

    return  ldndc_s;
}


lerr_t
ldndc_finalize( llandscapedndc_shell_t *  _ldndc_s)
{
    if ( !_ldndc_s)
    {
        return  LDNDC_ERR_OK;
    }
    if ( _ldndc_s->ldndc)
    {
        _ldndc_s->ldndc->finalize();
        delete  _ldndc_s->ldndc;
    }
    free( _ldndc_s);

    return  LDNDC_ERR_OK;
}


lerr_t
run_sim_text(
        char const *  _project_fname, char const *  _cfg_fname, char const *  _schedule)
{
    llandscapedndc_shell_t *  ldndc_s =
        ldndc_initialize( _project_fname, _cfg_fname, _schedule);
    if ( !ldndc_s)
    {
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }
    struct cgo_task_t *  ldndc_task = cgo_task_new( "ldndc");
    if ( !ldndc_task)
    {
        /* FIXME  free stuff */
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }
    ldndc_task->initialize = ldndc_task_initialize;
    ldndc_task->execute = ldndc_task_execute;
    ldndc_task->mem = ldndc_s;


    struct cgo_ports_t *  ldndc_ports = cgo_ports_new( PORTS_STATIC_INFO, N_PORTS);
    if ( !ldndc_ports)
    {
        /* FIXME  free stuff */
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }
    ldndc_task->ports = ldndc_ports;
    ldndc_set_ports( ldndc_task);

    struct cgo_shell_t *  ldndc_shell = cgo_shell_new( ldndc_task);
    if ( !ldndc_shell)
    {
        /* FIXME  free stuff */
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }

    /* TODO  read from configuration file */
    struct cgo_hostaddress_t *  db_addr = cgo_hostaddress_newset( "localhost", 6379);

    int  rc_connect = cgo_shell_connect( ldndc_shell, db_addr);
    if ( rc_connect)
    {
	    cgo_shell_disconnect( ldndc_shell);
	    cgo_shell_destroy( ldndc_shell);
	    ldndc_shell = NULL;
    }
    cgo_hostaddress_destroy( db_addr);

    int  rc_exe = cgo_shell_execute( ldndc_shell);

    ldndc_finalize( ldndc_s);

    cgo_ports_destroy( ldndc_ports);
    cgo_task_destroy( ldndc_task);
    cgo_shell_destroy( ldndc_shell);

    if ( rc_exe)
    {
        fprintf( stderr, "failed with status %d\n", rc_exe);
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

#include  "ldndc.h"
int
main(
        int  argc,
        char **  argv)
{
    __LDNDC_APPLICATION_INIT__(argc,argv,NULL,NULL,0)

    (void)ldndc_ieee_set_mode( LDNDC_FE_DIVBYZERO|LDNDC_FE_OVERFLOW|/*LDNDC_FE_UNDERFLOW|*/LDNDC_FE_INVALID);

    (void)ldndc_signal_traps_init();
    (void)ldndc_have_signal_trap( LDNDC_SIGFPE);
    (void)ldndc_have_signal_trap( LDNDC_SIGSEGV);
    (void)ldndc_have_signal_trap( LDNDC_SIGUSR1);

    lerr_t  rc( LDNDC_ERR_FAIL);

#ifdef  _HAVE_CMDLINE_PARSING
    struct  ldndc_args_info_t  args_info;
    struct  ldndc_cmdline_parser_params *  parser_params = ldndc_cmdline_parser_params_create();

    ldndc_cmdline_parser_params_init( parser_params);
    ldndc_cmdline_parser_init( &args_info);
    if ( ldndc_cmdline_parser_ext( argc, argv, &args_info, parser_params) != 0)
    {
        fprintf( stderr, "error parsing command line options\n");
        ldndc_cmdline_parser_free( &args_info);
        free( parser_params);
        __LDNDC_APPLICATION_DEINIT__(argc,argv,NULL,NULL,0)
        exit( EXIT_FAILURE);
    }

    rc = run_sim_text(
            ( args_info.inputs && args_info.inputs[0]) ? args_info.inputs[0] : NULL,
            ( args_info.config_given) ? args_info.config_arg : NULL,
            ( args_info.schedule_given) ? args_info.schedule_arg : NULL);
    fprintf( stderr, "node %4d: %s.\n",
            ldndc::mpi::comm_rank( ldndc::mpi::comm_ldndc),
            ( rc) ? "premature termination or incomplete run" : "normal termination");

    ldndc_cmdline_parser_free( &args_info);
    free( parser_params);
#endif

    __LDNDC_APPLICATION_DEINIT__(argc,argv,NULL,NULL,0)
    exit(( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE);

}  /*  end main ldndc  */

