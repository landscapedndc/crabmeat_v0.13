#!/bin/bash -f
#
# simple egrep wrapper that considers only but
# all (c++,cmake,etc) source files in directories
# below trunk
#
# first argument is search pattern
#
# second parameter can be
#
#   q   quiet, summary only
#   f   search in 'common' only
#   k   search in 'kernels' only
#   h   consider only header files (h,hpp,hxx)
#   c   consider only source files (c,cpp,cxx)
#   l   dump only list of matching files
#   w   search pattern must be "word"
#   =   omit decorative output (newline, summary, etc.)

#   e   dump editor invocation command for matching files
#   r   dump replace command for given search pattern and
#       replacement (e.g. $> <this-script>  <patt> rw+= <patt-repl> <dir>)

#   +   next parameter is considered subdirectory
#       which overrides internals


author="s. klatt <steffen.klatt@kit.edu>"

function  exit_missing_bin()
{
    offender="$1"

    echo  "missing program: $offender"
    exit 7
}

## use OSTYPE?
which uname 2>&1 1>/dev/null || exit_missing_bin "uname"
platform=`uname`

patt="$1"
arg="$2"
optarg1="$3"
optarg2="$4"
arg_f="$arg"
dir=""
filepattern=""
nodeco="yes"
opt_quiet=""
opt_file_edit=""
opt_file_list=""
opt_replace_cmd=""
opt_full_word=""

_editor=${EDITOR:-/usr/bin/editor}
editor=${LDNDC_EDITOR:-$_editor}
# TODO  cut options from $editor (with shell capabilities only) and check -x
if [ -z "$editor" -a ! -x "$_editor" ]
then
    editor=""
fi

## find
find_bin=`which find`
test ! -z "$find_bin" -a -x "$find_bin"  ||  exit_missing_bin "find"
find_opts=""
patt_base=" -type f -a -name '' "

## egrep
grep_bin=`which egrep`
test ! -z "$grep_bin" -a -x "$grep_bin"  ||  exit_missing_bin "egrep"
grep_opts=" -n  --color=always  -E "

## head
head_bin=`which head`
test ! -z "$head_bin" -a -x "$head_bin" || head_bin=""

## awk
awk_bin=`which awk`
test ! -z "$awk_bin" -a -x "$awk_bin" || awk_bin=""

## sed
sed_bin=`which sed`
test ! -z "$sed_bin" -a -x "$sed_bin" || sed_bin=""

line_number_show=1
if [ -z "$editor" -o -z "$head_bin" -o -z "$awk_bin" ]
then
   line_number_show=0
fi


case "$platform" in
    "Linux")
        ## older versions of gnu find do not support this
        find_opts+=""
        patt_base+=" -regextype posix-extended "
        grep_opts+=""
        ;;
    "Darwin")
        ## os x's find uses -E instead
        find_opts+=" -E "
        patt_base+=""
        grep_opts+=""
        ;;
    *)
        echo "platform \"$platform\" not supported. contact $author."
        exit 2
        ;;
esac



function  read_arg()
{
    _argv="$1"

    if [ -z "$arg" ]
    then
        return  0
    fi
    
    _arg="$arg"
    arg=`echo -n "$_arg" | tr -d "$_argv"`
    if [ "${_arg}x" != "${arg}x" ]
    then
        case "$_argv" in
            l)
                opt_file_list="yes"
                ;;
            e)
                opt_file_edit="yes"
                ;;
            q)
                opt_quiet="yes"
                ;;

            a)
                dir+=" ./apps ./test "
                ;;
            f)
                dir+=" ./common ./landscape "
                ;;
            k)
                dir+=" ./kernels "
                ;;
            C)
                dir+=" ./coupling "
                ;;

            +)
                dir="$optarg2"
                ;;

            h)
                filepattern+=\ -o\ -regex\ .*\\\.\(h\|hpp\|hxx\)
                ;;
            c)
                filepattern+=\ -o\ -regex\ .*\\\.\(c\|cc\|cpp\|cxx\)
                ;;
            b)
                ## note: no +=
                filepattern=\ -o\ -regex\ .*CMakeLists.*\\\.\(inc\|txt\)\ -o\ -regex\ .*\\\.\(cmake-inc\)
                dir="."
                ;;

            r)
                opt_replace_cmd="yes"
                ;;

            w)
                opt_full_word="yes"
                patt="\b${patt}\b"
                ;;

            =)
                nodeco="yes"
                ;;
            
            *)
                echo "straaaange ..."
                return  13
                ;;
        esac
    fi
    return  0
}

if [ -z "$arg" ]
then
    :
else
    arg=`echo -n "$arg" | tr [:upper:] [:lower:]`
    for a in l e q f k h c C b r w + =
    do
        read_arg $a
    done
    if [ ! -z "${arg}" ]
    then
        echo  "second argument ($arg_f, $arg) does not compute ;-)"
        exit 1
    fi
    ## if no replace was requested directory argument must have been
    ## at position 3 (if at all..)
    if [ -z "$opt_replace_cmd" ] #-a ! -z "$optarg1" ]
    then
        dir="$optarg1"
    fi
fi

## use 'sed' for replacing character sequences in source files
replace_bin="$sed_bin --in-place 's/$patt/$optarg1/g' "

## set default search directories
if [ -z "$dir" ]
then
    dir=" ./libs ./bindings ./shells ./clients ./common ./comp ./cpl ./domain ./io ./logging ./scientific ./tests ./cmake"
fi

## set default file pattern
if [ -z "$filepattern" ]
then
    filepattern=-o\ -regex\ .*\\\.\(h\|hpp\|hxx\|h\\.in\)\ -o\ -regex\ .*\\\.\(c\|cc\|cpp\|cxx\|inl\)\ -o\ -regex\ .*\\\.\(xml\|txt\)\ -o\ -regex\ .*\\\.\(cmake-conf\|cmake-inc\)\ -o\ -regex\ .*\\\.\(py\|i\)
fi

## generate file list
files="`$find_bin  $find_opts  $dir  $patt_base  $filepattern`"


if [ -z "$opt_quiet" ]
then
    if [ -z "$nodeco" ]
    then
        echo
    fi
    [ ! -z "$opt_file_edit" ] && echo -en "$editor  "
    [ ! -z "$opt_replace_cmd" ] && echo -n "$replace_bin  "
    for f in $files
    do
        res=`$grep_bin  $grep_opts  -e "$patt"  $f`
        if [ $? -eq 0 ]
        then
            if [ ! -z "$opt_file_edit" ]
            then
                echo -ne "$f  "
            elif [ ! -z "$opt_file_list" ]
            then
                echo -ne "$f\n"
            elif [ ! -z "$opt_replace_cmd" ]
            then
                echo -ne "$f  "
            else
                line_number=""
                if [ $line_number_show -ne 0 ]
                then
                    line_number="+`echo $res | $head_bin -n1 | $awk_bin -F ':' '{ print $1 }'`"
                fi

                echo    " $editor  $f  $line_number"
                echo    "$res"
                echo
            fi
        fi
    done
    [ ! -z "$opt_file_edit" -a ! -z "$nodeco" ] && echo -en "\n"
fi

if [ -z "$nodeco" ]
then
    echo
    echo  "----"
    echo  "`egrep "$patt" $files | wc -l` matching lines in `egrep -l "$patt" $files | sort | uniq | wc -l` file(s) below $dir for pattern '$patt'"
fi


