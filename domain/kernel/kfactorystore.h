/*!
 * @brief
 *    kernel factory store
 *
 * @author
 *    steffen klatt (created on: mar 3, 2016)
 */

#ifndef  CBM_KERNELFACTORYSTORE_H_
#define  CBM_KERNELFACTORYSTORE_H_

#include  "crabmeat-common.h"

namespace cbm {

class  kernel_t;
struct  CBM_API  kernelfactorystore_t
{
    public:
        kernelfactorystore_t( void *  _kernelfactorystore)
            : m_kernelfactorystore( _kernelfactorystore)
        {}
        virtual  ~kernelfactorystore_t() { }

        size_t  size();

        kernel_t *  construct_kernel( char const * /*name*/,
                lid_t const & /*object id*/, int * /*exists*/ = NULL);

    private:
        void *  m_kernelfactorystore;
};
} /* namespace cbm */

#endif  /*  !CBM_KERNELFACTORYSTORE_H_  */

