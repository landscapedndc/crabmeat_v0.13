/*!
 * @author
 *    steffen klatt (created on: dec 01, 2016)
 */

#include  "kernel/ksetup.h"
#include  "log/cbm_baselog.h"

cbm::ksetup_t::ksetup_t()
    : m_jquery( NULL, 0, NULL)
{ }
cbm::ksetup_t::ksetup_t(
        char const *  _name, char const *  _data)
    : m_jquery( _data, 0, _name)
{ }

cbm::ksetup_t::ksetup_t( cbm::ksetup_t const &  _rhs)
    : m_jquery( _rhs.m_jquery)
{ }

cbm::ksetup_t &
cbm::ksetup_t::operator=( cbm::ksetup_t const &  _rhs)
{
    if ( this != &_rhs)
        { this->m_jquery = _rhs.m_jquery; }
    return  *this;
}

cbm::ksetup_t::~ksetup_t()
{ }

bool  cbm::ksetup_t::parse_ok() const
    { return  this->m_jquery.parse_ok(); }

bool
cbm::ksetup_t::exists( char const *  _path)
{
    return  this->m_jquery.exists( _path);
}
bool
cbm::ksetup_t::exists( cbm::string_t const &  _path)
{
    return  this->m_jquery.exists( _path);
}

int
cbm::ksetup_t::query_size( char const *  _path)
{
    return  this->m_jquery.query_size( _path);
}


char const *
cbm::ksetup_t::query_string( char const *  _path,
        char const *  _default)
{
    return  this->m_jquery.query_string( _path, _default);
}
cbm::string_t
cbm::ksetup_t::query_string( cbm::string_t const &  _path,
        char const *  _default)
{
    return  this->m_jquery.query_string( _path, _default);
}


bool
cbm::ksetup_t::query_bool(
        char const *  _path, bool  _default)
{
    return  this->m_jquery.query_bool( _path, _default);
}
bool
cbm::ksetup_t::query_bool(
    cbm::string_t const &  _path, bool  _default)
{
    return  this->m_jquery.query_bool( _path, _default);
}


int
cbm::ksetup_t::query_int(
        char const *  _path, int  _default)
{
    return  this->m_jquery.query_int( _path, _default);
}
int
cbm::ksetup_t::query_int(
    cbm::string_t const &  _path, int  _default)
{
    return  this->m_jquery.query_int( _path, _default);
}


double
cbm::ksetup_t::query_double(
    char const *  _path, double  _default)
{
    return  this->m_jquery.query_double( _path, _default);
}
double
cbm::ksetup_t::query_double(
    cbm::string_t const &  _path, double  _default)
{
    return  this->m_jquery.query_double( _path, _default);
}

cbm::string_t
cbm::ksetup_t::query_any( char const *  _path,
        char const *  _default)
{
    return  this->m_jquery.query_any( _path, _default);
}

cbm::string_t
cbm::ksetup_t::query_any( cbm::string_t const &  _path,
        char const *  _default)
{
    return  this->m_jquery.query_any( _path, _default);
}

