/*!
 * @brief
 *    kernel factory
 *
 * @author
 *    steffen klatt (created on: oct 8, 2011)
 */

#ifndef  CBM_KERNELFACTORY_H_
#define  CBM_KERNELFACTORY_H_

#include  "crabmeat-common.h"
#include  "kernel/kbase.h"
#include  "memory/cbm_factorystore.h"
#include  <string>

#define  kernel_factory_prefix "kernel"

namespace cbm {

class  kernel_t;
struct  CBM_API  kernelfactory_base_t
{
    public:
        kernelfactory_base_t()
        {}
        virtual  ~kernelfactory_base_t() { }

        virtual  char const *  name() const = 0;

        virtual  kernel_t *  construct(
                lid_t const &) const = 0;
        virtual  void  destroy(
                kernel_t *) const = 0;
};

typedef cbm::factorystoreregister_t< kernelfactory_base_t >  kernelfactoryregister_t;

template < class _K >
struct  kernelfactory_t  :  public  kernelfactory_base_t
{
    virtual  ~kernelfactory_t();

    char const *  name() const;

    kernel_t *  construct( lid_t const &) const;
    void  destroy( kernel_t *) const;

    private:
        std::string  m_name;

    public:
        kernelfactory_t< _K >( char const *);
};

} /* namespace cbm */

template < class _K >
cbm::kernelfactory_t< _K >::kernelfactory_t(
        char const *  _kname)
        : kernelfactory_base_t(),
          m_name(( _kname) ? _kname : "")
{
    kernelfactoryregister_t::register_factory( kernel_factory_prefix, _kname, this);
}

template < class _K >
cbm::kernelfactory_t< _K >::~kernelfactory_t< _K >()
{
}

template < class _K >
char const *
cbm::kernelfactory_t< _K >::name()
const
{
    return  this->m_name.c_str();
}

template < class _K >
cbm::kernel_t *
cbm::kernelfactory_t< _K >::construct(
        lid_t const &  _id)
const
{
    return  static_cast< cbm::kernel_t * >( _K::new_instance( _id));
}
template < class _K >
void
cbm::kernelfactory_t< _K >::destroy(
        kernel_t *  _kernel)
const
{
    if ( _kernel)
    {
        _kernel->delete_instance();
    }
}


#endif  /*  !CBM_KERNELFACTORY_H_  */

