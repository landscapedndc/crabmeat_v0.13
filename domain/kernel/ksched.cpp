/*!
 * @brief
 *    kernel scheduler
 *
 * @author
 *    steffen klatt (created on: jul 04, 2016)
 */

#include  "kernel/ksched.h"
#include  "log/cbm_baselog.h"

#include  "kernel/ktemporal.h"
cbm::ksched_t::ksched_t()
    : m_ktemporal( NULL)
    { }
cbm::ksched_t::ksched_t(
        cbm::ktemporal_t *  _ktemporal,
            cbm::kernel_t *)
    : m_ktemporal( _ktemporal)
    { }

lerr_t
cbm::ksched_t::register_for_execution( cbm::kernel_t * _kclient, cbm::RunLevelArgs * _args)
{
    lerr_t rc_reg = this->m_ktemporal->register_for_execution( _kclient, _args);
    if ( rc_reg)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

