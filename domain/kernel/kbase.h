/*!
 * @brief
 *    base class for domain part
 *
 * @author
 *    steffen klatt (created on: oct 8, 2011)
 */

#ifndef  CBM_KERNEL_H_
#define  CBM_KERNEL_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"
#include  "comm/cbm_comm.h"

#include  "kernel/kobject.h"
#include  "kernel/ksetup.h"
#include  "kernel/kstats.h"

#include  "cfgfile/cbm_cfgfile.h"

namespace cbm {

class public_state_t;
class io_kcomm_t;
struct request_t;
struct reply_t;

enum RunLevel
{
    RL_NONE = -1,

    RL_CREATE = 0,
    RL_CONFIGURE,
    RL_REGISTERPORTS,
    RL_INITIALIZE,

#define RL_LoopStart RL_READ
    RL_READ, /*input refresh*/
    RL_SOLVE, /*rate calculations */
    RL_INTEGRATE, /*state update*/
    RL_WRITE, /*stream output*/
#define RL_LoopEnd RL_WRITE

    RL_FINALIZE,
    RL_UNREGISTERPORTS,
    RL_DESTROY,

    RL_ALL
};

struct RunLevelFlags
{
    bool terminate:1;
    bool skip:1;
    bool drop:1;
    bool error:1;
    int prio:4;  /* (low) -7, .., 0, .., 7 (high) */

    bool usr1:1;
    bool usr2:1;
};

struct RunLevelArgs
{
    RunLevelArgs( RunLevel  _rl=RL_NONE)
        : iokcomm( NULL),
            cfg( NULL), clk( NULL),
            kcfg( NULL), ptr( NULL),
            sse( -1), nextcall_sse( -1),
            rl( _rl), nextcall_rl( RL_NONE)
        {
            this->dT.q = this->nextcall_dT.q = -1;
            this->dT.unit = this->nextcall_dT.unit = '-';

            this->flags.terminate = 0;
            this->flags.skip = 0;
            this->flags.drop = 0;
            this->flags.error = 0;
            this->flags.prio = 0;

            this->flags.usr1 = 0;
            this->flags.usr2 = 0;
        }
    cbm::io_kcomm_t *  iokcomm;
    cbm::config_file_t const *  cfg; /*configuration file*/
    cbm::sclock_t const *  clk; /* TODO use sse instead*/

    char const *  kcfg; /*nul-terminated json formatted string*/
    void *  ptr; /*arbitrary data in local memory*/

    /*kernel feedback (nextcall_*)*/
    cbm::dT_t  dT, nextcall_dT; /*current/next time step size*/
    cbm::td_scalar_t  sse, nextcall_sse; /*current/next execution time step*/
    RunLevel  rl, nextcall_rl; /*current/next execution runlevel*/

    cbm::RunLevelFlags  flags;
};

class kernel_t;
struct CBM_API CBM_KernelTask
{
    virtual ~CBM_KernelTask() = 0;
    virtual RunLevel  runlevel() const = 0;
    virtual lerr_t  execute( kernel_t *, RunLevelArgs *) = 0;
};
template < RunLevel _RunLevel >
struct CBM_KernelRunLevelTask : CBM_KernelTask
{
    RunLevel  runlevel() const
        { return _RunLevel; }
};
struct CBM_API CBM_KernelTaskConfigure : CBM_KernelRunLevelTask<RL_CONFIGURE>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};
struct CBM_API CBM_KernelTaskRegisterPorts : CBM_KernelRunLevelTask<RL_REGISTERPORTS>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};
struct CBM_API CBM_KernelTaskInitialize : CBM_KernelRunLevelTask<RL_INITIALIZE>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};

struct CBM_API CBM_KernelTaskRead : CBM_KernelRunLevelTask<RL_READ>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};
struct CBM_API CBM_KernelTaskSolve : CBM_KernelRunLevelTask<RL_SOLVE>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};
struct CBM_API CBM_KernelTaskIntegrate : CBM_KernelRunLevelTask<RL_INTEGRATE>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};
struct CBM_API CBM_KernelTaskWrite : CBM_KernelRunLevelTask<RL_WRITE>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};

struct CBM_API CBM_KernelTaskUnregisterPorts : CBM_KernelRunLevelTask<RL_UNREGISTERPORTS>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};
struct CBM_API CBM_KernelTaskFinalize : CBM_KernelRunLevelTask<RL_FINALIZE>
{
    lerr_t  execute( kernel_t *, RunLevelArgs *);
};

class CBM_API kernel_t : public kernel_object_t
{
    CBM_KernelTask *  tasks[RL_ALL];
public:
    void  set_runlevel_task( RunLevel, CBM_KernelTask *);
    lerr_t  execute_runlevel( RunLevelArgs *);

public:
    virtual  char const *  name() const = 0;
    virtual  char const *  ID() const = 0;
    virtual  char const *  model_name()
        const { return this->name(); }

#ifdef  CRABMEAT_KERNEL_STATISTICS
public:
    cbm::resources_statistics_t  stats;
#endif

protected:
    kernel_t();
public:
    virtual  ~kernel_t() = 0;

public:
    /*! RunLevel RL_CONFIGURE */
    virtual lerr_t  configure( RunLevelArgs *)
        { return LDNDC_ERR_OK; }
    /*! Runlevel RL_REGISTERPORTS */
    virtual lerr_t  register_ports( RunLevelArgs *)
        { return LDNDC_ERR_OK; }
    /*! RunLevel RL_INITIALIZE */
    virtual lerr_t  initialize( RunLevelArgs *)
        { return LDNDC_ERR_OK; }

    /*! Runlevel RL_READ */
    virtual lerr_t  read( RunLevelArgs *)
        { return LDNDC_ERR_OK; }
    /*! Runlevel RL_SOLVE */
    virtual lerr_t  solve( RunLevelArgs *)
        { return LDNDC_ERR_OK; }
    /*! Runlevel RL_INTEGRATE */
    virtual lerr_t  integrate( RunLevelArgs *)
        { return LDNDC_ERR_OK; }
    /*! Runlevel RL_WRITE */
    virtual lerr_t  write( RunLevelArgs *)
        { return LDNDC_ERR_OK; }

    /*! Runlevel RL_FINALIZE */
    virtual lerr_t  finalize( RunLevelArgs *)
        { return LDNDC_ERR_OK; }
    /*! Runlevel RL_UNREGISTERPORTS */
    virtual lerr_t  unregister_ports( RunLevelArgs *)
        { return LDNDC_ERR_OK; }

public:
    virtual  int  process_request(
                reply_t *,  request_t const *)
        { return  1; }
    virtual  int  query_feature( char ** /*result buffer*/,
                            char const * /*query*/)
        { return  -1; }

#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
    virtual  int  create_checkpoint()
        { return  0; }
    virtual  int  restore_checkpoint( ldate_t)
        { return  0; }
#endif

    cbm::ksetup_t  setup_parser( char const * /*input*/,
        char const * = NULL /*kernel ID (use own if NULL)*/);


public: /*status*/
    bool  is_active() const
        { return  this->m_status == 0; }
    void  deactivate()
        { this->m_status = 1; }
private:
    int  m_status;

public: /*rank*/
    int  rank() const
        { return this->m_rank; }
    void  rank( int  _rank)
        { this->m_rank = _rank; }
private:
    int  m_rank;

public: /*URI*/
    cbm::uri_t const &  uri() const
        { return  this->m_uri; }
    void  uri( cbm::uri_t const &  _uri)
        { this->m_uri = _uri; }
private:
    cbm::uri_t  m_uri;

private:
    typedef lerr_t (kernel_t::*RunLevelMaintenanceTask)( RunLevelArgs *);
    RunLevelMaintenanceTask  pre_tasks[RL_ALL];
    lerr_t  pre_execute_runlevel( RunLevelArgs *);
    RunLevelMaintenanceTask  post_tasks[RL_ALL];
    lerr_t  post_execute_runlevel( RunLevelArgs *);
    /*! RunLevel RL_CONFIGURE */
    lerr_t  pre_configure( RunLevelArgs *);
    lerr_t  post_configure( RunLevelArgs *);
    /*! Runlevel RL_REGISTERPORTS */
    lerr_t  pre_register_ports( RunLevelArgs *);
    lerr_t  post_register_ports( RunLevelArgs *);
    /*! RunLevel RL_INITIALIZE */
    lerr_t  pre_initialize( RunLevelArgs *);
    lerr_t  post_initialize( RunLevelArgs *);

    /*! Runlevel RL_READ */
    lerr_t  pre_read( RunLevelArgs *);
    lerr_t  post_read( RunLevelArgs *);
    /*! Runlevel RL_SOLVE */
    lerr_t  pre_solve( RunLevelArgs *);
    lerr_t  post_solve( RunLevelArgs *);
    /*! Runlevel RL_INTEGRATE */
    lerr_t  pre_integrate( RunLevelArgs *);
    lerr_t  post_integrate( RunLevelArgs *);
    /*! Runlevel RL_WRITE */
    lerr_t  pre_write( RunLevelArgs *);
    lerr_t  post_write( RunLevelArgs *);

    /*! Runlevel RL_FINALIZE */
    lerr_t  pre_finalize( RunLevelArgs *);
    lerr_t  post_finalize( RunLevelArgs *);
    /*! Runlevel RL_UNREGISTERPORTS */
    lerr_t  pre_unregister_ports( RunLevelArgs *);
    lerr_t  post_unregister_ports( RunLevelArgs *);

};

} /* namespace cbm */

#define  CBM_KERNEL_LINKME(__name__) int ldndc_kernel_force_link_##__name__ = 0;
#define  CBM_KERNEL_LINKIT(__name__) void ldndc_kernel_force_link_function_##__name__( void) { extern int ldndc_kernel_force_link_##__name__; ldndc_kernel_force_link_##__name__ = 1; }

#endif  /*  !CBM_KERNEL_H_  */

