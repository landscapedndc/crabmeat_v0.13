
#ifndef  CBM_KERNEL_OBJECT_H_
#define  CBM_KERNEL_OBJECT_H_

#include  "cbm_object.h"

namespace cbm {
class  CBM_API  kernel_object_t  :  public  cbm::object_t
{
    public:
        kernel_object_t() : cbm::object_t() {}
        kernel_object_t( lid_t const &  _id) : cbm::object_t( _id) {}
};
} /* namespace cbm */

/* kernel objects */
#define  CBM_KERNEL_OBJECT(__type__,__id__) \
    LDNDC_OBJECT(__type__) \
    private: static char const * const  m_kname; \
    public: char const *  name() const { return  this->m_kname; } \
    private: static char const * const  m_ID; \
    public: char const *  ID() const { return  this->m_ID; } \
    private: \
        static  cbm::kernelfactory_t< __type__ > const  kernelfactory_##__id__;

#define  CBM_KERNEL_OBJECT_DEFN(__ns__,__type__,__id__,__factory_name__,__kname__) \
    cbm::kernelfactory_t< __ns__::__type__ > const \
	    __ns__::__type__::kernelfactory_##__id__( __factory_name__); \
    LDNDC_OBJECT_DEFN(__ns__::__type__) \
    char const * const  __ns__::__type__::m_kname = __kname__; \
    char const * const  __ns__::__type__::m_ID = __factory_name__;

#endif  /*  !CBM_KERNEL_OBJECT_H_  */

