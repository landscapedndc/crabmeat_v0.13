/*!
 * @brief
 *   kernel scheduler interface. when kernel is registered for
 *   execution the appropriate runlevels are executed:
 *
 *     - read
 *     - solve
 *     - integrate
 *     - write
 *
 * @author
 *    steffen klatt (created on: jul 04, 2016)
 */

#ifndef  CBM_KSCHED_H_
#define  CBM_KSCHED_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

namespace cbm {

class kernel_t;
struct RunLevelArgs;

class ktemporal_t;
class CBM_API ksched_t
{
    public:
        ksched_t();
        ksched_t( ktemporal_t *, kernel_t * /*client*/);

        lerr_t  register_for_execution( cbm::kernel_t * /*client*/,
                    cbm::RunLevelArgs * /*runlevel arguments*/);

    private:
        ktemporal_t *  m_ktemporal;
};
} /* namespace cbm */

#endif  /* !CBM_KSCHED_H_ */

