/*!
 * @author
 *    steffen klatt (created on: jul 03, 2016)
 */

#include  "kernel/kspatial.h"
#include  "cbm_rtcfg.h"

#include  "kernel/io-kcomm.h"
#include  "time/cbm_time.h"
#include  "cfgfile/cbm_cfgfile.h"
#include  "log/cbm_baselog.h"

#include  "openmp/cbm_omp.h"

cbm::uri_t  cbm::uri_generate_t::generate(
    kqueue_data_t const *  _kdesc, kernel_t const *  _parent)
{
    cbm::string_t  uri_str;
    if ( _kdesc->ksource.is_empty())
        { uri_str += _kdesc->kname; }
    else
    {
        uri_str << _kdesc->ksource;
        if ( _kdesc->kid != invalid_lid)
        {
            uri_str << "/";
            uri_str << _kdesc->kid;
        }
    }

    cbm::uri_t  kparent_uri = _parent->uri();
    cbm::uri_t  k_uri =
        kparent_uri.new_child( uri_str.c_str(), NULL);

    return  k_uri;
}

static cbm::RunLevelArgs  CBM_MakeRunLevelArgs(
        cbm::RunLevel _rl, cbm::knode_t const * _knode,
            cbm::mountargs_t const * _mountargs = NULL)
{
    cbm::RunLevelArgs  ra( _rl);

    ra.cfg = CBM_LibRuntimeConfig.cfg;
    ra.clk = CBM_LibRuntimeConfig.clk;
    if ( ra.clk)
        { ra.sse = ra.clk->sse(); }

    if ( _knode)
    {
        ra.iokcomm = _knode->iokcomm;
        ra.kcfg = _knode->kcfg;
    }
    if ( _mountargs)
    {
        ra.ptr = _mountargs->ptr;
    }

    return ra;
}

static void  CBM_StealKCfgMemory(
            cbm::knode_t *  _knode, cbm::kqueue_data_t *  _kdesc)
{
    _knode->kcfg = _kdesc->mountargs.cfg;
    _kdesc->mountargs.cfg = NULL;
}
static void  CBM_ReturnKCfgMemory(
    cbm::kqueue_data_t *  _kdesc, cbm::knode_t *  _knode)
{
    _kdesc->mountargs.cfg = _knode->kcfg;
    _knode->kcfg = NULL;
}
static void  CBM_FreeKCfgMemory( cbm::knode_t *  _knode)
{
    if ( _knode->kcfg)
    {
        cbm::strfree( _knode->kcfg);
        _knode->kcfg = NULL;
    }
}

cbm::knodes_t::knodes_t()
{
    this->m_nodes = CBM_SetCreate( CBM_MemPoolRoot,
            sizeof(knode_t), KNodeMemoryReserveCount, 0);
}
cbm::knodes_t::~knodes_t()
{
    iterator  knode = this->begin();
    while ( knode != this->end())
    {
        knode_t *  K = *knode;
        if ( K && K->parent == NULL)
        {
            /*skip root node*/
            CBM_FreeKCfgMemory( K);
            K->~knode_t();
        }
        else if ( K)
        {
            if ( K->kernel)
            {
                K->kernel->delete_instance();
                K->kernel = NULL;
            }
            if ( K->iokcomm)
            {
                /* it needs to live during kernel destruct */
                K->iokcomm->delete_instance();
                K->iokcomm = NULL;
            }
            CBM_FreeKCfgMemory( K);
            K->~knode_t();
        }

        ++knode;
    }
    CBM_SetDestroy( this->m_nodes);
}
cbm::knode_t *
cbm::knodes_t::insert_node( knode_t *  _kparent, knode_t *  _knode)
{
    CBM_Assert( _knode && _knode->kernel);
    cbm::string_t const  uri = _knode->kernel->uri().str();
    knode_t *  knode = static_cast< knode_t * >( CBM_SetInsert(
            this->m_nodes, uri.c_str(), NULL));
    if ( knode)
    {
        new (knode) knode_t();
        *knode = *_knode;
        CBM_LogDebug( "Insert KernelNode [@",(void*)knode,
                "] '", uri,"'  {",knode->kernel->model_name(),"}");
        knode->kcfg = NULL;
        knode->parent = _kparent;
        knode->sibling = NULL;
        knode->child = NULL;
        knode->next = NULL;
        /* append to children of parent node unless i am root */
        if ( _kparent)
        {
            knode_t *  child = _kparent->child;
            if ( child)
            {
                while ( child->sibling)
                    { child = child->sibling; }
                /* append new node (tree) */
                child->sibling = knode;
            }
            else
            {
                /* insert new node (tree) */
                _kparent->child = knode;
            }
        }
    }
    return knode;
}
cbm::knode_t *
cbm::knodes_t::remove_node( knode_t *  _knode,
                cbm::string_t const &  _uri)
{
    CBM_Assert( _knode);
    if ( _knode->child)
        { return _knode; /*error if not leaf*/ }

    /* re-link (tree) */
    if ( _knode->sibling && _knode->parent)
    {
        /* get first child of parent and to find my left sibling */
        knode_t *  knode = _knode->parent->child;
        while ( knode)
        {
            if ( knode->sibling == _knode)
            {
                knode->sibling = _knode->sibling;
                break;
            }
            knode = knode->sibling;
        }
    }

    knode_t *  knode = static_cast< knode_t * >( CBM_SetGetValue(
            this->m_nodes, _uri.c_str()));
    if ( knode)
    {
        CBM_FreeKCfgMemory( knode);
        knode->~knode_t();
    }
    knode = static_cast< knode_t * >( CBM_SetRemove(
            this->m_nodes, _uri.c_str()));
    if ( knode)
    {
// sk:note  this is actually an error because
// sk:note  memory managment is done by CBM_Set,
// sk:note  i.e., no "delete knode;"
    }
    return knode;
}
cbm::knode_t *
cbm::knodes_t::find_node( cbm::string_t const &  _uri)
{
    cbm::string_t const  uri = _uri;
    CBM_SetNodeIterator  ni =
        CBM_SetFind( this->m_nodes, uri.c_str());
    if ( !CBM_SetNodeIteratorIsEnd( ni))
        { return static_cast< cbm::knode_t * >( ni.value); }
    return NULL;
}
cbm::knode_t const *
cbm::knodes_t::find_node( cbm::string_t const &  _uri) const
{
    cbm::string_t const  uri = _uri;
    CBM_SetNodeIterator  ni =
        CBM_SetFind( this->m_nodes, uri.c_str());
    if ( !CBM_SetNodeIteratorIsEnd( ni))
        { return static_cast< cbm::knode_t * >( ni.value); }
    return NULL;
}

cbm::knodes_t::const_iterator::const_iterator()
    : m_ni( CBM_SetNodeIteratorEnd)
    { }
cbm::knodes_t::const_iterator::const_iterator( CBM_Set const &  _knodes)
    : m_ni( CBM_SetFindHead( _knodes))
    { }

cbm::knodes_t::iterator::iterator()
    : m_ni( CBM_SetNodeIteratorEnd)
    { }
cbm::knodes_t::iterator::iterator( CBM_Set &  _knodes)
    : m_ni( CBM_SetFindHead( _knodes))
    { }


#define  CBM_RootUri  "ldndc://"+CBM_LibRuntimeConfig.rc.ipaddr
CBM_KERNEL_OBJECT_DEFN(cbm,kspatial_t,__mounter__,"__mounter__","Mounter")
cbm::kspatial_t::kspatial_t()
        : cbm::kernel_t(),
          m_iokcomm( NULL)
{
    this->uri( cbm::uri_t( CBM_RootUri));
    this->rank( 0);
}

cbm::kspatial_t::~kspatial_t()
{
    this->kernels_destroy();
}

void
cbm::kspatial_t::set_modelcompositor(
        char const *  _modelcompositor)
    { this->m_modelcompositor = _modelcompositor; }

lerr_t
cbm::kspatial_t::enqueue_for_mount(
    cbm::string_t const &  _kname,
    lid_t const &  _kid, cbm::string_t  _ksource,
    cbm::kernel_t *  _kclient, cbm::mountargs_t const *  _mountargs)
{
    kqueue_data_t  kq;
    kq.kname = _kname;
    kq.kid = _kid;
    kq.ksource = _ksource;
    /* if no client was provided, make root the parent node */
    kq.kclient = _kclient ? _kclient : this;
    kq.kclient_uri = kq.kclient->uri().str();
    if ( _mountargs)
        { kq.mountargs = *_mountargs; }

    /* schedule for regridding */
    if ( this->m_kqueue.is_empty())
    {
        ksched_t ksched = this->m_iokcomm->temporal_controller( kq.kclient);
        RunLevelArgs  rl = CBM_MakeRunLevelArgs( RL_READ, NULL);
        rl.flags.drop = 1; /*drop after execution*/
        rl.flags.prio = 7; /*highest priority*/
        ksched.register_for_execution( this, &rl);
    }

    lerr_t rc_push = this->m_kqueue.push_back( kq);
    if ( rc_push)
    {
        CBM_LogError( "Unexpected error when attempting to queue kernel");
        return LDNDC_ERR_NOMEM;
    }

    CBM_LogDebug( "enqueuing new kernel mount request",
        "  [name=",kq.kname,",source=",_ksource,
        ",#requests=",this->m_kqueue.size(),"]");
    return  LDNDC_ERR_OK;
}
lerr_t
cbm::kspatial_t::enqueue_for_umount(
            cbm::string_t const & /*uri*/, cbm::kernel_t * /*client*/)
{
    CBM_LogError( "kspatial_t::enqueue_for_umount(): not implemented");
    return  LDNDC_ERR_OK;
}

cbm::services_t
cbm::kspatial_t::resolve_services(
        cbm::entities_t const &  _req_ents,
        cbm::services_t const &  _req_serv, int *  _rc)
{
    /* primary requirements */
    cbm::entities_t  V = _req_ents;
    /* explicitely requested services to resolve ambiguity */
    cbm::services_t  S = _req_serv;

    cbm::workflow_compositer_t  wfc =
        cbm::workflow_compositer_t( &this->m_servreg);

    return  wfc.resolve( _req_ents, _req_serv, _rc);
}

lerr_t
cbm::kspatial_t::configure( cbm::RunLevelArgs *  _args)
{
    cbm::string_t  service_registry_url =
        _args->cfg->service_registry_url();
    service_registry_url.format_expand();

    CBM_LogDebug( "connecting to service registry",
            "  [url=",service_registry_url,"]");
    lerr_t  rc_regconn =
        this->m_servreg.connect( service_registry_url.c_str());
    if ( rc_regconn)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kspatial_t::initialize( cbm::RunLevelArgs *  _args)
{
    this->m_iokcomm = _args->iokcomm;

    /* add myself as domain root */
    knode_t  knode;
    knode.kernel = this;
    knode.iokcomm = this->m_iokcomm;
    knode.kcfg = NULL;
    knode.flags = IsCreated|IsInitialized;

    knode.parent = NULL;
    knode.sibling = NULL;
    knode.child = NULL;

    knode.next = NULL;

    this->m_knodes.insert_node( NULL, &knode);

    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kspatial_t::read( cbm::RunLevelArgs *)
{
    lerr_t  rc_regrid = this->m_regrid();
    if ( rc_regrid)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::kspatial_t::m_regrid()
{
    if ( this->m_kqueue.is_empty())
        { return  LDNDC_ERR_OK; }

    CBM_LogDebug( "regridding..  [#requests=",this->m_kqueue.size(),"]");
    /* create kernels in m_kqueue
     *  and call their configuration routine */
    lerr_t  rc_kernels_create = this->kernels_create();
    if ( rc_kernels_create)
    {
        this->finalize( NULL);
        CBM_LogError( "region construction failed");
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_kernels_ioports = this->kernels_registerports();
    if ( rc_kernels_ioports)
    {
        this->finalize( NULL);
        CBM_LogError( "I/O port query failed");
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_kernels_init = this->kernels_initialize();
    if ( rc_kernels_init)
    {
        this->finalize( NULL);
        CBM_LogError( "region initialization failed");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
cbm::kspatial_t::finalize( cbm::RunLevelArgs *)
{
    lerr_t  rc = LDNDC_ERR_OK;
    lerr_t  rc_fin = this->kernels_finalize();
    if ( rc_fin)
        { rc = LDNDC_ERR_FAIL; }

    lerr_t  rc_unreg = this->kernels_unregisterports();
    if ( rc_unreg)
        { rc = LDNDC_ERR_FAIL; }
    return  rc;
}

void
cbm::kspatial_t::deactivate()
{
    for ( cbm::knodes_t::iterator  ni = this->m_knodes.begin();
            ni != this->m_knodes.end();  ++ni)
    {
        CBM_Assert( *ni != NULL);
        kernel_t *  a_kernel = (*ni)->kernel;
        if ( a_kernel == this)
            { continue; /* recursion! */ }
        if ( a_kernel && a_kernel->is_active())
            { this->deactivate_kernel( **ni); }
    }
    kernel_t::deactivate();
}


static lerr_t  CBM_DomainRootKernelConfigure(
        cbm::kqueue_data_t const * _kdesc, cbm::knode_t * _knode)
{
    CBM_Assert( _knode);
    CBM_Assert( _knode->kernel);
    CBM_Assert( _knode->kernel->rank() >= 0);

    cbm::RunLevelArgs  runlevel_args = CBM_MakeRunLevelArgs(
                cbm::RL_CONFIGURE, _knode, &_kdesc->mountargs);
    lerr_t  rc_kernel_conf =
        _knode->kernel->execute_runlevel( &runlevel_args);
    if ( _knode->iokcomm->state.ok
            && ( rc_kernel_conf == LDNDC_ERR_OK))
    {
        CBM_LogVerbose( "Successfully configured kernel",
                "  [uri=",_knode->kernel->uri().str(),"]");
        /* configure successful */
        _knode->flags |= IsCreated;
    }
    else
    {
        cbm::RunLevelArgs  ra_fin =
            CBM_MakeRunLevelArgs( cbm::RL_FINALIZE, _knode);
        _knode->kernel->execute_runlevel( &ra_fin);
        /* configure failed */
        _knode->flags &= ~IsCreated;

        return  LDNDC_ERR_FAIL;
    }
    CBM_LibRuntimeConfig.sc.current_kernel[0] = -1;

    return  LDNDC_ERR_OK;
}


lerr_t
cbm::kspatial_t::kernels_create()
{
    CBM_LogVerbose( "mounting queued kernels",
            "  [#requests=",this->m_kqueue.size(),"]");

    while ( ! this->m_kqueue.is_empty())
    {
        kqueue_data_t *  kdesc = this->m_kqueue.front();

        CBM_Assert( kdesc->kclient);
        /* check if parent has meanwhile disappeared */
        knode_t *  kparent_node = this->m_knodes.find_node( kdesc->kclient_uri);
        if ( kparent_node && kparent_node->kernel == kdesc->kclient)
            { /* all ok */ }
        else
        {
            CBM_LogError( "Parent kernel has disappeared,",
                    " which causes me much grief :|");
            return LDNDC_ERR_RUNTIME_ERROR;
        }

        knode_t  knode;
        lerr_t  rc_create = this->kernel_create( kdesc, &knode);
        if ( rc_create)
        {
            CBM_LogError( "Drop remaining queued kernels");
            this->m_kqueue.clear();
            return  LDNDC_ERR_FAIL;
        }
        else
        {
            CBM_LogDebug( "name=",kdesc->kname, " bid=",kdesc->kid, " sid=",kdesc->ksource);

            CBM_Assert( kparent_node);
            knode_t *  new_knode = this->m_knodes.insert_node( kparent_node, &knode);
            if ( !new_knode)
                { return LDNDC_ERR_RUNTIME_ERROR; }
            CBM_StealKCfgMemory( new_knode, kdesc);

            lerr_t  rc_conf = CBM_DomainRootKernelConfigure( kdesc, new_knode);
            if ( rc_conf)
            {
                CBM_Assert( new_knode->kernel);
                cbm::string_t const  kuri = new_knode->kernel->uri().str();

                CBM_Assert( new_knode->kernel);
                new_knode->kernel->delete_instance();
                new_knode->kernel = NULL;

                CBM_Assert( new_knode->iokcomm);
                new_knode->iokcomm->delete_instance();
                new_knode->iokcomm = NULL;

                CBM_ReturnKCfgMemory( kdesc, new_knode);

                this->m_knodes.remove_node( new_knode, kuri);
                this->m_kqueue.clear();
                return LDNDC_ERR_RUNTIME_ERROR;
            }
        }

        this->m_kqueue.pop_front();
    }
    return  LDNDC_ERR_OK;
}

static lerr_t  CBM_DomainRootSetIOService( cbm::kqueue_data_t const *  _kdesc,
        cbm::knode_t *  _knode, cbm::knode_t *  _client_knode,
            char const *  _modelcompositor)
{
    if ( _kdesc->kname == _modelcompositor)
    {
        if ( _kdesc->kid == invalid_lid)
            { return LDNDC_ERR_RUNTIME_ERROR; }
        cbm::kdesc_t  kdesc;
        cbm::set_source_descriptor( &kdesc,
                    _kdesc->kid, _kdesc->ksource.c_str());
        _knode->iokcomm->set_io_service(
                    _client_knode->iokcomm, &kdesc);
    }
    else
    {
        if ( _client_knode)
        {
            _knode->iokcomm->set_io_service( _client_knode->iokcomm);
    /*TODO work-around to have shared scratch*/
//            _knode->iokcomm->set_scratch( _client_knode->iokcomm->get_scratch());
        }
        else
            { return  LDNDC_ERR_RUNTIME_ERROR; }
    }
    return  LDNDC_ERR_OK;
}
static int  CBM_DomainRootFindRank( std::map< std::string, int > &  _ranks,
        cbm::knode_t const *  _knode)
{
    CBM_Assert( _knode && _knode->kernel);

    std::string const  k_id = _knode->kernel->ID();
    if ( _ranks.find( k_id) == _ranks.end())
        { _ranks[k_id] = 0; }

    int const  k_rank = _ranks[k_id];
    _ranks[k_id] = k_rank+1;    /*NOTE no reusing of rank IDs, overflow possible but unlikely*/
    return  k_rank;
}


lerr_t
cbm::kspatial_t::kernel_create(
    kqueue_data_t const *  _kdesc, knode_t *  _knode)
{
    CBM_Assert( _kdesc->kclient);
    cbm::uri_t  k_uri = uri_generate_t::generate( _kdesc, _kdesc->kclient);
    if ( this->m_knodes.find_node( k_uri.str()))
    {
        CBM_LogError( "kernel with URI exists  [uri=", k_uri.str(),"]");
        return  LDNDC_ERR_FAIL;
    }

    /* current kernel's id */
    lid_t  kernel_object_id = _kdesc->kid;
    if ( kernel_object_id == invalid_lid)
        { kernel_object_id = _kdesc->kclient->object_id(); }

    _knode->flags = 0;
    _knode->kernel = NULL;
    _knode->iokcomm = io_kcomm_t::new_instance( kernel_object_id);
    if ( !_knode->iokcomm)
        { return  LDNDC_ERR_NOMEM; }
    _knode->kcfg = NULL;

    knode_t *  client_knode =
        this->m_knodes.find_node( _kdesc->kclient->uri().str());
    lerr_t  rc = CBM_DomainRootSetIOService( _kdesc, _knode, client_knode,
                this->m_modelcompositor.c_str());
    if ( rc)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    char const *  k_name = _kdesc->kname.c_str();
    if ( cbm::is_empty( k_name)
            || cbm::is_invalid( k_name))
        { return  LDNDC_ERR_FAIL; }

    int  m_exists = 0;
    int  m_constructed = 0;

    _knode->kernel = CBM_LibRuntimeConfig.kfactory->construct_kernel(
                            k_name, kernel_object_id, &m_exists);
    if ( _knode->kernel)
        { m_constructed = 1; }

    if ( !m_exists)
    {
        CBM_LogError( "kernel unknown  [id=",kernel_object_id,",kernel=",k_name,"]");
        _knode->iokcomm->delete_instance();
        _knode->iokcomm = NULL;
        return  LDNDC_ERR_FAIL;
    }
    else if ( !m_constructed)
    {
        CBM_LogError( "instantiating kernel failed  [id=",kernel_object_id,"]");
        _knode->iokcomm->delete_instance();
        _knode->iokcomm = NULL;
        return  LDNDC_ERR_FAIL;
    }
    else
        { /*all ok*/ }
    _knode->flags |= IsCreated;

    CBM_Assert( _knode->kernel);

    CBM_LibRuntimeConfig.sc.current_kernel[cbm::omp::get_thread_num()]
            = _knode->kernel->object_id();

    _knode->kernel->uri( k_uri);
    int const  k_rank = CBM_DomainRootFindRank( this->m_ranks, _knode);
    if ( k_rank < 0)
        { return  LDNDC_ERR_FAIL; }
    _knode->kernel->rank( k_rank);

    return  LDNDC_ERR_OK;
}

#define  RankZeroAtHead 0
#define  RankZeroAtTail 1
lerr_t
cbm::kspatial_t::kernels_registerports()
    { return this->m_kernelsrunlevel( RL_REGISTERPORTS, IsRegisteredPorts, RankZeroAtHead); }
lerr_t
cbm::kspatial_t::kernels_initialize()
    { return this->m_kernelsrunlevel( RL_INITIALIZE, IsInitialized, RankZeroAtHead); }
lerr_t
cbm::kspatial_t::kernels_finalize()
    { return this->m_kernelsrunlevel( RL_FINALIZE, IsFinalized, RankZeroAtTail); }
lerr_t
cbm::kspatial_t::kernels_unregisterports()
    { return this->m_kernelsrunlevel( RL_UNREGISTERPORTS, IsUnregisteredPorts, RankZeroAtTail); }


static cbm::knode_t *  CBM_DomainRootKernelCallOrder( int _flag, int _order,
                cbm::knodes_t * _knodes, cbm::kernel_t * _kspatial)
{
    cbm::knode_t *  head_0 = NULL;
    cbm::knode_t *  node_0 = NULL;
    cbm::knode_t *  head_x = NULL;
    cbm::knode_t *  node_x = NULL;

    /* construct initial list */
    for ( cbm::knodes_t::iterator  n = _knodes->begin();
            n != _knodes->end();  ++n)
    {
        cbm::knode_t *  knode = *n;
        knode->next = NULL;
        if (( knode->flags&IsBad) || ( knode->flags&_flag)
            || ( knode->kernel==_kspatial) || ( !knode->kernel))
        { }
        else if ( knode->kernel->rank() == 0)
        {
            if ( head_0 == NULL)
                { head_0 = knode; }
            else
                { node_0->next = knode; }
            node_0 = knode;
        }
        else
        {
            if ( head_x == NULL)
                { head_x = knode; }
            else
                { node_x->next = knode; }
            node_x = knode;
        }
    }

    if ( head_0 && !head_x)
        { return head_0; }
    else if ( !head_0 && head_x)
        { return head_x; }
    else if (( _order == RankZeroAtHead) && node_0)
    {
        node_0->next = head_x;
        return head_0;
    }
    else if (( _order == RankZeroAtTail) && node_x)
    {
        node_x->next = head_0;
        return head_x;
    }
    else
        { /* well, better check your code :| */ }
    return NULL;
}
lerr_t
cbm::kspatial_t::m_kernelsrunlevel( cbm::RunLevel _rl, int _flag, int _order)
{
    lerr_t  rc = LDNDC_ERR_OK;
    knode_t *  head = CBM_DomainRootKernelCallOrder(
                    _flag, _order, &this->m_knodes, this);
    while ( head )
    {
        knode_t *  recall_head = NULL;
        lerr_t rc_rl = this->m_kernelsrunlevel(
                        _rl, _flag, head, &recall_head);
        if ( rc_rl)
        {
            /* clear linked list */
            for ( cbm::knodes_t::iterator  n = this->m_knodes.begin();
                n != this->m_knodes.end();  ++n)
            {
                *n;
                if ( *n )
                    { (*n)->next = NULL; }
            }
            head = NULL;
            rc = LDNDC_ERR_FAIL;
        }
        else
            { head = recall_head; }
    }
    return rc;
}
lerr_t
cbm::kspatial_t::m_kernelsrunlevel(
            cbm::RunLevel _rl, int _flag,
                knode_t * _head, knode_t ** _recall_head)
{
    int  c_bad = 0;

    knode_t *  knode = _head;
    knode_t *  rnode = NULL;
    while ( !c_bad && knode )
    {
        knode_t *  nnode = knode->next;
        knode->next = NULL;

        kernel_t *  a_kernel = knode->kernel;
        if ( a_kernel)
        {
            io_kcomm_t *  a_iokcomm = knode->iokcomm;
            if ( !a_iokcomm)
            {
                CBM_LogError( "A kernel without communicator; that makes me so sad :(");
                return  LDNDC_ERR_RUNTIME_ERROR;
            }

            RunLevelArgs  runlevel_args = CBM_MakeRunLevelArgs( _rl, knode);
            lerr_t  rc = LDNDC_ERR_FAIL;
            try
            {
                rc = a_kernel->execute_runlevel( &runlevel_args);
            }
            catch ( std::runtime_error &  err)
            {
                CBM_LogError( err.what() );
                rc = LDNDC_ERR_FAIL;
            }
            if ( a_iokcomm->state.ok && ( rc == LDNDC_ERR_OK))
            {
                if ( runlevel_args.nextcall_rl == _rl)
                {
                    /* enqueue for recall (preserves call order) */
                    if ( *_recall_head == NULL)
                        { *_recall_head = knode; }
                    else
                        { rnode->next = knode; }
                    rnode = knode;
                }
                else
                {
                    knode->flags |= _flag; /* all good */

                    if ( runlevel_args.nextcall_rl != RL_NONE)
                    {
                        CBM_LogWarn( "Request for run-level [",
                                runlevel_args.nextcall_rl,"] has been ignored. ",
                            "Currently only continuing initialization is supported.");
                    }
                }
            }
            else
            {
                /* count number of bad kernels */
                ++c_bad;

                knode->flags |= IsBad;
            }
        }
        else
            { crabmeat_assert( !knode->iokcomm); }

        knode = nnode;
    }
    return  ( c_bad > 0) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

lerr_t
cbm::kspatial_t::kernels_destroy()
{
    knode_t *  knode = CBM_DomainRootKernelCallOrder(
            IsDestroyed, RankZeroAtTail, &this->m_knodes, this);
    while ( knode )
    {
        if ( knode->kernel && ( knode->kernel != this))
        {
            if ( knode->kernel)
            {
                knode->kernel->delete_instance();
                knode->kernel = NULL;
            }
            knode->flags = 0;
        }
        knode = knode->next;
    }
    return LDNDC_ERR_OK;
}


void
cbm::kspatial_t::deactivate_kernel( knode_t &  _knode)
{
    CBM_LogWarn( "deactivate_kernel(): [", CBM_LibRuntimeConfig.clk->now(),"]",
        " deactivating kernel", "  [id=",_knode.kernel->uri().str(), "]");
    _knode.kernel->deactivate();

    CBM_LogError( "[TODO] Unchain from execution list");
    CBM_LogError( "[TODO] Deactivate kernels in subtree");

    if ( this->number_of_alive_kernels() == 0)
        { kernel_t::deactivate(); }
}

cbm::kernel_t *
cbm::kspatial_t::kernel_by_uri(
        cbm::uri_t const &  _uri)
{
    knode_t *  knode = this->m_knodes.find_node( _uri.str());
    if ( knode)
        { return knode->kernel; }
    return NULL;
}
cbm::kernel_t const *
cbm::kspatial_t::kernel_by_uri(
        cbm::uri_t const &  _uri)
const
{
    knode_t const *  knode = this->m_knodes.find_node( _uri.str());
    if ( knode)
        { return knode->kernel; }
    return NULL;
}

size_t
cbm::kspatial_t::number_of_kernels() const
    { return this->m_knodes.size(); }
size_t
cbm::kspatial_t::number_of_alive_kernels() const
{
    size_t  v = this->number_of_kernels();
    for ( cbm::knodes_t::const_iterator  ni = this->m_knodes.cbegin();
            ni != this->m_knodes.cend();  ++ni)
    {
        if (( ! (*ni)->kernel)
                || ( ! (*ni)->kernel->is_active()))
            { --v; }
    }
    return  v;
}
size_t
cbm::kspatial_t::number_of_valid_kernels() const
{
    size_t  v = this->number_of_kernels();
    for ( cbm::knodes_t::const_iterator  ni = this->m_knodes.cbegin();
            ni != this->m_knodes.cend();  ++ni)
    {
        if ( ! (*ni)->kernel)
            { --v; }
    }
    return  v;
}

