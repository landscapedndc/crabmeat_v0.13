/*!
 * @author
 *    steffen klatt  (created on: feb 15, 2014),
 *    edwin haas
 */

#include  "kernel/kscratch.h"
#include  "time/cbm_time.h"

bool const cbm::state_scratch_invalid_t< bool >::value = false;

static int  s_gc_callback( void * _obj, cbm::sclock_t const * _clk)
{
    lerr_t  rc = LDNDC_ERR_FAIL;

    cbm::state_scratch_t *  scratchpad =
        static_cast< cbm::state_scratch_t * >( _obj);
    if ( scratchpad)
    {
        //CBM_LogVerbose( "called garbage collector callback at timestep ", _sse);
        rc = scratchpad->run_garbage_collector( _clk->sse());
    }
    return  rc ? -1 : 0;
}

lerr_t
cbm::state_scratch_t::initialize(
                cbm::sclock_t const *  _lclock)
{
    if ( _lclock)
    {
        this->T_expire = _lclock->sse();
        int  rc = _lclock->register_event( "on-enter-timestep",
                    &s_gc_callback, this, "scratch");
        if ( rc == 0)
        {
            return  LDNDC_ERR_OK;
        }
    }
    return  LDNDC_ERR_FAIL;
}
lerr_t
cbm::state_scratch_t::finalize(
                cbm::sclock_t const *  _lclock)
{
    this->clear();
    if ( _lclock)
    {
        _lclock->unregister_event( "on-enter-timestep",
            &s_gc_callback, this, "scratch");
    }
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::state_scratch_t::run_garbage_collector(
        cbm::td_scalar_t  _sse)
{
    this->T_expire = _sse;
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::state_scratch_t::expire_list_enqueue(
            char const *  /*_key*/, cbm::td_scalar_t  /*_expire_time*/)
{
    if ( this->T_expire == -1)
        { return  LDNDC_ERR_FAIL; }

    return  LDNDC_ERR_FAIL;
}

lerr_t
cbm::state_scratch_t::expire_list_remove(
            cbm::td_scalar_t  /*_expire_time*/)
{
    return  LDNDC_ERR_OK;
}

void
cbm::state_scratch_t::clear()
{
    this->ht.clear();
}

bool
cbm::state_scratch_t::variable_exists(
        char const *  _key)
const
{
    return  this->ht.fetch( _key, -1, NULL, -1) == LDNDC_ERR_OK;
}

void
cbm::state_scratch_t::remove(
        char const *  _key)
{
    this->ht.remove( _key);
}

void
cbm::state_scratch_t::dump()
const
{
    this->ht.dump();
}

