/*!
 * @author
 *    steffen klatt (created on: dec 01, 2016)
 */

#include  "kernel/kbase.h"
#include  "kernel/io-kcomm.h"
#include  "log/cbm_baselog.h"

cbm::CBM_KernelTask::~CBM_KernelTask()
{ }

static cbm::CBM_KernelTaskConfigure  _CBM_KernelTaskConfigure;
lerr_t  cbm::CBM_KernelTaskConfigure::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->configure( _args); }

static cbm::CBM_KernelTaskRegisterPorts  _CBM_KernelTaskRegisterPorts;
lerr_t  cbm::CBM_KernelTaskRegisterPorts::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->register_ports( _args); }
static cbm::CBM_KernelTaskInitialize  _CBM_KernelTaskInitialize;
lerr_t  cbm::CBM_KernelTaskInitialize::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->initialize( _args); }


static cbm::CBM_KernelTaskRead  _CBM_KernelTaskRead;
lerr_t  cbm::CBM_KernelTaskRead::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->read( _args); }
static cbm::CBM_KernelTaskSolve  _CBM_KernelTaskSolve;
lerr_t  cbm::CBM_KernelTaskSolve::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->solve( _args); }
static cbm::CBM_KernelTaskIntegrate  _CBM_KernelTaskIntegrate;
lerr_t  cbm::CBM_KernelTaskIntegrate::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->integrate( _args); }
static cbm::CBM_KernelTaskWrite  _CBM_KernelTaskWrite;
lerr_t  cbm::CBM_KernelTaskWrite::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->write( _args); }


static cbm::CBM_KernelTaskFinalize  _CBM_KernelTaskFinalize;
lerr_t  cbm::CBM_KernelTaskFinalize::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->finalize( _args); }
static cbm::CBM_KernelTaskUnregisterPorts  _CBM_KernelTaskUnregisterPorts;
lerr_t  cbm::CBM_KernelTaskUnregisterPorts::execute(
            cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
    { return _kernel->unregister_ports( _args); }

cbm::kernel_t::kernel_t()
    : cbm::kernel_object_t(), m_status( 0), m_rank( -1)
{
    for ( int r = 0; r < RL_ALL; ++r)
    {
        this->pre_tasks[r] = NULL;
        this->tasks[r] = NULL;
        this->post_tasks[r] = NULL;
    }
    this->pre_tasks[RL_CONFIGURE] = &kernel_t::pre_configure;
    this->set_runlevel_task( RL_CONFIGURE, &_CBM_KernelTaskConfigure);
    this->post_tasks[RL_CONFIGURE] = &kernel_t::post_configure;

    this->pre_tasks[RL_REGISTERPORTS] = &kernel_t::pre_register_ports;
    this->set_runlevel_task( RL_REGISTERPORTS, &_CBM_KernelTaskRegisterPorts);
    this->post_tasks[RL_REGISTERPORTS] = &kernel_t::post_register_ports;

    this->pre_tasks[RL_INITIALIZE] = &kernel_t::pre_initialize;
    this->set_runlevel_task( RL_INITIALIZE, &_CBM_KernelTaskInitialize);
    this->post_tasks[RL_INITIALIZE] = &kernel_t::post_initialize;

    this->pre_tasks[RL_READ] = &kernel_t::pre_read;
    this->set_runlevel_task( RL_READ, &_CBM_KernelTaskRead);
    this->post_tasks[RL_READ] = &kernel_t::post_read;

    this->pre_tasks[RL_SOLVE] = &kernel_t::pre_solve;
    this->set_runlevel_task( RL_SOLVE, &_CBM_KernelTaskSolve);
    this->post_tasks[RL_SOLVE] = &kernel_t::post_solve;

    this->pre_tasks[RL_INTEGRATE] = &kernel_t::pre_integrate;
    this->set_runlevel_task( RL_INTEGRATE, &_CBM_KernelTaskIntegrate);
    this->post_tasks[RL_INTEGRATE] = &kernel_t::post_integrate;

    this->pre_tasks[RL_WRITE] = &kernel_t::pre_write;
    this->set_runlevel_task( RL_WRITE, &_CBM_KernelTaskWrite);
    this->post_tasks[RL_WRITE] = &kernel_t::post_write;

    this->pre_tasks[RL_FINALIZE] = &kernel_t::pre_finalize;
    this->set_runlevel_task( RL_FINALIZE, &_CBM_KernelTaskFinalize);
    this->post_tasks[RL_FINALIZE] = &kernel_t::post_finalize;

    this->pre_tasks[RL_UNREGISTERPORTS] = &kernel_t::pre_unregister_ports;
    this->set_runlevel_task( RL_UNREGISTERPORTS, &_CBM_KernelTaskUnregisterPorts);
    this->post_tasks[RL_UNREGISTERPORTS] = &kernel_t::post_unregister_ports;
}

cbm::kernel_t::~kernel_t()
{ }

void  cbm::kernel_t::set_runlevel_task(
        RunLevel _rl, CBM_KernelTask * _task)
{
    CBM_Assert( !_task || ( _rl==_task->runlevel()));
    this->tasks[_rl] = _task;
}

lerr_t  cbm::kernel_t::execute_runlevel( RunLevelArgs * _args)
{
    lerr_t rc_task = LDNDC_ERR_OK;

    rc_task = this->pre_execute_runlevel( _args);
    if ( rc_task)
        { return rc_task; }
    if ( this->tasks[_args->rl])
    {
        rc_task = this->tasks[_args->rl]->execute( this, _args);
        if ( rc_task)
            { return rc_task; }
    }
    rc_task = this->post_execute_runlevel( _args);
    if ( rc_task)
        { return rc_task; }
    return LDNDC_ERR_OK;
}

lerr_t  cbm::kernel_t::pre_execute_runlevel( RunLevelArgs * _args)
{
    if ( this->pre_tasks[_args->rl])
        { return (this->*pre_tasks[_args->rl])( _args); }
    return LDNDC_ERR_OK;
}
lerr_t  cbm::kernel_t::post_execute_runlevel( RunLevelArgs * _args)
{
    if ( this->post_tasks[_args->rl])
        { return (this->*post_tasks[_args->rl])( _args); }
    return LDNDC_ERR_OK;
}

cbm::ksetup_t  cbm::kernel_t::setup_parser(
        char const *  _setup, char const *  _kernel_id)
{
    char const *  kernel_id = _kernel_id;
    if ( !kernel_id || cbm::is_empty( kernel_id))
        { kernel_id = this->ID(); }
    cbm::string_t  kId;
    if ( kernel_id[0] != '/')
        { kId = "/"; }
    kId << kernel_id;
    return cbm::ksetup_t( kId.c_str(), _setup);
}

#include  "json/cbm_jquery.h"
#include  "time/cbm_time.h"
lerr_t  cbm::kernel_t::pre_configure( RunLevelArgs * _args)
{
    _args->dT.unit = '-';
    if ( _args->kcfg)
    {
        cbm::jquery_t  kcfg( _args->kcfg);
        char const *  dt = kcfg.query_string( "/dt", NULL);
        if ( dt)
        {
            _args->dT = cbm::resolve_dt( dt);
            if ( _args->dT.q <= 0)
            {
                CBM_LogError( "Error resolving dT [",dt,"] for kernel ", this->ID());
                return LDNDC_ERR_FAIL;
            }
// sk:dbg            CBM_LogVerbose( "dt=",_args->clk->dt(), "  dT=",kcfg.query_string( "/dt", "0"),
// sk:dbg                    "  *dT=",_args->dT.q, "  unit=",_args->dT.unit);
        }
    }
    if ( _args->dT.unit == '-')
    {
        _args->dT.q = _args->clk->dt();
        _args->dT.unit = 's';
    }
    _args->nextcall_dT = _args->dT;
    _args->nextcall_sse = -1; /*invalidate, i.e., use dT*/

    return LDNDC_ERR_OK;
}
lerr_t  cbm::kernel_t::post_configure( RunLevelArgs * _args)
{
    if ( _args->iokcomm)
    {
        ksched_t ksched = _args->iokcomm->temporal_controller( this);
        ksched.register_for_execution( this, _args);
    }
    return LDNDC_ERR_OK;
}

lerr_t  cbm::kernel_t::pre_register_ports( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_register_ports( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t  cbm::kernel_t::pre_initialize( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_initialize( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t  cbm::kernel_t::pre_read( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_read( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t  cbm::kernel_t::pre_solve( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_solve( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t  cbm::kernel_t::pre_integrate( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_integrate( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t  cbm::kernel_t::pre_write( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_write( RunLevelArgs * /*_args*/)
{
//    _args->nextcall_sse = cbm::add_dt( _args->sse, &_args->dT);
    return LDNDC_ERR_OK;
}

lerr_t  cbm::kernel_t::pre_finalize( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_finalize( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t  cbm::kernel_t::pre_unregister_ports( RunLevelArgs *)
    { return LDNDC_ERR_OK; }
lerr_t  cbm::kernel_t::post_unregister_ports( RunLevelArgs *)
    { return LDNDC_ERR_OK; }

