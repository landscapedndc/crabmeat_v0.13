/*!
 * @author
 *    steffen klatt (created on: apr 14, 2017)
 */

#include  "kernel/ktemporal.h"
#include  "kernel/io-kcomm.h"

#include  "time/cbm_time.h"
#include  "log/cbm_baselog.h"
#include  "cbm_rtcfg.h"

CBM_KERNEL_OBJECT_DEFN(cbm,ktemporal_t,__scheduler__,"__scheduler__","Scheduler")
cbm::ktemporal_t::ktemporal_t()
        : cbm::kernel_t(), m_cntr( 0)
    { }

cbm::ktemporal_t::~ktemporal_t()
    { }

#define SimClock (CBM_LibRuntimeConfig.clk)
#define RequestToTerminate CBM_LibRuntimeConfig.sc.request_term
lerr_t
cbm::ktemporal_t::launch( cbm::td_scalar_t _sse_stop)
{
    RunLevelArgs ra( RL_LoopStart);
    ra.iokcomm = NULL;
    ra.cfg = NULL;
    ra.clk = SimClock;
    ra.nextcall_sse = _sse_stop;

    lerr_t rc_launch = this->m_Launch( &ra);
    if ( rc_launch)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
cbm::ktemporal_t::configure( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
cbm::ktemporal_t::initialize( cbm::RunLevelArgs *)
    { return LDNDC_ERR_OK; }

lerr_t
cbm::ktemporal_t::write( cbm::RunLevelArgs * _args)
{
    _args->flags.terminate = 1; /*flags to return from execution, e.g. terminate simulation*/
    return LDNDC_ERR_OK;
}

lerr_t
cbm::ktemporal_t::register_for_execution(
        cbm::kernel_t * _kernel, cbm::RunLevelArgs * _args)
{
    /* push new node into queue */
    KSchedNode knode;
    knode.kernel = _kernel;
    knode.args = *_args;
    if ( knode.args.nextcall_rl == RL_NONE)
        { knode.args.nextcall_rl = RL_LoopStart; }
    if ( knode.args.nextcall_sse == -1)
        { knode.args.nextcall_sse = knode.args.sse; } //cbm::add_dt( knode.args.sse, &knode.args.dT); }
    if ( knode.args.nextcall_dT.q == -1 || knode.args.nextcall_dT.unit == '-')
        { knode.args.nextcall_dT = knode.args.dT; }

    this->m_InsertInSchedule( &knode);
    return  LDNDC_ERR_OK;
}

lerr_t
cbm::ktemporal_t::m_Launch( cbm::RunLevelArgs * _args)
{
    lerr_t rc_insertself = this->m_ScheduleSelf( _args);
    if ( rc_insertself)
        { return LDNDC_ERR_RUNTIME_ERROR; }

    td_scalar_t sse_now = SimClock->sse();

    while ( !this->m_prioq.empty() && !RequestToTerminate)
    {
        KSchedNode  knode = this->m_PopFromSchedule();

        /* advance clock by minimum time step */
        if ( sse_now < knode.args.nextcall_sse)
        {
            SimClock->advance( knode.args.nextcall_sse-sse_now/*min_dt*/);
            sse_now = knode.args.nextcall_sse;
        }

        /* execute runlevel for kernel */
        if ( knode.args.flags.skip == 0)
        {
            CBM_Assert( knode.args.nextcall_rl>=RL_LoopStart && knode.args.nextcall_rl<=RL_LoopEnd);

            lerr_t rc_exec = this->m_ExecuteKernel( &knode);
            if ( rc_exec) /* do NOT continue without it */
                { return LDNDC_ERR_FAIL; }
        }
        this->m_UpdateKSchedNode( &knode);

        /* check status flags */
        if ( knode.args.flags.terminate)
            { return LDNDC_ERR_OK; }
        if ( knode.args.flags.drop)
            { continue; }
        if ( knode.args.flags.error)
            { return LDNDC_ERR_RUNTIME_ERROR; }

        /* re-insert into queue */
        lerr_t rc_insert = this->m_InsertInSchedule( &knode);
        if ( rc_insert)
            { return LDNDC_ERR_RUNTIME_ERROR; }
    }
    return LDNDC_ERR_OK;
}

lerr_t
cbm::ktemporal_t::m_ScheduleSelf(
               cbm::RunLevelArgs const * _args)
{
    /* insert into queue; schedule for given sse */
    KSchedNode knode;
    knode.kernel = this;
    knode.args = *_args;
    knode.args.nextcall_rl = RL_LoopEnd;
    knode.args.flags.prio = -7; /*execute *after* other kernels during timestep and runlevel*/

    lerr_t rc_insert = this->m_InsertInSchedule( &knode);
    if ( rc_insert)
        { return LDNDC_ERR_FAIL; }
    return LDNDC_ERR_OK;
}

lerr_t
cbm::ktemporal_t::m_InsertInSchedule(
                cbm::KSchedNode * _knode)
{
    this->m_cntr += 1;
    _knode->cntr = this->m_cntr;
    this->m_prioq.push( *_knode);
    return LDNDC_ERR_OK;
}
cbm::KSchedNode
cbm::ktemporal_t::m_PopFromSchedule()
{
    KSchedNode  knode = this->m_prioq.top();
    this->m_prioq.pop();
    return knode;
}

lerr_t
cbm::ktemporal_t::m_ExecuteKernel(
               cbm::KSchedNode * _knode) const
{
    kernel_t *  kernel = _knode->kernel;
#ifdef  CRABMEAT_KERNEL_STATISTICS
    kernel->stats.tic();
#endif
    RunLevelArgs * ra = &_knode->args;

    ra->rl = ra->nextcall_rl;
    ra->nextcall_rl = RL_NONE;

    ra->sse = ra->clk->sse(); /*strictly ra->nextcall_sse*/
    ra->nextcall_sse = -1;

    ra->dT = ra->nextcall_dT;
    ra->nextcall_dT.q = -1;
    ra->nextcall_dT.unit = '-';

    lerr_t rc_exec = kernel->execute_runlevel( ra);

    /* check feed-back from kernel */
    if ( ra->nextcall_rl == RL_NONE)
    {
        /* set default runlevel for next call */
        if ( ra->rl == RL_LoopEnd)
            { ra->nextcall_rl = RL_LoopStart; }
        else
            { ra->nextcall_rl = static_cast<RunLevel>(
                    static_cast<int>( ra->rl) + 1); }
    }
    else
    {
        CBM_Assert( ra->nextcall_rl>=RL_LoopStart);
        CBM_Assert( ra->nextcall_rl<=RL_LoopEnd);
    }

    if ( (ra->nextcall_sse == -1) && ( ra->rl < ra->nextcall_rl))
    {
        /* remain in current timestep, when runlevel did not "wrap around" */
        ra->nextcall_sse = ra->sse;
        ra->nextcall_dT = ra->dT;
    }
    else if ( ra->nextcall_sse != -1) /*has precedence over dT but only temporarily*/
        { /*no op*/ }
    else if ( ra->nextcall_dT.q != -1 && ra->nextcall_dT.unit != '-')
        { ra->nextcall_sse = cbm::add_dt( ra->sse, &ra->nextcall_dT); }
    else
    {
        ra->nextcall_sse = cbm::add_dt( ra->sse, &ra->dT);
        ra->nextcall_dT = ra->dT;
    }

#ifdef  CRABMEAT_KERNEL_STATISTICS
    kernel->stats.toc();
#endif
    return rc_exec;
}

void
cbm::ktemporal_t::m_UpdateKSchedNode(
                cbm::KSchedNode * _knode) const
{
    _knode->args.flags.skip = 0;
}

