/*!
 * @brief
 *    interface to inter-kernel communicator (implementation)
 *
 * @author
 *    steffen klatt (created on: jan 26, 2015)
 */

#include  "kernel/kcomm.h"
#include  "utils/cbm_utils.h"

lerr_t
cbm::kcomm_t::register_message_handler(
        message_handler_function_t  _callback, void *  _obj)
{
    CBM_LogDebug( "register message handler for \"",this->uri(), "\"");
    this->msgbrk_id =
        cbm::kmessage_broker->register_message_handler( this->uri(), _callback, _obj);
    if ( cbm::is_valid( this->msgbrk_id))
        { return  LDNDC_ERR_OK; }
    return  LDNDC_ERR_FAIL;
}
void
cbm::kcomm_t::deregister_message_handler()
{
	if ( cbm::is_valid( this->msgbrk_id))
	{
        cbm::kmessage_broker->deregister_message_handler( this->msgbrk_id);
		this->msgbrk_id = invalid_lid;
	}
}
lerr_t
cbm::kcomm_t::send_message(
        cbm::msg_t *  _message, cbm::reply_t *  _reply)
{
    _message->sender = &this->m_uri;
    return  cbm::kmessage_broker->send_message( _message, _reply);
}
lerr_t
cbm::kcomm_t::bcast_message(
        cbm::msg_t *  _message, int *  _n_receivers)
{
    _message->sender = &this->m_uri;
    return  cbm::kmessage_broker->bcast_message( _message, _n_receivers);
}

