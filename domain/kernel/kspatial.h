/*!
 * @brief
 *    Root kernel which spans the whole simulation domain.
 *    This is a reference implementation
 *    for a "kernel" that handles domain construction,
 *    initialization and finalization.
 *
 * @author
 *    steffen klatt (created on: jul 03, 2016)
 */

#ifndef  CBM_KSPATIAL_H_
#define  CBM_KSPATIAL_H_

#include  "crabmeat-common.h"
#include  "cbm_servreg.h"

#include  "kernel/kbase.h"
#include  "kernel/kmount.h"

#include  "kernel/kfactory.h"
#include  "kernel/kfactorystore.h"

#include  "string/cbm_string.h"
#include  "time/cbm_time.h"

/* input classes */
#include  "input/setup/setup.h"
namespace cbm {
typedef source_descriptor_t kdesc_t;
typedef ldndc::setup::input_class_setup_t setup_t;
}

#include  "containers/cbm_set.h"
#include  <list>
#include  <map>

struct  lmessage_t;
struct  lreply_t;

namespace cbm {

/* knode flags */
#define  IsCreated              0x0001
#define  IsInitialized          0x0002
#define  IsRegisteredPorts      0x0004
#define  IsUnregisteredPorts    0x0008
#define  IsFinalized            0x0010
#define  IsDestroyed            0x0020
#define  IsBad                  0x0100

struct knode_t
{
    kernel_t *  kernel;
    io_kcomm_t *  iokcomm;

    char const *  kcfg;

    /* knode flags (see above) */
    int  flags;

    /* tree */
    knode_t *  parent;
    knode_t *  sibling;
    knode_t *  child;

    /* linked list*/
    knode_t *  next;
};
struct kqueue_data_t
{
    cbm::string_t  kname;

    lid_t  kid;
    cbm::string_t  ksource;

    kernel_t *  kclient;
    cbm::string_t  kclient_uri;
    cbm::mountargs_t  mountargs;
};
struct kqueue_t
{
    ~kqueue_t()
        { this->clear(); }

    bool  is_empty() const
        { return this->m_queue.empty(); }
    size_t  size() const
        { return this->m_queue.size(); }

    lerr_t  push_back( kqueue_data_t & _qe)
    {
        cbm::mountargs_t & args = _qe.mountargs;
        if ( args.cfg)
        {
            args.cfg = cbm::strdup( args.cfg);
            if ( !args.cfg)
                { return LDNDC_ERR_NOMEM; }
        }
        this->m_queue.push_back( _qe);
        return LDNDC_ERR_OK;
    }
    void  pop_front()
    {
        kqueue_data_t * qe = this->front();
        if ( qe && qe->mountargs.cfg)
            { cbm::strfree( qe->mountargs.cfg); }
        this->m_queue.pop_front();
    }
    kqueue_data_t *  front()
        { return  this->is_empty() ? NULL : &this->m_queue.front(); }
    kqueue_data_t const *  front() const
        { return  this->is_empty() ? NULL : &this->m_queue.front(); }

    void  clear()
    {
        while ( !this->is_empty())
            { this->pop_front(); }
        this->m_queue.clear();
    }
private:
    typedef  std::list< kqueue_data_t >  _kqueue_t;
    _kqueue_t  m_queue;
};

struct CBM_API uri_generate_t
{
    static cbm::uri_t  generate( kqueue_data_t const *,
                    kernel_t const * /*parent*/);
};

/* Stores pointers to all kernels in domain including
 * the root kernel itself (which is a bit of a pain..).
 * By linking kernels (parent,sibling,child) according
 * to mount request we obtain a hierarchical tree
 * structure representing the domain.
 */
#define KNodeMemoryReserveCount 32
class CBM_API knodes_t
{
public:
    knodes_t();
    ~knodes_t();

    size_t  size() const
        { return  CBM_SetSize( this->m_nodes); }

    knode_t *  insert_node( knode_t * /*parent*/, knode_t *);
    knode_t *  remove_node( knode_t *, cbm::string_t const & /*uri*/); /*NOTE only removes leaf nodes*/

    class const_iterator
    {
    public:
        const_iterator();
        const_iterator( CBM_Set const &);
        const_iterator &  operator++()
        {
            if ( !CBM_SetNodeIteratorIsEnd( this->m_ni))
                { this->m_ni = CBM_SetFindNext( this->m_ni); }
            return *this;
        }
        knode_t const *  operator*()
            { return static_cast< knode_t * >( this->m_ni.value); }
        bool  operator==( const_iterator const &  _rhs) const
            { return this->m_ni.value == _rhs.m_ni.value; }
        bool  operator!=( const_iterator const &  _rhs) const
            { return !this->operator==( _rhs); }
    private:
        CBM_SetNodeIterator m_ni;
    };
    const_iterator  cbegin() const
        { return const_iterator( this->m_nodes); }
    const_iterator  cend() const
        { return const_iterator(); }

    class iterator
    {
    public:
        iterator();
        iterator( CBM_Set &);
        iterator &  operator++()
        {
            if ( !CBM_SetNodeIteratorIsEnd( this->m_ni))
                { this->m_ni = CBM_SetFindNext( this->m_ni); }
            return *this;
        }
        knode_t *  operator*()
            { return static_cast< knode_t * >( this->m_ni.value); }
        bool  operator==( iterator const &  _rhs) const
            { return this->m_ni.value == _rhs.m_ni.value; }
        bool  operator!=( iterator const &  _rhs) const
            { return !this->operator==( _rhs); }
    private:
        CBM_SetNodeIterator m_ni;
    };
    iterator  begin()
        { return iterator( this->m_nodes); }
    iterator  end()
        { return iterator(); }

    knode_t *  find_node( cbm::string_t const &);
    knode_t const *  find_node( cbm::string_t const &) const;

private:
    CBM_Set  m_nodes;
};

class CBM_API kspatial_t : public kernel_t
{
    CBM_KERNEL_OBJECT(kspatial_t,__mounter__)
    public:
        kspatial_t();
        ~kspatial_t();

        bool  is_valid_simulation() const
            { return  this->number_of_alive_kernels() > 0; }
        bool  is_complete_simulation() const
            { return  this->number_of_valid_kernels()
                        == this->number_of_alive_kernels(); }

        size_t  number_of_kernels() const;
        size_t  number_of_alive_kernels() const;
        size_t  number_of_valid_kernels() const;

        /*!
         * @brief
         *    return the kernel with given URI
         */
        kernel_t const *  kernel_by_uri( uri_t const &) const;
        kernel_t *  kernel_by_uri( uri_t const &);

        knodes_t &  get_knodes()
            { return this->m_knodes; }
        knodes_t const &  get_knodes() const
            { return this->m_knodes; }

    public:
        void  set_modelcompositor( char const * /*name*/);

    public:
        lerr_t  configure( RunLevelArgs *);

        lerr_t  initialize( RunLevelArgs *);
        lerr_t  initialize_domain( kdesc_t const *, size_t);

        lerr_t  read( RunLevelArgs *);
        lerr_t  finalize( RunLevelArgs *);

    protected:
        kqueue_t  m_kqueue;
        /* input / output interface */
        io_kcomm_t *  m_iokcomm;

        /* container holding pointers to models */
        knodes_t  m_knodes;

        /* service registry */
        cbm::service_registry_t  m_servreg;

        /* model compositor kernel to use */
        cbm::string_t  m_modelcompositor;

        /* store next available rank for kernel class */
        std::map< std::string, int >  m_ranks;

    public:
        lerr_t  enqueue_for_mount( cbm::string_t const & /*kernel name*/,
            lid_t const & /*id*/, cbm::string_t /*source*/,
            kernel_t * /*client*/, cbm::mountargs_t const * /*data*/);
        lerr_t  enqueue_for_umount(
            cbm::string_t const & /*uri*/, kernel_t * /*client*/);

        cbm::services_t  resolve_services( cbm::entities_t const &,
            cbm::services_t const &, int * /*rc*/);

    public:
        /* deactivate kernel and decrease number of active kernels */
        void  deactivate_kernel( knode_t & /*non-null kernel*/);
        /* deactivate all kernels: this is used to terminate simulation
         * in a sane way .. e.g., in openmp builds we cannot simply break
         * from the time loop, so we fake an empty simulation
         */
        void  deactivate();

    private:
        lerr_t  kernels_create();
        lerr_t  kernel_create( kqueue_data_t const *, knode_t *);

        lerr_t  kernels_registerports();
        lerr_t  kernels_initialize();
        lerr_t  kernels_finalize();
        lerr_t  kernels_unregisterports();

        lerr_t  kernels_destroy();

    private:
        lerr_t  m_regrid();
        lerr_t  m_kernelsrunlevel( RunLevel, int /*knode flag*/, int /*call order*/);
        lerr_t  m_kernelsrunlevel( RunLevel, int /*knode flag*/,
                    knode_t * /*head*/, knode_t ** /*recall head*/);

    private:
        /* hide these buggers for now */
        kspatial_t( kspatial_t const &);
        kspatial_t & operator=( kspatial_t const &);
};

} /* namespace cbm */

#endif  /*  !CBM_KSPATIAL_H_  */

