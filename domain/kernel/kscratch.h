/*!
 * @brief
 *    object allowing to quickly store, retrieve
 *    and remove values by using a key with maximum 
 *    length of 128 characters
 *
 * @author
 *    steffen klatt  (created on: feb 15, 2014),
 *    edwin haas
 */

#ifndef LDNDC_STATE_SCRATCH_H_
#define LDNDC_STATE_SCRATCH_H_

#include  "crabmeat-common.h"

#include  "nosql/cbm_kvstore.h"
#include  "string/cbm_string.h"
#include  "time/cbm_time.h"

namespace cbm {

template < typename _T >
struct  state_scratch_invalid_t
{
    static _T const  value;
};
template < >
struct  CBM_API  state_scratch_invalid_t< bool >
{
    static bool const  value;
};

}

template < typename  _T >
_T const cbm::state_scratch_invalid_t< _T >::value =
    ldndc::invalid_t< _T >::value;

namespace cbm {

struct  CBM_API  state_scratch_t
{
    typedef  kvstore_t  scratch_hashtable_t;

    static void  make_item_key(
            std::string *  _key,
            char const *  _format, ...)
    {
        va_list  args;
        va_start( args, _format);
        *_key = cbm::string_t::format_t< 128 >::vmake( _format, args).str();
        va_end( args);
    }

    state_scratch_t() : T_expire( -1) {}

    lerr_t  initialize( cbm::sclock_t const *);
    lerr_t  finalize( cbm::sclock_t const *);

    lerr_t  run_garbage_collector( cbm::td_scalar_t /*timestep*/);

    /*!
     * make container empty: this effectively removes
     * all items from the container leaving it with
     * a size of zero.
     */
    void  clear();
    /*!
     * check if named variable exists in container
     */
    bool  variable_exists(
            char const * /*name*/) const;

    /*!
     * get value of named variable: if the datatype
     * used to get the value from the container
     * mismatches the datatype that was used when
     * setting the value, causes a runtime error (note
     * that this is considered a bug).
     * if the variable does not exist in the container
     * the value of @p buffer is left unchanged.
     *
     * if @p buffer is NULL it just checks variable's
     * existence.
     *
     * note, that we do not check for empty names.
     */
    template < typename  _T >
    lerr_t  get(
            char const * /*name*/, _T * /*buffer*/) const;
    /*!
     * like @fn get but sets value to default if
     * variable does not exist in container
     */
    template < typename  _T >
    lerr_t  get(
            char const * /*name*/,
            _T * /*buffer*/, _T const & /*default*/) const;
    /*!
     * set value of named variable. if a variable with
     * the same name already exists it is overwritten
     * irrespective of its datatype (which is also reset)
     *
     * note, that we do not check for empty names.
     */
    template < typename  _T >
    void  set(
            char const * /*name*/, _T const & /*value*/,
            int = 1 /*number of timestep until expiry*/);
    /*!
     * add value to value of existing named variable. if
     * no variable with this name exists the call is
     * equivalent to set().
     *
     * note, that we do not check for empty names.
     */
    template < typename  _T >
    void  addvalue(
            char const * /*name*/, _T const & /*value*/);
    /*!
     * like @fn get but also (re)sets the variable. if it
     * does not exist it is created.
     */
    template < typename  _T >
    lerr_t  get_and_set(
            char const * /*name*/, _T * /*buffer*/, _T const & /*value*/);
    /*!
     * like @fn get but also removes the variable if
     * it exists.
     *
     * if @p buffer is NULL it just removes the variable
     * from the container if it exists.
     */
    template < typename  _T >
    lerr_t  get_and_remove(
            char const * /*name*/, _T * /*buffer*/);
    /*!
     * like @fn get_and_remove but sets value to
     * default if variable does not exist in container
     */
    template < typename  _T >
    lerr_t  get_and_remove(
            char const * /*name*/,
            _T * /*buffer*/, _T const & /*default*/);
    /*!
     * remove the variable if it exists
     */
    void  remove(
            char const * /*name*/);

    void  dump() const;

    private:
        scratch_hashtable_t  ht;
        cbm::td_scalar_t  T_expire;

        lerr_t  expire_list_enqueue(
                    char const * /*key*/, cbm::td_scalar_t /*expire time*/);
        lerr_t  expire_list_remove(
                    cbm::td_scalar_t /*timestep*/);
};

} /* namespace cbm */

template < typename  _T >
lerr_t
cbm::state_scratch_t::get(
        char const *  _key, _T *  _reg)
const
{
    return  this->ht.fetch_pod< _T >( _reg, _key);
}
template < typename  _T >
lerr_t
cbm::state_scratch_t::get(
        char const *  _key,
        _T *  _reg, _T const &  _default)
const
{
    lerr_t  rc_get = this->get< _T >( _key, _reg);
    if ( rc_get == LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
    {
        *_reg = _default;
    }
    else if ( rc_get)
    {
        *_reg = state_scratch_invalid_t< _T >::value;
    }
    /* fall through */
    return  rc_get;
}

template < typename  _T >
void
cbm::state_scratch_t::set(
        char const *  _key, _T const &  _reg, int  _expire_time)
{
    this->ht.store_pod< _T >( _key, _reg);
    this->expire_list_enqueue( _key, _expire_time);
}

template < typename  _T >
void
cbm::state_scratch_t::addvalue(
        char const *  _key, _T const &  _add_value)
{
    _T  value;
    this->get( _key, &value, _T( 0));
    this->set( _key, value + _add_value);
}

template < typename  _T >
lerr_t
cbm::state_scratch_t::get_and_set(
        char const *  _key, _T *  _reg,
        _T const &  _reset_value)
{
    lerr_t const  rc_get = this->get( _key, _reg);
    this->set( _key, _reset_value);
    return  rc_get;
}
template < typename  _T >
lerr_t
cbm::state_scratch_t::get_and_remove(
        char const *  _key, _T *  _reg)
{
    return  this->ht.fetch_and_remove_pod( _reg, _key);
}
template < typename  _T >
lerr_t
cbm::state_scratch_t::get_and_remove(
        char const *  _key,
        _T *  _reg, _T const &  _default)
{
    lerr_t  rc_get = this->get_and_remove( _key, _reg);
    if ( rc_get == LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
    {
        *_reg = _default;
    }
    else if ( rc_get)
    {
        *_reg = state_scratch_invalid_t< _T >::value;
    }
    /* fall through */
    return  rc_get;
}

#endif  /*  !LDNDC_STATE_SCRATCH_H_  */

