/*!
 * @brief
 *    This is a reference implementation for
 *    a "kernel" that controls kernel execution
 *    that are part of a domain. Further, it
 *    continues or terminates simulation runs
 *    as prescribed by input or controls from
 *    outside.
 *
 *    This object modifies the simulation clock.
 *
 * @author
 *    steffen klatt (created on: apr 14, 2017)
 */

#ifndef  CBM_KTEMPORAL_H_
#define  CBM_KTEMPORAL_H_

#include  "crabmeat-common.h"

#include  "kernel/kbase.h"
#include  "kernel/kfactory.h"

#include  <queue>

namespace cbm {

struct KSchedNode
{
    kernel_t *  kernel;
    RunLevelArgs  args;
    uint64_t  cntr;
};

class CBM_API ktemporal_t : public kernel_t
{
    CBM_KERNEL_OBJECT(ktemporal_t,__scheduler__)
    public:
        ktemporal_t();
        ~ktemporal_t();

        lerr_t  launch( td_scalar_t /*stop time (sse)*/);
        lerr_t  register_for_execution( kernel_t *, RunLevelArgs *);

    public:
        lerr_t  configure( RunLevelArgs *);
        lerr_t  initialize( RunLevelArgs *);
        lerr_t  write( RunLevelArgs *);

    protected:
        struct KSchedNodeCompare
        {
            /* a || ( b && ( c || d && ( e || f))) */
            bool operator()( KSchedNode const & _lhs,
                            KSchedNode const & _rhs) const
                { return ( _lhs.args.nextcall_sse > _rhs.args.nextcall_sse)
                    || ( _lhs.args.nextcall_sse == _rhs.args.nextcall_sse
                            && ( _lhs.args.nextcall_rl > _rhs.args.nextcall_rl
                                || ( _lhs.args.nextcall_rl == _rhs.args.nextcall_rl
                                    && (( _lhs.args.flags.prio == _rhs.args.flags.prio
                                            && _lhs.cntr > _rhs.cntr)
                                        || ( _lhs.args.flags.prio < _rhs.args.flags.prio))))); }
        };
        /* scheduled kernels */
        std::priority_queue< KSchedNode, std::vector<KSchedNode>, KSchedNodeCompare >  m_prioq;

    private:
        uint64_t  m_cntr;

        lerr_t  m_Launch( RunLevelArgs *);
        lerr_t  m_ScheduleSelf( RunLevelArgs const *);
        lerr_t  m_InsertInSchedule( KSchedNode *);
        KSchedNode  m_PopFromSchedule();
        lerr_t  m_ExecuteKernel( KSchedNode *) const;

        void m_UpdateKSchedNode( KSchedNode *) const;

    private:
        /* hide these buggers for now */
        ktemporal_t( ktemporal_t const &);
        ktemporal_t &
                operator=( ktemporal_t const &);
};

} /* namespace cbm */

#endif  /*  !CBM_KTEMPORAL_H_  */

