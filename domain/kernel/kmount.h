/*!
 * @brief
 *    kernel mounter interface. when kernel is mounted/unmounted
 *    the appropriate runlevels are executed:
 *     - creating
 *     - configuring
 *     - port registration
 *     - initializing
 *
 *     - finalizing
 *     - port unregistration
 *     - deleting
 *
 * @author
 *    steffen klatt (created on: jul 04, 2016)
 */

#ifndef  CBM_KMOUNT_H_
#define  CBM_KMOUNT_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

namespace cbm {

class kernel_t;
struct RunLevelArgs;

struct entities_t;
struct services_t;

struct mountargs_t
{
    mountargs_t()
        : cfg( NULL), ptr( NULL) { }
    char const *  cfg; /*nul terminated json formatted string*/
    void *  ptr; /*arbitrary data in local memory*/
};

class kspatial_t;
class CBM_API kmount_t
{
    public:
        kmount_t();
        kmount_t( kspatial_t *, kernel_t * /*client*/);

        lerr_t  enqueue_for_mount( cbm::string_t const & /*kernel name*/,
            lid_t const & /*id*/, cbm::string_t /*source*/,
            cbm::kernel_t * /*client*/, cbm::mountargs_t const * /*data*/);
        lerr_t  enqueue_for_umount(
            cbm::string_t const & /*uri*/, cbm::kernel_t * /*client*/);

        lerr_t  resolve_services( cbm::services_t * /*discovered services*/,
                cbm::entities_t const & /*req. variables*/,
            cbm::services_t const & /*req. services*/, cbm::kernel_t * /*client*/);

    private:
        kspatial_t *  m_kspatial;
};

} /* namespace cbm */

#endif  /* !CBM_KMOUNT_H_ */

