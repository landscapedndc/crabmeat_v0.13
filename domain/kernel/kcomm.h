/*!
 * @brief
 *    interface to inputs; communication interface for
 *    all i/o.
 *
 * @author
 *    steffen klatt (created on: jan 26, 2015)
 */

#ifndef  CBM_KCOMM_H_
#define  CBM_KCOMM_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"
#include  "comm/cbm_comm.h"

namespace cbm {
class  CBM_API  kcomm_t
{
    public:
        kcomm_t() : msgbrk_id( invalid_lid) {}
        kcomm_t(
                char const *  _path, lid_t const &  _id)
            : msgbrk_id( invalid_lid)
        {
            this->m_uri =
                uri_t( uri_t::make_uri( _path, _id));
        }
        ~kcomm_t()
        {
            this->deregister_message_handler();
        }

        /*!
         * @brief
         *    uniform resource identifier
         */
        cbm::string_t  uri() const { return  this->m_uri.uri(); }

        lerr_t  register_message_handler(
                message_handler_function_t /*callback*/, void * /*object*/);
        void  deregister_message_handler();
        lerr_t  send_message( msg_t *, reply_t * = NULL);
        lerr_t  bcast_message( msg_t *, int * /*#receivers*/);

    private:
        uri_t  m_uri;
        lid_t  msgbrk_id;
};
} /* namespace cbm */

#endif  /*  !CBM_KCOMM_H_  */

