/*!
 * @brief
 *    declares structures holding resource information for
 *    kernels (e.g. memory usage and cpu time) independent
 *    on the availability of resource monitoring
 *
 * @author 
 *    steffen klatt (created on: sep 04, 2013),
 *    edwin haas
 */

#ifndef  CBM_STATISTICS_KERNEL_H_
#define  CBM_STATISTICS_KERNEL_H_

#include  "crabmeat-common.h"
#include  "stats/cbm_stats.h"

namespace cbm
{
    struct CBM_API proctime_summary_t
    {
        lid_t kmin, kmax;
        double min, avg, max, total; /* [s] */

        int nb;
    };
    struct CBM_API cputime_summary_t : proctime_summary_t
    { };
    struct CBM_API walltime_summary_t : proctime_summary_t
    { };

    struct CBM_API memory_summary_t
    {
        double prog_sz; /*[KB]*/
        double lib_sz; /*[KB]*/
        double data_sz; /*[KB]*/

        long int nb_mallocs;
        long int nb_bytes;
    };

} /*namespace cbm*/

#include  <vector>
namespace cbm
{
    struct kernel_stats_t
    {
        cbm::string_t modelname;
        int rank;
        int id;

        double cpumin, cpuavg, cpumax, cputotal;
    };
    typedef std::vector<kernel_stats_t> kernels_stats_t;
} /*namespace cbm*/

#endif  /*  CBM_STATISTICS_KERNEL_H_  */

