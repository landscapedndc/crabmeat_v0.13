/*!
 * @brief
 *  export required headers
 *
 * @author
 *  steffen klatt (created on: nov. 19, 2016)
 */

#ifndef  CBM_KERNEL_H__
#define  CBM_KERNEL_H__

#include  <kernel/io-kcomm.h>
#include  <kernel/kobject.h>
#include  <kernel/kbase.h>
#include  <kernel/kfactory.h>
#include  <kernel/ksetup.h>
#include  <kernel/kcomm.h>

#endif /* !CBM_KERNEL_H__ */

