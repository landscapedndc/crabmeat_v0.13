/*!
 * @brief
 *    interface to inputs (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 13, 2013)
 */

#include  "kernel/io-kcomm.h"
#include  "io-dcomm.h"
#include  "cbm_rtcfg.h"

#include  "input/ic.h"
#include  "input/ic-global.h"

LDNDC_CLIENT_OBJECT_DEFN(cbm::io_kcomm_t)
cbm::io_kcomm_t::io_kcomm_t()
        : cbm::client_object_t(),
          m_iodcomm( NULL), m_scratch( NULL)
{
    cbm::set_source_descriptor_defaults( &this->m_kdesc, "setup");
}
cbm::io_kcomm_t::io_kcomm_t(
        lid_t const &  _id)
        : cbm::client_object_t( _id),
          m_iodcomm( NULL), m_scratch( NULL)
{
    crabmeat_assert( _id != invalid_lid);
    cbm::set_source_descriptor_defaults( &this->m_kdesc, "setup");
}

static cbm::state_scratch_t *
cbm_io_kcomm_new_scratch(
        cbm::sclock_t const *  _clk, void *  _addr)
{
    cbm::state_scratch_t *  new_scratch = new cbm::state_scratch_t();
    if ( new_scratch)
    {
        new_scratch->set( ":scratch-owner:", (size_t)_addr);
        /* initialize scratch pad */
        if ( _clk)
            { new_scratch->initialize( _clk); }
    }
    return new_scratch;
}

static void
cbm_io_kcomm_delete_scratch( cbm::state_scratch_t *  _scratch,
        cbm::sclock_t const *  _clk, void *  _addr)
{
    if ( _scratch)
    {
        size_t  addr = 0;
        _scratch->get( ":scratch-owner:", &addr);
        if ( addr == (size_t)_addr)
        {
            _scratch->finalize( _clk);
            delete _scratch;
        }
    }
}

cbm::io_kcomm_t::~io_kcomm_t()
{
    /* deinitialize scratch pad */
    cbm_io_kcomm_delete_scratch(
        this->m_scratch, CBM_LibRuntimeConfig.clk, this);

    /* delete client input classes */
    for ( size_t  k = 0;  k < this->m_ic.size();  ++k)
    {
        if ( this->m_ic[k])
        {
            this->m_iodcomm->delete_input_class( this->m_ic[k]);
        }
    }
    this->m_ic.clear();
}

cbm::kcomm_t
cbm::io_kcomm_t::peer_communicator(
        char const *  _name, lid_t const &  _id)
{
    // TODO  
    // prepend own uri .. ?
    return  kcomm_t( _name, _id);
}
cbm::kmount_t
cbm::io_kcomm_t::spatial_controller( cbm::kernel_t * _kclient)
{
    return  cbm::kmount_t(
         this->m_iodcomm->get_spatialcontroller(), _kclient);
}
cbm::ksched_t
cbm::io_kcomm_t::temporal_controller( cbm::kernel_t * _kclient)
{
    return cbm::ksched_t(
        this->m_iodcomm->get_temporalcontroller(), _kclient);
}

#define  encode_in_uri_with_query(Turi,Fname,Fspatial,Funit) cbm::snprintf( Turi, CBM_UriSize, \
        "/%s&%s=%d&%s=%d&%s=%s", Fname, \
    CBM_UriQSpatial, Fspatial, CBM_UriQTemporal, 1, CBM_UriQUnit, (Funit ? Funit : ""));
#define  encode_in_uri_without_query(Turi,Fname,Fspatial,Funit) cbm::snprintf( Turi, CBM_UriSize, \
        "/%s?%s=%d&%s=%d&%s=%s", Fname, \
    CBM_UriQSpatial, Fspatial, CBM_UriQTemporal, 1, CBM_UriQUnit, (Funit ? Funit : ""));
static int encode_in_uri( char * _uri, char const * _name,
                lid_t const &  _obj_id, char const *  _unit)
{
    if ( strchr( _name, '?'))
        { return encode_in_uri_with_query( _uri, _name, _obj_id, _unit); }
    return encode_in_uri_without_query( _uri, _name, _obj_id, _unit);
}
CBM_Handle
cbm::io_kcomm_t::publish_port_priv( char const *  _name,
    char const *  _unit, lid_t const &  _obj_id,
        void *  _data, size_t  _data_sz,
    CBM_Datatype  _type, int  _flags)
{
    char  uri[CBM_UriSize];
    int  cpy_sz = encode_in_uri( uri, _name, _obj_id, _unit);
    if ( cpy_sz >= CBM_UriSize)
    {
        CBM_LogError( "URI too long  ",
            "[size=",cpy_sz,",max=",CBM_UriSize,",name=",_name,"]");
        return  CBM_None;
    }
    CBM_LogDebug( "URI=", uri);

    int  rc = 0;
    CBM_Handle  handle = CBM_CouplerPublishField(
         CBM_LibRuntimeConfig.cpl, uri,
            _data, _data_sz, _type, _flags, &rc);
    if ( rc)
        { handle = CBM_None; }
    return  handle;
}
lerr_t
cbm::io_kcomm_t::unpublish_port( CBM_Handle  _handle)
{
    int  rc = 0;
    CBM_CouplerUnpublishField( CBM_LibRuntimeConfig.cpl, _handle, &rc);
    return  rc ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

CBM_Handle
cbm::io_kcomm_t::subscribe_port_priv( char const *  _name,
    char const *  _unit, lid_t const &  _obj_id,
        CBM_Transform *  _transform, CBM_Callback  _callback, int  _flags)
{
    char  uri[CBM_UriSize];
    int  cpy_sz = encode_in_uri( uri, _name, _obj_id, _unit);
    if ( cpy_sz >= CBM_UriSize)
    {
        CBM_LogError( "URI too long  ",
            "[size=",cpy_sz,",max=",CBM_UriSize,",name=",_name,"]");
        return  CBM_None;
    }
    CBM_LogDebug( "URI=", uri, " [",_name,"]");

    int  rc = 0;
    CBM_Handle  handle = CBM_CouplerSubscribeField(
        CBM_LibRuntimeConfig.cpl, uri, _transform, _callback, _flags, &rc);
    if ( rc)
        { handle = CBM_None; }
    return  handle;
}
lerr_t
cbm::io_kcomm_t::unsubscribe_port( CBM_Handle  _handle)
{
    int  rc = 0;
    CBM_CouplerUnsubscribeField( CBM_LibRuntimeConfig.cpl, _handle, &rc);
    return  rc ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

char const *
cbm::io_kcomm_t::port_uri( CBM_Handle  _handle) const
{
    return  CBM_CouplerGetFieldUri( CBM_LibRuntimeConfig.cpl, _handle);
}
char const *
cbm::io_kcomm_t::port_unit( CBM_Handle  _handle,
                char *  _buf, size_t  _buf_sz) const
{
    return  CBM_CouplerGetFieldUnit( CBM_LibRuntimeConfig.cpl, _handle, _buf, _buf_sz);
}
int
cbm::io_kcomm_t::ping_port( CBM_Handle  _handle,
        CBM_FieldPingInfo *  _finfo)
{
    int const  pping = CBM_CouplerPingField(
        CBM_LibRuntimeConfig.cpl, _handle, _finfo, NULL);
    return  pping;
} 

size_t
cbm::io_kcomm_t::send_port_priv( CBM_Handle  _handle,
        void const *  _data, size_t  _data_sz, CBM_Datatype  _type, int  _flags)
{
    int  rc = 0;
    size_t  n = CBM_CouplerSendField( CBM_LibRuntimeConfig.cpl,
                    _handle, _data, _data_sz, _type, _flags, &rc);
    if ( rc)
        { n = invalid_size; }
    return  n;
}
size_t
cbm::io_kcomm_t::recv_port_priv( CBM_Handle  _handle,
        void *  _data, size_t  _data_sz, CBM_Datatype  _type, int  _flags)
{
    int  rc = 0;
    size_t  n = CBM_CouplerReceiveField( CBM_LibRuntimeConfig.cpl,
                _handle, _data, _data_sz, _type, _flags, &rc);
    if ( rc)
        { n = invalid_size; }
    return  n;
}

size_t
cbm::io_kcomm_t::recv_dynport_priv( CBM_Handle  _handle,
        void **  _data, CBM_Datatype  _type, int  _flags)
{
    void *  data = NULL;
    int  data_sz = this->ping_port( _handle);
    if ( data_sz > 0)
    {
        int  term_sz = 0;
        if ( CBM_TypesAreEqual(_type,CBM_CHAR)
                || CBM_TypesAreEqual(_type,CBM_UCHAR))
            { term_sz = 1; }
        data = (void *)CBM_Allocate( _type.size*(data_sz+term_sz));
    }

    int  recv_sz = 0;
    if ( data)
    {
        recv_sz = this->recv_port_priv( _handle, data, data_sz, _type, _flags);
        if (( recv_sz == data_sz) && ( CBM_TypesAreEqual(_type,CBM_CHAR)
                || CBM_TypesAreEqual(_type,CBM_UCHAR)))
        {
            char *  chr_data = static_cast< char * >( data);
            chr_data[data_sz] = '\0';
        }

        if ( recv_sz != data_sz)
            { recv_sz = -1; }
    }
    if ( _data && ( recv_sz != -1))
        { *_data = data; }
    else if ( data)
        { this->free_dyndata( data); }

    return  recv_sz;
}
void
cbm::io_kcomm_t::free_dyndata( void *  _data)
{
    if ( _data)
        { CBM_Free( _data); }
}

void
cbm::io_kcomm_t::set_io_service( io_dcomm_t *  _io_dcomm,
        cbm::source_descriptor_t const *  _re)
{
    this->m_iodcomm = _io_dcomm;
    if ( _re)
    {
        this->m_kdesc = *_re;
        this->set_object_id( _re->block_id);
    }
    else
    {
        cbm::set_source_descriptor_defaults( &this->m_kdesc, "setup");
    }
}
void
cbm::io_kcomm_t::set_io_service( io_kcomm_t *  _io_kcomm,
        cbm::source_descriptor_t const *  _re)
{
    crabmeat_assert( _io_kcomm);

    cbm::source_descriptor_t const *  re = _re;
    if ( re == NULL)
        { re = &_io_kcomm->m_kdesc; }
    this->set_io_service( _io_kcomm->m_iodcomm, re);
}


ldndc::input_class_client_base_t const *
cbm::io_kcomm_t::acquire_input( char const *  _type,
        cbm::source_descriptor_t const *  _source_info)
{
    ldndc::input_class_client_base_t const *  ic = NULL;
    if ( !_source_info)
    {
        for ( size_t  k = 0;  k < this->m_ic.size();  ++k)
        {
            if ( this->m_ic[k] &&
                    cbm::is_equal( this->m_ic[k]->input_class_type(), _type))
            {
                ic = this->m_ic[k];
                break;
            }
        }
        if ( !ic)
        {
            cbm::source_descriptor_t  source_info;
            lid_t  ic_id = this->resolve_source( _type, &source_info);
            if ( ic_id == invalid_lid)
            {
                CBM_LogError( "input ID for ", _type,
                    " for kernel ID ", this->object_id(), " was not set.");
                ic = NULL;
                this->state.ok = 0;
            }
            else
            {
                /* request wrapper input class for client */
                ic = this->m_iodcomm->new_input_class( _type, &source_info);
                if ( ic)
                    { this->m_ic.push_back( ic); }
            }
        }

        /* no input available ("error" handling postponed ..) */
        if ( ic == NULL)
            { this->state.ok = 0; }
    }
    else
    {
        ic = this->m_iodcomm->new_input_class( _type, _source_info);
        if ( !ic)
            { /* missing input here is OK */ }
    }
    return  ic;
}

void
cbm::io_kcomm_t::release_input(
        ldndc::input_class_client_base_t const *  _ic) const
{
    if ( _ic)
        { this->m_iodcomm->delete_input_class( _ic); }
}

ldndc::input_class_srv_base_t *
cbm::io_kcomm_t::fetch_input( char const *  _type,
        cbm::source_descriptor_t const *  _kd)
    { return  this->m_iodcomm->fetch_input( _type, _kd); }
ldndc::input_class_srv_base_t const *
cbm::io_kcomm_t::fetch_input( char const *  _type,
        cbm::source_descriptor_t const *  _kd) const
    { return  this->m_iodcomm->fetch_input( _type, _kd); }


ldndc::sink_handle_t
cbm::io_kcomm_t::sink_handle_acquire(
        char const *  _sink_identifier, int  _sink_index)
{
    crabmeat_assert( this->m_iodcomm);
    return  this->m_iodcomm->sink_handle_acquire(
            &this->m_kdesc, "setup", _sink_identifier, _sink_index);
}
lerr_t
cbm::io_kcomm_t::sink_handle_release(
        ldndc::sink_handle_t *  _h)
{
    crabmeat_assert( this->m_iodcomm);
    return  this->m_iodcomm->sink_handle_release( _h);
}

lid_t
cbm::io_kcomm_t::resolve_source( char const *  _type,
    cbm::source_descriptor_t *  _source_info) const
{
    crabmeat_assert( _source_info);

    if ( cbm::is_equal( _type, "setup"))
        { *_source_info = this->m_kdesc; }
    else
        { this->m_iodcomm->get_source_descriptor(
                _source_info, &this->m_kdesc, _type); }

    return  _source_info->block_id;
}

void
cbm::io_kcomm_t::set_scratch( cbm::state_scratch_t *  _scratch)
{
    if ( !this->m_scratch)
        { this->m_scratch = _scratch; }
}
cbm::state_scratch_t *
cbm::io_kcomm_t::get_scratch()
{
    if ( !this->m_scratch)
    {
        this->m_scratch = cbm_io_kcomm_new_scratch(
                CBM_LibRuntimeConfig.clk, this);
    }
    return this->m_scratch;
}

