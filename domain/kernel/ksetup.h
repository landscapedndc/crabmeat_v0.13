/*!
 * @brief
 *    interface to kernel specific input data
 *    (i.e., convenience wrapper around JSON library)
 *
 * @author
 *    steffen klatt (created on: dec 01, 2016)
 */

#ifndef  CBM_KSETUP_H_
#define  CBM_KSETUP_H_

#include  "crabmeat-common.h"
#include  "json/cbm_jquery.h"
#include  "string/cbm_string.h"

namespace cbm {

struct CBM_API ksetup_t
{
    ksetup_t();
    ksetup_t( char const * /*kernel name*/,
        char const * /*kernel data*/);
    ksetup_t( ksetup_t const &);
    ksetup_t &  operator=( ksetup_t const &);

    ~ksetup_t();

    bool  parse_ok() const;

    char const *  nested_setup( char const *  _path) const
        { return this->m_jquery.nested_string( _path); }
    char const *  nested_setup( cbm::string_t const &  _path) const
        { return this->nested_setup( _path.c_str()); }

    bool  exists( char const * /*path*/);
    bool  exists( cbm::string_t const & /*path*/);

    int  query_size( char const * /*path*/);

    /* query strings */
    char const *  query_string(
        char const * /*path*/, char const * /*default*/);
    cbm::string_t  query_string(
        cbm::string_t const & /*path*/, char const * /*default*/);

    bool  query_bool(
        char const * /*path*/, bool /*default*/);
    bool  query_bool(
        cbm::string_t const & /*path*/, bool /*default*/);

    int  query_int(
        char const * /*path*/, int /*default*/);
    int  query_int(
        cbm::string_t const & /*path*/, int /*default*/);

    double  query_double(
        char const * /*path*/, double /*default*/);
    double  query_double(
        cbm::string_t const & /*path*/, double /*default*/);


    cbm::string_t  query_any(
        char const * /*path*/, char const * /*default*/);
    cbm::string_t  query_any(
        cbm::string_t const & /*path*/, char const * /*default*/);

    template < typename _ValueType >
        _ValueType  query( char const * _path, _ValueType _default)
            { return this->m_jquery.get< _ValueType >( _path, _default); }

private:
    jquery_t  m_jquery;
};

} /* namespace cbm */

#endif /* !CBM_KSETUP_H_ */

