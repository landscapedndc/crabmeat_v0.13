/*!
 * @brief
 *    kernel factory store
 *
 * @author
 *    steffen klatt (created on: mar 3, 2016)
 */

#include  "kernel/kfactory.h"
#include  "kernel/kfactorystore.h"

#include  "log/cbm_baselog.h"

size_t
cbm::kernelfactorystore_t::size()
{
    crabmeat_assert( this->m_kernelfactorystore);
    return  static_cast< cbm::factorystore_t< kernelfactory_base_t > * >(
        this->m_kernelfactorystore)->size();
}

cbm::kernel_t *
cbm::kernelfactorystore_t::construct_kernel(
    char const *  _kname, lid_t const & _object_id, int *  _f_exists)
{
    crabmeat_assert( this->m_kernelfactorystore);
    cbm::kernelfactory_base_t const *  kf =
        static_cast< cbm::factorystore_t< kernelfactory_base_t > * >(
                this->m_kernelfactorystore)->factory( kernel_factory_prefix, _kname);

    if ( kf)
    {
        if ( _f_exists)
            { *_f_exists = 1; }
        return  kf->construct( _object_id);
    }
    if ( _f_exists)
        { *_f_exists = 0; }
    return  NULL;
}

