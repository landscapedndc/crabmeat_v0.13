/*!
 * @brief
 *    kernel mounter
 *
 * @author
 *    steffen klatt (created on: jul 04, 2016)
 */

#include  "kernel/kmount.h"
#include  "kernel/kfactorystore.h"

#include  "log/cbm_baselog.h"

#include  "kernel/kspatial.h"
cbm::kmount_t::kmount_t()
    : m_kspatial( NULL)
    { }
cbm::kmount_t::kmount_t(
        cbm::kspatial_t *  _kspatial,
            cbm::kernel_t *)
    : m_kspatial( _kspatial)
    { }

lerr_t
cbm::kmount_t::enqueue_for_mount( cbm::string_t const &  _kname,
    lid_t const &  _kid, cbm::string_t  _ksource,
    cbm::kernel_t *  _kclient, cbm::mountargs_t const *  _data)
{
    CBM_LogDebug( "enqueuing '",_kname,"@",_kid,"' {",_ksource,"}",
        " by ", (_kclient?_kclient->name():"?"),
            " {", (_kclient?_kclient->object_id():-1),"}");

    lerr_t  rc_enq = this->m_kspatial->enqueue_for_mount(
                _kname, _kid, _ksource, _kclient, _data);
    if ( rc_enq)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
lerr_t
cbm::kmount_t::enqueue_for_umount(
        cbm::string_t const &  _uri,
            cbm::kernel_t *  _kclient)
{
    CBM_LogDebug( "requesting umount for '", _uri,
        " by ", (_kclient?_kclient->name():"?"),
        " {", (_kclient?_kclient->object_id():-1),"}");

    lerr_t  rc_enq =
        this->m_kspatial->enqueue_for_umount( _uri, _kclient);
    if ( rc_enq)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

#include  "cbm_servreg.h"
lerr_t
cbm::kmount_t::resolve_services( cbm::services_t *  _serv,
        cbm::entities_t const &  _req_variables,
    cbm::services_t const &  _req_services,
                cbm::kernel_t * /*_kclient*/)
{
    crabmeat_assert( _serv);
    int  rc_resolve = 0;
    *_serv = this->m_kspatial->resolve_services(
                _req_variables, _req_services, &rc_resolve);
    if ( rc_resolve)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

