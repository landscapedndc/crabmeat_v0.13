/*!
 * @brief
 *  represents (spatial and temporal) simulation domain
 *
 *  @n
 *  spatial domain delegated to kspatial.
 *  @n
 *  temporal domain delegated to ktemporal
 *
 * @author
 *  steffen klatt (created: 2009),
 *  edwin haas
 */

#ifndef  CBM_DOMAIN_H_
#define  CBM_DOMAIN_H_

#include  "cbm_object.h"
#include  "time/cbm_time.h"

#include  "kernel/kspatial.h"
#include  "kernel/ktemporal.h"
#include  "kernel/kfactorystore.h"
#include  "kernel/kstats.h"

namespace cbm {

class  io_dcomm_t;
class  work_dispatcher_t;


class CBM_API domain_t : public cbm::object_t
{
    LDNDC_OBJECT(domain_t)
    public:
        class const_iterator
        {
        public:
            const_iterator() { }
            const_iterator( cbm::knodes_t const &  _knodes)
                : m_ki( _knodes.cbegin()) { }

            cbm::kernel_t const *  operator *()
                { return (*this->m_ki)->kernel; }
            const_iterator  operator++()
                { ++this->m_ki; return *this; }

            bool operator==( const_iterator const &  _rhs) const
                { return this->m_ki == _rhs.m_ki; }
            bool operator!=( const_iterator const &  _rhs) const
                { return this->m_ki != _rhs.m_ki; }
        private:
            cbm::knodes_t::const_iterator  m_ki;
        };
        const_iterator  cbegin() const
            { return const_iterator( this->m_kspatial->get_knodes()); }
        const_iterator  cend() const
            { return const_iterator(); }
        class iterator
        {
        public:
            iterator() { }
            iterator( cbm::knodes_t &  _knodes)
                : m_ki( _knodes.begin()) { }

            cbm::kernel_t *  operator *()
                { return (*this->m_ki)->kernel; }
            iterator  operator++()
                { ++this->m_ki; return *this; }

            bool operator==( iterator const &  _rhs) const
                { return this->m_ki == _rhs.m_ki; }
            bool operator!=( iterator const &  _rhs) const
                { return this->m_ki != _rhs.m_ki; }
        private:
            cbm::knodes_t::iterator  m_ki;
        };
        iterator  begin()
            { return iterator( this->m_kspatial->get_knodes()); }
        iterator  end()
            { return iterator(); }
    public:
        domain_t();
        virtual  ~domain_t();

        void  select_systemcomponent( char const * /*component*/,
                char const * /*name*/, cbm::kernel_t * = NULL /*object*/);

        /*!
         * @brief
         *    builds up all model units in this regional
         *    simulation based on input data. this is a
         *    preliminary step to launch!
         */
        virtual  lerr_t  initialize( cbm::io_dcomm_t *,
                    cbm::work_dispatcher_t * = NULL);

        /*!
         * @brief
         *    launch regional simulation run. A stop time (SSE)
         *    can be provided. This function allows for
         *    consectutive calls to continue from where it left
         *    off after previous call.
         */
        virtual  lerr_t  launch( cbm::td_scalar_t = 0 /*stop time*/);
        /*!
         * @brief
         *    call runlevels individually
         */
        virtual  lerr_t  read( cbm::td_scalar_t /*stop time*/) { return LDNDC_ERR_OK; }
        virtual  lerr_t  solve( cbm::td_scalar_t /*stop time*/) { return LDNDC_ERR_OK; }
        virtual  lerr_t  integrate( cbm::td_scalar_t /*stop time*/) { return LDNDC_ERR_OK; }
        virtual  lerr_t  write( cbm::td_scalar_t /*stop time*/) { return LDNDC_ERR_OK; }

        /*!
         * @brief
         *    call each kernels finalize method. after a
         *    call to this function no more steps are
         *    allowed.
         *
         * @note
         *    @fn finalize is called for each kernel, even
         *    though it might be deactivated. deactivated
         *    kernels need to decide on their own whether
         *    what to do.
         */
        virtual  lerr_t  finalize();


        /*!
         * @brief
         *    return the number of kernels
         */
        size_t  number_of_kernels() const;

        /*!
         * @brief
         *    return the number of valid kernels (currently 
         *    this means pointer to kernels is not null)
         */
        size_t  number_of_valid_kernels() const;

        /*!
         * @brief
         *    return the number of valid kernels that are
         *    tagged as active (runnable, i.e. a subset of
         *    valid kernels). kernels may be deactivated
         *    during the simulation, e.g. a kernel is
         *    deactivated after it returns from its step()
         *    function with a non-ok status
         */
        size_t  number_of_alive_kernels() const;

        bool  is_complete_simulation() const
            { return  this->m_kspatial && this->m_kspatial->is_complete_simulation(); }

#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
        int  create_checkpoint();
        int  restore_checkpoint( ldate_t);
#endif

#ifdef  CRABMEAT_ONLINECONTROL
        int  process_request(
                lreply_t *,  lrequest_t const *);
    private:
		int  send_request_to_kernel(
                lreply_t *,  lrequest_t const *);
#endif

    public:
        /* test if resource monitoring is available */
        int  have_kernel_resource_statistics() const;
        int  cputime_summary( cputime_summary_t *) const;
        int  walltime_summary( walltime_summary_t *) const;
        void  memory_summary( memory_summary_t *) const;

        int  kernel_resource_stats( kernels_stats_t &) const;

    protected:
        /* spatial domain handler (domainroot) */
        cbm::kspatial_t *  m_kspatial;
        cbm::io_kcomm_t *  m_kspatial_iokcomm;
        /* temporal domain handler (scheduler) */
        cbm::ktemporal_t *  m_ktemporal;
        cbm::io_kcomm_t *  m_ktemporal_iokcomm;

        /* domain communicator (takes ownership) */
        cbm::io_dcomm_t *  m_iodcomm;

        /* */
        struct _SystemComponent
        {
            _SystemComponent()
                : kernel( NULL), iokcomm( NULL) { }
            std::string  name;
            cbm::kernel_t *  kernel;
            cbm::io_kcomm_t *  iokcomm;
        };
        std::map< std::string, _SystemComponent >  m_systemcomponents;

        int  m_isfinalized;
        double  m_runtime;

        lerr_t  initialize_domain( cbm::work_dispatcher_t *, int * /*return domain size*/);
        lerr_t  m_initialize_temporal_controller();
        lerr_t  m_initialize_spatial_controller();
        lerr_t  m_domain_from_input( cbm::source_descriptor_t const *, size_t);

        int  check_if_launching_makes_sense(
            cbm::td_scalar_t /*requested stop time*/, lerr_t * /*return code*/);
};

} /* namespace cbm */

#endif  /*  !CBM_DOMAIN_H_  */

