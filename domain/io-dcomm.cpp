/*!
 * @brief
 *    high level interface to i/o subsystem (implementation)
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011)
 */

#include  <stdexcept>

#include  "io-dcomm.h"
#include  "cbm_project.h"
#include  "cbm_rtcfg.h"

LDNDC_CLIENT_OBJECT_DEFN(cbm::io_dcomm_t)
cbm::io_dcomm_t::io_dcomm_t()
        : cbm::client_object_t(),
          m_project( NULL),
          m_kspatial( NULL)
{ }

cbm::io_dcomm_t::~io_dcomm_t()
{
    this->m_kspatial = NULL;
    if ( this->m_project)
    {
        this->m_project->delete_instance();
        this->m_project = NULL;
    }
}

#include  "prjfile/cbm_prjfile.h"
lid_t const &
cbm::io_dcomm_t::open_project()
{
    if ( this->m_project)
    {
        CBM_LogError( "failed to open project: open project exists, close first.");
        return  invalid_lid;
    }

    this->m_project = cbm::project_t::new_instance( CBM_LibRuntimeConfig.prj->object_id());
    if ( !this->m_project)
    {
        CBM_LogError( "failed to allocate project");
        return  invalid_lid;
    }

    lerr_t  rc_open_project = this->m_project->open_project();
    if ( rc_open_project)
    {
        this->m_project->delete_instance();
        this->m_project = NULL;
        return  invalid_lid;
    }

    this->set_object_id( this->m_project->object_id());
    return  this->m_project->object_id();
}
lerr_t
cbm::io_dcomm_t::set_project(
        cbm::project_t *  _project)
{
    if ( this->m_project)
    {
        /* resetting project not supported */
        CBM_LogError( "set_project(): resetting project not supported");
        return  LDNDC_ERR_FAIL;
    }
    if ( !_project)
    {
        CBM_LogError( "set_project(): given pointer to project was NULL");
        return  LDNDC_ERR_FAIL;
    }

    this->m_project = _project;
    this->set_object_id( _project->object_id());
    return  LDNDC_ERR_OK;
}


/* I/O */

lerr_t
cbm::io_dcomm_t::reset_io(
        cbm::source_descriptor_t const *  _sd,
                char const *  _type)
{
    if ( this->m_project)
    {
        return  this->m_project->reset_io( _sd, _type);
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}
lerr_t
cbm::io_dcomm_t::reset_io()
{
    if ( this->m_project)
    {
        return  this->m_project->reset_io();
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}

lerr_t
cbm::io_dcomm_t::update_io(
        cbm::source_descriptor_t const *  _sd,
                char const *  _type)
{
    if ( this->m_project)
    {
        return  this->m_project->update_io( _sd, _type, *CBM_LibRuntimeConfig.clk);
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}
lerr_t
cbm::io_dcomm_t::update_io()
{
    if ( this->m_project)
    {
        return  this->m_project->update_io( *CBM_LibRuntimeConfig.clk);
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}


lerr_t
cbm::io_dcomm_t::commit_io()
{
    if ( this->m_project)
    {
        return  this->m_project->commit_io( *CBM_LibRuntimeConfig.clk);
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}


lid_t
cbm::io_dcomm_t::get_source_descriptor(
        cbm::source_descriptor_t *  _source_info,
        cbm::source_descriptor_t const *  _kernel_info,
        char const *  _type)
const
{
    crabmeat_assert( this->m_project);
    return  this->m_project->get_source_descriptor( _source_info, _kernel_info, _type);
}

#include  "input/ic-factory.h"
#include  "io/source-handle.h"
ldndc::input_class_client_base_t *
cbm::io_dcomm_t::new_input_class( char const *  _type,
    cbm::source_descriptor_t const *  _source_info)
{
    crabmeat_assert( this->m_project);
    crabmeat_assert( _source_info && _source_info->block_id != invalid_lid);

    /* construct wrapper input class on client side */
    ldndc::input_class_client_base_t *  ic = NULL;
    if ( ldndc::find_input_class_factory( 'C', _type))
    {
        ic = ldndc::find_input_class_factory(
            'C', _type)->construct( _source_info->block_id);
    }
    if ( ic)
    {
        ldndc::input_class_srv_base_t const *  ic_srv =
            this->m_project->get_input_class( _source_info, _type);
        if ( ic_srv)
        {
            /* create input object and set properties */
            ldndc::source_handle_t  ciobj( ic_srv);
            /* initialize client side input class */
            lerr_t  ic_init = ic->initialize( &ciobj);
            if ( ic_init)
            {
                CBM_LogError( "initializing client input class failed",
                    "  [class=",_type,";",*_source_info,"]");
                ldndc::find_input_class_factory( 'C', _type)->destruct( ic);
                ic = NULL;
            }
        }
        else
        {
            ldndc::find_input_class_factory( 'C', _type)->destruct( ic);
            ic = NULL;
// dk:off            CBM_LogError( "errors occured attempting to retrieve server input class",
// dk:off                   "  [id=",*_source_info,",class=",_type,"]");
        }
    }
    else
    {
        CBM_LogError( "failed to construct client input class, trouble ahead!",
            "  [id=",*_source_info,",class=",_type,"]");
    }
    return  ic;
}

void
cbm::io_dcomm_t::delete_input_class(
        ldndc::input_class_client_base_t const *  _ic)
{
    if ( _ic && ldndc::find_input_class_factory(
                        'C', _ic->input_class_type()))
    {
        ldndc::find_input_class_factory(
            'C', _ic->input_class_type())->destruct( _ic);
    }
}

ldndc::input_class_srv_base_t *
cbm::io_dcomm_t::fetch_input( char const *  _type,
        cbm::source_descriptor_t const *  _source_info)
{
    crabmeat_assert( this->m_project);
    crabmeat_assert( _source_info && _source_info->block_id != invalid_lid);

    ldndc::input_class_srv_base_t *  ic_srv =
        this->m_project->get_input_class( _source_info, _type);
    return  ic_srv;
}
ldndc::input_class_srv_base_t const *
cbm::io_dcomm_t::fetch_input( char const *  _type,
        cbm::source_descriptor_t const *  _source_info) const
{
    crabmeat_assert( this->m_project);
    crabmeat_assert( _source_info && _source_info->block_id != invalid_lid);

    ldndc::input_class_srv_base_t const *  ic_srv =
        this->m_project->get_input_class( _source_info, _type);
    return  ic_srv;
}


ldndc::sink_handle_t
cbm::io_dcomm_t::sink_handle_acquire(
        cbm::source_descriptor_t const *  _source_descriptor,
        char const *  _type, char const *  _sink_identifier, int  _sink_index)
{
    crabmeat_assert( this->m_project);
    lid_t  sink_descriptor = this->m_project->attach_sink( _sink_identifier, _sink_index);

    ldndc::sink_handle_t  h_c;
    if ( sink_descriptor == invalid_lid)
    {
        /* return handle with bad status */
        CBM_LogInfo( "did not attach to sink  [sink=",_sink_identifier,",index=",_sink_index,"]");
        h_c.state.ok = 0;

        return  h_c;
    }

    CBM_LogDebug( "successfully attached to sink  [sink=",_sink_identifier,",index=",_sink_index,"]");
    ldndc::sink_handle_srv_t  h_s =
        this->m_project->sink_handle_acquire(
                _source_descriptor, _type, sink_descriptor);
    h_c.set_acquired( h_s);
    h_c.set_lclock( CBM_LibRuntimeConfig.clk);

    return  h_c;
}

lerr_t
cbm::io_dcomm_t::sink_handle_release(
        ldndc::sink_handle_t *  _hdl)
{
    crabmeat_assert( this->m_project && _hdl);
    if ( _hdl->is_released())
    {
        return  LDNDC_ERR_OK;
    }
    /* no matter what trouble sink handle release causes .. */
    _hdl->set_released();
    return  this->m_project->sink_handle_release(
        _hdl->sink_descriptor(), _hdl->receiver_descriptor());
}


/* WORK DISPATCHING */

#include  "dispatcher/cbm_dispatcher.h"
lerr_t
cbm::io_dcomm_t::initialize_dispatcher(
        cbm::work_dispatcher_t *  _wd)
{
    lerr_t  rc = LDNDC_ERR_OK;
    if ( _wd)
        { rc = _wd->initialize( this); }
    return  rc;
}

std::vector< cbm::source_descriptor_t >
cbm::io_dcomm_t::object_id_list( char const *  _type,
        cbm::work_dispatcher_t *  _wd)
{
    crabmeat_assert( this->m_project);

    std::vector< cbm::source_descriptor_t >  region_v =
                    this->m_project->object_id_list( _type);
    std::vector< cbm::source_descriptor_t >  subregion_v;

    for ( size_t  k = 0;  k < region_v.size();  ++k)
    {
        if ( !_wd || _wd->acquire_kernel( region_v[k].block_id))
        {
            subregion_v.push_back( region_v[k]);
        }
    }
    return  subregion_v;
}


std::vector< cbm::source_descriptor_t >
cbm::io_dcomm_t::request_region(
        cbm::work_dispatcher_t *  _wd)
{
    return  this->object_id_list( "setup", _wd);
}

/* Spatial Controller (a.k.a. Mounter) */
lerr_t  cbm::io_dcomm_t::set_spatialcontroller(
        cbm::kspatial_t *  _kspatial)
{
    this->m_kspatial = _kspatial;
    return  LDNDC_ERR_OK;
}
cbm::kspatial_t *  cbm::io_dcomm_t::get_spatialcontroller()
{
    return  this->m_kspatial;
}

/* Temporal Controller (a.k.a. Scheduler) */
lerr_t  cbm::io_dcomm_t::set_temporalcontroller(
        cbm::ktemporal_t *  _ktemporal)
{
    this->m_ktemporal = _ktemporal;
    return  LDNDC_ERR_OK;
}
cbm::ktemporal_t *  cbm::io_dcomm_t::get_temporalcontroller()
{
    return  this->m_ktemporal;
}

