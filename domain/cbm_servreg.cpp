
#include  "domain/cbm_servreg.h"
#include  "string/cbm_string.h"
#include  "log/cbm_baselog.h"

#include  "json/cbm_json.h"

#ifdef  WIN32
#  define  servreg_FILEMODE "rb"
#else
#  define  servreg_FILEMODE "r"
#endif


cbm::service_registry_t::service_registry_t()
        : m_regurl( ""), m_isconnected( 0)
    { }
cbm::service_registry_t::~service_registry_t()
{
    this->disconnect();
}

lerr_t
cbm::service_registry_t::connect(
                    char const *  _regurl)
{
    lerr_t  rc_connect = LDNDC_ERR_FAIL;

    if ( cbm::is_equal( _regurl, this->m_regurl.c_str()))
    { 
        if ( this->m_isconnected)
            { return  LDNDC_ERR_OK; }
    }
    else
    {
        this->m_isconnected = 0;
        this->m_regurl = _regurl;
    }

    // TODO  assume file for now
    FILE *  fp = fopen( this->m_regurl.c_str(), servreg_FILEMODE);
    if ( fp == NULL)
    {
        CBM_LogError( "error opening Service Registry stream",
                "  [url=",this->m_regurl,"]");
        return  LDNDC_ERR_FAIL;
    }
    char  read_buf[65536];

    rapidjson::FileReadStream  instream( fp, read_buf, sizeof(read_buf));
    this->m_reg.ParseStream( instream);
    if ( this->m_reg.HasParseError())
    {
        CBM_LogError( "error occured when parsing Service Registry");
    }
    else
    {
        if ( this->m_reg.IsObject() && this->m_reg["services"].IsArray())
            { rc_connect = LDNDC_ERR_OK; }
        else
            { CBM_LogError( "failed to connect to Service Registry"); }
    }
    fclose( fp);

    if ( rc_connect == LDNDC_ERR_OK)
    {
        this->m_isconnected = 1;

        CBM_LogVerbose( "successfully connected to Service Registry");
// sk:dbg        CBM_LogDebug( "R: '",this->serialize(),"'");
    }
    else
    {
        this->m_isconnected = 0;
        this->m_regurl.clear();
    }

    crabmeat_assert( rc_connect==LDNDC_ERR_OK && this->m_isconnected);
    return  rc_connect;
}

lerr_t
cbm::service_registry_t::disconnect()
{
    this->m_regurl.clear();
    this->m_isconnected = 0;

    return  LDNDC_ERR_OK;
}

cbm::string_t
cbm::service_registry_t::serialize()
{
    rapidjson::StringBuffer  buffer;
    buffer.Clear();

    rapidjson::Writer< rapidjson::StringBuffer >  reg_writer( buffer);
    this->m_reg.Accept( reg_writer);

    return  cbm::string_t( buffer.GetString());
}

cbm::services_t
cbm::service_registry_t::query_providers( std::string const &  _entity_id,
        lflags_t  /*_flags*/, int *  _rc) const
{
    crabmeat_assert( this->m_isconnected);

    cbm::services_t  matching_services;

    rapidjson::Value const &  services_arr = this->m_reg["services"];
    if ( ! services_arr.IsArray())
    {
        if ( _rc)
            { *_rc = -1; }
        CBM_LogError( "Service Registry Stream malformed");
        return  cbm::services_t();
    }
    for ( rapidjson::SizeType  s = 0;  s < services_arr.Size();  ++s)
    {
        rapidjson::Value const &  jserv = services_arr[s];
        if ( jserv.HasMember("IGNORE") && jserv["IGNORE"].GetBool())
        {
            CBM_LogVerbose( "ignoring service due to 'IGNORE' flag",
                    "  [service=",jserv["id"].GetString(),"]");
            continue;
        }
        if ( ! jserv["id"].IsString())
        {
            if ( _rc)
                { *_rc = -1; }
            CBM_LogError( "Service Registry Stream malformed");
            return  cbm::services_t();
        }
        rapidjson::Value const &  provides_arr = jserv["provides"];
        CBM_LogDebug( "service=",jserv["id"].GetString(), "  #p=",provides_arr.Size());
        for ( rapidjson::SizeType  e = 0;  e < provides_arr.Size();  ++e)
        {
            CBM_LogDebug( "  provides=",provides_arr[e].GetString());
            rapidjson::Value const &  jprov = provides_arr[e];
            if ( ! jprov.IsString())
            {
                if ( _rc)
                    { *_rc = -1; }
                CBM_LogError( "Service Registry Stream malformed");
                return  cbm::services_t();
            }
            std::string  entity_id = jprov.GetString();
            if ( entity_id == _entity_id)
            {
                CBM_LogDebug( "found matching service",
                    "  [entity=",_entity_id,",service=",jserv["id"].GetString(),"]");

                service_t  serv;
                serv.id = jserv["id"].GetString();
                serv.name = jserv["name"].GetString();
                serv.m = NULL;

                matching_services.push_back( serv);
            }
        }
    }

    if ( _rc)
        { *_rc = 0; }
    return  matching_services;
}

cbm::service_properties_t const *
cbm::service_registry_t::query_service_properties(
        std::string  _service_id, int * _rc) const
{
    crabmeat_assert( this->m_isconnected);
    if ( _rc)
        { *_rc = -1; }

    rapidjson::Value const &  services_arr = this->m_reg["services"];
    if ( ! services_arr.IsArray())
    {
    }
    for ( rapidjson::SizeType  s = 0;  s < services_arr.Size();  ++s)
    {
        std::string  service_id = services_arr[s]["id"].GetString();
        if ( service_id == _service_id)
        {
            if ( _rc)
                { *_rc = 0; }
            return  &services_arr[s];
        }
    }
    return  NULL;
}

cbm::service_discover_t::service_discover_t(
            cbm::service_registry_t const *  _reg)
        : m_reg( _reg)
    { }

cbm::service_t
cbm::service_discover_t::discover( std::string const &  _entity_id,
            cbm::services_t const &  _services, int *  _rc) const
{
    CBM_LogDebug( "discover .. ", "  [variable=",_entity_id,"]");
    cbm::services_t  matching_services =
        this->m_reg->query_providers( _entity_id);
    size_t const  n_services = matching_services.size();
    /* unambiguous match */
    if ( n_services == 0)
    {
        if ( _rc)
            { *_rc = -1 ; }
        CBM_LogError( "Failed to find service matching your request",
                    "  [entity=",_entity_id,"]");
        return  cbm::service_t();
    }
    else if ( n_services == 1)
    {
        return  matching_services.front();
    }
    else
    {
        int  matches = 0;
        cbm::services_t::iterator  match = matching_services.end();
        for ( cbm::services_t::iterator  s = matching_services.begin();
                    s != matching_services.end();  ++s)
        {
        for ( cbm::services_t::const_iterator  r = _services.begin();
                    r != _services.end();  ++r)
            {
                if ( s->id == r->id)
                {
                    matches += 1;
                    match = s;
                }
            }
        }
        if ( matches == 1 )
        {
            CBM_LogDebug( "Resolved ambiguous match per user request",
                "  [entity=",_entity_id,",service=",match->id,"]");
            return  *match;
        }
        else
        {
            if ( _rc)
                { *_rc = -1; }
            CBM_LogError( "Unable to resolve ambiguous service request",
                    "  [entity=",_entity_id,
                ",#providers=",n_services,",matches=",matches,"]");
            if ( matches > 0 )
            {
                std::string  services_found =
                    "services found: " + matching_services.front().id;
                cbm::services_t::iterator  s = matching_services.begin();
                ++s;
                for ( ; s != matching_services.end();  ++s)
                {
                    services_found += ", " + s->id;
                }
                CBM_LogWrite( "services found:", services_found);
            }
            return  cbm::service_t();
        }
    }
}

cbm::service_properties_t const *
cbm::service_discover_t::service_properties(
            cbm::service_t const &  _service)
    const
{
    return  this->m_reg->query_service_properties( _service.id, NULL);
}


cbm::workflow_compositer_t::workflow_compositer_t(
        cbm::service_registry_t const *  _servreg)
    : m_servd( _servreg)
    { }

cbm::services_t
cbm::workflow_compositer_t::resolve(
            cbm::entities_t const &  _req_ents,
        cbm::services_t const &  _req_serv, int *  _rc)
{
    /* primary requirements */
    cbm::entities_t  V = _req_ents;
    /* explicitely requested services to resolve ambiguity */
    cbm::services_t  S = _req_serv;

    bool  resolvable = true;
    while ( ! V.empty() && resolvable)
    {
        /* identify and discover appropriate service */
        cbm::entity_t  v = V.front();
        CBM_LogDebug( "discover service for variable",
                "  [variable=",v.id,"]");
        int  rc_servd = 0;
        cbm::service_t  s =
            this->m_servd.discover( v.id, S, &rc_servd);
        if ( rc_servd)
        {
            CBM_LogError( "error occured during service discovery",
                    "  [variable=",v.id,"]");
            if ( _rc)
                { *_rc = -1; }
            resolvable = false;
        }
        else
        {
            CBM_LogDebug( "discovered service for variable",
                    "  [variable=",v.id,",service=",s.id,"]");

            /* find service properties */
            cbm::service_properties_t const &  servp =
                *this->m_servd.service_properties( s);
            s.m = &servp;

            /* add service to workflow */
            S.push_back( s);

            /* extract further dependencies */
            for ( size_t  d = 0;  d < servp["requires"].Size();  ++d)
            {
                std::string  v_r_id = servp["requires"][d].GetString();
                cbm::entity_t  v_r;
                v_r.id = v_r_id;
                V.push_back( v_r);
            }
            V.pop_front();
        }
    }
    if ( ! resolvable)
    {
        S.clear();
        if ( _rc)
            { *_rc = -1; }
    }
    return  S;
}

