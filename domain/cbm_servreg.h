/*!
 * @brief
 *    The "Service Registry" holds information about
 *    available services (e.g., models, readers, filters) and
 *    offers a query mechanism that searches services that
 *    provide a certain target quantity.
 *
 *    @n
 *
 *    The "Service Discovery" attempts to find a service matching
 *    a request best.
 *
 * @author
 *    steffen klatt (created on: aug 15, 2016)
 */

#ifndef  CRABMEAT_SERVICEREGISTRY_H_
#define  CRABMEAT_SERVICEREGISTRY_H_

#include  "crabmeat-common.h"
#include  "json/cbm_json.h"
#include  "string/cbm_string.h"

#include  <list>

namespace cbm {

enum sreg_e
{
    SREG_NONE, SREG_MIN
};

typedef  std::string  service_name_t;

struct entity_t
{
    std::string  name;
    std::string  id;
};

template < typename _T >
struct _cbm_vector
{
    std::list< _T >  v;
    typedef  typename std::list< _T >::iterator  iterator;
    typedef  typename std::list< _T >::const_iterator  const_iterator;

    size_t  size() const
        { return  this->v.size(); }
    bool  empty() const
        { return  this->v.empty(); }
    void  clear()
        { this->v.clear(); }
    void  push_back( _T const &  _elem)
    {
        for ( iterator  elem = this->begin();
                    elem != this->end();  ++elem)
        {
            if ( elem->id == _elem.id)
                { return; }
        }
        this->v.push_back( _elem);
    }

    _T &  front()
        { return  this->v.front(); }
    void  pop_front()
        { this->v.pop_front(); }

    iterator  begin()
        { return  this->v.begin(); }
    iterator  end()
        { return  this->v.end(); }
    const_iterator  begin() const
        { return  this->v.begin(); }
    const_iterator  end() const
        { return  this->v.end(); }

};

struct entities_t : public _cbm_vector< cbm::entity_t >
    { };

typedef rapidjson::Value service_properties_t;
struct service_t
{
    service_t()
        : m( NULL) { }

    std::string  name;
    std::string  id;

    cbm::service_properties_t const *  m;
};

struct services_t : public _cbm_vector< cbm::service_t >
    { };

struct service_registry_t
{
    service_registry_t();
    ~service_registry_t();

    lerr_t  connect( char const *
            /*registry URL (e.g., JSON file) */);
    lerr_t  disconnect();

    cbm::string_t  serialize();

    cbm::services_t  query_providers( std::string const & /*entity name*/,
        lflags_t = SREG_NONE, int * = NULL /*rc*/) const;

    service_properties_t const *  query_service_properties(
        std::string /*service*/, int * /*rc*/) const;


    private:
        std::string  m_regurl;
        rapidjson::Document  m_reg;
        int  m_isconnected;
};

struct service_discover_t
{
    service_discover_t( cbm::service_registry_t const * = NULL);

    cbm::service_t  discover( std::string const & /*entity*/,
        cbm::services_t const & /*explicitly requested*/,
            int * = NULL /*rc*/) const;
    cbm::service_properties_t const *  service_properties(
            cbm::service_t const &) const;

    private:
        cbm::service_registry_t const *  m_reg;
};

struct workflow_compositer_t
{
    workflow_compositer_t( service_registry_t const *);

    cbm::services_t  resolve(
        cbm::entities_t const & /*target variables*/,
            cbm::services_t const & /*requested services*/, int * /*rc*/);

    private:
        cbm::service_discover_t  m_servd;
};

} /* namespace cbm */

#endif  /*  ! CRABMEAT_SERVICEREGISTRY_H_  */

