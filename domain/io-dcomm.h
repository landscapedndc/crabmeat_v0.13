/*!
 * @brief
 *    interface to inputs; communication interface for
 *    all i/o in monolithic build.
 *
 * @author
 *    steffen klatt (created on: apr 13, 2013)
 */

#ifndef  CBM_IO_DCOMM_H_
#define  CBM_IO_DCOMM_H_

#include  "crabmeat-common.h"

#include  "io/sink-handle.h"

#include  "time/cbm_time.h"
#include  "cpl/cbm_cpl.h"

#include  <vector>

namespace ldndc {

    class  input_class_client_base_t;
    class  input_class_srv_base_t;
}

namespace cbm {

class project_t;
class work_dispatcher_t;

class kspatial_t;
class ktemporal_t;
class CBM_API io_dcomm_t  :  public  cbm::client_object_t
{
    LDNDC_CLIENT_OBJECT(io_dcomm_t)
    public:
        io_dcomm_t();
        ~io_dcomm_t();
    private:
        /* hide */
        io_dcomm_t( io_dcomm_t const &);
        io_dcomm_t &  operator=( io_dcomm_t const &);

    public:
        /*!
         * @brief
         *    creates and opens project */
        lid_t const &  open_project();
        /*!
         * @brief
         *    set previously opened project; takes
         *    ownership of given project and
         *    frees its memory */
        lerr_t  set_project( cbm::project_t *);

        lerr_t  initialize_dispatcher(
                cbm::work_dispatcher_t * /*work dispatcher*/);

        /*** input stream interface ***/
        /*!
         * @brief
         *    trigger single source reset
         */
        lerr_t  reset_io( cbm::source_descriptor_t const *,
                    char const * /*type*/);
        /*!
         * @brief
         *    trigger full i/o reset
         */
        lerr_t  reset_io();

        /*!
         * @brief
         *    trigger single source update
         */
        lerr_t  update_io( cbm::source_descriptor_t const *,
                    char const * /*type*/);
        /*!
         * @brief
         *    trigger full i/o update
         */
        lerr_t  update_io();
        /*!
         * @brief
         *    trigger full i/o commit
         */
        lerr_t  commit_io();

        lid_t  get_source_descriptor(
                cbm::source_descriptor_t * /*buffer*/,
                cbm::source_descriptor_t const * /*kernel info*/,
                char const *  /*type*/) const;

        ldndc::input_class_client_base_t *  new_input_class(
            char const * /*type*/, cbm::source_descriptor_t const *);
        /*!
         * @brief
         *    delete an instance of core input class
         *    created via method @fn new_input_class
         */
        void  delete_input_class(
                ldndc::input_class_client_base_t const *);

        ldndc::input_class_srv_base_t *  fetch_input(
            char const *  _type,
            cbm::source_descriptor_t const *);
        ldndc::input_class_srv_base_t const *  fetch_input(
            char const *  _type,
            cbm::source_descriptor_t const *) const;

        /*!
         * @brief
         *    return list of object IDs of type
         *    @p _type to be aquired as dictated by
         *    the work dispatcher (optional)
         */
        std::vector< cbm::source_descriptor_t >
            object_id_list( char const * /*type*/,
                cbm::work_dispatcher_t * = NULL);
        /*!
         * @brief
         *    return list of object IDs of type
         *    setup to be aquired for this region,
         *    i.e. possibly prescribed by work
         *    dispatcher
         */
        std::vector< cbm::source_descriptor_t >  request_region(
                cbm::work_dispatcher_t * = NULL);

    public:
        /*** output stream interface ***/
        ldndc::sink_handle_t  sink_handle_acquire(
                cbm::source_descriptor_t const *, char const * /*type*/,
                char const * /*sink identifier*/, int /*sink index*/);
        lerr_t  sink_handle_release( ldndc::sink_handle_t *);

    private:
        cbm::project_t *  m_project; /*owner*/

    public:
        lerr_t  set_spatialcontroller( cbm::kspatial_t *);
        cbm::kspatial_t *  get_spatialcontroller();
    private: cbm::kspatial_t *  m_kspatial; /* not owner */

    public:
        lerr_t  set_temporalcontroller( cbm::ktemporal_t *);
        cbm::ktemporal_t *  get_temporalcontroller();
    private: cbm::ktemporal_t *  m_ktemporal; /* not owner */
};

} /* namespace cbm */

#endif  /*  !CBM_IO_DCOMM_H_  */

