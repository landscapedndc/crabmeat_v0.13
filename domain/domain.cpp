/*!
 * @author
 *    steffen klatt (created: 2009),
 *    edwin haas
 */

#include  "domain/domain.h"
#include  "kernel/cbm_kernel.h"
#include  "time/cbm_time.h"

#include  "domain/io-dcomm.h"
#include  "kernel/io-kcomm.h"

#include  "openmp/cbm_omp.h"

#include  "cbm_rtcfg.h"
#include  "stats/cbm_stats.h"

/***  LANDSCAPE  ***/

LDNDC_OBJECT_DEFN(cbm::domain_t)
cbm::domain_t::domain_t()
        : cbm::object_t(),
          m_kspatial( NULL), m_kspatial_iokcomm( NULL),
          m_ktemporal( NULL), m_ktemporal_iokcomm( NULL),
          m_iodcomm( NULL),
          m_isfinalized( 0),
          m_runtime( 0.0)
{
}

cbm::domain_t::~domain_t()
{
    this->finalize();

    if ( this->m_ktemporal)
        { this->m_ktemporal->delete_instance(); }

    if ( this->m_ktemporal_iokcomm)
        { this->m_ktemporal_iokcomm->delete_instance(); }

    if ( this->m_kspatial)
        { this->m_kspatial->delete_instance(); }

    if ( this->m_kspatial_iokcomm)
        { this->m_kspatial_iokcomm->delete_instance(); }

    if ( this->m_iodcomm)
        { this->m_iodcomm->delete_instance(); }
}

void
cbm::domain_t::select_systemcomponent( char const *  _component,
                char const *  _name, cbm::kernel_t *  _kernel)
{
    if ( !_component || !_name)
        { return; }

    _SystemComponent  syscomp;
    syscomp.name = _name;
    syscomp.kernel = _kernel;
    if ( this->m_systemcomponents.find( _component)
            != this->m_systemcomponents.end())
        { CBM_LogWarn( "Overwriting system component '", _component, "'"); }
    this->m_systemcomponents[_component] = syscomp;
}

#if 0
lerr_t
cbm::domain_t::implement_spatialhandler()
{
    /* configure spatial handler */
    SpatialHandlerRunLevelConfigureData  rl_cfgdata;
    /* initialize spatial handler */
    SpatialHandlerRunLevelInitializeData  rl_inidata;

    return  LDNDC_ERR_OK;
}
lerr_t
cbm::domain_t::implement_temporalhandler()
{
    /* configure temporal handler */
    TemporalHandlerRunLevelConfigureData  rl_cfgdata;
    /* initialize temporal handler */
    TemporalHandlerRunLevelInitializeData  rl_inidata;

    return  LDNDC_ERR_OK;
}
#endif

lerr_t
cbm::domain_t::initialize( io_dcomm_t *  _iodcomm,
            cbm::work_dispatcher_t *  _dispatcher)
{
    CBM_LogVerbose( "region initialization started");
    if ( !_iodcomm)
    {
        CBM_LogError( "Got no input :-(");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    /* takes ownership */
    this->m_iodcomm = _iodcomm;

    lerr_t rc_temporal = this->m_initialize_temporal_controller();
    if ( rc_temporal)
        { return LDNDC_ERR_RUNTIME_ERROR; }
    lerr_t rc_spatial = this->m_initialize_spatial_controller();
    if ( rc_spatial)
        { return LDNDC_ERR_RUNTIME_ERROR; }


    int region_size = 0;
    lerr_t  rc_kspatial_initdomain =
            this->initialize_domain( _dispatcher, &region_size);
    if ( rc_kspatial_initdomain)
        { return  LDNDC_ERR_FAIL; }


    this->m_isfinalized = 0;

    CBM_LogVerbose( "region initialization successful  ",
            "[region-size=",region_size,"]");
    return  LDNDC_ERR_OK;
}


lerr_t
cbm::domain_t::m_initialize_temporal_controller()
{
    /* Temporal Controller */
    _SystemComponent  temporalcontroller;
    if ( this->m_systemcomponents.find( "KTemporal")
            != this->m_systemcomponents.end())
        { temporalcontroller = this->m_systemcomponents["KTemporal"]; }
    if ( temporalcontroller.kernel)
        { this->m_ktemporal = static_cast< ktemporal_t * >( temporalcontroller.kernel); }
    else if ( temporalcontroller.name == "") /*default*/
        { this->m_ktemporal = cbm::ktemporal_t::new_instance( 0); }
    else
    {
        CBM_LogError( "Using alternative TemporalController '",temporalcontroller.name,"' not supported.");
        return LDNDC_ERR_FAIL;
    }

    if ( !this->m_ktemporal)
        { return  LDNDC_ERR_NOMEM; }
    CBM_LogDebug( "Created TemporalController node  [node='",this->m_ktemporal->name(),"']");

    this->m_ktemporal_iokcomm = io_kcomm_t::new_instance( 0);
    if ( !this->m_ktemporal_iokcomm)
        { return  LDNDC_ERR_NOMEM; }
    this->m_iodcomm->set_temporalcontroller( this->m_ktemporal);
    this->m_ktemporal_iokcomm->set_io_service( this->m_iodcomm);

    cbm::RunLevelArgs  ra_cfg( RL_CONFIGURE);
    ra_cfg.iokcomm = this->m_ktemporal_iokcomm;
    ra_cfg.cfg = CBM_LibRuntimeConfig.cfg;
    ra_cfg.clk = CBM_LibRuntimeConfig.clk;
    lerr_t  rc_ktemporal_conf = this->m_ktemporal->configure( &ra_cfg);
    if ( rc_ktemporal_conf)
        { return  LDNDC_ERR_FAIL; }

    cbm::RunLevelArgs  ra_ini( RL_INITIALIZE);
    ra_ini.iokcomm = this->m_ktemporal_iokcomm;
    ra_ini.cfg = CBM_LibRuntimeConfig.cfg;
    ra_ini.clk = CBM_LibRuntimeConfig.clk;
    lerr_t  rc_ktemporal_init = this->m_ktemporal->initialize( &ra_ini);
    if ( rc_ktemporal_init)
        { return  rc_ktemporal_init; }

    return LDNDC_ERR_OK;
}

lerr_t
cbm::domain_t::m_initialize_spatial_controller()
{
    /* Spatial Controller */
    _SystemComponent  spatialcontroller;
    if ( this->m_systemcomponents.find( "KSpatial")
            != this->m_systemcomponents.end())
        { spatialcontroller = this->m_systemcomponents["KSpatial"]; }
    if ( spatialcontroller.kernel)
        { this->m_kspatial = static_cast< kspatial_t * >( spatialcontroller.kernel); }
    else if ( spatialcontroller.name == "") /*default*/
        { this->m_kspatial = cbm::kspatial_t::new_instance( 0); }
    else
    {
        CBM_LogError( "Using alternative spatialcontroller '",spatialcontroller.name,"' not supported.");
        return LDNDC_ERR_FAIL;
    }

    if ( !this->m_kspatial)
        { return  LDNDC_ERR_NOMEM; }
    CBM_LogDebug( "Created domain root node  [node='",this->m_kspatial->name(),"']");

    this->m_kspatial_iokcomm = io_kcomm_t::new_instance( 0);
    if ( !this->m_kspatial_iokcomm)
        { return  LDNDC_ERR_NOMEM; }

    this->m_kspatial->set_modelcompositor(
        this->m_systemcomponents["ModelCompositor"].name.c_str());
    this->m_iodcomm->set_spatialcontroller( this->m_kspatial);
    this->m_kspatial_iokcomm->set_io_service( this->m_iodcomm /* "ldndc:///" */);

    cbm::RunLevelArgs  ra_cfg( RL_CONFIGURE);
    ra_cfg.iokcomm = this->m_kspatial_iokcomm;
    ra_cfg.cfg = CBM_LibRuntimeConfig.cfg;
    ra_cfg.clk = CBM_LibRuntimeConfig.clk;
    lerr_t  rc_kspatial_conf = this->m_kspatial->configure( &ra_cfg);
    if ( rc_kspatial_conf)
        { return  LDNDC_ERR_FAIL; }

    cbm::RunLevelArgs  ra_ini( RL_INITIALIZE);
    ra_ini.iokcomm = this->m_kspatial_iokcomm;
    ra_ini.cfg = CBM_LibRuntimeConfig.cfg;
    ra_ini.clk = CBM_LibRuntimeConfig.clk;
    lerr_t  rc_kspatial_init = this->m_kspatial->initialize( &ra_ini);
    if ( rc_kspatial_init)
        { return  rc_kspatial_init; }

    return LDNDC_ERR_OK;
}


lerr_t
cbm::domain_t::initialize_domain(
        cbm::work_dispatcher_t * _dispatcher, int * _region_size)
{
#ifndef  CRABMEAT_KERNELS
    CBM_LogWarn( "build with kernels disabled.");
    CRABMEAT_FIX_UNUSED(_dispatcher);
    return  LDNDC_ERR_OK;
#else
    this->m_iodcomm->initialize_dispatcher( _dispatcher);
    /* get input class list (setups) as vector (openmp: index iterable container type) */
    std::vector< cbm::source_descriptor_t >
        region_def( this->m_iodcomm->request_region( _dispatcher));
    if ( _region_size)
        { *_region_size = region_def.size(); }

    /* sanity check */
    if ( region_def.size() == 0)
    {
        CBM_LogError( "no kernels in partition :-(  abort.");
        return  LDNDC_ERR_FAIL;
    }

    CBM_LogVerbose( "kick-starting initial region construction",
            "  [#domain=", region_def.size(),"]");
    if ( region_def.size() == 0)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    lerr_t  rc_enq = this->m_domain_from_input(
                    &region_def[0], region_def.size());
    if ( rc_enq)
    {
        this->finalize();
        CBM_LogError( "initial domain construction failed");
        return LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
#endif  /*  !CRABMEAT_KERNELS  */
}

lerr_t
cbm::domain_t::m_domain_from_input(
        cbm::source_descriptor_t const *  _kset, size_t  _n_kset)
{
    std::string const  ModelCompositor =
        this->m_systemcomponents["ModelCompositor"].name;
    kmount_t  kmount( this->m_kspatial, this->m_kspatial);
    for ( size_t  k = 0;  k < _n_kset;  ++k)
    {
        CBM_LogDebug( "enqueuing ldndc-setup mount request",
                "  [setup=",_kset[k].block_id,"]");
        lerr_t  rc_kmount =
            kmount.enqueue_for_mount( ModelCompositor.c_str(),
                _kset[k].block_id, _kset[k].source_identifier,
                NULL, NULL);
        if ( rc_kmount)
        {
            this->finalize();
            CBM_LogError( "initial domain construction failed; ",
                "failed to enqueue kernel ",
                "'", ModelCompositor, "' with ID ", _kset[k].block_id);
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
cbm::domain_t::launch( cbm::td_scalar_t  _sse_stop)
{
    cbm::td_scalar_t const  sse_final = CBM_LibRuntimeConfig.clk->sse_final();
    cbm::td_scalar_t const  sse_stop =
        ( (_sse_stop==0) || (_sse_stop>sse_final) ) ? sse_final : _sse_stop;

    lerr_t  rc_check = LDNDC_ERR_OK;
    int  rc_launch = this->check_if_launching_makes_sense( sse_stop, &rc_check);
    if ( rc_launch)
        { return  rc_check; }

#ifdef  CRABMEAT_MONITOR_RESOURCES
    /* total runtime */
    TIC(_lruntime);
    this->m_runtime = _lruntime;
#endif

    /* kick-off simulation */
    rc_launch = this->m_ktemporal->launch( sse_stop);

#ifdef  CRABMEAT_MONITOR_RESOURCES
    TOC(this->m_runtime);
#endif

    if ( this->number_of_alive_kernels() == 0)
        { CBM_LogError( "domain_t::launch(): ", "no more kernels alive :("); }

    if ( rc_launch || !this->is_complete_simulation())
        { return  LDNDC_ERR_RUN_INCOMPLETE; }
    return  LDNDC_ERR_OK;
}

int
cbm::domain_t::check_if_launching_makes_sense(
        cbm::td_scalar_t /*sse stop*/, lerr_t *  _rc)
{
    if ( CBM_LibRuntimeConfig.clk->is_final())
    {
        CBM_LogInfo( "(re)launching simulation past end of schedule. goodbye.");
        *_rc = LDNDC_ERR_OK;
        return  1;
    }
    if ( ! this->m_kspatial->is_valid_simulation())
    {
        CBM_LogInfo( "launching simulation without active kernels ;-)");
        *_rc = LDNDC_ERR_FAIL;
        return  1;
    }

    return  0;
}

lerr_t
cbm::domain_t::finalize()
{
    if ( this->m_isfinalized)
        { return  LDNDC_ERR_OK; }
    this->m_isfinalized = 1;

    if ( this->m_kspatial)
        { this->m_kspatial->finalize( NULL); }
    if ( this->m_ktemporal)
        { this->m_ktemporal->finalize( NULL); }
    return  LDNDC_ERR_OK;
}

size_t
cbm::domain_t::number_of_kernels() const
    { return  this->m_kspatial->number_of_kernels(); }
size_t
cbm::domain_t::number_of_alive_kernels() const
    { return  this->m_kspatial->number_of_alive_kernels(); }
size_t
cbm::domain_t::number_of_valid_kernels() const
{ return  this->m_kspatial->number_of_valid_kernels(); }


#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
int
cbm::domain_t::create_checkpoint()
{
    int  create_fails = 0;

//    for ( size_t  c = 0;  c < this->number_of_kernels();  ++c)
//    {
//        if ( this->m_state.kernels[c])
//        {
//            int  rc_checkpoint =
//                       this->kernels[c]->create_checkpoint();
//            if ( rc_checkpoint)
//            {
//                ++create_fails;
//            }
//        }
//    }
    return  create_fails;
}
int
cbm::domain_t::restore_checkpoint( ldate_t)
{
    int  restore_fails( 0);

//    for ( size_t  c = 0;  c < this->n_kernels;  ++c)
//    {
//        if ( this->kernels[c])
//        {
//            int  rc_checkpoint =
//                       this->kernels[c]->restore_checkpoint( _ldate);
//            if ( rc_checkpoint)
//            {
//                ++restore_fails;
//            }
//        }
//    }
    return  restore_fails;
}
#endif  /* CRABMEAT_OUTPUTS_SERIALIZE */

#ifdef  CRABMEAT_ONLINECONTROL
int
cbm::domain_t::process_request(
        lreply_t *  _reply,  lrequest_t const *  _request)
{
    int  rc_req = -1;

    if (( _request->command == "GET")
            || ( _request->command == "PUT"))
    {
        rc_req = this->send_request_to_kernel( _reply, _request);
    }
#ifdef  CRABMEAT_OUTPUTS_SERIALIZE
    else if ( _request->command == "CHECKPOINT_CREATE")
    {
        rc_req = this->create_checkpoint();
    }
    else if ( _request->command == "CHECKPOINT_RESTORE")
    {
// sk:todo        rc_req = this->restore_checkpoint( __a_date__);
    }
#endif  /* CRABMEAT_OUTPUTS_SERIALIZE */
    else if ( _request->command == "SLEEP")
    {
        /* put kernel to sleep */
    }
    else if ( _request->command == "PING")
    {
        if ( _request->uri.subpath( 1))
        {
            rc_req = send_request_to_kernel( _reply, _request);
        }
        else
        {
            _reply->data.set_string( ":return:", "PONG");
            rc_req = 0;
        }
    }
    else
    {
        /* no op */
    }

    return  rc_req;
}

int
cbm::domain_t::send_request_to_kernel(
        lreply_t *  _reply, lrequest_t const *  _request)
{
// sk:dbg    CBM_LogVerbose( "passing request to kernel",
// sk:dbg       "  [request=",_request->command,",from=",_request->get_uri(),"]");
    kernel_t *  K = NULL;
    char const *  k_path = _request->uri.subpath( 1);
    if ( k_path)
    {
        char const *  k_id_str = strchr( k_path, '.');
        if ( k_id_str)
        {
            lid_t  k_id = invalid_lid;
            cbm::s2n( k_id_str+1, &k_id);
            if ( k_id == invalid_lid)
            {
                CBM_LogError( "kernel <",k_id_str, "> not found",
                    "  [uri=",_request->get_uri(),"]");
            }
            else
            {
                K = this->kernel( k_id);
            }
        }
    }
    if ( K)
    {
        lrequest_t  req = _request->subrequest();
        int  rc_req = K->process_request( _reply, &req);
        if ( rc_req >= 0)
        {
            return  0;
        }
    }
    return  -1;
}
#endif  /* CRABMEAT_ONLINECONTROL */

int cbm::domain_t::cputime_summary(
            cbm::cputime_summary_t * _cs) const
{
    int n = 0;
#ifdef  CRABMEAT_KERNEL_STATISTICS
    cbm::cputime_monitor_t ct;

    _cs->kmin = invalid_lid;
    _cs->kmax = invalid_lid;
    for ( const_iterator  k = this->cbegin();  k != this->cend();  ++k)
    {
        kernel_t const *  K = *k;
        if ( !K || !K->is_active())
            { continue; }
        ++n;

        int  rc_c = ct.update( K->stats.cpu_total());
        if ( rc_c == -1)
            { _cs->kmin = K->object_id(); }
        else if ( rc_c == 1)
            { _cs->kmax = K->object_id(); }
        else if ( rc_c == 0)
            { _cs->kmin = _cs->kmax = K->object_id(); }
    }
    _cs->nb = ct.nb_samples();
    _cs->min = ct.min()*1.0e-09;
    _cs->max = ct.max()*1.0e-09;
    _cs->total = ct.total()*1.0e-09;
#else
    CRABMEAT_FIX_UNUSED(_cs);
#endif
    return n;
}

int cbm::domain_t::walltime_summary(
            cbm::walltime_summary_t * _ws) const
{
    int n = 0;
#ifdef  CRABMEAT_KERNEL_STATISTICS
    cbm::walltime_monitor_t wt;

    _ws->kmin = invalid_lid;
    _ws->kmax = invalid_lid;
    for ( const_iterator  k = this->cbegin();  k != this->cend();  ++k)
    {
        kernel_t const *  K = *k;
        if ( !K || !K->is_active())
            { continue; }
        ++n;

        int  rc_w = wt.update( K->stats.wall_total());
        if ( rc_w == -1)
            { _ws->kmin = K->object_id(); }
        else if ( rc_w == 1)
            { _ws->kmax = K->object_id(); }
        else if ( rc_w == 0)
            { _ws->kmin = _ws->kmax = K->object_id(); }
    }
    _ws->nb = wt.nb_samples();
    _ws->min = wt.min()*1.0e-09;
    _ws->max = wt.max()*1.0e-09;
    _ws->total = wt.total()*1.0e-09;
#else
    CRABMEAT_FIX_UNUSED(_ws);
#endif
    return n;
}

int cbm::domain_t::kernel_resource_stats( kernels_stats_t & _kstats) const
{
#ifdef  CRABMEAT_KERNEL_STATISTICS
    for ( const_iterator  k = this->cbegin();  k != this->cend();  ++k)
    {
        kernel_t const *  K = *k;
        if ( !K || !K->is_active())
            { continue; }

        cbm::resources_statistics_t const *  k_stat = &K->stats;
        if ( !k_stat)
            { continue; }

        kernel_stats_t  kstat;
        kstat.modelname = K->model_name();
        kstat.id = K->object_id();
        kstat.rank = K->rank();
        kstat.cpumin = k_stat->cpu_min()*1.0e-09;
        kstat.cpumax = k_stat->cpu_max()*1.0e-09;
        kstat.cpuavg = k_stat->cpu_avg()*1.0e-09;
        kstat.cputotal = k_stat->cpu_total()*1.0e-09;

        _kstats.push_back( kstat);
    }
#endif
    return _kstats.size();
}

void cbm::domain_t::memory_summary( memory_summary_t * _ms) const
{
#ifdef  CRABMEAT_KERNEL_STATISTICS
    cbm::memory_usage_t mu;
    cbm::memory_usage( &mu);

    _ms->prog_sz = (mu.size*4)/1024;
    _ms->lib_sz = mu.lib/256;
    _ms->data_sz = mu.data/256;
#else
    CRABMEAT_FIX_UNUSED(_ms);
#endif
}

int cbm::domain_t::have_kernel_resource_statistics() const
    { return cbm::have_resources(); }

