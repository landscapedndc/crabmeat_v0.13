/*!
 * @brief
 *    attempts to detect machine architecture
 *
 * @author 
 *    steffen klatt (created on: sep 02, 2012)
 */

#ifndef  CRABMEAT_ARCH_H_
#define  CRABMEAT_ARCH_H_

/* defines __WORDSIZE */
#include  <limits.h>

/* machine architecture detection extracted from:
 * https://qt.gitorious.org/qt/qtbase/blobs/master/src/corelib/global/qprocessordetection.h
 */

/* intel x86 */
#if  defined(i386) || defined(__i386) || defined(__i386__) || defined(_M_IX86) || defined(__X86__) || defined(_X86_)
#  define  CRABMEAT_ARCH_x86
#  define  CRABMEAT_ARCH_x86_32
#  define  CRABMEAT_ARCH_IS_32

/* intel x86 64 */
#elif  defined(x86_64) || defined(__x86_64) || defined(__x86_64__) || defined(__amd64) || defined(__amd64__) || defined(_M_X64) || defined(_M_AMD64)
#  define  CRABMEAT_ARCH_x86
#  define  CRABMEAT_ARCH_x86_64
#  define  CRABMEAT_ARCH_IS_64

/* arm64 */
#elif  defined(arm64) || defined(__arm64) || defined(__arm64__)
#  define  CRABMEAT_ARCH_arm
#  define  CRABMEAT_ARCH_arm_64
#  define  CRABMEAT_ARCH_IS_64

/* ia64 (itanium) */
#elif  defined(__ia64) || defined(__ia64__) || defined(_IA64) || defined(__IA64__) || defined(_M_IA64) || defined(__itanium__)
#  define  CRABMEAT_ARCH_ia
#  define  CRABMEAT_ARCH_ia_64
#  define  CRABMEAT_ARCH_IS_64

/* power pc */
#elif  defined(__ppc__) || defined(__ppc) || defined(__powerpc) || defined(__powerpc__) || defined(__POWERPC__) || defined(_ARCH_COM) || defined(_ARCH_PWR) || defined(_ARCH_PPC) || defined(_M_MPPC) || defined(_M_PPC)
#  define  CRABMEAT_ARCH_ppc
#  if defined(__ppc64__) || defined(__powerpc64__) || defined(__64BIT__)
#    define  CRABMEAT_ARCH_ppc_64
#    define  CRABMEAT_ARCH_IS_64
#  else
#    define  CRABMEAT_ARCH_ppc_32
#    define  CRABMEAT_ARCH_IS_32
#  endif

/* sun sparc */
#elif  defined(__sparc__) || defined(__sparc)
#  define  CRABMEAT_ARCH_sparc
#  if  __WORDSIZE == 64
#    define  CRABMEAT_ARCH_sparc_64
#    define  CRABMEAT_ARCH_IS_64
#  else
#    define  CRABMEAT_ARCH_sparc_32
#    define  CRABMEAT_ARCH_IS_32
#  endif

/* arm */
#elif  defined(__arm__)
#  define  CRABMEAT_ARCH_arm
#  if  __WORDSIZE == 64
#    define  CRABMEAT_ARCH_arm_64
#    define  CRABMEAT_ARCH_IS_64
#  else
#    define  CRABMEAT_ARCH_arm_32
#    define  CRABMEAT_ARCH_IS_32
#  endif

/* ? */
#else
        /* well, this could be a hell of a ride ... */
#endif

/* hacky replacement for missing stdint.h in msvc */
#ifndef  __WORDSIZE
#  include  <limits.h>
#  if    defined(CRABMEAT_ARCH_IS_32) || defined(CRABMEAT_OS_MSWIN32) || ((UINT_MAX) == (ULONG_MAX))
#    define  __WORDSIZE  32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  __WORDSIZE  64
#  else  
#    error  i had problems determining your machines word size
#  endif  /*  CRABMEAT_OS_MSWINx ..  */
#endif  /*  !__WORDSIZE  */


#endif  /*  !CRABMEAT_ARCH_H_  */

