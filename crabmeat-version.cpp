
#include  "crabmeat-version.h"


#include  "crabmeat-version.h.inc"
char const *  crabmeat_package_name()
{
    return CRABMEAT_PACKAGE_NAME;
}
char const *  crabmeat_package_name_short()
{
    return CRABMEAT_PACKAGE_NAME_SHORT;
}
char const *  crabmeat_package_version()
{
    return CRABMEAT_PACKAGE_VERSION;
}
char const *  crabmeat_package_build_timestamp()
{
    return CRABMEAT_PACKAGE_BUILD_TIMESTAMP;
}

#include  "crabmeat-revision.h.inc"
char const *  crabmeat_package_revision()
{
    return CRABMEAT_PACKAGE_REVISION;
}

#include  "crabmeat-build-configuration.h.inc"
char const *
crabmeat_package_build_configuration()
{
    return CRABMEAT_PACKAGE_BUILD_CONFIGURATION;
}

