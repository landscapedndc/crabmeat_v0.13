#!/bin/bash

pack_dir="`dirname $0`"
if [[ $pack_dir != /* ]]
then
    pack_dir=`pwd`
fi
. $pack_dir/make-package.conf

target="$1"
pack_bin="$pack_dir/make-package-${target}.sh"
if [ -z "${target}" -o ! -x "$pack_bin" ]
then
    ## TODO  determine target (e.g. uname)
    echo "usage: `basename $0` <target>" >&2
    echo "  targets: { linux32, linux64, mac32, mac64, win32, win64, arm32}" >&2
    exit 1
fi

build_dir="$pack_dir/build-$target"
mkdir -p "$build_dir"
if [ ! -d "$build_dir" ]
then
    echo "failed to create build directory" >&2
    exit 2
fi
cd $build_dir
source_dir="$build_dir/../.."
if [ -r "$pack_dir/$cmake_buildoptions_dir/cmake-options-${target}.cmake" ]
then
    cmake_options="-DLDNDC_OPTIONS_OPTIONS=$pack_dir/$cmake_buildoptions_dir/cmake-options-${target}.cmake"
fi
cmake -G 'Unix Makefiles' $cmake_options "$source_dir"

make -j4
cd -

pack_info=`$pack_dir/make-package-$target.sh "$build_dir" "$source_dir" . | $egrep_bin '@publish:'`
echo $pack_info

rm -r "$build_dir"

pack_package_name="`echo "$pack_info" | $egrep_bin -m 1 -o '@publish:PACK_ARCHIVE=.*$' | sed 's/.*=//'`"
pack_package_version="`echo "$pack_info" | $egrep_bin -m 1 -o '@publish:TARGET_VERSION=.*$' | sed 's/.*=//'`"
pack_fatalerrors="`echo "$pack_info" | $egrep_bin -m 1 -o '@publish:FATAL=.*$' | sed 's/.*=//'`"
if [ ! "_$pack_fatalerrors" = "_" ]
then
    echo "failed to pack distribution" >&2
else
    $pack_dir/upload-packages.sh "$pack_package_name":"$pack_package_version"
    rm -r "$pack_package_name" 
fi

