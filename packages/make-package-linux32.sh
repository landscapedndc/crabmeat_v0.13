#!/bin/bash

. `dirname $0`/make-package.conf
target="ldndc-linux32"

dl_suffix=".so"


target_version=""
target_binary_dir="${1:-$LDNDC_BUILD_ROOT/linux32}"
if [ -r "$target_binary_dir/$ldndcversionfile" ]
then
	target_version=`$cat_bin "$target_binary_dir/$ldndcversionfile"`
        target="ldndc-${target_version}.linux32"
fi
target_source_dir="${2:-$LDNDC_ROOT}"
target_pack_dir="${3:-$target_source_dir}"
target_pack_archive_dir="$target_pack_dir/$target"
buildtype="" #"${3:-""}"

. `dirname $0`/make-package-common.sh


## platform-specific libraries
echo "copying target-specific libraries..."
if [ -d "$target_binary_dir/ui" ]
then
        $cp_bin "$target_source_dir/ui/jldndc/jldndc.sh" "$target_pack_archive_dir"
fi

## create archive
archive_name="$target.$this_date.tar.bz2"
echo "creating archive... [$archive_name]"
$tar_bin -C $target_pack_dir -cjf "$target_pack_dir/$archive_name" "$target"
$rm_bin -r "$target_pack_archive_dir"

echo "@publish:PACK_ARCHIVE=$target_pack_dir/$archive_name"
echo "@publish:TARGET_VERSION=$target_version"
echo
echo "done."

