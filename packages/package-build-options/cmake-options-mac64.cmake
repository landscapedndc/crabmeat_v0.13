# vim: ft=cmake

#set( LDNDC_RELEASE OFF )

## parallelism
#set( PARALLEL_OPENMP_ENABLE_ldndc ON )

## models
set( MODELS_ENABLE_CMF_ldndc ON )

## libraries
set( LIBS_ENABLE_JSON_ldndc ON )

## kernels
set( KERNELS_ENABLE_CMF_ldndc ON )
set( KERNELS_ENABLE_NULL_ldndc OFF )
set( KERNELS_ENABLE_VALIDATE_ldndc OFF )
## utils
set( UTILS_ENABLE_ldndc ON )
## tools
set( TOOLS_ENABLE_ldndc ON )
## tests
set( TESTS_ENABLE_ldndc ON )

## features
#set( FEATURES_MONITOR_RESOURCES_ldndc ON )
#set( FEATURES_SIGNAL_HANDLING_ldndc ON )

## ui
set( UI_ENABLE_ldndc OFF )
set( UI_ENABLE_ldndc OFF )
set( TARGETS_C_API_ldndc OFF )

