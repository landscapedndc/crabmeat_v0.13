#!/bin/bash

. `dirname $0`/make-package.conf

package_list="$@"

for this_pkg in $package_list
do
    pkg=`echo "$this_pkg:" | cut -f1 -d':'`
    pkg_version=`echo "$this_pkg:" | cut -f2 -d':'`
    if [ "_$pkg_version" = "_" ]
    then
    	pkg_version="-"
    fi
    pkg_string="\\&nbsp;\\&nbsp;\\&nbsp;\\&nbsp;[${pkg_version}]"
    
    echo  "uploading package '$pkg' [$pkg_version]"
    case $pkg in
    
        *arm32*)
            pkg_pattern="Raspian 32bit"
            pkg_latest="ldndc-latest.arm32.tar.bz2"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/arm32
            $rm_bin $pkg_latest
            ;;
        
        *linux32*)
            pkg_pattern="Linux 32bit"
            pkg_latest="ldndc-latest.linux32.tar.bz2"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/linux32
            $rm_bin $pkg_latest
            ;;
        
        *linux64*)
            pkg_pattern="Linux 64bit"
            pkg_latest="ldndc-latest.linux64.tar.bz2"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/linux64
            $rm_bin $pkg_latest
            ;;
        
        *mac32*)
            pkg_pattern="MacOS X 32bit"
            pkg_latest="ldndc-latest.mac32.zip"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/mac32
            $rm_bin $pkg_latest
            ;;
        *mac64*)
            pkg_pattern="MacOS X 64bit"
            pkg_latest="ldndc-latest.mac64.zip"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/mac64
            $rm_bin $pkg_latest
            ;;
        
        *win32*)
            pkg_pattern="MS Windows 32bit"
            pkg_latest="ldndc-latest.win32.zip"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/win32
            $rm_bin $pkg_latest
            ;;
        *win64*)
            pkg_pattern="MS Windows 64bit"
            pkg_latest="ldndc-latest.win64.zip"
            $ln_bin -s $pkg $pkg_latest
            $scp_bin $pkg $pkg_latest $upload_target/packages/win64
            $rm_bin $pkg_latest
            ;;
        *)
	    pkg_pattern=""
            echo "don't know how to handle package.. [package=$pkg]"
            ;;
    esac
    
    if [ "$pkg_version" != "-" -a ! -z "$pkg_pattern" ]
    then
        $ssh_bin $downloadhostname sed --in-place "\"s/${pkg_pattern}.*\]/${pkg_pattern}${pkg_string}/\"" ${downloadwebsite}
    fi
done

