#!/bin/bash

if [ -z "$1" ]
then
    echo "usage: `basename $0` <binary dir> [<source dir> [<archive dir> [<build-type>]]]" >&2
    exit 2
fi

if [ ! -d "$target_binary_dir" ]
then
    echo "error: given target source directory does not exist [$target_binary_dir]" >&2
    exit 1
fi

$mkdir_bin -p "$target_pack_archive_dir"
if [ ! -d "$target_pack_archive_dir" ]
then
    echo "error: given package root directory does not exist [$target_pack_archive_dir]" >&2
    exit 1
fi

$mkdir_bin -p "$target_pack_archive_dir/bin" "$target_pack_archive_dir/doc" "$target_pack_archive_dir/lib" "$target_pack_archive_dir/parameters" "$target_pack_archive_dir/projects"

## binaries
echo "[II]  copying binaries..."
for bin in `find "$target_binary_dir/bin/$buildtype/" -maxdepth 1 -type f -name "*$bin_suffix"`
do
    $cp_bin "$bin" "$target_pack_archive_dir/bin/"
done
echo "[II]  copying tools..."
if [ -d "$target_binary_dir/bin/tools" ]
then
    $mkdir_bin -p "$target_pack_archive_dir/bin/tools"
    $cp_bin "$target_binary_dir/bin/tools/$buildtype/"*$bin_suffix "$target_pack_archive_dir/bin/tools/"
fi
echo "[II]  copying utils..."
if [ -d "$target_binary_dir/bin/utils" ]
then
    $mkdir_bin -p "$target_pack_archive_dir/bin/utils"
    $cp_bin "$target_binary_dir/bin/utils/$buildtype/"*$bin_suffix "$target_pack_archive_dir/bin/utils/"
fi
echo "[II]  copying tests..."
if [ -d "$target_binary_dir/bin/test" ]
then
    $mkdir_bin -p "$target_pack_archive_dir/bin/test"
    $cp_bin "$target_binary_dir/bin/test/$buildtype/"*$bin_suffix "$target_pack_archive_dir/bin/test/"
fi

## libraries
echo "[II]  copying libraries..."
$cp_bin "$target_binary_dir/lib/$buildtype/"*$dl_suffix "$target_pack_archive_dir/lib/" 2>/dev/null
if [ -d "$target_binary_dir/ui" ]
then
    $cp_bin "$target_binary_dir/lib/jldndc/$buildtype/"*$dl_suffix "$target_pack_archive_dir/lib/" 2>/dev/null
    $cp_bin "$target_binary_dir/ui/jldndc/"*.jar "$target_pack_archive_dir/lib/"
    $cp_bin "$target_source_dir/ui/jldndc/jldndc-help.txt" "$target_pack_archive_dir/"
    $cp_bin "$target_source_dir/ui/jldndc/share/"* "$target_pack_archive_dir/lib/"
fi



## parameters
echo "[II]  copying parameters..."
$cp_bin "$target_source_dir/examples/projects/example/parameters/"* "$target_pack_archive_dir/parameters/"
if [ -r "$BRANCH_PACK_RESOURCES_PATH/Lresources" ]
then
    $cp_bin "$BRANCH_PACK_RESOURCES_PATH/Lresources" "$target_pack_archive_dir/"
elif [ -r "$target_binary_dir/Lresources" ]
then
    $cp_bin "$target_binary_dir/Lresources" "$target_pack_archive_dir/"
elif [ -r "$HOME/.ldndc/Lresources" ]
then
    ## fallback, if generating did not work (e.g. cross-compiling)
    echo "[WW]  falling back to user's Lresources!"
    $cp_bin "$HOME/.ldndc/Lresources" "$target_pack_archive_dir/"
else
    echo "[WW]  missing Lresources!"
	echo "@publish:FATAL=Lresources missing"
fi
if [ -r "$target_pack_archive_dir/Lresources" -a -r "$target_binary_dir/version" ]
then
	ldndc_version="`cat "$target_binary_dir/version"`"
	lresources_version="`$egrep_bin -m 1 '__system__.Lresources.targetversion\ *=.*$' "$target_pack_archive_dir/Lresources" | sed 's/.*=\ "\(.*\)"/\1/'`"
	if [ "_$ldndc_version" = "_" -o "_$ldndc_version" != "_$lresources_version" ]
	then
		echo "@publish:FATAL=Lresources mismatch [$ldndc_version vs. $lresources_version]"
	fi
fi

## example inputs
echo "[II]  copying examples..."
$cp_bin "$target_source_dir/packages/ldndc.conf" "$target_pack_archive_dir/"
for ex in arable/DE_gebesee forest/DE_hoeglwald forest/DE_hoeglwald/DE_hoeglwald_beech forest/DE_hoeglwald/DE_hoeglwald_spruce grassland/UK_easterbush #example
do
    $mkdir_bin -p "$target_pack_archive_dir/projects/$ex"
    $cp_bin $target_source_dir/examples/projects/$ex/*.txt $target_source_dir/examples/projects/$ex/*.xml "$target_pack_archive_dir/projects/$ex/" 2>/dev/null
done

echo "[II]  copying documentation..."
$cp_bin "$target_source_dir/examples/projects/run-example.txt" "$target_pack_archive_dir/"
$cp_bin "$target_source_dir/doc/conffile.txt" "$target_pack_archive_dir/doc/"
if [ -e "$latex_bin" ]
then
    cd "$target_source_dir/doc/usersguide"
    echo "[II]  compiling Users Guide..."
	bash ./ldndc-usersguide-make.sh pdf
    rctex=$?
    cd -
    if [ $rctex -eq 0 ]
    then
        echo "[II]  copying Users Guide..."
        $cp_bin "$target_source_dir/doc/usersguide/${userguide_document}" "$target_pack_archive_dir/doc/"
    else
        echo "[EE]  failed to compile Users Guide" >&2
    fi
fi
#$mkdir_bin -p "$target_pack_archive_dir/doc/publications"
#$cp_bin "$target_source_dir/doc/publications/"*.pdf "$target_pack_archive_dir/doc/publications"

## adding version information
echo "`$svnversion_bin $target_source_dir` (`$date_bin`)" > "$target_pack_archive_dir/revision"

