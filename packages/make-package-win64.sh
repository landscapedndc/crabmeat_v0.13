#!/bin/bash

. `dirname $0`/make-package.conf
target="ldndc-win64"
bin_suffix=".exe"
dl_suffix=".dll"


target_version=""
target_binary_dir="${1:-$LDNDC_BUILD_ROOT/win64}"
if [ -r "$target_binary_dir/$ldndcversionfile" ]
then
	target_version=`$cat_bin "$target_binary_dir/$ldndcversionfile"`
        target="ldndc-${target_version}.win64"
fi
target_source_dir="${2:-$LDNDC_ROOT}"
target_pack_dir="${3:-$target_source_dir}"
target_pack_archive_dir="$target_pack_dir/$target"
buildtype="" #"${3:-Release}"

. `dirname $0`/make-package-common.sh


## platform-specific libraries
echo "copying target-specific libraries..."
if [ -d "$target_binary_dir/ui" ]
then
        $cp_bin "$target_source_dir/ui/jldndc/jldndc.bat" "$target_pack_archive_dir"
fi

## create archive
archive_name="$target.$this_date.zip"
echo "creating archive... [$archive_name]"
cd $target_pack_dir
$zip_bin -q -r "$archive_name" "$target"
$rm_bin -r "$target_pack_archive_dir"

echo "@publish:PACK_ARCHIVE=$target_pack_dir/$archive_name"
echo "@publish:TARGET_VERSION=$target_version"
echo
echo "done."

