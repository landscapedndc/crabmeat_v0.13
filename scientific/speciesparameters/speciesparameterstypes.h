/*!
 * @brief
 *    declare common types related to species parameter input
 *
 * @author
 *    steffen klatt (created on: aug 12, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SPECIESPARAMETERSTYPES_H_
#define  LDNDC_INPUT_SPECIESPARAMETERSTYPES_H_

#include  "crabmeat-common.h"
#include  "containers/cbm_typewrap.h"

#include  "species/speciesgroups.h"
#include  "string/cbm_string.h"
#include  "constants/cbm_const.h"

#ifdef  _DEBUG
#  include  "utils/cbm_utils.h"
#endif
#ifndef  __SPECIESPARAMETER_VALUE__
#  define  __SPECIESPARAMETER_VALUE__(__name__,__type__)  this->s.p[__SPECIESPARAMETER_index_ ## __name__].get_value<__type__>()
#endif
#define  SPECIESPARAMETER_(__name__,__type__)                                           \
public:                                                                                 \
    __type__  __name__()                                                                \
    const                                                                               \
    {                                                                                   \
        /*CBM_LogInfo( "requesting parameter .. " #__name__);*/                             \
        crabmeat_assert( cbm::is_valid( __SPECIESPARAMETER_VALUE__(__name__,__type__))); \
        return  __SPECIESPARAMETER_VALUE__(__name__,__type__);                          \
    }                                                                                   \
    static __type__  __name__##_##MIN()                                                 \
    {                                                                                   \
        return  SPECIESPARAMETER_##__name__##_MINVALUE;                                 \
    }                                                                                   \
    static __type__  __name__##_##MAX()                                                 \
    {                                                                                   \
        return  SPECIESPARAMETER_##__name__##_MAXVALUE;                                 \
    }                                                                                   \
private:
    

#define  SPECIESPARAMETER_BOOL(__name__)  SPECIESPARAMETER_(__name__,speciesparameter_t::bool_type)
#define  SPECIESPARAMETER_FLT(__name__)     SPECIESPARAMETER_(__name__,speciesparameter_t::float_type)
#define  SPECIESPARAMETER_INT(__name__)     SPECIESPARAMETER_(__name__,speciesparameter_t::int_type)
#define  SPECIESPARAMETER_UINT(__name__)  SPECIESPARAMETER_(__name__,speciesparameter_t::uint_type)

#define  SPECIESPARAMETER_INVALID_BOOL    false
#define  SPECIESPARAMETER_INVALID_FLT    ldndc::invalid_t< speciesparameter_t::float_type >::value
#define  SPECIESPARAMETER_INVALID_INT    ldndc::invalid_t< speciesparameter_t::int_type >::value
#define  SPECIESPARAMETER_INVALID_UINT    ldndc::invalid_t< speciesparameter_t::uint_type >::value


#include  "io/outputtypes.h"
namespace  ldndc{  namespace  speciesparameters
{
#define  LTYPEWRAP_SPECIES_PARAM_MAGIC  4
typedef  ltypewrap_t< ldndc_flt64_t, ldndc_int32_t, ldndc_uint32_t, LTYPEWRAP_SPECIES_PARAM_MAGIC >  speciesparameter_t;
#define  SPECIESPARAMETERS_bool_type  LDNDC_BOOL
#define  SPECIESPARAMETERS_float_type  LDNDC_FLOAT64
#define  SPECIESPARAMETERS_int_type  LDNDC_INT32
#define  SPECIESPARAMETERS_uint_type  LDNDC_UINT32

#include  "speciesparameters.h.inc"

struct  CBM_API  speciesparameters_meta_t
{
    speciesparameters_meta_t();

    speciesparameters_meta_t(
            char const * /*mnemonic*/,
            char const * /*parent*/,
            species::species_group_e /*group*/);

    speciesparameters_meta_t(
            speciesparameters_meta_t const &);
    speciesparameters_meta_t &  operator=(
            speciesparameters_meta_t const &);

    /* identifier for species parameter set */
    ldndc_string_t  mnemonic;
    /* direct parent in inheritance tree */
    ldndc_string_t  parent;
    /* species group */
    species::species_group_e  group;
};


struct  CBM_API  parameterized_species_t
{
    parameterized_species_t();

    speciesparameters_meta_t   m;
    speciesparameter_t  p[SPECIESPARAMETERS_CNT];

    int  parent_index;
    int  merge_count;
};

class  CBM_API  speciesparameters_set_t
{
    public:
        speciesparameters_set_t();
        speciesparameters_set_t(
                speciesparameters_meta_t const *);
        speciesparameters_set_t(
                parameterized_species_t const *);
        speciesparameters_set_t(
                speciesparameters_set_t const &);
        speciesparameters_set_t &  operator=(
                speciesparameters_set_t const &);

        /* checks if all parameters have a valid value, i.e. were explicitly set */
        bool  is_valid() const;

        size_t  size() const { return  SPECIESPARAMETERS_CNT; }

        /* meta info */
        char const *  PARENT() const { return  this->s.m.parent; }
        char const *  TYPE() const { return  this->s.m.mnemonic; }
        char const *  GROUPNAME() const { return  species::SPECIES_GROUP_NAMES[this->s.m.group]; }
        species::species_group_e  GROUP() const { return  this->s.m.group; }

        bool  IS_CROP() const { return  this->s.m.group == species::SPECIES_GROUP_CROP; }
        bool  IS_GRASS() const { return  this->s.m.group == species::SPECIES_GROUP_GRASS; }
        bool  IS_WOOD() const { return  this->s.m.group == species::SPECIES_GROUP_WOOD; }

        /* expands to getters for all species parameters */
        SPECIESPARAMETERS_GETTERS

    public:
        int  copy_valid(
                speciesparameters_set_t const *);
        int  copy_valid(
                speciesparameter_t const *);
        std::string  parametervalue_as_string(
                char const *) const;
        std::string  to_string( char const * = ";" /*delimiter*/) const;

    public:
        speciesparameter_t::float_type  GDD_GRAIN()
        const
        {
            if ( this->GDD_GRAIN_FILLING() > 0.0)
            { return  this->GDD_GRAIN_FILLING(); }
            else
            { return  0.5 * this->GDD_MATURITY(); }
        }

        speciesparameter_t::float_type  FALEAF()
        const
        {
            return  this->FRACTION_FOLIAGE() / (1.0 - this->FRACTION_FRUIT() - this->FRACTION_ROOT());
        }
    
    private:
        parameterized_species_t  s;
};

}}

#endif  /*  !LDNDC_INPUT_SPECIESPARAMETERSTYPES_H_  */

