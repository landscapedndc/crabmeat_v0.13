/*!
 * @author
 *      steffen klatt,
 *      edwin haas
 */

#include  "speciesparameters/speciesparameterstypes.h"

#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"

namespace  ldndc{  namespace  speciesparameters
{
#include  "speciesparameters.cpp.inc"

speciesparameters_meta_t::speciesparameters_meta_t()
            : group( species::SPECIES_GROUP_NONE)
{
    cbm::as_strcpy( &this->mnemonic, species::SPECIES_NAME_NONE);
    cbm::as_strcpy( &this->parent, species::SPECIES_NAME_NONE);
}
speciesparameters_meta_t::speciesparameters_meta_t(
            char const *  _mnemonic,
            char const *  _parent,
                   species::species_group_e  _group)
            : group( _group)
{
    cbm::as_strcpy( &this->mnemonic, _mnemonic ? _mnemonic : species::SPECIES_NAME_NONE);
    cbm::as_strcpy( &this->parent, _parent ? _parent : species::SPECIES_NAME_NONE);
}

speciesparameters_meta_t::speciesparameters_meta_t(
            speciesparameters_meta_t const &  _other)
{
    (void)this->operator=( _other);
}
speciesparameters_meta_t &
speciesparameters_meta_t::operator=(
            speciesparameters_meta_t const &  _other)
{
    if ( this != &_other)
    {
        cbm::as_strcpy( this->mnemonic, _other.mnemonic);
        cbm::as_strcpy( this->parent, _other.parent);
        this->group = _other.group;
    }

    return  *this;
}

parameterized_species_t::parameterized_species_t()
        : parent_index( -1), merge_count( 0)
{
}

speciesparameters_set_t::speciesparameters_set_t()
{
}
speciesparameters_set_t::speciesparameters_set_t(
        speciesparameters_meta_t const *  _m)
{
    crabmeat_assert( _m);
    this->s.m = *_m;
}
speciesparameters_set_t::speciesparameters_set_t(
        parameterized_species_t const *  _s)
{
    crabmeat_assert( _s);
    this->s.m = _s->m;
    this->copy_valid( _s->p);
}
speciesparameters_set_t::speciesparameters_set_t(
        speciesparameters_set_t const &  _other)
{
    this->s.m = _other.s.m;
    this->copy_valid( _other.s.p);
}
speciesparameters_set_t &
speciesparameters_set_t::operator=(
        speciesparameters_set_t const &  _other)
{
    if ( this == &_other)
    {
        return  *this;
    }

    this->s.m = _other.s.m;
    this->copy_valid( _other.s.p);

    return  *this;
}


int
speciesparameters_set_t::copy_valid(
        speciesparameters_set_t const *  _p)
{
    if ( _p && _p->is_valid())
    {
        return  this->copy_valid( _p->s.p);
    }
    return  -1;
}
int
speciesparameters_set_t::copy_valid(
        speciesparameter_t const *  _p)
{
    int  c = 0;
    for ( size_t  k = 0;  k < this->size();  ++k)
    {
        if ( _p[k].is_set())
        {
            this->s.p[k] = _p[k];
            ++c;
        }
    }
    return  c;
}


bool
speciesparameters_set_t::is_valid()
const
{
    return  ( this->GROUP() != species::SPECIES_GROUP_NONE) &&  !cbm::is_equal_i( this->TYPE(), species::SPECIES_NAME_NONE);
}


std::string
speciesparameters_set_t::parametervalue_as_string(
        char const *  _p_name)
const
{
    if ( !this->is_valid())
    {
        return  "nan";
    }

    unsigned int  p_index( invalid_uint);

    cbm::find_index(
            /* NOTE  cannot uppercase if lower case params exist! */
            cbm::to_upper( _p_name).c_str(),
            SPECIESPARAMETERS_NAMES,
            SPECIESPARAMETERS_CNT,
            &p_index);

    if ( p_index != invalid_uint)
    {
        return  this->s.p[p_index].get_value_as_string( SPECIESPARAMETERS_TYPES[p_index]);
    }
    return  "nan";
}


std::string
speciesparameters_set_t::to_string(
        char const *  _delimiter)
const
{
    std::string  string_representation;

    string_representation += SPECIESPARAMETERS_NAMES[0];
    string_representation += '=';
    string_representation +=
        this->parametervalue_as_string( SPECIESPARAMETERS_NAMES[0]);
    for ( size_t  j = 0;  j < SPECIESPARAMETERS_CNT;  ++j)
    {
        string_representation += _delimiter;

        string_representation += SPECIESPARAMETERS_NAMES[j];
        string_representation += '=';
        string_representation +=
            this->parametervalue_as_string( SPECIESPARAMETERS_NAMES[j]);
    }

    return  string_representation;
}

}}

