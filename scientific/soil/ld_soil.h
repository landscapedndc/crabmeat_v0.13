/*!
 * @brief
 *    collection of various helper functions
 *
 * @author
 *    Ruediger Grote,
 *    David Kraus
 */

#ifndef  LDNDC_SCIENTIFIC_SOIL_H_
#define  LDNDC_SCIENTIFIC_SOIL_H_

#include  "crabmeat-common.h"
#include  "ecosystemtypes.h"
#include  "string/cbm_string.h"
#include  "soillayers/soillayerstypes.h"

namespace  ldndc
{
    enum  henry_coefficient_e
    {
        henry_o2,
        henry_n2,
        henry_nh3,
        henry_n2o,
        henry_no,
        henry_ch4,
        henry_co2,

        num_henry_coeff
    };

    /*!
     * @brief
     */
    CBM_API
    double
    get_henry_coefficient(
            double /* temperature */,
            henry_coefficient_e /* henry coefficient*/);

    /*!
     * @brief
     *
     */
    CBM_API
    double
    porosity(
         double /* org. C fraction */,
         double /* soil density */,
         double /* _mineral_density */);

    /*!
     * @brief
     *
     */
    CBM_API
    double
    cn_ratio(
         cn_ratiolevel_e /* cn ratio level */,
         double /* depth of current spoil layer*/, 
         double /* depth upper soil layer */, 
         double /* cn ratio of upper soil layer */);

    /*!
     * @brief
     *
     */    
    CBM_API
    lerr_t 
    organic_carbon_fraction(
          double * /* org. C fraction */,
          double /* org. C fraction in 0.05 m */, 
          double /* org. C fraction in 0.30 m */, 
          double /* cumulated depth */); 

    /*!
     * @brief
     *
     */
    CBM_API
    lerr_t  
    estimate_organic_carbon_content(
                                  double * /* fitting parameter */,
                                  double * /* fitting parameter */,
                                  double /* org. C fraction in 0.05 m */, 
                                  double /* org. C fraction in 0.30 m */);

    class soil_texture
    {
        public:
            soil_texture( cbm::string_t /* soil name */,
                          double [14] /* soil texture rtiangle coordinates */);

            cbm::string_t name;
            double x_cord[7];
            double y_cord[7];
    };

    /*!
     * @brief
     *
     */
    CBM_API
    bool
    use_usda_soil_texture_triangle( ldndc::soillayers::soil_type_e /* soil type */);

    /*!
     * @brief
     *
     */
    CBM_API
    bool
    in_soil_texture_triangle_polynom(
                                 soil_texture /* soil texture type */,
                                 size_t /* number of polynom vertices */,
                                 double /* sand content (%) */,
                                 double /* clay content (%) */);

    /*!
     * @brief
     *
     */
    CBM_API
    cbm::string_t
    get_soil_texture(
             double /* sand content (%) */,
             double /* clay content (%) */);

    /*!
     * @brief
     *
     */
    CBM_API
    soillayers::soil_type_e
    get_soil_texture_type(
                      cbm::string_t /* soil texture */);
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_SCIENTIFIC_SOIL_H_  */
