/*!
 * @brief
 *
 * @author
 *    Steffen Klatt
 *    David Kraus
 */

#include  "scientific/soil/ld_soil.h"
#include  "constants/cbm_const.h"
#include  "math/cbm_math.h"

#include  "log/cbm_baselog.h"
#include  <stdlib.h>


struct CBM_API henry_coeff_t
{
    double  Hx_coeff;
    double  scale;
};


double 
ldndc::get_henry_coefficient(
        double  _temp, 
        henry_coefficient_e  _henry_coeff)
{
    static double const  T_REF_R( 1.0 / 298.0);
    static henry_coeff_t const  HENRY_X[num_henry_coeff] =
    {
        // set to average value from Lide and Frederikse, 1995; Dean, 1992; Kavanaugh and Trussell, 1980; Wilhelm, Battino, et al., 1977; Carpenter, 1966; Loomis, 1928
        { cbm::H_O2, 1650.0},
        // set to average value n2o
        { cbm::H_N2, 2675.0},
        // set to main value from Lide and Frederikse.
        { cbm::H_NH3, 4100.0},
        // set to average value from Lide and Frederikse, 1995; Dean, 1992; Wilhelm, Battino, et al., 1977; Perry, et al., 1963.
        { cbm::H_N2O, 2675.0},
        // set to average value from Lide and Frederikse, 1995; Dean, 1992; Schwartz and White, 1981.
        { cbm::H_NO, 1533.0},
        // set to average value Lide and Frederikse, 1995;  Dean, 1992; Kavanaugh and Trussell, 1980; Wilhelm, Battino, et al., 1977.
        { cbm::H_CH4, 1750.0},
        //set to average value from Lide and Frederikse, 1995; Dean, 1992; Yaws and Yang, 1992; Carroll, Slupsky, et al., 1991; 
        //Lelieveld and Crutzen, 1991; Pandis and Seinfeld, 1989; Jacob, 1986; Chameides, 1984; Hoffmann and Jacob, 1984; Martin, 1984; Durham, Overton, et al., 1981; 
        //Kavanaugh and Trussell, 1980; Edwards, Maurer, et al., 1978; Wilhelm, Battino, et al., 1977; Sillen and Martell, 1964; Buch, 1951; Morgan and Maass, 1931.
        { cbm::H_CO2, 2428.0}
    };

    if ( _henry_coeff < num_henry_coeff)
    {
        double const  H = HENRY_X[_henry_coeff].Hx_coeff;
        double const  S = HENRY_X[_henry_coeff].scale;
        return  H * std::exp( S / ( _temp + cbm::D_IN_K) - ( S * T_REF_R));
    }
    CBM_LogError( "[BUG]  Henry coefficient not supported!");
    return  ldndc::invalid_t< double >::value;
}


/* Ruehlmann et al., 2006 */
double
ldndc::porosity(
        double _frac_c_org, 
        double _soil_density,
        double _mineral_density)
{
    double const frac_om( _frac_c_org / cbm::CCORG);
    
    //~ double const dorg( 1.127 + 0.373 * frac_om); // for consistency we take the fixed value: DORG (as elsewhere)
    double const dorg( cbm::DORG);
    
    //~ double const particle_density( 1.0 / ((frac_om / dorg) + (1.0 - frac_om) / cbm::DMIN));
    double const particle_density( 1.0 / ((frac_om / dorg) + ((1.0 - frac_om) / _mineral_density)));
    
    double poro( 1.0 - _soil_density / particle_density);
    if (poro < 0.01)
    {
        poro = 0.01;
        CBM_LogInfo("porosity has been changed to a minimum value of 0.01");
    }
    
    return poro;
}


double
ldndc::cn_ratio(
        cn_ratiolevel_e  _cn_ratio_level,
        double  _depth_new, 
        double  _depth_old, 
        double  _cn_old)
{
    static double const  CN_RATIO_HIGH[3] = { 16.0, 20.0, 24.0 };
    static double const  CN_RATIO_LOW[3] = { 8.0, 10.0, 12.0 };
    
    double const CN_HIGH( CN_RATIO_HIGH[_cn_ratio_level]);
    double const CN_LOW( CN_RATIO_LOW[_cn_ratio_level]);
    
    if ( cbm::flt_greater_zero( _depth_old) &&
         cbm::flt_greater_zero( _cn_old) &&
         cbm::flt_less( _depth_old, _depth_new))
    {
        double const cn_old( cbm::bound( CN_LOW, CN_HIGH - (sqrt(_depth_old) * (CN_HIGH - CN_LOW)), CN_HIGH));
        double const cn_new( cbm::bound( CN_LOW, CN_HIGH - (sqrt(_depth_new) * (CN_HIGH - CN_LOW)), CN_HIGH));
        double const delta_cn( cn_old - cn_new);
        
        return cbm::bound( CN_LOW,
                           _cn_old - delta_cn,
                           CN_HIGH);
    }
        
    return cbm::bound( CN_LOW,
                       CN_HIGH - (sqrt(_depth_new) * (CN_HIGH - CN_LOW)),
                       CN_HIGH);
}


lerr_t 
ldndc::organic_carbon_fraction(
        double *_frac_c_org /* organic carbon in target soil depth */,
        double _c_org05 /* organic carbon in 0.05 (m) soil depth */,
        double _c_org30 /* organic carbon in 0.3 (m) soil depth */,
        double _depth /* soil depth in (m) */) 
{
    double  C1( 0.0), C2( 0.0);
    lerr_t  rc_occ = ldndc::estimate_organic_carbon_content( &C1, &C2, _c_org05, _c_org30);
    
    if ( rc_occ)
    {
        return  LDNDC_ERR_FAIL;
    }
    
    // for _c_org05 > _c_org30 we assume exponentially decreasing soc for the complete soil profile
    if ( _frac_c_org && C1 > 0.0 && C2 > 0.0)
    {        
        (*_frac_c_org) = C1 * exp( -C2 * _depth);
    }
    else
    {
        // for _c_org05 < _c_org30 we assume linearly increasing soc from 0.05 - 0.3 m
        // for 0.0 - 0.05 we assume _c_org05
        // for 0.3 - maxdepth we assume _c_org30
        if (_depth < 0.05)
        {
            (*_frac_c_org) = _c_org05;
        }
        else if (_depth < 0.3)
        {
            (*_frac_c_org) = _c_org05 + (_c_org30-_c_org05)/0.25 * (_depth - 0.05);
        }
        else
        {
            (*_frac_c_org) = _c_org30;    
        }
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::estimate_organic_carbon_content(
        double *_C1_arg,
        double *_C2_arg,
        double _c_org05 /* organic carbon in 0.05 (m) soil depth */,
        double _c_org30 /* organic carbon in 0.3 (m) soil depth */)
{
    // Carbon content at soil depth = 0.0 (fitting parameter))
    double  C1( 0.0);
    // Decrease of carbon content with depth (fitting parameter)
    double  C2( 0.0);
    
    // Fit an exponential through _c_org05 and _c_org30 to estimate organic C through the profile
    if (( _c_org05 > 0.0)  &&  ( _c_org30 > 0.0))
    {
        C1 = (std::exp((0.3*log(_c_org05) - 0.05*log(_c_org30)) / 0.25));
        C2 = (-1.0/0.3 * log(_c_org30 / C1));
    }
    
    if ( _C1_arg)
    {
        *_C1_arg = C1;
    }
    if ( _C2_arg)
    {
        *_C2_arg = C2;
    }
    
    return  LDNDC_ERR_OK;
}


ldndc::soil_texture::soil_texture(
                           cbm::string_t _name,
                           double _coordinates[14]):
                           name( _name)
{
    for ( size_t i = 0; i < 7; ++i)
    {
        x_cord[i] = _coordinates[i];
        y_cord[i] = _coordinates[i+7];
    }
}


bool
ldndc::use_usda_soil_texture_triangle(
                                      ldndc::soillayers::soil_type_e _soil_type)
{
    if ( _soil_type == ldndc::soillayers::SOIL_BEDROCK)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT_BOG)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT_POORFEN)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT_MODERATEFEN)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT_RICHFEN)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT_SWAMPFOREST_OMBROTROPHIC)
    {
        return false;
    }
    else if ( _soil_type == ldndc::soillayers::PEAT_SWAMPFOREST_MINEROTROPHIC)
    {
        return false;
    }

    return true;
}


bool
ldndc::in_soil_texture_triangle_polynom(
                                    soil_texture _soil,
                                    size_t _npoints,
                                    double _xt,
                                    double _yt)
{
    bool inside( false);
    bool on_border( false);

    double xold( _soil.x_cord[_npoints-1]);
    double yold( _soil.y_cord[_npoints-1]);

    for ( size_t i = 0; i < _npoints; ++i)
    {
        double xnew( _soil.x_cord[i]);
        double ynew( _soil.y_cord[i]);

        double x1, x2, y1, y2;
        if ( xnew > xold)
        {
            x1 = xold;
            x2 = xnew;
            y1 = yold;
            y2 = ynew;
        }
        else
        {
            x1 = xnew;
            x2 = xold;
            y1 = ynew;
            y2 = yold;
        }


        if ( ((xnew < _xt) && (_xt <= xold)) ||
            ((!(xnew < _xt)) && (!(_xt <= xold))))
        {
            /* The test point lies on a non-vertical border */
            if ( cbm::flt_equal( (_yt-y1) * (x2-x1),
                                (y2-y1) * (_xt-x1)))
            {
                on_border = true;
            }
            else if ( cbm::flt_greater( (y2-y1)*(_xt-x1) + 0.001,
                                       (_yt-y1)*(x2-x1)))
            {
                inside = !inside;
            }
        }
        else
        {
            bool cond_1( cbm::flt_equal( xnew, _xt) || cbm::flt_equal( xold, _xt));
            bool cond_2( cbm::flt_equal((_yt-y1)*(x2-x1), (y2-y1)*(_xt-x1)));
            bool cond_3( (  (ynew <= _yt) &&  (_yt <= yold)) ||
                        ((!(ynew < _yt)) &&  (!(_yt < yold))));
            if (cond_1 && cond_2 && cond_3)
            {
                on_border = true;
            }
        }

        xold = xnew;
        yold = ynew;
    }

    if ( !on_border)
    {
        return inside;
    }
    else
    {
        return true;
    }
}


cbm::string_t
ldndc::get_soil_texture(
                    double _sand /* sand content (%) */,
                    double _clay /* clay content (%) */)
{
    double silty_loam_coords[14] = {0, 0, 23, 50, 20, 8, 0, 12, 27, 27, 0, 0, 12, 0};
    soil_texture silty_loam( "silty loam", silty_loam_coords);

    double sandy_coords[14] = {85, 90, 100, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0 };
    soil_texture sandy( "sand", sandy_coords);

    double silty_clay_loam_coords[14] = {0, 0, 20, 20, 0, 0, 0, 27, 40, 40, 27, 0, 0, 0 };
    soil_texture silty_clay_loam( "silty clay loam", silty_clay_loam_coords);

    double loam_coords[14] = {43, 23, 45, 52, 52, 0, 0, 7, 27, 27, 20, 7, 0, 0 };
    soil_texture loam( "loam", loam_coords);

    double clay_loam_coords[14] = {20, 20, 45, 45, 0, 0, 0, 27, 40, 40, 27, 0, 0, 0 };
    soil_texture clay_loam( "clay loam", clay_loam_coords);

    double sandy_loam_coords[14] = {50, 43, 52, 52, 80, 85, 70, 0, 7, 7, 20, 20, 15, 0 };
    soil_texture sandy_loam( "sandy loam", sandy_loam_coords);

    double silty_clay_coords[14] = {0, 0, 20, 0, 0, 0, 0, 40, 60, 40, 0, 0, 0, 0 };
    soil_texture silty_clay( "silty clay", silty_clay_coords);

    double sandy_clay_loam_coords[14] = {52, 45, 45, 65, 80, 0, 0, 20, 27, 35, 35, 20, 0, 0 };
    soil_texture sandy_clay_loam( "sandy clay loam", sandy_clay_loam_coords);

    double loamy_sand_coords[14] = {70, 85, 90, 85, 0, 0, 0, 0, 15, 10, 0, 0, 0, 0 };
    soil_texture loamy_sand( "loamy sand", loamy_sand_coords);

    double clayey_coords[14] = {20, 0, 0, 45, 45, 0, 0, 40, 60, 100, 55, 40, 0, 0 };
    soil_texture clayey( "clay", clayey_coords);

    double silt_coords[14] = {0, 0, 8, 20, 0, 0, 0, 0, 12, 12, 0, 0, 0, 0 };
    soil_texture silt( "silt", silt_coords);

    double sandy_clay_coords[14] = {45, 45, 65, 0, 0, 0, 0, 35, 55, 35, 0, 0, 0, 0 };
    soil_texture sandy_clay( "sandy clay", sandy_clay_coords);

    cbm::string_t texture("none");

    if ( cbm::flt_greater_equal_zero( _sand) &&
         cbm::flt_greater_equal_zero( _clay))
    {
        if ( in_soil_texture_triangle_polynom( silty_loam, 6, _sand, _clay))
        {
            texture = silty_loam.name;
        }
        if ( in_soil_texture_triangle_polynom( sandy, 3, _sand, _clay))
        {
            texture = sandy.name;
        }
        if ( in_soil_texture_triangle_polynom( silty_clay_loam, 4, _sand, _clay))
        {
            texture = silty_clay_loam.name;
        }
        if ( in_soil_texture_triangle_polynom( loam, 5, _sand, _clay))
        {
            texture = loam.name;
        }
        if ( in_soil_texture_triangle_polynom( clay_loam, 4, _sand, _clay))
        {
            texture = clay_loam.name;
        }
        if ( in_soil_texture_triangle_polynom( sandy_loam, 7, _sand, _clay))
        {
            texture = sandy_loam.name;
        }
        if ( in_soil_texture_triangle_polynom( silty_clay, 3, _sand, _clay))
        {
            texture = silty_clay.name;
        }
        if ( in_soil_texture_triangle_polynom( sandy_clay_loam, 5, _sand, _clay))
        {
            texture = sandy_clay_loam.name;
        }
        if ( in_soil_texture_triangle_polynom( loamy_sand, 4, _sand, _clay))
        {
            texture = loamy_sand.name;
        }
        if ( in_soil_texture_triangle_polynom( clayey, 5, _sand, _clay))
        {
            texture = clayey.name;
        }
        if ( in_soil_texture_triangle_polynom( silt, 4, _sand, _clay))
        {
            texture = silt.name;
        }
        if ( in_soil_texture_triangle_polynom( sandy_clay, 3, _sand, _clay))
        {
            texture = sandy_clay.name;
        }
    }

    return texture;
}


ldndc::soillayers::soil_type_e
ldndc::get_soil_texture_type(
                      cbm::string_t _soil_texture)
{
    if ( cbm::is_equal( _soil_texture.c_str(), "silty loam"))
    {
        return ldndc::soillayers::SOIL_SILTY_LOAM;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "sand"))
    {
        return ldndc::soillayers::SOIL_SAND;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "silty clay loam"))
    {
        return ldndc::soillayers::SOIL_SILTY_CLAY_LOAM;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "loam"))
    {
        return ldndc::soillayers::SOIL_LOAM;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "clay loam"))
    {
        return ldndc::soillayers::SOIL_CLAY_LOAM;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "sandy loam"))
    {
        return ldndc::soillayers::SOIL_SANDY_LOAM;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "silty clay"))
    {
        return ldndc::soillayers::SOIL_SILTY_CLAY;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "sandy clay loam"))
    {
        return ldndc::soillayers::SOIL_SANDY_CLAY_LOAM;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "loamy sand"))
    {
        return ldndc::soillayers::SOIL_LOAMY_SAND;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "clay"))
    {
        return ldndc::soillayers::SOIL_CLAY;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "silt"))
    {
        return ldndc::soillayers::SOIL_SILT;
    }
    else if ( cbm::is_equal( _soil_texture.c_str(), "sandy clay"))
    {
        return ldndc::soillayers::SOIL_SANDY_CLAY;
    }
    else
    {
        return ldndc::soillayers::SOIL_NONE;
    }
}

