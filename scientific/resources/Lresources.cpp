/*!
 * @brief
 *    interface to the LResources (implementation)
 *
 * @author
 *    steffen klatt (created on: mar 15, 2014),
 *    edwin haas
 */

#define  CRABMEAT_GENERATE_GLOBAL_INSTANCES
#include  "resources/Lresources.h"
#include  "hash/cbm_hash.h"
#include  "log/cbm_baselog.h"


bool
cbm::Lresources_t::is_open()
const
{
    return  this->exists( "__system__.read_successful");
}

lerr_t
cbm::Lresources_t::read(
        char const *  _pkg_version, char const *  _filename)
{
    if ( !_filename || cbm::is_empty( _filename))
    {
        CBM_LogError( "Lresources_t::read(): ",
                "invalid filename argument");
        return  LDNDC_ERR_FAIL;
    }

    this->remove_all();

    FILE *  res_toplevelfile;
    res_toplevelfile = this->open_file( _filename);
    if ( !res_toplevelfile)
    {
        CBM_LogError( "Lresources_t::read(): ",
                "failed to open top-level Lresources file  [filename=",_filename,"]");
        return  LDNDC_ERR_FAIL;
    }

    int  linenum = 0;
    /* kick-off resource reading */
    int  rc_read = this->read_file( res_toplevelfile, &linenum);
    if ( rc_read)
    {
        CBM_LogError( "Lresources_t::read(): ",
                "errors occured during reading resources file  [filename=",_filename,",line=",linenum,",status=",rc_read,"]");
        return  LDNDC_ERR_FAIL;
    }

    if ( _pkg_version)
    {
        if ( this->exists( LRESOURCES_TARGETVERSION_KEY))
        {
            if ( !cbm::is_equal( this->find( LRESOURCES_TARGETVERSION_KEY), _pkg_version))
            {
                CBM_LogWarn( "Lresources target version mismatch; expect monsters and orks to mess with you :(",
                        "\n\tplease update the Lresources database!",
                        "  [target=",this->find( LRESOURCES_TARGETVERSION_KEY),",expected=",_pkg_version,"]");
            }
        }
        else
        {
            CBM_LogWarn( "Lresources is missing target version; consider an update!");
        }
    }

    int  rc_ins = this->insert_resource( "__system__", "read_successful", "1");
    if ( rc_ins)
    {
        return  LDNDC_ERR_FAIL;
    }
    CBM_LogVerbose( "Lresources: set ", this->get_number_of_entries(), " items in database");
    return  LDNDC_ERR_OK;
}

#include  "utils/lutils-fs.h"
#include  "string/lstring-transform.h"
FILE *
cbm::Lresources_t::open_file(
        char const *  _filename)
{
    crabmeat_assert( _filename);

    std::string  resfilename = _filename;
    cbm::format_expand( &resfilename);
    if ( !cbm::is_file( resfilename.c_str()))
    {
        return  NULL;
    }

    cbm::hash::sha1_hexstring_t  resfile_hash;
    cbm::hash::sha1hex( resfilename.c_str(), resfilename.length(), resfile_hash);
    if ( this->exists( "__system__.resourcefiles", resfile_hash))
    {
        CBM_LogError( "Lresources_t::open_file(): ",
                "detected duplicate or cycle in resource file inclusion  [filename=",resfilename,"]");
        return  NULL;
    }

    FILE *  resfile = fopen( resfilename.c_str(), "r");
    if ( !resfile)
    {
        CBM_LogError( "Lresources_t::open_file(): ",
                "failed to open Lresources file  [filename=",resfilename,"]");
    }
    else
    {
        int  rc_ins = this->insert_resource( "__system__.resourcefiles", resfile_hash, "1");
        if ( rc_ins)
        {
            CBM_LogError( "Lresources_t::open_file(): ",
                    "failed to include filename into Lresources  [filename=",resfilename,"]");
            fclose( resfile);
            resfile = NULL;
        }
    }

    return  resfile;
}

/* inspired by options_parse( ..) from Damian Pietras' MOC player project (see options.c) */
int
cbm::Lresources_t::read_file(
        FILE *  _resfile, int *  _linenum)
{
    if ( !_resfile)
    {
        return  -128;
    }

    int  rc_read = 0;

    int  chr;
    int  assignment = 0;
    int  comment_line = 0;
    int  quotes = 0;
    int  block_header = 0;
    int  eol_r = 0;

    std::string  section( "");

    std::string  key( "");
    std::string  value( "");

    unsigned int  line_num = 1;

    while (( chr = getc( _resfile)) != EOF)
    {
        if ( chr == '\r')
        {
            continue;
        }
        if ( is_newline(chr))
        {
            eol_r = 1;
            comment_line = 0;
            line_num += 1;
        }

        /* discard each character until end of comment line */
        if ( comment_line && !eol_r)
        {
            continue;
        }

        /* start next line */
        if ( eol_r)
        {
            if ( !key.empty())
            {
                if ( assignment)
                {
                    rc_read = this->insert_resource( section.c_str(), key.c_str(), value.c_str());
                    if ( rc_read)
                    {
                        break;
                    }
                }
                else
                {
                    FILE *  inc_resfile = NULL;
                    std::string  incl_fn;
                    int  rc_inc = this->try_open_include_file( &inc_resfile, key.c_str(), &incl_fn);
                    CBM_LogDebug( "read file ",incl_fn);
                    if ( rc_inc == 0 && inc_resfile)
                    {
                        int  this_linenum = 0;
                        int  rc_read_inc = this->read_file( inc_resfile, &this_linenum);
                        CBM_LogDebug( "done reading file ",incl_fn);
                        if ( rc_read_inc)
                        {
                            CBM_LogError( "Lresources_t::read_file(): ",
                                    "errors occured during reading resources file  [filename=",incl_fn,",line=",this_linenum,",status=",rc_read_inc,"]");
                            rc_read = -1;
                            break;
                        }
                        CBM_LogVerbose( "read additional Lresources file ", incl_fn);
                    }
                    else if ( rc_inc == 0 && !inc_resfile) /* it was an optional "include" line */
                    {
                        /* skip reading optional include file */
                    }
                    else if ( rc_inc < 0) /*it was an "include" line */
                    {
                        CBM_LogError( "Lresources_t::read_file(): ",
                                "errors in Lresources file  [key=",key,",line=",line_num,"]");
                        rc_read = -10;
                        break;
                    }
                    else
                    {
                        CBM_LogError( "Lresources_t::read_file(): ",
                            "lonely key word [key=",key,",line=",line_num,"]");
                        rc_read = -11;
                        break;
                    }
                }
            }

            assignment = 0;
            comment_line = 0;
            quotes = 0;
            block_header = 0;

            key.clear();
            value.clear();

            eol_r = 0;
        }
        else if ( is_comment(chr) && (!quotes) && (!block_header))
        {
            comment_line = 1;
        }
        else if (( !quotes) && (!assignment) && (key.empty()) && is_block_open(chr))
        {
            section.clear();
            block_header = 1;
        }
        else if ( block_header && is_block_close(chr))
        {
            block_header = 0;
        }
        else if ( block_header)
        {
            /* removes all blanks from section name */
            if ( !is_space( chr))
            {
                section += chr;
            }
        }
        else if (( !quotes) && is_quotes_open(chr))
        {
            quotes = 1;
        }
        else if ((  quotes) && is_quotes_close(chr))
        {
            quotes = 0;
        }
        else if (( !quotes) && is_assignment(chr))
        {
            assignment = 1;
        }
        else if (( quotes || (!is_space( chr))) && assignment)
        {
            if ( key.empty())
            {
                CBM_LogError( "Lresources_t::read_file(): ",
                        "value without key [line=",line_num,"]");
                rc_read = -2;
                break;
            }
            value += chr;
        }
        else if ( quotes || (!is_space( chr)))
        {
            key += chr;
        }
        else
        {
            /* blanks */
        }
    }

    fclose( _resfile);
    if ( _linenum) *_linenum = ( line_num == 1) ? 1 : ( line_num-1);
    return  rc_read;
}

#include  "string/lstring-compare.h"
int
cbm::Lresources_t::try_open_include_file(
           FILE **  _inc_resfile, char const *  _resline, std::string *  _incl_fn)
{
    crabmeat_assert( _inc_resfile);

    if ( cbm::strlen( _resline) <= cbm::strlen( "include"))
    {
        return  1;
    }
    if ( !cbm::has_prefix_i( _resline, "include"))
    {
        CBM_LogError( "Lresources_t::try_open_include_file(): ",
                "failed to parse resource line  [line=",_resline,"]");
        return  2;
    }

    char const *  incl_fn = _resline + cbm::strlen( "include");
    bool  is_optional = false;
    if ( cbm::has_prefix_i( _resline, "includeoptional"))
    {
        incl_fn += strlen( "optional");
        is_optional = true;
    }

    if ( cbm::is_empty( incl_fn))
    {
        /* no no no :-| */
        CBM_LogError( "Lresources_t::try_open_include_file(): ",
                "resource include filename is empty");
        return  -1;
    }
    else
    {
        std::string  incl_fn_exp;
        cbm::format_expand( &incl_fn_exp, incl_fn);
        if ( _incl_fn)
        {
            *_incl_fn = incl_fn_exp;
        }
        *_inc_resfile = this->open_file( incl_fn_exp.c_str());
        if ( !*_inc_resfile)
        {
            if ( is_optional)
            {
                CBM_LogVerbose( "unable to load optional Lresources include file  [file=",incl_fn_exp,"]");
                return  0;
            }
            CBM_LogError( "Lresources_t::try_open_include_file(): ",
                    "failed to open Lresources include file  [file=",incl_fn_exp,"]");
            return  -3;
        }
    }

    return  ( *_inc_resfile != NULL) ? 0 : -2;
}

int
cbm::Lresources_t::insert_resource(
        char const *  _section,
        char const *  _key, char const *  _value)
{
    return  this->insert( _section, _key, _value);
}

cbm::Lresources_t &  cbm::DefaultResources()
{
    static Lresources_t  global_CBM_DefaultResources;
    return  global_CBM_DefaultResources;
}

