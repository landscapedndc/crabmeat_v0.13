/*!
 * @brief
 *    interface to the ldndc resources:
 *
 *    this structure reads all Lresources. such resources
 *    are uninterpreted key/value pairs without type
 *    information. the idea is somewhat based on the
 *    Xresources ;-)
 *
 * @author
 *    steffen klatt (created on: mar 15, 2014),
 *    edwin haas
 */

/*
 *
 *
 * Lresources.txt
 * ## BEGIN
 * # this is the top-level resources file
 * #
 * airchemistry.bc.* = 0.0        (???)
 * airchemistry.bc.nitrogen = 370
 *
 * include moreresources.txt
 * include evenmoreresources.txt
 *
 * species.maximumslots = 256
 *
 * ## END
 */

#ifndef  LDNDC_LRESOURCES_H_
#define  LDNDC_LRESOURCES_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"
#include  "cbm_rtcfg.h"

#include  "containers/lkeyvalbase.h"

#include  <stdio.h>

#define  LRESOURCES_TARGETVERSION_KEY  "__system__.Lresources.targetversion"

#define  LRESOURCES_FILE  LDNDC_RESOURCES_PATH "/" LDNDC_RESOURCES_FILENAME
#define  LRESOURCES_TABLE_SIZE  65536
#define  LRESOURCES_KEY_MAXSIZE  127
#define  LRESOURCES_VALUE_MAXSIZE  91
namespace cbm
{
    typedef  keyvaluebase_t< LRESOURCES_TABLE_SIZE, LRESOURCES_KEY_MAXSIZE, LRESOURCES_VALUE_MAXSIZE >  resource_container_t;
    class  CBM_API  Lresources_t  :  public  resource_container_t
          {
        public:
            bool  is_open() const;
            /*!
             * @brief
             *    read given resources file.
             */
            lerr_t  read(
                char const * /*package version*/,
                char const * = LDNDC_RESOURCES_FILENAME /*filename*/);

        private:

            FILE *  open_file(
                    char const * /*filename*/);
            int  read_file(
                    FILE * /*opened resources file*/, int * /*line number*/);
            int  try_open_include_file(
                    FILE **, char const *, std::string * /*resulting filename*/);
            int  insert_resource(
                    char const *,
                    char const *, char const *);
    };
}

/* resources dictionary */
namespace cbm {
    extern CBM_API Lresources_t &  DefaultResources(); }
#define  CBM_DefaultResources cbm::DefaultResources()

#endif  /*  !LDNDC_LRESOURCES_H_  */

