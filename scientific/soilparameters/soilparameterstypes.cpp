/*!
 * @brief
 *    container class for one complete soil
 *    and humus parameters set
 * 
 * @author
 *      steffen klatt (created on jun 19, 2013),
 *      edwin haas
 */

#include  "soilparameters/soilparameterstypes.h"
namespace  ldndc{ namespace  soilparameters
{
#include  "soilparameters.cpp.inc"
}}
namespace  ldndc{ namespace  humusparameters
{
#include  "humusparameters.cpp.inc"
}}

