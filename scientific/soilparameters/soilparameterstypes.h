/*!
 * @brief
 *    declare common types related to soil parameter input
 *
 * @author
 *    steffen klatt (created on: aug 12, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SOILPARAMETERSTYPES_H_
#define  LDNDC_INPUT_SOILPARAMETERSTYPES_H_

#include  "crabmeat-common.h"
#include  "containers/cbm_typewrap.h"

#ifdef  _DEBUG
#  include  "utils/cbm_utils.h"
#endif
#ifndef  __HUMUS_SOIL_PARAMETER_VALUE__
#  define  __HUMUS_SOIL_PARAMETER_VALUE__(__p_idx__,__type__,__TYPE__,__name__,__dtype__,__buf__)            \
        __buf__[__p_idx__ * __type__##parameters:: __TYPE__##PARAMETERS_CNT                    \
            + __##__TYPE__##PARAMETER_index_ ## __name__].get_value<__dtype__>()
#endif
#define  HUMUS_SOIL_PARAMETER_(__type__,__TYPE__,__name__,__dtype__)                        \
public:                                                                                     \
    __dtype__  __TYPE__ ## _ ## __name__( size_t  _p_idx)                                   \
    const                                                                                   \
    {                                                                                       \
        if ( _p_idx == soillayers::__TYPE__##_NONE)                                         \
        {                                                                                   \
            return  __HUMUS_SOIL_PARAMETER_VALUE__(0,__type__,__TYPE__,__name__,__dtype__,__type__##_default_props_);   \
        }                                                                                   \
        return  __HUMUS_SOIL_PARAMETER_VALUE__(_p_idx,__type__,__TYPE__,__name__,__dtype__,__type__##_props_);          \
    }                                                                                       \
    static __dtype__  __TYPE__ ## _ ## __name__ ## _ ## MIN()                               \
    {                                                                                       \
        return  __TYPE__ ## PARAMETER_ ## __name__ ## _MINVALUE;                            \
    }                                                                                       \
    static __dtype__  __TYPE__ ## _ ## __name__ ## _ ## MAX()                               \
    {                                                                                       \
        return  __TYPE__ ## PARAMETER_ ## __name__ ## _MAXVALUE;                            \
    }                                                                                       \
private:


#define  HUMUSPARAMETER_(__name__,__dtype__)    HUMUS_SOIL_PARAMETER_(humus,HUMUS,__name__,__dtype__)
#define  SOILPARAMETER_(__name__,__dtype__)    HUMUS_SOIL_PARAMETER_(soil,SOIL,__name__,__dtype__)

#define  HUMUSPARAMETER_BOOL(__name__)        HUMUSPARAMETER_(__name__,humusparameters::humusparameter_t::bool_type)
#define  HUMUSPARAMETER_FLT(__name__)        HUMUSPARAMETER_(__name__,humusparameters::humusparameter_t::float_type)
#define  HUMUSPARAMETER_INT(__name__)        HUMUSPARAMETER_(__name__,humusparameters::humusparameter_t::int_type)
#define  HUMUSPARAMETER_UINT(__name__)        HUMUSPARAMETER_(__name__,humusparameters::humusparameter_t::uint_type)
#define  HUMUSPARAMETER_INVALID_BOOL    false
#define  HUMUSPARAMETER_INVALID_FLT    ldndc::invalid_t< humusparameters::humusparameter_t::float_type >::value
#define  HUMUSPARAMETER_INVALID_INT    ldndc::invalid_t< humusparameters::humusparameter_t::int_type >::value
#define  HUMUSPARAMETER_INVALID_UINT    ldndc::invalid_t< humusparameters::humusparameter_t::uint_type >::value


#define  SOILPARAMETER_BOOL(__name__)        SOILPARAMETER_(__name__,soilparameters::soilparameter_t::bool_type)
#define  SOILPARAMETER_FLT(__name__)        SOILPARAMETER_(__name__,soilparameters::soilparameter_t::float_type)
#define  SOILPARAMETER_INT(__name__)        SOILPARAMETER_(__name__,soilparameters::soilparameter_t::int_type)
#define  SOILPARAMETER_UINT(__name__)        SOILPARAMETER_(__name__,soilparameters::soilparameter_t::uint_type)
#define  SOILPARAMETER_INVALID_BOOL    false
#define  SOILPARAMETER_INVALID_FLT    ldndc::invalid_t< soilparameters::soilparameter_t::float_type >::value
#define  SOILPARAMETER_INVALID_INT    ldndc::invalid_t< soilparameters::soilparameter_t::int_type >::value
#define  SOILPARAMETER_INVALID_UINT    ldndc::invalid_t< soilparameters::soilparameter_t::uint_type >::value


#include  "io/outputtypes.h"
namespace  ldndc{ namespace  soilparameters
{
#define  LTYPEWRAP_SOIL_PARAM_MAGIC  1
typedef  ltypewrap_t< ldndc_flt64_t, ldndc_int32_t, ldndc_uint32_t, LTYPEWRAP_SOIL_PARAM_MAGIC >  soilparameter_t;
#define  SOILPARAMETERS_bool_type  LDNDC_BOOL
#define  SOILPARAMETERS_float_type  LDNDC_FLOAT64
#define  SOILPARAMETERS_int_type  LDNDC_INT32
#define  SOILPARAMETERS_uint_type  LDNDC_UINT32

#include  "soilparameters.h.inc"
}}
namespace  ldndc{ namespace  humusparameters
{
#define  LTYPEWRAP_HUMUS_PARAM_MAGIC  2
typedef  ltypewrap_t< ldndc_flt64_t, ldndc_int32_t, ldndc_uint32_t, LTYPEWRAP_HUMUS_PARAM_MAGIC >  humusparameter_t;
#define  HUMUSPARAMETERS_bool_type  LDNDC_BOOL
#define  HUMUSPARAMETERS_float_type  LDNDC_FLOAT64
#define  HUMUSPARAMETERS_int_type  LDNDC_INT32
#define  HUMUSPARAMETERS_uint_type  LDNDC_UINT32

#include  "humusparameters.h.inc"
}}

#endif  /*  !LDNDC_INPUT_SOILPARAMETERSTYPES_H_  */

