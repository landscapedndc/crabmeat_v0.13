/*!
 * @brief
 *    Defines a list of supported ecosystems
 *
 * @author
 *    Steffen Klatt  (created on: jan 11, 2012),
 *    Edwin Haas
 */

#ifndef LDNDC_STATE_ECOSYSTEMTYPES_H_
#define LDNDC_STATE_ECOSYSTEMTYPES_H_

#include  "crabmeat-common.h"

/*!
 * @brief
 *    list of supported ecosystems
 *
 * @note
 *    check LMOD_FLAG_* enum in module.h when changing this
 *    ecosystem type enum
 */
enum  ecosystem_type_e
{
    /*! dummy type */
    ECOSYS_NONE        = 0u,
    /*! arable */
    ECOSYS_ARABLE,
    /*! lowland arable (paddy rice) */
    ECOSYS_ARABLE_LOWLAND,
    /*! forest */
    ECOSYS_FOREST,
    /*! undisturbed natural forest */
    ECOSYS_FOREST_NATURAL,
    /*! grassland */
    ECOSYS_GRASSLAND,
    /*! wetland */
    ECOSYS_WETLAND,

    /*! number of supported ecosystem types */
    ECOSYS_CNT
};
extern CBM_API const char *  ECOSYSTEM_NAMES[ECOSYS_CNT + 1/*sentinel*/];

enum  cn_ratiolevel_e
{
    CNRATIO_LEVEL_LOW,
    CNRATIO_LEVEL_MEDIUM,
    CNRATIO_LEVEL_HIGH
};
extern CBM_API cn_ratiolevel_e const ECOSYSTEM_CN_RATIO_LEVELS[ECOSYS_CNT];


#endif  /*  !LDNDC_STATE_ECOSYSTEMTYPES_H_  */

