/*!
 * @brief
 *    co2 synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_CO2_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_CO2_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  co2_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(co2_t,CO2)
    };

    extern  co2_t CBM_API const  synth_airchemistry_co2;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_CO2_H_  */

