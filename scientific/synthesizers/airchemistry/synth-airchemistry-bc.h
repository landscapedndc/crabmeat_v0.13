/*!
 * @brief
 *    bc synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_BC_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_BC_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  bc_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(bc_t,BC)
    };

    extern  bc_t CBM_API const  synth_airchemistry_bc;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_BC_H_  */

