/*!
 * @brief
 *    no synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_NO_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_NO_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  no_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(no_t,NO)
    };

    extern  no_t CBM_API const  synth_airchemistry_no;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_NO_H_  */

