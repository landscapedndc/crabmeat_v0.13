/*!
 * @brief
 *    o2 synthesizer (implementation)
 *
 * @author
 *    david kraus
 */

#include  "synthesizers/airchemistry/synth-airchemistry-o2.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::o2_t::~o2_t()
{
}


lerr_t
ldndc::airchemistry::synth::o2_t::synthesize(
        record_item_t *  _o2_s,
        record_item_t *  _o2_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(O2);

    if ( _o2_d)
    {
        *_o2_d = _info->o2;
    }
    if ( _o2_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(o2,O2);
        if ( cbm::is_valid( o2_d))
        {
            *_o2_s = o2_d;
        }
        else
        {
            *_o2_s = ( _o2_d) ? *_o2_d : _info->o2;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(O2);
}


ldndc::airchemistry::synth::o2_t const  ldndc::airchemistry::synth::synth_airchemistry_o2;

