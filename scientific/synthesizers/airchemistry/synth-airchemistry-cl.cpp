/*!
 * @brief
 *    cl synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-cl.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::cl_t::~cl_t()
{
}


lerr_t
ldndc::airchemistry::synth::cl_t::synthesize(
        record_item_t *  _cl_s,
        record_item_t *  _cl_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(CL);

    if ( _cl_d)
    {
        *_cl_d = _info->cl;
    }
    if ( _cl_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(cl,CL);
        if ( cbm::is_valid( cl_d))
        {
            *_cl_s = cl_d;
        }
        else
        {
            *_cl_s = ( _cl_d) ? *_cl_d : _info->cl;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(CL);
}


ldndc::airchemistry::synth::cl_t const  ldndc::airchemistry::synth::synth_airchemistry_cl;

