/*!
 * @brief
 *    n synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_N_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_N_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  n_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(n_t,N)
    };

    extern  n_t CBM_API const  synth_airchemistry_n;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_N_H_  */

