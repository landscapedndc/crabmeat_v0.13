/*!
 * @brief
 *    bc synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-bc.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::bc_t::~bc_t()
{
}


lerr_t
ldndc::airchemistry::synth::bc_t::synthesize(
        record_item_t *  _bc_s,
        record_item_t *  _bc_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(BC);

    if ( _bc_d)
    {
        *_bc_d = _info->bc;
    }
    if ( _bc_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(bc,BC);
        if ( cbm::is_valid( bc_d))
        {
            *_bc_s = bc_d;
        }
        else
        {
            *_bc_s = ( _bc_d) ? *_bc_d : _info->bc;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(BC);
}


ldndc::airchemistry::synth::bc_t const  ldndc::airchemistry::synth::synth_airchemistry_bc;

