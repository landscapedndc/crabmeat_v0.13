/*!
 * @brief
 *    no3dry synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_NO3_DRY_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_NO3_DRY_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  no3dry_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(no3dry_t,NO3_DRY)
    };

    extern  no3dry_t CBM_API const  synth_airchemistry_no3dry;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_NO3_DRY_H_  */

