/*!
 * @brief
 *    o2 synthesizer
 *
 * @author
 *      david kraus
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_O2_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_O2_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  o2_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(o2_t,O2)
    };

    extern  o2_t CBM_API const  synth_airchemistry_o2;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_O2_H_  */

