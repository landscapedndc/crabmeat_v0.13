/*!
 * @brief
 *    na synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-na.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::na_t::~na_t()
{
}


lerr_t
ldndc::airchemistry::synth::na_t::synthesize(
        record_item_t *  _na_s,
        record_item_t *  _na_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NA);

    if ( _na_d)
    {
        *_na_d = _info->na;
    }
    if ( _na_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(na,NA);
        if ( cbm::is_valid( na_d))
        {
            *_na_s = na_d;
        }
        else
        {
            *_na_s = ( _na_d) ? *_na_d : _info->na;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NA);
}


ldndc::airchemistry::synth::na_t const  ldndc::airchemistry::synth::synth_airchemistry_na;

