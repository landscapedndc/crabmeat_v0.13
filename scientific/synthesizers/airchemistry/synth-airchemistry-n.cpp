/*!
 * @brief
 *    n synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-n.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::n_t::~n_t()
{
}


lerr_t
ldndc::airchemistry::synth::n_t::synthesize(
        record_item_t *  _n_s,
        record_item_t *  _n_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(N);

    if ( _n_d)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(nh4,NH4);
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(no3,NO3);
        if ( cbm::is_valid( nh4_d) && cbm::is_valid( no3_d))
        {
            *_n_d = nh4_d + no3_d;
        }
        else if ( cbm::is_valid( nh4_d))
        {
            *_n_d = nh4_d / NH4_FRAC;
        }
        else if ( cbm::is_valid( no3_d))
        {
            *_n_d = no3_d / NO3_FRAC;
        }
        else
        {
            *_n_d = _info->n;
        }
    }
    if ( _n_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(n,N);
                if ( cbm::is_valid( n_d))
                {
                        *_n_s = n_d;
                }
                else
        {
            *_n_s = ( _n_d) ? *_n_d : _info->n;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(N);
}


ldndc::airchemistry::synth::n_t const  ldndc::airchemistry::synth::synth_airchemistry_n;

