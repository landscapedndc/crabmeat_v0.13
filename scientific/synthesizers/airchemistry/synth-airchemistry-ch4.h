/*!
 * @brief
 *    ch4 synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_CH4_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_CH4_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  ch4_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(ch4_t,CH4)
    };

    extern  ch4_t CBM_API const  synth_airchemistry_ch4;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_CH4_H_  */

