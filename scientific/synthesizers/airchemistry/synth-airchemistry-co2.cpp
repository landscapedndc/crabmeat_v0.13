/*!
 * @brief
 *    co2 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-co2.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::co2_t::~co2_t()
{
}


lerr_t
ldndc::airchemistry::synth::co2_t::synthesize(
        record_item_t *  _co2_s,
        record_item_t *  _co2_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(CO2);

    if ( _co2_d)
    {
        *_co2_d = _info->co2;
    }
    if ( _co2_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(co2,CO2);
        if ( cbm::is_valid( co2_d))
        {
            *_co2_s = co2_d;
        }
        else
        {
            *_co2_s = ( _co2_d) ? *_co2_d : _info->co2;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(CO2);
}


ldndc::airchemistry::synth::co2_t const  ldndc::airchemistry::synth::synth_airchemistry_co2;

