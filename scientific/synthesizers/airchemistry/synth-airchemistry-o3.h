/*!
 * @brief
 *    o3 synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_O3_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_O3_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  o3_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(o3_t,O3)
    };

    extern  o3_t CBM_API const  synth_airchemistry_o3;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_O3_H_  */

