/*!
 * @brief
 *    na synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_NA_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_NA_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  na_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(na_t,NA)
    };

    extern  na_t CBM_API const  synth_airchemistry_na;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_NA_H_  */

