/*!
 * @brief
 *    ch4 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-ch4.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::ch4_t::~ch4_t()
{
}


lerr_t
ldndc::airchemistry::synth::ch4_t::synthesize(
        record_item_t *  _ch4_s,
        record_item_t *  _ch4_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(CH4);

    if ( _ch4_d)
    {
        *_ch4_d = _info->ch4;
    }
    if ( _ch4_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(ch4,CH4);
        if ( cbm::is_valid( ch4_d))
        {
            *_ch4_s = ch4_d;
        }
        else
        {
            *_ch4_s = ( _ch4_d) ? *_ch4_d : _info->ch4;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(CH4);
}


ldndc::airchemistry::synth::ch4_t const  ldndc::airchemistry::synth::synth_airchemistry_ch4;

