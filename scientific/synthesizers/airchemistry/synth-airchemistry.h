/*!
 * @brief
 *    airchemistry data synthesizer
 *
 * @author
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_H_

#include  "synthesizers/synth-base-streamdata.h"
#include  "airchemistry/airchemistrytypes.h"
#include  "time/cbm_date.h"


#define  SYNTH_AIRCHEMISTRY_REC_SIZE  SYNTH_REC_SIZE_(airchemistry)

#define  SYNTH_AIRCHEMISTRY_REC_S_DECL_GET(__reg__,__item__)  SYNTH_REC_DECL_GET_(airchemistry,__reg__,s,__item__)
#define  SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(__reg__,__item__)  SYNTH_REC_DECL_GET_(airchemistry,__reg__,d,__item__)

#define  SYNTH_AIRCHEMISTRY_REC_S_DECL_GET_AND_CHK_VALID(__reg__,__item__)  SYNTH_REC_DECL_GET_AND_CHK_VALID_(airchemistry,__reg__,s,__item__)
#define  SYNTH_AIRCHEMISTRY_REC_D_DECL_GET_AND_CHK_VALID(__reg__,__item__)  SYNTH_REC_DECL_GET_AND_CHK_VALID_(airchemistry,__reg__,d,__item__)

#define  SYNTH_AIRCHEMISTRY_INVALIDATE_BUFFER(__buf__)  SYNTH_INVALIDATE_BUFFER_(airchemistry,__buf__)

#define  SYNTH_AIRCHEMISTRY_CHK_MATCHING(__item__)  SYNTH_CHK_MATCHING_(airchemistry,__item__)

#define  SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(__item__)  SYNTH_CALL_LOOP_TEST_AND_SET_(airchemistry,__item__)
#define  SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(__item__)  SYNTH_RETURN_SUCCESS_(airchemistry,__item__)

#define  SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(__class__,__item_enum__)  SYNTH_RECORD_ITEM_COMMON_DECL_(airchemistry,__class__,__item_enum__)


#define  NH4_FRAC  (0.635)
#define  NO3_FRAC  (1.0 - NH4_FRAC)

template < typename, unsigned int >
class  lvector_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
typedef  record::item_type  record_item_t;
struct  scratch_t  :  public  ldndc::synth::streamdata_scratch_base_t< streamdata_info_t >
{
    lerr_t  initialize(
            airchemistry_info_t const *  /* boundary info */,
            ltime_t const *  /* input date */,
            ltime_t const *  /* output date */);

    lerr_t  configure(
            airchemistry_info_t const *  /* boundary info */,
            ldate_t const *  /* current date */,
            unsigned int  /* current target subday */);
};

struct  CBM_API  item_base_t  :  public  ldndc::synth::streamdata_item_base_t< streamdata_info_t, scratch_t >
{
};


class  CBM_API  lsynth_airchemistry_t  :  public  ldndc::synth::streamdata_synth_base_t< streamdata_info_t >
{
    public:
        lsynth_airchemistry_t();
        lsynth_airchemistry_t(
                airchemistry_info_t const *  /* airchemistry meta info */,
                ltime_t const *  /* input date */,
                ltime_t const *  /* output date */);

        virtual  ~lsynth_airchemistry_t();

        /*!
         * @brief
         *    before synthesizing, update internal structures
         */
        lerr_t  configure(
                ldate_t const *  /* target date */,
                unsigned int = ldndc::invalid_t< unsigned int >::value /* target subday */);

        /*!
         * @brief
         *    synthesize values for invalid record items
         */
        lerr_t  synthesize_record(
                record_item_t const * /* subday data buffer (input) */, record_item_t * /* subday data buffer (output) */,
                record_item_t const * /* day data buffer (input) */, record_item_t * /* day data buffer (output) */,

                ldate_t const *  /* current date */,

                lvector_t< unsigned int, 1 > * = NULL  /* modification count */);

        /*!
         * @brief
         *    check record for plausible values
         *
         * @todo
         *    use bitset?
         */
        lerr_t  check_record(
                record_item_t const * /* subday data buffer (input) */,
                record_item_t const * /* day data buffer (input) */) const;

        inline
        unsigned int  subday()
        const
        {
            return  this->scratch_.subday;
        }

        inline
        unsigned int  time_resolution_inout()
        const
        {
            return  this->scratch_.t_res_inout;
        }

    private:
        airchemistry_info_t  info_;
        scratch_t  scratch_;
};
extern  item_base_t const *  airchemistry_synthesizers[record::RECORD_SIZE];
}  /*  namespace synth  */
}  /*  namespace airchemistry  */
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_H_  */

