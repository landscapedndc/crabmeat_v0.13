/*!
 * @brief
 *    so4 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-so4.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::so4_t::~so4_t()
{
}


lerr_t
ldndc::airchemistry::synth::so4_t::synthesize(
        record_item_t *  _so4_s,
        record_item_t *  _so4_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(SO4);

    if ( _so4_d)
    {
        *_so4_d = _info->so4;
    }
    if ( _so4_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(so4,SO4);
        if ( cbm::is_valid( so4_d))
        {
            *_so4_s = so4_d;
        }
        else
        {
            *_so4_s = ( _so4_d) ? *_so4_d : _info->so4;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(SO4);
}


ldndc::airchemistry::synth::so4_t const  ldndc::airchemistry::synth::synth_airchemistry_so4;

