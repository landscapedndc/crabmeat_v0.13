/*!
 * @brief
 *    nh4dry synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-nh4dry.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::nh4dry_t::~nh4dry_t()
{
}


lerr_t
ldndc::airchemistry::synth::nh4dry_t::synthesize(
        record_item_t *  _nh4dry_s,
        record_item_t *  _nh4dry_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NH4_DRY);

    if ( _nh4dry_d)
    {
        *_nh4dry_d = 0.0;
    }
    if ( _nh4dry_s)
    {
        *_nh4dry_s = 0.0;
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NH4_DRY);
}


ldndc::airchemistry::synth::nh4dry_t const  ldndc::airchemistry::synth::synth_airchemistry_nh4dry;

