/*!
 * @brief
 *    no3 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-no3.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::no3_t::~no3_t()
{
}


lerr_t
ldndc::airchemistry::synth::no3_t::synthesize(
        record_item_t *  _no3_s,
        record_item_t *  _no3_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NO3);

    if ( _no3_d)
    {
        *_no3_d = _info->no3;
    }
    if ( _no3_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(no3,NO3);
        if ( cbm::is_valid( no3_d))
        {
            *_no3_s = no3_d;
        }
        else
        {
            *_no3_s = _info->no3;
        }
    }


    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NO3);
}


ldndc::airchemistry::synth::no3_t const  ldndc::airchemistry::synth::synth_airchemistry_no3;

