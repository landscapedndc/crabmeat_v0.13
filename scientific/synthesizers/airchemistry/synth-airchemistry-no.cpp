/*!
 * @brief
 *    no synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-no.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::no_t::~no_t()
{
}


lerr_t
ldndc::airchemistry::synth::no_t::synthesize(
        record_item_t *  _no_s,
        record_item_t *  _no_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NO);

    if ( _no_d)
    {
        *_no_d = _info->no;
    }
    if ( _no_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(no,NO);
        if ( cbm::is_valid( no_d))
        {
            *_no_s = no_d;
        }
        else
        {
            *_no_s = ( _no_d) ? *_no_d : _info->no;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NO);
}


ldndc::airchemistry::synth::no_t const  ldndc::airchemistry::synth::synth_airchemistry_no;

