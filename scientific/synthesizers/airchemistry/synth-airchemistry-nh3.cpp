/*!
 * @brief
 *    nh3 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-nh3.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::nh3_t::~nh3_t()
{
}


lerr_t
ldndc::airchemistry::synth::nh3_t::synthesize(
        record_item_t *  _nh3_s,
        record_item_t *  _nh3_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NH3);

    if ( _nh3_d)
    {
        *_nh3_d = _info->nh3;
    }
    if ( _nh3_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(nh3,NH3);
        if ( cbm::is_valid( nh3_d))
        {
            *_nh3_s = nh3_d;
        }
        else
        {
            *_nh3_s = ( _nh3_d) ? *_nh3_d : _info->nh3;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NH3);
}


ldndc::airchemistry::synth::nh3_t const  ldndc::airchemistry::synth::synth_airchemistry_nh3;

