/*!
 * @brief
 *    cl synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_AIRCHEMISTRY_CL_H_
#define  LDNDC_DATASYNTH_AIRCHEMISTRY_CL_H_

#include  "synthesizers/airchemistry/synth-airchemistry.h"

class  ldate_t;

namespace  ldndc{ namespace  airchemistry{ namespace  synth
{
    struct  CBM_API  cl_t  :  public  ldndc::airchemistry::synth::item_base_t
    {
        SYNTH_AIRCHEMISTRY_RECORD_ITEM_COMMON_DECL(cl_t,CL)
    };

    extern  cl_t CBM_API const  synth_airchemistry_cl;
}}}

#endif  /*  !LDNDC_DATASYNTH_AIRCHEMISTRY_CL_H_  */

