/*!
 * @brief
 *    nh4 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-nh4.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::nh4_t::~nh4_t()
{
}


lerr_t
ldndc::airchemistry::synth::nh4_t::synthesize(
        record_item_t *  _nh4_s,
        record_item_t *  _nh4_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NH4);

    if ( _nh4_d)
    {
        *_nh4_d = _info->nh4;
    }
    if ( _nh4_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(nh4,NH4);
        if ( cbm::is_valid( nh4_d))
        {
            *_nh4_s = nh4_d;
        }
        else
        {
            *_nh4_s = _info->nh4;
        }
    }


    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NH4);
}


ldndc::airchemistry::synth::nh4_t const  ldndc::airchemistry::synth::synth_airchemistry_nh4;

