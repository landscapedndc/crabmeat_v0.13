/*!
 * @brief
 *    no2 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-no2.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::no2_t::~no2_t()
{
}


lerr_t
ldndc::airchemistry::synth::no2_t::synthesize(
        record_item_t *  _no2_s,
        record_item_t *  _no2_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(NO2);

    if ( _no2_d)
    {
        *_no2_d = _info->no2;
    }
    if ( _no2_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(no2,NO2);
        if ( cbm::is_valid( no2_d))
        {
            *_no2_s = no2_d;
        }
        else
        {
            *_no2_s = ( _no2_d) ? *_no2_d : _info->no2;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(NO2);
}


ldndc::airchemistry::synth::no2_t const  ldndc::airchemistry::synth::synth_airchemistry_no2;

