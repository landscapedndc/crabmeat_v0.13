/*!
 * @brief
 *    o3 synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/airchemistry/synth-airchemistry-o3.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::airchemistry::synth::o3_t::~o3_t()
{
}


lerr_t
ldndc::airchemistry::synth::o3_t::synthesize(
        record_item_t *  _o3_s,
        record_item_t *  _o3_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        airchemistry_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check airchemistry boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_AIRCHEMISTRY_CALL_LOOP_TEST_AND_SET(O3);

    if ( _o3_d)
    {
        *_o3_d = _info->o3;
    }
    if ( _o3_s)
    {
        SYNTH_AIRCHEMISTRY_REC_D_DECL_GET(o3,O3);
        if ( cbm::is_valid( o3_d))
        {
            *_o3_s = o3_d;
        }
        else
        {
            *_o3_s = ( _o3_d) ? *_o3_d : _info->o3;
        }
    }

  
    SYNTH_AIRCHEMISTRY_RETURN_SUCCESS(O3);
}


ldndc::airchemistry::synth::o3_t const  ldndc::airchemistry::synth::synth_airchemistry_o3;

