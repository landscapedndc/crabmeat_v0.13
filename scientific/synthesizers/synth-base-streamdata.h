/*!
 * @brief
 *    common stream data synthesizer base
 *
 * @author
 *      steffen klatt (created on: aug 28, 2013)
 */

#ifndef  LDNDC_DATASYNTH_COMMON_STREAMDATA_H_
#define  LDNDC_DATASYNTH_COMMON_STREAMDATA_H_

#include  "synthesizers/synth.h"
#include  "time/cbm_date.h"

#define  SYNTH_REC_SIZE_(__ic__)  ((size_t)__ic__::record::RECORD_SIZE)

#define  SYNTH_REC_GET_(__ic__,__reg__,__item__)  (__reg__)[__ic__::record::RECORD_ITEM_ ## __item__]

#define  SYNTH_REC_DECL_GET_(__ic__,__reg__,__t__,__item__)            \
    element_type const  __reg__##_##__t__(                    \
            SYNTH_REC_GET_(__ic__,_scratch->rec_##__t__,__item__))/*;*/

#define  SYNTH_REC_DECL_GET_AND_CHK_VALID_(__ic__,__reg__,__t__,__item__)    \
    SYNTH_REC_DECL_GET_(__ic__,__reg__,__t__,__item__);            \
    if ( cbm::is_invalid( __reg__##_##__t__))                \
    {                                    \
        CBM_LogError( "dependency was invalid:", #__item__);        \
        return  LDNDC_ERR_FAIL;                        \
    }

#define  SYNTH_INVALIDATE_BUFFER_(__ic__,__buf__)                \
            cbm::mem_set(                            \
            __buf__, static_cast< size_t >(                \
                __ic__::record::RECORD_SIZE),        \
                ldndc::invalid_t< __ic__::record::item_type >::value)/*;*/
       
#ifdef  _DEBUG
#  define  SYNTH_CHK_MATCHING_(__ic__,__item__)                            \
    if ( __ic__::record::RECORD_ITEM_ ## __item__ != this->record_item_type())    \
    {                                            \
        CBM_LogFatal( "types not matching ..",                        \
            __ic__::RECORD_ITEM_NAMES[__ic__::record::RECORD_ITEM_##__item__],\
            "vs",                                    \
                   __ic__::RECORD_ITEM_NAMES[this->record_item_type()]);        \
    }
#else
#  define  SYNTH_CHK_MATCHING_(__ic__,__item__) /*no op*/
#endif

#define  SYNTH_CALL_LOOP_TEST_AND_SET_(__ic__,__item__)                        \
    SYNTH_CHK_MATCHING_(__ic__,__item__)                            \
    if ( _scratch->call_mask_test( __ic__::record::RECORD_ITEM_ ## __item__))        \
    {                                            \
        CBM_LogError( "detected loop in ",#__ic__," synthesizer calls (", #__item__, ")");    \
        return  LDNDC_ERR_FAIL;                                \
    }                                            \
    _scratch->call_mask_set( __ic__::record::RECORD_ITEM_ ## __item__)/*;*/
#define  SYNTH_RETURN_SUCCESS_(__ic__,__item__)                            \
    _scratch->call_mask_unset( __ic__::record::RECORD_ITEM_ ## __item__);            \
    return  LDNDC_ERR_OK/*;*/


#define  SYNTH_RECORD_ITEM_COMMON_DECL_(__ic__,__class__,__item_enum__)                \
    __class__() {}                                        \
    enum                                            \
    {                                            \
        R = record::RECORD_ITEM_## __item_enum__                    \
    };                                            \
    record::record_item_e  record_item_type()                        \
    const                                            \
    {                                            \
        return  (record::record_item_e)R;                        \
    }                                            \
                                                \
    ~__class__();                                        \
                                                \
    lerr_t  synthesize(                                    \
            element_type * /* subday data item (output) */,                \
            element_type * /* day data item (output) */,                \
                                                \
            scratch_t *  /* scratch object */,                    \
                                                \
            ldate_t const *  /* current date */,                    \
            unsigned int  /* subday to synthesize */,                \
                                                \
            __ic__##_info_t const *  /* meta info */) const;


template < typename, unsigned int >
class  lvector_t;

namespace  ldndc{ namespace  synth
{
    template < typename  _SD >
    struct  streamdata_scratch_base_t
    {
        virtual  ~streamdata_scratch_base_t() = 0;
        /* holds updated data items */
        /*    subday */
        typename _SD::element_type  rec_s[_SD::RECORD_SIZE];
        /*    day */
        typename _SD::element_type  rec_d[_SD::RECORD_SIZE];

        unsigned int  src_res, tgt_res;
        unsigned int  t_res_inout;
        unsigned int  subday;

        /* shifted according to latitude */
        unsigned int  julian_day;

        double  day_fraction()
            const
            {
                return  1.0 / (double)this->tgt_res;
            }
        double  t_scale()
            const
            {
                return  24.0 / (double)this->tgt_res/* FIXME  t_res_inout ??? */;
            }

        virtual  lerr_t  initialize(
                typename _SD::boundary_data_type const *  /* boundary info */,
                ltime_t const *  /* input date */,
                ltime_t const *  /* output date */) = 0;

        virtual  lerr_t  configure(
                typename _SD::boundary_data_type const *  /* boundary info */,
                ldate_t const *  /* current date */,
                unsigned int  /* current target subday */) = 0;

        /* helper to detect loops in nested synthesizer calls (no distinction between subdaily/daily) */
        lflags_t  call_mask;
        void  call_mask_set(
                typename _SD::record_item_e  _e)
        {
            this->call_mask |= (lflags_t)1 << _e;
        }
        void  call_mask_unset(
                typename _SD::record_item_e  _e)
        {
            this->call_mask &= ~((lflags_t)1 << _e);
        }
        bool  call_mask_test(
                typename _SD::record_item_e  _e) const
        {
            return  (( this->call_mask & ((lflags_t)1 << _e)) != 0);
        }
    };

    template < typename  _SD >
    streamdata_scratch_base_t< _SD >::~streamdata_scratch_base_t()
    {
    }

    template < typename  _SD, typename  _SCRATCH_T >
    struct  streamdata_item_base_t
    {
        typedef  typename _SD::element_type  element_type;

        virtual  ~streamdata_item_base_t() = 0;

        virtual  typename _SD::record_item_e  record_item_type() const = 0;

        virtual  lerr_t  synthesize(
                typename _SD::element_type * /* subday data item (output) */,
                typename _SD::element_type * /* day data item (output) */,

                _SCRATCH_T *  /* scratch object */,

                ldate_t const *  /* current date */,
                unsigned int  /* subday to synthesize */,

                typename _SD::boundary_data_type const *  /* boundary info */) const = 0;
    };

    template < typename  _SD, typename  _SCRATCH_T >
    streamdata_item_base_t< _SD, _SCRATCH_T >::~streamdata_item_base_t()
    {
    }

    template < typename  _SD >
    class  streamdata_synth_base_t  :  public  lsynth_base_t
    {
        public:
            streamdata_synth_base_t();
            virtual  ~streamdata_synth_base_t() = 0;

            /*!
             * @brief
             *    before synthesizing, update internal structures
             */
            virtual  lerr_t  configure(
                    ldate_t const *  /* target date */,
                    unsigned int = ldndc::invalid_t< unsigned int >::value /* target subday */) = 0;

            /*!
             * @brief
             *    synthesize values for invalid record items
             */
            virtual  lerr_t  synthesize_record(
                    typename _SD::element_type const * /* subday data buffer (input) */, typename _SD::element_type * /* subday data buffer (output) */,
                    typename _SD::element_type const * /* day data buffer (input) */, typename _SD::element_type * /* day data buffer (output) */,

                    ldate_t const *  /* current date */,

                    lvector_t< unsigned int, 1 > * = NULL  /* modification count */) = 0;

            /*!
             * @brief
             *    check record for plausible values
             *
             * @todo
             *    use bitset?
             */
            virtual  lerr_t  check_record(
                    typename _SD::element_type const * /* subday data buffer (input) */,
                    typename _SD::element_type const * /* day data buffer (input) */) const = 0;
    };

    template < typename  _SD >
    streamdata_synth_base_t< _SD >::streamdata_synth_base_t()
                : lsynth_base_t()
    {
    }

    template < typename  _SD >
    streamdata_synth_base_t< _SD >::~streamdata_synth_base_t()
    {
    }

}  /*  namespace synth  */
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_DATASYNTH_COMMON_STREAMDATA_H_  */

