/*!
 * @brief
 *    data synthesizer base class
 *
 * @author
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_BASE_H_
#define  LDNDC_DATASYNTH_BASE_H_

#include  "crabmeat-common.h"

namespace  ldndc { namespace synth
{
    struct  CBM_API  lsynth_base_t
    {
        virtual  ~lsynth_base_t() = 0;
    };
}  /*  namespace synth  */
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_DATASYNTH_BASE_H_  */

