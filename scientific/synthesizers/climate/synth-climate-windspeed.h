/*!
 * @brief
 *    wind speed synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_WINDSPEED_H_
#define  LDNDC_DATASYNTH_CLIMATE_WINDSPEED_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  windspeed_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(windspeed_t,WIND_SPEED)
    };

    extern  windspeed_t CBM_API const  synth_climate_windspeed;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_WINDSPEED_H_  */

