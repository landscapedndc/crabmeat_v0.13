/*!
 * @brief
 *    precipitation synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_PRECIP_H_
#define  LDNDC_DATASYNTH_CLIMATE_PRECIP_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  precipitation_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(precipitation_t,PRECIP)
    };

    extern  precipitation_t CBM_API const  synth_climate_precip;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_PRECIP_H_  */

