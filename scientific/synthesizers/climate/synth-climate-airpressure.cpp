/*!
 * @brief
 *    air pressure synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-airpressure.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  <math.h>
#include  "constants/lconstants-meteo.h"


ldndc::climate::synth::air_pressure_t::~air_pressure_t()
{
}


lerr_t
ldndc::climate::synth::air_pressure_t::synthesize(
        record_item_t *  _air_pressure_s,
        record_item_t *  _air_pressure_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        climate_info_t const *  _c_info)
const
{
    crabmeat_assert( _c_info);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(AIR_PRESSURE);

    record_item_t  this_air_pressure = cbm::PRESS0 * exp( -_c_info->elevation / cbm::ELEV0);

    if ( _air_pressure_d)
    {
        *_air_pressure_d = this_air_pressure;
    }
    if ( _air_pressure_s)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET(air_pressure,AIR_PRESSURE);
        if ( cbm::is_valid( air_pressure_d))
        {
            *_air_pressure_s = air_pressure_d;
        }
        else
        {
            *_air_pressure_s = this_air_pressure;
        }
    }

  
    SYNTH_CLIMATE_RETURN_SUCCESS(AIR_PRESSURE);
}


ldndc::climate::synth::air_pressure_t const  ldndc::climate::synth::synth_climate_airpressure;

