/*!
 * @brief
 *    air pressure synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_AIRPRESSURE_H_
#define  LDNDC_DATASYNTH_CLIMATE_AIRPRESSURE_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  air_pressure_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(air_pressure_t,AIR_PRESSURE)
    };

    extern  air_pressure_t CBM_API const  synth_climate_airpressure;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_AIRPRESSURE_H_  */

