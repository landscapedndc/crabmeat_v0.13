/*!
 * @brief
 *    temperature average synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_TEMPAVG_H_
#define  LDNDC_DATASYNTH_CLIMATE_TEMPAVG_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  temperature_average_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(temperature_average_t,TEMP_AVG)
    };

    extern  temperature_average_t CBM_API const  synth_climate_tempavg;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_TEMPAVG_H_  */

