/*!
 * @brief
 *    Relative humidity synthesizer (implementation)
 *
 * @author
 *    Ruediger Grote,
 *    Steffen Klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-relhumidity.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  "scientific/meteo/ld_meteo.h"

ldndc::climate::synth::relative_humidity_t::~relative_humidity_t()
{}

/*!
 * @page climate_synthesizer
 * @section climate_synthesizer_rh Relative humidity
 * \f[
 * vps := f(T)
 * \f]
 * If vapour pressure deficit is given:
 * \f[
 * rh = 100 \left( 1 - \frac{vpd}{vps} \right)
 * \f]
 * If vapour pressure deficit is not given, the following assumptions are made:
 * - Saturated conditions at \f$ T_{min} \f$ (i.e., condensation at night time).
 * - Constant amount of water during the day
 */
lerr_t
ldndc::climate::synth::relative_humidity_t::synthesize(
        record_item_t *  _rel_humidity_s /* [%] */,
        record_item_t *  _rel_humidity_d /* [%] */,
        scratch_t *  _scratch,
        ldate_t const *,
        unsigned int,
        climate_info_t const *)
const
{
    crabmeat_assert( _scratch);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(REL_HUMUDITY);

    if ( _rel_humidity_d)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
        double  vps_d;
        ldndc::meteo::vps( t_avg_d, &vps_d);

        SYNTH_CLIMATE_REC_D_DECL_GET(vpd,VPD);
        if ( cbm::is_valid( vpd_d))
        {
            *_rel_humidity_d = cbm::bound( 0.0, 100.0 * ( 1.0 - vpd_d / vps_d), 100.0);
        }
        else
        {
            SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_min,TEMP_MIN);
            double  vps_min;
            ldndc::meteo::vps( t_min_d, &vps_min);
            *_rel_humidity_d = cbm::bound( 0.0, 100.0 * ( vps_min / vps_d), 100.0);
        }
    }

    if ( _rel_humidity_s)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET(rel_humidity,REL_HUMUDITY);
        if ( cbm::is_valid( rel_humidity_d))
        {
            SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
            double  vps_d;
            ldndc::meteo::vps( t_avg_d, &vps_d);
            
            SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
            double  vps_s;
            ldndc::meteo::vps( t_avg_s, &vps_s);

            *_rel_humidity_s = cbm::bound( 0.0, (vps_d / vps_s) * rel_humidity_d, 100.0);
        }
        else
        {
            SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
            double  vps_s;
            ldndc::meteo::vps( t_avg_s, &vps_s);
            
            SYNTH_CLIMATE_REC_S_DECL_GET(vpd,VPD);
            if ( cbm::is_valid( vpd_s))
            {
                *_rel_humidity_s = cbm::bound( 0.0, 100.0 * ( 1.0 - vpd_s / vps_s), 100.0);
            }
            else
            {
                SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_min,TEMP_MIN);
                double  vps_min;
                ldndc::meteo::vps( t_min_d, &vps_min);
                *_rel_humidity_s = cbm::bound( 0.0, 100.0 * ( vps_min / vps_s), 100.0);
            }
        }
    }

    SYNTH_CLIMATE_RETURN_SUCCESS(REL_HUMUDITY);
}


ldndc::climate::synth::relative_humidity_t const  ldndc::climate::synth::synth_climate_relhumidity;

