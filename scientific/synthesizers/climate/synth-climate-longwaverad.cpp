/*!
 * @brief
 *    long wave radiation synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-longwaverad.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  "math/cbm_math.h"
#include  "scientific/meteo/ld_meteo.h"


ldndc::climate::synth::longwave_radiation_t::~longwave_radiation_t()
{
}


lerr_t
ldndc::climate::synth::longwave_radiation_t::synthesize(
        record_item_t *  _lw_rad_s /* [Wm-2] */,
        record_item_t *  _lw_rad_d /* [Wm-2] */,
        scratch_t *  _scratch,
        ldate_t const *  _date,
        unsigned int,
        climate_info_t const *  _c_info)
const
{
    crabmeat_assert( _scratch);
    crabmeat_assert( _c_info);
    crabmeat_assert( _date);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(LONGWAVE_RAD);

    if ( _lw_rad_d)
    {
//        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_min,TEMP_MIN);
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
//        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_max,TEMP_MAX);
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(ex_rad,EX_RAD);
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(vpd,VPD);
                
        double vps( 0.0);
        ldndc::meteo::vps( t_avg_d, &vps);
        
        double const vp( cbm::bound_min( 0.0, vps - vpd_d));
        
        /* incoming longwave radiation */
        double const cloud_fraction( ldndc::meteo::cloud_fraction_angstrom( _scratch->julian_day, _date->days_in_year(),
                                                                            _c_info->latitude, ex_rad_d));
        *_lw_rad_d = ldndc::meteo::longwave_radiation( cloud_fraction, t_avg_d, vp);

        
//        double const  radmax( ldndc::meteo::daily_solar_radiation(
//                                                                  _c_info->latitude,
//                                                                  static_cast< unsigned int >( _scratch->julian_day),
//                                                                  static_cast< unsigned int >( _date->days_in_year())));
//        // according to Cai et al. 2007 (note: 210^-5 is interpreted as 0.000021)
//        double const  radsol(( 0.75 + 2.1e-05 * _c_info->elevation) * radmax);
//        if ( radsol > 0.0)
//        {
//            *_lw_rad_d =
//            0.5 * cbm::SIGMA_MJ * cbm::J_IN_MJ * cbm::M2_IN_CM2
//            * ( cbm::sqr( cbm::sqr( t_max_d + cbm::D_IN_K)) + cbm::sqr( cbm::sqr( t_min_d + cbm::D_IN_K)))
//            * ( 0.34 - 0.14 * sqrt( vps * cbm::KPA_IN_MBAR))
//            * (( 1.35 * ex_rad_d / radsol) - 0.35);
//            *_lw_rad_d *= cbm::WM_IN_JDCM;
//        }
//        else
//        {
//            *_lw_rad_d = 0.0;
//        }
    }

    if ( _lw_rad_s)
    {
//        // - instantaneous long wave radiation
//        /* after Nagar et al. 2002, with adiabatic lapse rate (GAMMA) */
//
//          // dry adiabatic temperature lapse rate (oC km-1)
//          static double const  GAMMA( 0.0);
//
//          // relation between atmospheric scale height and vapor scale height (between 10 and 40o lat)
//          static double const  HHW( 4.0);
  
        SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
        SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(ex_rad,EX_RAD);
        SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(vpd,VPD);
        
        double  vps( 0.0);
        ldndc::meteo::vps( t_avg_s, &vps);
        
        double const vp( cbm::bound_min( 0.0, vps - vpd_s));
        
        /* incoming longwave radiation */
        double const cloud_fraction( ldndc::meteo::cloud_fraction_angstrom( _scratch->julian_day, _date->days_in_year(),
                                                                            _c_info->latitude, ex_rad_s));
        *_lw_rad_s = ldndc::meteo::longwave_radiation( cloud_fraction, t_avg_s, vp);
//          *_lw_rad_s = cbm::SIGMA
//            * cbm::sqr( cbm::sqr( t_avg_s + cbm::D_IN_K))
//              * (( 0.684 * pow( vps - vpd_s, 1.0/9.0)
//              / ( pow( 1.0 + HHW, 1.0/9.0))
//              * ( 1.0 + ( 0.2 * ( 1.0 - 0.117 * GAMMA)) / ( 1.0 + cbm::SIGMA))
//              + 0.0656 - 0.0003 * ( t_avg_s + cbm::D_IN_K - 288.0)));
      }
  

    SYNTH_CLIMATE_RETURN_SUCCESS(LONGWAVE_RAD);
}

ldndc::climate::synth::longwave_radiation_t const  ldndc::climate::synth::synth_climate_lwrad;

