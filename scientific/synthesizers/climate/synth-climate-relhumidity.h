/*!
 * @brief
 *    relative humidity synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_RELHUMDITY_H_
#define  LDNDC_DATASYNTH_CLIMATE_RELHUMDITY_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  relative_humidity_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(relative_humidity_t,REL_HUMUDITY)
    };

    extern  relative_humidity_t CBM_API const  synth_climate_relhumidity;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_RELHUMDITY_H_  */

