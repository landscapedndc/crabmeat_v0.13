/*!
 * @brief
 *    extraterrestrial radiation synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *    steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-exrad.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  "scientific/meteo/ld_meteo.h"


ldndc::climate::synth::extraterrestrial_radiation_t::~extraterrestrial_radiation_t()
{
}


lerr_t
ldndc::climate::synth::extraterrestrial_radiation_t::synthesize(
        record_item_t *  _ex_rad_s /* [Wm-2] */,
        record_item_t *  _ex_rad_d /* [Wm-2] */,

        scratch_t *  _scratch,

        ldate_t const *  _date,
        unsigned int  _s,

        climate_info_t const *  _c_info)
const
{
    CBM_Assert( _scratch);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(EX_RAD);

    if ( _ex_rad_d)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET(t_max,TEMP_MAX);
        SYNTH_CLIMATE_REC_D_DECL_GET(t_min,TEMP_MIN);

        double const  radmax( ldndc::meteo::daily_solar_radiation(
                    _c_info->latitude,
                    static_cast< unsigned int >( _scratch->julian_day),
                    static_cast< unsigned int >( _date->days_in_year())));

        // according to Hargreaves and Samani 1982 (cit. in Fortin et al. 2008)
        if ( cbm::is_valid( t_max_d) && cbm::is_valid( t_min_d))
        {
            if ( t_max_d < t_min_d)
            {
                return  LDNDC_ERR_FAIL;
            }
            *_ex_rad_d = cbm::bound( 0.0, 0.17 * sqrt( t_max_d - t_min_d) * radmax, 0.99*radmax);

            // TODO: This needs an slope/aspect correction!!
        }
        else
        {
            *_ex_rad_d = radmax * ( 1.0 - _c_info->cloudiness);
        }
    }

    if ( _ex_rad_s)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(ex_rad,EX_RAD);

        *_ex_rad_s = 0.0;

        double  hour_ts( _s * _scratch->t_scale());

        double const  D( ldndc::meteo::daylength( _c_info->latitude, static_cast< unsigned int >( _date->julian_day())));
        // hour of sunrise
        double const  RISE( 12.0 - D * 0.5);
        double const  RISE_24( 24.0 - RISE);
        
        if ( cbm::flt_in_range( RISE, hour_ts, RISE_24))
        {
            *_ex_rad_s = ((-cos(2.0 * cbm::PI * (hour_ts - RISE) / D) + 1.0) / D) * (cbm::HR_IN_DAY * ex_rad_d);
        }
    }

    SYNTH_CLIMATE_RETURN_SUCCESS(EX_RAD);
}


ldndc::climate::synth::extraterrestrial_radiation_t const  ldndc::climate::synth::synth_climate_exrad;

