/*!
 * @brief
 *    extraterrestrial radiation synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_EXRAD_H_
#define  LDNDC_DATASYNTH_CLIMATE_EXRAD_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  extraterrestrial_radiation_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(extraterrestrial_radiation_t,EX_RAD)
    };

    extern  extraterrestrial_radiation_t CBM_API const  synth_climate_exrad;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_EXRAD_H_  */

