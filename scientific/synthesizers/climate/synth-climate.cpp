/*!
 * @brief
 *   Climate data synthesizer (implementation)
 *
 * @author
 *   Ruediger Grote,
 *   Steffen Klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"

#include  "constants/cbm_const.h"
#include  "math/cbm_math.h"
#include  "scientific/meteo/ld_meteo.h"

#include  "containers/cbm_vector.h"

#include  <stdlib.h>
#include  <time.h>


double
ldndc::climate::synth::scratch_t::fp_base(
        double  _latitude)
const
{
    if (( _latitude > 60.0) || ( _latitude < -60.0))
    {
        return  50.0;
    }
    else if ( _latitude > 40.0)
    {
        return  50.0 - 2.0 * ( 60.0 - _latitude);
    }
    else if ( _latitude < -40.0)
    {
        return  50.0 - 2.0 * ( 60.0 + _latitude);
    }
    else
    {
        return  10.0;
    }
}


double
ldndc::climate::synth::scratch_t::precip_value_fact_fun(
        double  _precip_sum,
        unsigned int  _diy)
const
{
    return  ( 2.0 * _precip_sum) / ( double( _diy) * cbm::sqr( 1.0 - this->precip_exp));
}

lerr_t
ldndc::climate::synth::scratch_t::initialize(
        climate_info_t const *  _info,
        ltime_t    const *  _time_in,
        ltime_t    const *  _time_out)
{
    ldate_t const  time_in( _time_in->from());

    this->src_res = _time_in->time_resolution();
    this->tgt_res = _time_out->time_resolution();
    this->t_res_inout = this->tgt_res / this->src_res;
    CBM_LogDebug( "t-res-src=",this->src_res, ", t-res-tgt=",this->tgt_res, ",  t-res-inout=",this->t_res_inout);
    this->subday = 1;

    /**  shifted julian day  **/
    unsigned int  ylen = static_cast< unsigned int >( time_in.days_in_year());
    this->julian_day = static_cast< unsigned int >( time_in.julian_day());
    ldndc::meteo::latitude_rad( _info->latitude, &this->julian_day, NULL, &ylen);

    /**  precipitation  **/
    this->precip_rng =
        cbm::random::rng_uniform_t( 141592653u, 897932384u, 264338327u, 502884197);
  
    this->precip_d = ldndc::invalid_t< record_item_t >::value;
    this->precip_overshoot = PRECIP_OVERSHOOT_MAX;
    this->precip_exp = exp( -0.15 * this->fp_base( _info->latitude) / _info->rainfall_intensity);
    this->precip_value_fact = this->precip_value_fact_fun(
        _info->precip_sum, static_cast< unsigned int >( time_in.days_in_year()));
    this->precip_generated = ( time_in.julian_day() == 1u) ? 0.0 : 
        ( _info->precip_sum * ( static_cast< double >( time_in.julian_day() - 1) / static_cast< double >( time_in.days_in_year())));

    /* reset call board */
    call_mask = 0;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::climate::synth::scratch_t::configure(
        climate_info_t const *  _info,
        ldate_t const *  _date,
        unsigned int  _s)
{
    crabmeat_assert( _info);
    crabmeat_assert( _date);

    this->subday = _s;
    this->julian_day = static_cast< unsigned int >( _date->julian_day());

    unsigned int  ylen = static_cast< unsigned int >(  _date->days_in_year());
    ldndc::meteo::latitude_rad( _info->latitude, &this->julian_day, NULL, &ylen);

    /* beginning of each day */
    if ( this->subday == 1)
    {
        this->precip_d = ldndc::invalid_t< record_item_t >::value;
    }

    /* new year... */
    if (( this->subday == 1) && ( this->julian_day == 1u))
    {
        this->precip_value_fact = precip_value_fact_fun(
            _info->precip_sum, static_cast< unsigned int >( _date->days_in_year()));
        this->precip_generated = 0.0;
    }

    /* reset call board */
    call_mask = 0;

    return  LDNDC_ERR_OK;
}


#include  "synthesizers/climate/synth-climate-airpressure.h"
#include  "synthesizers/climate/synth-climate-exrad.h"
#include  "synthesizers/climate/synth-climate-longwaverad.h"
#include  "synthesizers/climate/synth-climate-precip.h"
#include  "synthesizers/climate/synth-climate-relhumidity.h"
#include  "synthesizers/climate/synth-climate-tempavg.h"
#include  "synthesizers/climate/synth-climate-tempmax.h"
#include  "synthesizers/climate/synth-climate-tempmin.h"
#include  "synthesizers/climate/synth-climate-vpd.h"
#include  "synthesizers/climate/synth-climate-windspeed.h"

/*!
 * @page climate_synthesizer
 * @section climate_synthesizer_order Order
 * Synthesizing climte follows an order reflecting dependencies among record items.
 * Topological sorting is non-trivial due to varying dependencies.
 * - Air pressure
 * - Precipitation
 * - Wind speed
 * - Extraterrestrial radiation
 * - Mean temperature
 * - Max temperature
 * - Min temperature
 * - Vapour pressure deficit
 * - Relative humidity
 * - Longwave radiation
 */
ldndc::climate::record::record_item_e const  ldndc::climate::synth::lsynth_climate_t::SYNTH_ORDER[ldndc::climate::record::RECORD_SIZE] =
{
    /* no dependencies */
    ldndc::climate::record::RECORD_ITEM_AIR_PRESSURE,
    ldndc::climate::record::RECORD_ITEM_PRECIP,
    ldndc::climate::record::RECORD_ITEM_WIND_SPEED,

    /* at least one dependency */
    /* NOTE
     *    extraterrestrial radiation calculated differently for
     *    measured or synthesized temperatures 
     */
    ldndc::climate::record::RECORD_ITEM_EX_RAD,
    ldndc::climate::record::RECORD_ITEM_TEMP_AVG,
    ldndc::climate::record::RECORD_ITEM_TEMP_MAX,
    ldndc::climate::record::RECORD_ITEM_TEMP_MIN,
    /* NOTE
     *    vapor pressure deficit calculated differently for measured
     *    or synthesized relative humidity
     */
    ldndc::climate::record::RECORD_ITEM_VPD,
    ldndc::climate::record::RECORD_ITEM_REL_HUMUDITY,
    ldndc::climate::record::RECORD_ITEM_LONGWAVE_RAD
};
/* NOTE order must match enumeration in climate types */
ldndc::climate::synth::item_base_t const *  ldndc::climate::synth::climate_synthesizers[ldndc::climate::record::RECORD_SIZE] =
{
    &ldndc::climate::synth::synth_climate_airpressure,
    &ldndc::climate::synth::synth_climate_exrad,
    &ldndc::climate::synth::synth_climate_lwrad,
    &ldndc::climate::synth::synth_climate_precip,
    &ldndc::climate::synth::synth_climate_tempavg,
    &ldndc::climate::synth::synth_climate_tempmax,
    &ldndc::climate::synth::synth_climate_tempmin,
    &ldndc::climate::synth::synth_climate_relhumidity,
    &ldndc::climate::synth::synth_climate_vpd,
    &ldndc::climate::synth::synth_climate_windspeed
};
ldndc::climate::synth::lsynth_climate_t::lsynth_climate_t()
        : ldndc::synth::streamdata_synth_base_t< streamdata_info_t >(),
          info_( climate_info_defaults)
{
}
ldndc::climate::synth::lsynth_climate_t::lsynth_climate_t(
        climate_info_t const *  _info,
        ltime_t const *  _time_in,
        ltime_t const *  _time_out)
        : ldndc::synth::streamdata_synth_base_t< streamdata_info_t >(),
          info_(( _info) ? (*_info) : climate_info_defaults)
{
    for ( int  i = 0;  i < record::RECORD_SIZE;  ++i)
    {
        record::record_item_e  r = static_cast< record::record_item_e >( i);
        if ( r != climate_synthesizers[i]->record_item_type())
        {
            CBM_LogFatal( "BUG");
        }
    }

// sk:??    if ( _time_out->from().subday() > 1)
// sk:??    {
// sk:??        CBM_LogFatal( "starting at non-day boundary not possible. sorry :-(");
// sk:??    }
    this->scratch_.initialize( _info, _time_in, _time_out);
}


ldndc::climate::synth::lsynth_climate_t::~lsynth_climate_t()
{
}


lerr_t
ldndc::climate::synth::lsynth_climate_t::configure(
        ldate_t const *  _date,
        unsigned int  _s)
{
    crabmeat_assert( _date);
    return  this->scratch_.configure( &this->info_, _date, (cbm::is_invalid( _s) ? ( static_cast< unsigned int >( _date->subday())) : _s));
}

#define  _HAVE_FIX_BUGGY_CLIMATE_DATA
#ifdef  _HAVE_FIX_BUGGY_CLIMATE_DATA
#include  "utils/lutils-swap.h"
#define  R(__i__)  ldndc::climate::record::RECORD_ITEM_##__i__
static void _fix_buggy_in( ldndc::climate::synth::record_item_t *  _rec)
{
    struct
    {
        bool  temp_min:1;
        bool  temp_max:1;
        bool  temp_avg:1;
        bool  precip:1;
    } v;

    v.temp_min = cbm::is_valid( _rec[R(TEMP_MIN)]) ? 1 : 0;
    v.temp_max = cbm::is_valid( _rec[R(TEMP_MAX)]) ? 1 : 0;
    v.temp_avg = cbm::is_valid( _rec[R(TEMP_AVG)]) ? 1 : 0;
    v.precip = cbm::is_valid( _rec[R(PRECIP)]) ? 1 : 0;


    if ( v.temp_min && v.temp_max)
    {
        if ( _rec[R(TEMP_MIN)] > _rec[R(TEMP_MAX)])
        {
            cbm::swap_values( _rec[R(TEMP_MIN)], _rec[R(TEMP_MAX)]);
        }
        if ( v.temp_avg && (( _rec[R(TEMP_MIN)] > _rec[R(TEMP_AVG)]) || ( _rec[R(TEMP_MAX)] < _rec[R(TEMP_AVG)])))
        {
            _rec[R(TEMP_AVG)] = 0.5*( _rec[R(TEMP_MIN)] + _rec[R(TEMP_MAX)]);
        }
    }
    else if ( v.temp_min && v.temp_avg)
    {
        if ( _rec[R(TEMP_MIN)] > _rec[R(TEMP_AVG)])
        {
            cbm::swap_values( _rec[R(TEMP_MIN)], _rec[R(TEMP_AVG)]);
        }
    }
    else if ( v.temp_max && v.temp_avg)
    {
        if ( _rec[R(TEMP_MAX)] < _rec[R(TEMP_AVG)])
        {
            cbm::swap_values( _rec[R(TEMP_MAX)], _rec[R(TEMP_AVG)]);
        }
    }
    if ( v.precip && ( _rec[R(PRECIP)] < 0.0))
    {
        _rec[R(PRECIP)] = 0.0;
    }
}
static void _fix_buggy_out( ldndc::climate::synth::record_item_t * /*_rec_s*/,
		ldndc::climate::synth::record_item_t * /*_rec_d*/ )
{
// example	if ( _rec_s[R(TEMP_MIN)] > _rec_d[R(TEMP_AVG)] )
// example	{
// example		_rec_s[R(TEMP_MIN)] = _rec_d[R(TEMP_MIN)];
// example	}
}
#undef  R
#endif  /*  _HAVE_FIX_BUGGY_CLIMATE_DATA  */

 /*
 * @brief
 *    extraterrestric radiation and daylength are calculated.
 *
 *    changed RG:
 *     - daily radiation is now estimated from solar radiation and temperature
 *         amplitude if the value is not read from file
 *
 *    TODO: estimate solar radiation in dependence on slope and aspect
 *                (see Allen et al. 2006)
 */
lerr_t
ldndc::climate::synth::lsynth_climate_t::synthesize_record(
        record_item_t const *  _c_rec_s_i, record_item_t *  _c_rec_s_o,
        record_item_t const *  _c_rec_d_i, record_item_t *  _c_rec_d_o,
        ldate_t const *  _date,
        lvector_t< unsigned int, 1 > *  _mod_cnt)
{
    crabmeat_assert( _c_rec_s_o || _c_rec_d_o);

    /* copy or clear temporary buffers */
    if ( _c_rec_s_i)
    {
        cbm::mem_cpy( this->scratch_.rec_s, _c_rec_s_i, SYNTH_CLIMATE_REC_SIZE);
    }
    else
    {
        cbm::mem_set( this->scratch_.rec_s, SYNTH_CLIMATE_REC_SIZE, ldndc::invalid_t< record_item_t >::value);
    }
    if ( _c_rec_d_i)
    {
        cbm::mem_cpy( this->scratch_.rec_d, _c_rec_d_i, SYNTH_CLIMATE_REC_SIZE);
    }
    else
    {
        cbm::mem_set( this->scratch_.rec_d, SYNTH_CLIMATE_REC_SIZE, ldndc::invalid_t< record_item_t >::value);
    }

// sk:off    /* sort synth order, available records items first, update at first subday timestep */
// sk:off    if ( this->scratch_.subday == 1)
// sk:off    {
        int  l = 0;
        for ( size_t  j = 0;  j < ldndc::climate::record::RECORD_SIZE;  ++j)
        {
// sk:off            if ( cbm::is_valid( this->scratch_.rec_d[SYNTH_ORDER[j]]))
            {
                this->synth_order_[l++] = SYNTH_ORDER[j];
            }
        }
// sk:off        int  k = l;
// sk:off        for ( size_t  j = 0;  j < ldndc::climate::record::RECORD_SIZE;  ++j)
// sk:off        {
// sk:off            if ( cbm::is_invalid( this->scratch_.rec_d[SYNTH_ORDER[j]]))
// sk:off            {
// sk:off                this->synth_order_[k++] = SYNTH_ORDER[j];
// sk:off            }
// sk:off        }
// sk:off    }

#ifdef  _HAVE_FIX_BUGGY_CLIMATE_DATA
    if ( _c_rec_s_i)
    {
        _fix_buggy_in( this->scratch_.rec_s);
    }
    if ( _c_rec_d_i)
    {
        _fix_buggy_in( this->scratch_.rec_d);
    }
#endif  /*  _HAVE_FIX_BUGGY_CLIMATE_DATA  */

    for ( size_t  j = 0;  j < SYNTH_CLIMATE_REC_SIZE;  ++j)
    {
        record::record_item_e  i = this->synth_order_[j];
        if ( ! climate_synthesizers[i])
        {
            CBM_LogError( "climate data item synthesizer may not be NULL  [index=", i,"]");
            return  LDNDC_ERR_FAIL;
        }

        /* shortcut for index */
        record::record_item_e const  I( i);

        /* synthesize subday item if output buffer was provided */
        record_item_t *  c_rec_s_o_I(( _c_rec_s_o && cbm::is_invalid( this->scratch_.rec_s[I])) ? (this->scratch_.rec_s + I) : NULL);
        /* synthesize day item if output buffer was provided */
        record_item_t *  c_rec_d_o_I(( _c_rec_d_o && cbm::is_invalid( this->scratch_.rec_d[I])) ? (this->scratch_.rec_d + I) : NULL);
        if ( c_rec_s_o_I || c_rec_d_o_I)
        {
            /* synthesize requested data item(s) */
            lerr_t  rc = climate_synthesizers[i]->synthesize(
                    c_rec_s_o_I, c_rec_d_o_I,
                    &this->scratch_,
                    _date, this->scratch_.subday,
                    &this->info_);

            if ( rc)
            {
                CBM_LogError( "error synthesizing climate record item  [item=", RECORD_ITEM_NAMES[I], "]");
                return  LDNDC_ERR_FAIL;
            }
            else if ( _mod_cnt)
            {
                (*_mod_cnt)[I] += 1;
            }
        }
    }

#ifdef  _HAVE_FIX_BUGGY_CLIMATE_DATA
	_fix_buggy_out( this->scratch_.rec_s, this->scratch_.rec_d );
#endif  /*  _HAVE_FIX_BUGGY_CLIMATE_DATA  */

    /* update record */
    if ( _c_rec_s_o)
    {
        cbm::mem_cpy( _c_rec_s_o, this->scratch_.rec_s, SYNTH_CLIMATE_REC_SIZE);
    }
    if ( _c_rec_d_o)
    {
        cbm::mem_cpy( _c_rec_d_o, this->scratch_.rec_d, SYNTH_CLIMATE_REC_SIZE);
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::climate::synth::lsynth_climate_t::check_record(
        record_item_t const *  /*_c_rec_s_i*/,
        record_item_t const *  /*_c_rec_d_i*/)
const
{
// sk:later    /**  SANITY CHECKS  */
// sk:later    if ( REC_I_SCRATCH_REG_(&this->scratch_,TEMP_MAX) < REC_I_SCRATCH_REG_(&this->scratch_,TEMP_MIN))
// sk:later    {
// sk:later        CBM_LogError( "minimum temperature higher than maximum temperature. [min=",REC_I_SCRATCH_REG_(&this->scratch_,TEMP_MIN),", max=",REC_I_SCRATCH_REG_(&this->scratch_,TEMP_MAX),"]");
// sk:later        return  LDNDC_ERR_FAIL;
// sk:later    }
// sk:later
// sk:later    if (( REC_I_SCRATCH_REG_(&this->scratch_,TEMP_AVG) < REC_I_SCRATCH_REG_(&this->scratch_,TEMP_MIN)) || ( REC_I_SCRATCH_REG_(&this->scratch_,TEMP_AVG) > REC_I_SCRATCH_REG_(&this->scratch_,TEMP_MAX)))
// sk:later    {
// sk:later        CBM_LogError( "average temperature outside min/max interval");
// sk:later        return  LDNDC_ERR_FAIL;
// sk:later    }

// sk:later    rel.humidity 0<=h<=100

    return  LDNDC_ERR_FAIL/*OK*/;
}



// sk:later lerr_t
// sk:later __LDNDC_IOCLIENT_INPUT_CLASS(climate)::t_avg_update_(
// sk:later         size_t  _records)
// sk:later const
// sk:later {
// sk:later // FIXME  must be scaled to year, consider year boundaries
// sk:later     if ( _records > 0 )
// sk:later     {
// sk:later         unsigned int  item_stride( buf_->get_record_alloc_size());
// sk:later 
// sk:later         double const  t_avg_sum( cbm::sum( buf_->get_data(), item_stride*_records, CL_RECORD_ITEM_TEMP_AVG, item_stride));
// sk:later         t_avg_ = (( load_status_->read_size() - _records)*t_avg_ + t_avg_sum) / load_status_->read_size();
// sk:later     }
// sk:later            else
// sk:later     {
// sk:later         CBM_LogWarn( "cannot calculate (update) average temperature from zero values");
// sk:later         t_avg_ = invalid_dbl;
// sk:later         return  LDNDC_ERR_FAIL;
// sk:later     }
// sk:later     return  LDNDC_ERR_OK;
// sk:later }
// sk:later 
// sk:later lerr_t
// sk:later __LDNDC_IOCLIENT_INPUT_CLASS(climate)::t_amp_update_(
// sk:later         size_t  _records)
// sk:later const
// sk:later {
// sk:later // FIXME  must be scaled to year, consider year boundaries
// sk:later     if ( _records > 0 )
// sk:later     {
// sk:later         /* shortcut to data array */
// sk:later         record_item_t const *  dat( buf_->get_data());
// sk:later 
// sk:later         /* special case */
// sk:later         if ( _records == 1)
// sk:later         {
// sk:later             t_amp_ = dat[CL_RECORD_ITEM_TEMP_MAX] - dat[CL_RECORD_ITEM_TEMP_MIN];
// sk:later             return  LDNDC_ERR_OK;
// sk:later         }
// sk:later 
// sk:later         if ( ! t_min_ /* && ! t_max_ (is implied) */)
// sk:later         {
// sk:later             /* find the lowest and highest elements of tavg (local copy t)
// sk:later              * we try this n times (max(n)=30), so we get the avg of the coldest
// sk:later              * n days and the warmest n days for our TAMP calculation
// sk:later              *
// sk:later              * note that _records does not grow on successive reloads
// sk:later              */
// sk:later             n_ = std::min( (size_t)(_records/2), (size_t)30);
// sk:later 
// sk:later             cbm::malloc_array( &t_min_, n_, std::numeric_limits< record_item_t >::max());
// sk:later             cbm::malloc_array( &t_max_, n_, std::numeric_limits< record_item_t >::min());
// sk:later 
// sk:later             t_min_max_ = 0; t_max_min_ = 0;
// sk:later         }
// sk:later 
// sk:later         for ( size_t  k = 0;  k < _records;  ++k)
// sk:later         {
// sk:later             size_t const  K(( k << climate_records_t::record_size_log) + CL_RECORD_ITEM_TEMP_AVG);
// sk:later 
// sk:later             if ( dat[K] > t_max_[t_max_min_])
// sk:later             {
// sk:later                 t_max_[t_max_min_] = dat[K];
// sk:later                 t_max_min_ = 0;
// sk:later                 for ( size_t  l = 1;  l < n_;  ++l)
// sk:later                 {
// sk:later                     if ( t_max_[l] < t_max_[t_max_min_])
// sk:later                     {
// sk:later                         t_max_min_ = l;
// sk:later                     }
// sk:later                 }
// sk:later             }
// sk:later 
// sk:later             if ( dat[K] < t_min_[t_min_max_])
// sk:later             {
// sk:later                 t_min_[t_min_max_] = dat[K];
// sk:later                 t_min_max_ = 0;
// sk:later                 for ( size_t  l = 1;  l < n_;  ++l)
// sk:later                 {
// sk:later                     if ( t_min_[l] > t_min_[t_min_max_])
// sk:later                     {
// sk:later                         t_min_max_ = l;
// sk:later                     }
// sk:later                 }
// sk:later             }
// sk:later         }
// sk:later 
// sk:later         t_amp_ = (1.0/ (double)n_) * ( cbm::sum( t_max_, n_) - cbm::sum( t_min_, n_));
// sk:later 
// sk:later         //LOGDEBUG3( "n", n_, "max", cbm::sum( t_max_, n_), "min", cbm::sum( t_min_, n_));
// sk:later     }
// sk:later     else
// sk:later     {
// sk:later         CBM_LogWarn( "cannot calculate (update) temperature amplitude from zero values");
// sk:later         t_amp_ = invalid_dbl;
// sk:later         return  LDNDC_ERR_FAIL;
// sk:later     }
// sk:later     return  LDNDC_ERR_OK;
// sk:later }
// sk:later 
// sk:later 
// sk:later lerr_t
// sk:later __LDNDC_IOCLIENT_INPUT_CLASS(climate)::p_sum_update_(
// sk:later         size_t  _records)
// sk:later const
// sk:later {
// sk:later     crabmeat_assert( ig_);
// sk:later 
// sk:later     if ( _records > 0)
// sk:later     {
// sk:later         /* convenience reference to simulation time */
// sk:later         ldate_t  sim_time;
// sk:later         if ( ig_->has_input( INPUT_SETUP))
// sk:later         {
// sk:later             sim_time = ig_->get_input_class< input_class_setup_t >()->get_time();
// sk:later         }
// sk:later         int const  time_res( ig_->get_input_class< __LDNDC_IOCLIENT_INPUT_CLASS(climate) >()->get_time( sim_time).time_resolution());
// sk:later 
// sk:later         unsigned int  item_stride( buf_->get_record_alloc_size());
// sk:later 
// sk:later         double const  p_sum_sum( cbm::sum( buf_->get_data(), item_stride*_records, CL_RECORD_ITEM_PRECIP, item_stride));
// sk:later         //LOGDEBUG4( "p_sum_", p_sum_, "p_sum_sum", p_sum_sum, "rec(last)", _records, "rec(full)", load_status_->read_size());
// sk:later         /* roughly.. lacking leap year support */
// sk:later         p_sum_ = ((( load_status_->read_size() - _records) / time_res) * p_sum_ + ( 365 * p_sum_sum)) / ( load_status_->read_size() / time_res);
// sk:later     }
// sk:later     else
// sk:later     {
// sk:later         CBM_LogWarn( "cannot calculate (update) annual precipitation from zero values");
// sk:later         p_sum_ = invalid_dbl;
// sk:later         return  LDNDC_ERR_FAIL;
// sk:later     }
// sk:later     return  LDNDC_ERR_OK;
// sk:later }

