/*!
 * @brief
 *    Temperature average synthesizer (implementation)
 *
 * @author
 *    Ruediger Grote,
 *    Steffen Klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-tempavg.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  "scientific/meteo/ld_meteo.h"

#include  "synthesizers/climate/synth-climate-tempmin.h"
#include  "synthesizers/climate/synth-climate-tempmax.h"

ldndc::climate::synth::temperature_average_t::~temperature_average_t()
{}

/*!
 * @page climate_synthesizer
 * @section climate_synthesizer_tavg Mean temperature
 * ...
 */
lerr_t
ldndc::climate::synth::temperature_average_t::synthesize(
        record_item_t *  _tempavg_s /* [oC] */,
        record_item_t *  _tempavg_d /* [oC] */,
        scratch_t *  _scratch,
        ldate_t const *  _date,
        unsigned int  _s,
        climate_info_t const *  _c_info)
const
{
    crabmeat_assert( _c_info);
    crabmeat_assert( _date);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(TEMP_AVG);

    if ( _tempavg_d)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET(tmin,TEMP_MIN);
        SYNTH_CLIMATE_REC_D_DECL_GET(tmax,TEMP_MAX);

        if ( cbm::is_invalid( tmin_d) || cbm::is_invalid( tmax_d))
        {
            /* have three or two data items missing */
            double  d( double( _date->julian_day()) + (( _c_info->latitude < 0.0) ? ((_date->julian_day() < 183u) ? 183.0 : -182.0) : 0.0));
            double const  day_fact = 1.0 - cos(( 2.0 * cbm::PI * d) / (double)_date->days_in_year());
            //LOGDEBUG3( "day-fact", day_fact, "d", d, "diy", _date->days_in_current_year());

            *_tempavg_d = _c_info->temp + 0.5 * _c_info->temp_amplitude * ( day_fact - 1.0);
        }
        else
        {
            /* Thornton et al. 1997 */
            *_tempavg_d = ldndc::meteo::thornton97( tmin_d, tmax_d);
        }
    }

    if ( _tempavg_s)
    {
// TODO move constants to scratch

        int  i = static_cast< int >( cbm::bound_min( 1.0, ceil( TSMIN * _scratch->day_fraction())));
        double  i_inv( 1.0 / (double)i);

        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);

        SYNTH_CLIMATE_REC_D_DECL_GET(tmin,TEMP_MIN);
        record_item_t  tmin_d_use( tmin_d);
        if ( cbm::is_invalid( tmin_d))
        {
            temperature_minimum_t  tmin_f;
            lerr_t  rc = tmin_f.synthesize( NULL, &tmin_d_use, _scratch, _date, _s, _c_info);
            if ( rc)
            {
                return  rc;
            }
        }
        SYNTH_CLIMATE_REC_D_DECL_GET(tmax,TEMP_MAX);
        record_item_t  tmax_d_use( tmax_d);
        if ( cbm::is_invalid( tmax_d))
        {
            temperature_maximum_t  tmax_f;
            lerr_t  rc = tmax_f.synthesize( NULL, &tmax_d_use, _scratch, _date, _s, _c_info);
            if ( rc)
            {
                return  rc;
            }
        }

        double const  AMP(( tmax_d_use - tmin_d_use) * 0.5);
        double const  RISE( 12.0 - ldndc::meteo::daylength( _c_info->latitude, static_cast< unsigned int >( _date->julian_day())) * 0.5);
        double const  NOON( 14.0);
        
        double const  DELTA_HR( _scratch->t_scale() * i_inv);
        double  hour_ts( _s * _scratch->t_scale());

        *_tempavg_s = 0.0;

          while ( i--)
        {
            // hour of current timestep
            hour_ts += DELTA_HR;

            // - instantaneous temperature
            //   after De Wit 1978, described in Berninger 1994
            double  C[4] = { AMP, -14.0, 10.0, RISE};
            if (hour_ts < RISE)
            {
                  C[1] = 10.0;
              }
              else if (hour_ts <= NOON)
              {
                  C[0] = -C[0]; C[1] = -RISE; C[2] = 14.0; C[3] = -C[3];
              }
              else
              {
                  // no op
              }
  
            *_tempavg_s += i_inv * ( tavg_d + C[0] * cos( cbm::PI * ( hour_ts + C[1]) / ( C[2] + C[3])));
        }
    }


    SYNTH_CLIMATE_RETURN_SUCCESS(TEMP_AVG);
}


ldndc::climate::synth::temperature_average_t const  ldndc::climate::synth::synth_climate_tempavg;

