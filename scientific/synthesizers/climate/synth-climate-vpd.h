/*!
 * @brief
 *    vapor pressure deficit synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_VPD_H_
#define  LDNDC_DATASYNTH_CLIMATE_VPD_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  vapor_pressure_deficit_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(vapor_pressure_deficit_t,VPD)
    };

    extern  vapor_pressure_deficit_t CBM_API const  synth_climate_vpd;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_VPD_H_  */

