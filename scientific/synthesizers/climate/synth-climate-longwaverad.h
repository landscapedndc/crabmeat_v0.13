/*!
 * @brief
 *    long wave radiation synthesizer
 *
 * @author
 *      ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_LONGWAVERAD_H_
#define  LDNDC_DATASYNTH_CLIMATE_LONGWAVERAD_H_

#include  "synthesizers/climate/synth-climate.h"

class  ldate_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    struct  CBM_API  longwave_radiation_t  :  public  item_base_t
    {
        SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(longwave_radiation_t,LONGWAVE_RAD)
    };

    extern  longwave_radiation_t CBM_API const  synth_climate_lwrad;
}}}

#endif  /*  !LDNDC_DATASYNTH_CLIMATE_LONGWAVERAD_H_  */

