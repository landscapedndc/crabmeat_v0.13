/*!
 * @brief
 *    Climate data synthesizer
 *
 * @author
 *    Steffen Klatt (created on: jan 19, 2013)
 */

#ifndef  LDNDC_DATASYNTH_CLIMATE_H_
#define  LDNDC_DATASYNTH_CLIMATE_H_

#include  "synthesizers/synth-base-streamdata.h"
#include  "climate/climatetypes.h"
#include  "time/cbm_date.h"

#include  "math/cbm_math.h"

#define  SYNTH_CLIMATE_REC_SIZE  SYNTH_REC_SIZE_(climate)

#define  SYNTH_CLIMATE_REC_S_DECL_GET(__reg__,__item__)  SYNTH_REC_DECL_GET_(climate,__reg__,s,__item__)
#define  SYNTH_CLIMATE_REC_D_DECL_GET(__reg__,__item__)  SYNTH_REC_DECL_GET_(climate,__reg__,d,__item__)

#define  SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(__reg__,__item__)  SYNTH_REC_DECL_GET_AND_CHK_VALID_(climate,__reg__,s,__item__)
#define  SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(__reg__,__item__)  SYNTH_REC_DECL_GET_AND_CHK_VALID_(climate,__reg__,d,__item__)

#define  SYNTH_CLIMATE_INVALIDATE_BUFFER(__buf__)  SYNTH_INVALIDATE_BUFFER_(climate,__buf__)

#define  SYNTH_CLIMATE_CHK_MATCHING(__item__)  SYNTH_CHK_MATCHING_(climate,__item__)

#define  SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(__item__)  SYNTH_CALL_LOOP_TEST_AND_SET_(climate,__item__)
#define  SYNTH_CLIMATE_RETURN_SUCCESS(__item__)  SYNTH_RETURN_SUCCESS_(climate,__item__)

#define  SYNTH_CLIMATE_RECORD_ITEM_COMMON_DECL(__class__,__item_enum__)  SYNTH_RECORD_ITEM_COMMON_DECL_(climate,__class__,__item_enum__)


/* ??? */
#define  TSMIN                    (24.0)
/* maximum overshoot pool */
#define  PRECIP_OVERSHOOT_MAX            (70.0)
/* stretching factor */
#define  PRECIP_STRETCHING_FACTOR        (150.0)
/* reduction rate for overshoot decrease calculation */
#define  PRECIP_OVERSHOOT_REDUCTION_RATE    (0.3)
/* probability of precipitation streching */
#define  PRECIP_STRETCHING_PROBABILITY        (0.9)


template < typename, unsigned int >
class  lvector_t;

namespace  ldndc{ namespace  climate{ namespace  synth
{
    typedef  record::item_type  record_item_t;
    struct  CBM_API  scratch_t  :  public  ldndc::synth::streamdata_scratch_base_t< streamdata_info_t >
    {
        /* shifted according to latitude */
        unsigned int  julian_day;

        cbm::random::rng_uniform_t  precip_rng;

        record_item_t  precip_generated;
        record_item_t  precip_overshoot;

        record_item_t  precip_d;
        record_item_t  precip_exp;
        record_item_t  precip_value_fact;

        double  fp_base(
                double  /* latitude */) const;

        double  precip_value_fact_fun(
                double  /* precipitation sum */,
                unsigned int  /* days in year */) const;

        lerr_t  initialize(
                climate_info_t const *  /* climate meta info */,
                ltime_t const *  /* input date */,
                ltime_t const *  /* output date */);

        lerr_t  configure(
                climate_info_t const *  /* climate meta info */,
                ldate_t const *  /* current date */,
                unsigned int  /* current target subday */);
    };


    struct  CBM_API  item_base_t  :  public  ldndc::synth::streamdata_item_base_t< streamdata_info_t, scratch_t >
    {
    };


    class  CBM_API  lsynth_climate_t  :  public  ldndc::synth::streamdata_synth_base_t< streamdata_info_t >
    {
        public:
            lsynth_climate_t();
            lsynth_climate_t(
                    climate_info_t const *  /* climate meta info */,
                    ltime_t const *  /* input date */,
                    ltime_t const *  /* output date */);


            virtual  ~lsynth_climate_t();

            /*!
             * @brief
             *    before synthesizing, update internal structures
             */
            lerr_t  configure(
                    ldate_t const *  /* target date */,
                    unsigned int = ldndc::invalid_t< unsigned int >::value /* target subday */);

            /*!
             * @brief
             *    synthesize values for invalid record items
             */
            lerr_t  synthesize_record(
                    record_item_t const * /* subday data buffer (input) */, record_item_t * /* subday data buffer (output) */,
                    record_item_t const * /* day data buffer (input) */, record_item_t * /* day data buffer (output) */,

                    ldate_t const *  /* current date */,

                    lvector_t< unsigned int, 1 > * = NULL  /* modification count */);

            /*!
             * @brief
             *    check record for plausible values
             *
             * @todo
             *    use bitset?
             */
            lerr_t  check_record(
                    record_item_t const * /* subday data buffer (input) */,
                    record_item_t const * /* day data buffer (input) */) const;

            inline
            unsigned int  subday()
            const
            {
                return  this->scratch_.subday;
            }

            inline
            unsigned int  time_resolution_inout()
            const
            {
                return  this->scratch_.t_res_inout;
            }

        private:
            climate_info_t  info_;
            scratch_t  scratch_;

            /* default synthesizer execution order */
            static  record::record_item_e const  SYNTH_ORDER[record::RECORD_SIZE];
            /* (reordered) synthesizer execution order array for day timestep */
            record::record_item_e  synth_order_[record::RECORD_SIZE];
    };
    extern  item_base_t const *  climate_synthesizers[record::RECORD_SIZE];
}}}


#endif  /*  !LDNDC_DATASYNTH_CLIMATE_H_  */

