/*!
 * @brief
 *    minimum temperature data synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-tempmin.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::climate::synth::temperature_minimum_t::~temperature_minimum_t()
{
}

lerr_t
ldndc::climate::synth::temperature_minimum_t::synthesize(
        record_item_t *  _tempmin_s /* [oC] */,
        record_item_t *  _tempmin_d /* [oC] */,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        climate_info_t const *)
const
{
    crabmeat_assert( _scratch);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(TEMP_MIN);

    if ( _tempmin_d)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);

        SYNTH_CLIMATE_REC_D_DECL_GET(tmax,TEMP_MAX);
        if ( cbm::is_invalid( tmax_d))
        {
            *_tempmin_d = tavg_d * (( tavg_d > 0.0) ? 0.6 : 1.4);
        }
        else
        {
            *_tempmin_d = 2.0 * tavg_d - tmax_d;
        }
    }

    if ( _tempmin_s)
    {
        SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);

        /* choose to use updated one if available */
        record_item_t  tmin_d_use;
        if ( _tempmin_d)
        {
            tmin_d_use = *_tempmin_d;
        }
        else
        {
            SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tmin,TEMP_MIN);
            tmin_d_use = tmin_d;
        }

        // +/-40% is the default for the daily amplitude
        // scale the amplitude depeding on time resolution of tavg_s
        // hourly temperature is assumed to be sufficient stable and taken directly as minimum considering only a tiny offset
        *_tempmin_s = cbm::bound_min( tmin_d_use,
                                      tavg_s * (( tavg_s > 0.0) ? 0.99 - 0.4 * (1.0 - _scratch->tgt_res / 24.0) :
                                                                  1.01 + 0.4 * (1.0 - _scratch->tgt_res / 24.0)));
    }


    SYNTH_CLIMATE_RETURN_SUCCESS(TEMP_MIN);
}


ldndc::climate::synth::temperature_minimum_t const  ldndc::climate::synth::synth_climate_tempmin;

