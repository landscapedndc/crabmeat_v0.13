/*!
 * @brief
 *    precipitation synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-precip.h"

#include  "time/cbm_date.h"
#include  "constants/cbm_const.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"


ldndc::climate::synth::precipitation_t::~precipitation_t()
{
}


lerr_t
ldndc::climate::synth::precipitation_t::synthesize(
        record_item_t *  _precip_s /* [mm] */,
        record_item_t *  _precip_d /* [mm] */,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        climate_info_t const *  _c_info)
const
{
    crabmeat_assert( _scratch);
    crabmeat_assert( _c_info);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(PRECIP);

    if ( _precip_d)
    {
        double const  precip_jitter =
            _scratch->precip_rng.frand() - _scratch->precip_exp;
        double const  random_stretch_limit =
            _scratch->precip_rng.frand();

        if (( precip_jitter > 0.0)
             && ( _scratch->precip_generated < _c_info->precip_sum))
        {
            // generation of precipitation between zero and double of average
            *_precip_d = precip_jitter * _scratch->precip_value_fact;

            // decrease of precipitation overshoot
            if ( _scratch->precip_overshoot > 0.0)
            {
                _scratch->precip_overshoot -=
                    ( *_precip_d * PRECIP_OVERSHOOT_REDUCTION_RATE);
                *_precip_d *= ( 1.0 - PRECIP_OVERSHOOT_REDUCTION_RATE);

                if ( _scratch->precip_overshoot < 0.0)
                {
                    *_precip_d -= _scratch->precip_overshoot;
                    _scratch->precip_overshoot = 0.0;
                }
            }

            double const  stretch_limit =
                ( _scratch->precip_overshoot > PRECIP_OVERSHOOT_MAX) ? 1.0 :
                ( 1.0 - PRECIP_STRETCHING_PROBABILITY * ( 1.0 - ( _scratch->precip_overshoot / PRECIP_OVERSHOOT_MAX)));

            /* precipitation stretching */
            if ( random_stretch_limit > stretch_limit)
            {
                // constant ...
                double const  stretch = PRECIP_STRETCHING_FACTOR / _scratch->fp_base( _c_info->latitude);
                if ( stretch < 1.0)
                {
                    *_precip_d = 0.0;
                }
                else
                {
                    _scratch->precip_overshoot += ( *_precip_d * ( stretch - 1.0));
                    *_precip_d *= stretch;
                }
            }

            _scratch->precip_generated += *_precip_d;
        }
        else
        {
            *_precip_d = 0.0;
        }
    }

    if ( _precip_s)
    {
        if ( cbm::is_invalid( _scratch->precip_d))
        {
            if ( _precip_d)
            {
                /* keep for subday precipitation */
                _scratch->precip_d = *_precip_d;
            }
            else
            {
                SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(precip,PRECIP);
                _scratch->precip_d = precip_d;
            }
        }

        /* instantaneous precipitation */
          if ( _scratch->precip_d > ( _c_info->rainfall_intensity * cbm::HR_IN_DAY))
          {
              *_precip_s = _scratch->precip_d * _scratch->t_scale() * _scratch->day_fraction();
              _scratch->precip_d = cbm::bound_min( 0.0, _scratch->precip_d - *_precip_s);
          }
          else if ( _scratch->precip_d > ( _c_info->rainfall_intensity * _scratch->t_scale()))
          {
              *_precip_s = _c_info->rainfall_intensity * _scratch->t_scale();
              _scratch->precip_d = cbm::bound_min( 0.0, _scratch->precip_d - *_precip_s);
          }
          else
          {
            *_precip_s = _scratch->precip_d;
              _scratch->precip_d = 0.0;
          }
    }


    SYNTH_CLIMATE_RETURN_SUCCESS(PRECIP);
}


ldndc::climate::synth::precipitation_t const  ldndc::climate::synth::synth_climate_precip;

