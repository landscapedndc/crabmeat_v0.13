/*!
 * @brief
 *    Vapor pressure deficit synthesizer (implementation)
 *
 * @author
 *    Ruediger Grote,
 *    Steffen Klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-vpd.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  "scientific/meteo/ld_meteo.h"

ldndc::climate::synth::vapor_pressure_deficit_t::~vapor_pressure_deficit_t()
{}

/*!
 * @page climate_synthesizer
 * @section climate_synthesizer_vpd Vapor pressure deficit
 * \f[
 * vps := f(T)
 * \f]
 * If relative humidity is given:
 * \f[
 * vpd = vps \left(1 - \frac{rh}{100} \right)
 * \f]
 * If relative humidity is not given, the following assumptions are made:
 * - Saturated conditions at \f$ T_{min} \f$ (i.e., condensation at night time).
 * - Constant amount of water during the day
 * \f{eqnarray*}{
 * vps_{min} := f(T_{min}) \\
 * vpd = vps - vps_{min}
 * \f}
 * If daily vpd is given, subdaily vpd is derived by:
 * \f{eqnarray*}{
 * vps_{daily} := f(T_{daily}) \\
 * vps_{subdaily} := f(T_{subdaily}) \\
 * vp_{daily} = vps_{daily} - vpd_{daily} \\
 * vpd_{subdaily} = vps_{subdaily} - vp_{daily}
 * \f}
 */
lerr_t
ldndc::climate::synth::vapor_pressure_deficit_t::synthesize(
        record_item_t *  _vpd_s /* [kPa] */,
        record_item_t *  _vpd_d /* [kPa] */,
        scratch_t *  _scratch,
        ldate_t const *,
        unsigned int,
        climate_info_t const *)
const
{
    crabmeat_assert( _scratch);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(VPD);

    if ( _vpd_d)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
        double  vps;
        ldndc::meteo::vps( t_avg_d, &vps);

        SYNTH_CLIMATE_REC_D_DECL_GET(rel_humidity,REL_HUMUDITY);
        if ( cbm::is_invalid( rel_humidity_d))
        {
            SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_min,TEMP_MIN);
            if ( cbm::flt_equal( t_min_d, t_avg_d))
            {
                *_vpd_d = 0.0;
            }
            else
            {
                double  vps_min;
                ldndc::meteo::vps( t_min_d, &vps_min);
                *_vpd_d = cbm::bound_min( 0.0, vps - vps_min);
            }
        }
        else
        {
            *_vpd_d = cbm::bound_min( 0.0, vps * ( 1.0 - ( rel_humidity_d / 100.0)));
        }
    }

    if ( _vpd_s)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET(vpd,VPD);
        if ( cbm::is_valid( vpd_d))
        {
            SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
            double  vps_d;
            ldndc::meteo::vps( t_avg_d, &vps_d);

            SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
            double  vps_s;
            ldndc::meteo::vps( t_avg_s, &vps_s);
            
            double vp_d = vps_d - vpd_d;    //vapour pressure from daily input

            *_vpd_s = vps_s - vp_d; //subdaily vpd calculated from subdaily saturated vapour pressure - actual vapour pressure as given by daily input
        }
        else
        {
            SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(t_avg,TEMP_AVG);
            double  vps;
            ldndc::meteo::vps( t_avg_s, &vps);
            
            SYNTH_CLIMATE_REC_S_DECL_GET(rel_humidity,REL_HUMUDITY);
            if ( cbm::is_invalid( rel_humidity_s))
            {
                SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(t_min,TEMP_MIN);
                if ( cbm::flt_equal( t_min_d, t_avg_s))
                {
                    *_vpd_s = 0.0;
                }
                else
                {
                    double  vps_min;
                    ldndc::meteo::vps( t_min_d, &vps_min);
                    *_vpd_s = cbm::bound_min( 0.0, vps - vps_min);
                }
            }
            else
            {
                *_vpd_s = cbm::bound_min( 0.0, vps * ( 1.0 - ( rel_humidity_s / 100.0)));
            }
        }
    }


    SYNTH_CLIMATE_RETURN_SUCCESS(VPD);
}


ldndc::climate::synth::vapor_pressure_deficit_t const  ldndc::climate::synth::synth_climate_vpd;

