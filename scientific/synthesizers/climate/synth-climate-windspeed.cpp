/*!
 * @brief
 *    wind speed synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-windspeed.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"


ldndc::climate::synth::windspeed_t::~windspeed_t()
{
}


lerr_t
ldndc::climate::synth::windspeed_t::synthesize(
        record_item_t *  _windspeed_s /* [ms-1] */,
        record_item_t *  _windspeed_d /* [ms-1] */,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        climate_info_t const *  _c_info)
const
{
    crabmeat_assert( _scratch);
    crabmeat_assert( _c_info);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(WIND_SPEED);

    if ( _windspeed_d)
    {
        *_windspeed_d = _c_info->windspeed;
    }

// sk:mv  to other function
// sk:mv    if ( w_speed < 0.1)
// sk:mv    {
// sk:mv        w_speed = 0.1;
// sk:mv    }

    if ( _windspeed_s)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET(windspeed,WIND_SPEED);
        if ( cbm::is_valid( windspeed_d))
        {
            *_windspeed_s = windspeed_d;
        }
        else
        {
            *_windspeed_s = _c_info->windspeed;
        }
    }


    SYNTH_CLIMATE_RETURN_SUCCESS(WIND_SPEED);
}


ldndc::climate::synth::windspeed_t const  ldndc::climate::synth::synth_climate_windspeed;

