/*!
 * @brief
 *    temperature maximum synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: jan 19, 2013)
 */

#include  "synthesizers/climate/synth-climate-tempmax.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"


ldndc::climate::synth::temperature_maximum_t::~temperature_maximum_t()
{
}


lerr_t
ldndc::climate::synth::temperature_maximum_t::synthesize(
        record_item_t *  _tempmax_s /* [oC] */,
        record_item_t *  _tempmax_d /* [oC] */,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        climate_info_t const *)
const
{
    crabmeat_assert( _scratch);

    SYNTH_CLIMATE_CALL_LOOP_TEST_AND_SET(TEMP_MAX);

    if ( _tempmax_d)
    {
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);

        SYNTH_CLIMATE_REC_D_DECL_GET(tmin,TEMP_MIN);
        if ( cbm::is_invalid( tmin_d))
        {
            *_tempmax_d = tavg_d * (( tavg_d > 0.0) ? 1.4 : 0.6);
        }
        else
        {
            *_tempmax_d = 2.0 * tavg_d - tmin_d;
        }
    }

    if ( _tempmax_s)
    {
        SYNTH_CLIMATE_REC_S_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);
        SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tavg,TEMP_AVG);

        /* choose to use updated one if available */
        record_item_t  tmax_d_use;
        if ( _tempmax_d)
        {
            tmax_d_use = *_tempmax_d;
        }
        else
        {
            SYNTH_CLIMATE_REC_D_DECL_GET_AND_CHK_VALID(tmax,TEMP_MAX);
            tmax_d_use = tmax_d;
        }

        // +/-40% is the default for the daily amplitude
        // scale the amplitude depeding on time resolution of tavg_s
        // hourly temperature is assumed to be sufficient stable and taken directly as maximum considering only a tiny offset
        *_tempmax_s = cbm::bound_max( tavg_s * (( tavg_s > 0.0) ? 1.01 + 0.4 * (1.0 - _scratch->tgt_res / 24.0) :
                                                                  0.99 - 0.4 * (1.0 - _scratch->tgt_res / 24.0)),
                                      tmax_d_use);
    }


    SYNTH_CLIMATE_RETURN_SUCCESS(TEMP_MAX);
}

ldndc::climate::synth::temperature_maximum_t const  ldndc::climate::synth::synth_climate_tempmax;

