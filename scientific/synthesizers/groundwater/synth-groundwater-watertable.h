/*!
 * @brief
 *    watertable synthesizer
 *
 * @author
 *    steffen klatt (created on: oct 19  2015)
 */

#ifndef  LDNDC_DATASYNTH_GROUNDWATER_WATERTABLE_H_
#define  LDNDC_DATASYNTH_GROUNDWATER_WATERTABLE_H_

#include  "synthesizers/groundwater/synth-groundwater.h"

class  ldate_t;

namespace  ldndc{ namespace  groundwater{ namespace  synth
{
    struct  CBM_API  watertable_t  :  public  ldndc::groundwater::synth::item_base_t
    {
        SYNTH_GROUNDWATER_RECORD_ITEM_COMMON_DECL(watertable_t,WATERTABLE)
    };

    extern  watertable_t CBM_API const  synth_groundwater_watertable;
}}}

#endif  /*  !LDNDC_DATASYNTH_GROUNDWATER_WATERTABLE_H_  */

