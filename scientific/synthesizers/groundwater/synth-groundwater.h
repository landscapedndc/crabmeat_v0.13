/*!
 * @brief
 *    groundwater data synthesizer
 *
 * @author
 *    steffen klatt (created on: oct 19  2015)
 */

#ifndef  LDNDC_DATASYNTH_GROUNDWATER_H_
#define  LDNDC_DATASYNTH_GROUNDWATER_H_

#include  "synthesizers/synth-base-streamdata.h"
#include  "groundwater/groundwatertypes.h"
#include  "time/cbm_date.h"


#define  SYNTH_GROUNDWATER_REC_SIZE  SYNTH_REC_SIZE_(groundwater)

#define  SYNTH_GROUNDWATER_REC_S_DECL_GET(__reg__,__item__)  SYNTH_REC_DECL_GET_(groundwater,__reg__,s,__item__)
#define  SYNTH_GROUNDWATER_REC_D_DECL_GET(__reg__,__item__)  SYNTH_REC_DECL_GET_(groundwater,__reg__,d,__item__)

#define  SYNTH_GROUNDWATER_REC_S_DECL_GET_AND_CHK_VALID(__reg__,__item__)  SYNTH_REC_DECL_GET_AND_CHK_VALID_(groundwater,__reg__,s,__item__)
#define  SYNTH_GROUNDWATER_REC_D_DECL_GET_AND_CHK_VALID(__reg__,__item__)  SYNTH_REC_DECL_GET_AND_CHK_VALID_(groundwater,__reg__,d,__item__)

#define  SYNTH_GROUNDWATER_INVALIDATE_BUFFER(__buf__)  SYNTH_INVALIDATE_BUFFER_(groundwater,__buf__)

#define  SYNTH_GROUNDWATER_CHK_MATCHING(__item__)  SYNTH_CHK_MATCHING_(groundwater,__item__)

#define  SYNTH_GROUNDWATER_CALL_LOOP_TEST_AND_SET(__item__)  SYNTH_CALL_LOOP_TEST_AND_SET_(groundwater,__item__)
#define  SYNTH_GROUNDWATER_RETURN_SUCCESS(__item__)  SYNTH_RETURN_SUCCESS_(groundwater,__item__)

#define  SYNTH_GROUNDWATER_RECORD_ITEM_COMMON_DECL(__class__,__item_enum__)  SYNTH_RECORD_ITEM_COMMON_DECL_(groundwater,__class__,__item_enum__)


template < typename, unsigned int >
class  lvector_t;

namespace  ldndc{ namespace  groundwater{ namespace  synth
{
typedef  record::item_type  record_item_t;
struct  scratch_t  :  public  ldndc::synth::streamdata_scratch_base_t< streamdata_info_t >
{
    lerr_t  initialize(
            groundwater_info_t const *  /* boundary info */,
            ltime_t const *  /* input date */,
            ltime_t const *  /* output date */);

    lerr_t  configure(
            groundwater_info_t const *  /* boundary info */,
            ldate_t const *  /* current date */,
            unsigned int  /* current target subday */);
};

struct  CBM_API  item_base_t  :  public  ldndc::synth::streamdata_item_base_t< streamdata_info_t, scratch_t >
{
};


class  CBM_API  lsynth_groundwater_t  :  public  ldndc::synth::streamdata_synth_base_t< streamdata_info_t >
{
    public:
        lsynth_groundwater_t();
        lsynth_groundwater_t(
                groundwater_info_t const *  /* groundwater meta info */,
                ltime_t const *  /* input date */,
                ltime_t const *  /* output date */);

        virtual  ~lsynth_groundwater_t();

        /*!
         * @brief
         *    before synthesizing, update internal structures
         */
        lerr_t  configure(
                ldate_t const *  /* target date */,
                unsigned int = ldndc::invalid_t< unsigned int >::value /* target subday */);

        /*!
         * @brief
         *    synthesize values for invalid record items
         */
        lerr_t  synthesize_record(
                record_item_t const * /* subday data buffer (input) */, record_item_t * /* subday data buffer (output) */,
                record_item_t const * /* day data buffer (input) */, record_item_t * /* day data buffer (output) */,

                ldate_t const *  /* current date */,

                lvector_t< unsigned int, 1 > * = NULL  /* modification count */);

        /*!
         * @brief
         *    check record for plausible values
         *
         * @todo
         *    use bitset?
         */
        lerr_t  check_record(
                record_item_t const * /* subday data buffer (input) */,
                record_item_t const * /* day data buffer (input) */) const;

        inline
        unsigned int  subday()
        const
        {
            return  this->scratch_.subday;
        }

        inline
        unsigned int  time_resolution_inout()
        const
        {
            return  this->scratch_.t_res_inout;
        }

    private:
        groundwater_info_t  info_;
        scratch_t  scratch_;
};
extern  item_base_t const *  groundwater_synthesizers[record::RECORD_SIZE];
}  /*  namespace synth  */
}  /*  namespace groundwater  */
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_DATASYNTH_GROUNDWATER_H_  */

