/*!
 * @brief
 *    watertable synthesizer (implementation)
 *
 * @author
 *    ruediger grote,
 *      steffen klatt (created on: aug 28, 2013)
 */

#include  "synthesizers/groundwater/synth-groundwater-watertable.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::groundwater::synth::watertable_t::~watertable_t()
{
}


lerr_t
ldndc::groundwater::synth::watertable_t::synthesize(
        record_item_t *  _watertable_s,
        record_item_t *  _watertable_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        groundwater_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check groundwater boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_GROUNDWATER_CALL_LOOP_TEST_AND_SET(WATERTABLE);

    if ( _watertable_d)
    {
        *_watertable_d = _info->watertable;
    }
    if ( _watertable_s)
    {
        SYNTH_GROUNDWATER_REC_D_DECL_GET(watertable,WATERTABLE);
        if ( cbm::is_valid( watertable_d))
        {
            *_watertable_s = watertable_d;
        }
        else
        {
            *_watertable_s = _info->watertable;
        }
    }


    SYNTH_GROUNDWATER_RETURN_SUCCESS(WATERTABLE);
}


ldndc::groundwater::synth::watertable_t const  ldndc::groundwater::synth::synth_groundwater_watertable;

