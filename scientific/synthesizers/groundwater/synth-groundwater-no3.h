/*!
 * @brief
 *    no3 synthesizer
 *
 * @author
 *    steffen klatt (created on: oct 19  2015)
 */

#ifndef  LDNDC_DATASYNTH_GROUNDWATER_NO3_H_
#define  LDNDC_DATASYNTH_GROUNDWATER_NO3_H_

#include  "synthesizers/groundwater/synth-groundwater.h"

class  ldate_t;

namespace  ldndc{ namespace  groundwater{ namespace  synth
{
    struct  CBM_API  no3_t  :  public  ldndc::groundwater::synth::item_base_t
    {
        SYNTH_GROUNDWATER_RECORD_ITEM_COMMON_DECL(no3_t,NO3)
    };

    extern  no3_t CBM_API const  synth_groundwater_no3;
}}}

#endif  /*  !LDNDC_DATASYNTH_GROUNDWATER_NO3_H_  */

