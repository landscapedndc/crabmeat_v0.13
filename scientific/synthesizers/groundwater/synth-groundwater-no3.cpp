/*!
 * @brief
 *    no3 synthesizer (implementation)
 *
 * @author
 *    steffen klatt (created on: oct 19  2015)
 */

#include  "synthesizers/groundwater/synth-groundwater-no3.h"

#include  "time/cbm_date.h"

#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

ldndc::groundwater::synth::no3_t::~no3_t()
{
}


lerr_t
ldndc::groundwater::synth::no3_t::synthesize(
        record_item_t *  _no3_s,
        record_item_t *  _no3_d,

        scratch_t *  _scratch,

        ldate_t const *,
        unsigned int,

        groundwater_info_t const *  _info)
const
{
    if ( !_info || !_scratch)
    {
        CBM_LogError( "missing inputs. check groundwater boundary information and scratch objects");
        return  LDNDC_ERR_FAIL;
    }

    SYNTH_GROUNDWATER_CALL_LOOP_TEST_AND_SET(NO3);

    if ( _no3_d)
    {
        *_no3_d = _info->no3;
    }
    if ( _no3_s)
    {
        SYNTH_GROUNDWATER_REC_D_DECL_GET(no3,NO3);
        if ( cbm::is_valid( no3_d))
        {
            *_no3_s = no3_d;
        }
        else
        {
            *_no3_s = _info->no3;
        }
    }


    SYNTH_GROUNDWATER_RETURN_SUCCESS(NO3);
}


ldndc::groundwater::synth::no3_t const  ldndc::groundwater::synth::synth_groundwater_no3;

