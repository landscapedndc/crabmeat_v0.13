/*!
 * @brief
 *    groundwater data synthesizer (implementation)
 *
 * @author
 *    steffen klatt (created on: oct 19  2015)
 */

#include  "synthesizers/groundwater/synth-groundwater.h"
#include  "containers/cbm_vector.h"
#include  "log/cbm_baselog.h"

lerr_t
ldndc::groundwater::synth::scratch_t::initialize(
        groundwater_info_t const *  /*_info*/,
        ltime_t    const *  _time_in,
        ltime_t    const *  _time_out)
{
    ldate_t const  time_in( _time_in->from());

    this->src_res = _time_in->time_resolution();
    this->tgt_res = _time_out->time_resolution();
    this->t_res_inout = this->tgt_res / this->src_res;
// sk:dbg    CBM_LogDebug( "t-res-src=",this->src_res, ", t-res-tgt=",this->tgt_res, ",  t-res-inout=",this->t_res_inout);
    this->subday = 1;

    /**  shifted julian day  **/
    this->julian_day = static_cast< unsigned int >( time_in.julian_day());

    /* reset call board */
    call_mask = 0;

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::groundwater::synth::scratch_t::configure(
        groundwater_info_t const *  /*_info*/,
        ldate_t const *  _date,
        unsigned int  _s)
{
// sk:unused    crabmeat_assert( _info);
    crabmeat_assert( _date);
    this->subday = _s;

    /* set julian day */
    this->julian_day = static_cast< unsigned int >( _date->julian_day());

    /* reset call board */
    call_mask = 0;

    return  LDNDC_ERR_OK;
}

#include  "synthesizers/groundwater/synth-groundwater-no3.h"
#include  "synthesizers/groundwater/synth-groundwater-watertable.h"


/* NOTE order must match enumeration in groundwater types */
ldndc::groundwater::synth::item_base_t const *  ldndc::groundwater::synth::groundwater_synthesizers[ldndc::groundwater::record::RECORD_SIZE] =
{
    &ldndc::groundwater::synth::synth_groundwater_watertable,
    &ldndc::groundwater::synth::synth_groundwater_no3,
    NULL,
    NULL,
    NULL
};
ldndc::groundwater::synth::lsynth_groundwater_t::lsynth_groundwater_t()
        : ldndc::synth::streamdata_synth_base_t< streamdata_info_t >(),
          info_( groundwater_info_defaults)
{
}
ldndc::groundwater::synth::lsynth_groundwater_t::lsynth_groundwater_t(
        groundwater_info_t const *  _info,
        ltime_t const *  _time_in,
        ltime_t const *  _time_out)
        : ldndc::synth::streamdata_synth_base_t< streamdata_info_t >(),
          info_(( _info) ? (*_info) : groundwater_info_defaults)
{
    for ( int  i = 0;  i < record::RECORD_SIZE;  ++i)
    {
        record::record_item_e  r = static_cast< record::record_item_e >( i);
        if ( groundwater_synthesizers[i]
            && ( r != groundwater_synthesizers[i]->record_item_type()))
        {
            CBM_LogFatal( "BUG [",i,"]");
        }
    }

// sk:??    if ( _time_out->from().subday() > 1)
// sk:??    {
// sk:??        CBM_LogFatal( "starting at non-day boundary not possible. sorry :-(");
// sk:??    }
    this->scratch_.initialize( _info, _time_in, _time_out);
}


ldndc::groundwater::synth::lsynth_groundwater_t::~lsynth_groundwater_t()
{
}


lerr_t
ldndc::groundwater::synth::lsynth_groundwater_t::configure(
        ldate_t const *  _date,
        unsigned int  _s)
{
    crabmeat_assert( _date);
    return  this->scratch_.configure( &this->info_, _date, (cbm::is_invalid( _s) ? ( static_cast< unsigned int >( _date->subday())) : _s));
}

#define  _HAVE_FIX_BUGGY_GROUNDWATER_DATA
#ifdef  _HAVE_FIX_BUGGY_GROUNDWATER_DATA
#include  "utils/lutils-swap.h"
#define  R(__i__)  input::groundwater::record::RECORD_ITEM_##__i__
static void _fix_buggy_in( ldndc::groundwater::synth::record_item_t * /*_rec*/)
{
}
static void _fix_buggy_out( ldndc::groundwater::synth::record_item_t * /*_rec_s*/,
		ldndc::groundwater::synth::record_item_t * /*_rec_d*/)
{
}
#undef  R
#endif  /*  _HAVE_FIX_BUGGY_GROUNDWATER_DATA  */

lerr_t
ldndc::groundwater::synth::lsynth_groundwater_t::synthesize_record(
        record_item_t const *  _rec_s_i, record_item_t *  _rec_s_o,
        record_item_t const *  _rec_d_i, record_item_t *  _rec_d_o,
        ldate_t const *  _date,
        lvector_t< unsigned int, 1 > *  _mod_cnt)
{
    crabmeat_assert( _rec_s_o || _rec_d_o);

    /* copy or clear temporary buffers */
    if ( _rec_s_i)
    {
        cbm::mem_cpy( this->scratch_.rec_s, _rec_s_i, SYNTH_GROUNDWATER_REC_SIZE);
    }
    else
    {
        cbm::mem_set( this->scratch_.rec_s, SYNTH_GROUNDWATER_REC_SIZE, ldndc::invalid_t< record_item_t >::value);
    }
    if ( _rec_d_i)
    {
        cbm::mem_cpy( this->scratch_.rec_d, _rec_d_i, SYNTH_GROUNDWATER_REC_SIZE);
    }
    else
    {
        cbm::mem_set( this->scratch_.rec_d, SYNTH_GROUNDWATER_REC_SIZE, ldndc::invalid_t< record_item_t >::value);
    }

#ifdef  _HAVE_FIX_BUGGY_GROUNDWATER_DATA
    if ( _rec_s_i)
    {
        _fix_buggy_in( this->scratch_.rec_s);
    }
    if ( _rec_d_i)
    {
        _fix_buggy_in( this->scratch_.rec_d);
    }
#endif  /*  _HAVE_FIX_BUGGY_GROUNDWATER_DATA  */
    for ( size_t  j = 0;  j < SYNTH_GROUNDWATER_REC_SIZE;  ++j)
    {
        if ( ! groundwater_synthesizers[j])
        {
            continue;
        }

        /* shortcut for index */
        record::record_item_e const  I( static_cast< record::record_item_e >( j));

        /* synthesize subday item if output buffer was provided */
        record_item_t *  rec_s_o_I(( _rec_s_o && cbm::is_invalid( this->scratch_.rec_s[I])) ? (this->scratch_.rec_s + I) : NULL);
        /* synthesize day item if output buffer was provided */
        record_item_t *  rec_d_o_I(( _rec_d_o && cbm::is_invalid( this->scratch_.rec_d[I])) ? (this->scratch_.rec_d + I) : NULL);
        if ( rec_s_o_I || rec_d_o_I)
        {

            /* synthesize requested data item(s) */
            lerr_t  rc = groundwater_synthesizers[j]->synthesize(
                    rec_s_o_I, rec_d_o_I,
                    &this->scratch_,
                    _date, this->scratch_.subday,
                    &this->info_);

            if ( rc)
            {
                CBM_LogError( "error synthesizing groundwater record item  [item=", RECORD_ITEM_NAMES[I], "]");
                return  LDNDC_ERR_FAIL;
            }
            else if ( _mod_cnt)
            {
                (*_mod_cnt)[I] += 1;
            }
        }
    }

#ifdef  _HAVE_FIX_BUGGY_GROUNDWATER_DATA
	_fix_buggy_out( this->scratch_.rec_s, this->scratch_.rec_d );
#endif  /*  _HAVE_FIX_BUGGY_GROUNDWATER_DATA  */

    /* update record */
    if ( _rec_s_o)
    {
        cbm::mem_cpy( _rec_s_o, this->scratch_.rec_s, SYNTH_GROUNDWATER_REC_SIZE);
    }
    if ( _rec_d_o)
    {
        cbm::mem_cpy( _rec_d_o, this->scratch_.rec_d, SYNTH_GROUNDWATER_REC_SIZE);
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::groundwater::synth::lsynth_groundwater_t::check_record(
        record_item_t const *  /*_rec_s_i*/,
        record_item_t const *  /*_rec_d_i*/)
const
{
    return  LDNDC_ERR_FAIL/*OK*/;
}

