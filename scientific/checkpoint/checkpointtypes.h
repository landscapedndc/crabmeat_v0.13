/*!
 * @brief
 *    declare common types related to checkpoint input
 *
 * @author
 *    steffen klatt (created on: may 23, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_CHECKPOINTTYPES_H_
#define  LDNDC_INPUT_CHECKPOINTTYPES_H_

#include  "crabmeat-common.h"
#include  "io/outputtypes.h"
#include  "string/cbm_string.h"

namespace  ldndc{ namespace  checkpoint
{
#define  LDNDC_CHECKPOINT_RECORDRANK  1
#define  LDNDC_CHECKPOINT_RECORDSIZE  3

extern CBM_API ldndc_string_t const  IDS[LDNDC_CHECKPOINT_RECORDSIZE];
extern CBM_API ldndc_output_size_t const  SIZES[LDNDC_CHECKPOINT_RECORDRANK];
extern CBM_API atomic_datatype_t const  TYPES[LDNDC_CHECKPOINT_RECORDRANK];
extern CBM_API ldndc_output_rank_t const  RANK;
extern CBM_API ldndc_output_size_t const  SIZE;

struct CBM_API checkpoint_buffer_t
{
    checkpoint_buffer_t();
    checkpoint_buffer_t(
        char const * /*class*/, char const * /*name*/);
    ~checkpoint_buffer_t();
    void  free();

    char const *  entity_class;
    char const *  entity_name;

    ldndc_sink_meta_timestamp_t  timestamp;

    char *  buffer;
    ldndc_int32_t  n_buffer;
};

}}

#endif  /*  !LDNDC_INPUT_CHECKPOINTTYPES_H_  */

