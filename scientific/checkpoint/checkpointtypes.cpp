/*!
 * @brief
 *    common types related to checkpoint input
 *
 * @author
 *    steffen klatt (created on: may 23, 2014),
 *    edwin haas
 */

#include  "checkpoint/checkpointtypes.h"

ldndc_output_rank_t const  ldndc::checkpoint::RANK = LDNDC_CHECKPOINT_RECORDRANK;
ldndc_string_t const  ldndc::checkpoint::IDS[LDNDC_CHECKPOINT_RECORDSIZE] =
{
    "__class__", "__entity__", "value"
};
ldndc_output_size_t const  ldndc::checkpoint::SIZES[LDNDC_CHECKPOINT_RECORDRANK] =
{
    3
};
atomic_datatype_t const  ldndc::checkpoint::TYPES[LDNDC_CHECKPOINT_RECORDRANK] =
{
    LDNDC_RAW
};

ldndc_output_size_t const  ldndc::checkpoint::SIZE = LDNDC_CHECKPOINT_RECORDSIZE;

ldndc::checkpoint::checkpoint_buffer_t::checkpoint_buffer_t(
        char const * _entity_class, char const * _entity_name)
    : entity_class( _entity_class), entity_name( _entity_name), timestamp( 0),
      buffer( NULL), n_buffer( 0)
{
}
ldndc::checkpoint::checkpoint_buffer_t::checkpoint_buffer_t()
    : entity_class( NULL), entity_name( NULL), timestamp( 0),
      buffer( NULL), n_buffer( 0)
{
}
ldndc::checkpoint::checkpoint_buffer_t::~checkpoint_buffer_t()
{
    this->free();
}

#include  <stdlib.h>
void
ldndc::checkpoint::checkpoint_buffer_t::free()
{
    if ( this->buffer)
    {
        ::free( this->buffer);
    }
    this->buffer = NULL;
    this->n_buffer = 0;

    this->entity_class = NULL;
    this->entity_name = NULL;
}

