/*!
 * @brief
 *    declare common types related to groundwater input
 *
 * @author
 *    steffen klatt (created on: may 29, 2015),
 *    david kraus
 */

#ifndef  LDNDC_INPUT_GROUNDWATER_TYPES_H_
#define  LDNDC_INPUT_GROUNDWATER_TYPES_H_

#include  "crabmeat-common.h"
#include  "datafilters/filter-average.h"

namespace  ldndc{ namespace  groundwater
{
/*!
 * @brief
 *    groundwater meta data
 */
struct  CBM_API  groundwater_info_t
{
    /*! groundwater table  [m] */
    double  watertable;
    /*! nitrate concentration  [] */
    double  no3;
};

extern  CBM_API groundwater_info_t const  groundwater_info_defaults;

struct  CBM_API  record
{
    /* groundwater item datatype */
    typedef  double  item_type;
    enum  record_item_e
    {
        RECORD_ITEM_WATERTABLE,
        RECORD_ITEM_NO3,

        RECORD_ITEM_DUMMY_1,
        RECORD_ITEM_DUMMY_2,
        RECORD_ITEM_DUMMY_3,
        RECORD_SIZE
    };
};

extern CBM_API char const *  RECORD_ITEM_UNITS[record::RECORD_SIZE];

extern CBM_API char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE];
extern CBM_API char const *  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE];

struct  CBM_API  streamdata_info_t
{
    enum { RECORD_SIZE = record::RECORD_SIZE };
    enum { BUFFER_SIZE_LOG = _CONFIG_BUFFERSIZE_LOG_GROUNDWATER };

    typedef  record::record_item_e  record_item_e;
    typedef  record::item_type  element_type;
    typedef  groundwater_info_t  boundary_data_type;
};


extern  CBM_API data_filter_average_t< record::item_type > const  groundwater_datafilter_average;

extern  CBM_API data_filter_t< record::item_type > const *  GROUNDWATER_DATAFILTER_LIST[record::RECORD_SIZE];

}}

#endif  /*  !LDNDC_INPUT_GROUNDWATER_TYPES_H_  */


