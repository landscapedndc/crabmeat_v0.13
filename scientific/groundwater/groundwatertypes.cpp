/*!
 * @author
 *    steffen klatt (created on: may 29, 2015),
 *    david kraus
 */

#include  "groundwater/groundwatertypes.h"

namespace  ldndc{ namespace  groundwater
{

char const *  RECORD_ITEM_UNITS[record::RECORD_SIZE] =
{
    "m", "?", "*", "*", "*"
};

char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE] =
{
    "watertable", "no3", "*", "*", "*"
};
char const *  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE] =
{
    "watertable", "nitrate", "*", "*", "*"
};


data_filter_average_t< record::item_type > const  groundwater_datafilter_average;

data_filter_t< record::item_type > const *  GROUNDWATER_DATAFILTER_LIST[record::RECORD_SIZE] =
{
        &groundwater_datafilter_average, /*  WATERTABLE */
        &groundwater_datafilter_average, /*  NO3 */
        NULL,
        NULL,
        NULL
};

groundwater_info_t const  groundwater_info_defaults =
{
    99.0 /* watertable depth */,
    0.0 /* no3 */
};

}}

