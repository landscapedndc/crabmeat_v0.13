/*!
 * @brief
 *
 * @author
 *    David Kraus,
 *    Ruediger Grote,
 *    Steffen Klatt,
 */

#include  "scientific/meteo/ld_meteo.h"


/*!
 * @page miclibs
 * Stefan–Boltzmann equation for the power radiated
 * from a black body in terms of its temperature.
 */
double
ldndc::meteo::stefan_boltzmann(
                               double _temp /* temperature (oC) */)
{
    return cbm::SIGMA * std::pow(_temp + 273.16, 4.0);
}


/*!
 * @page miclibs
 * Thornton
 */
double
ldndc::meteo::thornton97(
        double  _t_min, double  _t_max)
{
    return  0.394 * _t_min + 0.606 * _t_max;
}


/*!
 * @page miclibs
 * Global radiation depending on latitude
 */
double
ldndc::meteo::latitude_rad(
        double  _latitude,
        unsigned int *  _nd, unsigned int *  _ys, unsigned int *  _ye)
{
    crabmeat_assert( !_ye || ( *_ye == 365u || *_ye == 366u));

    double  lat_r( _latitude * cbm::PI / 180.0);

    if ( _latitude < 0.0)
    {
        if ( _nd)
        {
            unsigned int  ylen(( _ye) ? *_ye : 365);
            /* note: only approximately */
            if ( *_nd > ylen)
            {
                *_nd %= ylen;
            }
            *_nd += ( *_nd > (( ylen == 366u) ? 184u : 183u)) ? -((int)(( ylen == 366u) ? 184u : 183u)) : 182u;
        }

        if ( _ys)
        {
            *_ys = 183u;
        }

        if ( _ye)
        {
            *_ye = 182u;
        }

        return  -lat_r;
    }
    else
    {
        /* _nd might be "normalized" */
        if ( _nd)
        {
            if ( _ye && ( *_nd > *_ye))
            {
                /* note: only approximately */
                *_nd %= *_ye;
            }
        }

        if ( _ys)
        {
            *_ys = 1u;
        }

        /* _ye needs no modification */

        return  lat_r;
    }
}


double
ldndc::meteo::latitude_rad2(
                            double  _latitude)
{
    return (cbm::PI / 180.0 * _latitude);
}


/*!
 * @page miclibs
 * Sun declination
 */
double
ldndc::meteo::sun_declination(
        double  _latitude,
        unsigned int  _jday)
{
    unsigned int  jday( _jday);

    (void)latitude_rad( _latitude, &jday);

    /* Lee et al 2010 */
    return  0.4093 * sin(( 2.0 * cbm::PI * jday) / 365.0 - 1.405);
}

double
ldndc::meteo::sun_declination2(
                               unsigned int  _jday,
                               unsigned int  _days_in_current_year)
{
    return  0.409 * sin((2.0 * cbm::PI * (double)_jday / (double)_days_in_current_year) - 1.39);
}

double
ldndc::meteo::solar_elevation_sinus(
        double  _latitude,
        unsigned int  _jday,
        unsigned int  _subday, unsigned int  _t_res)
{
    double  lat_r( latitude_rad( _latitude));

    double  sdec( sun_declination( _latitude, _jday));
    double  sinld( sin( lat_r) * sin( sdec));
    double  cosld( cos( lat_r) * cos( sdec));

    if (( _t_res == 1) || cbm::is_invalid( _t_res))
    {
        /* at noon */
        return  sinld + cosld;
    }
    return  sinld + cosld * cos( 2.0 *  cbm::PI * ((( ((_subday<<1)<=_t_res) ? 1.0 : -1.0 ) * ( (double)_subday / (double)_t_res)) + 0.5));
}

/*!
 * @page miclibs
 * Eccentricity factor for calculation of extraterrestrial radiation
 * (Fortin et al., 2008)
 */
double
ldndc::meteo::eccentricity(
        unsigned int  _jday, unsigned int  _days_in_year)
{
    return ( 1.0 + cbm::EXC * cos( 2.0 *  cbm::PI * (double)_jday / (double)_days_in_year));
}

/*!
 * @page miclibs
 * Daily extraterrestrial radiation as developed by Sellers 1961
 * (cit. in Fortin et al. 2008, and Cai et al. 2007)
 * note: formula of sun declination wrong in Fortin et al. 2008
 */
double
ldndc::meteo::daily_solar_radiation(
        double  _latitude,
        unsigned int  _jday, unsigned int  _days_in_year)
{
    double const  lat_r( latitude_rad2( _latitude));
    double const  sdec( sun_declination2( _jday, _days_in_year));

    /* solar constant considering earth axis variations [W m-2] */
    double  rad_max( cbm::AVG_SOLAR_CONSTANT / cbm::PI * eccentricity( _jday, _days_in_year));

    double const  angle = acos( -tan( lat_r) * tan( sdec));

    rad_max *= ( angle * sin( sdec) * sin( lat_r) + cos( sdec) * cos( lat_r) * sin( angle));

    return  rad_max;
}


/*!
 * @page miclibs
 * Ratio between diffuse and total solar irradiance after Friend et al., 2001
 */
double
ldndc::meteo::subdaily_solar_radiation_fraction(
        double  _ses)
{
    if ( cbm::flt_greater_zero( _ses))
    {
        return  cbm::bound_max( 0.847 - 1.61 * _ses + 1.04 * cbm::sqr( _ses), 1.0);
    }
    return  1.0;
}

/*!
 * @page miclibs
 * Lizaso et al. 2005
 */
double
ldndc::meteo::daily_solar_radiation_fraction(
        double  _rad, double  _rad_max)
{
    if ( cbm::flt_greater_zero( _rad_max))
    {
        return  cbm::bound_max( /*C1=*/0.156 + /*C2=*/0.86 / (1.0 + exp( 11.1 * ( _rad / _rad_max - 0.53))), 1.0);
    }
    return  0.0;
}


/*!
 * @page miclibs
 * Daylength
 */
double
ldndc::meteo::daylength(
        double  _latitude, unsigned int  _jday)
{
    double const  sdec( sun_declination( _latitude, _jday));
    double const  lat_r( latitude_rad( _latitude));

    double  tanld(( sin( lat_r) * sin( sdec)) / ( cos( lat_r) * cos( sdec)));

    return  12.0 * ( 1.0 + 2.0 * asin( cbm::bound( -1.0, tanld, 1.0)) / cbm::PI);
}


/*!
 * @page miclibs
 * Daylength period
 */
double
ldndc::meteo::daylength_period(
        double  _latitude, unsigned int  _jday)
{
    double const  sdec( sun_declination( _latitude, _jday));
    double const  lat_r( latitude_rad( _latitude));

    /* note:  sin( -4.0 * cbm::PI / 180.0) --> sin( -pi/45) */
    double const  sinld_cosld(( -sin( -cbm::PI / 45.0) + ( sin( lat_r) * sin( sdec))) / ( cos( lat_r) * cos( sdec)));

    return  cbm::bound_max( 12.0 * ( cbm::PI + 2.0 * asin( cbm::bound( -1.0, sinld_cosld, 1.0))) / cbm::PI, (double)cbm::HR_IN_DAY);
}


/*!
 * @page miclibs
 * Saturated vapor pressure according to hamon:1963a
 *
 * When foliage temperature is above 0oC, saturated vapor pressure is calculated as:
 * \f[
 * svp = 0.61078 * \exp(17.26939 * \frac{T}{T + 237.3})
 * \f]
 *
 * When foliage temperature is below 0oC, it is:
 * \f[
 * svp = 0.61078 * \exp(21.87456 * \frac{T}{T + 265.5})
 * \f]
 */
void
ldndc::meteo::vps(
        double  _temp,
        double *  _vps,
        double *  _slope)
{
    /* saturation vapor pressure  [kPa] (Murray, 1967) */
    static double const  V1[] = { 0.61078, 0.61078};
    static double const  V2[] = { 17.26939, 21.87456};
    static double const  V3[] = { 237.3, 265.5};

    if ( _vps)
    {
        size_t const  k_vp(( _temp > 0.0) ? 0 : 1);
        *_vps = V1[k_vp] * exp( V2[k_vp] * _temp / ( _temp + V3[k_vp]));

        if ( _slope)
        {
            *_slope = V3[k_vp] * V2[k_vp] * (*_vps) / cbm::sqr( _temp + V2[k_vp]);
        }
    }
}


/*!
 * @page miclibs
 * Annual mean temperature
 */
double
ldndc::meteo::annual_average_temperature(
        double  _t_ny, double  _t_nd)
{
    double  fnd( 1.0 / 365.0);
    return  ( _t_ny * ( 365.0 - fnd) + ( _t_nd * fnd)) * fnd;
}


/*!
 * @page miclibs
 * @section longwave_radiation Incoming longwave radiation
 * Incoming longwave radiation is given by:
 * \f[
 * LWR_{in} = \varepsilon \sigma T^4 \; ,
 * \f]
 * wherein \f$ \varepsilon, \sigma, T \f$ refer to
 * the emissivity, the Stefan-Boltzmann constant
 * and air temperature, respectively.
 */
double
ldndc::meteo::longwave_radiation(
                          double _clouds,
                          double _temp,
                          double _vp)
{
    /*!
     * @page miclibs
     * The emissivity \f$ \varepsilon \f$ has two components
     * , i.e., emissivity under clear (\f$ \varepsilon_{cl} \f$)
     * and clouded sky (\f$ \varepsilon_{cl} \f$):
     * \f[
     * \varepsilon = (1 - f_{cl}) \varepsilon_{cs} + f_{cl} \varepsilon_{cl}
     * \f]
     * The partitioning coefficient \f$ f_{cl} \f$ refers to the
     * cloud fracion. For \f$ \varepsilon_{cl} \f$, a constant value
     * of 0.976 is used @cite greuell:1997a.
     */
    double const epsilon_cs( radiation_emissivity_clear_sky( _vp));
    double const epsilon_cl( 0.976);
    double const emissivity( (1.0 - _clouds) * epsilon_cs + _clouds * epsilon_cl);
    return emissivity * cbm::SIGMA * std::pow(_temp + 273.16, 4.0);
}


/*!
 * @page miclibs
 * <b> Clear sky atmospheric emissivity </b> \n
 * Emissivity of incoming longwave radiation under clear sky conditions
 * is calculated based on vapour pressure \f$ vp \f$ @cite brunt:1932a :
 * \f[
 * \varepsilon_{cs} = B_c + B_d \sqrt{vp}
 * \f]
 * The parameters \f$ B_c \f$ and \f$ B_d \f$ are set to 0.53 and 0.212,
 * respectively @cite kraalingen:1997a.
 */
double
ldndc::meteo::radiation_emissivity_clear_sky(
                                      double _vp /* vapour pressure */)
{
    /* Brunt parameters (-) */
    double const bc_brunt( 0.53);
    double const bd_brunt( 0.212);

    return bc_brunt + bd_brunt * cbm::sqrt( _vp);
}


/*!
 * @page miclibs
 * <b> Cloudiness </b> \n
 * Cloudiness is derived from global radiation
 * using the Angstrom formular:
 * \f[
 * f_{cl} = \frac{\frac{SWR}{SWR^{\ast}} - A}{B}
 * \f]
 * using the Angstrom parameters \f$ A = 0.29 \f$
 * and \f$ B = 0.52 \f$ @cite kraalingen:1997a.
 */
double
ldndc::meteo::cloud_fraction_angstrom(
                               int _doy,
                               int _nd_doy,
                               double _lat,
                               double _rad_short_day)
{
    /* A value of Angstrom formula [-] */
    double ang_a( 0.29);

    /* B value of Angstrom formula [-] */
    double ang_b( 0.52);

    /* Average atmospheric transmission coefficient [-] */
    double const solar_radiation( ldndc::meteo::daily_solar_radiation(_lat, _doy, _nd_doy));

    return (1.0 - cbm::bound( 0.0, _rad_short_day / solar_radiation, 1.0) - ang_a) / ang_b;
}
