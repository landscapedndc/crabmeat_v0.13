/*!
 * @brief
 *    collection of various helper functions
 *
 * @author
 *      david kraus,
 *      ruediger grote,
 *      steffen klatt,
 *      ...
 */

#ifndef  LDNDC_SCIENTIFIC_METEO_H_
#define  LDNDC_SCIENTIFIC_METEO_H_

#include  "crabmeat-common.h"

#include  "math/cbm_math.h"
#include  "utils/cbm_utils.h"

#include  "constants/cbm_const.h"

namespace  ldndc  {  namespace  meteo
{
    inline double CBM_API afgen(
                                const double  ABSMIN,
                                const double  MIN,
                                const double  MAX,
                                const double  value)
    {
        return  std::max( ABSMIN, 1.0 - ( 1.0 / ( MAX - MIN)) * std::max( 0.0, value - MIN));
    }

    /*!
     * @brief
     *    saturation vapor pressure function (mb) ( t is temperature in Kelvin)
     *
     * @note
     *      formerly called "ES" in module watercycleCANOAK
     */
    inline double CBM_API svp(
                              double  t)
    {
        return  exp( 54.8781919 - ( 6790.4985 / t) - ( 5.02808 * log( t)));
    }

    //FIXME  polish us!
    /*! Latent heat of Vaporisation, J kg-1 */
    inline double CBM_API LAMBDA(
                                 double  tak,
                                 double  dtk)
    {
        return  ( 3149000.0 - 2370.0 * tak) + (( tak < dtk) ? 334.0 : 0.0);
    }


    /*! first derivative of es with respect to tk (routine needs to convert es(t) (mb) to Pa) */
    inline double CBM_API DESDT(
                                double  t,
                                double  fl18,
                                double  rgc)
    {
        return  ldndc::meteo::svp( t) * 100.0 * fl18  / ( rgc * t * t);
    }

    /*! The second derivative of the saturation vapor pressure temperature curve,
     * using the polynomial equation of Paw U
     */
    inline double CBM_API DES2DT(
                                 double  t,
                                 double  dtk)
    {
        //double tcel = t - dtk;
        //double y = 2. * 1.675 + 6. * 0.01408 * tcel + 12. * 0.0005818 * tcel * tcel;
        //return y;
        return  cbm::poly2( t - dtk,  2.0 * 1.675,  6.0 * 0.01408,  12.0 * 0.0005818);
    }
    
    
    inline double CBM_API Ft_pow(
                                 double  _base,
                                 double  _scale,
                                 double  _temperature)
    {
        return  pow( _base, ( _temperature - _scale) / _scale);
    }


    inline double CBM_API Ft_oneill(
                                    double  _t_max,
                                    double  _t_opt,
                                    double  _exp,
                                    double  _temperature)
    {
        if ( cbm::flt_equal( _t_opt, _temperature /*d0==d1*/) || ( cbm::flt_equal_zero( _exp /*_exp==0*/)))
        {
            return  1.0;
        }
        if (( _t_max < _temperature) || cbm::flt_equal( _t_max, _temperature /*d0==0*/))
        {
            return  0.0;
        }
        if ( cbm::flt_equal( _t_max, _t_opt /*d1==0*/))
        {
            return  std::numeric_limits< double >::infinity();
        }

        double  d0( _t_max - _temperature);
        double  d1( _t_max - _t_opt);

        return  pow( d0 / d1, _exp) * exp( _exp * ( d1 - d0) / d1);
    }


    inline double CBM_API F_weibull(
                                    double  wfps,
                                    double  P1,
                                    double  P2,
                                    double  P3)
    {
        return  ( 1.0 - ( P3 / ( 1.0 + exp((wfps - P1) * P2))));
    }


    inline double CBM_API F_surf(
                                 double  var,
                                 double  a,
                                 double  b)
    {
        return var / ( var + exp( a - b * var));
    }


    inline double CBM_API Ftemp(
                                double CT,
                                double HA,
                                double HD,
                                double DS,
                                double tempK)
    {
        return  exp( CT - HA / ( cbm::RGAS * tempK)) / ( 1.0 + exp(( DS * tempK - HD) / ( cbm::RGAS * tempK)));
    }


    inline double CBM_API Three_Point_Effect(
                                             double  T,
                                             double  Topt,
                                             double  Thigh,
                                             double  Tlow)
    {
        if (( T > Tlow) && ( T < Thigh))
        {
            double  l(Topt - Tlow);
            double  h(Thigh - Topt);
            
            return  ( (( T - Tlow) / l) * pow((( Thigh - T) / h), h / l));
        }
        else
        {
            return  0.0;
        }
    }


    inline double CBM_API Ft_q10(
                                 float  T,
                                 float  Topt,
                                 float  Tmax,
                                 float  q10)
    {
        if (( T < 0.0)  ||  ( T > Tmax))
        {
            return  0.0;
        }
        else
        {
            double  wt( log( q10) * ( Tmax - Topt));
            double  f( 1.0 + pow(( 1.0 + 40.0 / wt), 0.5));
            //double  f( 1.0 + 40.0 / wt);
            double  xt( wt * wt * f * f / 400.0);
            //double  xt( sqrt( f));
            //        xt = wt * wt * ( 1.0 + xt + xt + f) / 400.0;
            double  vt(( Tmax - T) / ( Tmax - Topt));
            return  pow( vt, xt) * exp ( xt * ( 1.0 - vt));
        }
    }


    inline double CBM_API relativeImpact(
                                         double  v,
                                         double  vSat)
    {
        if ( v > 0.5*vSat)
        {
            return  1.0 - 2.0 * cbm::sqr((v - vSat) / vSat);
        }
        else
        {
            return        2.0 * cbm::sqr( v         / vSat);
        }
    }

    /*!
     * @brief
     *    Stefan Boltzmann equation.
     *
     * @param
     *    temperature
     */
    double
    CBM_API stefan_boltzmann(
                             double /* temperature (oC) */);

    /*!
     * @brief
     *    estimate average temperature from minimum
     *    and maximum temperature
     *
     * @param
     *    minimum temperature
     * @param
     *    maximum temperature
     */
    double CBM_API thornton97(
                              double,
                              double);

    /*!
     * @brief
     *    distinguishes between northern and southern
     *    hemisphere and converts it to radians
     * @param
     *    latitude
     * @param
     *    julian day, if given it adds half a year if
     *    latitude indicates southern hemisphere
     * @param
     *    "year start", if given it writes the first
     *    day of the year to the parameter.
     * @param
     *    "year end", if given it writes the last
     *    days of the year to the parameter. the value
     *    passed in must then hold the number of days
     *    in the current year (i.e. either 365 or 366).
     */
    double CBM_API latitude_rad(
                                double,
                                unsigned int * = NULL,
                                unsigned int * = NULL,
                                unsigned int * = NULL);

    /*!
     * @brief
     *    distinguishes between northern and southern
     *    hemisphere and converts it to radians
     */
    double CBM_API latitude_rad2(
                                 double);

    /*!
     * @brief
     *    calculate sun declination
     *
     * @param
     *    latitude
     * @param
     *    julian day
     */
    double CBM_API sun_declination(
                                   double,
                                   unsigned int);

    /*!
     * @brief
     *    calculate sun declination
     *
     * @param
     *    julian day
     * @param
     *    days in current year
     */
    double CBM_API sun_declination2(
                                    unsigned int,
                                    unsigned int);

    /*!
     * @brief
     *    sinus of (sub-daily) solar elevation
     *    (sinb_ts,sinb_nd)
     *
     *    if only latitude and sun declination is given,
     *    it returns the (averaged sub-)daily sinus
     *    of sub-daily solar elevation.
     *
     * @todo
     *    i suspect a bug in the original code!
     *
     * @param
     *    latitude
     * @param
     *    julian day
     * @param (optional, see desciption)
     *    subday
     * @param (optional, see desciption)
     *    time resolution
     */
    double CBM_API solar_elevation_sinus(
                                         double,
                                         unsigned int,
                                         unsigned int = invalid_uint, unsigned int = invalid_uint);

    /*!
     * @brief
     *    eccentricity factor for calculation of solar global radiation
     *    (Fortin et al., 2008)
     *
     * @param
     *    julian day
     * @param
     *    days in year
     */
    double CBM_API eccentricity(
                                unsigned int, unsigned int);

    /*!
     * @brief
     *    solar global radiation  [Wm-2]
     *    (radMax)
     *
     * @param
     *    latitude
     * @param
     *    julian day
     * @param
     *    days in year
     */
    double CBM_API daily_solar_radiation(
                                         double,
                                         unsigned int, unsigned int);

    /*!
     * @brief
     *    fraction of direct radiation to total radiation
     *    (fdif_ts)
     *
     *    (Friend et al 2001)
     *
     * @param
     *    solar elevation sinus
     */
    double CBM_API subdaily_solar_radiation_fraction(
                                                     double);

    /*!
     * @brief
     *    (averaged sub-)daily fraction of direct radiation to total radiation
     *    (fdif_nd)
     *
     *    (Lizaso et al. 2005)
     *
     * @param
     *    radiation  [Wm-2]
     * @param
     *    solar global radiation  [Wm-2]
     */
    double CBM_API daily_solar_radiation_fraction(
                                                  double, double);

    /*!
     * @brief
     *    daylength  [h]
     *    (dayl)
     *
     * @param
     *    latitude
     * @param
     *    julian day
     */
    double CBM_API daylength(
                             double,
                             unsigned int);

    /*!
     * @brief
     *    daylength period, including twilight periods  [h]
     *    (daylp)
     *
     * @param
     *    latitude
     * @param
     *    julian day
     */
    double CBM_API daylength_period(
                                    double,
                                    unsigned int);


    /*!
     * @brief
     *    returns saturated vapor pressure [kPa] for
     *    given temperature [oC]
     *
     * @param
     *    temperature
     * @param
     *    result buffer for saturated vapor pressure
     */
    void CBM_API vps(
                     double, double *, double *_slope=NULL);


    /*!
     * @brief
     *    annual average temperature correction [oC]
     *
     * @note
     *    number of days in current year approximated by 365
     *
     * @param
     *    annual average temperature
     * @param
     *    current temperature
     */
    double CBM_API annual_average_temperature(
                                              double, double);

    double CBM_API longwave_radiation(
                                      double _clouds,
                                      double _temp,
                                      double _vp);

    double CBM_API radiation_emissivity_clear_sky(
                                                  double _vp /* vapour pressure */);

    double CBM_API cloud_fraction_angstrom(
                                           int _doy,
                                           int _nd_doy,
                                           double _lat,
                                           double _rad_short_day);
}  /*  namespace meteo  */
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_SCIENTIFIC_METEO_H_  */

