/*!
 * @brief
 *
 * @author
 *      Steffen Klatt (created on: jun 20, 2013)
 *      Edwin Haas
 *      David Kraus
 */

#include  "soillayers/soillayerstypes.h"

namespace  ldndc{ namespace  soillayers
{
char const *  HUMUS_MNEMONIC[HUMUS_CNT] =
{
    "HUMUS", 
    "MODER", "MODER_BEECH", "MODER_BIRCH", "MODER_EUCALYPTUS", "MODER_OAK", "MODER_PINE", "MODER_SPRUCE", 
    "MULL", "MULL_BEECH", "MULL_BIRCH", "MULL_EUCALYPTUS", "MULL_OAK", "MULL_PINE", "MULL_SPRUCE", 
    "RAWHUMUS", "RAWHUMUS_BEECH", "RAWHUMUS_BIRCH", "RAWHUMUS_EUCALYPTUS", "RAWHUMUS_OAK", 
	"RAWHUMUS_PINE", "RAWHUMUS_SPRUCE",
    "NONE"
};
char const *  HUMUS_NAMES[HUMUS_CNT] =
{
    "humus",
    "moder", "moder beech", "moder birch", "moder eucalyptus", "moder oak", "moder pine", "moder spruce", 
    "mull", "mull beech", "mull birch", "mull eucalyptus", "mull oak", "mull pine", "mull spruce", 
    "raw humus", "raw humus beech", "raw humus birch", "raw humus eucalyptus", "raw humus oak", 
	"raw humus pine", "raw humus spruce",
    "none"
};

char const *  SOIL_MNEMONIC[SOIL_CNT] =
{
    "BEDR", "CLAY", "CLLO",
    "LOAM", "LOSA", "ORMA",
    "SACL", "SALO", "SAND",
    "SICL", "SILO", "SILT",
    "SLCL", "SNCL",
    "PEAT", "PEAT_BOG", "PEAT_POORFEN", "PEAT_MODERATEFEN", "PEAT_RICHFEN",
    "PEAT_SWAMPFOREST_OMBROTROPHIC", "PEAT_SWAMPFOREST_MINEROTROPHIC",
    "NONE"
};
char const *  SOIL_NAMES[SOIL_CNT] =
{
    "bedrock", 
    "clay", "clay loam", 
    "loam", "loamy sand", 
    "organic material", 
    "sandy clay", "sandy loam", "sand", 
    "silty clay", "silty loam", "silt",
    "silty clay loam", "sandy clay loam",
    "peat", "peat bog", "peat poor fen", "peat moderate fen", "peat rich fen",
    "peat swamp forest ombrotrophic", "peat swamp forest minerotrophic",
    "none"
};
}}

