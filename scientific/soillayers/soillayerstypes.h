/*!
 * @brief
 *
 * @author
 *      Steffen Klatt (created on: mar 31, 2013)
 *      Edwin Haas
 *      David Kraus
 */

#ifndef  LDNDC_INPUT_SOILLAYERSTYPES_H_
#define  LDNDC_INPUT_SOILLAYERSTYPES_H_

#include  "crabmeat-common.h"

namespace  ldndc{ namespace  soillayers
{

/*!
 * @brief
 *    list of supported humus types
 */
enum  humus_type_e
{
    HUMUS_HUMUS            = 0U,            /* HUMUS, humus */
    
    HUMUS_MODER,                            /* MODER, moder */
    HUMUS_MODER_BEECH,                      /* MODER_BEECH,  mull */
    HUMUS_MODER_BIRCH,                      /* MODER_BIRCH,  mull */
    HUMUS_MODER_EUCALYPTUS,                 /* MODER_EUCALYPTUS,  mull */
    HUMUS_MODER_OAK,                        /* MODER_OAK,  mull */
    HUMUS_MODER_PINE,                       /* MODER_PINE,  mull */
    HUMUS_MODER_SPRUCE,                     /* MODER_SPRUCE,  mull */
    
    HUMUS_MULL,                             /* MULL,  mull */
    HUMUS_MULL_BEECH,                       /* MULL_BEECH,  mull */
    HUMUS_MULL_BIRCH,                       /* MULL_BIRCH,  mull */
    HUMUS_MULL_EUCALYPTUS,                  /* MULL_EUCALYPTUS,  mull */
    HUMUS_MULL_OAK,                         /* MULL_OAK,  mull */
    HUMUS_MULL_PINE,                        /* MULL_PINE,  mull */
    HUMUS_MULL_SPRUCE,                      /* MULL_SPRUCE,  mull */

    HUMUS_RAWHUMUS,                         /* RAWHUMUS, raw humus */
	HUMUS_RAWHUMUS_BEECH,                   /* RAWHUMUS_BEECH,  raw humus */
    HUMUS_RAWHUMUS_BIRCH,                   /* RAWHUMUS_BIRCH,  raw humus */
    HUMUS_RAWHUMUS_EUCALYPTUS,              /* RAWHUMUS_EUCALYPTUS,  raw humus */
    HUMUS_RAWHUMUS_OAK,                     /* RAWHUMUS_OAK,  raw humus */
    HUMUS_RAWHUMUS_PINE,                    /* RAWHUMUS_PINE,  raw humus */
    HUMUS_RAWHUMUS_SPRUCE,                  /* RAWHUMUS_SPRUCE,  raw humus */
    
    HUMUS_NONE,                             /* NONE, no specific humus type, defaults apply */
    HUMUS_CNT
};
extern CBM_API char const *  HUMUS_MNEMONIC[HUMUS_CNT];
extern CBM_API char const *  HUMUS_NAMES[HUMUS_CNT];

/*!
 * @brief
 *    list of supported soil types
 */
enum  soil_type_e
{
    SOIL_BEDROCK            = 0U,           /* BEDR, bedrock */
    SOIL_CLAY,                              /* CLAY, clay */
    SOIL_CLAY_LOAM,                         /* CLLO, clay loam */
    SOIL_LOAM,                              /* LOAM, loam */
    SOIL_LOAMY_SAND,                        /* LOSA, loamy sand */
    SOIL_ORGANIC_MATERIAL,                  /* ORMA, organic material */
    SOIL_SANDY_CLAY,                        /* SACL, sandy clay */
    SOIL_SANDY_LOAM,                        /* SALO, sandy loam */
    SOIL_SAND,                              /* SAND, sand */
    SOIL_SILTY_CLAY,                        /* SICL, silty clay */
    SOIL_SILTY_LOAM,                        /* SILO, silty loam */
    SOIL_SILT,                              /* SILT, silt */
    SOIL_SILTY_CLAY_LOAM,                   /* SLCL, silty clay loam */
    SOIL_SANDY_CLAY_LOAM,                   /* SNCL, sandy clay loam */
    PEAT,
    PEAT_BOG,
    PEAT_POORFEN,
    PEAT_MODERATEFEN,
    PEAT_RICHFEN,
    PEAT_SWAMPFOREST_OMBROTROPHIC,
    PEAT_SWAMPFOREST_MINEROTROPHIC,
    
    SOIL_NONE,                              /* NONE, no specific soil type, defaults apply */
    SOIL_CNT
};
extern CBM_API char const *  SOIL_MNEMONIC[SOIL_CNT];
extern CBM_API char const *  SOIL_NAMES[SOIL_CNT];

}}

#endif  /*  !LDNDC_INPUT_SOILLAYERSTYPES_H_  */

