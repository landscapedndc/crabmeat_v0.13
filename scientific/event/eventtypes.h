/*!
 * @brief
 *    common event related data types
 *
 * @author
 *    Steffen Klatt (created on: dec 10, 2011),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_EVENT_TYPES_H_
#define  LDNDC_INPUT_EVENT_TYPES_H_

#include  "crabmeat-common.h"
#include  "event/events/eventbase.h"

#include  "string/cbm_string.h"
#include  "time/cbm_time.h"

namespace  ldndc{ namespace  event
{
#include  "event.h.inc"

typedef  uintptr_t  reader_eventhandle_t;

struct  CBM_API  event_info_t
{
    event_info_t( ldndc::event::event_type_e  _type = ldndc::event::EVENT_NONE)
        : type( _type), t_exec_s( std::string( "")), r_data( 0)
    {}

    /*! event type */
    ldndc::event::event_type_e  type;

    /*! time string */
    std::string  t_exec_s;

    /*! reader internal data or pointer to event (just a tad nasty) */
    union
    {
        reader_eventhandle_t  r_data;
        Event *  event;
    };
};

/*!
 * @brief
 *    structure to internally handle events
 *
 * @note
 *    never ever pass reference to client
 *
 * @todo
 *    add ID, see iterator in event view
 */
struct  event_handle_t
{
    
    event_handle_t()
        : event( NULL),
          t_exec( 0),
          r( 1), R( 1)
    {
    }

    event_handle_t(
            Event const *  _event,
            ltime_t const &  _t_exec_l,
            ltime_t::td_scalar_t  _r = 1)
        : event( _event),
          tspan_exec( _t_exec_l),
          t_exec( _t_exec_l.from().seconds_since_epoch()),
          r( _r), R( _r)
        { }

    /* pointer to event properties ( _not_ owner) */
    Event const *  event;
    /* event type */
    inline
    ldndc::event::event_type_e  type()
    const
    {
        return  ( this->event) ? this->event->event_type() : ldndc::event::EVENT_NONE;
    }
    char const *  name()
    const
    {
        if ( this->event)
        {
            return  ldndc::event::EVENT_NAMES[this->type()];
        }
        return  invalid_str;
    }

    /* exec timespan */
    ltime_t  tspan_exec;
    /* exec timestep offset */
    ltime_t::td_scalar_t  t_exec;
    /* repeat counter (decremented each time event is dispatched) */
    ltime_t::td_scalar_t  r;
    /* repeat counter (not modified) */
    ltime_t::td_scalar_t  R;
};

}}

#endif  /*  !LDNDC_INPUT_EVENT_TYPES_H_  */

