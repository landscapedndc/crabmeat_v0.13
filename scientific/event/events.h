/*!
 * @brief
 *    convenience header for pulling all event declarations
 *
 * @author
 *    Steffen Klatt (created on: dec 10, 2011),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_EVENTS_H_
#define  LDNDC_INPUT_EVENTS_H_

#include  "events.h.inc"

#endif  /*  !LDNDC_INPUT_EVENTS_H_  */

