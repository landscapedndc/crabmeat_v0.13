/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: dec 03, 2014),
 */

#include  "event/events/event-checkpoint.h"

char const *  ldndc::event::__event_attrib_class_name(checkpoint)::CHECKPOINT_COMMAND_NAMES[CHECKPOINT_COMMAND_CNT] =
{
    "create", "restore"
};

ldndc::event::__event_attrib_class_name(checkpoint)::__event_attrib_class_name(checkpoint)()
        : event_attribute_t(),
          command( CHECKPOINT_COMMAND_CNT), options( "")
{
}
ldndc::event::__event_attrib_class_name(checkpoint)::~__event_attrib_class_name(checkpoint)()
{
}

bool
ldndc::event::__event_class_name(checkpoint)::has_option(
        char const *  _option)
const
{
    cbm::tokenizer  options( this->attribs_->options.tokenize( ' '));
    for ( size_t  k = 0;  k < options.token_cnt();  ++k)
    {
        if ( options[k])
        {
            cbm::key_value_t  kv =
                cbm::as_key_value( options[k]);
            if ( kv.key == _option)
            {
                return  true;
            }
        }
    }
    return  false;
}
cbm::string_t
ldndc::event::__event_class_name(checkpoint)::get_option_argument(
        char const *  _option)
const
{
    cbm::tokenizer  options( this->attribs_->options.tokenize( ' '));
    for ( size_t  k = 0;  k < options.token_cnt();  ++k)
    {
        if ( options[k])
        {
            cbm::key_value_t  kv =
                cbm::as_key_value( options[k]);
            if ( kv.key == _option)
            {
                return  kv.value;
            }
        }
    }
    return  ldndc_empty_string;
}

std::string
ldndc::event::__event_class_name(checkpoint)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT_(event_as_text,"command","",
        __event_attrib_class_name(checkpoint)::CHECKPOINT_COMMAND_NAMES[this->attribs_->command]);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"options",options.c_str());

    return  event_as_text.str();
}

