/*!
 * @brief
 *    event "irrigate" declaration
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_IRRIGATE_H_
#define  LDNDC_INPUT_EVENT_IRRIGATE_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  irrigate

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! quantity [mm] */
    double  amount;

    /*! store irrigation amount in reservoir */
    bool  reservoir;

    /*! pH value (not used) */
// sk:not implemented    double  ph;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        double  amount() const
            { return  this->attribs_->amount; }
        bool  reservoir() const
            { return  this->attribs_->reservoir; }
// sk:not implemented        double  ph() const { return  this->attribs_->ph; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_IRRIGATE_H_  */

