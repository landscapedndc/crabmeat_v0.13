/*!
 * @brief
 *    event "defoliate" implementation
 *
 * @author
 *    steffen klatt (created on: apr 29, 2013),
 *    ruediger grote,
 *    edwin haas
 */

#include  "event/events/event-defoliate.h"
#include  "species/speciestypes.h"

ldndc::event::__event_attrib_class_name(defoliate)::__event_attrib_class_name(defoliate)()
                : event_attribute_t(),
                  name( ldndc::species::SPECIES_NAME_NONE),
                  reduction_volume( invalid_dbl),
                  export_foliage( false)
{
}
ldndc::event::__event_attrib_class_name(defoliate)::~__event_attrib_class_name(defoliate)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(defoliate)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"reductionvolume",reduction_volume);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"exportfoliage",export_foliage);

    return  event_as_text.str();
}

