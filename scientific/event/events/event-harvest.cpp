/*!
 * @brief
 *    event "harvest" declaration
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 *    David Kraus
 * @date
 *    Dec 10, 2011
 */

#include  "event/events/event-harvest.h"

ldndc::event::__event_attrib_class_name(harvest)::__event_attrib_class_name(harvest)()
        : event_attribute_t(),

          name( ldndc::species::SPECIES_NAME_NONE),

          height( invalid_dbl),

          remains_relative( invalid_dbl),
          remains_absolute( invalid_dbl),

          export_stem_wood( 1.0),
          export_branch_wood( 0.0),
          export_root_wood( 0.0),

          mulching( false)
{
}
ldndc::event::__event_attrib_class_name(harvest)::~__event_attrib_class_name(harvest)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(harvest)::to_string()
const
{
    std::ostringstream  event_as_text;

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"height",height);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_relative",remains_relative);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute",remains_absolute);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"exportstemwood",export_stem_wood);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"exportbranchwood",export_branch_wood);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"exportrootwood",export_root_wood);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"mulching",mulching);

    return  event_as_text.str();
}

