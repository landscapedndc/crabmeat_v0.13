/*!
 * @brief
 *    (management) event factories
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011)
 */

#ifndef  LDNDC_INPUT_EVENT_FACTORIES_H_
#define  LDNDC_INPUT_EVENT_FACTORIES_H_

#include  "event/events/eventbase.h"

namespace  ldndc{ namespace  event{
struct  CBM_API  abstract_event_factory_t
{
    abstract_event_factory_t() {}
    virtual  ~abstract_event_factory_t() = 0;
    virtual  ldndc::event::event_type_e  event_type() const = 0;

    virtual  Event *  construct(
            lid_t const &, event_attribute_t **) const = 0;
    static  void  destroy( Event *  _event)
    {
        CBM_DefaultAllocator->destroy( _event);
    }
};

template < typename  _E, typename  _A >
struct  event_factory_t  :  public  abstract_event_factory_t
{
    event_factory_t()
        : abstract_event_factory_t()
    { }

    ldndc::event::event_type_e  event_type()
    const
    {
        return  (ldndc::event::event_type_e)_E::event_type_;
    }

    Event *  construct( lid_t const &  _id,
            event_attribute_t **  _attribs)
    const
    {
        _A *  attr = CBM_DefaultAllocator->construct< _A >();
        if ( _attribs)
        {
            *_attribs = static_cast< event_attribute_t * >( attr);
        }
        return  static_cast< Event * >( CBM_DefaultAllocator->construct_args< _E >(
                    static_cast< size_t >( 1), _id, attr));
    }
};

extern CBM_API abstract_event_factory_t const *  event_factory[EVENT_CNT];
extern CBM_API event_attribute_t const *  event_attribute_defaults[EVENT_CNT];
}}

#endif  /*  !LDNDC_INPUT_EVENT_FACTORIES_H_  */

