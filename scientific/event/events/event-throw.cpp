/*!
 * @brief
 *    event "throw" implementation
 *
 * @author
 *    steffen klatt (created on: apr 29, 2013),
 *    ruediger grote,
 *    edwin haas
 */

#include  "event/events/event-throw.h"
#include  "species/speciestypes.h"

ldndc::event::__event_attrib_class_name(throw)::__event_attrib_class_name(throw)()
    : event_attribute_t(), name( ldndc::species::SPECIES_NAME_NONE),
    reduction_number( invalid_dbl), reduction_number_is_absolute( false),
    reduction_volume( invalid_dbl), reduction_volume_is_absolute( false),
    reason( "")
{
}
ldndc::event::__event_attrib_class_name(throw)::~__event_attrib_class_name(throw)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(throw)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);

    if ( this->reduction_number_is_absolute())
        { LEVENT_WRITE_ATTRIB_PREFIX_TXT(event_as_text,"reductionnumber","!",reduction_number); }
    else
        { LEVENT_WRITE_ATTRIB_TXT(event_as_text,"reductionnumber",reduction_number); }
    if ( this->reduction_volume_is_absolute())
        { LEVENT_WRITE_ATTRIB_PREFIX_TXT(event_as_text,"reductionvolume","!",reduction_volume); }
    else
        { LEVENT_WRITE_ATTRIB_TXT(event_as_text,"reductionvolume",reduction_volume); }

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"reason",reason);

    return  event_as_text.str();
}

