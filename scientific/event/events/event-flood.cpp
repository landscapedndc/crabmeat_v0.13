/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#include  "event/events/event-flood.h"

ldndc::event::__event_attrib_class_name(flood)::__event_attrib_class_name(flood)()
        : event_attribute_t(),
          watertable( invalid_dbl),
          bundheight( invalid_dbl),
          percolationrate( invalid_dbl),
          irrigationheight( invalid_dbl),
          irrigationamount( invalid_dbl),
          unlimitedwater( true),
          saturationlevel( invalid_dbl),
          soildepth( invalid_dbl)
{
}
ldndc::event::__event_attrib_class_name(flood)::~__event_attrib_class_name(flood)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(flood)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"watertable",watertable);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"bundheight",bundheight);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"percolationrate",percolationrate);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"irrigationheight",irrigationheight);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"irrigationamount",irrigationamount);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"unlimitedwater",unlimitedwater);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"saturationlevel",saturationlevel);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"soildepth",soildepth);

    return  event_as_text.str();
}

