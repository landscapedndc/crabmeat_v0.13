/*!
 * @brief
 *    event "reparameterizespecies" declaration
 *
 * @author
 *    steffen klatt (created on: nov 06, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_REPARAMETERIZESPECIES_H_
#define  LDNDC_INPUT_EVENT_REPARAMETERIZESPECIES_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  reparameterizespecies


#include  "species/speciestypes.h"
namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t 
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! species type identifier */
    std::string  type;
    /*! species name (unique species instance identifier) */
    std::string  name;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        char const *  species_type() const
            { return  this->attribs_->type.c_str(); }
        char const *  species_name() const
            { return  this->attribs_->name.c_str(); }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_REPARAMETERIZESPECIES_H_  */

