/*!
 * @brief
 *    event "defoliate" declaration
 *
 * @author
 *    steffen klatt (created on: apr 29, 2013),
 *    ruediger grote,
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_DEFOLIATE_H_
#define  LDNDC_INPUT_EVENT_DEFOLIATE_H_

#include  "event/events/eventbase.h"

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  defoliate

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /** attributes **/

    /* species name (identifier) */
    std::string  name;

    /*! stemwood (or stand) volume to be removed [0,1] */
    double  reduction_volume;

    /*! amount of foliage exported (by insects) or not exported (if fungi)  {0,1} */
    bool  export_foliage;

};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
        /* default constructor, event attribute pointer, ... */
    EVENT_COMMON_DECL(__this_event_name)

    public:
        char const *  species_name() const
            { return  this->attribs_->name.c_str(); }
        double  reduction_volume() const
            { return  this->attribs_->reduction_volume; }
        bool  export_foliage() const
            { return  this->attribs_->export_foliage; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_DEFOLIATE_H_  */

