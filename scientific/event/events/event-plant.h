/*!
 * @brief
 *    event "plant" declaration
 *
 * @author
 *  - Steffen Klatt (created on: dec 10, 2011),
 *  - Edwin Haas
 *  - David Kraus
 */

#ifndef  LDNDC_INPUT_EVENT_PLANT_H_
#define  LDNDC_INPUT_EVENT_PLANT_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  plant


#include  "species/speciestypes.h"
#include  "speciesparameters/speciesparameterstypes.h"
namespace  ldndc{
namespace  event{

/*!
 * @page event_input
 * @section event_input_plant Planting event
 * The following event attributes can be defined (vegetation type-specific):
 * - name (all)
 * - type (all)
 * - group (all)
 * - fractionalcover (all)
 * - initialbiomass (all)
 * - covercrop (crop, grass)
 * - seedbedduration (crop)
 * - seedlingnumber (crop)
 * - heightmax (grass, wood)
 * - heightmin (wood)
 * - rootingdepth (grass, wood)
 * - creductionfactor (wood)
 * - dbh (wood)
 * - treenumber (wood)
 * - treevolume (wood)
 */
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t 
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! species group (see llib) */
    ldndc::species::species_group_e  group;

    /*! species type identifier */
    std::string  type;
    /*! species name (unique species instance identifier) */
    std::string  name;
    /*! long name */
    std::string  longname;

    std::string  location;

    /*! group specific properties */
    species::species_properties_t *  properties;
    union
    {
        species::crop_properties_t  crop;
        species::grass_properties_t  grass;
        species::wood_properties_t  wood;
    };

    /*! full set of species parameters (have precedence over default set) */
    speciesparameters::parameterized_species_t *  params;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        ldndc::species::species_group_e  group() const
            { return  this->attribs_->group; }

        char const *  species_type() const
            { return  this->attribs_->type.c_str(); }
        char const *  species_name() const
            { return  this->attribs_->name.c_str(); }
        char const *  species_longname() const
            { return  this->attribs_->longname.c_str(); }
        char const *  location() const
            { return  this->attribs_->location.c_str(); }

        species::species_properties_t const *  species_properties()
            const { return  this->attribs_->properties; }
        species::crop_properties_t const &  crop()
            const { return  this->attribs_->crop; }
        species::grass_properties_t const &  grass()
            const { return  this->attribs_->grass; }
        species::wood_properties_t const &  wood()
            const { return  this->attribs_->wood; }

        /* make sure it exists! (e.g. call to params().exists()) */
        bool  speciesparameters_given() const;
        speciesparameters::speciesparameters_set_t  params() const;
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_PLANT_H_  */

