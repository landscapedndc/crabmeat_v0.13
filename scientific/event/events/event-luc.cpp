/*!
 * @brief
 *    event "LUC" declaration
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#include  "event/events/event-luc.h"

char const *  ldndc::event::__event_attrib_class_name(luc)::COMMAND_NAMES[LUC_COMMAND_CNT+2] =
{
    "add", "del", "replace", "sleep", "wake",
           "<#command>", "none"
};
char const *  ldndc::event::__event_attrib_class_name(luc)::LUC_REPLACE_DELIM = "BY";
char const    ldndc::event::__event_attrib_class_name(luc)::LUC_ARGS_DELIM = ' ';

ldndc::event::__event_attrib_class_name(luc)::__event_attrib_class_name(luc)()
        : event_attribute_t(),
          command( LUC_COMMAND_NONE),
          args( "")
{
}
ldndc::event::__event_attrib_class_name(luc)::~__event_attrib_class_name(luc)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(luc)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT_(event_as_text,"command","",__event_attrib_class_name(luc)::COMMAND_NAMES[this->attribs_->command]);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"args",args);

    return  event_as_text.str();
}

