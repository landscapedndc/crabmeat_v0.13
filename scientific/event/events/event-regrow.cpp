/*!
 * @brief
 *    event "regrow" implementation
 *
 * @author
 *    steffen klatt (created on: apr 29, 2013),
 *    ruediger grote,
 *    edwin haas
 */

#include  "event/events/event-regrow.h"
#include  "species/speciestypes.h"

ldndc::event::__event_attrib_class_name(regrow)::__event_attrib_class_name(regrow)()
    : event_attribute_t(),
    name( ldndc::species::SPECIES_NAME_NONE),
    height_max( invalid_dbl), tree_number( invalid_dbl),
    tree_number_resize_factor( invalid_dbl),
    export_aboveground_biomass( 1.0)
{ }

ldndc::event::__event_attrib_class_name(regrow)::~__event_attrib_class_name(regrow)()
{ }

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(regrow)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);
    
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"heightmax",height_max);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"treenumber",tree_number);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"treenumberresizefactor",tree_number_resize_factor);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"exportabovegroundbiomass",export_aboveground_biomass);
    return  event_as_text.str();
}

