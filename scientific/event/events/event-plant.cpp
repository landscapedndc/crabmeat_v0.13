/*!
 * @brief
 *      ...
 *
 * @author
 *    Steffen Klatt (created on: dec 10, 2011),
 *    Edwin Haas
 */

#include  "event/events/event-plant.h"
#include  "memory/cbm_mem.h"

ldndc::event::__event_attrib_class_name(plant)::__event_attrib_class_name(plant)()
        : event_attribute_t(),
          group( species::SPECIES_GROUP_NONE),
          type( species::SPECIES_NAME_NONE),
          name( species::SPECIES_NAME_NONE),
          location( "?"),
          properties( NULL),
          params( NULL)
{
    this->properties = &this->crop;
}
ldndc::event::__event_attrib_class_name(plant)::~__event_attrib_class_name(plant)()
{
    if ( this->params)
    {
        CBM_DefaultAllocator->destroy( this->params);
    }
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(plant)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"type",type);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);
    LEVENT_WRITE_ATTRIB_TXT_(event_as_text,"group","",species::SPECIES_GROUP_NAMES[this->attribs_->group]);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"location",location);

    switch ( this->attribs_->group)
    {
        case  species::SPECIES_GROUP_CROP:
        {
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"initialbiomass",crop.initial_biomass);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"fractionalcover",crop.fractional_cover);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"covercrop",crop.cover_crop);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"seedlingnumber",crop.seedling_number);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"seedbedduration",crop.seedbed_duration);

            break;
        }
        case  species::SPECIES_GROUP_GRASS:
        {
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"initialbiomass",grass.initial_biomass);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"fractionalcover",grass.fractional_cover);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"covercrop",grass.cover_crop);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"rootingdepth",grass.root_depth);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"heightmax",grass.height_max);

            break;
        }
        case  species::SPECIES_GROUP_WOOD:
        {
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"initialbiomass",wood.initial_biomass);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"fractionalcover",wood.fractional_cover);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"rootingdepth",wood.root_depth);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"heightmax",wood.height_max);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"heightmin",wood.height_min);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"creductionfactor",wood.reduc_fac_c);

            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"dbh",wood.dbh);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"treenumber",wood.number);
            LEVENT_WRITE_ATTRIB_TXT(event_as_text,"treevolume",wood.volume);

            break;
        }
        default:
        {
            /* group could not be deduced (implies no attributes were given) */
        }
    }

    return  event_as_text.str();
}


bool
ldndc::event::__event_class_name(plant)::speciesparameters_given()
const
{
    return  this->attribs_->params != NULL;
}
ldndc::speciesparameters::speciesparameters_set_t
ldndc::event::__event_class_name(plant)::params()
const
{
    speciesparameters::speciesparameters_meta_t  m( this->species_type(), NULL, this->group());
    speciesparameters::speciesparameters_set_t  p( &m);

    if ( this->attribs_->params)
    {
        p.copy_valid( this->attribs_->params->p);
    }

    return  p;
}

