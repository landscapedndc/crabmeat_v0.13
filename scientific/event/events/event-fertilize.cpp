/*!
 * @file
 * @author
 *    Steffen Klatt (created on: dec 10, 2011),
 *    Edwin hHaas
 */

#include  "event/events/event-fertilize.h"

ldndc::event::__event_attrib_class_name(fertilize)::__event_attrib_class_name(fertilize)()
    : event_attribute_t(),
      type( invalid_string),
      location( invalid_string),
      amount( 0.0),
      ni_amount( 0.0),
      ui_amount( 0.0),
      depth( 0.0)
{
}

ldndc::event::__event_attrib_class_name(fertilize)::~__event_attrib_class_name(fertilize)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(fertilize)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"type",type);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"location",location);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"amount",amount);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"ni_amount",ni_amount);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"ui_amount",ui_amount);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"depth",depth);

    return  event_as_text.str();
}
