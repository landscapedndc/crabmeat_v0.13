/*!
 * @brief
 *    event "graze" declaration
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_GRAZE_H_
#define  LDNDC_INPUT_EVENT_GRAZE_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  graze

namespace  ldndc{ namespace  event{

struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    enum  livestock_type_e
    {
        LIVESTOCK_CATTLE,
        LIVESTOCK_HORSE,
        LIVESTOCK_SHEEP,

        LIVESTOCK_CNT,
        LIVESTOCK_USERDEFINED,
        LIVESTOCK_NONE
    };
    static char const *  LIVESTOCK_NAMES[LIVESTOCK_CNT];

    struct  livestock_info_t
    {
        livestock_info_t();
        static livestock_info_t  properties_from_type(
                livestock_type_e);

        /*! food demand  [kg C/day] */
        double  demand_carbon;

        /*! carbon in excreta  [kg C/head/day] */
        double  dung_carbon;
        /*! nitrogen in excreta  [kg N/head/day]*/
        double  dung_nitrogen;
        /*! nitrogen in excreta in form of urine  [kg N/head/day] */
        double  urine_nitrogen;
    };
    static livestock_info_t const  LIVESTOCK_DEFAULTS[LIVESTOCK_CNT];

    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! predefined or user-defined livestock type */
    livestock_type_e  type;

    /*! livestock type parameterization */
    livestock_info_t  properties;

    /*! quantity of livestock  [head ha-1] */
    double  head_count;
    /*! time the livestock is grazing  [hours] */
    double  grazing_hours;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        attributes_type::livestock_type_e  livestock_type() const { return  this->attribs_->type; }
        char const *  livestock_name()
        const
        {
            if ( this->livestock_type() < attributes_type::LIVESTOCK_CNT)
            {
                return  attributes_type::LIVESTOCK_NAMES[this->livestock_type()];
            }
            return  invalid_str;
        }
        attributes_type::livestock_info_t const &  livestock_properties() const
            { return  this->attribs_->properties; }

        double  head_count() const
            { return  this->attribs_->head_count; }
        bool  have_livestock() const
            { return  this->head_count() > 0.0; }
        double  grazing_hours() const
            { return  this->attribs_->grazing_hours; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_GRAZE_H_  */

