/*!
 * @brief
 *    event "manure" declaration
 *
 * @author
 *  - Steffen Klatt (created on: dec 10, 2011),
 *  - Edwin Haas
 *  - David Kraus
 */

#ifndef  LDNDC_INPUT_EVENT_MANURE_H_
#define  LDNDC_INPUT_EVENT_MANURE_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  manure

namespace  ldndc{
namespace  event{

/*!
 * @page event_input
 * @section event_input_manure Manure event
 * The following event attributes can be defined:
 * - type
 * - depth
 * - c
 * - cn
 * - availc
 * - availn
 * - ph
 * - volume
 * - nh4fraction
 * - no3fraction
 * - ureafraction
 * - donfraction
 * - cellulosefraction
 * - ligninfraction
 */
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! manure type (determined from name, see @p MANURE_NAMES) */
    cbm::string_t  type;

    /*! determines which soil layers are affected [m] */
    double  depth;

    /*! amount of carbon [kg C ha-1] */
    double  c;

    /*! C/N ratio [-] */
    double  cn;

    /*! available dissolved carbon [-] (does not apply to all manure types) */
    double  avail_c;

    /*! available dissolved nitrogen [-] (does not apply to all manure types) */
    double  avail_n;

    /*! ph value */
    double  ph;

    /*! liquid volume */
    double volume;

    /*!fraction of nh4 of dissolved nitrogen [-] (does not apply to all manure types) */
    double  nh4_fraction;

    /*!fraction of no3 of dissolved nitrogen [-] (does not apply to all manure types) */
    double  no3_fraction;

    /*!fraction of urea of dissolved nitrogen [-] (does not apply to all manure types) */
    double  urea_fraction;

    /*!fraction of don of dissolved nitrogen [-] (does not apply to all manure types) */
    double  don_fraction;

    /*!fraction of cellulose of non-dissolved carbon [-] (does not apply to all manure types) */
    double  cellulose_fraction;

    /*!fraction of lignin of non-dissolved carbon [-] (does not apply to all manure types) */
    double  lignin_fraction;
};

class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        cbm::string_t const &  type() const
            { return  this->attribs_->type; }

        double  depth() const
            { return  this->attribs_->depth; }
        double  c() const
            { return  this->attribs_->c; }
        double  cn() const
            { return  this->attribs_->cn; }
        double  avail_c() const
            { return  this->attribs_->avail_c; }
        double  avail_n() const
            { return  this->attribs_->avail_n; }
        double  ph() const
            { return  this->attribs_->ph; }
        double  volume() const
            { return  this->attribs_->volume; }
        double  nh4_fraction() const
            { return  this->attribs_->nh4_fraction; }
        double  no3_fraction() const
            { return  this->attribs_->no3_fraction; }
        double  urea_fraction() const
            { return  this->attribs_->urea_fraction; }
        double  don_fraction() const
            { return  this->attribs_->don_fraction; }
        double  cellulose_fraction() const
            { return  this->attribs_->cellulose_fraction; }
        double  lignin_fraction() const
            { return  this->attribs_->lignin_fraction; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_MANURE_H_  */

