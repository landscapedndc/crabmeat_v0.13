/*!
 * @brief
 *    event "cut" declaration
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 *    David Kraus
 * @date
 *    Dec 10, 2011
 */

#ifndef  LDNDC_INPUT_EVENT_CUT_H_
#define  LDNDC_INPUT_EVENT_CUT_H_

#include  "event/events/eventbase.h"
#include  "species/speciestypes.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  cut

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*!
     * @brief
     *    name of species to be cut. this serves as a
     *    key to find the corresponding species (see plant
     *    event)
     */
    std::string  name;

    /*!
     * @brief
     *      Height of stubble remaining on field in [m]
     */
    double  height;

    /*!
     * @brief
     *      Relative mount of biomass remaining on field given as relative value [-]
     */
    double  remains_relative;

    /*!
     * @brief
     *      Amount of total biomass remaining on field given as absolute biomass [kgC m-2]
     */
    double  remains_absolute;

    /*!
     * @brief
     *      Amount of fruit biomass remaining on field [kgC m-2]
     */
    double  remains_absolute_fruit;

    /*!
     * @brief
     *      Amount of foliage biomass remaining on field [kgC m-2]
     */
    double  remains_absolute_foliage;

    /*!
     * @brief
     *      Amount of living structural tissue biomass remaining on field [kgC m-2]
     */
    double  remains_absolute_living_structural_tissue;

    /*!
     * @brief
     *      Amount of dead structural tissue biomass remaining on field [kgC m-2]
     */
    double  remains_absolute_dead_structural_tissue;

    /*!
     * @brief
     *      Amount of root biomass remaining on field [kgC m-2]
     */
    double  remains_absolute_root;

    /*!
     * @brief
     *      Export fraction of fruit biomass [0-1]
     */
    double  export_fruit;

    /*!
     * @brief
     *      Export fraction of foliage biomass [0-1]
     */
    double  export_foliage;

    /*!
     * @brief
     *      Export fraction of living structural tissue biomass [0-1]
     */
    double  export_living_structural_tissue;

    /*!
     * @brief
     *      Export fraction of dead structural tissue biomass [0-1]
     */
    double  export_dead_structural_tissue;

    /*!
     * @brief
     *      Export fraction of root biomass [0-1]
     */
    double  export_root;

    bool mulching;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:

    char const *  species_name() const
        { return  this->attribs_->name.c_str(); }

    double  height() const
        { return this->attribs_->height; }

    double  remains_relative() const
        { return this->attribs_->remains_relative; }
    double  remains_absolute() const
        { return this->attribs_->remains_absolute; }

    double  remains_absolute_fruit() const
        { return  this->attribs_->remains_absolute_fruit; }
    double  remains_absolute_foliage() const
        { return  this->attribs_->remains_absolute_foliage; }
    double  remains_absolute_living_structural_tissue() const
        { return  this->attribs_->remains_absolute_living_structural_tissue; }
    double  remains_absolute_dead_structural_tissue() const
        { return  this->attribs_->remains_absolute_dead_structural_tissue; }
    double  remains_absolute_root() const
        { return  this->attribs_->remains_absolute_root; }

    double  export_fruit() const
        { return  this->attribs_->export_fruit; }
    double  export_foliage() const
        { return  this->attribs_->export_foliage; }
    double  export_living_structural_tissue() const
        { return  this->attribs_->export_living_structural_tissue; }
    double  export_dead_structural_tissue() const
        { return  this->attribs_->export_dead_structural_tissue; }
    double  export_root() const
        { return  this->attribs_->export_root; }

    bool  mulching() const
        { return  this->attribs_->mulching; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_CUT_H_  */

