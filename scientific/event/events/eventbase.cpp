/*!
 * @author
 *    steffen klatt (created on: dec 10, 2011)
 *
 * @todo
 *    add assertions
 */

#include  "event/events/eventbase.h"
namespace  ldndc{ namespace  event{
#include  "event.cpp.inc"
}}

ldndc::event::event_attribute_t::~event_attribute_t()
{ }


ldndc::event::Event::Event( lid_t const &  _id)
        : m_id( _id)
{ }
ldndc::event::Event::~Event()
{ }

