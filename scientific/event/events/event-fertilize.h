/*!
 * @brief
 *    Event "fertilize" declaration
 *
 * @author
 *   - Steffen Klatt (created on: dec 10, 2011),
 *   - Edwin Haas
 *   - David Kraus
 */

#ifndef  LDNDC_INPUT_EVENT_FERTILIZE_H_
#define  LDNDC_INPUT_EVENT_FERTILIZE_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  fertilize

namespace  ldndc{
namespace  event{

/*!
 * @page event_input
 * @section event_input_fertilize Fertilize event
 * The following event attributes can be defined:
 * - type
 * - location
 * - amount
 * - ni_amount
 * - ui_amount
 * - depth
 */
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! fertilizer type */
    cbm::string_t  type;

    /*! location of fertilizer application */
    cbm::string_t  location;

    /*! the amount of nitrogen fertilizer applied each time step [kg N ha-1] */
    double  amount;

    /*! the amount of nitrification inhibitor applied each time step [kg ha-1] */
    double  ni_amount;

    /*! the amount of urease inhibitor applied each time step [kg ha-1] */
    double  ui_amount;

    /*! determines which soil layers are affected [m] */
    double  depth;
};

class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        cbm::string_t const &  type() const
            { return this->attribs_->type; }

        cbm::string_t const &  location() const
            { return this->attribs_->location; }

        double  amount() const
            { return  this->attribs_->amount; }

        double  ni_amount() const
            { return  this->attribs_->ni_amount; }

        double  ui_amount() const
            { return  this->attribs_->ui_amount; }

        double  depth() const
            { return  this->attribs_->depth; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_FERTILIZE_H_  */
