/*!
 * @brief
 *    event "throw" ((un)natural mortality) declaration
 *
 * @author
 *    steffen klatt (created on: apr 29, 2013),
 *    ruediger grote,
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_THROW_H_
#define  LDNDC_INPUT_EVENT_THROW_H_

#include  "event/events/eventbase.h"

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  throw

namespace  ldndc{ namespace  event{
struct  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /** attributes **/

    /* species name (identifier) */
    std::string  name;

    /*! number of individuals per ha to be removed [% or -]*/
    double  reduction_number;
    /*! interpret reduction numberas as either
     *  percentage or absolute number (default: false, i.e., [%]) */
    bool  reduction_number_is_absolute;
    /*! stemwood (or stand) volume to be removed [% or m3/ha]*/
    double  reduction_volume;
    /*! interpret reduction volume as either
     *  percentage or absolute volume (default: false, i.e., [%]) */
    bool  reduction_volume_is_absolute;

    /* specify reason of uprooting, breakage, etc.. */
    std::string  reason;
};
class  __event_class_name(__this_event_name)  :  public  Event
{
    /* default constructor, event attribute pointer, ... */
    EVENT_COMMON_DECL(__this_event_name)

    public:
        char const *  species_name() const
            { return  this->attribs_->name.c_str(); }

        double  reduction_number() const
            { return  this->attribs_->reduction_number; }
        bool  reduction_number_is_absolute() const
            { return  this->attribs_->reduction_number_is_absolute; }
        double  reduction_volume() const
            { return  this->attribs_->reduction_volume; }
        bool  reduction_volume_is_absolute() const
            { return  this->attribs_->reduction_volume_is_absolute; }

        std::string const &  reason() const
            { return  this->attribs_->reason; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_THROW_H_  */

