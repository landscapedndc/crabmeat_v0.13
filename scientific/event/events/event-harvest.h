/*!
 * @brief
 *    event "harvest" declaration
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 *    David Kraus
 * @date
 *    Dec 10, 2011
 */

#ifndef  LDNDC_INPUT_EVENT_HARVEST_H_
#define  LDNDC_INPUT_EVENT_HARVEST_H_

#include  "event/events/eventbase.h"
#include  "species/speciestypes.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  harvest

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*!
     * @brief
     *    name of species to be harvested. this serves as a
     *    key to find the corresponding species (see plant
     *    event)
     */
    std::string  name;

    /*!
     * @brief
     *      Height of stubble remaining on field in [m]
     */
    double  height;

    /*!
     * @brief
     *      Relative mount of biomass remaining on field given as relative value [-]
     */
    double  remains_relative;

    /*!
     * @brief
     *      Amount of total biomass remaining on field given as absolute biomass [kgC m-2]
     */
    double  remains_absolute;

    /*!
     * @brief
     *      Export fraction of stem wood biomass [0-1]
     */
    double  export_stem_wood;

    /*!
     * @brief
     *      Export fraction of branch wood biomass [0-1]
     */
    double  export_branch_wood;

    /*!
     * @brief
     *      Export fraction of root wood biomass [0-1]
     */
    double  export_root_wood;

    bool mulching;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:

    char const *  species_name() const
        { return  this->attribs_->name.c_str(); }

    double  height() const
        { return this->attribs_->height; }

    double  remains_relative() const
        { return this->attribs_->remains_relative; }
    double  remains_absolute() const
        { return this->attribs_->remains_absolute; }

    double  export_stem_wood() const
        { return  this->attribs_->export_stem_wood; }
    double  export_branch_wood() const
        { return  this->attribs_->export_branch_wood; }
    double  export_root_wood() const
        { return  this->attribs_->export_root_wood; }

    bool  mulching() const
        { return  this->attribs_->mulching; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_HARVEST_H_  */

