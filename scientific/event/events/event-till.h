/*!
 * @brief
 *    event "till" declaration
 *
 * @author
 *    Steffen Klatt (created on: dec 10, 2011),
 *    Edwin Haas
 *    David Kraus
 */

#ifndef  LDNDC_INPUT_EVENT_TILL_H_
#define  LDNDC_INPUT_EVENT_TILL_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  till

namespace  ldndc{
namespace  event{

/*!
 * @page event_input
 * @section event_input_till Tilling event
 * The following event attributes can be defined:
 * - depth
 */
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! tilling depth (determined by tilling method used) [m] */
    double  depth;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        double  depth() const
            { return  this->attribs_->depth; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_TILL_H_  */

