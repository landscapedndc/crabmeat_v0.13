/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#include  "event/events/event-till.h"


ldndc::event::__event_attrib_class_name(till)::__event_attrib_class_name(till)()
        : event_attribute_t(), depth( 0.0)
{ }
ldndc::event::__event_attrib_class_name(till)::~__event_attrib_class_name(till)()
{ }

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(till)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"depth",depth);

    return  event_as_text.str();
}

