/*!
 * @brief
 *    Event "flood" declaration
 *
 * @author
 *  - Steffen Klatt (created on: dec 10, 2011),
 *  - Edwin Haas
 *  - David Kraus
 */

#ifndef  LDNDC_INPUT_EVENT_FLOOD_H_
#define  LDNDC_INPUT_EVENT_FLOOD_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  flood

namespace  ldndc{
namespace  event{

/*!
 * @page event_input
 * @section event_input_flood Flooding event
 * The following event attributes can be defined:
 * - watertable
 * - bundheight
 * - percolationrate
 * - irrigationheight
 * - irrigationamount
 * - unlimitedwater
 * - saturationlevel
 * - soildepth
 */
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! water table height */
    double  watertable;
    
    /*! bund height */
    double  bundheight;

    /*! percolation rate */
    double  percolationrate;

    /*! fixed height for irrigation water */
    double  irrigationheight;

    /*! fixed amount of irrigation water */
    double  irrigationamount;

    /*! irrigation water for flooding is magically available */
    double  unlimitedwater;

    /*! level of targeted saturation */
    double  saturationlevel;

    /*! depht of soil considered for management */
    double  soildepth;
};

class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        double  water_table_height() const
            { return  this->attribs_->watertable; }
        double  bund_height() const
            { return  this->attribs_->bundheight; }
        double  percolation_rate() const
            { return  this->attribs_->percolationrate; }
        double  irrigation_height() const
            { return  this->attribs_->irrigationheight; }
        double  irrigation_amount() const
            { return  this->attribs_->irrigationamount; }
        double  unlimited_water() const
            { return  this->attribs_->unlimitedwater; }
        double  saturation_level() const
            { return  this->attribs_->saturationlevel; }
        double  soil_depth() const
            { return  this->attribs_->soildepth; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_FLOOD_H_  */
