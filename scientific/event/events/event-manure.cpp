/*!
 * @brief
 *    ...
 *
 * @author
 *    Steffen Klatt (created on: dec 10, 2011),
 *    Edwin Haas
 *    David Kraus
 */

#include  "event/events/event-manure.h"

ldndc::event::__event_attrib_class_name(manure)::__event_attrib_class_name(manure)()
        : event_attribute_t(),
          type( invalid_string),
          depth( invalid_dbl),
          c( invalid_dbl), 
          cn( invalid_dbl),
          avail_c( invalid_dbl), 
          avail_n( invalid_dbl),
          ph( invalid_dbl),
          volume( invalid_dbl),
          nh4_fraction( invalid_dbl),
          no3_fraction( invalid_dbl),
          urea_fraction( invalid_dbl),
          don_fraction( invalid_dbl),
          cellulose_fraction( invalid_dbl),
          lignin_fraction( invalid_dbl)
{
}
ldndc::event::__event_attrib_class_name(manure)::~__event_attrib_class_name(manure)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(manure)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"type",type);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"depth",depth);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"c",c);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"cn",cn);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"availc",avail_c);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"availn",avail_n);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"ph",ph);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"volume",volume);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"nh4fraction",nh4_fraction);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"no3fraction",no3_fraction);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"ureafraction",urea_fraction);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"donfraction",don_fraction);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"cellulosefraction",cellulose_fraction);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"ligninfraction",lignin_fraction);
    return  event_as_text.str();
}

