/*!
 * @brief
 *    event "cut" declaration
 *
 * @author
 *    Steffen Klatt
 *    Edwin Haas
 *    David Kraus
 * @date
 *    Dec 10, 2011
 */

#include  "event/events/event-cut.h"

ldndc::event::__event_attrib_class_name(cut)::__event_attrib_class_name(cut)()
        : event_attribute_t(),

          name( ldndc::species::SPECIES_NAME_NONE),

          height( invalid_dbl),

          remains_relative( invalid_dbl),
          remains_absolute( invalid_dbl),

          remains_absolute_fruit( invalid_dbl),
          remains_absolute_foliage( invalid_dbl),
          remains_absolute_living_structural_tissue( invalid_dbl),
          remains_absolute_dead_structural_tissue( invalid_dbl),
          remains_absolute_root( invalid_dbl),

          export_fruit( invalid_dbl),
          export_foliage( invalid_dbl),
          export_living_structural_tissue( invalid_dbl),
          export_dead_structural_tissue( invalid_dbl),
          export_root( invalid_dbl),

          mulching( false)
{
}
ldndc::event::__event_attrib_class_name(cut)::~__event_attrib_class_name(cut)()
{
}

std::string
ldndc::event::__event_class_name(cut)::to_string()
const
{
    std::ostringstream  event_as_text;

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"height",height);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_relative",remains_relative);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute",remains_absolute);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute_fruit",remains_absolute_fruit);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute_foliage",remains_absolute_foliage);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute_living_structural_tissue",remains_absolute_living_structural_tissue);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute_dead_structural_tissue",remains_absolute_dead_structural_tissue);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"remains_absolute_root",remains_absolute_root);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"export_fruit",export_fruit);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"export_foliage",export_foliage);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"export_living_structural_tissue",export_living_structural_tissue);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"export_dead_structural_tissue",export_dead_structural_tissue);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"export_root",export_root);

    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"mulching",mulching);

    return  event_as_text.str();
}

