/*!
 * @brief
 *    event "fire" declaration
 *
 * @author
 *    steffen klatt (created on: jan 31, 2013),
 *    edwin haas
 */

#include  "event/events/event-fire.h"

ldndc::event::__event_attrib_class_name(fire)::__event_attrib_class_name(fire)()
        : event_attribute_t(),
          burned_area( invalid_dbl)
{
}
ldndc::event::__event_attrib_class_name(fire)::~__event_attrib_class_name(fire)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(fire)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"burnedarea",burned_area);

    return  event_as_text.str();
}

