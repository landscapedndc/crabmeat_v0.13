/*!
 * @brief
 *    event "checkpoint" declaration
 *
 * @author
 *    david kraus (created on: jun 23, 2014)
 */

#ifndef  LDNDC_INPUT_EVENT_CHECKPOINT_H_
#define  LDNDC_INPUT_EVENT_CHECKPOINT_H_

#include  "event/events/eventbase.h"
#include  "string/cbm_string.h"

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  checkpoint

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    enum  command_e
    {
        CHECKPOINT_COMMAND_CREATE,
        CHECKPOINT_COMMAND_RESTORE,

        CHECKPOINT_COMMAND_CNT
    };
    static char const *  CHECKPOINT_COMMAND_NAMES[CHECKPOINT_COMMAND_CNT];

    /*! checkpoint command: either "create" or "restore" */
    command_e  command;
    /*! options */
    cbm::string_t  options;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        char const *  command_name() const;
    public:
        bool  is_create() const
        {
            return  this->attribs_->command == 
                __event_attrib_class_name(__this_event_name)::CHECKPOINT_COMMAND_CREATE;
        }
        bool  is_restore() const
        {
            return  this->attribs_->command ==
                __event_attrib_class_name(__this_event_name)::CHECKPOINT_COMMAND_RESTORE;
        }

        bool  has_option(
                char const * /*option*/) const;
        cbm::string_t  get_option_argument(
                char const * /*option*/) const;
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_CHECKPOINT_H_  */

