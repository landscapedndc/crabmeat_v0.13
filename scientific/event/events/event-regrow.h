/*!
 * @brief
 *    event "regrow" declaration
 *
 * @author
 *    steffen klatt (created on: apr 29, 2013),
 *    ruediger grote,
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_REGROW_H_
#define  LDNDC_INPUT_EVENT_REGROW_H_

#include  "event/events/eventbase.h"

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  regrow

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /** attributes **/

    /* species name (identifier) */
    std::string  name;

    /*! upper height of vegetation (i.e. average height of highest vegetation cohort)  [m] */
    double  height_max;

    /*! reset number of trees */
    double  tree_number;
    
    /*! change number of individuals [0,..,inf] */
    double  tree_number_resize_factor;

    /*! define fraction of aboveground biomass export [0-1] */
    double export_aboveground_biomass;
};
    
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    /* default constructor, event attribute pointer, ... */
    EVENT_COMMON_DECL(__this_event_name)

    public:
        char const *  species_name() const
            { return  this->attribs_->name.c_str(); }
        double  tree_number() const
            { return  this->attribs_->tree_number; }
        double  tree_number_resize_factor() const
            { return  this->attribs_->tree_number_resize_factor; }
        double  height_max() const
            { return  this->attribs_->height_max; }
        double  export_aboveground_biomass() const
            { return  this->attribs_->export_aboveground_biomass; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_REGROW_H_  */

