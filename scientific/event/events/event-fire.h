/*!
 * @brief
 *    event "fire" declaration
 *
 * @author
 *    steffen klatt (created on: jan 31, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_FIRE_H_
#define  LDNDC_INPUT_EVENT_FIRE_H_

#include  "event/events/eventbase.h"

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  fire

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    double  burned_area;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
        /* default constructor, event attribute pointer, ... */
    EVENT_COMMON_DECL(__this_event_name)

    public:
        double  burned_area() const
            { return this->attribs_->burned_area; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_FIRE_H_  */

