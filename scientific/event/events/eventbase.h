/*!
 * @brief
 *    base class for (management) events
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011)
 */

#ifndef  LDNDC_INPUT_EVENTBASE_H_
#define  LDNDC_INPUT_EVENTBASE_H_

#include  "crabmeat-common.h"

#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"
#include  "memory/cbm_mem.h"

namespace  ldndc{ namespace  event{
#include  "event.h.inc"

struct  event_attribute_t
{
    virtual  ~event_attribute_t() = 0;
};

#define  LMANA_EVENT_TYPE(__ev_class__,__ev_attr_t__,__ev_type__) \
    public: \
        typedef  __ev_attr_t__  attributes_type; \
        enum \
        { \
            event_type_ = __ev_type__ \
        }; \
        event_type_e  event_type() \
        const \
        { \
            return  static_cast< event_type_e >( event_type_); \
        } \
        char const *  name() const \
            { return  EVENT_NAMES[event_type_]; } \
        std::string  to_string() const;                        \
    public: \
        __ev_class__( \
            lid_t const &  _id, __ev_attr_t__ const *  _attribs) \
            : Event( _id), attribs_( _attribs) { } \
        ~__ev_class__() \
        { \
            if ( attribs_) \
            { \
                CBM_DefaultAllocator->destroy( attribs_); \
            } \
        } \
    private: \
        __ev_attr_t__ const *  attribs_; \
        /* hide copy constructor and assignment operator */ \
        __ev_class__( __ev_class__ const &); \
        __ev_class__ &  operator=( __ev_class__ const &);


#define  EVENT_COMMON_DECL(__ev_name__) \
    LMANA_EVENT_TYPE(__event_class_name(__ev_name__),__event_attrib_class_name(__ev_name__),TOKENPASTE(__EVENT_ENUM_,__ev_name__))

#define  LEVENT_WRITE_ATTRIB_TXT_(__ss__,__key__,__value_prefix__,__value__) \
    /*if ( cbm::is_valid( __value__))*/ \
    { \
        __ss__     << ((cbm::is_empty( (__ss__).str())) ? "" : "; ") \
            << __key__ \
            << " = " \
            << "\"" << __value_prefix__ << __value__ << "\""; \
    } \
    CRABMEAT_FIX_UNUSED(__ss__)/*;*/

#define  LEVENT_WRITE_ATTRIB_TXT(__ss__,__key__,__value__) \
    LEVENT_WRITE_ATTRIB_TXT_(__ss__,__key__,"",this->attribs_->__value__)
#define  LEVENT_WRITE_ATTRIB_PREFIX_TXT(__ss__,__key__,__value_prefix__,__value__) \
    LEVENT_WRITE_ATTRIB_TXT_(__ss__,__key__,__value_prefix__,this->attribs_->__value__)

/* ldndc event base class */
class  CBM_API  Event
{
    public:
        enum  lmana_flag_e
        {
            EVFLAG_NONE        = 0u,
            EVFLAG_            = 1u << 0
        };
    public:
        virtual  event_type_e  event_type() const = 0;
        virtual char const *  name() const = 0;

    public:
        Event( lid_t const &);
        virtual  ~Event() = 0;

        lid_t const &  id()
            const { return  m_id; }

        virtual  std::string  to_string() const = 0;

    private:
        lid_t  m_id;

        Event( Event const &);
        Event &  operator=( Event const &);
};

#define  __event_class_name(__event_name__)        TOKENPASTE(Event__,__event_name__)
#define  __event_attrib_class_name(__event_name__)    TOKENPASTE3(event_,__event_name__,_attribute_t)


#define  EVENT_FACTORY_DECL(__event_name__)                                                        \
    /* factory */                                                                    \
    extern  event_factory_t< __event_class_name(__event_name__), __event_attrib_class_name(__event_name__) > const  TOKENPASTE(event_factory_,__event_name__);\
    /* default attributes */                                                            \
    extern  __event_attrib_class_name(__event_name__) const  TOKENPASTE(event_attribute_default_,__event_name__);

#define  EVENT_FACTORY_DEFN(__event_name__)                            \
    /* factory */                                        \
    event_factory_t< __event_class_name(__event_name__), __event_attrib_class_name(__event_name__) > const  TOKENPASTE(event_factory_,__event_name__);\
    /* default attributes */                                                            \
    __event_attrib_class_name(__event_name__) const  TOKENPASTE(event_attribute_default_,__event_name__);

#include  "eventbase.h.inc"

}}

#endif  /*  !LDNDC_INPUT_EVENTBASE_H_  */

