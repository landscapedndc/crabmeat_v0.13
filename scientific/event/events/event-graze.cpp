/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#include  "event/events/event-graze.h"

char const *  ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_NAMES[ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_CNT] =
{
    "cattle", "horse", "sheep"
};
ldndc::event::__event_attrib_class_name(graze)::livestock_info_t const
    ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_DEFAULTS[ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_CNT] =
{
    ldndc::event::__event_attrib_class_name(graze)::livestock_info_t::properties_from_type( 
            ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_CATTLE),
    ldndc::event::__event_attrib_class_name(graze)::livestock_info_t::properties_from_type( 
            ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_HORSE),
    ldndc::event::__event_attrib_class_name(graze)::livestock_info_t::properties_from_type( 
            ldndc::event::__event_attrib_class_name(graze)::LIVESTOCK_SHEEP)
};
ldndc::event::__event_attrib_class_name(graze)::livestock_info_t
ldndc::event::__event_attrib_class_name(graze)::livestock_info_t::properties_from_type(
        livestock_type_e  _type)
{
    livestock_info_t  animal;
    switch ( _type)
    {
        case LIVESTOCK_CATTLE:
        {
            animal.demand_carbon = 4.6;
            animal.dung_carbon = 1.44;
            animal.dung_nitrogen = 0.4*0.16;
            animal.urine_nitrogen = 0.6*0.16;
            break;
        }
        case LIVESTOCK_HORSE:
        {
            animal.demand_carbon = 3.0;
            animal.dung_carbon = 1.35;
            animal.dung_nitrogen = 0.4*0.11;
            animal.urine_nitrogen = 0.6*0.11;
            break;
        }
        case LIVESTOCK_SHEEP:
        {
            animal.demand_carbon = 0.88;
            animal.dung_carbon = 0.277;
            animal.dung_nitrogen = 0.4*0.025;
            animal.urine_nitrogen = 0.6*0.025;
            break;
        }

        default:
        {
            /* no op */
        }
    }

    return  animal;
}

ldndc::event::__event_attrib_class_name(graze)::livestock_info_t::livestock_info_t()
        : demand_carbon( invalid_dbl),
          dung_carbon( invalid_dbl), dung_nitrogen( invalid_dbl),
          urine_nitrogen( invalid_dbl)
{
}
ldndc::event::__event_attrib_class_name(graze)::__event_attrib_class_name(graze)()
        : event_attribute_t(), type( LIVESTOCK_NONE),
          head_count( 0.0), grazing_hours( 0.0)
{
}
ldndc::event::__event_attrib_class_name(graze)::~__event_attrib_class_name(graze)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(graze)::to_string()
const
{
    std::ostringstream  event_as_text;

    LEVENT_WRITE_ATTRIB_TXT_(event_as_text,"livestock","", this->livestock_name());
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"headcount",head_count);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"grazinghours",grazing_hours);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"dungcarbon",properties.dung_carbon);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"dungnitrogen",properties.dung_nitrogen);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"urinenitrogen",properties.urine_nitrogen);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"demandcarbon",properties.demand_carbon);

    return  event_as_text.str();
}

