/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: nov 06, 2013),
 *    edwin haas
 */

#include  "event/events/event-reparameterizespecies.h"

ldndc::event::__event_attrib_class_name(reparameterizespecies)::__event_attrib_class_name(reparameterizespecies)()
        : event_attribute_t(),
          type( ldndc::species::SPECIES_NAME_NONE),
          name( ldndc::species::SPECIES_NAME_NONE)
{
}
ldndc::event::__event_attrib_class_name(reparameterizespecies)::~__event_attrib_class_name(reparameterizespecies)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(reparameterizespecies)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"name",name);
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"type",type);

    return  event_as_text.str();
}

