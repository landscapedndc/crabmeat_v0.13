/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#include  "event/events/event-irrigate.h"

ldndc::event::__event_attrib_class_name(irrigate)::__event_attrib_class_name(irrigate)()
        : event_attribute_t(),
          amount( 0.0),
          reservoir( false)/*,
          ph( invalid_dbl)*/
{
}
ldndc::event::__event_attrib_class_name(irrigate)::~__event_attrib_class_name(irrigate)()
{
}

#include  "string/cbm_string.h"
std::string
ldndc::event::__event_class_name(irrigate)::to_string()
const
{
    std::ostringstream  event_as_text;
    LEVENT_WRITE_ATTRIB_TXT(event_as_text,"amount",amount);
 
    return  event_as_text.str();
}

