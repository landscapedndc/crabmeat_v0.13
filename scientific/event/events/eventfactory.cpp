/*!
 * @brief
 *    put factories into factory array
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011)
 *
 * @todo
 *    add assertions
 */

#include  "event/events/eventfactory.h"
#include  "event/events.h"

ldndc::event::abstract_event_factory_t::~abstract_event_factory_t()
{
}

/* autogenerated factories array */
namespace  ldndc{ namespace  event{
#include  "eventfactories.cpp.inc"
}}

