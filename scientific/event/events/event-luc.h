/*!
 * @brief
 *    event "LUC" declaration
 *
 * @author
 *    steffen klatt (created on: dec 10, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_LUC_H_
#define  LDNDC_INPUT_EVENT_LUC_H_

#include  "event/events/eventbase.h"


#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  luc

namespace  ldndc{ namespace  event{
struct  CBM_API  __event_attrib_class_name(__this_event_name)  :  event_attribute_t
{
    enum  command_e
    {
        LUC_MODULE_ADD,
        LUC_MODULE_DEL,
        LUC_MODULE_REPLACE,
        LUC_MODULE_SLEEP,
        LUC_MODULE_WAKE,

        LUC_COMMAND_CNT,
        LUC_COMMAND_NONE
    };
    static char const *  COMMAND_NAMES[LUC_COMMAND_CNT+2];
    static char const *  LUC_REPLACE_DELIM;
    static char const    LUC_ARGS_DELIM;

    __event_attrib_class_name(__this_event_name)();
    ~__event_attrib_class_name(__this_event_name)();

    /*! command that is performed on module (selection) by this event */
    command_e  command;
    /*! event command arguments */
    std::string  args;
};
class  CBM_API  __event_class_name(__this_event_name)  :  public  Event
{
    EVENT_COMMON_DECL(__this_event_name)

    public:
        __event_attrib_class_name(__this_event_name)::command_e  command() const
            { return  this->attribs_->command; }

        std::string const &  args() const
            { return  this->attribs_->args; }
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_LUC_H_  */

