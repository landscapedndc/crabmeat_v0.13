/*!
 * @brief
 *    common types related to setup input
 *
 * @author
 *    steffen klatt (created on: sep 03, 2013),
 *    edwin haas
 */

#include  "setup/setuptypes.h"
#include  "string/cbm_string.h"

namespace  ldndc{ namespace  setup
{
setup_info_t const  setup_info_defaults =
{
    0.0 /* x  [user defined] */,
    0.0 /* y  [user defined] */,
    0.0 /* z  [user defined] */,

    1.0 /* dx  [user defined] */,
    1.0 /* dy  [user defined] */,
    1.0 /* dz  [user defined] */,

    1.0 /* area  [m^2] */,
    1.0 /* volume  [m^3] */,

    40 /* canopylayers */,
    -1 /* soillayers */,
    
    100.0 /* elevation  [m] */,
    45.0 /* latitude  [degree] */,
    10.0 /* longitude  [degree] */,

    1  /* timezone  [-] */,
    0.0  /* aspect  [?] */,
    0.0  /* slope  [?] */,
};

} }

