/*!
 * @brief
 *    declare common types related to setup input
 *
 *    @n
 *
 *    for now, also pulls in all kernel specific input
 *    declarations
 *
 * @author
 *    steffen klatt (created on: aug 11, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SETUP_TYPES_H_
#define  LDNDC_INPUT_SETUP_TYPES_H_

#include  "crabmeat-common.h"
#include  "geom/geom-orientation.h"

namespace  ldndc{ namespace setup
{
/*!
 * @brief
 *    setup defaults
 */
struct  CBM_API  setup_info_t
{
    /*! centroid [m] */
    double  x, y, z;
    /*! centroid [m] */
    double  dx, dy, dz;
    /*! area  [m^2] */
    double  area;
    /*! volume  [m^3] */
    double  volume;

    /*! number of canopylayers  [-] */
    int  canopylayers;
    /*! number of soillayers  [-] */
    int  soillayers;
    
    /*! elevation  [m] */
    double  elevation;
    /*! latitude  [degree] */
    double  latitude;
    /*! longitude  [degree] */
    double  longitude;

    /*! GMT timezone   [?] */
    int  timezone;
    /*! aspect  [degree] */
    double  aspect;
    /*! slope  [degree] */
    double  slope;
};

extern  setup_info_t CBM_API const  setup_info_defaults;

}}

#endif  /*  !LDNDC_INPUT_SETUP_TYPES_H_  */

