/*!
 * @brief
 *    declare common types related to climate input
 *
 * @author
 *    steffen klatt (created on: aug 13, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_CLIMATE_TYPES_H_
#define  LDNDC_INPUT_CLIMATE_TYPES_H_

#include  "crabmeat-common.h"

#include  "datafilters/filter-copy.h"
#include  "datafilters/filter-minimum.h"
#include  "datafilters/filter-maximum.h"
#include  "datafilters/filter-average.h"
#include  "datafilters/filter-average-nonzero.h"
#include  "datafilters/filter-sum.h"

namespace  ldndc{ namespace  climate
{
/*!
 * @brief
 *    climate station meta data
 */
struct  CBM_API  climate_info_t
{
    /*! climate station's elevation  [m] */
    double  elevation;
    /*! climate station's latitude  [degree] */
    double  latitude;
    /*! climate station's longitude  [degree] */
    double  longitude;

    /*! average cloudiness  [%] */
    double  cloudiness;
    /*! average rainfall intensity  [%] */
    double  rainfall_intensity;
    /*! average wind speed  [m/s] */
    double  windspeed;

    /*! annual average temperature  [oC] */
    double  temp;
    /*! annual average temperature amplitude  [oC] */
    double  temp_amplitude;
    /*! annual average precipitation  [mm] */
    double  precip_sum;
};
#define  CLIMATE_INFO_ITEM_CNT  (9)
extern CBM_API ldndc_string_t const  CLIMATE_INFO_ITEM_IDS[CLIMATE_INFO_ITEM_CNT];
extern  climate_info_t CBM_API const  climate_info_defaults;


struct  CBM_API  record
{
    /* climate record item datatype */
    typedef  double  item_type;

    enum  record_item_e
    {
        /*! air pressure  [mbar] */
        RECORD_ITEM_AIR_PRESSURE,
        /*! extraterrestrial radiation  [Wm-2] */
        RECORD_ITEM_EX_RAD,
        /*! long wave radiation  [Wm-2] */
        RECORD_ITEM_LONGWAVE_RAD,
        /*! precipitation  [mm] */
        RECORD_ITEM_PRECIP,
        /*! average temperature  [oC] */
        RECORD_ITEM_TEMP_AVG,
        /*! maximum temperature  [oC] */
        RECORD_ITEM_TEMP_MAX,
        /*! minimum temperature  [oC] */
        RECORD_ITEM_TEMP_MIN,
        /*! relative humidity  [%] */
        RECORD_ITEM_REL_HUMUDITY,
        /*! vapor pressure deficit  [kPa] */
        RECORD_ITEM_VPD,
        /*! wind speed  [ms-1] */
        RECORD_ITEM_WIND_SPEED,

        RECORD_SIZE
    };
};

extern CBM_API ldndc_string_t const  RECORD_ITEM_UNITS[record::RECORD_SIZE];

// sk:TODO  remove char const * versions
extern CBM_API char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE];
extern CBM_API ldndc_string_t const  RECORD_ITEM_IDS[record::RECORD_SIZE];
extern CBM_API ldndc_string_t const  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE];

struct  CBM_API  streamdata_info_t
{
    enum { RECORD_SIZE = record::RECORD_SIZE };
    enum { BUFFER_SIZE_LOG = _CONFIG_BUFFERSIZE_LOG_CLIMATE };

    typedef  record::record_item_e  record_item_e;
    typedef  record::item_type  element_type;

    typedef  climate_info_t  boundary_data_type;
};


extern  data_filter_copy_t< record::item_type > CBM_API const  climate_datafilter_copy;
extern  data_filter_maximum_t< record::item_type > CBM_API const  climate_datafilter_maximum;
extern  data_filter_minimum_t< record::item_type > CBM_API const  climate_datafilter_minimum;
extern  data_filter_average_t< record::item_type > CBM_API const  climate_datafilter_average;
extern  data_filter_average_nonzero_t< record::item_type > CBM_API const  climate_datafilter_average_nonzero;
extern  data_filter_sum_t< record::item_type > CBM_API const  climate_datafilter_sum;

extern  data_filter_t< record::item_type > CBM_API const *  CLIMATE_DATAFILTER_LIST[record::RECORD_SIZE];

}}

#endif  /*  !LDNDC_INPUT_CLIMATE_TYPES_H_  */

