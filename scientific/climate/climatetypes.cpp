/*!
 * @author
 *    steffen klatt (created on: dec 16, 2012),
 *    edwin haas
 */

#include  "climate/climatetypes.h"

namespace  ldndc{ namespace  climate
{

ldndc_string_t const  CLIMATE_INFO_ITEM_IDS[CLIMATE_INFO_ITEM_CNT] =
{
        "elevation",
        "latitude",
        "longitude",

        "cloudiness",
        "rainfallintensity",
        "windspeed",

        "temperature",
        "temperatureamplitude",
        "precipitationsum"
};

climate_info_t const  climate_info_defaults =
{
        100.0 /*elevation*/,
        45.0 /*latitude*/,
        10.0 /*longitude*/,

        0.55 /*cloudiness*/,
        5.0 /*rain fall intensity*/,
        1.5 /*wind speed*/,

        8.0 /*temperature*/,
        20.0 /*temperature amplitude*/,
        800.0 /*precipitation sum*/
};

ldndc_string_t const  RECORD_ITEM_UNITS[record::RECORD_SIZE] =
{
    "bar:10^-3", "W:m^-2", "W:m^-2", "mm:m^-2", "oC", "oC", "oC", "%", "Pa:10^3", "m:s^-1"
};

char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE] =
{
    /* double */    "press", "grad", "lrad", "prec", "tavg", "tmax", "tmin", "rh", "vpd", "wind"
};
ldndc_string_t const  RECORD_ITEM_IDS[record::RECORD_SIZE] =
{
    /* double */    "press", "grad", "lrad", "prec", "tavg", "tmax", "tmin", "rh", "vpd", "wind"
};
ldndc_string_t const  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE] =
{
    "air pressure", "global radiation", "longwave radiation", "precipitation", "average temperature", "maximum temperature", "minimum temperature", "relative humidity", "vapor pressure deficit", "wind speed"
};


data_filter_copy_t< record::item_type > const  climate_datafilter_copy;
data_filter_maximum_t< record::item_type > const  climate_datafilter_maximum;
data_filter_minimum_t< record::item_type > const  climate_datafilter_minimum;
data_filter_average_t< record::item_type > const  climate_datafilter_average;
data_filter_average_nonzero_t< record::item_type > const  climate_datafilter_average_nonzero;
data_filter_sum_t< record::item_type > const  climate_datafilter_sum;


data_filter_t< record::item_type > const *  CLIMATE_DATAFILTER_LIST[record::RECORD_SIZE] =
{
    &climate_datafilter_average, /* air pressure  */
    &climate_datafilter_average_nonzero, /* extraterrestrial radiation */
    &climate_datafilter_average_nonzero, /* long wave radiation */
    &climate_datafilter_sum, /* precipitation */
    &climate_datafilter_average, /* average temperature */
    &climate_datafilter_maximum, /* maximum temperature */
    &climate_datafilter_minimum, /* minimum temperature */
    &climate_datafilter_average,/* relative humidity */
    &climate_datafilter_average, /* vapor pressure deficit */
    &climate_datafilter_average /* wind speed */
};


}}

