/*!
 * @brief
 *    Defines a list of supported ecosystems
 *
 * @author
 *    Steffen Klatt  (created on: jan 11, 2012),
 *    Edwin Haas
 */

#include  "ecosystemtypes.h"

const char *  ECOSYSTEM_NAMES[ECOSYS_CNT + 1/*sentinel*/] =
{
    "none",

    "arable", "arable_lowland",
    "forest", "forest_natural",
    "grassland",
    "wetland",

    NULL /*sentinel*/
};


cn_ratiolevel_e const ECOSYSTEM_CN_RATIO_LEVELS[ECOSYS_CNT] = 
{
    CNRATIO_LEVEL_MEDIUM,

    CNRATIO_LEVEL_LOW, CNRATIO_LEVEL_LOW,
    CNRATIO_LEVEL_HIGH, CNRATIO_LEVEL_HIGH,
    CNRATIO_LEVEL_LOW,
    CNRATIO_LEVEL_HIGH
};
