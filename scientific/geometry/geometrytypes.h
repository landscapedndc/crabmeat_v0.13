/*!
 * @brief
 *    declare common types related to geometry input
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_GEOMETRY_TYPES_H_
#define  LDNDC_INPUT_GEOMETRY_TYPES_H_

#include  "crabmeat-common.h"
#include  "geom/geom.h"

namespace  ldndc{ namespace  geometry
{

struct  CBM_API  record
{
    enum  record_item_e
    {
        RECORD_ITEM_X,
        RECORD_ITEM_Y,
        RECORD_ITEM_Z,

        RECORD_SIZE
    };
};

extern CBM_API char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE];
extern CBM_API char const *  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE];

}}

#endif  /*  !LDNDC_INPUT_GEOMETRY_TYPES_H_  */


