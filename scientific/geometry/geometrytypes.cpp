/*!
 * @author
 *    steffen klatt (created on: feb 03, 2014),
 *    edwin haas
 */

#include  "geometry/geometrytypes.h"

namespace  ldndc{ namespace  geometry
{

char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE] =
{
    "x", "y", "z"
};
char const *  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE] =
{
    "latitudinal position", "longitudinal position", "elevation"
};

}}

