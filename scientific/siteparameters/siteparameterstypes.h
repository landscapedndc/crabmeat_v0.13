/*!
 * @brief
 *    declare common types related to site parameter input
 *
 * @author
 *    steffen klatt (created on: aug 12, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SITEPARAMETERSTYPES_H_
#define  LDNDC_INPUT_SITEPARAMETERSTYPES_H_

#include  "crabmeat-common.h"
#include  "containers/cbm_typewrap.h"

#ifdef  _DEBUG
#  include  "utils/cbm_utils.h"
#endif
#ifndef  __SITEPARAMETER_VALUE__
#  define  __SITEPARAMETER_VALUE__(__name__,__type__)  site_params_[__SITEPARAMETER_index_ ## __name__].get_value<__type__>()
#endif
#define  SITEPARAMETER_(__name__,__type__)                                              \
public:                                                                                 \
    __type__  __name__()                                                                \
    const                                                                               \
    {                                                                                   \
        crabmeat_assert( cbm::is_valid( __SITEPARAMETER_VALUE__(__name__,__type__)));    \
        return  __SITEPARAMETER_VALUE__(__name__,__type__);                             \
    }                                                                                   \
    static __type__  __name__##_##MIN()                                                 \
    {                                                                                   \
        return  SITEPARAMETER_##__name__##_MINVALUE;                                    \
    }                                                                                   \
    static __type__  __name__##_##MAX()                                                 \
    {                                                                                   \
        return  SITEPARAMETER_##__name__##_MAXVALUE;                                    \
    }                                                                                   \
private:
#define  SITEPARAMETER_BOOL(__name__)    SITEPARAMETER_(__name__,siteparameter_t::bool_type)
#define  SITEPARAMETER_FLT(__name__)    SITEPARAMETER_(__name__,siteparameter_t::float_type)
#define  SITEPARAMETER_INT(__name__)    SITEPARAMETER_(__name__,siteparameter_t::int_type)
#define  SITEPARAMETER_UINT(__name__)    SITEPARAMETER_(__name__,siteparameter_t::uint_type)

#define  SITEPARAMETER_INVALID_BOOL    false
#define  SITEPARAMETER_INVALID_FLT    ldndc::invalid_t< siteparameter_t::float_type >::value
#define  SITEPARAMETER_INVALID_INT    ldndc::invalid_t< siteparameter_t::int_type >::value
#define  SITEPARAMETER_INVALID_UINT    ldndc::invalid_t< siteparameter_t::uint_type >::value

#include  "io/outputtypes.h"
namespace  ldndc{ namespace  siteparameters
{
#define  LTYPEWRAP_SITE_PARAM_MAGIC  3
typedef  ltypewrap_t< ldndc_flt64_t, ldndc_int32_t, ldndc_uint32_t, LTYPEWRAP_SITE_PARAM_MAGIC >  siteparameter_t;
#define  SITEPARAMETERS_bool_type  LDNDC_BOOL
#define  SITEPARAMETERS_float_type  LDNDC_FLOAT64
#define  SITEPARAMETERS_int_type  LDNDC_INT32
#define  SITEPARAMETERS_uint_type  LDNDC_UINT32

#include  "siteparameters.h.inc"
}}

#endif  /*  !LDNDC_INPUT_SITEPARAMETERSTYPES_H_  */

