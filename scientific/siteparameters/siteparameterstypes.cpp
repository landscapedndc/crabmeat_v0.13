/*!
 * @brief
 *    container class for site parameters
 *
 * @author
 *    steffen klatt (created on: jun 19, 2013),
 *    edwin haas
 */

#include  "siteparameters/siteparameterstypes.h"
namespace  ldndc{ namespace  siteparameters
{
#include  "siteparameters.cpp.inc"
}}

