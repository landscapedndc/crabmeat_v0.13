/*!
 * @brief
 *    Declare common types related to site input
 *
 * @author
 *  - Steffen Klatt (created on: jun 06, 2013)
 *  - David Kraus
 *  - Edwin Haas
 */

#ifndef  LDNDC_INPUT_SITE_TYPES_H_
#define  LDNDC_INPUT_SITE_TYPES_H_

#include  "crabmeat-common.h"
#include  "cbm_types.h"

namespace  ldndc{ namespace  site
{
/*!
 * @brief
 *    site defaults
 */
struct  CBM_API  site_info_t
{
    /*! saturated hydraulic conductivity below soil [cm:min-1] */
    double  sksbottom;

    /*! water table  [m] */
    double  watertable;
};

extern CBM_API site_info_t const  site_info_defaults;


struct  CBM_API  iclass_site_stratum_t
{
    iclass_site_stratum_t();

    /*! depth of the soil profile [mm] */
    double  stratum_height;
    /*! number of soil layers per stratum [1..n]*/
    int  split;

    /*! average pH value */
    double  ph;
    /*! stone fraction [vol%] */
    double  stone_fraction;
    /*! soil density of soil incl. stones [kg dm-3] */
    double  bulk_density;
    /*! saturated hydraulic conductivity [cm min-1] */
    double  sks;
    /*! organic carbon content [0-1] */
    double  c_org;
    /*! organic nitrogen content [0-1] */
    double  n_org;
    /*! clay content [0-1] */
    double  clay;
    /*! sand content [0-1] */
    double  sand;
    /*! water content at wilting point [mm m-3] */
    double  wcmin;
    /*! water holding capacity [mm m-3] */
    double  wcmax;
    /*! maximum water filled pore space [%] */
    double  wfps_max;
    /*! minimum water filled pore space [%] */
    double  wfps_min;
    /*! total pore space [mm m-3] */
    double  porosity;
    /*! macropores to limit wcmax [mm m-3] */
    double  macropores;

    /*! iron content [%] */
    double  iron;

    /*! VanGenuchten parameter n [] */
    double  vangenuchten_n;
    /*! VanGenuchten parameter alpha [] */
    double  vangenuchten_alpha;

    /*! initial soil water content [mm m-3] */
    double  wc_init;
    /*! initial soil temperature [oC] */
    double  temp_init;
    
    /*! initial soil NH4 content [mg kg-1] */
    double  nh4_init;
    /*! initial soil NO3 content [mg kg-1] */
    double  no3_init;
    /*! initial soil DON content [mg kg-1] */
    double  don_init;
    /*! initial soil DOC content [mg kg-1] */
    double  doc_init;
    /*! initial fraction of particulate organic matter [0-1] */
    double  pom_init;
    /*! initial fraction of active organic matter [0-1] */
    double  aorg_init;
};
static size_t const   STRATUM_PROPERTIES_CNT = 25;
extern CBM_API ldndc_string_t const  STRATUM_PROPERTY_NAMES[STRATUM_PROPERTIES_CNT+1/*sentinel*/];

}}

#endif  /*  !LDNDC_INPUT_SITE_TYPES_H_  */

