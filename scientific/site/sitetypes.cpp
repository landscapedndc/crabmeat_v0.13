/*!
 * @brief
 *    common types related to site input
 *
 * @author
 *    Steffen Klatt (created on: jun 06, 2013),
 *    David Kraus
 *    Edwin Haas
 */

#include  "site/sitetypes.h"

ldndc::site::site_info_t const  ldndc::site::site_info_defaults =
{
    invalid_dbl, /* saturated hydraulic conductivity at soil bottom */
    99.0 /* water table */
};

ldndc_string_t const  ldndc::site::STRATUM_PROPERTY_NAMES[ldndc::site::STRATUM_PROPERTIES_CNT+1] =
{
    "ph", "soil_skeleton", "bulk_density", "hydraulic_conductivity",
    "organic_carbon", "organic_nitrogen",
    "clay", "sand",
    "wilting_point", "field_capacity",
    "maximum_water_filled_pore_space",
    "minimum_water_filled_pore_space",
    "total_pore_space",
    "macropores",
    "iron_content",
    "vangenuchten_n", "vangenuchten_alpha",
    "wc_init", "temp_init",
    "nh4_init","no3_init",
    "don_init","doc_init",
    "pom_init",
    "aorg_init",
    "" /*sentinel*/
};

