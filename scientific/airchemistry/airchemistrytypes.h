/*!
 * @brief
 *    declare common types related to air chemistry input
 *
 * @author
 *    steffen klatt (created on: aug 13, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_AIRCHEMISTRY_TYPES_H_
#define  LDNDC_INPUT_AIRCHEMISTRY_TYPES_H_

#include  "crabmeat-common.h"
#include  "datafilters/filter-average.h"

namespace  ldndc{ namespace  airchemistry
{
/*!
 * @brief
 *    air chemistry meta data
 */
struct  CBM_API  airchemistry_info_t
{
    /*!   [] */
    double  ch4;
    /*!   [] */
    double  co2;
    /*!   [] */
    double  nh3;
    /*!   [] */
    double  nh4;
    /*!   [] */
    double  nh4dry;
    /*!   [] */
    double  no;
    /*!   [] */
    double  no2;
    /*!   [] */
    double  no3;
    /*!   [] */
    double  no3dry;
    /*!   [] */
    double  o2;
    /*!   [] */
    double  o3;
};

extern  CBM_API airchemistry_info_t const  airchemistry_info_defaults;

struct  CBM_API  record
{
    /* air chemistry item datatype */
    typedef  double  item_type;
    enum  record_item_e
    {
        RECORD_ITEM_CH4,
        RECORD_ITEM_CO2,
        RECORD_ITEM_NH3,
        RECORD_ITEM_NH4,
        RECORD_ITEM_NH4_DRY,
        RECORD_ITEM_NO,
        RECORD_ITEM_NO2,
        RECORD_ITEM_NO3,
        RECORD_ITEM_NO3_DRY,
        RECORD_ITEM_O2,
        RECORD_ITEM_O3,

        RECORD_SIZE
    };
};

extern CBM_API char const *  RECORD_ITEM_UNITS[record::RECORD_SIZE];

extern CBM_API char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE];
extern CBM_API char const *  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE];

struct  CBM_API  streamdata_info_t
{
    enum { RECORD_SIZE = record::RECORD_SIZE };
    enum { BUFFER_SIZE_LOG = _CONFIG_BUFFERSIZE_LOG_AIRCHEM };

    typedef  record::record_item_e  record_item_e;
    typedef  record::item_type  element_type;
    typedef  airchemistry_info_t  boundary_data_type;
};


extern  CBM_API data_filter_average_t< record::item_type > const  airchemistry_datafilter_average;

extern  CBM_API data_filter_t< record::item_type > const *  AIRCHEMISTRY_DATAFILTER_LIST[record::RECORD_SIZE];

}}

#endif  /*  !LDNDC_INPUT_AIRCHEMISTRY_TYPES_H_  */


