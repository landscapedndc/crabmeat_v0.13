/*!
 * @author
 *    steffen klatt (created on: mar 05, 2013),
 *    edwin haas
 */

#include  "airchemistry/airchemistrytypes.h"

namespace  ldndc{ namespace  airchemistry
{

char const *  RECORD_ITEM_UNITS[record::RECORD_SIZE] =
{
    "ppm", "ppm", "ppm", "g:m^-3", "g:m^-2:d^-1", "ppm", "ppm", "g:m^-3", "g:m^-2:d^-1", "m^3:m^-3", "ppm"
};

char const *  RECORD_ITEM_NAMES[record::RECORD_SIZE] =
{
    "ch4", "co2", "nh3", "nh4", "nh4dry", "no", "no2", "no3", "no3dry", "o2", "o3"
};
char const *  RECORD_ITEM_NAMES_LONG[record::RECORD_SIZE] =
{
    "methane", "carbon dioxide", "ammonia", "ammonium", "ammonium dry", "nitric oxide", "nitrogen dioxide", "nitrate", "nitrate dry", "oxygen", "ozone"
};


data_filter_average_t< record::item_type > const  airchemistry_datafilter_average;

data_filter_t< record::item_type > const *  AIRCHEMISTRY_DATAFILTER_LIST[record::RECORD_SIZE] =
{
        &airchemistry_datafilter_average, /*  CH4 */
        &airchemistry_datafilter_average, /*  CO2 */
        &airchemistry_datafilter_average, /*  NH3 */
        &airchemistry_datafilter_average, /*  NH4 */
        &airchemistry_datafilter_average, /*  NH4_DRY */
        &airchemistry_datafilter_average, /*  NO */
        &airchemistry_datafilter_average, /*  NO2 */
        &airchemistry_datafilter_average, /*  NO3 */
        &airchemistry_datafilter_average, /*  NO3_DRY */
        &airchemistry_datafilter_average, /*  O2 */
        &airchemistry_datafilter_average, /*  O3 */
};

airchemistry_info_t const  airchemistry_info_defaults =
{
    0.0 /* ch4 */,
    370.0 /* co2 */,
    0.0 /* nh3 */,
    0.0 /* nh4 */,
    0.0 /* nh4dry */,
    0.0 /* no */,
    0.0 /* no2 */,
    0.0 /* no3 */,
    0.0 /* no3dry */,
    0.208 /* o2 */,
    0.0 /* o3 */
};

}}

