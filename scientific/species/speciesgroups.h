/*!
 * @brief
 *    declare species groups known to the simulation system
 *
 * @author
 *    steffen klatt (created on: jun 20, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SPECIESGROUPS_H_
#define  LDNDC_INPUT_SPECIESGROUPS_H_

#include  "crabmeat-common.h"


namespace  ldndc{ namespace  species
{
/*!
 * @brief
 *      groups of species, the user can assign to her species
 *
 *      these are meant to be some sort of main species categories
 *      to allow for generalization of species related processes
 */
enum  species_group_e
{
    /*! indicates no species group set */
    SPECIES_GROUP_NONE,
    /*! matches all groups */
    SPECIES_GROUP_ANY,

    /*! crops */
    SPECIES_GROUP_CROP,
    /*! grasses */
    SPECIES_GROUP_GRASS,
    //SPECIES_GROUP_HERB,
    //SPECIES_GROUP_SHRUB,
    /*! woody species (trees) */
    SPECIES_GROUP_WOOD,

    /*! number of species groups */
    SPECIES_GROUP_CNT
};
extern CBM_API char const *  SPECIES_GROUP_NAMES[SPECIES_GROUP_CNT];
extern CBM_API char const *  internal_SPECIES_GROUP_ANY;

/*!
 * @brief
 *      mnemonic for species none-type. this type has
 *      to be listed in each species parameter set
 *      section exactly once. it holds defaults that
 *      are used to account for missing parameters in
 *      non-none-type species parameters section.
 */
extern CBM_API char const *  SPECIES_NAME_NONE;

/*!
 * @brief
 *    make species group number a bit mask for or'ing
 */
template < species_group_e  _G >
struct  select_species_t
{
    enum
    {
        select = 1 << _G,
        deselect = ~select
    };

    static int  fselect( species_group_e  _g)
    {
        return  1 << _g;
    }
};

template < species_group_e  _g >
struct CBM_API  species_group_t
{
    enum
    {
        group_id = _g
    };
    static species_group_e  group() { return  _g; }
    static char const *  name() { return  SPECIES_GROUP_NAMES[_g]; }
};
struct CBM_API  none  :  public  species_group_t< SPECIES_GROUP_NONE >
{
    enum
    {
        select = select_species_t< SPECIES_GROUP_NONE >::select,
        deselect = ~0
    };
};
struct CBM_API  any  :  public  species_group_t< SPECIES_GROUP_ANY >
{
    enum
    {
        select = select_species_t< SPECIES_GROUP_CROP >::select|select_species_t< SPECIES_GROUP_GRASS >::select|select_species_t< SPECIES_GROUP_WOOD >::select,
        deselect = ~0
    };
};
typedef  any  no_none;
typedef  none  no_any;
struct CBM_API  crop  :  public  species_group_t< SPECIES_GROUP_CROP >
{
    enum
    {
        select = select_species_t< SPECIES_GROUP_CROP >::select,
        deselect = ~0
    };
};
struct CBM_API  no_crop  :  public  species_group_t< SPECIES_GROUP_NONE >
{
    enum
    {
        select = 0,
        deselect = select_species_t< SPECIES_GROUP_CROP >::deselect
    };
};
struct CBM_API  grass  :  public  species_group_t< SPECIES_GROUP_GRASS >
{
    enum
    {
        select = select_species_t< SPECIES_GROUP_GRASS >::select,
        deselect = ~0
    };
};
struct CBM_API  no_grass  :  public  species_group_t< SPECIES_GROUP_NONE >
{
    enum
    {
        select = 0,
        deselect = select_species_t< SPECIES_GROUP_GRASS >::deselect
    };
};
struct CBM_API  wood  :  public  species_group_t< SPECIES_GROUP_WOOD >
{
    enum
    {
        select = select_species_t< SPECIES_GROUP_WOOD >::select,
        deselect = ~0
    };
};
struct CBM_API  no_wood  :  public  species_group_t< SPECIES_GROUP_NONE >
{
    enum
    {
        select = 0,
        deselect = select_species_t< SPECIES_GROUP_WOOD >::deselect
    };
};



template < species_group_e  _G >
struct  species_group_from_enum_t
{
};
template < >
struct  species_group_from_enum_t< SPECIES_GROUP_NONE >
{
    typedef  none  group;
};
template < >
struct  species_group_from_enum_t< SPECIES_GROUP_ANY >
{
    typedef  any  group;
};
template < >
struct  species_group_from_enum_t< SPECIES_GROUP_CROP >
{
    typedef  crop  group;
};
template < >
struct  species_group_from_enum_t< SPECIES_GROUP_GRASS >
{
    typedef  grass  group;
};
template < >
struct  species_group_from_enum_t< SPECIES_GROUP_WOOD >
{
    typedef  wood  group;
};

struct  species_groups_selector_t
{
    virtual ~species_groups_selector_t() {}
    virtual bool  is_selected( species_group_e) const = 0;
};
template < typename  _G1 = species::any, typename  _G2 = species::none, typename  _G3 = species::none, 
         typename  _G4 = species::none, typename  _G5 = species::none, typename  _G6 = species::none, typename  _G7 = species::none >
struct  species_groups_select_t : public species_groups_selector_t
{
    enum
    {
        select = (_G1::select|_G2::select|_G3::select|_G4::select|_G5::select|_G6::select|_G7::select) &
                (_G1::deselect&_G2::deselect&_G3::deselect&_G4::deselect&_G5::deselect&_G6::deselect&_G7::deselect)
    };
    bool  is_selected( species_group_e  _group) const
    {
        return  ( _group==SPECIES_GROUP_ANY && int(this->select)!=int(species::none::select)) ||
            ( this->select & select_species_t< SPECIES_GROUP_ANY >::fselect( _group));
    }
};


}  /* namespace species */
}  /* namespace ldndc */


#endif  /*  !LDNDC_INPUT_SPECIESGROUPS_H_  */

