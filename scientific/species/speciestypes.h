/*!
 * @brief
 *    declare common types related to species
 *
 * @author
 *    steffen klatt (created on: apr 04, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SPECIESTYPES_H_
#define  LDNDC_INPUT_SPECIESTYPES_H_

#include  "crabmeat-common.h"
#include  "memory/cbm_mem.h"
#include  "time/cbm_date.h"
#include  "string/cbm_string.h"
#include  "species/speciesgroups.h"
#include  "speciesparameters/speciesparameterstypes.h"


#include  "containers/lstack.h"

namespace  ldndc{ namespace  species
{
/*! convenience macro to check if species instances are identical based on name (==ID) */
#define     IS_SPECIE(__name1__,__name2__)    cbm::is_equal_i( __name1__, __name2__)
/*! convenience macro to check if species instances are same type, i.e. share parameter set */
#define  MATCH_SPECIE(__type1__,__type2__)    cbm::is_equal_i( __type1__, __type2__)

typedef  ldndc::stack< unsigned int >  replace_index_t;

struct  species_properties_t;
struct  CBM_API  specie_properties_factory_base_t
{
    specie_properties_factory_base_t() {}
    virtual  ~specie_properties_factory_base_t() = 0;

    virtual  species_properties_t *  new_instance() const = 0;
    virtual  void  delete_instance( species_properties_t *) const = 0;
    virtual  species_properties_t *  clone( species_properties_t *, species_properties_t const *) const = 0;
};
template < typename  _G >
struct  CBM_API  specie_properties_factory_t  :  public  specie_properties_factory_base_t
{
    specie_properties_factory_t()
        : specie_properties_factory_base_t()
    {
    }
    species_properties_t *  new_instance()
    const
    {
        return  CBM_DefaultAllocator->construct< _G >();
    }
    void  delete_instance( species_properties_t *  _props)
    const
    {
        if ( _props)
        {
            CBM_DefaultAllocator->destroy( static_cast< _G * >( _props));
        }
    }

    species_properties_t *  clone(
            species_properties_t *  _props_dst, species_properties_t const *  _props_src)
    const
    {
        crabmeat_assert( _props_src);

        _G *  this_props_dst = static_cast< _G * >( _props_dst);
        if ( !this_props_dst)
        {
            this_props_dst = static_cast< _G * >( this->new_instance());
            if ( !this_props_dst)
            {
                return  NULL;
            }
        }
        *this_props_dst = *static_cast< _G const * >( _props_src);

        return  this_props_dst;
    }
};

struct  CBM_API  species_properties_t
{
    /*! species group */
    species_group_e  group;

    /*! biomass of species at time of "planting" (kg ha-1) */
    double  initial_biomass;
    /*! area fraction of species relative to total ground coverage (0-1) */
    double  fractional_cover;

    void initialize( species_group_e  _group)
    {
        this->group = _group;
        this->initial_biomass = invalid_dbl;
        this->fractional_cover = 1.0;
    }
};

struct  CBM_API  crop_properties_t  :  public  species_properties_t
{
    /*! specifies if this crop is a cover crop */
    bool  cover_crop;

    crop_properties_t &  initialize()
    {
        species_properties_t::initialize( SPECIES_GROUP_CROP);
        this->cover_crop = false;
        this->seedbed_duration = 0;
        this->seedling_number = invalid_dbl;
        
        return  *this;
    }

    /*! number of days in seedbed */
    int  seedbed_duration;
    /*! number of individuals per ha [-] */
    double  seedling_number;
};
extern  crop_properties_t CBM_API const  crop_properties_default;
extern  specie_properties_factory_t< crop_properties_t > CBM_API const  crop_properties_factory;

struct  CBM_API  grass_properties_t  :  public  species_properties_t
{
        /*! specifies if this grass is a cover crop */
        bool  cover_crop;

    grass_properties_t &  initialize()
    {
        species_properties_t::initialize( SPECIES_GROUP_GRASS);
        this->height_max = invalid_dbl;
        this->height_min = invalid_dbl;

        this->root_depth = invalid_dbl;

        this->cover_crop = false;

        return  *this;
    }
    /*! upper height of vegetation (i.e. average height of highest vegetation cohort)  [m] */
    double  height_max;
    /*! height of canopy start (from the ground)  [m] */
    double  height_min;
    /*! root depth  [m] */
    double  root_depth;
};
extern  grass_properties_t CBM_API const  grass_properties_default;
extern  specie_properties_factory_t< grass_properties_t > CBM_API const  grass_properties_factory;

struct  CBM_API  wood_properties_t  :  public  species_properties_t
{
    wood_properties_t &  initialize()
    {
        species_properties_t::initialize( SPECIES_GROUP_WOOD);
        this->dbh = invalid_dbl;
        this->height_max = invalid_dbl;
        this->height_min = invalid_dbl;
        this->reduc_fac_c = invalid_dbl;
        this->root_depth = invalid_dbl;
        this->number = invalid_dbl;
        this->volume = invalid_dbl;

        return  *this;
    }

    /*! average individual diameter at 1.3 m height  [m] */
    double  dbh;
    /*! upper height of vegetation (i.e. average height of highest vegetation cohort)  [m] */
    double  height_max;
    /*! height of canopy start (from the ground)  [m] */
    double  height_min;
    /*! reduction factor for initialization of the nitrogen and basic cation concentration relative to the maximum (0-1) */
    double  reduc_fac_c;
    /*! root depth  [m] */
    double  root_depth;
    /*! number of individuals per ha [-] */
    double  number;
    /*! stemwood (or stand) volume [m3 ha-1] */
    double  volume;
};
extern  wood_properties_t CBM_API const  wood_properties_default;
extern  specie_properties_factory_t< wood_properties_t > CBM_API const  wood_properties_factory;

extern  species_properties_t CBM_API const *  species_properties_default[SPECIES_GROUP_CNT];
extern  specie_properties_factory_base_t CBM_API const *  species_properties_factories[SPECIES_GROUP_CNT];
struct  CBM_API  species_properties_factory_t
{
    species_properties_factory_t() {}

    species_properties_t *  new_properties( species_group_e) const;
    void  delete_properties( species_properties_t *) const;

    species_properties_t *  clone( species_properties_t *, species_properties_t const *) const;
};
extern  species_properties_factory_t CBM_API const  species_properties_factory;

#define  SPECIES_T_PROPERTIES_GETTER(__group__,__prop__)                        \
    public:                                                \
        /* getter */                                        \
        __prop__ ## _properties_t const *  __prop__()                        \
        const                                            \
        {                                            \
            crabmeat_assert( this->properties_exist());                    \
            /* NOTE                                    */    \
            /* if this assertion fires, make sure you access species properties    */    \
            /* via the correct getter, i.e. according to its group!            */    \
            crabmeat_assert( this->group() == __group__);        \
            /*return  static_cast< __prop__##_properties_t const * >( this->properties_);    */\
            return  static_cast< __prop__##_properties_t const * >( &this->group_properties_.__prop__);    \
        }                                            \
    private:

class  CBM_API  species_t
{
    public:
        species_t();
        ~species_t();

    private:
        /* species parameter set */
        speciesparameters::speciesparameters_set_t  par_;
    public:
        speciesparameters::speciesparameters_set_t const &  get_parameters()
        const
        {
            return  this->par_;
        }
        void  set_parameters(
                speciesparameters::speciesparameters_set_t const &  _par)
        {
            this->par_ = _par;
        }
        speciesparameters::speciesparameters_set_t const *  operator->()
        const
        {
            return  &this->par_;
        }

        /* convenience wrapper for group of species */
        species_group_e  group()
        const
        {
            return  ( this->have_group_properties_) ? this->par_.GROUP() : SPECIES_GROUP_NONE;
        }
        /* convenience wrapper for groupname of species */
        char const *  groupname()
        const
        {
            return  SPECIES_GROUP_NAMES[this->have_group_properties_ ? this->par_.GROUP() : SPECIES_GROUP_NONE];
        }
        /* convenience wrapper for type of species */
        char const *  type()
        const
        {
            return  this->par_.TYPE();
        }
    private:
        std::string  name_;
    public:
        /* name of species */
        char const *  name()
        const
        {
            return  this->name_.c_str();
        }
        char const *  name( char const *  _name)
        {
            this->name_ = _name;
            return  this->name_.c_str();
        }

    private:
        ldate_t  plant_date_;
    public:
        ldate_t const &  plant_date()
        const
        {
            return  this->plant_date_;
        }
        ldate_t const &  plant_date( ldate_t const &  _pd)
        {
            this->plant_date_ = _pd;
            return  this->plant_date_;
        }

    private:
        /* additional group specific properties for species */
        bool  have_group_properties_;
        union  group_properties_t
        {
            crop_properties_t  crop;
            grass_properties_t  grass;
            wood_properties_t  wood;
        }  group_properties_;
    public:
        bool  properties_exist()
        const
        {
            return  this->have_group_properties_;
        }

        species_properties_t *  get_properties()
        {
            return  static_cast< species_properties_t * >( &this->group_properties_.wood);
        }
        species_properties_t const *  get_properties()
        const
        {
            return  static_cast< species_properties_t const * >( &this->group_properties_.wood);
        }
        /* convenience getters for group specific properties */
        SPECIES_T_PROPERTIES_GETTER(SPECIES_GROUP_CROP,crop)
        SPECIES_T_PROPERTIES_GETTER(SPECIES_GROUP_GRASS,grass)
        SPECIES_T_PROPERTIES_GETTER(SPECIES_GROUP_WOOD,wood)

    public:
        /* reset species properties */
        lerr_t  set_properties(
                species_group_e  _group,
                species_properties_t const *  _props);
};

}}

#endif  /*  !LDNDC_INPUT_SPECIESTYPES_H_  */

