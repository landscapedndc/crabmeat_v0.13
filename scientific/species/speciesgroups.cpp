/*!
 * @brief
 *    species groups known to the simulation system (implementation)
 *
 * @author
 *    steffen klatt (created on: jun 20, 2013),
 *    edwin haas
 */

#include  "species/speciesgroups.h"

char const *  ldndc::species::SPECIES_GROUP_NAMES[ldndc::species::SPECIES_GROUP_CNT] = { "none", "any",  "crop", "grass", "wood"};
char const *  ldndc::species::internal_SPECIES_GROUP_ANY = ":ANY:";
char const *  ldndc::species::SPECIES_NAME_NONE = "NONE";

