/*!
 * @brief
 *    common types related to species (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 04, 2013),
 *    edwin haas
 */

#include  "species/speciestypes.h"

namespace  ldndc{ namespace  species
{

specie_properties_factory_base_t::~specie_properties_factory_base_t()
{
}

/* crop properties defaults and factory */
crop_properties_t const  crop_properties_default( crop_properties_t().initialize());
specie_properties_factory_t< crop_properties_t > const  crop_properties_factory;

/* grass properties defaults and factory */
grass_properties_t const  grass_properties_default( grass_properties_t().initialize());
specie_properties_factory_t< grass_properties_t > const  grass_properties_factory;

/* wood properties defaults and factory */
wood_properties_t const  wood_properties_default( wood_properties_t().initialize());
specie_properties_factory_t< wood_properties_t > const  wood_properties_factory;

/* default species properties of all types */
species_properties_t const *  species_properties_defaults[SPECIES_GROUP_CNT] =
{
    NULL /*none*/,
    NULL /*any*/,
    &crop_properties_default,
    &grass_properties_default,
    &wood_properties_default
};
/* species properties factory for all types */
specie_properties_factory_base_t const *  species_properties_factories[SPECIES_GROUP_CNT] =
{
    NULL /*none*/,
    NULL /*any*/,
    &crop_properties_factory,
    &grass_properties_factory,
    &wood_properties_factory
};
/* species properties factory */
species_properties_factory_t const  species_properties_factory = species_properties_factory_t();


species_properties_t *
species_properties_factory_t::new_properties(
        species_group_e  _group)
const
{
    species_properties_t * p = species_properties_factories[_group]->new_instance();
    if ( p)
    {
        /* initialize */
        species_properties_factories[_group]->clone( p, species_properties_defaults[_group]);
    }
    return  p;
}
void
species_properties_factory_t::delete_properties(
        species_properties_t *  _props)
const
{
    if ( _props)
    {
        species_group_e  this_group( _props->group);
        species_properties_factories[this_group]->delete_instance( _props);
    }
}

species_properties_t *
species_properties_factory_t::clone(
        species_properties_t *  _props_dst, species_properties_t const *  _props_src)
const
{
    if ( !_props_src)
    {
        return  NULL;
    }
    species_group_e  this_group( _props_src->group);
    return  species_properties_factories[this_group]->clone( _props_dst, _props_src);
}

species_t::species_t()
        : name_( SPECIES_NAME_NONE),
          have_group_properties_( false)
{
}

species_t::~species_t()
{
}


lerr_t
species_t::set_properties(
        species_group_e  _group,
        species_properties_t const *  _props)
{
    this->have_group_properties_ = true;

    /* set new properties .. use defaults otherwise */
    species_properties_t *  p = species_properties_factory.clone(
                    this->get_properties(),
                    ( _props) ? _props : species_properties_defaults[_group]);

    return ( p) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}
}}

