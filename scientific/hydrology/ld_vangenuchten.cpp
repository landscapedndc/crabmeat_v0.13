/*!
 * @brief
 *    class containing full set of van Genuchten parametrization
 *
 * @author
 *    David Kraus (created on: february 28, 2017)
 */

#include  "scientific/hydrology/ld_vangenuchten.h"
#include  "math/cbm_math.h"



double
ldndc::hydrology::capillary_pressure(
                   double const &_wcl,
                   double const &_vg_alpha,
                   double const &_vg_n,
                   double const &_vg_m,
                   double const &_wcl_s,
                   double const &_wcl_r)
{
    crabmeat_assert( _wcl_s > _wcl_r);
    if ( cbm::flt_greater( _wcl_s, _wcl))
    {      
        // The effective water content scales between 0.01 and 0.99, and represents 
        // how close the actual water content is to the residual and saturated water content.
        // The actual water content can't get smaller than the residual water content.
        // However, for _wcl=_wcl_r and wcle=0, the capillary_pressure (or water potential) diverges.
        double const wcle( cbm::bound( 0.001, (_wcl - _wcl_r) / (_wcl_s - _wcl_r), 0.999));
        return ((1.0 / _vg_alpha) * pow( pow( wcle, (-1.0 / _vg_m)) - 1.0, (1.0 / _vg_n)));
    }
    else
    {
        double const wcle( 0.99);
        return ((1.0 / _vg_alpha) * pow( pow( wcle, (-1.0 / _vg_m)) - 1.0, (1.0 / _vg_n)));
    }
}


double
ldndc::hydrology::water_content(
                    double const &_cp,
                    double const &_vg_alpha,
                    double const &_vg_n,
                    double const &_vg_m,
                    double const &_wcl_s,
                    double const &_wcl_r)
{
    double const wcle( pow(1.0 + pow( _cp * _vg_alpha, _vg_n), (-1.0)*_vg_m));

    return (wcle * (_wcl_s - _wcl_r) + _wcl_r);
}


double
ldndc::hydrology::hydraulic_conductivity(
                                 double const &_wcl,
                                 double const &_vg_m,
                                 double const &_wcl_s,
                                 double const &_wcl_r,
                                 double const &_ks)
{
    //~ double const vg_m( 1.0 - 1.0 / _vg_n);
    double const wcle( (_wcl - _wcl_r) / (_wcl_s - _wcl_r));

    return (_ks * pow(wcle, 0.5) * pow( 1.0 - pow(( 1.0 - pow(wcle, (1.0/_vg_m))), _vg_m), 2.0));
}



ldndc::hydrology::VanGenuchten::VanGenuchten()
{}


ldndc::hydrology::VanGenuchten::VanGenuchten(
                                             double _vg_alpha,
                                             double _vg_n,
                                             double _k_sat,
                                             double _theta_r,
                                             double _theta_s)
{
    // Note: Van Genuchten parameter are read in for standard values of specific soil types. 
    // However, parameter also do change within a soil type depending on texture (see e.g. Porebska et al. 2006, Int. Agrophysics)
    // Following the [Ad-hoc-AG Boden der Staatlichen Geologischen Dienste und der BGR, 1999]
    // the parameter m should be set to 1, if the texture, bulk density and organic-C content are known.
    //~ double const vg_m( 1.0 );
    // The formula m = 1 - 1/n can get negative, if n<1, and shouldn't be used if the grain sizes are available.
    // double const vg_m( 1.0 - 1.0 / _vg_n);
    this->m_vg_alpha = _vg_alpha;
    this->m_vg_n = _vg_n;
    this->m_vg_m = (1.0 - 1.0 / _vg_n);

    this->m_k_sat = _k_sat;

    this->m_theta_r = _theta_r;
    this->m_theta_s = _theta_s;
}


ldndc::hydrology::VanGenuchten::~VanGenuchten()
{}


void
ldndc::hydrology::VanGenuchten::set_parameters(
                                               double _vg_alpha,
                                               double _vg_n,
                                               double _k_sat,
                                               double _theta_r,
                                               double _theta_s)
{
    this->m_vg_alpha = _vg_alpha;
    this->m_vg_n = _vg_n;
    this->m_vg_m = (1.0 - 1.0 / _vg_n);

    this->m_k_sat = _k_sat;

    this->m_theta_r = _theta_r;
    this->m_theta_s = _theta_s;
}


double
ldndc::hydrology::VanGenuchten::capillary_pressure(
                                                   double const & _se)
{
    return ((1.0 / this->m_vg_alpha) * pow( pow( _se, (-1.0 / this->m_vg_m)) - 1.0, (1.0 / this->m_vg_n)));
}



double
ldndc::hydrology::VanGenuchten::hydraulic_conductivity(
                                                       double const & _se)
{
    return (this->m_k_sat * pow(_se, 0.5) * pow(1.0 - pow(1.0 - pow(_se, 1.0 / this->m_vg_m), this->m_vg_m), 2.0));
}



double
ldndc::hydrology::VanGenuchten::specific_moisture_storage(
                                                          double const & _h)
{
    return (-this->m_vg_alpha * this->m_vg_n * _h * (1.0 / this->m_vg_n - 1.0)
            * pow(this->m_vg_alpha * std::abs(_h), this->m_vg_n - 1.0)
            * (this->m_theta_r - this->m_theta_s)
            * pow(pow(this->m_vg_alpha * std::abs(_h), this->m_vg_n) + 1.0, (1.0 / this->m_vg_n - 2.0)));
}



double
ldndc::hydrology::VanGenuchten::volumetric_moisture_content(
                                                            double const & _h)
{
    return ((this->m_theta_s - this->m_theta_r) / (1.0 + this->m_vg_alpha * std::abs(_h)));
}



double
ldndc::hydrology::VanGenuchten::effective_saturation(
                                                     double const & _h)
{
    double const theta( (this->m_theta_s - this->m_theta_r) / pow(1.0 + pow(this->m_vg_alpha * std::abs(_h), this->m_vg_n), this->m_vg_m) + this->m_theta_r);

    return ((theta - this->m_theta_r) / (this->m_theta_s - this->m_theta_r));
}

