/*!
 * @brief
      class containing full set of van Genuchten parametrization
 * 
 * @author
 *    David Kraus (created on: february 28, 2017)
 */

#ifndef  LDNDC_SCIENTIFIC_VANGENUCHTEN_H_
#define  LDNDC_SCIENTIFIC_VANGENUCHTEN_H_

#include  "crabmeat-common.h"

namespace  ldndc { namespace hydrology
{
    CBM_API double
    capillary_pressure(
               double const & /*water saturation/content*/,
               double const & /*Van Genuchten alpha*/,
               double const & /*Van Genuchten n*/,
               double const & /*Van Genuchten m*/,
               double const & /*saturated water saturation/content*/,
               double const & /*residual water saturation/content*/);

    CBM_API double
    water_content(
          double const & /*capillary pressure*/,
          double const & /*Van Genuchten alpha*/,
          double const & /*Van Genuchten n*/,
          double const & /*Van Genuchten m*/,
          double const & /*saturated water saturation/content*/,
          double const & /*residual water saturation/content*/);

    CBM_API double
    hydraulic_conductivity(
                   double const & /*water saturation/content*/,
                   double const & /*Van Genuchten m*/,
                   double const & /*saturated water saturation/content*/,
                   double const & /*residual water saturation/content*/,
                   double const & /*saturated hydraulic conductivity*/);


    class CBM_API VanGenuchten
    {
        public:

            VanGenuchten();

            VanGenuchten(
                 double /* Van Genuchten alpha */,
                 double /* Van Genuchten n */,
                 double /* Saturated hydraulic conductivity */,
                 double /* Residual water saturation */,
                 double /* Maximum water saturation */);

            ~VanGenuchten();

            void
            set_parameters(
                 double /* Van Genuchten alpha */,
                 double /* Van Genuchten n */,
                 double /* Saturated hydraulic conductivity */,
                 double /* Residual water saturation */,
                 double /* Maximum water saturation */);

            double
            capillary_pressure(
                       double const & /*effective saturation*/);

            double
            hydraulic_conductivity(
                           double const & /*effective saturation*/);

            double
            specific_moisture_storage(
                              double const & /*effective saturation*/);

            inline double vg_alpha() const { return this->m_vg_alpha;}
            inline double vg_n() const { return this->m_vg_n;}
            inline double vg_m() const { return this->m_vg_m;}

            inline double theta_r() const { return this->m_theta_r;}
            inline double theta_s() const { return this->m_theta_s;}

        private:

            double m_vg_alpha;
            double m_vg_n;
            double m_vg_m;

            double m_k_sat;
            double m_theta_s;
            double m_theta_r;

            double
            volumetric_moisture_content(
                                double const & /*pressure head*/);

            double
            effective_saturation(
                         double const & /*pressure head*/);
    };

}  /*  namespace hydrology  */
}  /*  namespace ldndc  */

#endif  /*  !LDNDC_SCIENTIFIC_VANGENUCHTEN_H_  */

