/*!
 * @brief
 *    attempts to detect platform and compiler. we
 *    also set compiler specific and platform specific
 *    macros.
 *
 *    based on:
 *    http://sourceforge.net/apps/mediawiki/predef/index.php?title=Main_Page
 *    or
 *    http://sourceforge.net/p/predef/wiki/Compilers/
 *
 * @author 
 *    steffen klatt (created on: sep 02, 2012)
 */

#ifndef  BZ_COMPILERS_H_
#define  BZ_COMPILERS_H_

/** compiler detection **/

/* clang (llvm), note: have before gcc because clang also identifies as gcc.. */
#if  defined(__clang__) /*|| defined(__llvm__)*/
#  define  BZ_COMPILER_CLANG
#  define  BZ_COMPILER_NAME "Clang"
/* pgi */
#elif  defined(__PGI)
#  define  BZ_COMPILER_PGI
#  define  BZ_COMPILER_NAME "PGI"
/* gnu c/c++ */
#elif  defined(__GNUC__) || defined(__GNUG__)
#  define  BZ_COMPILER_GCC
#  define  BZ_COMPILER_NAME "GCC"
/* intel */
#elif  defined(__INTEL_COMPILER) || defined(__ICL)
#  define  BZ_COMPILER_INTEL
#  define  BZ_COMPILER_NAME "Intel"
/* mingw */
#elif  defined(__MINGW32__) || defined(__MINGW64__)
#  define  BZ_COMPILER_MINGW
#  define  BZ_COMPILER_NAME "MinGW"
/* ms c */
#elif  defined(_MSC_VER)
#  define  BZ_COMPILER_MSC
#  define  BZ_COMPILER_NAME "MSVC"
/* ? */
#else
        /* we wish you the best of luck */
#endif

#endif  /*  !BZ_COMPILERS_H_  */

