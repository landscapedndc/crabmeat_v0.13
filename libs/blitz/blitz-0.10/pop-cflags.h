#ifndef  LDNDC_POPCOMPILERFLAGS_H_
#define  LDNDC_POPCOMPILERFLAGS_H_

#if  defined(CRABMEAT_COMPILER_GCC)
#  if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#    pragma GCC diagnostic pop
#  endif
#elif  defined(CRABMEAT_COMPILER_CLANG)
#  pragma clang diagnostic pop
#elif  defined(CRABMEAT_COMPILER_MSC)
#  pragma warning (pop)
#else
  /* nothing here */
#endif

#endif  /*  !LDNDC_POPCOMPILERFLAGS_H_  */

