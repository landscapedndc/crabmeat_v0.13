// -*- C++ -*-
/***************************************************************************
 * blitz/bzconfig.h      Select compiler-specific config file
 *
 * $Id$
 *
 * Copyright (C) 1997-2011 Todd Veldhuizen <tveldhui@acm.org>
 *
 * This file is a part of Blitz.
 *
 * Blitz is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Blitz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public 
 * License along with Blitz.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Suggestions:          blitz-devel@lists.sourceforge.net
 * Bugs:                 blitz-support@lists.sourceforge.net    
 *
 * For more information, please see the Blitz++ Home Page:
 *    https://sourceforge.net/projects/blitz/
 *
 ***************************************************************************/

/* Select the compiler-specific config.h header file */
#ifndef BZCONFIG_H
#define BZCONFIG_H

#include  "bzconfig.h.inc"
#include  "blitz/bzconfig-man.h"

// sk:off #if defined(__APPLE__)
// sk:off #if defined(__GNUC__)
// sk:off /* GNU gcc compiler for newer Mac OS X Darwin */
// sk:off #include <blitz/gnu/bzconfig.h>
// sk:off #else
// sk:off /* IBM xlc compiler for Darwin */
// sk:off #include <blitz/apple/bzconfig.h>
// sk:off #endif
// sk:off 
// sk:off #elif defined(__INTEL_COMPILER)
// sk:off /* Intel icc compiler */
// sk:off #include <blitz/intel/bzconfig.h>
// sk:off 
// sk:off #elif defined(_MSC_VER)
// sk:off /* Microsoft VS.NET compiler */
// sk:off #include <blitz/ms/bzconfig.h>
// sk:off 
// sk:off #elif defined(__xlC__)
// sk:off /* IBM xlC compiler */
// sk:off #include <blitz/ibm/bzconfig.h>
// sk:off 
// sk:off #elif defined(__DECCXX)
// sk:off /* Compaq cxx compiler */
// sk:off #include <blitz/compaq/bzconfig.h>
// sk:off 
// sk:off #elif defined(__HP_aCC)
// sk:off /* HP aCC compiler */
// sk:off #include <blitz/hp/bzconfig.h>
// sk:off 
// sk:off #elif defined(_SGI_COMPILER_VERSION)
// sk:off /* SGI CC compiler */
// sk:off #include <blitz/sgi/bzconfig.h>
// sk:off 
// sk:off #elif defined(__SUNPRO_CC)
// sk:off /* SunPRO CC compiler */
// sk:off #include <blitz/sun/bzconfig.h>
// sk:off 
// sk:off #elif defined(__PATHCC__)
// sk:off /* Pathscale pathCC compiler */
// sk:off #include <blitz/pathscale/bzconfig.h>
// sk:off 
// sk:off #elif defined(__GNUC__)
// sk:off /* GNU gcc compiler */
// sk:off #include <blitz/gnu/bzconfig.h>
// sk:off 
// sk:off #elif defined(__PGI)
// sk:off /* PGI pgCC compiler */
// sk:off #include <blitz/pgi/bzconfig.h>
// sk:off 
// sk:off #elif defined(__KCC)
// sk:off /* KAI KCC compiler */
// sk:off #include <blitz/kai/bzconfig.h>
// sk:off 
// sk:off #elif defined(__FUJITSU)
// sk:off /* Fujitsu FCC compiler */
// sk:off #include <blitz/fujitsu/bzconfig.h>
// sk:off 
// sk:off /* Add other compilers here */
// sk:off 
// sk:off #else
// sk:off #error Unknown compiler
// sk:off #endif

#endif /* BZCONFIG_H */
