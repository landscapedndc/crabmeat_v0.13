/*!
 * @brief
 *    wrapper for headers of used back-end
 *
 * @author 
 *    steffen klatt (created on: may 18, 2013),
 *    edwin haas
 */

#ifndef  BZ_VECTOR_BACKEND_H_ 
#define  BZ_VECTOR_BACKEND_H_

#include  "bzconfig.h.inc"

//#if  defined(_DEBUG) && !defined(BZ_DEBUG)
//#  define  BZ_DEBUG
//#elif  !defined(_DEBUG) && defined(BZ_DEBUG)
//#  undef  BZ_DEBUG
//#endif
#if  !defined(CRABMEAT_HAVE_ASSERT) && defined(BZ_DEBUG)
#  ifdef  BZ_DEBUG
#    undef  BZ_DEBUG
#  endif
#endif

#include  "push-cflags.h"

#include  "blitz/blitz.h"
#include  "blitz/array.h"
#include  "blitz/indexexpr.h"
#include  "blitz/bzdebug.h"
#include  "blitz/timer.h"
#include  "random/mt.h"

#include  "pop-cflags.h"

#endif  /*  !BZ_VECTOR_BACKEND_H_  */

