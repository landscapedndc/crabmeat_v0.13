#ifndef  LDNDC_PUSHCOMPILERFLAGS_H_
#define  LDNDC_PUSHCOMPILERFLAGS_H_

#include  "bz-compiler.h"

#if  defined(BZ_COMPILER_GCC)
#  if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6)
#    pragma GCC diagnostic push
#  endif
#  pragma GCC diagnostic ignored "-Wfloat-equal"
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wsign-compare"
#  pragma GCC diagnostic ignored "-Wstrict-aliasing"
#  pragma GCC diagnostic ignored "-Wuninitialized"
#  pragma GCC diagnostic ignored "-Wunused-but-set-parameter"
#  pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wunused-variable"
#elif  defined(BZ_COMPILER_CLANG)
#  pragma clang diagnostic push
#  pragma clang diagnostic ignored "-Wcast-align"
#  pragma clang diagnostic ignored "-Wfloat-equal"
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wsign-compare"
#  pragma clang diagnostic ignored "-Wstrict-aliasing"
#  pragma clang diagnostic ignored "-Wuninitialized"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wunused-variable"
#elif  defined(BZ_COMPILER_MSC)
#  pragma warning (push)
#  pragma warning (disable:4100)  /* unreferenced formal parameter */
#  pragma warning (disable:4189)  /* local variable is initialized but not referenced */
#  pragma warning (disable:4522)  /* multiple assignment operators specified */
#else
   /* hmm, here goes the noise ... */
#endif

#endif  /*  !LDNDC_PUSHCOMPILERFLAGS_H_  */

