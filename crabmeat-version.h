
#ifndef  CRABMEAT_VERSION_H_
#define  CRABMEAT_VERSION_H_

#include  "crabmeat-common.h"
extern CBM_API char const *  crabmeat_package_name();
extern CBM_API char const *  crabmeat_package_name_short();

extern CBM_API char const *  crabmeat_package_version();
extern CBM_API char const *  crabmeat_package_revision();
extern CBM_API char const *  crabmeat_package_build_timestamp();

extern CBM_API char const *  crabmeat_package_build_configuration();

#endif  /*  !CRABMEAT_VERSION_H_  */

