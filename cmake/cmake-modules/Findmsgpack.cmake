
set( MSGPACK_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/libs/msgpack/msgpack-c-0.6.0/include )
set( MSGPACK_LIBRARY cbme_msgpack )
if( MSGPACK_INCLUDE_DIR AND MSGPACK_LIBRARY )
    set( MSGPACK_FOUND TRUE )
else()
    set( MSGPACK_FOUND FALSE )
endif()

