
set( MODELS_CMF_DIR ${CMAKE_SOURCE_DIR}/models/cmf)
set( CMF_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/models/cmf ${CMAKE_SOURCE_DIR}/models/cmf/math/integrators/sundials_cvode/include)
set( CMF_LIBRARY model-cmf)
if( CMF_INCLUDE_DIR AND CMF_LIBRARY)
    set( CMF_FOUND TRUE)
endif()

