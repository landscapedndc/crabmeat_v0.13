# - Try to find METIS
# Once done, this will define
#
#  METIS_FOUND - system has METIS
#  METIS_INCLUDE_DIR - the METIS include directories
#  METIS_LIBRARIES - library to link to
#
#set( METIS_INCLUDE_SEARCH_PATHS
#    /usr/include
#    /usr/local/include
#    $ENV{HOME}/local/include )
#
#set( METIS_LIB_SEARCH_PATHS
#    /usr/lib
#    /usr/lib64
#    /usr/local/lib
#    /usr/local/lib64
#    $ENV{HOME}/local/lib
#    $ENV{HOME}/local/lib64 )
#
#find_path( METIS_INCLUDE_DIR NAMES metis.h
#        PATHS ${METIS_INCLUDE_SEARCH_PATHS} )
#
#if( NOT METIS_INCLUDE_DIR STREQUAL METIS_INCLUDE_DIR-NOTFOUND )
#    find_library( METIS_LIBRARIES NAMES metis
#            PATHS ${METIS_LIB_SEARCH_PATHS} )
#
#    if ( METIS_LIBRARIES STREQUAL METIS_LIBRARIES-NOTFOUND )
#        set(METIS_FOUND FALSE )
#    else()
#        set(METIS_FOUND TRUE )
#    endif()
#endif()

set(METIS_FOUND FALSE )
