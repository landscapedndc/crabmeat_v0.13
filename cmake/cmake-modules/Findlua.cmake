
set( LUA_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/libs/lua/lua-5.3.0/src)
set( LUA_LIBRARY cbme_lua)
if( LUA_INCLUDE_DIR AND LUA_LIBRARY)
    set( LUA_FOUND TRUE)
endif()

