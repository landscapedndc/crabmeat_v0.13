
set( BLITZ_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/libs/blitz/blitz-0.10 )
set( BLITZ_LIBRARY cbme_blitz )
if( BLITZ_INCLUDE_DIR AND BLITZ_LIBRARY )
    set( BLITZ_FOUND TRUE )
else()
    set( BLITZ_FOUND FALSE )
endif()

