# ======================================================================
#
#  LandscapeDNDC
#  -------------
# 
#  "A regional, modular biogeochemical land-surface model, based on
#   the DNDC concept of biogeochemical C and N cycling and various
#   other models/ submodels."
#
#     Karlsruhe Institute of Technology (KIT)
#     Institute of Meteorology and Climate Research -
#     Atmospheric Environmental Research (IMK-IFU)
# 
#     Kreuzeckbahnstr. 19
#     D-82467 Garmisch-Partenkirchen
#
# ======================================================================

cmake_minimum_required( VERSION 2.8.11...3.22.1 FATAL_ERROR)

project(crabmeat CXX C)

## just checking..
message( STATUS "running cmake generator front-end \"${CMAKE_GENERATOR}\"")

set( CMAKE_MACOSX_RPATH ON )

include( crabmeat.cmake-conf )
file( MAKE_DIRECTORY ${CMAKE_GENERATE_DIR})

## set package version
include( cmake/crabmeat-version.cmake-inc )
## set working directory revision number
include( cmake/crabmeat-revision.cmake-inc)

## run enabled generators
include( cmake/generate.cmake-inc )

## set compiler identification
include( cmake/compilers.cmake-inc )

## check for headers, functions and symbols
include( cmake/check-includes/check-includes.cmake-inc )
include( cmake/check-functions/check-functions.cmake-inc )


## try to determine available external libraries
include( cmake/packages.cmake-inc )
## non-interactively define settings
include( cmake/options.cmake-inc )
## non-interactively define settings
include( cmake/options-custom.cmake-inc )
## set site (i.e. source or build tree) specific variables
include( cmake/options-local.cmake-inc )
## check dependencies
include( cmake/options-check.cmake-inc )

## build 3rd party libraries
add_subdirectory( libs )

## load compiler specific settings (overwriting compiler
## setting via options file not supported)
include( cmake/compilers/compiler-options.cmake-inc )

## set release or debug build type
if( CRABMEAT_RELEASE)
    set( CMAKE_CXX_FLAGS_DEBUG)

    set( CMAKE_VERBOSE_MAKEFILE FALSE)
    set( CMAKE_BUILD_TYPE "release" CACHE INTERNAL "." FORCE)
else()
    set( CMAKE_VERBOSE_MAKEFILE TRUE)
    set( CMAKE_BUILD_TYPE "debug" CACHE INTERNAL "." FORCE)
endif()

if( TARGETS_SHARED_LIBRARIES_crabmeat)
    set( CRABMEAT_SOURCE_DIR ${CMAKE_SOURCE_DIR} )
    include( ${CMAKE_SOURCE_DIR}/cmake/functions.d/getversion.cmake-function )
    crabmeat_getversion( CRABMEAT_PACKAGE_MAJOR_VERSION CRABMEAT_PACKAGE_MINOR_VERSION CRABMEAT_PACKAGE_PATCH_LEVEL CRABMEAT_PACKAGE_VERSION )
endif( TARGETS_SHARED_LIBRARIES_crabmeat)

# ==============================================================================
#
#  specify subprojects
#
# ==============================================================================
add_subdirectory( common cbmbase )
add_subdirectory( logging cbmlog )
add_subdirectory( comp cbmcomp )
add_subdirectory( cpl cbmcpl )
add_subdirectory( scientific cbmscientific )
add_subdirectory( io cbmio )
add_subdirectory( domain cbm )
if ( TESTS_ENABLE_crabmeat )
    add_subdirectory( tests runtime/tests )
endif()

install( FILES ${CMAKE_SOURCE_DIR}/crabmeat.h ${CMAKE_SOURCE_DIR}/crabmeat-version.h ${CMAKE_SOURCE_DIR}/crabmeat-config.h
    ${CMAKE_SOURCE_DIR}/crabmeat-arch.h ${CMAKE_SOURCE_DIR}/crabmeat-compiler.h
    ${CMAKE_SOURCE_DIR}/crabmeat-dllexport.h ${CMAKE_SOURCE_DIR}/crabmeat-os.h
    DESTINATION include/crabmeat )

install( DIRECTORY ${CMAKE_GENERATE_DIR}/ DESTINATION include/crabmeat/inc FILES_MATCHING PATTERN "*.h.inc" )
install( DIRECTORY ${CMAKE_GENERATE_DIR}/ DESTINATION include/crabmeat/inc FILES_MATCHING PATTERN "*.cpp.inc" )

## additional targets
include( cmake/crabmeat-apidocs.cmake-inc )
include( cmake/crabmeat-mrproper.cmake-inc )
## summary of cmake configuration
include( cmake/crabmeat-configuration.cmake-inc )

## determine size of source basepath 
string( LENGTH "${CMAKE_SOURCE_DIR}" CMAKE_SOURCE_DIR_CHAR_CNT )
configure_file( "${CMAKE_SOURCE_DIR}/crabmeat-config.h.in"
    "${CMAKE_GENERATE_DIR}/crabmeat-config.h.inc" )

configure_file( "${CMAKE_SOURCE_DIR}/cmake/cmake-modules/Findcrabmeat.cmake.in"
    "${CMAKE_BINARY_DIR}/Findcrabmeat.cmake" @ONLY )
install( FILES ${CMAKE_BINARY_DIR}/Findcrabmeat.cmake
    DESTINATION share/CMake/Modules )

# ==============================================================================
#
#  configuration feedback
#
# ==============================================================================

message( STATUS "" )
message( STATUS "===========================================================================" )
message( STATUS "   CrabMEAT build configuration settings" )
message( STATUS "===========================================================================" )
message( STATUS "" )
string( REPLACE "\\n" "\n" CRABMEAT_BUILD_INFO_NOBS "${CRABMEAT_BUILD_INFO}" )
message( STATUS "${CRABMEAT_BUILD_INFO_NOBS}" )
message( STATUS "build API documentation (using doxygen)  [${CRABMEAT_API_DOCS}]" )
message( STATUS "install prefix  [${CMAKE_INSTALL_PREFIX}]" )
message( STATUS "\n\n" )

