/*!
 * @brief
 *    ldndc input class types
 *
 *    C API
 *
 * @author 
 *    steffen klatt  (created on: may 13, 2013)
 */

#ifndef  __C_API__LDNDC_INPUTCLASSTYPES_H_
#define  __C_API__LDNDC_INPUTCLASSTYPES_H_

#include  "crabmeat-common.h"
#include  "input/ic-types.h"

#ifdef  __cplusplus
extern "C"
{
#endif

/* resolve input class type from name */
input_class_type_e CBM_API  ldndc_resolve_input_class_type_by_name( char const *);

#ifdef  __cplusplus
}  /*  extern "C"  */
#endif

#endif  /*  !__C_API__LDNDC_INPUTCLASSTYPES_H_  */

