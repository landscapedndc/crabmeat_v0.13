/*!
 * @brief
 *    ldndc input class types
 *
 *    C API  (implementation)
 *
 * @author 
 *    steffen klatt  (created on: may 13, 2013)
 */

#include  "input/ic-types-c-api.h"
#include  "utils/lutils.h"

#ifdef  __cplusplus
extern "C"
{
#endif

input_class_type_e
ldndc_resolve_input_class_type_by_name(
                char const *  _ic_name)
{
        input_class_type_e  e( INPUT_CLASS_CNT);
        cbm::find_enum( _ic_name, INPUT_NAMES, INPUT_CLASS_CNT, &e);

        return  e;
}

#ifdef  __cplusplus
}  /*  extern "C"  */
#endif

