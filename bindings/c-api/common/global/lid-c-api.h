/*!
 * @brief
 *    ldndc object ID
 *
 *    C API
 *
 * @author 
 *    steffen klatt  (created on: may 10, 2013)
 */

#ifndef  __C_API__LDNDC_GLOBAL_ID_H_
#define  __C_API__LDNDC_GLOBAL_ID_H_

#include  "global/lid.h"

#ifdef  __cplusplus
extern "C"
{
#endif

        /* ldndc entity ID (e.g. identifier for kernels in input sources) */
        lid_t  CBM_API  ldndc_invalid_id();

#ifdef  __cplusplus
}  /*  extern "C"  */
#endif

#endif  /*  !__C_API__LDNDC_GLOBAL_ID_H_  */

