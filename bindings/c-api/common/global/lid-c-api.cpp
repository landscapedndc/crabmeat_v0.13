/*!
 * @brief
 *    ldndc object ID
 *
 *    C API  (implementation)
 *
 * @author 
 *    steffen klatt  (created on: may 10, 2013)
 */

#include  "global/lid-c-api.h"

#ifdef  __cplusplus
extern "C"
{
#endif

        lid_t  ldndc_invalid_id()
        {
                return  ldndc::invalid_t< lid_t >::value;
        }

#ifdef  __cplusplus
}  /*  extern "C"  */
#endif

