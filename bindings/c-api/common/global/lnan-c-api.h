/*!
 * @brief
 *    defines not-a-number constants
 *
 *    C API
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 */

#ifndef  __C_API__LDNDC_GLOBAL_NAN_H_
#define  __C_API__LDNDC_GLOBAL_NAN_H_

#include  "crabmeat-common.h"

#ifdef __cplusplus
extern  "C"
{
#endif
#define  __C_API__LDNDC_INVALID_VAL_DECL(__type__,__name__)  __type__  CBM_API  ldndc_invalid_##__name__()/*;*/

    __C_API__LDNDC_INVALID_VAL_DECL(char,char);
    __C_API__LDNDC_INVALID_VAL_DECL(unsigned char,unsigned_char);
    __C_API__LDNDC_INVALID_VAL_DECL(short int,short_int);
    __C_API__LDNDC_INVALID_VAL_DECL(unsigned short int,unsigned_short_int);
    __C_API__LDNDC_INVALID_VAL_DECL(int,int);
    __C_API__LDNDC_INVALID_VAL_DECL(unsigned int,unsigned_int);
    __C_API__LDNDC_INVALID_VAL_DECL(long int,long_int);
    __C_API__LDNDC_INVALID_VAL_DECL(unsigned long int,unsigned_long_int);
    __C_API__LDNDC_INVALID_VAL_DECL(long long int,long_long_int);
    __C_API__LDNDC_INVALID_VAL_DECL(unsigned long long int,unsigned_long_long_int);

    __C_API__LDNDC_INVALID_VAL_DECL(float,float);
    __C_API__LDNDC_INVALID_VAL_DECL(double,double);

#ifdef  __cplusplus
}  /*  extern "C"  */
#endif

#endif  /*  !__C_API__LDNDC_GLOBAL_NAN_H_  */

