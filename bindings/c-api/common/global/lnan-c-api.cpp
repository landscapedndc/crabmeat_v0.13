/*!
 * @brief
 *    defines not-a-number constants
 *
 *    C API (implementation)
 *
 * @author 
 *    steffen klatt  (created on: jan 11, 2012),
 */

#include  "global/lnan-c-api.h"

#ifdef  __cplusplus
extern  "C"
{
#endif

#define  __C_API__LDNDC_INVALID_VAL_DEFN(__type__,__name__)     \
    __type__  ldndc_invalid_##__name__()                    \
        {                                                       \
                return  ldndc::invalid_t< __type__ >::value;    \
        }

    __C_API__LDNDC_INVALID_VAL_DEFN(char,char)
    __C_API__LDNDC_INVALID_VAL_DEFN(unsigned char,unsigned_char)
    __C_API__LDNDC_INVALID_VAL_DEFN(short int,short_int)
    __C_API__LDNDC_INVALID_VAL_DEFN(unsigned short int,unsigned_short_int)
    __C_API__LDNDC_INVALID_VAL_DEFN(int,int)
    __C_API__LDNDC_INVALID_VAL_DEFN(unsigned int,unsigned_int)
    __C_API__LDNDC_INVALID_VAL_DEFN(long int,long_int)
    __C_API__LDNDC_INVALID_VAL_DEFN(unsigned long int,unsigned_long_int)
    __C_API__LDNDC_INVALID_VAL_DEFN(long long int,long_long_int)
    __C_API__LDNDC_INVALID_VAL_DEFN(unsigned long long int,unsigned_long_long_int)

    __C_API__LDNDC_INVALID_VAL_DEFN(float,float)
    __C_API__LDNDC_INVALID_VAL_DEFN(double,double)

#ifdef  __cplusplus
}  /*  extern "C"  */
#endif

