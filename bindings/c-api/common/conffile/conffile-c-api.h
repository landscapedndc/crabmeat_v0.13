/*!
 * @brief
 *    interface to configuration file
 *
 *    C API
 *
 * @author
 *    steffen klatt (created on: apr 15, 2013)
 */

#ifndef  __C_API__LDNDC_CONFIGFILE_H_
#define  __C_API__LDNDC_CONFIGFILE_H_

/* wrapper for configuration file */
#include  "conffile/conffile.h"

#ifdef __cplusplus
extern  "C"
{
#endif

typedef  struct ldndc::config_file_t  ldndc_config_file_t;

ldndc_config_file_t CBM_API *  ldndc_config_file_create();

void CBM_API  ldndc_config_file_destroy(
        ldndc_config_file_t *);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

#endif  /*  !__C_API__LDNDC_CONFIGFILE_H_  */

