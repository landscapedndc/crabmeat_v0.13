/*!
 * @brief
 *    interface to the ldndc top-level project file
 *
 *    C API  (implementation)
 *
 * @author
 *    steffen klatt (created on: may 14, 2013),
 *    edwin haas
 */

#include  "projectfile/projectfile-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

ldndc_project_file_t *
ldndc_projectfile_create()
{
    return  ldndc::project_file_t::new_instance();
}
void
ldndc_projectfile_destroy(
        ldndc_project_file_t *  _projectfile)
{
    if ( _projectfile)
    {
        _projectfile->delete_instance();
    }
}

lerr_t
ldndc_projectfile_read(
        ldndc_project_file_t *  _projectfile,
        char const *  _filename)
{
    if ( _projectfile)
    {
        return  _projectfile->read( _filename);
    }
    return  LDNDC_ERR_FAIL;
}

//char const *  ldndc_projectfile_schedule(
//        ldndc_project_file_t *);

//ldndc::stream_attributes_t const *  ldndc_projectfile_stream_attributes(
//        ldndc_project_file_t *);
//        input_class_type_e);
//lid_t  ldndc_projectfile_default_id(
//        ldndc_project_file_t *);
//        input_class_type_e);

//char const *  ldndc_projectfile_stream_prefix(
//        ldndc_project_file_t *);

#ifdef __cplusplus
}
#endif

