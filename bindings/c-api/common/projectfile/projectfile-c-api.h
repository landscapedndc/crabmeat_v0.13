/*!
 * @brief
 *    interface to the ldndc top-level project file
 *
 *    C API
 *
 * @author
 *    steffen klatt (created on: may 14, 2013),
 *    edwin haas
 */

#ifndef  __C_API__LDNDC_PROJECTFILE_H_
#define  __C_API__LDNDC_PROJECTFILE_H_

#include  "projectfile/projectfile.h"

#ifdef __cplusplus
extern  "C"
{
#endif

typedef  struct ldndc::project_file_t  ldndc_project_file_t;

ldndc_project_file_t CBM_API *  ldndc_projectfile_create();

void CBM_API  ldndc_projectfile_destroy(
        ldndc_project_file_t *);

lerr_t CBM_API  ldndc_projectfile_read(
        ldndc_project_file_t *,
        char const *);

//char const *  ldndc_projectfile_schedule(
//        ldndc_project_file_t *);

//ldndc::stream_attributes_t const *  ldndc_projectfile_stream_attributes(
//        ldndc_project_file_t *);
//        input_class_type_e);
//lid_t  ldndc_projectfile_default_id(
//        ldndc_project_file_t *);
//        input_class_type_e);

//char const *  ldndc_projectfile_stream_prefix(
//        ldndc_project_file_t *);

#ifdef __cplusplus
}
#endif

#endif  /*  !__C_API__LDNDC_PROJECTFILE_H_  */

