/*!
 * @brief
 *    high level interface to i/o subsystem (implementation)
 *
 *    C API
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011)
 */

#include  "client/io/io-service-client-c-api.h"
#include  "input/ic-headers-client.h"

#ifdef __cplusplus
extern  "C"
{
#endif

io_comm_t *  io_services_create(
        lid_t  _id)
{
    return  ldndc::io_comm_t::new_instance( _id);
}

void  io_services_destroy(
        io_comm_t *  _io)
{
    _io->delete_instance();
}

// sk:??    void  set_project( ldndc::project_t *);

lid_t  io_services_open_project(
        io_comm_t *  _io,
        char const *  _project_file_name)
{
    return  _io->open_project( _project_file_name);
}

// sk:??    void  io_services_set_schedule( ltime_t const *);
// sk:??    ltime_t const *  get_schedule() const;

// sk:??    lerr_t  io_services_update_input_class_states( lclock_t const &);

// sk:off    lid_t  io_services_get_source_id(
// sk:off            io_comm_t *  _io,
// sk:off            lid_t  _id,
// sk:off            input_class_type_e  _ic_type)
// sk:off    {
// sk:off        return  _io->get_source_id( _id, _ic_type);
// sk:off    }
// sk:off
// sk:off    input_class_base_t const *  io_services_get_input_class(
// sk:off            io_comm_t *  _io,
// sk:off            lid_t  _id,
// sk:off            input_class_type_e  _ic_type)
// sk:off    {
// sk:off        return  _io->get_input_class( _id, _ic_type);
// sk:off    }
// sk:off    input_class_base_t const *  io_services_new_input_class(
// sk:off            io_comm_t *  _io,
// sk:off            lid_t  _id,
// sk:off            input_class_type_e  _ic_type)
// sk:off    {
// sk:off        return  _io->new_input_class( _id, _ic_type);
// sk:off    }
// sk:off
// sk:off    void  io_services_delete_input_class(
// sk:off            io_comm_t *  _io,
// sk:off            input_class_base_t const *  _ic)
// sk:off    {
// sk:off        _io->delete_input_class( _ic);
// sk:off    }

// sk:??        /*!
// sk:??         * @brief
// sk:??         *    return copy of client global input class
// sk:??         *    of type @p _I
// sk:??         */
// sk:??        template < typename  _I >
// sk:??        _I  get_input_class_global(
// sk:??                lid_t const &  /*stream id*/);
// sk:??        /*!
// sk:??         * @brief
// sk:??         *    return new'd instance of client global
// sk:??         *    input class of type @p _I (free with 
// sk:??         *    @fn delete_input_class_global)
// sk:??         */
// sk:??        template < typename  _I >
// sk:??        _I *  new_input_class_global(
// sk:??                lid_t const &  /*stream id*/);
// sk:??        /*!
// sk:??         * @brief
// sk:??         *    delete an instance of global input class
// sk:??         *    created via method @fn new_input_class_global
// sk:??         */
// sk:??        void  delete_input_class_global(
// sk:??                input_class_global_base_t const *);


    void  io_services_object_id_list(
            io_comm_t *  _io,
            input_class_type_e  _ic_type,
            lid_t **  _id_list,
            int *  _size)
    {
        std::vector< lid_t >  id_l;

        switch ( _ic_type)
        {
            case INPUT_AIRCHEMISTRY:
                id_l = _io->object_id_list< input_class_airchemistry_t >();
                break;
            case INPUT_CLIMATE:
                id_l = _io->object_id_list< input_class_climate_t >();
                break;
            case INPUT_EVENT:
                id_l = _io->object_id_list< input_class_event_t >();
                break;
            case INPUT_SETUP:
                id_l = _io->object_id_list< input_class_setup_t >();
                break;
            case INPUT_SITE:
                id_l = _io->object_id_list< input_class_site_t >();
                break;
            case INPUT_SITEPARAMETERS:
                id_l = _io->object_id_list< input_class_siteparameters_t >();
                break;
            case INPUT_SOILPARAMETERS:
                id_l = _io->object_id_list< input_class_soilparameters_t >();
                break;
            case INPUT_SPECIESPARAMETERS:
                id_l = _io->object_id_list< input_class_speciesparameters_t >();
                break;

            default:
                CBM_LogFatal( "[BUG]  invalid input class type");
        }

        if ( id_l.size() > 0)
        {
            *_id_list = new lid_t[id_l.size()];
            if ( ! *_id_list)
            {
                return;
            }
            if ( _size)
            {
                *_size = id_l.size();
            }
            for ( size_t  j = 0;  j < id_l.size();  ++j)
            {
                (*_id_list)[j] = id_l[j];
            }
        }
    }
    void  io_services_object_id_list_by_name(
            io_comm_t *  _io,
            char const *  _ic_type,
            lid_t **  _id_list,
            int *  _size)
    {
        input_class_type_e  ic = INPUT_CNT;
        cbm::find_enum( _ic_type, INPUT_NAMES, INPUT_CNT, &ic);
        if ( ic < INPUT_CNT)
        {
            io_services_object_id_list( _io, ic, _id_list, _size);
            return;
        }
    }

// sk:??        std::vector< lid_t >  object_id_list(
// sk:??                ldndc::work_dispatcher_t * = NULL);
// sk:??        /*!
// sk:??         * @brief
// sk:??         *    return list of object IDs of type
// sk:??         *    setup to be aquired for this region,
// sk:??         *    i.e. possibly prescribed by work
// sk:??         *    dispatcher
// sk:??         */
// sk:??        std::vector< lid_t >  request_region(
// sk:??                ldndc::work_dispatcher_t * = NULL);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

