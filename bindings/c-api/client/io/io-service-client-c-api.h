/*!
 * @brief
 *    interface to inputs; communication interface for
 *    all i/o in monolithic build.
 *
 *    C API
 *
 * @author
 *    steffen klatt (created on: apr 13, 2013)
 */

#ifndef  __C_API__LDNDC_IO_SERVICES_CLIENT_H_
#define  __C_API__LDNDC_IO_SERVICES_CLIENT_H_

/* wrapper for i/o services */
#include  "io/io-comm.h"

/* c api */
#include  "lproject-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

typedef  struct ldndc::io_comm_t  ldndc_io_comm_t;

ldndc_io_comm_t CBM_API *  ldndc_io_services_create(
        lid_t);

void CBM_API  ldndc_io_services_destroy(
        ldndc_io_comm_t *);

void CBM_API  ldndc_io_services_set_project(
        ldndc_project_t *);

lid_t CBM_API  ldndc_io_services_open_project(
        ldndc_io_comm_t *,
        char const *  /*project file name*/);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

#endif  /*  !__C_API__LDNDC_IO_SERVICES_CLIENT_H_  */

