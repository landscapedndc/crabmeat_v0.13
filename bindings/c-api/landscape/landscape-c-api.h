/*!
 * @brief
 *    C API
 *
 * @author
 *    steffen klatt (created: may 15, 2013),
 *    edwin haas
 */

#ifndef  __C_API__LDNDC_LANDSCAPE_H_
#define  __C_API__LDNDC_LANDSCAPE_H_

/* wrapper for landscape */
#include  "landscape/landscape.h"

/* c api */
#include  "conffile/conffile-c-api.h"
#include  "client/io/io-service-client-c-api.h"


#ifdef __cplusplus
extern  "C"
{
#endif


typedef  ldndc::llandscape_t  ldndc_landscape_t;

ldndc_landscape_t CBM_API *  ldndc_landscape_create( lid_t);
void CBM_API  ldndc_landscape_destroy(
        ldndc_landscape_t *);

void CBM_API  ldndc_landscape_set_configuration(
        ldndc_landscape_t *,
        ldndc_config_file_t const *);
void CBM_API  ldndc_landscape_set_schedule(
        ldndc_landscape_t *,
        ltime_t const *);
lerr_t CBM_API  ldndc_landscape_initialize(
        ldndc_landscape_t *,
        ldndc_io_comm_t *
/* sk:later        ldndc_work_dispatcher_t * = NULL*/);

lerr_t CBM_API  ldndc_landscape_launch(
        ldndc_landscape_t *,
        unsigned int = 0u);

lerr_t CBM_API  ldndc_landscape_finalize(
        ldndc_landscape_t *);

unsigned int CBM_API  ldndc_landscape_get_timestep(
        ldndc_landscape_t *);

size_t CBM_API  ldndc_landscape_get_full_cores_cnt(
        ldndc_landscape_t *);

size_t CBM_API  ldndc_landscape_get_valid_cores_cnt(
        ldndc_landscape_t *);

size_t CBM_API  ldndc_landscape_get_active_cores_cnt(
        ldndc_landscape_t *);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif



#endif  /*  !__C_API__LDNDC_LANDSCAPE_H_  */

