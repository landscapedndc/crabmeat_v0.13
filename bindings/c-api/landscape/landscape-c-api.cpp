/*!
 * @brief
 *    C API  (implementation)
 *
 * @author
 *    steffen klatt (created: may 15, 2013),
 *    edwin haas
 */

#include  "landscape/landscape-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

ldndc_landscape_t *
ldndc_landscape_create( lid_t  _id)
{
        return  ldndc_landscape_t::new_instance( _id);
}

void
ldndc_landscape_destroy(
        ldndc_landscape_t *)
{
        if ( _ls)
        {
                _ls->delete_instance();
        }
}

void
ldndc_landscape_set_configuration(
        ldndc_landscape_t *,
        ldndc::config_file_t const *)
{
}

void
ldndc_landscape_set_schedule(
        ldndc_landscape_t *,
        ltime_t const *)
{
}

lerr_t
ldndc_landscape_initialize(
        ldndc_landscape_t *,
        io_comm_t *,
        work_dispatcher_t * = NULL)
{
        return  LDNDC_ERR_FAIL;
}

lerr_t
ldndc_landscape_launch(
        ldndc_landscape_t *,
        unsigned int = 0u)
{
        return  LDNDC_ERR_FAIL;
}

lerr_t
ldndc_landscape_finalize(
        ldndc_landscape_t *)
{
        return  LDNDC_ERR_FAIL;
}

unsigned int
ldndc_landscape_get_timestep(
        ldndc_landscape_t *)
{
    if ( _ls
}


size_t
ldndc_landscape_get_full_cores_cnt(
        ldndc_landscape_t *)
{
        return  0;
}

size_t
ldndc_landscape_get_valid_cores_cnt(
        ldndc_landscape_t *)
{
        return  0;
}

size_t
ldndc_landscape_get_active_cores_cnt(
        ldndc_landscape_t *)
{
        return  0;
}

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

