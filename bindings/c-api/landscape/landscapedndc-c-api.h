/*!
 * @brief
 *    C API
 *
 * @author
 *    steffen klatt (created: may 21, 2014),
 *    edwin haas
 */

#ifndef  __C_API__LDNDC_LANDSCAPEDNDC_H_
#define  __C_API__LDNDC_LANDSCAPEDNDC_H_

/* wrapper for landscapedndc */
#include  "landscape/landscapedndc.h"

#ifdef __cplusplus
extern  "C"
{
#endif

typedef  ldndc::llandscapedndc_t  ldndc_llandscapedndc_t;

#ifdef __cplusplus
}  /*  extern "C"  */
#endif



#endif  /*  !__C_API__LDNDC_LANDSCAPEDNDC_H_  */

