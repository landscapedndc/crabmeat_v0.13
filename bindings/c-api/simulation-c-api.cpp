/*!
 * @brief
 *    C API  (implementation)
 *
 * @author
 *    steffen klatt (created: may 15, 2013),
 *    edwin haas
 */

#include  "simulation-c-api.h"
#include  "string/lstring-transform.h"

#include  "app/lapps-common.h"
#include  "lkernels.h"
#include  "lkernels-link.h"

#include  <stdio.h>

#ifdef __cplusplus
extern  "C"
{
#endif

ldndc_simulation_t *
ldndc_simulation_create(
        lid_t  _id,
        char const *  _project_fname, char const *  _config_fname,
        char const *  _schedule)
{
    ldndc_simulation_t *  lsim = new ldndc_simulation_t();
    if ( lsim)
    {
        lsim->ldndc = new  ldndc_llandscapedndc_t(
                _project_fname, _schedule, _config_fname, NULL);
        if ( !lsim->ldndc)
        {
            fprintf( stderr, "nomem\n");
            delete  lsim;
            return  NULL;
        }
        else if ( lsim->ldndc->status != 0)
        {
            fprintf( stderr, "ldndc initialization failed\n");
            delete  lsim->ldndc;
            delete  lsim;
            return  NULL;
        }
    }
    else
    {
        fprintf( stderr, "nomem\n");
        return  NULL;
    }

    lsim->ldndc->region.set_object_id( _id);

        return  lsim;
}

void
ldndc_simulation_destroy(
                ldndc_simulation_t *  _lsim)
{
        if ( _lsim)
        {
        if ( _lsim->ldndc)
        {
            delete  _lsim->ldndc;
        }
                delete  _lsim;
        }
}

lerr_t
ldndc_simulation_launch(
                ldndc_simulation_t *  _lsim,
        long  _timesteps)
{
    if ( !_lsim)
    {
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_launch = _lsim->ldndc->step( _timesteps);
        return  rc_launch;
}

void
ldndc_simulation_finalize(
        ldndc_simulation_t *  _lsim)
{
    if ( _lsim)
    {
        _lsim->ldndc->finalize();
    }
}

unsigned int
ldndc_simulation_get_timestep(
                ldndc_simulation_t *  _lsim)
{
    return  _lsim->ldndc->lclock()->time_step();
}

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

