/*!
 * @brief
 *    interface to event input
 *
 *    C API
 *
 * @author
 *    steffen klatt (created on: may 13, 2013)
 */

#ifndef  __C_API__LDNDC_INPUT_EVENT_H_
#define  __C_API__LDNDC_INPUT_EVENT_H_

#include  "client/ldndc-client.h"
#include  "input/event/event.h"
#include  "c-api/client/io/io-service-client-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

    int  CBM_API  input_event_get_event_handles_cnt(
            io_comm_t *,
            lid_t);

    lerr_t  CBM_API  input_event_get_event_handles(
            io_comm_t *  /*io*/,
            lid_t  _id  /*event object id*/,
            event_handle_t *,
            size_t  _n);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

#endif  /*  !__C_API__LDNDC_INPUT_EVENT_H_  */

