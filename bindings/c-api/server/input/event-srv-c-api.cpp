/*!
 * @brief
 *    interface to event input (implementation)
 *
 *    C API
 *
 * @author
 *    steffen klatt (created on: may 13, 2013)
 */

#include  "c-api/server/input/event-srv-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

int  CBM_API  input_event_get_event_handles_cnt(
        io_comm_t *  _io,
        lid_t  _id)
{
    crabmeat_assert( _io);

    input_class_event_t *  event = _io->new_input_class< input_class_event_t >( _id);
    if ( event)
    {
        int  event_handles_cnt = (int)event->get_event_handles_cnt();
        _io->delete_input_class( event);

        return  event_handles_cnt;
    }
    return  0;
}

lerr_t  CBM_API  input_event_get_event_handles(
        io_comm_t *  _io,
        lid_t  _id,
        event_handle_t *  _buf,
        size_t  _n)
{
    crabmeat_assert( _io);

    input_class_event_t *  event = _io->new_input_class< input_class_event_t >( _id);
    if ( event)
    {
        event->get_event_handles( _buf, _n);
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

