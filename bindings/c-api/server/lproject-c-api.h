/*!
 * @brief
 *    C API
 *
 * @author
 *    steffen klatt (created on: may 14, 2013)
 */

#ifndef  __C_API__LDNDC_IO_PROJECT_H_
#define  __C_API__LDNDC_IO_PROJECT_H_

/* wrapper for ldndc project */
#include  "simdrv/lproject.h"

/* c api */
#include  "projectfile/projectfile-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

typedef  struct ldndc::project_t  ldndc_project_t;
typedef  struct ldndc::input_class_srv_base_t  ldndc_input_class_srv_base_t;
typedef  struct ldndc::input_class_global_srv_base_t  ldndc_input_class_global_srv_base_t;

ldndc_project_t CBM_API *  ldndc_project_create(
        lid_t  _id);

void CBM_API  ldndc_project_destroy(
        ldndc_project_t *);

void CBM_API  ldndc_project_set_projectfile(
        ldndc_project_t *,
        ldndc_project_file_t const * /*project file*/);
ldndc_project_file_t CBM_API const * get_projectfile(
        ldndc_project_t *);

int CBM_API  ldndc_project_get_schedule(
        ldndc_project_t *,
        char []/*schedule buffer*/, size_t /*max schedule size*/);

lid_t CBM_API  ldndc_project_attach_stream(
        ldndc_project_t *,
        input_class_type_e /*input class type*/,
        io_format_type_e /*format*/,
        char const * /*stream source descriptor*/);
// sk:todo    lerr_t  detach_stream(
// sk:todo        ldndc_project_t *,
// sk:todo        lid_t const & /*stream id*/);

//void  ldndc_project_detach_all_streams();

//size_t  number_of_open_streams( ldndc_project_t *);
//size_t  number_of_open_streams( ldndc_project_t *, input_class_type_e);

//size_t  number_of_core_blocks( ldndc_project_t *);
//size_t  number_of_core_blocks( ldndc_project_t *, input_class_type_e);

lerr_t CBM_API  ldndc_project_open_project(
        ldndc_project_t *);
//lerr_t  close_project(
//        ldndc_project_t *);

ldndc_input_class_srv_base_t CBM_API *  ldndc_project_get_input_class(
        ldndc_project_t *,
        lid_t  /*object id*/,
        input_class_type_e  /*input class type*/);
//        lclock_t const * = NULL /*simulation clock*/);
            
lid_t CBM_API  ldndc_project_get_source_id(
        ldndc_project_t *,
        lid_t  /*kernel id*/,
        input_class_type_e  /*input class type*/);

ldndc_input_class_global_srv_base_t CBM_API *  ldndc_project_get_input_class_global(
        ldndc_project_t *,
        lid_t  /*stream id*/,
        input_class_type_e  /*input class type*/);

void CBM_API  ldndc_project_get_object_id_list(
        ldndc_project_t *,
        input_class_type_e  /*input class type*/,
        lid_t **  /*result id list*/,
        int * /*result id list size*/);

void CBM_API  ldndc_project_object_id_list_by_name(
        ldndc_project_t *,
        char const *,
        lid_t **,
        int *);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif


#endif  /*  !__C_API__LDNDC_IO_PROJECT_H_  */

