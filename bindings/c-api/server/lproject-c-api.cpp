/*!
 * @brief
 *    C API  (implementation)
 *
 * @author
 *    steffen klatt (created on: may 14, 2013)
 */

#include  "lproject-c-api.h"
#include  "io/input-source.h"
#include  "input/ic-headers-srv.h"

#include  "time/ldate.h"
#include  "global/global.h"
#include  "app/lapps-common.h"

#ifdef __cplusplus
extern  "C"
{
#endif

ldndc_project_t *
ldndc_project_create(
        lid_t  _id)
{
    return  ldndc::project_t::new_instance( _id);
}

void
ldndc_project_destroy(
        ldndc_project_t *  _project)
{
    if ( _project)
    {
        _project->delete_instance();
    }
}

void
ldndc_project_set_projectfile(
        ldndc_project_t *  _project,
        ldndc_project_file_t const * _projectfile)
{
    if ( _project)
    {
        ldndc::config_file_t  cf( lconf::CONFIG_FILE);
        /* ignore return status ... */
        cf.read();

        _project->set_configuration( &cf);
        ldndc_set_libconf_from_configurationfile( &cf);

        _project->set_project_file( _projectfile);
    }
}
ldndc_project_file_t const *
get_projectfile(
        ldndc_project_t *  _project)
{
    if ( _project)
    {
        return  _project->get_project_file();
    }
    return  NULL;
}

int
ldndc_project_get_schedule(
        ldndc_project_t *  _project,
        char  _schedule[], size_t  _schedule_max_size)
{
    if ( _project && _project->get_project_file())
    {
        if ( _project->get_project_file()->schedule())
        {
            /* pipe through ltime to normalize representation */
            ltime_t  lsched;
            lerr_t  rc_schedule = lsched.from_string( _project->get_project_file()->schedule());
            if ( rc_schedule)
            {
                return  0;
            }

            int  schedule_size = (int)lsched.to_string_long().size() + 1 /*null termination*/;
            if ( (int)_schedule_max_size < schedule_size)
            {
                return  0;
            }
            cbm::strncpy( _schedule, lsched.to_string_long().c_str(), schedule_size);
            _schedule[schedule_size] = '\0';
            return  schedule_size;
        }
    }
    return  0;
}


lid_t
ldndc_project_attach_stream(
        ldndc_project_t *  _project,
        input_class_type_e  _type,
        io_format_type_e  _io_format,
        char const *  _stream_source)
{
    if ( _project)
    {
        ldndc::source_info_t  source_info( _type);
        source_info.format = _io_format;
        source_info.name = _stream_source;
        return  _project->attach_source( &source_info);
    }
    return  invalid_lid;
}

// sk:todo    lerr_t  detach_stream(
// sk:todo        ldndc_project_t *,
// sk:todo        lid_t const & /*stream id*/);

//void  ldndc_project_detach_all_streams();

//size_t  number_of_open_streams( ldndc_project_t *);
//size_t  number_of_open_streams( ldndc_project_t *, input_class_type_e);

//size_t  number_of_core_blocks( ldndc_project_t *);
//size_t  number_of_core_blocks( ldndc_project_t *, input_class_type_e);

lerr_t
ldndc_project_open_project(
        ldndc_project_t *  _project)
{
    if ( _project)
    {
        /* set reference time if exists */
        if ( _project->get_project_file())
        {
            ltime_t  ref_time;
            ref_time.from_string( _project->get_project_file()->schedule());
            _project->set_reference_time( ref_time);
        }
        return  _project->open_project();
    }
    return  LDNDC_ERR_FAIL;
}
//lerr_t  close_project(
//        ldndc_project_t *);

ldndc_input_class_srv_base_t *
ldndc_project_get_input_class(
        ldndc_project_t *  _project,
        lid_t  _id,
        input_class_type_e  _type)
//        lclock_t const * = NULL /*simulation clock*/);
{
    if ( _project)
    {
        cbm::source_descriptor_t  s;
        cbm::set_source_descriptor_defaults( &s, _type, _id);
        _project->get_input_class( &s, _type);
    }
    return  NULL;
}
            
lid_t
ldndc_project_get_source_id(
        ldndc_project_t *  _project,
        lid_t  _kernel_id,
        input_class_type_e  _type)
{
    if ( _project)
    {
        cbm::source_descriptor_t  s;
        cbm::source_descriptor_t  k;
        cbm::set_source_descriptor_defaults( &k, _type, _kernel_id);
        return  _project->get_source_descriptor( &s, &k, _type);
    }
    return  invalid_lid;
}

ldndc_input_class_global_srv_base_t *
ldndc_project_get_input_class_global(
        ldndc_project_t *  _project,
        lid_t  _stream_id,
        input_class_type_e  _type)
{
    if ( _project)
    {
        return  _project->get_input_class_global( _stream_id, _type);
    }
    return  NULL;
}

void
ldndc_project_get_object_id_list(
        ldndc_project_t *  /*_project*/,
        input_class_type_e  /*_ic_type*/,
        lid_t **  _id_list,
        int *  _size)
{
// sk:off    std::vector< lid_t >  id_l;
// sk:off
// sk:off    switch ( _ic_type)
// sk:off    {
// sk:off        case INPUT_AIRCHEMISTRY:
// sk:off            id_l = _project->object_id_list< ldndc::airchemistry::input_class_airchemistry_srv_t >();
// sk:off            break;
// sk:off        case INPUT_CLIMATE:
// sk:off            id_l = _project->object_id_list< ldndc::climate::input_class_climate_srv_t >();
// sk:off            break;
// sk:off        case INPUT_EVENT:
// sk:off            id_l = _project->object_id_list< ldndc::event::input_class_event_srv_t >();
// sk:off            break;
// sk:off        case INPUT_SETUP:
// sk:off            id_l = _project->object_id_list< ldndc::setup::input_class_setup_srv_t >();
// sk:off            break;
// sk:off        case INPUT_SITE:
// sk:off            id_l = _project->object_id_list< ldndc::site::input_class_site_srv_t >();
// sk:off            break;
// sk:off        case INPUT_SITEPARAMETERS:
// sk:off            id_l = _project->object_id_list< ldndc::siteparameters::input_class_siteparameters_srv_t >();
// sk:off            break;
// sk:off        case INPUT_SOILPARAMETERS:
// sk:off            id_l = _project->object_id_list< ldndc::soilparameters::input_class_soilparameters_srv_t >();
// sk:off            break;
// sk:off        case INPUT_SPECIESPARAMETERS:
// sk:off            id_l = _project->object_id_list< ldndc::speciesparameters::input_class_speciesparameters_srv_t >();
// sk:off            break;
// sk:off
// sk:off        default:
// sk:off            CBM_LogFatal( "[BUG]  invalid input class type");
// sk:off    }

    std::vector< lid_t >  id_l;
    id_l.push_back( 0);
    if ( id_l.size() > 0)
    {
        *_id_list = new lid_t[id_l.size()];
        if ( ! *_id_list)
        {
            return;
        }
        if ( _size)
        {
            *_size = id_l.size();
        }
        for ( size_t  j = 0;  j < id_l.size();  ++j)
        {
            (*_id_list)[j] = id_l[j];
        }
    }
}
void
ldndc_project_object_id_list_by_name(
        ldndc_project_t *  _project,
        char const *  _ic_type,
        lid_t **  _id_list,
        int *  _size)
{
    input_class_type_e  ic = INPUT_CNT;
    cbm::find_enum( _ic_type, INPUT_NAMES, INPUT_CNT, &ic);
    if ( ic < INPUT_CNT)
    {
        ldndc_project_get_object_id_list( _project, ic, _id_list, _size);
        return;
    }
}

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

