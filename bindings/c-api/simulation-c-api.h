/*!
 * @brief
 *    C API
 *
 * @author
 *    steffen klatt (created: may 15, 2013),
 *    edwin haas
 */

#ifndef  __C_API__LDNDC_SIMULATION_H_
#define  __C_API__LDNDC_SIMULATION_H_

/* c api */
#include  "landscape/landscapedndc-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

typedef  struct  ldndc_simulation_t_s
{
    ldndc_llandscapedndc_t *  ldndc;

}  ldndc_simulation_t;

ldndc_simulation_t CBM_API *  ldndc_simulation_create(
        lid_t  /*id*/,
        char const * /*project filename*/,
        char const * /*configuration filename*/,
        char const * /*schedule*/);

void CBM_API  ldndc_simulation_destroy(
                ldndc_simulation_t *);

lerr_t CBM_API  ldndc_simulation_launch(
                ldndc_simulation_t *,
        long  /*timesteps*/);

void CBM_API  ldndc_simulation_finalize(
                ldndc_simulation_t *);

unsigned int CBM_API  ldndc_simulation_get_timestep(
                ldndc_simulation_t *);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif



#endif  /*  !__C_API__LDNDC_SIMULATION_H_  */

