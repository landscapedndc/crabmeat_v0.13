@echo off
REM Configure with cmake for SWIG generation
cmake -G "NMake Makefiles" -DTARGETS_PYTHON_API_ldndc:BOOL=ON
if ERRORLEVEL 1 goto error

nmake ldndc-binding-python
if ERRORLEVEL 1 goto error

goto end
:error
echo "Build stopped due to errors"
:end
echo .

