# -*- coding: utf-8 -*-
"""
Script to generate python docstrings from state.txt

@author: philipp kraft
"""

fin = file( '${CMAKE_SOURCE_DIR}/kernels/mobile/state/state.txt')
lastline=''
print '*' * 60
print ' generating docstring.i from state.txt'
print '${MOBILE_SUBSTATES}'
mobile_substates = '${MOBILE_SUBSTATES}'.split(';')
classnames = dict()
for ss_long in mobile_substates :
    classnames[ss_long]=ss_long
print  classnames
def classname(target):
    return 'ldndc::kernels::mobile::substate_' + classnames[target] + '_t'
    
class substatedoc:
    def __init__(self,target,field,dtype,size,unit,lastline=''):
        self.target = target.strip()
        self.field = field.strip()
        self.size = size.strip()
        self.dtype = dtype.strip()
        self.unit = unit.strip().replace(':','*')
        self.desc = lastline.strip('#').strip().replace('"',"'")
    def __repr__(self):
        return classnames[self.target] + '.' + self.field
    @property     
    def name(self):
        return repr(self)
    def __cmp__(self,other):
        return cmp(repr(self),repr(other))
        
        
        
classdoc = {}        
for lineno,l in enumerate(fin):
    try:
        if l.strip() == '':
            continue
        elif l.strip().startswith('#'):
            lastline = l
            continue
        else:
            target, ident, field, dtype, size, init, unit =l.split(None,6)
            classdoc.setdefault(target,[]).append(
                substatedoc(
                            target,field,dtype,size,unit,lastline
                ))
            lastline =''
    except Exception as e:
        raise RuntimeError('line %i: %s' % (lineno,e))
for k,v in classdoc.iteritems():
    v.sort()

fout = file( '${LIBRARY_OUTPUT_PATH}/docstring.i','wb')
fout.write('// Contains the description of the properties\n')
for k,doclist in classdoc.iteritems():
    fout.write('\n\n// ' + '~' * 60 + '\n')
    fout.write('//  ' + k + ' ' + classnames[k] + '\n')
    fout.write('// ' + '~' * 60 + '\n')    
    fout.write('%feature("docstring") ' + classname(k) + ' "\n')
    fout.write('%s - substate %s\n' % (k,classname(k)))
    for doc in doclist:
        fout.write('     %(field)s %(dtype)s[%(size)s] [%(unit)s]: %(desc)s \n' % doc.__dict__)
    fout.write('";\n\n')
fout.close()
print 'docstring.i is generated'
print '*' * 60
print     
        
   
