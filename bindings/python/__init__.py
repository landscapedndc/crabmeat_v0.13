import os as __os
# Check for Lresources
if not 'LDNDC_RESOURCES_PATH' in __os.environ:
    __os.environ['LDNDC_RESOURCES_PATH'] = __os.path.realpath(__os.path.dirname(__file__))
if not __os.path.exists(__os.environ['LDNDC_RESOURCES_PATH']):
    raise ImportError('Could not find file: %s' % __os.environ['LDNDC_RESOURCES_PATH'])


from ldndc import landscape
from .version import __version__

