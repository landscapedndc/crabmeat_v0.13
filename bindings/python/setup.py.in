#!/usr/bin/python
# Some imports from the standardlibraries.
import os
import sys
import shutil
from distutils.core import setup as distutils_setup
from distutils.core import Extension as distutils_extension
try:
    # Import a function to get a path to the include directories of numpy
    from numpy import get_include as get_numpy_include
except ImportError:
    raise RuntimeError("For building and running of LandscapeDNDC.py an installation of numpy is needed")

def cmake_to_python_list( _clist, _delim=';') :
    clist = _clist.strip()
    if len( clist) > 0 :
        return  clist.split( _delim)
    return []
    
# Delete binary files, if "clean" command is invoked
if "clean" in sys.argv:
    print "del build/lib.win32-2.6 /q /s"
    shutil.rmtree("build/lib.win32-2.6")
include_dirs = cmake_to_python_list( '''${LDNDC_INCLUDE_DIRS}''')
include_dirs += [get_numpy_include()]
include_dirs += ['.']
define_macros = cmake_to_python_list( '''${LDNDC_MACRO_DEFINES}''')

compile_args = cmake_to_python_list( "${CMAKE_CXX_FLAGS}", ' ')
print 'compile flags: ', compile_args
link_args = []
print 'link flags: ', link_args
libraries = cmake_to_python_list( '''${LDNDC_LINK_ADDITIONAL_LIBRARIES}''')
library_dirs = cmake_to_python_list( '''${LDNDC_STATIC_LIBDIR}''')
libraries += cmake_to_python_list('''${LDNDC_STATIC_LIBS}''')
print 'libraries: ', libraries

if sys.platform=='win32':
    compile_args.extend('/EHsc /wd4102'.split())
    print ''.join(compile_args)
    compile_args.extend('/D%s' % M for M in define_macros)
    define_macros = []
    # Include debug information into the binary for profiling and debugging
    link_args = [ "/DEBUG",r'/PDB:"build\_ldndc.pdb"',"/LTCG"]
    libraries.append('Shell32')

elif sys.platform in ( 'linux2', 'darwin'):
    for M in define_macros:
        compile_args.append('-D%s' % M)
else:
    # Any other platforms (eg Solaris etc)? 
    raise RuntimeError("Your platform '%s' is currently not supported" % sys.platform)
src = cmake_to_python_list( '''${LDNDC_SOURCES_SWIG_REL}''')


# Create extension object using source files and arguments
ldndc_ext = distutils_extension('_ldndc',
              sources=src,
              include_dirs=include_dirs,
              extra_compile_args=compile_args, 
              extra_link_args=link_args,
              library_dirs=library_dirs,
              libraries=libraries,
              swig_opts=['-c++','-v','-w302,325,362,389'] + ['-I%s' % id for id in include_dirs])

print "extension set up."

# Make the setup, according to the command
distutils_setup( 
    name='ldndc', 
    ext_package='ldndc',
    ext_modules=[ldndc_ext], 
    package_dir={'ldndc':'bindings/python'},
    packages=['ldndc'],
    package_data={'ldndc':['Lresources']},
    version='${LDNDC_VERSION}',
    author='IFK-IFU, KIT, Garmisch-Partenkirchen, Germany'
)

print "setup finished."

