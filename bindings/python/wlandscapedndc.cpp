
#include  "wlandscapedndc.h"
#include  "kernels/lkernels.h"
#include  "kernels/lkernels-link.h"

#include  "string/lstring-convert.h"
#include  "string/lstring-compare.h"

#include  <stdexcept>

#define CHECKLANDSCAPE if (this->landscape_.status) {throw std::runtime_error("LandscapeDNDC is not correctly initialized");}
#define CHECKKERNEL if (!this->kernel_) {throw std::runtime_error("kernel is null. This is a serious problem");}

wlandscapedndc_t::wlandscapedndc_t( std::string projectfilename, std::string configfilename) 
    : landscape_( projectfilename.c_str(), NULL, configfilename.c_str())
{
    if ( this->landscape_.status != 0)
           {
        throw std::runtime_error( "Initialization of LandscapeDNDC failed with status "
                +std::string( this->get_status_message()));
    }
}

wlandscapedndc_t::~wlandscapedndc_t()
{
    this->landscape_.finalize();
}

wkernel_t wlandscapedndc_t::get_kernel( int  _k)
{
    CHECKLANDSCAPE
    if ( _k < 0)
           {
#ifdef LDNDC_SWIG_NEGATIVEINDEX
        _k = this->landscape_.size() - _k;
#else
        throw std::out_of_range("get_kernel(): Negative index is not allowed to query a landscape for a kernel");
#endif
    } 
    if ( _k < int( this->landscape_.size()))
           {
        ldndc::kernels::lkernel_t * a_kernel =
            this->landscape_.kernel_at( static_cast< size_t >( _k));
        if ( !a_kernel)
               {
            throw std::runtime_error("Got NULL instead of a valid kernel at position " + cbm::n2s( _k));
        }
        return  wkernel_t( a_kernel, this);
    }
    throw std::out_of_range("get_kernel(): index is not a valid kernel");
}
wkernel_t  wlandscapedndc_t::get_kernel_by_id( lid_t const &  _object_id)
{
    CHECKLANDSCAPE
    for ( size_t  k = 0;  k < this->landscape_.size();  ++k)
    {
        ldndc::kernels::lkernel_t * a_kernel =
            this->landscape_.kernel_at( k);
        if ( a_kernel && ( a_kernel->object_id() == _object_id))
        {
            return  wkernel_t( a_kernel, this);
        }
    }
    throw std::runtime_error( "no such kernel; failed object ID is " + cbm::n2s( _object_id));
}


ldate_t wlandscapedndc_t::get_modeltime() const
{
    CHECKLANDSCAPE
    return  this->landscape_.lclock()->now();
}
ldate_t wlandscapedndc_t::get_starttime() const
{
        CHECKLANDSCAPE
        return landscape_.lclock()->fromto().from();
}
ldate_t wlandscapedndc_t::get_endtime() const
{
        CHECKLANDSCAPE
        return landscape_.lclock()->fromto().to();
}
int wlandscapedndc_t::get_timesteps() const
{
        CHECKLANDSCAPE
        return landscape_.lclock()->timesteps();
}
int wlandscapedndc_t::get_timeresolution() const
{
    CHECKLANDSCAPE
    return  this->landscape_.lclock()->time_resolution();
}

void wlandscapedndc_t::launch( int  timesteps)
{
    CHECKLANDSCAPE
    lerr_t rc = this->landscape_.step( timesteps);
    if (rc)
           {
        throw std::runtime_error("landscape.launch had error, see log for details");
    }
}


ldate_t wlandscapedndc_t::round_ldate(const ldate_t& hiresdate) const
{
    ldate_t::ltime_res_t res_new = this->get_timeresolution();
    ldate_t::ltime_res_t res_old = hiresdate.time_resolution();
    ldate_t::ldate_scalar_t new_subdays = (hiresdate.subday() * res_old) / res_new;
    return ldate_t(hiresdate.year(),hiresdate.month(),hiresdate.day(),new_subdays,res_new);
}

size_t wlandscapedndc_t::kernelcount() const
{
    CHECKLANDSCAPE
    return  this->landscape_.size();
}



ldndc::public_state_t* wkernel_t::get_state()
{
    CHECKKERNEL
    return  this->kernel_->public_state();
}

ldate_t wkernel_t::get_modeltime() const
{
    return  this->landscape_->get_modeltime();
}
int wkernel_t::get_timeresolution() const
{
    return  this->landscape_->get_timeresolution();
}

std::vector< unsigned long int > wkernel_t::get_neighbors() const
{
    neighbor_t const *  neighbors = this->get_setup()->get_neighbors();
    size_t  n_neighbors = this->get_setup()->get_number_of_neighbors();
    std::vector< unsigned long int >  v_neighbors;
    for ( size_t  n = 0;  n < n_neighbors;  ++n)
    {
        v_neighbors.push_back( neighbors[n].desc.block_id);
    }
    return  v_neighbors;
}
double  wkernel_t::get_neighbor_intersect( unsigned long int  _id) const
{
    neighbor_t const *  neighbors = this->get_setup()->get_neighbors();
    size_t  n_neighbors = this->get_setup()->get_number_of_neighbors();
    for ( size_t  n = 0;  n < n_neighbors;  ++n)
    {
        if ( neighbors[n].desc.block_id == _id)
        {
            return  neighbors[n].intersect;
        }
    }
    return  invalid_t< double >::value;
}



bool wkernel_t::is_active()
{
    CHECKKERNEL
    return  kernel_->active();
}
bool wkernel_t::is_MoBiLE() const
{
    CHECKKERNEL
    return  cbm::is_equal_i( this->get_setup()->model_name(),
			ldndc::kernels::mobile::lkernel_mobile_t::ID);
}


#include "kernels/lkernels.h"
void dump_available_kernels()
{
    ldndc::kernels::lkernels_info_t::list_available_kernels( 0);
}
#include "kernels/mobile/modules/modules.h"
void dump_available_mobile_modules()
{
    ldndc::kernels::mobile::list_available_modules( 2);
}

