/*!
@brief Wraps the ldate_t type as a Python datetime object for SWIG.

@author: Philipp Kraft (uni-giessen)
!*/

// Include headers for datetime and ldate
%{
// Import C-API for Python's datetime type
#include "datetime.h"
// Import definition of ldndc date type
#include "time/ldate.h"
%}

// Import datetime library on python side
%pythoncode
{
import datetime
}

// Initilize the datetime C-API. If not done, nasty segfaults may occur
%init %{
PyDateTime_IMPORT;
%}

// Helper functions to convert between types
%{

// Convert a python datetime object to an ldate_t
static ldate_t convert_datetime_to_ldate(PyObject* dt) {
	if (PyDateTime_Check(dt)) {
		unsigned int 
			year  = PyDateTime_GET_YEAR(dt),
			month = PyDateTime_GET_MONTH(dt),
			day   = PyDateTime_GET_DAY(dt),
			second = PyDateTime_DATE_GET_SECOND(dt) + 60 * (PyDateTime_DATE_GET_MINUTE(dt) + 60 * PyDateTime_DATE_GET_HOUR(dt));
		return ldate_t(year,month,day,second,86400);
	} else if (PyDate_Check(dt)) {
		return ldate_t(PyDateTime_GET_DAY(dt),
			PyDateTime_GET_MONTH(dt),
			PyDateTime_GET_YEAR(dt),0,1);
	} else {
		return ldate_t();
	}

}
// Convert a ldate_t to a python datetime object. NOTE: Uses only ms precision, not usec!
static PyObject* convert_ldate_to_datetime(ldate_t ldate) {
	long long hour = (24l*ldate.subday()) / ldate.time_resolution(),
		minute = (24l * 60l * ldate.subday()) / ldate.time_resolution() - (hour*60l),
		second = (24l * 60l * 60l * ldate.subday()) / ldate.time_resolution() -  60l * (hour*60l + minute),
		ms = (24l * 60l * 60l * 1000l * ldate.subday()) / ldate.time_resolution() - 1000l * (60l * (60l * hour + minute) + second);
	return PyDateTime_FromDateAndTime(ldate.year(),ldate.month(),ldate.day(),hour,minute,second,ms * 1000);
}
%}
                         
// Make typemap for input. Functions expecting a ldate_t will expect a python datetime or date.
%typemap(in) ldate_t {
    $1 = convert_datetime_to_ldate($input);
    if ($1 == ldate_t())  {
        SWIG_exception_fail(SWIG_TypeError,"Can't convert input value to cmf.Time object");
    }
}
// Typemap to check if the given value can be casted to a ldate_t
%typemap(typecheck,precedence=0) ldate_t {
    $1 = PyDateTime_Check(dt) || PyDate_Check(dt);
}
// Typemap to convert a returned ldate_t to a python datetime
%typemap(out) ldate_t {
	$result = convert_ldate_to_datetime($1);
}
