%feature("compactdefaultargs");
%feature("autodoc","1");
// Include typemaps for STL
%include "typemaps.i"
%include "std_string.i"
%include "std_vector.i"

// enable exception support
%include "exception.i"
%exception {
    try {
        $action
    } catch (const std::out_of_range& e) {
        SWIG_exception(SWIG_IndexError, e.what());    
    } catch (const std::exception& e) {
        SWIG_exception(SWIG_RuntimeError, e.what());
    }
    
}
%include "attribute.i"

%module ldndc

%{
#include "wlandscapedndc.h"
%}
namespace std
{
    %template(UnsignedLongIntVector) vector<unsigned long int>;
}


// Include documentation
%include "docstring.i"
%feature("docstring") ldndc::event::input_class_event_t "
Python interface for the actual landscapeDNDC events.

lkernel.event is a sequence of all active events in landscapeDNDC and can be used
as any other immutable sequence (iterate with for, len, get event per index). However,
the in - Operator is overloaded for simple querying for a specific event type:

    'plant' in lkernel.event

returns True, if any plant event is in lkernel.event. Checking for a specific event 
(using an Event object) is still possible. You can filter the events by type using 
these two different ways:

    lkernel.event.iterbytype('plant')
    lkernel.event & 'plant'
    
The first way returns an iterable over all 'plant' events and the second one a list 
";
%define %_eventdoc(__type__)
%feature("docstring") ldndc::event::Event__ ## __type__ "
Specifies a single event of the type '__type__'. you can get the attributes
from the original xml-definition using to_string(). These attributes are partly
available using the specific attributes.
";
%enddef
%define %eventdoc(__types__...)
%formacro(%_eventdoc,__types__)
%enddef
%eventdoc(cut,die,defoliate,fire,flood,graze,harvest,irrigate,luc,manure,plant,regrow,thin,throw,till)

// ldate.i exposes ldndc's ldate_t as a Python datetime, and accepts Python datetime for a ldate_t.
// NOTE: ldate_t has a meaningful resolution, which can effect the ldndc runtime. When wrapping a function that 
// expects an ldate_t make sure you do consider resolution.
%include "ldate.i"

// lvector.i exposes ldndc's lvector_t as a NumPy ndarray. If lvector_t is exposed as an object's field,
// the wrapper returns a NumPy array pointer to the memory OWNED by the lvector_t. If an lvector_t is newly generated
// this will FAIL with a segfault.
%include "lvector.i"


// The substates use quite a number of macros cluttered over multiple header files
// the following %import directives help SWIG to understand the macros
%import "g_crabmeat-config.h"
%import "crabmeat-common.h"

%include "global/lid.h"
%include "global/lid-owner.h"
//%import "global/lid-owner.h"
%include "global/lclock-owner.h"
//%import "global/lclock-owner.h"
%include "global/lobject.h"
//%import "global/lobject.h"
%import "input/ic-names.h"
%import "input/ic-names-client.h"

%ignore ldndc::__input_class_base_t;
%ignore ldndc::input_class_global_base_t;
%ignore ldndc::kernels::mobile::substate_base_t;

%import "ldndc-client.h"
%import "input/ic-names-srv.h"
%include "input/ic-env.h"
%include "input/ic-client.h"
%import "input/ic-global-client.h"
%import "monolithic/input/ic-client-monolithic.h"

%{
#include "g_ldndc/g_events.h"
#include "g_ldndc/g_eventbase.h"
%}
%include "input/climate/climatetypes.h"
%include "monolithic/input/climate/climate-client.h"
%include "monolithic/input/event/event-client.h"
%include "monolithic/input/setup/setup-client.h"
%include "monolithic/input/site/site-client.h"
%include "monolithic/input/species/species-client.h"

%import "kernels/g_lkernels_config.h"
%import "kernels/mobile/g_mobile-config.h"
%import "kernels/lkernel-object.h"
%import "kernels/mobile/g_substatetypes.h"
%import "kernels/mobile/state/substate-names.h"
%import "kernels/mobile/state/substate.h"

// Import the generated macros for the substates. Needed to parse the substate headers
%import "kernels/mobile/g_substate_airchemistry.h"
%import "kernels/mobile/g_substate_microclimate.h"
%import "kernels/mobile/g_substate_physiology.h"
%import "kernels/mobile/g_substate_soilchemistry.h"
%import "kernels/mobile/g_substate_surfacebulk.h"
%import "kernels/mobile/g_substate_watercycle.h"
%import "kernels/mobile/state/substates.h"



%import "g_ldndc/g_event.h"
%include  "input/event/events/eventbase.h"
%include  "input/event/events/event-cut.h"
%include  "input/event/events/event-defoliate.h"
%include  "input/event/events/event-die.h"
%include  "input/event/events/event-fertilize.h"
%include  "input/event/events/event-fire.h"
%include  "input/event/events/event-flood.h"
%include  "input/event/events/event-graze.h"
%include  "input/event/events/event-harvest.h"
%include  "input/event/events/event-luc.h"
%include  "input/event/events/event-irrigate.h"
%include  "input/event/events/event-manure.h"
%include  "input/event/events/event-plant.h"
%include  "input/event/events/event-regrow.h"
%include  "input/event/events/event-thin.h"
%include  "input/event/events/event-throw.h"
%include  "input/event/events/event-till.h"


// prevent warning about assignment operator
%ignore *::operator=;

%pythoncode {
def parsedocstring(obj):
    """
    Creates a description of object obj by parsing its docstring. Works only for the substates.
    """
    doc = obj.__doc__
    res ={}
    for l in doc.split('\n'):
        if l.startswith(' ' * 5):
            ls =l.strip().split(None,1)
            res[ls[0]] = l.split(':')[-1].strip()
    class descriptor:
        def __init__(self,obj,dict):
            self.__dict__ = dict
            self.name = str(obj)
        def __repr__(self):
            return ("Description of %s:\n" % self.name)
        def __str__(self):
            return ("Description of %s:\n" % self.name) + '\n'.join('    %-16s : %s' % (k,v) for k,v in self.__dict__.iteritems() if k!='name')
        def __getitem__(self,k):
            return self.__dict__[k]
            
    return descriptor(obj,res)
    
}
// Include the substates. The fields of the substates are loaded from the generated macros in g_state.h


%define %makesubstate(__type__)
%include  "mobile/state/substates/__type__.h"
// Extends the substate by a nicer representation and a fielddescription property
%extend ldndc::kernels::mobile::substate_ ## __type__ ## _t {
%pythoncode {
    def __repr__(self):
        return "<ldndc.__type__>"
    @property
    def fielddescription(self):
        return parsedocstring(self)
}}
%enddef

%makesubstate(airchemistry)
%makesubstate(microclimate)
%makesubstate(physiology)
%makesubstate(soilchemistry)
%makesubstate(surfacebulk)
%makesubstate(watercycle)

%define %makeinput(__type__)
%include  "monolithic/input/__type__/__type__-client.h"
// Extends the input class by a nicer representation and a fielddescription property
%extend ldndc::__type__::input_class_ ## __type__ ## _t {
%pythoncode {
    def __repr__(self):
        return "<ldndc.input.__type__, [%i]>" % ( self.object_id())
}}
%enddef

%makeinput(climate)
%makeinput(event)
%makeinput(setup)
%makeinput(site)
%makeinput(species)



// Event-Handling

// ignore getters that are not useful from python
%ignore ldndc::event::input_class_event_t::allocate_instance; 
// class_type useful? 
%ignore ldndc::event::input_class_event_t::class_type;
%ignore ldndc::event::input_class_event_t::clone_instance;
%ignore ldndc::event::input_class_event_t::delete_instance;
%ignore ldndc::event::input_class_event_t::flags;
%ignore ldndc::event::input_class_event_t::get_event_handles;
%ignore ldndc::event::input_class_event_t::get_event_handles_cnt;
%ignore ldndc::event::input_class_event_t::has_internal_state;
%ignore ldndc::event::input_class_event_t::input_class_type;
%ignore ldndc::event::input_class_event_t::new_instance;
%ignore ldndc::event::input_class_event_t::state; 

%extend ldndc::event::input_class_event_t
{
    // Make private _getitem function to wrap subscript operator
    ldndc::event::Event const &  _getitem( size_t _k)
    {
        return  *(self->operator[]( _k));
    }
    
    %pythoncode
    {
        # Implementation of the sequence protocol
        def __len__( self) :
            return  self.event_cnt()
        def __iter__(self):
            for i in xrange(self.event_cnt()):
                event = self._getitem(i).cast()
                if event:
                    yield event
        def __getitem__(self,indx):
            if indx<0:
                indx = len(self)+indx
            return self._getitem(indx).cast()
        def iterbytype(self,eventtypename):
            for i in xrange(self.event_cnt()):
                event = self._getitem(i).cast()
                if event and event.name() == eventtypename:
                    yield event
        def __and__(self,eventtypename):
            return list(self.iterbytype(eventtypename))
        def __contains__(self,item):
            if issubclass(type(item),Event):
                return item.this in (e.this for e in self)
            else:
                return bool(self & item)
        def __repr__(self):
            return 'ldndcEvents([' + ','.join(repr(e) for e in self) + '])'
        
            

}

}
%define %_eventcast(__type__)
    ldndc::event::Event__ ## __type__ const * _as_ ## __type__()
    {
        return  dynamic_cast< ldndc::event::Event__ ## __type__ const * >( self);
    }
%enddef
%define %_eventprint(__type__)
%extend ldndc::event::Event__ ## __type__
{
    std::string  __repr__()
    {
        std::string name = self->name();
        return  "ldndcEvent:" + name + " (" + self->to_string() + ")";
    }
    
}    
%enddef
%define %eventprint(__types__...)
%formacro(%_eventprint,__types__)
%enddef

%define %eventcast(__types__...)
%formacro(%_eventcast,__types__)
%enddef
%eventprint(cut,die,defoliate,fire,flood,graze,harvest,irrigate,luc,manure,plant,regrow,thin,throw,till)

%extend ldndc::event::Event
{
    std::string  __repr__()
    {
        std::string name = self->name();
        return  "ldndcEvent:" + name + " (" + self->to_string() + ")";
    }

    %eventcast(cut,die,defoliate,fire,flood,graze,harvest,irrigate,luc,manure,plant,regrow,thin,throw,till)
    
%pythoncode
{
    def cast( self) :
        try:
            return getattr(self,'_as_%s' % self.name())()
        except AttributeError:
            return self    
}
}

// Prepare the wlandscapedndc.h file as a SWIG interface.

// Use nicer names (without the _wrap)
%rename(landscape) wlandscapedndc_t;
%rename(kernel) wkernel_t;
// use wlandscapedndc.h as interface definition
%include "wlandscapedndc.h"
%extend wlandscapedndc_t {
%pythoncode {
    def __str__(self):
        return "<ldndc.landscape, %i kernels>" % len(self)
    def __len__(self):
        return self.kernelcount()
    def __getitem__(self,index):
        if (index<0):
            index =  len(self) + index;
        return self.get_kernel(index)  
  
}}
%extend wkernel_t {
%pythoncode {
    def __str__(self):
        return "<ldndc.kernel, %s:%i (%s)>" % ( self.setup.model_name(), self.id(), self.setup.site_name())

    climate = property( get_climate)
    event = property( get_event)
    setup = property( get_setup)
    site = property( get_site)
    species = property( get_species)

    airchemistry = property(get_airchemistry)
    microclimate = property(get_microclimate)
    physiology = property(get_physiology)
    soilchemistry = property(get_soilchemistry)
    surfacebulk = property(get_surfacebulk)
    watercycle = property(get_watercycle)

    landscape = property(get_landscape)
}}

