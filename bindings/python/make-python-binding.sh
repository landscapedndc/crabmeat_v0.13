#!/bin/sh
## run from build directory

source_dir="${1:-..}"

# Configure with cmake for SWIG generation
cmake --warn-uninitialized -G "Unix Makefiles" -DTARGETS_PYTHON_API_ldndc:BOOL=ON "${source_dir}"
if [ $? -ne 0 ]
then
        echo "cmake failed.." 1>&2
        exit 1
fi

make -j4 ldndc-binding-python

