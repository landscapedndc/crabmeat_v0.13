
#include  "ldndc-client.h"
#include  "string/lstring-basic.h"
#include  "time/ldate.h"
#include  "mobile/state/state.h"

/* To be included in .i file as %include */
#include  "mobile/state/substates.h"
#include  "landscape/landscapedndc.h"

#include  "kernels/lkernel.h"
#include  "lkernel/io-kcomm.h"

#include  "input/setup/setup.h"
#include  "input/site/site.h"
#include  "input/climate/climate.h"
#include  "input/species/species.h"

class wlandscapedndc_t;

// Macro to access the substates directly from kernel
#define  LDNDC_SWIG_SUBSTATE_GETTER(__type__) MOBILE_NAMESPACE::__LDNDC_SUBSTATE_CLASS(__type__) *  get_##__type__()                \
{ return  dynamic_cast< MOBILE_NAMESPACE::public_state_mobile_t * >( this->get_state())->get_substate< MOBILE_NAMESPACE::__LDNDC_SUBSTATE_CLASS(__type__) >();    }

// Macro to access the input directly from kernel
#define  LDNDC_SWIG_INPUT_GETTER(__type__) ldndc::__type__::__LDNDC_INPUT_CLASS(__type__) const *  get_##__type__() const        \
{ return  this->kernel_->io()->get_input_class< ldndc::__type__::__LDNDC_INPUT_CLASS(__type__) >();    }


/// This class holds a simplified interface for kernel
///
/// Might have direct access to the substates (no wrapper for public_state)
/// Wrapper is assignable and copy constructable, can be treated as a stack object and be used in containers.
class wkernel_t
{
private:
    ldndc::kernels::lkernel_t* kernel_;
    wlandscapedndc_t * landscape_;
    wkernel_t(ldndc::kernels::lkernel_t* kernel, wlandscapedndc_t * landscape)
        : kernel_(kernel), landscape_(landscape)
    {    }

    ldndc::public_state_t* get_state();
public:
    // Can only be created by wlandscapedndc_t, no need to include lkernels.h in interface
    friend class wlandscapedndc_t;
    // Copy c'tor
    wkernel_t(const wkernel_t& other)
        : kernel_(other.kernel_),landscape_(other.landscape_)
    {}
    unsigned long int  id() const { return  this->kernel_->object_id(); }

    wlandscapedndc_t * get_landscape() const
           {
        return landscape_;
    }
    // Assignment operator, nice to have 
    void operator=(const wkernel_t& other)
           {
        this->kernel_ = other.kernel_;
        this->landscape_ = other.landscape_;
    }
    bool is_active();

    bool is_MoBiLE() const;

    ldate_t  get_modeltime() const;
    int  get_timeresolution() const;

    std::vector< unsigned long int >  get_neighbors() const;
    double  get_neighbor_intersect( unsigned long int /*neighbor ID*/) const;

    /*!
    * @brief
    *    retrieve reference to various substate objects
    */
    LDNDC_SWIG_SUBSTATE_GETTER(airchemistry)
    LDNDC_SWIG_SUBSTATE_GETTER(microclimate)
    LDNDC_SWIG_SUBSTATE_GETTER(physiology)
    LDNDC_SWIG_SUBSTATE_GETTER(soilchemistry)
    LDNDC_SWIG_SUBSTATE_GETTER(surfacebulk)
    LDNDC_SWIG_SUBSTATE_GETTER(watercycle)

    /*!
     * @brief
     *    retrieve references to various input objects
     */
    LDNDC_SWIG_INPUT_GETTER(climate)
    LDNDC_SWIG_INPUT_GETTER(event)
    LDNDC_SWIG_INPUT_GETTER(setup)
    LDNDC_SWIG_INPUT_GETTER(site)
    LDNDC_SWIG_INPUT_GETTER(species)
};

/// This class makes a thin wrapper around a complete landscape object
///
/// Until now only text file loading is supported similar to apps/ldndc.cpp 
/// The landscape object is finalized and deleted when the wrapper is deleted
class wlandscapedndc_t
{
private:
    ldndc::llandscapedndc_t  landscape_;
    ldate_t  round_ldate( ldate_t const &) const;

public:
    /// Creates a new landscape from a project file and a configuration file
    wlandscapedndc_t(std::string /*project filename*/, std::string /*configuration filename*/);

    int  get_status() const { return  this->landscape_.status; }
    char const *  get_status_message() const
    {
        return  this->landscape_.get_status_message();
           }

    /// Get a wrapped kernel
    wkernel_t  get_kernel( int /*index*/);
    wkernel_t  get_kernel_by_id( lid_t const & /*object ID*/);
    size_t  kernelcount() const;

    /// Returns the current model time
    ldate_t  get_modeltime() const;
    ldate_t  get_endtime() const;
    ldate_t  get_starttime() const;
    int  get_timesteps() const;
    int  get_timeresolution() const;

    /// Models the landscape for timesteps.
    void  launch( int = 1 /*number of timesteps*/);
    void  finalize() { this->landscape_.finalize(); }

    /// Destructor, finalizes the landscape
    ~wlandscapedndc_t();
};

extern void  dump_available_kernels();
extern void  dump_available_mobile_modules();

