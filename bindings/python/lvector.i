/// @brief SWIG-Interface file to construct a wrapper for lvector_t (lvector.h)
///
/// Exposes the lvector_t type to Python as an numpy array
/// ===The output typemap (converts a lvector_t to Python) shares the data pointer of
/// the original lvector_t with the resulting numpy array. May produce a seg fault under 
/// weird corner cases (lvector_t is destroyed, but numpy array still exists)
///
/// ===The input typemap (accepts a numpy array to set values of lvector_t)
/// 


/*************************************************************************
General purpose stuff for code that uses numpy array
**************************************************************************/
// import_array() has to be called once to avoid nasty seg faults
%init %{ import_array(); %}

// Some macro definitions to link correctly to numpy
%header %{
// numpy/arrayobject.h requires this Macro-constant. Some Compilers (e.g. gcc) defines it internally, others (e.g. VS2008) not
#ifndef SIZEOF_LONG_DOUBLE
#define SIZEOF_LONG_DOUBLE sizeof(long double) 
#endif
#ifndef NPY_NO_DEPRECATED_API
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#endif

%}


/*************************************************************************
Some basic functions for the conversion
**************************************************************************/
%{
// including numpy array definition
#include <numpy/arrayobject.h>
#include "containers/lvector.h"
#include <sstream>

// General purpose value->string function. Used for error messages
template<typename _C>
static std::string to_string(_C value) {
	std::stringstream str;
	str << value;
	return str.str();
}

// Helper functions to return the pointer to the data of lvector_t. Used by as_npy_array
template<typename _v>
_v* lvector_pointer(lvector_t<_v,1> vector) {
    return &(vector[0]);
}
template<typename _v>
_v* lvector_pointer(lvector_t<_v,2> vector) {
    return &(vector[0][0]);
}
template<typename _v>
_v* lvector_pointer(lvector_t<_v,3> vector) {
    return &(vector[0][0][0]);
}
template<typename _v>
_v* lvector_pointer(lvector_t<_v,4> vector) {
    return &(vector[0][0][0][0]);
}


// This struct detemines the numpy typecode from a C++ code using the following specializations. Needed for from_npy_array
template<class T> struct npy_types
{
	static NPY_TYPES typecode;
};
template<> NPY_TYPES npy_types<int>::typecode = NPY_INT;
template<> NPY_TYPES npy_types<double>::typecode = NPY_DOUBLE;
template<> NPY_TYPES npy_types<float>::typecode = NPY_FLOAT;
template<> NPY_TYPES npy_types<bool>::typecode = NPY_BOOL;
template<> NPY_TYPES npy_types<unsigned int>::typecode = NPY_UINT;
template<> NPY_TYPES npy_types<unsigned long int>::typecode = NPY_ULONG;


// This function creates a numpy array from an lvector_t.
// The ownership of the data is afterwards shared between the numpy array and lvector_t. lvector_t destroys the data when leaving scope.
template< typename _v, unsigned int _R >
PyObject* as_npy_array(const lvector_t<_v,_R>& a,bool copy) {
	npy_intp* dims = new npy_intp[a.rank()];
	for (size_t i=0;i<a.rank();++i) {
		dims[i] = a.extent(i);
	}
    _v* data = 0;
    int flags = NPY_ARRAY_CARRAY;
    if (copy) {
        data = new _v[a.size()];
        flags |= NPY_ARRAY_OWNDATA;
        a.copy_to_carray(data);
    } else {
        // pointer to the data of the array
        data = lvector_pointer(a);
    }
	// Creates the numpy array using size, place and type information
	return PyArray_New(
		&PyArray_Type, a.rank(),dims,npy_types<_v>::typecode,0 /*strides pointer*/,
		(void *)data, 0, /*itemsize*/
		flags,0);
}


// This function creates a new array<T> from any object providing the array interface or from a sequence.
// First a numpy array is created to ensure right ordering, secondly an array<T> is created by copying the given data.
template< typename _v, unsigned int _R >
int from_npy_array(PyObject* op,lvector_t<_v,_R>& lvector) {
	// Create a contiguous and behaved numpy array from the given object
	// May include a copy of the data
	// Returns NULL if:
	// - given object was not a sequence of numbers or an object exposing the array interface or
	// - array had more dimensions
	size_t lrank = lvector.rank();
	size_t lsize = lvector.size();
	PyArrayObject* ao = (PyArrayObject*) PyArray_ContiguousFromAny(op,npy_types<_v>::typecode,lrank,lrank);
	if (!ao) {// If given object was not convertable into an array (no sequence of numbers or implementation of the array interface)	
		throw std::invalid_argument("Object is not an array");
	} else	{
		size_t size = (size_t)PyArray_Size((PyObject*)ao);
		size_t ndim = (size_t)PyArray_NDIM(ao);
		// Check rank of array
		if (ndim!=lrank) {
			Py_DECREF(ao);
			throw std::invalid_argument("Rank of array ("+ to_string(ndim) + ") does not equal expected rank" + to_string(lrank));

		}
        typename lvector_t<_v,_R>::extent_type  asizes[] = {0,0,0,0};
        for (size_t i=0;i<ndim;++i) 
                asizes[i] = PyArray_DIM(ao,i);

		// Get pointer to data
		_v * p_array=(_v*)PyArray_DATA(ao);
		// Make lvector of correct size data into lvector
		lvector.resize( asizes);
                // Copy data to lvector_t
                lvector.copy_from_carray(p_array);
		Py_DECREF(ao);
		return size;
	}
}
template < typename _v,unsigned int _R >
void lvector_copy_to_target(lvector_t < _v,_R > & target, lvector_t < _v,_R > & source) {
    // Check sizes of arrays
    for (size_t i = 0;i<target.rank();++i) {
        size_t tsize = target.extent(i);
        size_t ssize = source.extent(i);
        if (tsize!=ssize) {
            throw std::invalid_argument("Dimension "+to_string(i)+" has in the source vector size "+ to_string(ssize) + " but expected " + to_string(tsize) + " in target");
        }
    }
    target = source;
}
%}
/********************************************************************************
 The actual typemaps have to be defined for each specialization
 ********************************************************************/
// A swig macro to be called for every combination of type and rank
// is called by %array_interface
%define %array_interface_rank(Class,Rank)

// In typemap, copies data from a numpy array and kills the old data (improvement possible!)
%typemap(in) lvector_t<Class,Rank> {
    // Convert an array_wrapper from numpy array
    try { from_npy_array<Class,Rank>($input,$1); }
    // If from_npy_array throws, give Python error
    catch(std::invalid_argument &e) {
        PyErr_SetString(PyExc_ValueError,e.what()); 
        return NULL;
    }
}
// copies the date retrieved from in typemap to the target lvector_t
%typemap(memberin) lvector_t<Class,Rank> {
    try {
        lvector_copy_to_target($1,$input);   
    } catch (std::invalid_argument &e) {
        PyErr_SetString(PyExc_ValueError,e.what()); 
        return NULL;
    }
}
// In typemap, copies data from a numpy array and kills the old data (improvement possible!)
%typemap(in) lvector_t<Class,Rank>* {
    // Convert an array_wrapper from numpy array
    try { from_npy_array<Class,Rank>($input,*$1); }
    // If from_npy_array throws, give Python error
    catch(std::invalid_argument &e) {
        PyErr_SetString(PyExc_ValueError,e.what()); 
        return NULL;
    }
}

// copies the date retrieved from in typemap to the target lvector_t
%typemap(memberin) lvector_t<Class,Rank>* {
    try {
        lvector_copy_to_target(*$1,*$input);   
    } catch (std::invalid_argument &e) {
        PyErr_SetString(PyExc_ValueError,e.what()); 
        return NULL;
    }
}




// Out typemap, returns the data of array<T> wrapped as a numpy array, 
// thus converting the reference counting mechanism to the Python reference counting
%typemap(out,optimal="1") lvector_t<Class,Rank> {
    $result = as_npy_array<Class,Rank>($1,false);
}
%typemap(out,optimal="1") lvector_t<Class,Rank>* {
    $result = as_npy_array<Class,Rank>(*$1,false);
}
%enddef


// Creates the interface for a type
%define %array_interface(Class)
    %array_interface_rank(Class,1);
    %array_interface_rank(Class,2);
%enddef

// Add the array interface
%array_interface(int);
%array_interface(unsigned int);
%array_interface(unsigned long int);
%array_interface(double);
%array_interface(float);
%array_interface(bool);
