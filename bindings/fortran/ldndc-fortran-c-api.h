/*!
 * @brief
 *    C API
 *
 * @note
 *    some of the functions assume a fixed type of kernel, it
 *    will crash if this assumption is violated.
 *
 * @author
 *    steffen klatt (created: may 22, 2014),
 *    edwin haas
 */

#ifndef  __C_API__EMEP_LDNDC_SIMULATION_H_
#define  __C_API__EMEP_LDNDC_SIMULATION_H_

/* c api */
#include  "simulation-c-api.h"

#ifdef __cplusplus
extern  "C"
{
#endif

ldndc_simulation_t *  ldndc_create_fixedproject();
int  ldndc_size(
        ldndc_simulation_t * /*ldndc*/);
int  ldndc_step(
        ldndc_simulation_t * /*ldndc*/, int /*timesteps*/);
void  ldndc_finalize(
        ldndc_simulation_t * /*ldndc*/);

struct  ldndc_message_t
{
    /* send */
    double  no_emis;
    double  nh3_emis;

    /* receive */
    double  upt_no3;
    double  upt_nh4;

    double  dry_nitrogen;
};
ldndc_message_t *  ldndc_message_create( int /*number of messages*/);
void  ldndc_message_destroy( ldndc_message_t * /*message*/);

int  ldndc_getstate(
                ldndc_simulation_t * /*ldndc*/, int /*kernel index*/, ldndc_message_t *);

int  ldndc_setstate(
                ldndc_simulation_t * /*ldndc*/, int /*kernel index*/, ldndc_message_t const *);

#ifdef __cplusplus
}  /*  extern "C"  */
#endif



#endif  /*  !__C_API__EMEP_LDNDC_SIMULATION_H_  */

