
        program ldndcfromfortran

        use, intrinsic :: iso_c_binding
        implicit none

        type, bind(c) :: ldndcmsg
                real( kind=c_double) :: no_emis
                real( kind=c_double) :: nh3_emis

                real( kind=c_double) :: upt_no3
                real( kind=c_double) :: upt_nh4
                real( kind=c_double) :: dry_nitrogen
        end type

        type(c_ptr) :: ldndc
        type(c_ptr) :: buf
        type(ldndcmsg), pointer :: fbuf
        integer :: t, lerr

        interface
        function msgcreate( n) bind(c,name="ldndc_message_create")
                use, intrinsic :: iso_c_binding
                integer( kind=c_int) :: n
                type(c_ptr) :: msgcreate
        end function
        subroutine msgdestroy( m) bind(c,name="ldndc_message_destroy")
                use, intrinsic :: iso_c_binding
                type(c_ptr), value :: m
        end subroutine

        function ldndccreate() bind(c,name="ldndc_create_fixedproject")
                use, intrinsic :: iso_c_binding
                type(c_ptr) :: ldndccreate
        end function
        function ldndcstep( p, timesteps) bind(c,name="ldndc_step")
                use, intrinsic :: iso_c_binding
                type(c_ptr), value :: p
                integer( kind=c_int), value :: timesteps
                integer( kind=c_int) :: ldndcstep
        end function

        subroutine ldndcfinalize( p) bind(c,name="ldndc_finalize")
                use, intrinsic :: iso_c_binding
                type(c_ptr), value :: p
        end subroutine

        function ldndcsetstate( p, k, b) bind(c,name="ldndc_setstate")
                use, intrinsic :: iso_c_binding
                type(c_ptr), value :: p
                integer( kind=c_int), value :: k
                type(c_ptr), value :: b
                integer( kind=c_int) :: ldndcsetstate
        end function
        function ldndcgetstate( p, k, b) bind(c,name="ldndc_getstate")
                use, intrinsic :: iso_c_binding
                type(c_ptr), value :: p
                integer( kind=c_int), value :: k
                type(c_ptr), value :: b
                integer( kind=c_int) :: ldndcgetstate
        end function

        function ldndcsize( p) bind(c,name="ldndc_size")
                use, intrinsic :: iso_c_binding
                type(c_ptr), value :: p
                integer( kind=c_int) :: ldndcsize
        end function

        end interface

        ldndc = ldndccreate()
        if ( .not. c_associated(ldndc)) then
                write(*,*) "failed to create and initialize ldndc"
                stop
        end if
        write(*,*) "ldndc size=", ldndcsize( ldndc)

        buf = msgcreate( 1)
        if ( .not. c_associated( buf)) then
                write(*,*) "failed to allocate message buffer"
                stop
        end if


        do t = 1, 365

                lerr = ldndcstep( ldndc, 1)
                if ( lerr .ne. 0) then
                        write(*,*) "failed to simulate timestep ",t
                end if

c               needs to be a loop for regional applications;
c               use ldndcsize() to get number of kernels
c               alternatively, make buf an array by giving ldndcsize N
c               to msgcreate( N) and wrap ldndcgetstate
                lerr = ldndcgetstate( ldndc, 0, buf)
                if ( lerr .ne. 0) then
                        write(*,*) "failed to retrieve state items"
                end if
                call c_f_pointer( buf, fbuf)
                write(*,*) "no=",fbuf%no_emis, "  nh3=",fbuf%nh3_emis

c               INJECT NEW STATE VALUES AND EXECUTE EMEP
c               eerr = emepstep( 1)
c               UPDATE MESSAGE BUFFER

c               see comment above for regional applications
                lerr = ldndcsetstate( ldndc, 0, buf)
                if ( lerr .ne. 0) then
                        write(*,*) "failed to set state items"
                end if

        end do

        call ldndcfinalize( ldndc)
        call msgdestroy( buf)

        stop

        end program

