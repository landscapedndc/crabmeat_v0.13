/*!
 * @brief
 *    C API  (implementation)
 *
 * @author
 *    steffen klatt (created: may 22, 2014),
 *    edwin haas
 */

#include  "ldndc-fortran-c-api.h"
#include  "mobile/mobile.h"

#include  <stdio.h>

#ifdef __cplusplus
extern  "C"
{
#endif

ldndc_simulation_t *  ldndc_create_fixedproject()
{
    ldndc_simulation_t *  ldndc = ldndc_simulation_create(
            0, "ldndc_projectfile.xml", "ldndc_configurationfile.conf", NULL);
    fprintf( stderr, "create():  c pointer=%p   int-c-pointer=%lu\n", (void*)ldndc, (long unsigned int)ldndc);
    return  ldndc;
}
int  ldndc_size(
        ldndc_simulation_t *  _ldndc)
{
    return  static_cast< int >( _ldndc->ldndc->size());
}

int  ldndc_step(
        ldndc_simulation_t *  _ldndc, int  _timesteps)
{
// dbg    fprintf( stderr, "step():  c pointer=%p   int-c-pointer=%lu\n", (void*)_ldndc, (long unsigned int)_ldndc);
    return  (int)ldndc_simulation_launch( _ldndc, _timesteps);
}

void ldndc_finalize(
        ldndc_simulation_t *  _ldndc)
{
    ldndc_simulation_finalize( _ldndc);
    ldndc_simulation_destroy( _ldndc);
}

ldndc_message_t *  ldndc_message_create(
        int  _n)
{
    if ( _n < 1)
    {
        return  NULL;
    }
    ldndc_message_t *  msg = new ldndc_message_t[_n];
    fprintf( stderr, "message_create():  %p\n", (void*)msg);
    return  msg;
}
void  ldndc_message_destroy(
        ldndc_message_t *  _msg)
{
    if ( _msg)
    {
        fprintf( stderr, "message_destroy():  %p\n", (void*)_msg);
        delete[]  _msg;
    }
}
int  ldndc_getstate(
                ldndc_simulation_t *  _ldndc, int  _kernel_at, ldndc_message_t *  _buf)
{
    if ( !_ldndc || _kernel_at < 0 || !_buf)
    {
        return  -1;
    }
    ldndc::kernels::mobile::lkernel_mobile_t const *  m_kernel =
           reinterpret_cast< ldndc::kernels::mobile::lkernel_mobile_t const * >( _ldndc->ldndc->kernel_at( _kernel_at));
    if ( !m_kernel)
    {
        return  -2;
    }

    ldndc::kernels::mobile::substate_soilchemistry_t const *  soilchem =
        m_kernel->public_state()->get_substate< ldndc::kernels::mobile::substate_soilchemistry_t >();
    if ( !soilchem)
    {
        return  -3;
    }

    _buf->no_emis = soilchem->dNO;
    _buf->nh3_emis = soilchem->dNH3;
// dbg    fprintf( stderr, "getstate():  no-emis=%f  nh3-emis=%f\n", _buf->no_emis, _buf->nh3_emis);

    return  0;
}

int  ldndc_setstate(
                ldndc_simulation_t *  _ldndc, int  _kernel_at, ldndc_message_t const *  _buf)
{
    if ( !_ldndc || _kernel_at < 0 || !_buf)
    {
        return  -1;
    }
    ldndc::kernels::mobile::lkernel_mobile_t *  m_kernel =
           reinterpret_cast< ldndc::kernels::mobile::lkernel_mobile_t * >( _ldndc->ldndc->kernel_at( _kernel_at));
    if ( !m_kernel)
    {
        return  -2;
    }

    ldndc::kernels::mobile::substate_airchemistry_t const *  airchem =
        m_kernel->public_state()->get_substate< ldndc::kernels::mobile::substate_airchemistry_t >();
    if ( !airchem)
    {
        return  -3;
    }

// dbg    fprintf( stderr, "setstate():  no3=%f  nh4=%f\n", _buf->upt_no3, _buf->upt_nh4);
    // FIXME  ??->upt_no3 = _buf->upt_no3;
    // FIXME  ??->upt_nh4 = _buf->upt_nh4;

    // FIXME  airchem->nh4_dry = _buf->nh4_dry;
    // FIXME  airchem->no3_dry = _buf->no3_dry;

    return  0;
}

#ifdef __cplusplus
}  /*  extern "C"  */
#endif

