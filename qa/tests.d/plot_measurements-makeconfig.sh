#!/bin/bash

_sources="$1"
_plots="$2"
_time="$3"
_output="$4"

cat << __EOF__

styles:
  sim_style: &sim_style
    type: standard
    linestyle: line
    linewidth: 1
    color: red

  meas_style: &meas_style
    type: standard
    linestyle: points
    linewidth: 1
    color: blue

defaults:
  output: $_output
  time: $_time
  height: 10
  width: 18

sources:
`cat $_sources`

plots:
`cat $_plots`

__EOF__

