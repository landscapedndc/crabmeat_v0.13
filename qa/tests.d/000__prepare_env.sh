##
## set up working directories, update input and measurements

if [ ${#@} -lt 5 ]
then
        echo -en "usage: \n`basename $0` <qa user conf> <branch> <branch-build> <branch-root> <branch-binary-root>\n"
        exit 110
fi

_qa_userconf_name="$1"
_branch="$2"
_branchbuild="$3"
_qa_branch_root="$4"
_branch_binary_root="$5"

if [ -r "$_qa_branch_root/$_qa_userconf_name" ]
then
        . "$_qa_branch_root/$_qa_userconf_name"
fi

echo "branch-root=$_qa_branch_root  branch-binary-dir=$_branch_binary_root  branch=$_branch  branch-build=$_branchbuild" 1>&2

## sk:todo  ## base test bed
## sk:todo  if [ -z "$qa_sandbox" ]
## sk:todo  then
## sk:todo          exit  111
## sk:todo  elif [ ! -r "$qa_sandbox" ]
## sk:todo  then
## sk:todo          $mkdir_bin  "$qa_sandbox"
## sk:todo  fi
## sk:todo  ## data dir
## sk:todo  if [ ! -r "$qa_sandbox_data" ]
## sk:todo  then
## sk:todo          $mkdir_bin  "$qa_sandbox_data"
## sk:todo  fi
## sk:todo  ## inputs dir
## sk:todo  if [ ! -r "$qa_sandbox_inputs" ]
## sk:todo  then
## sk:todo          $mkdir_bin  "$qa_sandbox_inputs"
## sk:todo  fi
## sk:todo  ## outputs dir
## sk:todo  if [ -r "$qa_sandbox_outputs" ]
## sk:todo  then
## sk:todo          cd "$qa_sandbox"
## sk:todo          $rm_bin -r -f --one-file-system "$qa_sandbox_outputs"
## sk:todo          $mkdir_bin  "$qa_sandbox_outputs"
## sk:todo          cd -
## sk:todo  fi

## sk:todo  ## update data
## sk:todo  cd "$qa_sandbox_data"
## sk:todo  for dataset in ${qa_test_data[@]}
## sk:todo  do
## sk:todo          echo $dataset
## sk:todo          $svn_bin 
## sk:todo  done
## sk:todo  
## sk:todo  ## update inputs
## sk:todo  cd "$qa_sandbox_inputs"
## sk:todo  for inputset in ${qa_test_inputs[@]}
## sk:todo  do
## sk:todo          echo $inputset
## sk:todo  done

