
# kick off qa script

if [ ${#@} -lt 6 ]
then
        echo -en "usage: \n`basename $0` <qa user conf> <branch> <branch revision> <branch-build> <branch-root> <branch-binary-root>\n" 1>&2
        echo -en "arguments: ${@}\n" 1>&2
        exit 110
fi

_qa_userconf_name="$1"
_branch="$2"
_branch_revision="$3"
_branchbuild="$4"
_qa_branch_root="$5"
_branch_binary_root="$6"

if [ -r "$_qa_branch_root/$_qa_userconf_name" ]
then
        . "$_qa_branch_root/$_qa_userconf_name"
fi

#echo "`basename $0`:_qa_userconf_name=$_qa_userconf_name _branch=$_branch _branchbuild=$_branchbuild _qa_branch_root=$_qa_branch_root _branch_binary_root=$_branch_binary_root"

if [ ! -x "$_branch_binary_root/bin/ldndc" ]
then
        echo -en "LDNDC binary does not exist or is not executable  [binary=$_branch_binary_root/bin/ldndc]\n" 1>&2
        exit 1
fi
if [ ! -r "$_branch_binary_root/Lresources" ]
then
        echo -en "LDNDC resources database does not exist or is not readable  [missing=$_branch_binary_root/Lresources]\n" 1>&2
        exit 1
else
        $cp_bin -f $_branch_binary_root/Lresources $HOME/.ldndc/
fi

qa_config_path="$HOME/.ldndc"
qa_config_tmpl="$qa_config_path/output-qa.yml.TMPL"
qa_config_for_branch="$qa_config_path/output-qa.${_branch}.yml"

qa_branch_binary_root_sed=`echo $_branch_binary_root | $tr_bin '/' '%' | $sed_bin 's/%/\\\\\//g'`
qa_sandbox_root_sed=`echo $qa_sandbox_root | $tr_bin '/' '%' | $sed_bin 's/%/\\\\\//g'`

$sed_bin "s/__QA_LDNDC_BINARY_FOLDER__/$qa_branch_binary_root_sed\/bin\//" "$qa_config_tmpl" | \
                $sed_bin "s/__QA_SANDBOX_FOLDER__/$qa_sandbox_root_sed/g" > "$qa_config_for_branch"

qa_bin="$HOME/branches/qa-app/Main.R"

YMLINDENT="  "
YMLINDENT2="${YMLINDENT}${YMLINDENT}"
for testproject in ${qa_testprojects_l[@]}
do
	testproject_basename=`basename $testproject`
        printf "$YMLINDENT$testproject:\n${YMLINDENT2}projectFile : ${testproject_basename}.xml\n${YMLINDENT2}input : $qa_testproject_root/$testproject\n${YMLINDENT2}validOutput : $qa_testprojects_ref_root/$testproject/${testproject_basename}_output/\n${YMLINDENT2}output : $qa_sandbox_root_outputs/$testproject/${testproject_basename}_output\n" >> $qa_config_for_branch
done

if [ ! -r "$qa_config_for_branch" ]
then
        echo -en "failed to generate qa configuration file  [config=$qa_config_for_branch]" 1>&2
        exit 8
else
        $mkdir_bin -p "$qa_sandbox_root/log"
        $mkdir_bin -p "$qa_sandbox_root_outputs"

        cd "$qa_app_root"
        $R_bin $qa_bin $qa_config_for_branch 1> "$qa_sandbox_root/log/R-${_branch}.log"  2> "$qa_sandbox_root/log/R-${_branch}.err.log"
        if [ $? -ne 0 ]
        then
                linkpath="`printf "%s.r%05d" "${_branch}" ${_branch_revision}`"

                $mv_bin $qa_sandbox_root_outputs "$qa_sandbox_root/$linkpath/"
		if [ -e $qa_sandbox_root/$linkpath/log ]
		then
			$mv_bin $qa_sandbox_root/$linkpath/log $qa_sandbox_root/$linkpath/log.2
		fi
                $mv_bin $qa_sandbox_root/log "$qa_sandbox_root/$linkpath/"
                cd $qa_sandbox_root
                $tar_bin -cjf "$qa_archive_root/${linkpath}.$qa_archive_suffix" "$linkpath"

		if [ -r "$qa_sandbox_root/$linkpath/log/summary.txt" ]
		then
			echo -e "error summary non-empty.. dumping to stderr"
	                $cat_bin  "$qa_sandbox_root/$linkpath/log/summary.txt" 1>&2
		else
			echo -e "no error summary; assuming no errors :-)"
		fi
                $rm_bin -r -f "$qa_sandbox_root/$linkpath"

                exit 1
        fi
fi

