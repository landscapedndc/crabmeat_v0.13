
# clean up

if [ ${#@} -lt 6 ]
then
        echo -en "usage: \n`basename $0` <qa user conf> <branch> <branch revision> <branch-build> <branch-root> <branch-binary-root>\n" 1>&2
        echo -en "arguments: ${@}\n" 1>&2
        exit 110
fi

_qa_userconf_name="$1"
_branch="$2"
_branch_revision="$3"
_branchbuild="$4"
_qa_branch_root="$5"
_branch_binary_root="$6"

if [ -r "$_qa_branch_root/$_qa_userconf_name" ]
then
        . "$_qa_branch_root/$_qa_userconf_name"
fi

$rm_bin -f -r --one-file-system "$qa_sandbox_root_outputs" $qa_sandbox_root/log

