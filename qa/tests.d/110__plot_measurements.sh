
# plot measurements against results

if [ ${#@} -lt 6 ]
then
        echo -en "usage: \n`basename $0` <qa user conf> <branch> <branch revision> <branch-build> <branch-root> <branch-binary-root>\n" 1>&2
        echo -en "arguments: ${@}\n" 1>&2
        exit 110
fi

_qa_userconf_name="$1"
_branch="$2"
_branch_revision="$3"
_branchbuild="$4"
_qa_branch_root="$5"
_branch_binary_root="$6"

if [ -r "$_qa_branch_root/$_qa_userconf_name" ]
then
        . "$_qa_branch_root/$_qa_userconf_name"
fi

#echo "`basename $0`: _qa_userconf_name=$_qa_userconf_name _branch=$_branch _branchbuild=$_branchbuild _qa_branch_root=$_qa_branch_root _branch_binary_root=$_branch_binary_root"

rc_plot=0

plot_sources="`mktemp`"
plot_plots="`mktemp`"
tmp_source="$qa_sandbox_root/plotpipe.tmp.XeFrZO6vhP"

function add_source
{
	local _source="$1"
	local _provider="$2"
	local _provider_source="$3"

	local _S="$4"
	S_str="__SRC_$_S"

	printf "\n  %s: &%s\n    path: &__path_%s %s\n    format: txt\n" "$S_str" "$S_str" "$S_str" "$_source" >> $plot_sources

	if [ "${_provider}X" != "-X" ]
	then
		printf "\n    provider:\n      program: %s\n      arguments:\n        - *__path_%s\n        - %s" "$_provider" "$S_str" "$_provider_source" >> $plot_sources 
	fi

	echo "$S_str"
}

function add_plot
{
	local _m_col="$1"
	local _m_src_anchor="$2"
	local _m_scaley="$3"

	local _o_col="$4"
	local _o_src_anchor="$5"
	local _o_scaley="$6"

	local _mo_plot_time="$7"

	local _P="$8"

	P_str="__PLOT_$_P"

	S2="  "
	S4="$S2$S2"
	S6="$S4$S2"
	S8="$S4$S4"
	plot_plots_tag="`printf "\n$S2%s:\n" "$P_str"`"
	plot_plots_time=""
	if [ ! -z "$_mo_plot_time" ]
	then
		plot_plots_time="`printf "${S4}time: %s" "$_mo_plot_time"`"
	fi
	plot_plots_comps="`printf "${S4}compounds:" "$P_str" "$_mo_plot_time"`"
	plot_plots_m="`printf "$S6%s:\n${S8}datasource: *%s\n${S8}style: *%s\n${S8}filter:\n${S8}${S2}scaley: $_m_scaley" "$_m_col" "$_m_src_anchor" "meas_style"`"
	plot_plots_o="`printf "$S6%s:\n${S8}datasource: *%s\n${S8}style: *%s\n${S8}filter:\n${S8}${S2}scaley: $_o_scaley" "$_o_col" "$_o_src_anchor" "sim_style"`"
	printf "\n%s\n%s\n%s\n%s\n%s\n" "$plot_plots_tag" "$plot_plots_time" "$plot_plots_comps" "$plot_plots_m" "$plot_plots_o" >> $plot_plots
}

data_archive_base="`printf "%s.r%05d" "${_branch}" ${_branch_revision}`"
if [ ! -r "$qa_archive_root/${data_archive_base}.$qa_archive_suffix" ]
then
	echo "nothing to be done for \"${data_archive_base}.$qa_archive_suffix\".." >> $qa_reportfile_debug
	exit 0
fi
$tar_bin -xjf "$qa_archive_root/${data_archive_base}.$qa_archive_suffix" -C "$qa_sandbox_root"
if [ ! -d "$qa_sandbox_root/$data_archive_base" ]
then
	echo "expected to find directory \"$qa_sandbox_root/$data_archive_base\" but did not :(" 1>&2
	exit 1
fi

$egrep_bin '^[^ #]' $qa_testprojects_file | cat -n | while read _k _proj _compounds _plot_time _rest
do
	$rm_bin -f $plot_sources $plot_plots

        echo "$_k  $_proj   $_compounds"
        compounds=( `echo -n "$_compounds" | $tr_bin ':' ' '`)
	project_basename="`basename $_proj`"
        for c in ${compounds[@]}
        do
                compound_infofile="$qa_testprojects_plot_root/$_proj/${c}.plot"
                if [ -r "$compound_infofile" ]
                then
                        :
                else
                        echo -e "no info file available for compound  [compound=$c]\n" 1>&2
			continue
                fi

		## set defaults
		qa_plot_time=""
		
		qa_plot_s_src=""
		qa_plot_s_name=""
		qa_plot_s_scaley="1.0"
		
		qa_plot_m_src=""
		qa_plot_m_name=""
		qa_plot_m_scaley="1.0"

		qa_plot_compound_defaults="$qa_testprojects_qa_root/${c}_defaults.plot"
		if [ -r "$qa_plot_compound_defaults" ]
		then
			qa_plot_current_project="$project_basename"
			. "$qa_plot_compound_defaults"
			#echo "$qa_plot_current_project: sourced $qa_plot_compound_defaults" >> $qa_reportfile_debug
		fi

		## set specialized options
		. "$compound_infofile"

                measurement_file="$qa_measurements_root/$_proj/$qa_plot_m_src"
		measurement_column="$qa_plot_m_name"
                if [ -r "$measurement_file" ]
                then
                        :
                else
                        echo -e "measurement file for compound does not exist [missing=$measurement_file]\n" 1>&2
			continue
                fi

                output_file="$qa_sandbox_root/$data_archive_base/$_proj/$qa_plot_s_src"
		output_column="$qa_plot_s_name"
		if [ -r "$output_file" ]
		then
			:
		else
			echo -e "output file for compound does not exist  [missing=$output_file]\n" 1>&2
			continue
		fi

		if [ -z "$measurement_column" -o -z "$output_column" ]
		then
			echo -e "measurement or output column name for compound not given  [o-col=$output_column,m-col=$measurement_column]\n" 1>&2
			continue
		fi

		src_m_anchor=`add_source "$tmp_source" "$measurement_parser" "$measurement_file" "m_${c}_$_k"`
		src_o_anchor=`add_source "$output_file" "-" "-" "o_${c}_$_k"`

		m_scaley=$qa_plot_m_scaley
		o_scaley=$qa_plot_s_scaley

		add_plot "$measurement_column" "$src_m_anchor" "$m_scaley" "$output_column" "$src_o_anchor" "$o_scaley" "$qa_plot_time" "${c}_$_k"
        done

	if [ -r "$plot_sources" -a -r "$plot_plots" -a -r "$measurement_plotter" ]
	then
#		proj_file="$qa_testproject_root/$_proj/`basename $_proj`.xml"
#		if [ ! -r "$proj_file" ]
#		then
#			#echo -e "project file not found; need to read schedule  [missing=$proj_file]\n" 1>&2
#			continue
#		fi
#		plot_time="`egrep -m1 'schedule\ .*time=' $proj_file | egrep -o '"[0-9].*"' | tr -d '"'`"
		plot_time="$_plot_time"
		echo $plot_time >> $qa_reportfile_debug
		if [ -z "$plot_time" ]
		then
			#echo -e "failed to read schedule from project file  [project=$proj_file]\n" 1>&2
			echo -e "no plot schedule given  [project=$_proj]\n" >>$qa_reportfile_debug
			continue
		fi
		bash `dirname $0`/plot_measurements-makeconfig.sh $plot_sources $plot_plots "$plot_time" "$qa_sandbox_root/$data_archive_base/$_proj/measurements.pdf" >> $qa_reportfile_debug

		cwd=`pwd`
		cd `dirname $measurement_plotter`
		bash `dirname $0`/plot_measurements-makeconfig.sh $plot_sources $plot_plots "$plot_time" "$qa_sandbox_root/$data_archive_base/$_proj/measurements_vs_outputs.pdf" | Rscript $measurement_plotter - 
		rc="$?"
		if [ $rc -ne 0 ]
		then
			rc_plot=1
		fi
		cd $cwd
		$rm_bin -f "$plot_sources" "$plot_plots" "$tmp_source"
	fi
done

## archive and move to archive directory
if [ -d "$qa_sandbox_root/$data_archive_base" ]
then
	cd $qa_sandbox_root
	$tar_bin -cjf "$qa_archive_root/${data_archive_base}.$qa_archive_suffix" "$data_archive_base"
	## cleaning up
	$rm_bin -r "$qa_sandbox_root/$data_archive_base"
fi

exit $rc_plot

