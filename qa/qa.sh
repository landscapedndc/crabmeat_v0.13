#!/bin/bash
# vim: ft=sh

if [ ${#@} -lt 7 ]
then
        echo -en "usage: \nqa.sh <reportfile> <qa user conf> <branch source root> <branch binary root> <branch> <branch revision> <branch-build>\n" 1>&2
        echo -en "${#@}" 1>&2
        exit 100
fi

_qa_reportfile="$1"
_qa_userconf_name="$2"
_branch_source_root="$3"
_branch_binary_root="$4"
_branch="$5"
_branch_revision="$6"
_branchbuild="$7"
#echo "L17: reportfile=$_qa_reportfile  branch-root=$_branch_source_root  branch-binary-dir=$_branch_binary_root  branch=$_branch  branch-revision=$_branch_revision branch-build=$_branchbuild" 1>&2

qa_branch_root="$_branch_source_root/qa"

if [ -r "$qa_branch_root/$_qa_userconf_name" ]
then
        . "$qa_branch_root/$_qa_userconf_name"
fi

if [ $QA_DEBUG -a ! -z "$qa_reportfile_debug" ]
then
	if [ -r "$qa_reportfile_debug" ]
	then
		echo "" > $qa_reportfile_debug
	fi
else
	qa_reportfile_debug=/dev/null
fi


function  rc_test_append
{
        local  test_cmd="$1"
        local  rc="$2"

        teststatus="ok"
        if [ $rc -ne 0 ]
        then
                teststatus="fail"

                printf "[%s:%d] %s (output follows)\n\n" $teststatus $rc "$test_cmd" >> $_qa_reportfile
                cat "${_qa_reportfile}.tmp" >> $_qa_reportfile
        fi
	if [ $QA_DEBUG ]
	then
		cat "${_qa_reportfile}.tmp" >> $qa_reportfile_debug
	fi

        $rm_bin -f "${_qa_reportfile}.tmp"
}

## we scan for tests written in
##      gnuplot
##      perl
##      python
##      R
##      bash
test_suite_l=( `$ls_bin $qa_branch_root/$qa_tests_relroot | $egrep_bin '[0-9][0-9][0-9]__[a-z][0-9a-z_]*.(gplt|pl|py|R|sh)$' 2>/dev/null` )
echo "running tests: [${test_suite_l[@]}]" 1>&2

## debugging, etc..
#__qa_bin_prefix="nice -n16"
#__qa_bin_prefix="echo"
__qa_bin_prefix=""

k=0
for qa_test_i in ${test_suite_l[@]}
do
	k=$(( $k + 1 ))
	printf "[%02d] \"$qa_test_i\"\n" $k 1>&2

        qa_test="$qa_branch_root/$qa_tests_relroot/$qa_test_i"
        ## skip directories
        if [ -d "$qa_test" -o ! -r "$qa_test" ]
        then
                echo "skipping invalid test  [test=$qa_test]"
                continue
        fi

        cd "$qa_sandbox"

        qa_test_cmd_line="$qa_test  $_qa_userconf_name $_branch $_branch_revision $_branchbuild $qa_branch_root $_branch_binary_root"

        case "${qa_test##*.}" in

                gplt)
                        if [ -z "$gnuplot_bin" -o ! -x "$gnuplot_bin" ]
                        then
                                echo "missing interpreter for script  [script=`basename $qa_test`]" 1>&2
                                continue
                        fi
                        $__qa_bin_prefix  $gnuplot_bin $qa_test_cmd_line 2> ${_qa_reportfile}.tmp 1>>$qa_reportfile_debug
                        rc_test_append "$gnuplot_bin $qa_test_cmd_line" $?
                        ;;

                pl)
                        if [ -z "$perl_bin" -o ! -x "$perl_bin" ]
                        then
                                echo "missing interpreter for script  [script=`basename $qa_test`]" 1>&2
                                continue
                        fi
                        $__qa_bin_prefix  $perl_bin $qa_test_cmd_line 2> ${_qa_reportfile}.tmp 1>>$qa_reportfile_debug
                        rc_test_append "$perl_bin $qa_test_cmd_line" $?
                        ;;

                py)
                        if [ -z "$python_bin" -o ! -x "$python_bin" ]
                        then
                                echo "missing interpreter for script  [script=`basename $qa_test`]" 1>&2
                                continue
                        fi
                        $__qa_bin_prefix  $python_bin $qa_test_cmd_line 2> ${_qa_reportfile}.tmp 1>>$qa_reportfile_debug
                        rc_test_append "$python_bin $qa_test_cmd_line" $?
                        ;;

                R)
                        if [ -z "$R_bin" -o ! -x "$R_bin" ]
                        then
                                echo "missing interpreter for script  [script=`basename $qa_test`]" 1>&2
                                continue
                        fi
                        $__qa_bin_prefix  $R_bin $qa_test_cmd_line 2> ${_qa_reportfile}.tmp 1>>$qa_reportfile_debug
                        rc_test_append "$R_bin $qa_test_cmd_line" $?
                        ;;

                sh)
                        if [ -z "$bash_bin" -o ! -x "$bash_bin" ]
                        then
                                echo "missing interpreter for script  [script=`basename $qa_test`]" 1>&2
                                continue
                        fi
                        $__qa_bin_prefix  $bash_bin $qa_test_cmd_line  2>>${_qa_reportfile}.tmp 1>>$qa_reportfile_debug
                        rc_test_append "$bash_bin $qa_test_cmd_line" $?
                        ;;

                *)
                        rc_test_append "sorry .. no clue how to handle test script  [script=`basename $qa_test`]" "-1"
                        ;;
        esac
done

if [ -e "$_qa_reportfile" ]
then
        echo -n "fail"
        exit 1
else
        echo -n "ok"
        exit 0
fi

