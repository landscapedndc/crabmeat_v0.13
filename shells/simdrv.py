

import  os
import  sys
try :
    sys.path.insert( 0, os.environ['CGO_ROOT']+'/shells/python')
except KeyError :
    print "environment variable CGO_ROOT not set"
    sys.exit( 1)

import  cgo_helpers
import  cgo_db

import  signal
import  time

TIMESTEPS = 10

CGO_ERR_SIGINT = 100

class  cgo_signal_handler( object) :
    def  __init__( self, _sig=signal.SIGINT) :
        self.sig = _sig
        self.signal_handler_orig = signal.getsignal( self.sig)
        self.interrupted = False
        self.released = True


    def  on( self) :
        def  signal_handler( _signum, _frame):
            self.released = False
            self.interrupted = True
        signal.signal( self.sig, signal_handler)

    def  off( self) :
        if  self.released :
            return  False
        signal.signal( self.sig, self.signal_handler_orig)
        self.released = True
        return  True


class  simulation_driver( object) :

    def  __init__( self, _n_tasks, _timesteps) :

        self.sighandler = cgo_signal_handler()

	self.n_exspected_tasks = _n_tasks
	self.timesteps = _timesteps

	self.task_pool = {}
	self.task_names = []

	self.db = cgo_db.cgo_db()
	self.connect( 'localhost', 6379)

        self.finalized = False

    def  connect( self, _db_ipaddr, _db_port) :
	self.db.connect( cgo_helpers.cgo_hostaddress( _db_ipaddr, _db_port))
        self.db.flushdb()

    def  await_tasks( self) :

        join_reply = self.__tasks_join()
        if  join_reply != "OK" :
            return  self.__tasks_terminate( join_reply)
        print 'tasks=%s' % str( self.task_names)

        self.task_pool = { 0: self.task_names}
        pub_reply = self.__tasks_publish_ports()
        if  pub_reply != "OK" :
            return  self.__tasks_terminate( pub_reply)

        sched_reply = self.__tasks_schedule()
        if  sched_reply != "OK" :
            return  self.__tasks_terminate( sched_reply)

        return  "OK"

    def  __tasks_join( self) :

        while True :
            client_command = self.db.wait_command()
            client_name, command = client_command.split( ':')
            if command == 'JOIN' :
                join_status = self.__task_join( client_name)
                if join_status != 'OK' :
                    return  join_status
                print  'client "%s" joined at %s' % ( client_name, time.asctime())

                if len( self.task_names) == self.n_exspected_tasks :
                    break

            else :
                return  "[EE] command not understood  [command=%s, by=%s]" % ( command, client_name)
        return  'OK'

    def  __task_join( self, _client_name) :
      	if  _client_name in self.task_pool :
            return  "[EE] task already exists  [task=%s]" % ( _client_name)

        print( "new client group: \"%s\"" % ( _client_name))
	self.task_names.append( _client_name)
        return  "OK"

    def  __tasks_schedule( self) :
	is_ok = self.__check_dependencies()
	if not is_ok :
	    return  "tasks have unmet dependencies"
	is_ok = self.__construct_task_graph()
	if not is_ok :
	    return  "tasks have cyclic dependencies"
	is_ok = self.__schedule_tasks()
	if not is_ok :
	    return  "tasks could not be scheduled for execution"
	return  "OK"


    def  __check_dependencies( self) :
	for task in self.task_names :
	    dep_ok = self.db.test_task_dependencies( task,
			[ t for t in self.task_names if t != task])
	    if not dep_ok :
		return False
	return  True

    def  __construct_task_graph( self) :
	self.task_pool = {}
	for task in self.task_names :
	    self.task_pool[task] = []
	for task in self.task_names :
	    for other_task in self.task_names :
		if task == other_task :
		    continue
		if self.db.has_task_dependency( task, other_task) :
		    self.task_pool[task].append( other_task)
	print  "task-pool=%s" % self.task_pool
	return  True

    def  __schedule_tasks( self) :
	sched_rc = "OK"

	task_pool = {}
	task_rank = 0
	while self.task_pool :
	    task_pool[task_rank] = []
	    for task in self.task_pool :
		if len( self.task_pool[task]) == 0 :
		    task_pool[task_rank].append( task)

	    for task in task_pool[task_rank] :
		del self.task_pool[task]

	    ## if no 'source' exists, and pool is not empty
	    if self.task_pool and ( len( task_pool[task_rank]) == 0) :
		sched_rc = "cycle"
		break

	    for task in task_pool[task_rank] :
		for other_task in self.task_pool :
		    if task in self.task_pool[other_task] :
			self.task_pool[other_task].remove( task)


	    task_rank += 1

	if sched_rc == "OK" :
	    self.task_pool = task_pool
	    print  "task-schedule=%s" % self.task_pool
	    return  True
	return  False
	    


    def  execute( self) :
        self.sighandler.on()
        exec_reply = self.__execute()
        self.sighandler.off()
        return  exec_reply
    def  __execute( self) :

        rc_ini = self.tasks_initialize()
        if  self.sighandler.interrupted :
            return  self.__tasks_terminate( "SIGINT")
        if  rc_ini != "OK" :
            return  self.__tasks_terminate( "initialize failed")

        rc_exe = self.tasks_execute()
        if  self.sighandler.interrupted :
            return  self.__tasks_terminate( "SIGINT")
        if  rc_exe != "OK" :
            return  self.__tasks_terminate( "execute failed")

        rc_fin = self.tasks_finalize()
        if  self.sighandler.interrupted :
            return  self.__tasks_terminate( "SIGINT")
        if  rc_fin != "OK" :
            return  self.__tasks_terminate( "finalize failed")

        return  self.__tasks_terminate( "OK")

    def  __tasks_terminate( self, _status) :
	## terminate tasks
        if not self.finalized :
            self.tasks_finalize()
        for task in self.task_names :
            self.db.send_to_client_queue( task, 'TERM')
        return  _status


    def  __tasks_graph_call( self, _task_pool, _task_msg) :

        rc = "OK"
	for rank in _task_pool :

	    n_tasks = len( _task_pool[rank])

	    for task in _task_pool[rank] :
		#print "send command \"%s\" to %s" % ( _task_msg, task)
                self.db.send_to_client_queue( task, _task_msg)
	    while  n_tasks > 0 :
                task_done = self.db.wait_command()
                client, message = task_done.split( ':')
		if  message == '%s_FAIL' % ( _task_msg) :
		    print "[EE]  DRV: command failed to execute on client \"%s\": %s" % ( client, _task_msg)
		    rc = 'ERROR'
                elif  message == '%s_OK' % ( _task_msg) :
		    n_tasks -= 1
                else :
                    ## put message back
                    self.db.send_to_master_queue( task_done, False)
		    
        return  rc
                    

    def  __tasks_publish_ports( self) :
	print "publish.."
	return  self.__tasks_graph_call( self.task_pool, 'P')

    def  tasks_initialize( self) :
	print "initialize.."
	return  self.__tasks_graph_call( self.task_pool, 'I')

    def  tasks_execute( self) :
	print "execute.."
	for t in range( self.timesteps) :
	    #print  "t=%d" % ( t)
	    rc_exec = self.__tasks_graph_call( self.task_pool, 'E')
            if  rc_exec != "OK" :
                return  rc_exec
        return  "OK"
        
    def  tasks_finalize( self) :
	print "finalize.."
        if self.finalized == True :
            return  "OK"
        self.finalized = True
	return  self.__tasks_graph_call( self.task_pool, 'F')



if __name__ == '__main__' :

    n_clients = int( sys.argv[1]) if len( sys.argv) > 1 else N_CLIENTS
    timesteps = int( sys.argv[2]) if len( sys.argv) > 2 else TIMESTEPS

    simdrv = simulation_driver( n_clients, timesteps)
    status_msg = simdrv.await_tasks()
    if  status_msg == "OK" :
	status_msg = simdrv.execute()
    print  status_msg

    sys.exit( 0 if status_msg == "OK" else 1)

