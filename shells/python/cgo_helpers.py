
class  cgo_hostaddress( object) :
    def  __init__( self, _addr, _port) :
        self.address = str( _addr)
        self.port = int( _port)

    def  fqdn( self, _proto=None) :
        proto = ''
        if _proto :
                proto = '%s://' % ( _proto)
        return  '%s%s:%d' % ( proto, self.address, self.port)

