
import  cgo_db

class  cgo_port( object) :
    def  __init__( self, _name, _type, _datatype=None) :
	self.name = _name
	self.type = _type
	self.datatype = _datatype

	self.active = True

	self.dt = 1
	self.count = -1
	self.offset = 0

	self.bb = 0.0

	self.unit = '?'

	self.update_callback = None
	self.dir = '?'

    def  set_property( self, _property, _value) :
	if _property == 'count' :
	    self.count = int( _value)
	elif _property == 'offset' :
	    self.offset == int( _value)
	else :
	    print  "unknown property  [property=%s]" % ( _property)
	    return  None
	return  self

    def  update( self) :
	if not self.update_callback is None :
	    self.update_callback( self)

class  cgo_ports( object) :
    def  __init__( self, _name) :
	self.task_name = _name
	self.ports = []

    def  register_ports( self, _ports) :
        for port in _ports['out'] :
	    self.register_port( port, "out")
        for port in _ports['in'] :
            self.register_port( port, "int")

    def  register_port( self, _port_name, _dir, _callback) :
	new_port = None
	for port in self.ports :
	    if port.name == _port_name and port.dir == _dir :
		new_port = port
		break
	if new_port is None :
	    new_port = cgo_port( _port_name, _dir)
	    if _dir == 'in' and new_port.name[0] == '+' :
                new_port.name = new_port.name.lstrip( '+')
                new_port.offset = 1

	    print "registering port \"%s\" [%s]" % ( new_port.name, _dir)
 	    
	    self.ports.append( new_port)

	new_port.update_callback = _callback
	return  new_port

    def  publish_ports( self, _db) :
        for port in self.ports :
	    pub_listname = '@ports.%s.%s' % ( port.type, self.task_name)
            print  'publish \"%s\" in \"%s\"' % ( port.name, pub_listname)
            _db.sadd( pub_listname, port.name)
	    if ( port.offset > 0) :
		_db.sadd( '%s.provide_initial_value' % ( pub_listname), port.name)

    def  _readwrite_shared( self, _dir) :
	for port in self.ports :
	    if port.active and port.offset == 0 and port.count != 0 and port.type == _dir :
		port.dir = _dir
		port.update()
		if port.count != -1 :
		    port.count -= 1
	    elif port.offset > 0 :
		port.offset -= 1
    def  read_shared( self) :
	self._readwrite_shared( 'in')
    def  write_shared( self) :
	self._readwrite_shared( 'out')


class  cgo_task( object) :
    def  __init__( self, _name, _db) :
        self.name = _name
        self.db = _db

        self.ports = cgo_ports( self.name)

    def  register_ports( self, _ports) :
	self.ports.register_ports( _ports)
    def  register_port( self, _port_name, _dir, _callback) :
	return  self.ports.register_port( _port_name, _dir, _callback)

    def  publish_ports( self) :
	self.ports.publish_ports( self.db)

    def  initialize( self) :
        return  0
    def  execute( self) :
        return  0
    def  finalize( self) :
        return  0

    def  read_shared( self) :
	self.ports.read_shared()
    def  write_shared( self) :
	self.ports.write_shared()


class  cgo_shell( object) :
    def  __init__( self, _task) :
        self.task = _task

    def  join( self) :
        self.task.db.clear_client_queue()
        self.task.db.join_simulation()

    def  execute( self) :

        print( '%s: launching task' % ( self.task.name))
        rc = 0

        n_msg = 0
        n_exe = 0
        while  True :

            task_cmd = self.task.db.wait_command()

            print "received command: %s: %s" % ( self.task.name, task_cmd)

            rc_cmd = 0

            if  task_cmd == 'E' :
		self.task.read_shared()
        	rc_cmd = self.task.execute()
		self.task.write_shared()
                n_exe += 1

	    elif  task_cmd == 'P' :
		rc_cmd = self.task.publish_ports()
            elif  task_cmd == 'I' :
		self.task.read_shared()
                rc_cmd = self.task.initialize()
		self.task.write_shared()

            elif  task_cmd == 'F' :
                rc_cmd = self.task.finalize()
		self.task.write_shared()

            elif  task_cmd == b'TERM' :
                print( '%s: driver asked us to terminate  [command="%s"]' % ( self.task.name, task_cmd))
                break

            else :
                print( '%s: driver command not understood  [command="%s"]' % ( self.task.name, task_cmd))

            if  rc_cmd :
	    	rc = rc_cmd
		print( '%s: task failed  [stage=%s]' % ( self.task.name, task_cmd))

## TODO  send status (e.g. rc_cmd)
            self.task.db.send_to_master_queue(
                    '%s_%s' % ( task_cmd, 'OK' if rc==0 else 'FAIL'))


            n_msg += 1

        print( '')
        print( '%s: received %d task execute notifications [total=%d]' % ( self.task.name, n_exe, n_msg))

        return  rc

