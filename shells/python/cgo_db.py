
import  redis
import  msgpack


class  cgo_db_key( object) :
    def  __init__( self, _component, _id, _entity) :
        self.key = '%s@%d[%s:%d]%s' % ( "cgo", 0, _component, int( _id), _entity)

    def  get_key( self) :
        return  self.key


class  cgo_db( object) :
    def  __init__( self, _name=None) :
        self.db = None
        self.name = _name if _name is not None else 'MASTER'
        self.queue = '@queue.%s' % ( self.name)
        self.master_queue = '@queue.%s' % ( 'MASTER')

    def  connect( self, _addr) :
        print( 'connecting to db @%s' % ( _addr.fqdn()))

        if  self.db is not None :
            return  0

        self.db = redis.StrictRedis( _addr.address, _addr.port)
        if  self.db is None :
            return  -1
        return  0

    def  flushdb( self) :
        self.db.flushdb()

    def  has_task_dependency( self, _task, _other_task) :
        task_ports_in = '@ports.in.%s' % ( _task)
        task_ports_out = '@ports.out.%s' % ( _other_task)
        inout_inter = self.db.sinter( [task_ports_in, task_ports_out])
        if  len( inout_inter) > 0 :
            initialized_items = self.db.smembers( '%s.provide_initial_value' % ( task_ports_in))
            for item in initialized_items :
                if item in inout_inter :
                    inout_inter.discard( item)
        return  len( inout_inter) > 0
            
    def  test_task_dependencies( self, _task, _other_tasks) :
        task_ports_in = '@ports.in.%s' % ( _task)
        task_ports_in_card = self.db.scard( task_ports_in)
        print "#dep(%s)=%d" % ( _task, task_ports_in_card)
        if  task_ports_in_card == 0 :
            return  True

        task_ports_out_all = '@ports.in.ALL.%s' % ( _task)
        tasks_ports_out = [ '@ports.out.%s' % ( t) for t in _other_tasks]
        print "%s: available output port lists %s" % ( _task, tasks_ports_out)
        if  len( tasks_ports_out) == 0 :
            return  False

        ports_out_union = self.db.sunionstore( task_ports_out_all, tasks_ports_out)
        print "%s: #out-ports=%d" % ( _task, ports_out_union)
        if  ports_out_union > 0 :
            inout_inter = self.db.sinter( [ task_ports_in, task_ports_out_all])
            print "%s: in/out intersect -- %s [|%d|]" % ( _task, inout_inter, len( inout_inter))
            self.db.delete( task_ports_out_all)
            if  len( inout_inter) == task_ports_in_card :
                return  True
        return  False
        

#    def  set( self, _entity, _value) :
#        self.db.set( _entity.get_key(), _value)
#    def  get( self, _entity) :
#        return  self.db.get( _entity.get_key())
#
#
#        
#    def  sets( self, _entity, _value) :
#        self.set( _entity, str( _value))
#    def  gets( self, _entity) :
#        sval = self.get( _entity)
#        if sval is None :
#            return  None
#        return  str( sval)
#
#    def  setd( self, _entity, _value) :
#        self.set( _entity, _value)
#    def  getd( self, _entity) :
#        dval = self.get( _entity)
#        if dval is None :
#            return  None
#        return  float( dval)
#
#    def  seti( self, _entity, _value) :
#        self.set( _entity, _value)
#    def  geti( self, _entity) :
#        ival = self.get( _entity)
#        if ival is None :
#            return  None
#        return  int( ival)
#
#
#
#    def  hset( self, _entity, _field, _index, _value) :
#        self.db.hset( _entity.get_key(), '%s@%d' % ( _field, _index), _value)
#    def  hget( self, _entity, _field, _index) :
#        return  self.db.hget( _entity.get_key(), '%s@%d' % ( _field, _index))
#
#    def  mssets( self, _entity, _field, _index, _value) :
#        self.hset( _entity, _field, _index, _value)
#    def  msgets( self, _entity, _field, _index) :
#        sval = self.hget( _entity, _field, _index)
#        if sval is None :
#            return  None
#        return  str( sval)
#    def  mssetd( self, _entity, _field, _index, _value) :
#        self.hset( _entity, _field, _index, _value)
#    def  msgetd( self, _entity, _field, _index) :
#        dval = self.hget( _entity, _field, _index)
#        if dval is None :
#            return  None
#        return  float( dval)
#
#
#    def  ssetv_( self, _entity, _field, _values) :
#	self.db.hset( _entity.get_key(), '%s' % ( _field), '%s' % ( self.vector_delimiter.join( str( v) for v in _values)))
#	self.db.hset( _entity.get_key(), '%s_size' % ( _field), '%d' % ( len( _values)))
#    def  ssetvd( self, _entity, _field, _values) :
#	self.ssetv_( _entity, _field, _values)
#    def  ssetvi( self, _entity, _field, _values) :
#	self.ssetv_( _entity, _field, _values)
#    def  sgetvd( self, _entity, _field) :
#	vval = self.db.hget( _entity.get_key(), '%s' % ( _field))
#	if  vval is None :
#	    return  None
#	return  [ float( v) for v in vval.split( self.vector_delimiter) ]
#    def  sgetvi( self, _entity, _field) :
#	vval = self.db.hget( _entity.get_key(), '%s' % ( _field))
#	if  vval is None :
#	    return  None
#	return  [ int( v) for v in vval.split( self.vector_delimiter) ]

    def  send_to_master_queue( self, _message, _prepend_name=True) :
        msg = _message
        if _prepend_name :
            msg = '%s:%s' % ( self.name, _message)
        print msg
        self.db.rpush( self.master_queue, msg)


    def  join_simulation( self) :
        self.send_to_master_queue( 'JOIN')

    def  clear_client_queue( self) :
        print 'clearing client queue  [client=%s]' % ( self.name)
        self.db.delete( '@queue.%s' % ( self.name))

    def  send_to_client_queue( self, _client_name, _message) :
        print _message
        self.db.rpush( '@queue.%s' % ( _client_name), _message)

    def  wait_command( self) :
        command = self.db.blpop( [ self.queue], 0)
        print  command
        return  command[1]

    def  sadd( self, _set, _name) :
        self.db.sadd( _set, _name)

    def  read_vector( self, _key, _geoindex) :
        vector = self.db.hget( _key, _geoindex)
        if vector is None :
            return  []
        return  msgpack.unpackb( vector)

    def  write_vector( self, _key, _geoindex, _vector) :
        self.db.hset( _key, _geoindex, msgpack.packb( _vector))


