
#ifndef  CGO_DB_H_
#define  CGO_DB_H_

#include  "cgo_ports.h"
#include  "cgo_helpers.h"
#include  <stddef.h>

#ifdef  __cplusplus
extern "C"
{
#endif


#define  CGO_EOK  0
#define  CGO_EFAIL  -1
#define  CGO_ENOEXISTS  1
#define  CGO_ENOMEM  -3
#define  CGO_EINSUFFMEM  2

#define  CGO_VECTOR_ELEMENT_DELIMITER  ';'

#define  CGO_DB_VALUE_MAXSIZE  128
/* key example: '[climate:0]precipitation' */
#define  CGO_DB_KEY_MAXSIZE  95
/* unit example: 'kg/m^3' */
#define  CGO_DB_UNIT_MAXSIZE  31
struct cgo_db_key_t
{
    char  key[CGO_DB_KEY_MAXSIZE+1];
// sk:TODO    char  unit[CGO_DB_UNIT_MAXSIZE+1];
};

// sk:off struct cgo_db_key_t *  cgo_db_key_new( char const *  prefix, char const *  component, int  id, char const *  entity);
// sk:off void  cgo_db_key_destroy( struct cgo_db_key_t *  key);
int  cgo_db_key_set( struct cgo_db_key_t *  key, char const *  component, int  id, char const *  entity);
int  cgo_db_key_setg( struct cgo_db_key_t *  key, char const *  component, int  id, char const *  entity, char const *  subentity);
char const *  cgo_db_key_get( struct cgo_db_key_t const *  key);

extern void  cgo_free_db_connection( void *);

struct cgo_db_connection_pool_t
{
    struct cgo_lifo_t *  ctxs;
    struct cgo_hostaddress_t *  host;
    int  size, maxsize;
};
struct cgo_db_connection_pool_t *  cgo_db_connection_pool_new( struct cgo_hostaddress_t const *  haddr, int  maxsize);
void  cgo_db_connection_pool_destroy( struct cgo_db_connection_pool_t *  cxn_pool);
void *  cgo_db_connection_pool_conn_acquire( struct cgo_db_connection_pool_t *  cxn_pool);
int  cgo_db_connection_pool_conn_release( struct cgo_db_connection_pool_t *  cxn_pool, void *  cxn);

//#define  CGO_BD_HAVE_BATCH_OP
int  cgo_db_batchstart( void *);
int  cgo_db_batchstop( void *);

struct cgo_db_t
{
    struct cgo_db_connection_pool_t *  cxns;
};

struct cgo_db_t *  cgo_db_new( struct cgo_hostaddress_t const *, int  maxsize);
void  cgo_db_destroy( struct cgo_db_t *);

int cgo_db_publish_port( struct cgo_db_t *,
        char const * /*task name*/, struct cgo_port_t const * /*port*/);


int  cgo_db_send_to_master_queue( struct cgo_db_t *, char const * /*client name [NULL to omit]*/, char const * /*message*/);
int  cgo_db_join_simulation( struct cgo_db_t *, char const * /*client name*/);
int  cgo_db_clear_client_queue( struct cgo_db_t *, char const * /*client name*/);
int  cgo_db_send_to_client_queue( struct cgo_db_t *, char const * /*client name*/, char const * /*message*/);
int  cgo_db_wait_command( struct cgo_db_t *, char const * /*client name*/, char ** /*command*/);


struct  cgo_geospatial_key_t
{
    char const *  portname;
    long unsigned int  geoindex;
    int  timestamp;
    int  delta_t;
};
int  cgo_db_read_array( struct cgo_db_t *, struct cgo_geospatial_key_t *, void * /*data*/, int /*rank*/, int * /*lengths*/, cgo_datatype_e);
int  cgo_db_read_vector( struct cgo_db_t *, struct cgo_geospatial_key_t *, void * /*data*/, int * /*length*/, cgo_datatype_e);
int  cgo_db_write_array( struct cgo_db_t *, struct cgo_geospatial_key_t *, void * /*data*/, int /*rank*/, int * /*lengths*/, cgo_datatype_e);
int  cgo_db_write_vector( struct cgo_db_t *, struct cgo_geospatial_key_t *, void * /*data*/, int * /*length*/, cgo_datatype_e);

#ifdef  __cplusplus
}
#endif

#endif  /*  !CGO_DB_H_  */

