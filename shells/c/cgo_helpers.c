
#include  "cgo_helpers.h"

#include  <stdlib.h>
#include  <stdio.h>
#include  <string.h>

char *  cgo_strdup( char const *  _string)
{
        char *  string_cpy = NULL;
        if ( _string)
        {
                size_t  len = strlen( _string);
                string_cpy = (char*)malloc(( len+1) * sizeof( char));
                if ( string_cpy)
                {
                        strcpy( string_cpy, _string);
                }
        }
        return  string_cpy;
}

struct cgo_hostaddress_t *  cgo_hostaddress_new()
{
    struct cgo_hostaddress_t *  haddr =
        (struct cgo_hostaddress_t *)malloc( sizeof( struct cgo_hostaddress_t));
    if ( haddr)
    {
        haddr->hostname = NULL;
        haddr->port = -1;
    }
    return  haddr;
}
struct cgo_hostaddress_t *  cgo_hostaddress_newset( char const *  _addr, int  _port)
{
    struct cgo_hostaddress_t *  haddr = cgo_hostaddress_new();
    if ( haddr)
    {
        cgo_hostaddress_set( haddr, _addr, _port);
    }
    return  haddr;
}

void  cgo_hostaddress_destroy( struct cgo_hostaddress_t *  _haddr)
{
    if ( _haddr)
    {
        if ( _haddr->hostname)
        {
            free( _haddr->hostname);
        }
        free(( void *)_haddr);
    }
}

int  cgo_hostaddress_copy( struct cgo_hostaddress_t *  _clone, struct cgo_hostaddress_t const *  _haddr)
{
        if ( !_clone || !_haddr)
        {
                return  -1;
        }

        if ( _haddr->hostname)
        {
                _clone->hostname = cgo_strdup( _haddr->hostname);
        }
        _clone->port = _haddr->port;

        return  0;
}

struct cgo_hostaddress_t *  cgo_hostaddress_clone( struct cgo_hostaddress_t const *  _haddr)
{
        if ( !_haddr)
        {
                return  NULL;
        }

        struct cgo_hostaddress_t *  clone = cgo_hostaddress_new();
        if ( clone)
        {
                int  rc_copy = cgo_hostaddress_copy( clone, _haddr);
                if ( rc_copy)
                {
                        free( clone);
                        return  NULL;
                }
        }

        return  clone;
}

void  cgo_hostaddress_set( struct cgo_hostaddress_t *  _haddr, char const *  _addr, int  _port)
{
    if ( _haddr->hostname)
    {
        free(( void*)_haddr->hostname);
    }
    _haddr->hostname = cgo_strdup( _addr);
    _haddr->port = _port;
}

char *  cgo_hostaddress_fqdn( struct cgo_hostaddress_t *  _haddr, char const *  _proto)
{
    char *  addr = malloc( sizeof( char)*( strlen(_proto) + 3/* "://" */ + strlen(_haddr->hostname) + 1 /* ":" */ + 10));
    if ( addr)
    {
        sprintf( addr, "%s://%s:%d", _proto, _haddr->hostname, _haddr->port);
    }
    return  addr;
}
void  cgo_hostaddress_fqdn_free( char *  _fqdn)
{
    if ( _fqdn)
    {
        free(( void *)_fqdn);
    }
}



struct cgo_listdata_t *  cgo_listdata_new()
{
        struct cgo_listdata_t *  cgo_listdata =
                (struct cgo_listdata_t *)malloc( sizeof( struct cgo_listdata_t));
        if ( cgo_listdata)
        {
                cgo_listdata->next = NULL;
                cgo_listdata->data = NULL;
        }
        return  cgo_listdata;
}

void  cgo_listdata_destroy( struct cgo_listdata_t *  _listdata, void (*freedata)( void *))
{
        if ( _listdata)
        {
                if ( freedata)
                {
                        freedata( _listdata->data);
                }
                free(( void*)_listdata);
        }
}



struct cgo_lifo_t *  cgo_lifo_new()
{
        struct cgo_lifo_t *  lifo =
                (struct cgo_lifo_t *)malloc( sizeof( struct cgo_lifo_t));
        if ( lifo)
        {
                lifo->head = NULL;
                lifo->tail = NULL;
        }
        return  lifo;
}
void  cgo_lifo_destroy( struct cgo_lifo_t *  _lifo, void (*freedata)( void *))
{
        if ( _lifo)
        {
                struct cgo_listdata_t *  lifoitem = _lifo->head;
                while ( lifoitem)
                {
                        struct cgo_listdata_t *  delitem = lifoitem;
                        lifoitem = lifoitem->next;

                        cgo_listdata_destroy( delitem, freedata);
                }
                free( _lifo);
        }
}

int  cgo_lifo_empty( struct cgo_lifo_t const *  _lifo)
{
        return  _lifo->head == NULL;
}

int  cgo_lifo_push( struct cgo_lifo_t *  _lifo, void *  _data)
{
        struct cgo_listdata_t *  lifoitem = cgo_listdata_new();
        if ( lifoitem)
        {
                lifoitem->next = NULL;
                lifoitem->data = _data;

                if ( !_lifo->head)
                {
                        _lifo->head = lifoitem;
                        _lifo->tail = _lifo->head;
                }
                else
                {
                        _lifo->tail->next = lifoitem;
                        _lifo->tail = _lifo->tail->next;
                }
// sk:dbg                printf( "cgo_lifo_push(): " "queuing element in lifo\n");

                return  0;
        }

        return  -1;
}

void *  cgo_lifo_pop( struct cgo_lifo_t *  _lifo)
{
        if ( _lifo->head)
        {
                struct cgo_listdata_t *  lifoitem = _lifo->head;
                _lifo->head = _lifo->head->next;
                if ( _lifo->head == NULL)
                {
                        _lifo->tail = NULL;
                }

                void *  data = lifoitem->data;
                cgo_listdata_destroy( lifoitem, NULL);
// sk:dbg                printf( "cgo_lifo_pop(): " "dequeuing element from lifo\n");
                return  data;
        }

        return  NULL;
}
