#ifndef  CGO_PORTS_H_
#define  CGO_PORTS_H_

#include  <string.h>
#include  "cgo_helpers.h"

#ifdef  __cplusplus
extern "C"
{
#endif

enum  cgo_port_type_e
{
    PORT_IN = 1,
    PORT_OUT = 2,
    PORT_INOUT = PORT_IN|PORT_OUT
};

enum  cgo_datatype
{
    CGO_STRING,
    CGO_INT,
    CGO_UNSIGNED_INT,
    CGO_FLOAT,
    CGO_DOUBLE
};
typedef  enum cgo_datatype  cgo_datatype_e;

struct ports_static_info_t
{
        char const *  name;
        enum cgo_port_type_e  type;
        enum cgo_datatype  datatype;
};

struct cgo_port_t
{
    char *  name; /*item's key*/
    enum cgo_port_type_e  type;

    int  dt; /*call update() every 'dt' timesteps*/
    int  count; /*call update() 'count' times, -1 for unlimited*/
    int  offset; /*call update() only after 'offset' timesteps*/

    double  bb;

    char  unit[20];
    enum cgo_datatype  datatype;
//    int  rank;
//    int *  sizes;

    int  active; /*0 if inactive, i.e. update() is not called*/
    int (*update)( struct cgo_port_t *, void *);
    void *  data;
    enum cgo_port_type_e  dir;
};
int  cgo_port_init( struct cgo_port_t *, char const * /*name*/, enum cgo_port_type_e, enum cgo_datatype);
void  cgo_port_deinit( struct cgo_port_t *);
//int  cgo_port_set_size_scalar( struct cgo_port_t *);
//int  cgo_port_set_size_vector( struct cgo_port_t *, int /*size*/);
//int  cgo_port_set_size_matrix( struct cgo_port_t *, int /*size 1*/, int /*size 2*/);
//int  cgo_port_set_size( struct cgo_port_t *, int /*rank*/, int const * /*sizes*/);

struct cgo_ports_t
{
    struct cgo_port_t *  ports;
    size_t  n_ports;

    /* set to nonzero if task has dependencies
     * but can provide initial values */
    int  has_initialized_dependencies;
};
struct cgo_ports_t *  cgo_ports_new(
        struct ports_static_info_t const * /*ports info*/, size_t /*#ports info*/);
void  cgo_ports_destroy( struct cgo_ports_t *);
int  cgo_ports_init( struct cgo_ports_t *,
        struct ports_static_info_t const * /*ports info*/, size_t /*#ports info*/);
void  cgo_ports_deinit( struct cgo_ports_t *);
//int  cgo_ports_set_size_scalar( struct cgo_ports_t *, char const * /*name*/);
//int  cgo_ports_set_size_vector( struct cgo_ports_t *, char const * /*name*/, int /*size*/);
//int  cgo_ports_set_size_matrix( struct cgo_ports_t *, char const * /*name*/, int /*size 1*/, int /*size 2*/);
//int  cgo_ports_set_size( struct cgo_ports_t *, char const * /*name*/, int /*rank*/, int const * /*sizes*/);

#ifdef  __cplusplus
}
#endif

#endif  /*  !CGO_PORTS_H_  */

