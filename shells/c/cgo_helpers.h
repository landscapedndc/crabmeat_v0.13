
#ifndef  CGO_HELPERS_H_
#define  CGO_HELPERS_H_

#ifdef  __cplusplus
extern "C"
{
#endif

char *  cgo_strdup( char const * /*string*/);

struct cgo_hostaddress_t
{
        char *  hostname;
        int  port;
};

struct cgo_hostaddress_t *  cgo_hostaddress_new();
struct cgo_hostaddress_t *  cgo_hostaddress_newset( char const *  addr, int  port);
void  cgo_hostaddress_destroy( struct cgo_hostaddress_t *  haddr);
int  cgo_hostaddress_copy( struct cgo_hostaddress_t *  clone, struct cgo_hostaddress_t const *  haddr);
struct cgo_hostaddress_t *  cgo_hostaddress_clone( struct cgo_hostaddress_t const *  haddr);
void  cgo_hostaddress_set( struct cgo_hostaddress_t *  haddr, char const *  addr, int  port);
char *  cgo_hostaddress_fqdn( struct cgo_hostaddress_t *  haddr, char const *  proto);
void  cgo_hostaddress_fqdn_free( char *  fqdn);


struct cgo_listdata_t
{
    struct cgo_listdata_t *  next;
    void *  data;
};

struct cgo_listdata_t *  cgo_listdata_new();
void  cgo_listdata_destroy( struct cgo_listdata_t *  listdata, void (*freedata)( void *));



struct cgo_lifo_t
{
    struct cgo_listdata_t  *  head;
    struct cgo_listdata_t  *  tail;
};

struct cgo_lifo_t *  cgo_lifo_new();
void  cgo_lifo_destroy( struct cgo_lifo_t *  lifo, void (*freedata)( void *));
int  cgo_lifo_empty( struct cgo_lifo_t const *  lifo);
int  cgo_lifo_push( struct cgo_lifo_t *  lifo, void *);
void *  cgo_lifo_pop( struct cgo_lifo_t *  lifo);

#ifdef  __cplusplus
}
#endif

#endif  /*!  CGO_HELPERS_H_  */

