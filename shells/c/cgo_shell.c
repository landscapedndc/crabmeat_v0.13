
#include  "cgo_shell.h"
#include  "cgo_db.h"

#include  <stdlib.h>
#include  <stdio.h>

#include  <string.h>
#include  <hiredis/hiredis.h>

#include  <time.h>


int  cgo_port_init( struct cgo_port_t *  _port, char const *  _name,
                enum cgo_port_type_e  _type, enum cgo_datatype  _datatype)
{
        _port->name = cgo_strdup( _name);
        _port->type = _type;
        _port->datatype = _datatype;

        _port->dt = 1;
        _port->count = -1;
        _port->offset = 0;

        _port->bb = 0;

//        _port->rank = 0;
//        _port->sizes = NULL;

        _port->active = 1;
        _port->dir = _type;
        _port->update = NULL;
        _port->data = NULL;

        return  0;
}
void cgo_port_deinit( struct cgo_port_t *  _port)
{
        if ( _port->name)
        {
                free( _port->name);
        }
//        if ( _port->sizes)
//        {
//                free( _port->sizes);
//        }
}

struct cgo_ports_t *  cgo_ports_new(
                struct ports_static_info_t const *  _ports_info, size_t  _n_ports_info)
{
        struct cgo_ports_t *  ports = (struct cgo_ports_t *)malloc( sizeof( struct cgo_ports_t));
        if ( ports)
        {
                int  rc = cgo_ports_init( ports, _ports_info, _n_ports_info);
                if ( rc)
                {
                        free( ports);
                        ports = NULL;
                }
        }
        return  ports;
}
void  cgo_ports_destroy( struct cgo_ports_t *  _ports)
{
        if ( _ports)
        {
                cgo_ports_deinit( _ports);
                if ( _ports->ports)
                {
                        free( _ports->ports);
                }
                free( _ports);
        }
}
int  cgo_ports_init( struct cgo_ports_t *  _ports,
                struct ports_static_info_t const *  _ports_info, size_t  _n_ports_info)
{
        int  rc = 0;

        if ( !_ports_info || _n_ports_info == 0)
        {
                _ports->ports = NULL;
                _ports->n_ports = 0;
        }
        else
        {
                _ports->ports = (struct cgo_port_t *)malloc( _n_ports_info * sizeof( struct cgo_port_t));
                if ( !_ports->ports)
                {
                        rc = 1;
                }
                else
                {
                        _ports->n_ports = _n_ports_info;
                        for ( size_t  p = 0;  p < _n_ports_info;  ++p)
                        {
                                cgo_port_init( &_ports->ports[p], _ports_info[p].name,
                                                _ports_info[p].type, _ports_info[p].datatype);
                        }
                }
        }
        return  rc;
}
void  cgo_ports_deinit( struct cgo_ports_t *  _ports)
{
        (void)_ports;
}
// TODO  move to cgo_ports.c
struct cgo_port_t *  cgo_ports_find_port( struct cgo_ports_t *  _ports, char const *  _port_name)
{
        size_t  p = 0;
        while ( _ports && _ports->n_ports > p)
        {
                if ( strcmp( _ports->ports[p].name, _port_name) == 0)
                {
                        break;
                }
                ++p;
        }

        return  ( p < _ports->n_ports) ? &_ports->ports[p] : NULL;
}
int  cgo_task_register_port( struct cgo_task_t *  _task,
                char const *  _port_name, int (*_callback)( struct cgo_port_t *, void *))
{
        struct cgo_port_t *  port = cgo_ports_find_port( _task->ports, _port_name);
        if ( port)
        {
                port->update = _callback;
                port->data = _task->mem;
        }

        return  port ? 0 : -1;
}
int  cgo_task_set_port_property( struct cgo_task_t *  _task,
                char const *  _port_name, char const *  _property, void *  _value)
{
        int  rc = 0;
        struct cgo_port_t *  port = cgo_ports_find_port( _task->ports, _port_name);
        if ( port)
        {
                if ( strcmp( _property, "offset") == 0)
                {
                        port->offset = *((int*)_value);
                }
                else
                {
                        /* unknown property */
                        rc = -2;
                }
        }
        else
        {
                /* unknown port */
                rc = -1;
        }
        return  rc;
}


struct cgo_task_t *  cgo_task_new( char const *  _name)
{
    struct cgo_task_t *  task = (struct cgo_task_t *)malloc( sizeof( struct cgo_task_t));
    if ( task)
    {
        task->initialize = NULL;
        task->execute = NULL;
        task->finalize = NULL;
        task->mem = NULL;
        task->name = cgo_strdup( _name);

                task->ports = NULL;

        struct cgo_hostaddress_t *  haddr = cgo_hostaddress_new();
        cgo_hostaddress_set( haddr, "localhost", 6379);

        task->db = cgo_db_new( haddr, 1000);
        if ( task->db)
        {
            fprintf( stderr, "%s: created new task and got db handle\n", _name);
        }
        else
        {
            cgo_task_destroy( task);
            task = NULL;
        }
        cgo_hostaddress_destroy( haddr);
    }
    return  task;
}

void  cgo_task_destroy( struct cgo_task_t *  task)
{
    if ( task)
    {
        if ( task->name)
        {
            free(( void*)task->name);
        }
        if ( task->db)
        {
            cgo_db_destroy( task->db);
        }
        free((void *)task);
    }
}

int  cgo_task_publish_ports( struct cgo_task_t *  _task)
{
        int  c = 0;

        for ( size_t  p = 0;  p < _task->ports->n_ports;  ++p)
        {
                struct cgo_port_t *  port = &_task->ports->ports[p];
                if ( port)
                {
                        cgo_db_publish_port( _task->db, _task->name, port);
                        ++c;
                }
        }
        return  c;
}

static int  cgo_task_readwrite_shared( struct cgo_task_t *  _task, enum cgo_port_type_e  _inout)
{
        int  c = 0;
        for ( size_t  p = 0;  p < _task->ports->n_ports;  ++p)
        {
                struct cgo_port_t *  port = &_task->ports->ports[p];

                if ( port && ( port->type & _inout) && port->active && port->update && port->count && ( port->offset == 0))
                {
                        fprintf( stderr, "updating port \"%s\"\n", port->name);
                        port->dir = _inout;
                        int  rc_update = port->update( port, _task->db);
                        if ( rc_update)
                        {
                                fprintf( stderr, "errors occured during port update  [port=%s]\n", port->name);
                        }
                        c += 1;

                        if ( port->count > 0)
                        {
                                port->count -= 1;
                        }
                }
                else if ( port->offset > 0)
                {
                        port->offset -= 1;
                }
        }

        return  c;
}
int  cgo_task_write_shared( struct cgo_task_t *  _task)
{
        return  cgo_task_readwrite_shared( _task, PORT_OUT);
}
int  cgo_task_read_shared( struct cgo_task_t *  _task)
{
        return  cgo_task_readwrite_shared( _task, PORT_IN);
}


struct cgo_shell_t *  cgo_shell_new( struct cgo_task_t *  _task)
{
    struct cgo_shell_t *  shell = (struct cgo_shell_t *)malloc( sizeof( struct cgo_shell_t));
    if ( shell)
    {
        shell->task = _task;
    }
    return  shell;
}

void  cgo_shell_destroy( struct cgo_shell_t *  _shell)
{
    if ( _shell)
    {
        cgo_shell_disconnect( _shell);
        free(( void *)_shell);
    }
}

int  cgo_shell_connect( struct cgo_shell_t *  _shell, struct cgo_hostaddress_t *  _db_addr)
{
        (void)_db_addr;
        cgo_db_clear_client_queue( _shell->task->db, _shell->task->name);
        cgo_db_join_simulation( _shell->task->db, _shell->task->name);
    return  0;
}

void  cgo_shell_disconnect( struct cgo_shell_t *  _shell)
{
    if ( !_shell)
    {
        return;
    }
}

int  cgo_shell_execute( struct cgo_shell_t *  _shell)
{
    fprintf( stderr, "%s: starting task execution\n", _shell->task->name);
    int  rc = 0;
    int  n_msg = 0, n_exe = 0;

        while ( 1)
        {
                char *  task_cmd = NULL;
                cgo_db_wait_command( _shell->task->db, _shell->task->name, &task_cmd);
        
                fprintf( stderr, "%s: taskcmd=%s\n", _shell->task->name, task_cmd);

                int  rc_cmd = 0;
                if ( strcmp( task_cmd, "E" /*execute*/) == 0)
                {
                        if ( _shell->task->execute)
                        {
                                int  rc_read = cgo_task_read_shared( _shell->task);             /* S_1( f, t) */
                                int  rc_exec = _shell->task->execute(
                                                _shell->task, NULL, 0);                                 /* S_2( f, t) */
                                int  rc_write = cgo_task_write_shared( _shell->task);           /* O_i( f, t) */
                                rc_cmd = ( rc_read < 0 || rc_write < 0 || rc_exec) ? -1 : 0;
                        }
                        n_exe += 1;
                }
                else if ( strcmp( task_cmd, "P" /*publish ports*/) == 0)
                {
                        int  n_ports = cgo_task_publish_ports( _shell->task);
                        rc_cmd = n_ports < 0 ? -1 : 0;
                }
                else if ( strcmp( task_cmd, "I" /*initialize*/) == 0)
                {
                        if ( _shell->task->initialize)
                        {
                                fprintf( stderr, "%s: initializing...\n", _shell->task->name);
                                int  rc_read = cgo_task_read_shared( _shell->task);             /* S_1( f, t) */
                                int  rc_exec = _shell->task->initialize(
                                                _shell->task, NULL, 0);                                 /* f_init( t_0) */
                                int  rc_write = cgo_task_write_shared( _shell->task);           /* O_i( f, t_0) */
                                rc_cmd = ( rc_read < 0 || rc_write < 0 || rc_exec) ? -1 : 0;
                        }
                }
                else if ( strcmp( task_cmd, "F" /*finalize*/) == 0)
                {
                        if ( _shell->task->finalize)
                        {
                                rc_cmd = _shell->task->finalize(
                                                _shell->task, NULL, 0);
                                cgo_task_write_shared( _shell->task);           /* O_f( f, t) */
                        }
                }
        else if ( strcmp( task_cmd, "TERM") == 0)
                {
                        fprintf( stderr, "%s: driver asked us to terminate  [command=\"%s\"]\n", _shell->task->name, task_cmd);
                        free( task_cmd);
                        break;
                }
                else
                {
                        fprintf( stderr, "%s: driver command not understood  [command=\"%s\"]\n",
                                        _shell->task->name, task_cmd);
                }

                if ( rc_cmd)
                {
                        rc = -1;
                        fprintf( stderr, "%s: task failed  [stage=%s]\n",
                                        _shell->task->name, task_cmd);
                }

                char  cmd_reply[16];
                sprintf( cmd_reply, "%s_%s", task_cmd, rc ? "FAIL" : "OK");

                cgo_db_send_to_master_queue( _shell->task->db, _shell->task->name, cmd_reply);
                free( task_cmd);

        n_msg += 1;
    }

    fprintf( stdout, "%s: received %d task execute notifications [total=%d]\n",
            _shell->task->name, n_exe, n_msg);
    return  rc;
}

