
#ifndef  CGO_SHELL_H_
#define  CGO_SHELL_H_

#include  "cgo_ports.h"
#include  "cgo_helpers.h"
#include  "cgo_db.h"

#ifdef  __cplusplus
extern "C"
{
#endif

struct cgo_task_t
{
    int (*initialize)( struct cgo_task_t *, char **, int);
    int (*execute)( struct cgo_task_t *, char **, int);
    int (*finalize)( struct cgo_task_t *, char **, int);

    char *  name;
    struct cgo_ports_t *  ports;
    struct cgo_db_t *  db;

    int  dt;
    long int  t;

    void *  mem;
};

struct cgo_task_t *  cgo_task_new( char const *);
void  cgo_task_destroy( struct cgo_task_t *);

int  cgo_task_publish_ports( struct cgo_task_t *);
int  cgo_task_write_shared( struct cgo_task_t *);
int  cgo_task_read_shared( struct cgo_task_t *);

struct cgo_port_t *  cgo_ports_find_port( struct cgo_ports_t *, char const * /*port name*/);
int  cgo_task_register_port( struct cgo_task_t *, char const * /*port name*/, int (*)( struct cgo_port_t *, void *));
int  cgo_task_set_port_property( struct cgo_task_t *, char const * /*port name*/, char const * /*property*/, void * /*property value*/);

struct cgo_shell_t
{
    struct cgo_task_t *  task;
};

struct cgo_shell_t *  cgo_shell_new( struct cgo_task_t *  task);
void  cgo_shell_destroy( struct cgo_shell_t *  shell);
int  cgo_shell_connect( struct cgo_shell_t *  shell, struct cgo_hostaddress_t * /*db addr*/);
void  cgo_shell_disconnect( struct cgo_shell_t *  shell);
int  cgo_shell_execute( struct cgo_shell_t *  shell);

#ifdef  __cplusplus
}
#endif

#endif  /*  !CGO_SHELL_H_  */

