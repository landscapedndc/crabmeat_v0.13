
#include  "cgo_db.h"

#include  <stdlib.h>
#include  <stdio.h>

#include  <string.h>
#include  <hiredis/hiredis.h>

int  cgo_db_key_set( struct cgo_db_key_t *  _key, char const *  _component, int  _id, char const *  _entity)
{
    int  rc_print = snprintf( _key->key, CGO_DB_KEY_MAXSIZE,
            "%s@%d[%s:%d]%s", "cgo", 0, _component ? _component : "", _id, _entity);
    if ( rc_print < 0 || rc_print > CGO_DB_KEY_MAXSIZE)
    {
        return  -1;
    }
    _key->key[CGO_DB_KEY_MAXSIZE] = '\0';
    return  0;
}

char const *  cgo_db_key_get( struct cgo_db_key_t const *  _key)
{
    return  (char const *)_key->key;
}



void  cgo_free_db_connection( void *  _conn)
{
        redisFree(( struct redisContext *)_conn);
}


struct cgo_db_connection_pool_t *  cgo_db_connection_pool_new( struct cgo_hostaddress_t const *  _haddr, int  _maxsize)
{
        struct cgo_db_connection_pool_t *  conn =
                (struct cgo_db_connection_pool_t *)malloc( sizeof( struct cgo_db_connection_pool_t));
        if ( conn)
        {
                conn->ctxs = cgo_lifo_new();
                if ( !conn->ctxs)
                {
                        cgo_db_connection_pool_destroy( conn);
                        return  NULL;
                }
                conn->host = cgo_hostaddress_clone( _haddr);
                conn->size = 0;
                conn->maxsize = _maxsize;
        }
        return  conn;
}
void  cgo_db_connection_pool_destroy( struct cgo_db_connection_pool_t *  _cxn_pool)
{
        if ( _cxn_pool)
        {
                cgo_lifo_destroy( _cxn_pool->ctxs, cgo_free_db_connection);
                cgo_hostaddress_destroy( _cxn_pool->host);

                free( _cxn_pool);
        }
}

void *  cgo_db_connection_pool_conn_acquire( struct cgo_db_connection_pool_t *  _cxn_pool)
{
        if ( !_cxn_pool || !_cxn_pool->ctxs)
        {
                return  NULL;
        }

        if ( !cgo_lifo_empty( _cxn_pool->ctxs))
        {
                void *  cxn = cgo_lifo_pop( _cxn_pool->ctxs);
                if ( cxn)
                {
// sk:dbg                        printf( "cgo_db_connection_pool_conn_acquire(): "
// sk:dbg                                        "reusing existing db connection\n");
                        return  cxn;
                }
        }
        else if ( _cxn_pool->size < _cxn_pool->maxsize)
        {
                struct timeval  timeout = { 1, 500000 };
                struct redisContext *  ctx = redisConnectWithTimeout( _cxn_pool->host->hostname, _cxn_pool->host->port, timeout);
                if ( !ctx || ctx->err)
                {
                        fprintf( stderr, "db connection error: \"%s\"  [host=%s,port=%d]",
                                        ctx->errstr, _cxn_pool->host->hostname, _cxn_pool->host->port);
                        if ( ctx)
                        {
                                redisFree( ctx);
                        }

                        return  NULL;
                }

// sk:dbg                printf( "cgo_db_connection_pool_conn_acquire(): "
// sk:dbg                                "created new db connection\n");
                _cxn_pool->size += 1;
                return  ctx;
        }

        return  NULL;
}
int  cgo_db_connection_pool_conn_release( struct cgo_db_connection_pool_t *  _cxn_pool, void *  _cxn)
{
        if ( !_cxn_pool || !_cxn)
        {
                return  -1;
        }

        if ( _cxn_pool->size >= _cxn_pool->maxsize)
        {
                redisFree( _cxn);
                return  0;
        }

        int  rc_push = cgo_lifo_push( _cxn_pool->ctxs, _cxn);
        if ( rc_push)
        {
                redisFree( _cxn);
                return  -1;
        }

// sk:dbg        printf( "cgo_db_connection_pool_conn_release(): "
// sk:dbg                        "returning no longer needed db connection\n");
        return  0;
}



struct cgo_db_t *  cgo_db_new( struct cgo_hostaddress_t const *  _haddr, int  _maxsize)
{
    struct cgo_db_t *  ds =
        (struct cgo_db_t *)malloc( sizeof( struct cgo_db_t));
    if ( ds)
    {
        ds->cxns = cgo_db_connection_pool_new( _haddr, _maxsize);
    }
    return  ds;
}
void  cgo_db_destroy( struct cgo_db_t *  _db)
{
    if ( _db)
    {
        if ( _db->cxns)
        {
            cgo_db_connection_pool_destroy( _db->cxns);
        }
        free(( void *)_db);
    }
}


#define  s_redis_connection_acquire(__db__,__ctx__)            \
void *  __ctx__ = cgo_db_connection_pool_conn_acquire( __db__->cxns);    \
if ( !__ctx__)                                \
{                                    \
        return  cgo_db_readwrite_return( __db__, __ctx__, CGO_EFAIL);    \
}
#define  s_redis_connection_release(__db__,__ctx__,__status__)        \
cgo_db_readwrite_return(__db__,__ctx__,__status__);

#define  s_redis_handle_reply(__reply_ptr__,__reply_arg__)        \
if ( !__reply_ptr__ || __reply_ptr__->type == REDIS_REPLY_ERROR)    \
{                                    \
    if ( __reply_ptr__)                        \
        {                                \
                fprintf( stderr, "redis error: %s\n", __reply_ptr__->str);\
        freeReplyObject( __reply_ptr__);            \
        }                                \
        return  CGO_EFAIL;                        \
}                                    \
if ( __reply_arg__)                            \
{                                    \
        *__reply_arg__ = __reply_ptr__;                    \
}                                    \
else                                    \
{                                    \
        freeReplyObject( __reply_ptr__);                \
}                                    \
(void)_redis_reply/*;*/

static  int  cgo_db_readwrite_return( struct cgo_db_t *  _db, void *  _ctx, int  _rc)
{
        if ( _db && _ctx)
        {
                cgo_db_connection_pool_conn_release( _db->cxns, _ctx);
        }
        return  _rc;
}

static int  s_redis_command_stream( void *  _redis_context, char const * _redis_cmd, char const *  _key, unsigned long int  _geoindex,
                char **  _bin_data, size_t *  _bin_data_size, struct redisReply **  _redis_reply)
{
        redisReply *  reply = NULL;
        if ( _bin_data)
        {
                reply = (struct redisReply *)redisCommand((struct redisContext *)_redis_context,
                                "%s %s %lu %b", _redis_cmd, _key, _geoindex, *_bin_data, *_bin_data_size);
        }
        else
        {
                reply = (struct redisReply *)redisCommand((struct redisContext *)_redis_context,
                                "%s %s %lu", _redis_cmd, _key, _geoindex);
        }
    s_redis_handle_reply(reply,_redis_reply);
        return  CGO_EOK;
}
static int  s_redis_command_read_stream( void *  _redis_context, char const *  _key, unsigned long int  _geoindex,
                struct redisReply **  _redis_reply)
{
        return  s_redis_command_stream( _redis_context, "hget", _key, _geoindex, NULL, 0, _redis_reply);
}

static int  s_redis_command_write_stream( void *  _redis_context, char const *  _key, unsigned long int  _geoindex,
                char **  _bin_data, size_t *  _bin_data_size, struct redisReply **  _redis_reply)
{
        return  s_redis_command_stream( _redis_context, "hset", _key, _geoindex, _bin_data, _bin_data_size, _redis_reply);
}

static int  s_redis_command( void *  _redis_context, char const *  _redis_command, struct redisReply **  _redis_reply)
{
        redisReply *  reply = (struct redisReply *)redisCommand((struct redisContext *)_redis_context, _redis_command);
        if ( !reply || reply->type == REDIS_REPLY_ERROR)
        {
                if ( reply)
                {
            freeReplyObject( reply);
                }
                return  -1;
        }
        if ( _redis_reply)
        {
                *_redis_reply = reply;
        }
        else
        {
                freeReplyObject( reply);
        }
        return  0;
}

static int  s_redis_queue_delete( void *  _redis_context,
        char const *  _queue, redisReply **  _redis_reply)
{
    redisReply *  reply = NULL;
    reply = (struct redisReply *)redisCommand((struct redisContext *)_redis_context,
            "del @queue.%s", _queue);
    s_redis_handle_reply( reply, _redis_reply);
        return  CGO_EOK;
}

static int  s_redis_queue_send( void *  _redis_context,
        char const *  _queue, char const *  _client_name, char const *  _message, redisReply **  _redis_reply)
{
    char  queue_msg_buf[64];
    char const *  queue_msg = _message;
    if ( _client_name)
    {
        snprintf( queue_msg_buf, 63, "%s:%s", _client_name, _message);
        queue_msg_buf[63] = '\0';
        queue_msg = queue_msg_buf;
    }

    redisReply *  reply = NULL;
    reply = (struct redisReply *)redisCommand((struct redisContext *)_redis_context,
            "rpush @queue.%s %s", _queue, queue_msg);
    s_redis_handle_reply( reply, _redis_reply);
        return  CGO_EOK;
}


int cgo_db_publish_port( struct cgo_db_t *  _db,
        char const *  _task_name, struct cgo_port_t const *  _port)
{
    s_redis_connection_acquire( _db, ctx);

        int  rc = 0;
        char  port_s[128];
        if ( _port->type & PORT_IN)
        {
                sprintf( port_s, "sadd @ports.in.%s %s", _task_name, _port->name);
                rc = s_redis_command( ctx, port_s, NULL);
                if ( !rc && _port->offset > 0)
                {
                        sprintf( port_s, "sadd @ports.in.%s.provide_initial_value %s", _task_name, _port->name);
                        rc = s_redis_command( ctx, port_s, NULL);
                }
        }
        if ( !rc && ( _port->type & PORT_OUT))
        {
                sprintf( port_s, "sadd @ports.out.%s %s", _task_name, _port->name);
                rc = s_redis_command( ctx, port_s, NULL);
        }

    return  s_redis_connection_release( _db, ctx, (rc ? CGO_EFAIL : CGO_EOK));
}

int  cgo_db_send_to_master_queue( struct cgo_db_t *  _db, char const * _client_name, char const * _message)
{
    s_redis_connection_acquire( _db, ctx);
    int  rc_send = s_redis_queue_send( ctx, "MASTER", _client_name, _message, NULL);
    return  s_redis_connection_release( _db, ctx, rc_send);
}


int  cgo_db_join_simulation( struct cgo_db_t *  _db, char const *  _client_name)
{
    return  cgo_db_send_to_master_queue( _db, _client_name, "JOIN");
}

int  cgo_db_clear_client_queue( struct cgo_db_t *  _db, char const *  _client_name)
{
    fprintf( stderr, "clearing client queue  [client=%s]\n", _client_name);

    s_redis_connection_acquire( _db, ctx);
    int  rc_delete = s_redis_queue_delete( ctx, _client_name, NULL);
    return  s_redis_connection_release( _db, ctx, rc_delete);
}

int  cgo_db_send_to_client_queue( struct cgo_db_t *  _db, char const *  _client_name, char const *  _message)
{
    fprintf( stderr, "%s: client queue appended\n", _message);

    s_redis_connection_acquire( _db, ctx);
    int  rc_send = s_redis_queue_send( ctx, _client_name, NULL, _message, NULL);
    return  s_redis_connection_release( _db, ctx, rc_send);
}

int  cgo_db_wait_command( struct cgo_db_t *  _db, char const *  _client_name, char ** _cmd)
{
    fprintf( stderr, "%s: waiting for command\n", _client_name);

    s_redis_connection_acquire( _db, ctx);

    redisReply *  reply = NULL;
    reply = (struct redisReply *)redisCommand((struct redisContext *)ctx,
            "blpop @queue.%s 0", _client_name);
        if ( !reply || reply->type == REDIS_REPLY_ERROR)
        {
                if ( reply)
                {
            freeReplyObject( reply);
                }
        return  s_redis_connection_release( _db, ctx, CGO_EFAIL);
        }
    if ( reply->type == REDIS_REPLY_ARRAY)
    {
        if ( reply->elements == 2)
        {
            *_cmd = cgo_strdup( reply->element[1]->str);
            freeReplyObject( reply);
        }
        }
    return  s_redis_connection_release( _db, ctx, CGO_EOK);
}



#include  "msgpack.h"
static int  unpack_array_double( char *  _buf, size_t  _size, double *  _array, int  _rank, int *  _lengths)
{
        if ( !_buf || _size == 0 || !_array || !_lengths || _rank == 0)
        {
                return  CGO_EFAIL;
        }

        int  flat_length = _lengths[0];
        for ( int  r = 1;  r < _rank;  ++r)
        {
                flat_length *= _lengths[r];
        }

        int  rc = CGO_EOK;

        msgpack_unpacked  msg;
        msgpack_unpacked_init( &msg);

        if ( msgpack_unpack_next( &msg, _buf, _size, NULL))
        {
                msgpack_object  root = msg.data;
                if ( root.type == MSGPACK_OBJECT_ARRAY)
                {
                        int  length = root.via.array.size;
                        //fprintf( stderr, "[DD]  array size = %d (%d)\n", length,  flat_length);
                        if ( length != flat_length)
                        {
                                fprintf( stderr, "[EE]  array sizes do not match  [found=%d,requested=%d]\n", length,  flat_length);
                                rc = CGO_EFAIL;
                        }
                        else for ( int  l = 0; l < length;  ++l)
                        {
                                _array[l] = root.via.array.ptr[l].via.dec;
                        }
                }
        }

        msgpack_unpacked_destroy( &msg);

        return  rc;
}

int  cgo_db_read_array( struct cgo_db_t *  _db, struct cgo_geospatial_key_t *  _geoindex,
                void *  _data, int  _rank, int *  _lengths, cgo_datatype_e  _datatype)
{
        int  rc = CGO_EOK;
        void *  ctx = cgo_db_connection_pool_conn_acquire( _db->cxns);
        if ( !ctx)
        {
                return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }

        redisReply *  redis_reply = NULL;
        rc = s_redis_command_read_stream( ctx, _geoindex->portname, _geoindex->geoindex, &redis_reply);
        if ( !redis_reply || redis_reply->type == REDIS_REPLY_ERROR)
        {
                fprintf( stderr, "[EE]  failed to receive message\n");
                if ( redis_reply)
                {
                        freeReplyObject( redis_reply);
                }
                return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }

        switch ( _datatype)
        {
                case CGO_DOUBLE:
                {
                        rc = unpack_array_double( redis_reply->str, redis_reply->len, (double*)_data, _rank, _lengths);
                        break;
                }
                default:
                        return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }

        freeReplyObject( redis_reply);

        return  cgo_db_readwrite_return( _db, ctx, rc);
}
int  cgo_db_read_vector( struct cgo_db_t *  _db,
                struct cgo_geospatial_key_t *  _geoindex, void *  _data, int *  _length, cgo_datatype_e  _datatype)
{
        return  cgo_db_read_array( _db, _geoindex, _data, 1, _length, _datatype);
}

static int  pack_array_double( char **  _buf, size_t *  _size, double const *  _array, int  _rank, int *  _lengths)
{
        if ( !_buf || !_size || !_array || !_lengths || _rank == 0)
        {
                return  CGO_EFAIL;
        }

        int  flat_length = _lengths[0];
        for ( int  r = 1;  r < _rank;  ++r)
        {
                flat_length *= _lengths[r];
        }

        msgpack_sbuffer  sbuf;
        msgpack_sbuffer_init( &sbuf);

        msgpack_packer  pck;
        msgpack_packer_init( &pck, &sbuf, msgpack_sbuffer_write);
// TODO  write rank and lengths
        msgpack_pack_array( &pck, flat_length);
        double const *  array = _array;
        for ( int  l = 0;  l < flat_length;  ++l, ++array)
        {
                msgpack_pack_double( &pck, *array);
        }

        *_size = sbuf.size;
        *_buf = malloc( sbuf.size);
        memcpy( *_buf, sbuf.data, sbuf.size);
        msgpack_sbuffer_destroy( &sbuf);

        return  CGO_EOK;
}
static void  free_array_buffer( char *  _buf)
{
        if ( _buf)
        {
                free( _buf);
        }
}

int  cgo_db_write_array( struct cgo_db_t *  _db, struct cgo_geospatial_key_t *  _geoindex,
                void *  _data, int  _rank, int *  _lengths, cgo_datatype_e  _datatype)
{
        int  rc = CGO_EOK;
        void *  ctx = cgo_db_connection_pool_conn_acquire( _db->cxns);
        if ( !ctx)
        {
                return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }

        char *  buf = NULL;
        size_t  buf_size = 0;
        switch ( _datatype)
        {
                case CGO_DOUBLE:
                {
                        rc = pack_array_double( &buf, &buf_size, (double*)_data, _rank, _lengths);
                        break;
                }
                default:
                        return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }
        if ( !buf)
        {
                fprintf( stderr, "[EE]  failed to pack message\n");
                return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }
        if ( rc)
        {
                fprintf( stderr, "[EE]  failed to pack message\n");
                free( buf);
                return  cgo_db_readwrite_return( _db, ctx, CGO_EFAIL);
        }

//        printf( "buffer size = %d (%d)\n", buf_size, *_length);

        rc = s_redis_command_write_stream( ctx, _geoindex->portname, _geoindex->geoindex, &buf, &buf_size, NULL);
        free_array_buffer( buf);

        return  cgo_db_readwrite_return( _db, ctx, rc);
}
int  cgo_db_write_vector( struct cgo_db_t *  _db,
                struct cgo_geospatial_key_t *  _geoindex, void *  _data, int *  _length, cgo_datatype_e  _datatype)
{
        return  cgo_db_write_array( _db, _geoindex, _data, 1, _length, _datatype);
}

