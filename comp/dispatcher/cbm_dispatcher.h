/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#ifndef  CBM_DISPATCHER_H_
#define  CBM_DISPATCHER_H_

#include  "crabmeat-common.h"
#include  "cbm_object.h"

#include  "mpi/cbm_mpi.h"

namespace cbm {
class  io_dcomm_t;
class CBM_API work_dispatcher_t : public cbm::object_t
{
    public:
        virtual  char const *  dispatch_strategy_name() const = 0;
        virtual  work_dispatcher_t *  create( MPI_Comm const & /*mpi communicator*/,
                lid_t const & /*object id*/) const = 0;

    public:
        /*!
         * @brief
         *
         * @param
         *    number of processing units in group
         */
        work_dispatcher_t();
        work_dispatcher_t( MPI_Comm const & /*mpi communicator*/,
                lid_t const &  /*object id*/);

        virtual ~work_dispatcher_t() = 0;

        virtual lerr_t  initialize( cbm::io_dcomm_t *)
            { return  LDNDC_ERR_OK; }

        /*!
         * @brief
         *    total number of nodes
         */
        virtual int size() const;
        /*!
         * @brief
         *    node rank this work dispatcher serves
         */
        virtual int rank() const;


        /*!
         * @brief
         *    return true if work dispatcher strategy
         *    yields "take kernel" for given kernel ID.
         *    return false otherwise.
         *
         * @note
         *    kernel ID possibly not used (e.g. round
         *    robin stategy)
         */
        virtual bool acquire_kernel( lid_t const &) = 0;

    protected:
        MPI_Comm  m_mpicomm;

        int  m_size;
        int  m_rank;

    private:
        /* hide these guys */
        work_dispatcher_t( work_dispatcher_t const &);
        work_dispatcher_t &  operator=( work_dispatcher_t const &);
};
}  /*  namespace cbm  */

#endif  /*  !CBM_DISPATCHER_H_  */

