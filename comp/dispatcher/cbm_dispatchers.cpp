/*!
 * @brief
 *    work dispatcher factory
 *
 * @author
 *    steffen klatt (created on: apr 20, 2016)
 */

#include  "dispatcher/cbm_dispatchers.h"

#include  "dispatcher/noop.h"
namespace { 
    cbm::work_dispatcher_noop_t const  work_dispatcher_noop_factory; }
#include  "dispatcher/roundrobin.h"
namespace { 
    cbm::work_dispatcher_roundrobin_t const  work_dispatcher_roundrobin_factory; }
#if defined(CRABMEAT_METIS) && defined(CRABMEAT_MPI)
#  include  "dispatcher/metis.h"
namespace { 
    cbm::work_dispatcher_metis_t const  work_dispatcher_metis_factory; }
#endif
cbm::work_dispatcher_t const *  work_dispatchers[] =
{
    &work_dispatcher_noop_factory,
    &work_dispatcher_roundrobin_factory,
#if defined(CRABMEAT_METIS) && defined(CRABMEAT_MPI)
    &work_dispatcher_metis_factory,
#endif

    NULL /*sentinel*/
};

#include  <stdio.h>
void
cbm::work_dispatcher_factory_t::list_available()
{
    size_t  w = 0;
    while ( work_dispatchers[w])
    {
        fprintf( stdout, "%s\n",
            work_dispatchers[w]->dispatch_strategy_name());
        ++w;
    }
}

#include  "string/cbm_string.h"
cbm::work_dispatcher_t *
cbm::work_dispatcher_factory_t::new_dispatcher( char const *  _name,
    MPI_Comm const &  _mpicomm, lid_t const &  _id)
{
    size_t  w = 0;
    while ( work_dispatchers[w])
    {
        if ( cbm::is_equal( _name,
            work_dispatchers[w]->dispatch_strategy_name()))
        {
            return  work_dispatchers[w]->create( _mpicomm, _id);
        }
        ++w;
    }
    /* no such work dispatcher */
    return  NULL;
}

void
cbm::work_dispatcher_factory_t::free_dispatcher(
    work_dispatcher_t *  _dispatcher)
{
    if ( _dispatcher)
        { delete _dispatcher; }
}

