/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#ifndef  CBM_DISPATCHERS_H_
#define  CBM_DISPATCHERS_H_

#include  "crabmeat-common.h"

#include  "mpi/cbm_mpi.h"

namespace cbm
{
class  work_dispatcher_t;
struct CBM_API work_dispatcher_factory_t
{
    static void  list_available();
    static work_dispatcher_t *  new_dispatcher( char const * /*name*/,
        MPI_Comm const & /*mpi communicator*/, lid_t const & /*object id*/);
    static void  free_dispatcher(
        work_dispatcher_t * /*dispatcher*/);
};
} /* namespace cbm */


#endif  /*  !CBM_DISPATCHERS_H_  */

