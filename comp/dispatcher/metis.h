/*!
 * @brief
 *    work dispatcher based on graph partitioner Metis
 *
 * @author
 *    martin wlotzka,
 *    benjamin foerster
 */

#ifndef  CBM_DISPATCHER_METIS_H_
#define  CBM_DISPATCHER_METIS_H_

#include  "dispatcher/cbm_dispatcher.h"

#include  <metis.h>

#include  <set>
#include  <vector>

namespace cbm {
class  io_dcomm_t; }
class CBM_API work_dispatcher_metis_t : public work_dispatcher_t
{
    LDNDC_OBJECT(work_dispatcher_metis_t)
    public:
        char const *  dispatch_strategy_name() const
            { return  dispatch_strategy_name_(); }
    private:
        static char const *  dispatch_strategy_name_()
            { return  "metis"; }
    public:
        work_dispatcher_t *  create( MPI_Comm const &  _mpicomm,
                lid_t const &  _id) const
            { return  new work_dispatcher_metis_t( _mpicomm, _id); }

    public:
        work_dispatcher_metis_t();
        work_dispatcher_metis_t( MPI_Comm const &, lid_t const &);

        ~work_dispatcher_metis_t();

        lerr_t  initialize( cbm::io_dcomm_t *);

        bool  acquire_kernel( lid_t const &);

    private:
        std::set< lid_t >  m_cells;

        struct cellid_t
        {
            cellid_t() : uid( 0) { }
            cellid_t( int _uid) : uid( _uid) { }

            int uid;
        };
        struct cell_conn_t
        {
            cell_conn_t() : uid1( 0), uid2( 0) { }
            cell_conn_t( int _uid1, int _uid2)
                : uid1( _uid1), uid2( _uid2) { }

            int uid1, uid2;
        };

        lerr_t  m_initialize( cbm::io_dcomm_t *);

        lerr_t  m_construct_cellgraph( cbm::io_dcomm_t *,
            std::vector< cellid_t > * /*cells*/,
            std::vector< cell_conn_t > * /*connections*/);
        lerr_t  m_compute_partitioning(
            size_t /*#cells*/, cellid_t const * /*cells*/,
            size_t /*#connections*/, cell_conn_t const * /*connections*/,
            idx_t * /*partitioning*/);
        lerr_t  m_bcast_partitioning(
            size_t /*#cells*/, idx_t * /*cell partitioning*/);
        lerr_t  m_assign_local_cells(
            size_t /*#cells*/, std::vector< cellid_t > * /*cells*/,
            idx_t * /*cell partitioning*/);
};
} /* namespace cbm */

#endif  /*  !CBM_DISPATCHER_METIS_H_  */

