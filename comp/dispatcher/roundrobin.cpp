/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#include  "dispatcher/roundrobin.h"
#include  "log/cbm_baselog.h"

LDNDC_OBJECT_DEFN(cbm::work_dispatcher_roundrobin_t)
cbm::work_dispatcher_roundrobin_t::work_dispatcher_roundrobin_t()
        : cbm::work_dispatcher_t(),
          p_( 0), q_( -1)
    { }
cbm::work_dispatcher_roundrobin_t::work_dispatcher_roundrobin_t(
        MPI_Comm const &  _mpicomm, lid_t const &  _id)
        : cbm::work_dispatcher_t( _mpicomm, _id),
          p_( 0), q_( -1)
{
    this->q_ = this->rank();
    this->p_ = 0;
}

cbm::work_dispatcher_roundrobin_t::~work_dispatcher_roundrobin_t()
    { }

bool
cbm::work_dispatcher_roundrobin_t::acquire_kernel(
        lid_t const &)
{
    CBM_Assert( this->rank() >= 0);

    bool  acquire( false);
    /* dispatch strategy */
    if ( this->p_ == this->q_)
    {
        q_ += this->size();
        acquire = true;
    }
    ++this->p_;

    return  acquire;
}

