/*!
 * @brief
 *    work dispatcher based on graph partitioner Metis
 *
 * @author
 *    martin wlotzka,
 *    benjamin foerster,
 *    steffen klatt
 */

#include  "dispatcher/metis.h"
#include  "log/cbm_baselog.h"
#include  "mpi/cbm_mpi.h"

cbm::work_dispatcher_metis_t::work_dispatcher_metis_t()
    : cbm::work_dispatcher_t()
    { }
cbm::work_dispatcher_metis_t::work_dispatcher_metis_t(
        MPI_Comm const &  _mpicomm, lid_t const &  _lid)
    : cbm::work_dispatcher_t( _mpicomm, _lid)
    { }

cbm::work_dispatcher_metis_t::~work_dispatcher_metis_t()
    { }

lerr_t
cbm::work_dispatcher_metis_t::initialize( cbm::io_dcomm_t *  _io_dcomm)
{
    lerr_t  rc;
    rc = this->m_initialize( _io_dcomm);
    return  rc;
}

bool
cbm::work_dispatcher_metis_t::acquire_kernel( lid_t const &  _id)
{
    return  this->m_cells.count( _id) == 1;
}


#include  <vector>
lerr_t
cbm::work_dispatcher_metis_t::m_initialize(
        cbm::io_dcomm_t *  _io_dcomm)
{
    lerr_t  rc_init = LDNDC_ERR_OK;

    std::vector< cellid_t >  cells;
    std::vector< cell_conn_t >  connections;

    this->m_cells.clear();

    /* retrieve cells and neighbors */
    rc_init = this->m_construct_cellgraph(
            _io_dcomm, &cells, &connections);
    if ( rc_init)
        { return  LDNDC_ERR_FAIL; }
    if ( cells.size() == 0)
        { return  LDNDC_ERR_OK; }

    idx_t *  metis_partitioning = new idx_t[cells.size()];
    if ( !metis_partitioning)
    {
        return  LDNDC_ERR_NOMEM;
    }

    /* find partitioning */
    rc_init = this->m_compute_partitioning( cells.size(), &(cells[0]),
            connections.size(), &(connections[0]), metis_partitioning);
    if ( rc_init == LDNDC_ERR_OK)
    {
        rc_init = this->m_bcast_partitioning(
                    cells.size(), metis_partitioning);
        if ( rc_init == LDNDC_ERR_OK)
        {
            rc_init = this->m_assign_local_cells( cells.size(),
                &cells, metis_partitioning);
        }
    }
    delete[] metis_partitioning;


    return  rc_init;
}

#include  "setup/setup.h"
#include  "domain/io-dcomm.h"
lerr_t
cbm::work_dispatcher_metis_t::m_construct_cellgraph(
    cbm::io_dcomm_t *  _io_dcomm, std::vector< cellid_t > *  _cells,
    std::vector< cell_conn_t > *  _conn)
{
    std::vector< cbm::source_descriptor_t >
            full_domain( _io_dcomm->request_region( NULL));
    size_t const  nb_kernels = full_domain.size();
    for ( size_t  p = 0;  p < nb_kernels;  ++p)
    {
        setup::input_class_setup_t *  setup_in = static_cast< setup::input_class_setup_t * >(
            _io_dcomm->new_input_class( "setup", &(full_domain[p])));
        if ( !setup_in)
        {
            CBM_LogError( "Failed to retrieve input \"setup\"",
                " [identifier=", full_domain[p],"]");
            return  LDNDC_ERR_FAIL;
        }

        int  uid = setup_in->object_id();
        for ( size_t  c = 0;  c < _cells->size(); ++c)
        {
            if ( _cells->at( c).uid == uid)
            {
                CBM_LogError( "duplicate kernel ID;",
                    " this is currently not allowed  [id=",uid,"]");
        	_io_dcomm->delete_input_class( setup_in);
                return  LDNDC_ERR_RUNTIME_ERROR;
            }
        }

        if ( setup_in->dispatch_on_any())
        {
            this->m_cells.insert( uid);
        }
        else
        {
            _cells->push_back( cellid_t( uid));
        }

        size_t const  nb_neighbors = setup_in->get_number_of_neighbors();
        setup::neighbor_t const *  cell_neighbors = setup_in->get_neighbors();
        for ( size_t  n = 0;  n < nb_neighbors;  ++n)
        {
            int const  uid_neighbor = cell_neighbors[n].desc.block_id;
            _conn->push_back( cell_conn_t( uid, uid_neighbor));
        }

        _io_dcomm->delete_input_class( setup_in);
    }

    return  LDNDC_ERR_OK;
}


/* The original version of this function was written by Martin
 * Wlotzka and Benjamin Foerster and is part of MPI CMF, which
 * is based on CMF by Philipp Kraft.
 */

lerr_t
cbm::work_dispatcher_metis_t::m_compute_partitioning(
    size_t _nb_cells, cellid_t const * _cells,
    size_t _nb_connections, cell_conn_t const * _connections,
    idx_t *  _metis_partitioning)
{
    /* nothing to do */
    if ( _nb_cells == 0)
        { return LDNDC_ERR_OK; }
    if ( _cells == NULL)
        { return LDNDC_ERR_FAIL; }
    if (( _nb_connections > 0) && ( _connections == NULL))
        { return LDNDC_ERR_FAIL; }

    size_t  nb_global_cells = 0;

    std::map< int, size_t >  user_2_metis;

    for ( size_t  i = 0;  i < _nb_cells;  ++i)
    {
        // check for duplicate user-IDs
        for ( size_t  j = 0;  j < i;  ++j)
        {
            if ( _cells[i].uid == _cells[j].uid)
            {
                CBM_LogError( "Duplicate cell user-ID  [id=",_cells[i].uid,"]");
                return  LDNDC_ERR_FAIL;
            }
        }
        user_2_metis[_cells[i].uid] = i;
        ++nb_global_cells;
    }
    crabmeat_assert( nb_global_cells == _nb_cells);


    // for each cell: set of connected cells
    // this is used to build the adjacency list for metis
    std::vector< std::set< int > >  conn( _nb_cells);

    // fill the connection sets
    for ( size_t  i = 0;  i < _nb_connections;  ++i)
    {
        int id1 = _connections[i].uid1;
        int id2 = _connections[i].uid2;
        if (id1 == id2)
        {
            CBM_LogError( "Cell user-ID ", id1, " is connected to itself");
            return  LDNDC_ERR_FAIL;
        }

        bool const  id1_found = (user_2_metis.count(id1) == 1);
        bool const  id2_found = (user_2_metis.count(id2) == 1);

        if (!id1_found)
        {
            CBM_LogError( "Connection defined between cell user-IDs ",
                id1, " and ", id2, ", but ", id1, " was not found in the cells list");
            return  LDNDC_ERR_FAIL;
        }
        if (!id2_found)
        {
            CBM_LogError( "Connection defined between cell user-IDs ",
                id1, " and ", id2, ", but ", id2, " was not found in the cells list");
            return  LDNDC_ERR_FAIL;
        }

        int const  metis_id1 = user_2_metis[id1];
        int const  metis_id2 = user_2_metis[id2];

        // store connections in both directions as required for metis
        conn[metis_id1].insert(metis_id2);
        conn[metis_id2].insert(metis_id1);
    }

    // rank 0 only
    if ( this->m_rank == 0)
    {
        lerr_t  rc_part = LDNDC_ERR_OK;

        if ( this->m_size == 1)
        {
            // manual "partitioning" in case of only one process to prevent METIS error
            for ( size_t  i = 0;  i < _nb_cells;  ++i)
                { _metis_partitioning[i] = 0; }
        }
        else
        {
            size_t nb_conn = 0;

            // count the total number of connections
            for ( size_t  i = 0;  i < _nb_cells;  ++i)
                { nb_conn += conn[i].size(); }

            // prepare graph structure for METIS
            idx_t * metis_xadj = new idx_t[_nb_cells + 1];
            metis_xadj[0] = 0;
            idx_t * metis_adjncy = new idx_t[nb_conn];

            for ( size_t i = 0;  i < _nb_cells;  ++i)
            {
                int  j = 0;
                metis_xadj[i + 1] = metis_xadj[i] + conn[i].size();
                for ( std::set< int >::const_iterator  it = conn[i].begin();
                        it != conn[i].end();  ++it)
                {
                    metis_adjncy[metis_xadj[i] + j] = *it;
                    ++j;
                }
            }

            // let METIS compute the partitioning
            idx_t  metis_nb_constraints = 1;
            idx_t  metis_options[METIS_NOPTIONS];
            METIS_SetDefaultOptions( metis_options);
            metis_options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_VOL; // minimize communication volume
            idx_t  metis_nb_proc = this->m_size;
            idx_t  metis_nb_global_cells = _nb_cells;
            idx_t  metis_objective_value = 0;

            int const  metis_result = METIS_PartGraphKway(
                    &metis_nb_global_cells, // nb. of vertices
                    &metis_nb_constraints,  // nb. of constraints
                    metis_xadj,             // vertex offset
                    metis_adjncy,           // adjacencies
                    NULL,                   // vertex weights
                    NULL,                   // vertex size
                    NULL,                   // weight of adjacencies
                    &metis_nb_proc,         // nb. of parts
                    NULL,                   // target partition weight
                    NULL,                   // imbalance tolerance
                    metis_options,          // options
                    &metis_objective_value, // quality of the partitioning
                    _metis_partitioning      // partition vector
                    );

            // error handling
            if( metis_result != METIS_OK)
            {
                rc_part = LDNDC_ERR_FAIL;

                switch( metis_result){
                    case METIS_ERROR_INPUT:
                        CBM_LogError( "METIS Error: input error");
                        break;

                    case METIS_ERROR_MEMORY:
                        CBM_LogError( "METIS Error: memory error");
                        break;

                    case METIS_ERROR:
                    default:
                        CBM_LogError( "METIS Error: unspecified error");
                        break;
                }
            }

            delete[] metis_xadj;
            delete[] metis_adjncy;
        }
        if ( rc_part)
            { return  rc_part; }
    }  /* END if ( m_rank == 0) */

    return  LDNDC_ERR_OK;
}

lerr_t
cbm::work_dispatcher_metis_t::m_bcast_partitioning(
        size_t _nb_cells, idx_t *  _metis_partitioning)
{
    /* broadcast partitioning vector to all procs */
    int  rc_bcast = MPI_Bcast( _metis_partitioning,
                _nb_cells, MPI_INT, 0, this->m_mpicomm);
    return  rc_bcast ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

lerr_t
cbm::work_dispatcher_metis_t::m_assign_local_cells(
        size_t _nb_cells, std::vector< cellid_t > *  _cells,
            idx_t *  _metis_partitioning)
{
    if ( _nb_cells && ( !_cells || !_metis_partitioning))
        { return  LDNDC_ERR_FAIL; }

    /* collect cells for rank */
    for ( size_t  k = 0;  k < _nb_cells;  ++k)
    {
        if ( _metis_partitioning[k] == this->m_rank)
        {
            this->m_cells.insert(( *_cells)[k].uid);
        }
    }
    return  LDNDC_ERR_OK;
}

