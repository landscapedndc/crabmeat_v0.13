/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#include  "dispatcher/cbm_dispatcher.h"

cbm::work_dispatcher_t::work_dispatcher_t()
        : cbm::object_t(),
          m_size( -1 /*invalidate*/),
          m_rank( -1 /*invalidate*/)
    { }
cbm::work_dispatcher_t::work_dispatcher_t(
        MPI_Comm const &  _mpicomm, lid_t const &  _id)
        : cbm::object_t( _id),
          m_mpicomm( _mpicomm),
          m_size( -1 /*invalidate*/),
          m_rank( -1 /*invalidate*/)
{
    this->m_rank = mpi::comm_rank( this->m_mpicomm);
    this->m_size = mpi::comm_size( this->m_mpicomm);
}

cbm::work_dispatcher_t::~work_dispatcher_t()
    { }

int cbm::work_dispatcher_t::size() const
    { return  this->m_size; }

int cbm::work_dispatcher_t::rank() const
    { return  this->m_rank; }

