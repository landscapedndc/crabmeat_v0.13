/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#include  "dispatcher/noop.h"

LDNDC_OBJECT_DEFN(cbm::work_dispatcher_noop_t)
cbm::work_dispatcher_noop_t::work_dispatcher_noop_t()
        : cbm::work_dispatcher_t()
    { }
cbm::work_dispatcher_noop_t::work_dispatcher_noop_t(
        MPI_Comm const &  _mpicomm, lid_t const &  _id)
        : cbm::work_dispatcher_t( _mpicomm, _id)
    { }

cbm::work_dispatcher_noop_t::~work_dispatcher_noop_t()
    { }

bool
cbm::work_dispatcher_noop_t::acquire_kernel(
        lid_t const &)
{
    return  true;
}

