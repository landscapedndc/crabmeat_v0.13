/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 *    the "round robin" work dispatcher assigns every p-th
 *    kernel K_i to region R_j, where j is the number of regions,
 *    i.e. j = 0,...,p-1. note the i is not the kernel ID but the
 *    index in the "list" they are processed.
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#ifndef  CBM_DISPATCHER_ROUNDROBIN_H_
#define  CBM_DISPATCHER_ROUNDROBIN_H_

#include  "dispatcher/cbm_dispatcher.h"

namespace cbm
{
class CBM_API work_dispatcher_roundrobin_t : public work_dispatcher_t
{
    LDNDC_OBJECT(work_dispatcher_roundrobin_t)
    public:
        char const *  dispatch_strategy_name() const
            { return  dispatch_strategy_name_(); }
        work_dispatcher_t *  create( MPI_Comm const &  _mpicomm,
                lid_t const &  _id) const
            { return  new work_dispatcher_roundrobin_t( _mpicomm, _id); }

    private:
        static char const *  dispatch_strategy_name_()
        {
            return  "roundrobin";
        }

    public:
        work_dispatcher_roundrobin_t();
        work_dispatcher_roundrobin_t(
            MPI_Comm const & /*mpi communicator*/,
                lid_t const & /*object id*/);

        ~work_dispatcher_roundrobin_t();

        /*!
         * @brief
         *
         * @note
         *      this strategy relies on the fact that
         *      all nodes 0, ..., n are worker nodes
         */
        bool  acquire_kernel( lid_t const &);

    private:
        int  p_, q_;
};
} /* namespace cbm */

#endif  /*  !CBM_DISPATCHER_ROUNDROBIN_H_  */

