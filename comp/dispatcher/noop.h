/*!
 * @brief
 *    work dispatcher task is to distribute the total of all
 *    single core simulations to possibly multiple regional
 *    simulations. in other words, it provides a dispatch
 *    strategy for cores to processing units.
 *
 *    the "noop" (no operation) work dispatcher simply returns
 *    true, i.e. it serves a single region)
 *
 * @author
 *    steffen klatt (created on: oct 01, 2011)
 */

#ifndef  CBM_DISPATCHER_NOOP_H_
#define  CBM_DISPATCHER_NOOP_H_

#include  "dispatcher/cbm_dispatcher.h"

namespace cbm
{
class CBM_API work_dispatcher_noop_t : public work_dispatcher_t
{
    LDNDC_OBJECT(work_dispatcher_noop_t)
    public:
        char const *  dispatch_strategy_name() const
            { return  dispatch_strategy_name_(); }
        work_dispatcher_t *  create( MPI_Comm const &  _mpicomm,
                lid_t const &  _id) const
            { return  new work_dispatcher_noop_t( _mpicomm, _id); }

        work_dispatcher_noop_t();
        work_dispatcher_noop_t(
            MPI_Comm const & /*mpi communicator*/,
                lid_t const & /*object id*/);
        ~work_dispatcher_noop_t();

        bool acquire_kernel( lid_t const &);


    private:
        static char const *  dispatch_strategy_name_()
            { return  "noop"; }
};
typedef  work_dispatcher_noop_t  work_dispatcher_default_t;

} /* namespace cbm */

#endif  /*  !CBM_DISPATCHER_NOOP_H_  */

