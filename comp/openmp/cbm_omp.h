/*!
 * @brief 
 *    wrappes openMP library functions to avoid ifdef littering. if
 *    openMP is deactivated functions are implemented as dummies.
 *
 * @author 
 *      steffen klatt  (created on: jan 31, 2012),
 *      edwin haas
 */


#ifndef  CBM_OPENMP_WRAPPER_H_
#define  CBM_OPENMP_WRAPPER_H_

#include  "crabmeat-config.h"
#include  "crabmeat-dllexport.h"
#ifdef  CRABMEAT_OPENMP
#  include  <omp.h>
#endif
namespace  cbm
{
    namespace  omp
    {
#ifdef  CRABMEAT_OPENMP
        typedef  ::omp_lock_t  omp_lock_t;
        typedef  ::omp_nest_lock_t  omp_nest_lock_t;
#else
        typedef  char  omp_lock_t;
        typedef  char  omp_nest_lock_t;
#endif

        /* lock interface */
        extern CBM_API omp_lock_t *  new_lock();
        extern CBM_API void  free_lock( omp_lock_t *);

        extern CBM_API void  init_lock( omp_lock_t *);
        extern CBM_API void  destroy_lock( omp_lock_t *);

        extern CBM_API void  set_lock( omp_lock_t *, char const * = NULL /*name*/);
        extern CBM_API void  unset_lock( omp_lock_t *, char const * = NULL /*name*/);

        extern CBM_API int  test_lock( omp_lock_t *, char const * = NULL /*name*/);

        /* nested lock interface */
        extern CBM_API omp_nest_lock_t *  new_nest_lock();
        extern CBM_API void  free_nest_lock( omp_nest_lock_t *);

        extern CBM_API void  init_nest_lock( omp_nest_lock_t *);
        extern CBM_API void  destroy_nest_lock( omp_nest_lock_t *);

        extern CBM_API void  set_nest_lock( omp_nest_lock_t *, char const * = NULL /*name*/);
        extern CBM_API void  unset_nest_lock( omp_nest_lock_t *, char const * = NULL /*name*/);

        extern CBM_API int  test_nest_lock( omp_nest_lock_t *, char const * = NULL /*name*/);

        /* misc */
        extern CBM_API int  get_num_threads();
        extern CBM_API int  get_thread_num();
    }  /* namespace omp */
} /* namespace cbm */


#endif  /*  !CBM_OPENMP_WRAPPER_H_  */

