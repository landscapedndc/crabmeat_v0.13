/*!
 * @brief 
 *    wrappes openMP library functions to avoid ifdef littering
 *
 * @author 
 *      steffen klatt  (created on: jan 31, 2012),
 *      edwin haas
 */

#include  "openmp/cbm_omp.h"

#ifdef  CRABMEAT_OPENMP

/* define this macro if you want lock names to be written to log */
//#define  LDNDC_OMP_SHOW_LOCK_NAMES  1
#if  defined(_DEBUG) || !defined(LDNDC_OMP_SHOW_LOCK_NAMES)
#  define  LDNDC_OMP_CBM_LogWrite  CBM_LogDebug
#else
#  define  LDNDC_OMP_CBM_LogWrite  CBM_LogInfo
#endif

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"


/* lock interface implementation */
cbm::omp::omp_lock_t *
cbm::omp::new_lock()
{
    cbm::omp::omp_lock_t *  omp_lock = CBM_DefaultAllocator->construct< omp_lock_t >();
    crabmeat_assert( omp_lock);

    return  omp_lock;
}

void
cbm::omp::free_lock( omp_lock_t *  _lock)
{
    if ( _lock)
    {
        CBM_DefaultAllocator->destroy( _lock);
    }
}

void
cbm::omp::init_lock( omp_lock_t *  _lock)
{
    omp_init_lock( _lock);
}

void
cbm::omp::destroy_lock( omp_lock_t *  _lock)
{
    omp_destroy_lock( _lock);
}

void
cbm::omp::set_lock( omp_lock_t *  _lock, char const *  _name)
{
    CRABMEAT_FIX_UNUSED(_name);
#ifdef  LDNDC_OMP_SHOW_LOCK_NAMES
    if ( _name)
    {
        LDNDC_OMP_CBM_LogWrite( "setting lock[@",(void*)_lock,"]: \"", _name, "\"");
    }
#endif  /*  LDNDC_OMP_SHOW_LOCK_NAMES  */
    omp_set_lock( _lock);
}

void
cbm::omp::unset_lock( omp_lock_t *  _lock, char const *  _name)
{
    CRABMEAT_FIX_UNUSED(_name);
#ifdef  LDNDC_OMP_SHOW_LOCK_NAMES
    if ( _name)
    {
        LDNDC_OMP_CBM_LogWrite( "unsetting lock[@",(void*)_lock,"]: \"", _name, "\"");
    }
#endif  /*  LDNDC_OMP_SHOW_LOCK_NAMES  */
    omp_unset_lock( _lock);
}

int
cbm::omp::test_lock( omp_lock_t *  _lock, char const *  _name)
{
    CRABMEAT_FIX_UNUSED(_name);
#ifdef  LDNDC_OMP_SHOW_LOCK_NAMES
    if ( _name)
    {
        LDNDC_OMP_CBM_LogWrite( "testing lock[@",(void*)_lock,"]: \"", _name, "\"");
    }
#endif  /*  LDNDC_OMP_SHOW_LOCK_NAMES  */
    return  omp_test_lock( _lock);
}


/* nested lock interface implementation */
cbm::omp::omp_nest_lock_t *
cbm::omp::new_nest_lock()
{
    cbm::omp::omp_nest_lock_t *  omp_lock = CBM_DefaultAllocator->construct< omp_nest_lock_t >();
    crabmeat_assert( omp_lock);

    return  omp_lock;
}

void
cbm::omp::free_nest_lock( omp_nest_lock_t *  _lock)
{
    if ( _lock)
    {
        CBM_DefaultAllocator->destroy( _lock);
    }
}

void
cbm::omp::init_nest_lock( omp_nest_lock_t *  _lock)
{
    omp_init_nest_lock( _lock);
}

void
cbm::omp::destroy_nest_lock( omp_nest_lock_t *  _lock)
{
    omp_destroy_nest_lock( _lock);
}

void
cbm::omp::set_nest_lock( omp_nest_lock_t *  _lock, char const *  _name)
{
    CRABMEAT_FIX_UNUSED(_name);
#ifdef  LDNDC_OMP_SHOW_LOCK_NAMES
    if ( _name)
    {
        LDNDC_OMP_CBM_LogWrite( "setting nested lock[",(void*)_lock,"]: \"", _name, "\"");
    }
#endif  /*  LDNDC_OMP_SHOW_LOCK_NAMES  */
    omp_set_nest_lock( _lock);
}

void
cbm::omp::unset_nest_lock( omp_nest_lock_t *  _lock, char const *  _name)
{
    CRABMEAT_FIX_UNUSED(_name);
#ifdef  LDNDC_OMP_SHOW_LOCK_NAMES
    if ( _name)
    {
        LDNDC_OMP_CBM_LogWrite( "unsetting nested lock[",(void*)_lock,"]: \"", _name, "\"");
    }
#endif  /*  LDNDC_OMP_SHOW_LOCK_NAMES  */
    omp_unset_nest_lock( _lock);
}

int
cbm::omp::test_nest_lock( omp_nest_lock_t *  _lock, char const *  _name)
{
    CRABMEAT_FIX_UNUSED(_name);
#ifdef  LDNDC_OMP_SHOW_LOCK_NAMES
    if ( _name)
    {
        LDNDC_OMP_CBM_LogWrite( "testing nested lock[",(void*)_lock,"]: \"", _name, "\"");
    }
#endif  /*  LDNDC_OMP_SHOW_LOCK_NAMES  */
    return  omp_test_nest_lock( _lock);
}


/* misc */
int
cbm::omp::get_num_threads()
{
#ifdef  _DEBUG
    if ( omp_get_num_threads() > CRABMEAT_OPENMP_MAXTHREADS)
    {
        CBM_LogFatal( "oops, please increase CRABMEAT_OPENMP_MAXTHREADS to at least ", omp_get_num_threads());
    }
#endif
    return  omp_get_num_threads();
}

int
cbm::omp::get_thread_num()
{
    return  omp_get_thread_num();
}

#else  /* not having openMP */

/* lock interface dummy implementation */
cbm::omp::omp_lock_t *
cbm::omp::new_lock() { return (omp_lock_t *)NULL; }

void
cbm::omp::free_lock( omp_lock_t *) { /* no op */ }
    
void
cbm::omp::init_lock( omp_lock_t *) { /* no op */ }

void
cbm::omp::destroy_lock( omp_lock_t *) { /* no op */ }

void
cbm::omp::set_lock( omp_lock_t *, char const *) { /* no op */ }

void
cbm::omp::unset_lock( omp_lock_t *, char const *) { /* no op */ }

int
cbm::omp::test_lock( omp_lock_t *, char const *) { return  1; }


/* nested lock interface dummy implementation */
cbm::omp::omp_nest_lock_t *
cbm::omp::new_nest_lock() { return (omp_nest_lock_t *)NULL; }

void
cbm::omp::free_nest_lock( omp_nest_lock_t *) { /* no op */ }
    
void
cbm::omp::init_nest_lock( omp_nest_lock_t *) { /* no op */ }

void
cbm::omp::destroy_nest_lock( omp_nest_lock_t *) { /* no op */ }

void
cbm::omp::set_nest_lock( omp_nest_lock_t *, char const *) { /* no op */ }

void
cbm::omp::unset_nest_lock( omp_nest_lock_t *, char const *) { /* no op */ }

int
cbm::omp::test_nest_lock( omp_nest_lock_t *, char const *) { return  1; }


/* misc */
int
cbm::omp::get_num_threads() { return  1; }

int
cbm::omp::get_thread_num() { return 0; }

#endif  /*  CRABMEAT_OPENMP  */

