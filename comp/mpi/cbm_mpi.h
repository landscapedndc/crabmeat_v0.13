/*!
 * @brief 
 *    wrappes MPI library functions to avoid ifdef littering. if
 *    MPI is deactivated functions are implemented as dummies.
 * 
 * @author 
 *      steffen klatt  (created on: mar 17, 2013)
 */


#ifndef  CBM_MPI_WRAPPER_H_
#define  CBM_MPI_WRAPPER_H_

#include  "crabmeat-common.h"
#ifdef  CRABMEAT_MPI
#  include  "io/outputtypes.h"
#  include  "utils/cbm_utils.h"
#  include  <mpi.h>
#else
#  define  MPI_Comm  char
#  define  MPI_Op  char
#  define  MPI_COMM_WORLD  '\0'
#endif

namespace  cbm
{
    namespace  mpi
    {
        /* rank of dedicated master node */
        extern CBM_API int  MASTER;
        extern CBM_API MPI_Comm  comm_ldndc;


        extern CBM_API bool  have_mpi();

        extern CBM_API int  init( int *, char ***);
        extern CBM_API int  finalize();

        extern CBM_API int  comm_rank(
                MPI_Comm);
        extern CBM_API int  world_rank();
        extern CBM_API int  comm_size( MPI_Comm);
        extern CBM_API int  world_size();

        extern CBM_API int  comm_dup(
                MPI_Comm /*original*/, MPI_Comm * /*duplicate*/);

        extern CBM_API int  barrier( MPI_Comm);
        extern CBM_API int  world_barrier();

        extern CBM_API int  status(
                int /*status*/, MPI_Comm);

#ifdef  CRABMEAT_MPI
        template < typename  /*cbm datatype*/ >
        struct  mpi_atomic_t
        {
        };
        template < >
        struct  mpi_atomic_t< ldndc_flt64_t >
        {
            mpi_atomic_t();
            MPI_Datatype const  datatype;
        };
        template < >
        struct  mpi_atomic_t< ldndc_flt32_t >
        {
            mpi_atomic_t();
            MPI_Datatype const  datatype;
        };
        template < >
        struct  mpi_atomic_t< ldndc_int32_t >
        {
            mpi_atomic_t();
            MPI_Datatype const  datatype;
        };
        template < >
        struct  mpi_atomic_t< ldndc_char_t >
        {
            mpi_atomic_t();
            MPI_Datatype const  datatype;
        };
#endif

        template < typename  _T, size_t  _n >
        struct  tiny_comm_t
        {
            _T  buf[_n];
            static size_t  buffer_size() { return  _n; }

            MPI_Comm  comm;
            int  rank;

            tiny_comm_t(
                    MPI_Comm = comm_ldndc);
            int  comm_size() { return  ::cbm::mpi::comm_size( this->comm); }
            int  comm_rank() { return  ::cbm::mpi::comm_rank( this->comm); }

            int  recv( int /*source*/, int = 0 /*tag*/);
            int  send( int /*target*/, int = 0 /*tag*/);
            int  bcast( int /*source*/);

            int  allreduce_or();
            private:
                int  allreduce_OP( MPI_Op);
        };
        struct  error_comm_t
        {
            error_comm_t( MPI_Comm  _comm = comm_ldndc)
                : err_comm( _comm) {}

            int  recv_error( int _err_code)
            {
                this->err_comm.buf[0] = _err_code;
                this->err_comm.allreduce_or();
                return  this->err_comm.buf[0];
            }
            private:
                tiny_comm_t< int, 1 >  err_comm;
        };

    }  /* namespace mpi */
} /* namespace cbm */

template < typename  _T, size_t  _n >
cbm::mpi::tiny_comm_t< _T, _n >::tiny_comm_t(
        MPI_Comm  _comm)
    : comm( _comm),
      rank( mpi::comm_rank( _comm))
{
}

template < typename  _T, size_t  _n >
int
cbm::mpi::tiny_comm_t< _T, _n >::recv(
    int  _source, int  _tag)
{
    CRABMEAT_FIX_UNUSED(_source);CRABMEAT_FIX_UNUSED(_tag);
    int  rc_recv = 0;
#ifdef  CRABMEAT_MPI
    rc_recv = MPI_Recv( static_cast< void * >( this->buf), _n,
                mpi_atomic_t< _T >().datatype, _source, _tag, this->comm, NULL);
#endif
    return  rc_recv;
}
template < typename  _T, size_t  _n >
int
cbm::mpi::tiny_comm_t< _T, _n >::send(
    int  _target, int  _tag)
{
    CRABMEAT_FIX_UNUSED(_target);CRABMEAT_FIX_UNUSED(_tag);
    int  rc_send = 0;
#ifdef  CRABMEAT_MPI
    rc_send = MPI_Send( static_cast< void * >( this->buf), _n,
                mpi_atomic_t< _T >().datatype, _target, _tag, this->comm);
#endif
    return  rc_send;
}
template < typename  _T, size_t  _n >
int
cbm::mpi::tiny_comm_t< _T, _n >::bcast(
        int  _root)
{
    CRABMEAT_FIX_UNUSED(_root);
    int  rc_bcast = 0;
#ifdef  CRABMEAT_MPI
    rc_bcast = MPI_Bcast( static_cast< void * >( this->buf), _n,
                mpi_atomic_t< _T >().datatype, _root, this->comm);
#endif
    return  rc_bcast;
}
template < typename  _T, size_t  _n >
int
cbm::mpi::tiny_comm_t< _T, _n >::allreduce_OP(
        MPI_Op  _mpi_op)
{
    CRABMEAT_FIX_UNUSED(_mpi_op);
    int  rc_allreduce = 0;
#ifdef  CRABMEAT_MPI
    _T  recv_buf[_n];
    rc_allreduce = MPI_Allreduce( static_cast< void * >( this->buf),
            static_cast< void * >( recv_buf), _n,
                mpi_atomic_t< _T >().datatype, _mpi_op, this->comm);
    if ( rc_allreduce == 0)
    {
        cbm::mem_cpy( this->buf, recv_buf, _n);
    }
#endif
    return  rc_allreduce;
}
template < typename  _T, size_t  _n >
int
cbm::mpi::tiny_comm_t< _T, _n >::allreduce_or()
{
#ifdef  CRABMEAT_MPI
    return  this->allreduce_OP( MPI_LOR);
#else
    return  0;
#endif
}


#ifdef  CRABMEAT_MPI
namespace  ldndc {
struct  sink_layout_t;
}

namespace  cbm {
    namespace  mpi{

        struct  packed_comm_t
        {
            ldndc::sink_layout_t const *  layout;
            char *  pbuf;
            int  n_pbuf;
            int  pbufpos;

            int  n_pbuf_single;
            size_t  buffer_size() const { return  static_cast< size_t >( this->n_pbuf); }

            MPI_Comm  comm;
            int  rank;

            packed_comm_t(
                    MPI_Comm = comm_ldndc);
            packed_comm_t(
                    ldndc::sink_layout_t const *, size_t = 1,
                    MPI_Comm = comm_ldndc);
            packed_comm_t(
                    packed_comm_t const &);
            packed_comm_t &  operator=(
                    packed_comm_t const &);
            ~packed_comm_t();

            int  set_layout(
                    ldndc::sink_layout_t const *);

            int  recv( int /*source*/, int /*tag*/);
            int  isend( int /*target*/, int /*tag*/);
            int  isend_multi( int * /*targets*/, int * /*tags*/, int /*number of targets*/);

            int  pack( void ** /*data according to layout*/);
            int  unpack( void ** /*data according to layout*/);

            int  initialize(
                    ldndc::sink_layout_t const *,
                    char const * /*source buffer*/, int /*source buffer size*/, int /*count*/);
            int  reallocate(
                    char const * /*source buffer*/, int /*source buffer size*/, int /*count*/);
        };

        MPI_Datatype  resolve_mpi_datatype(
                atomic_datatype_t /*atomic datatype*/);
        extern CBM_API size_t  packsize_of_layout(
                ldndc::sink_layout_t const * /*layout*/, MPI_Comm);
        extern CBM_API size_t  malloc_contiguous_from_layout(
                void ** /*buffer*/, ldndc::sink_layout_t const * /*layout*/,
                size_t /*count*/, size_t /*overhead*/, MPI_Comm  _comm);
        extern CBM_API void  free_contiguous_from_layout(
                void * /*buffer*/);
    }
}
#endif  /*  CRABMEAT_MPI  */


#endif  /*  !CBM_MPI_WRAPPER_H_  */

