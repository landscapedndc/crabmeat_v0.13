/*!
 * @brief 
 *    wrappes MPI library functions to avoid ifdef littering. if
 *    MPI is deactivated functions are implemented as dummies
 *    (implementation).
 *
 * @author 
 *      steffen klatt  (created on: mar 17, 2013)
 */

#include  "mpi/cbm_mpi.h"

#ifdef  CRABMEAT_MPI

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"

#include  "memory/cbm_mem.h"

bool
cbm::mpi::have_mpi()
{
    return  true;
}
int
cbm::mpi::init(
        int *  _argc, char ***  _argv)
{
    return  MPI_Init( _argc, _argv);
}
int
cbm::mpi::finalize()
{
    return  MPI_Finalize();
}


int
cbm::mpi::comm_rank(
        MPI_Comm  _mpi_comm)
{
    int  r = 0;
    MPI_Comm_rank( _mpi_comm, &r);

    return  r;
}

int
cbm::mpi::comm_size(
        MPI_Comm  _mpi_comm)
{
    int  s = 0;
    MPI_Comm_size( _mpi_comm, &s);

    return  s;
}

int
cbm::mpi::comm_dup(
        MPI_Comm  _comm_orig, MPI_Comm *  _comm_copy)
{
    return  MPI_Comm_dup( _comm_orig, _comm_copy);
}

int
cbm::mpi::barrier(
        MPI_Comm  _mpi_comm)
{
    return  MPI_Barrier( _mpi_comm);
}

int
cbm::mpi::status(
        int  _status, MPI_Comm  _mpi_comm)
{
    cbm::mpi::error_comm_t  err_comm( _mpi_comm);
    return  err_comm.recv_error( _status);
}


cbm::mpi::mpi_atomic_t< ldndc_flt64_t >::mpi_atomic_t()
    : datatype( MPI_DOUBLE) { }
cbm::mpi::mpi_atomic_t< ldndc_flt32_t >::mpi_atomic_t()
    : datatype( MPI_FLOAT) { }
cbm::mpi::mpi_atomic_t< ldndc_int32_t >::mpi_atomic_t()
    : datatype( MPI_INT) { }
cbm::mpi::mpi_atomic_t< ldndc_char_t >::mpi_atomic_t()
    : datatype( MPI_CHAR) { }


#else  /* not having MPI */

int
cbm::mpi::init(
        int *, char ***)
{
    /* no op */
    return  0;
}
int
cbm::mpi::finalize()
{
    /* no op */
    return  0;
}

bool
cbm::mpi::have_mpi()
{
    return  false;
}

int
cbm::mpi::comm_rank(
        MPI_Comm)
{
    return  0;
}

int
cbm::mpi::comm_size(
        MPI_Comm)
{
    return  1;
}

int
cbm::mpi::comm_dup(
        MPI_Comm, MPI_Comm *)
{
    return  0;
}

int
cbm::mpi::barrier(
        MPI_Comm)
{
    return  1;
}

int
cbm::mpi::status(
        int  _status, MPI_Comm)
{
    return  _status;
}

#endif  /*  CRABMEAT_MPI  */

int  cbm::mpi::MASTER = 0;
MPI_Comm  cbm::mpi::comm_ldndc;

int
cbm::mpi::world_rank()
{
    return  comm_rank( MPI_COMM_WORLD);
}
int
cbm::mpi::world_size()
{
    return  comm_size( MPI_COMM_WORLD);
}
int
cbm::mpi::world_barrier()
{
    return  barrier( MPI_COMM_WORLD);
}

#ifdef  CRABMEAT_MPI
static const MPI_Datatype  ldndc_to_mpi_atomic_datatype[] =
{
    MPI_DATATYPE_NULL /*dummy*/,
    MPI_SIGNED_CHAR /*bool :-| */,

    MPI_SIGNED_CHAR,
    MPI_UNSIGNED_CHAR,
    MPI_SHORT,
    MPI_UNSIGNED_SHORT,
    MPI_INT,
    MPI_UNSIGNED,
    MPI_LONG_LONG,
    MPI_UNSIGNED_LONG_LONG,

    MPI_FLOAT,
    MPI_DOUBLE,
#ifdef  LDNDC_HAVE_FLOAT128
    MPI_LONG_DOUBLE,
#endif

    MPI_CHAR,
    MPI_BYTE /*string :-| */
};
MPI_Datatype
cbm::mpi::resolve_mpi_datatype(
        atomic_datatype_t  _ldndc_datatype)
{
    if (( _ldndc_datatype < LDNDC_ATOMIC_DATATYPE_COMPOUND) || ( LDNDC_ATOMIC_DATATYPE_BOOL <= _ldndc_datatype))
    {
        return  ldndc_to_mpi_atomic_datatype[_ldndc_datatype];
    }
    return  MPI_DATATYPE_NULL;
}
size_t
cbm::mpi::packsize_of_layout(
        ldndc::sink_layout_t const *  _layout, MPI_Comm  _comm)
{
    if ( !_layout)
    {
        return  ldndc::invalid_t< size_t >::value;
    }
    size_t  n_bytes = 0;
    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        int  d_count = static_cast< int >( _layout->sizes[r]);
        if ( _layout->types[r] == LDNDC_STRING)
        {
            d_count *= sizeof_CBM_STRING;
        }
        MPI_Datatype const  d_type = resolve_mpi_datatype( _layout->types[r]);

        int  d_size = 0;
        MPI_Pack_size( d_count, d_type, _comm, &d_size);
        n_bytes += static_cast< size_t >( d_size);
    }

    return  n_bytes;
}
size_t
cbm::mpi::malloc_contiguous_from_layout(
        void **  _buf, ldndc::sink_layout_t const *  _layout,
        size_t  _count, size_t  /*_overhead*/, MPI_Comm  _comm)
{
    if ( !_layout)
    {
        return  ldndc::invalid_t< size_t >::value;
    }

    size_t const  n_bytes = mpi::packsize_of_layout( _layout, _comm);
    if ( n_bytes && cbm::is_valid( n_bytes))
    {
        size_t const  n_fullbytes = _count * n_bytes /*+ _overhead*/;
        if ( _buf)
        {
            *_buf = CBM_DefaultAllocator->allocate_init_type< ldndc_byte_t >( n_fullbytes, 0);
            if ( !*_buf)
            {
                return  0;
            }
        }
        return  n_fullbytes;
    }
    return  LDNDC_ERR_INVALID_ARGUMENT;
}
void
cbm::mpi::free_contiguous_from_layout(
        void *  _buf)
{
    if ( _buf)
    {
        CBM_DefaultAllocator->deallocate( _buf);
    }
}



cbm::mpi::packed_comm_t::packed_comm_t(
        MPI_Comm  _comm)
        : layout( NULL), pbuf( NULL), n_pbuf( 0), pbufpos( 0),
          n_pbuf_single( 0),
          comm( _comm), rank( mpi::comm_rank( _comm))
{
    this->initialize( NULL, NULL, 0, 0);
}
cbm::mpi::packed_comm_t::packed_comm_t(
        ldndc::sink_layout_t const *  _layout, size_t  _count, MPI_Comm  _comm)
        : layout( _layout), pbuf( NULL), n_pbuf( 0), pbufpos( 0),
          n_pbuf_single( 0),
          comm( _comm), rank( mpi::comm_rank( _comm))
{
    this->initialize( _layout, NULL, 0, _count);
}
#include  "utils/lutils.h"
cbm::mpi::packed_comm_t::packed_comm_t(
        cbm::mpi::packed_comm_t const &  _pcomm)
        : layout( _pcomm.layout), pbuf( NULL), n_pbuf( _pcomm.n_pbuf), pbufpos( _pcomm.pbufpos),
          n_pbuf_single( _pcomm.n_pbuf_single),
          comm( _pcomm.comm), rank( mpi::comm_rank( _pcomm.comm))
{
    crabmeat_assert( _pcomm.n_pbuf_single > 0);
    this->initialize( _pcomm.layout, _pcomm.pbuf, _pcomm.n_pbuf, _pcomm.n_pbuf/_pcomm.n_pbuf_single);
}
cbm::mpi::packed_comm_t &
cbm::mpi::packed_comm_t::operator=(
        cbm::mpi::packed_comm_t const &  _pcomm)
{
    if ( this == &_pcomm)
    {
        return  *this;
    }

    this->layout = _pcomm.layout;
    this->pbufpos = _pcomm.pbufpos;
    this->n_pbuf_single = _pcomm.n_pbuf_single;
    this->comm = _pcomm.comm;
    this->rank = mpi::comm_rank( this->comm);

    this->reallocate( _pcomm.pbuf, _pcomm.n_pbuf, _pcomm.n_pbuf);

    return  *this;
}
cbm::mpi::packed_comm_t::~packed_comm_t()
{
    if ( this->pbuf)
    {
// sk:bdg        CBM_LogDebug( "free [@",(void *)this->pbuf, "]");
        free_contiguous_from_layout( this->pbuf);
    }
    this->pbuf = NULL;
    this->n_pbuf = 0;
    this->pbufpos = 0;
    this->n_pbuf_single = 0;

    this->layout = NULL;
}
int
cbm::mpi::packed_comm_t::set_layout(
        ldndc::sink_layout_t const *  _layout)
{
    if ( !_layout)
    {
        return  -1;
    }

    this->layout = _layout;
    this->n_pbuf_single = mpi::malloc_contiguous_from_layout(
            NULL, _layout, 1, 1*MPI_BSEND_OVERHEAD, this->comm);
// sk:bdg    CBM_LogDebug( "single packsize=",this->n_pbuf_single);
    return  0;
}
int
cbm::mpi::packed_comm_t::initialize(
        ldndc::sink_layout_t const *  _layout, char const *  _pbuf, int  _n_pbuf, int  _count)
{
    if ( _layout)
    {
        this->set_layout( _layout);

        if ( _count > 0)
        {
            this->n_pbuf = mpi::malloc_contiguous_from_layout(
                    (void **)&this->pbuf, _layout, _count, _count*MPI_BSEND_OVERHEAD, this->comm);
            if ( !this->pbuf)
            {
                return  -1;
            }
            if ( _pbuf)
            {
                cbm::mem_cpy< ldndc_byte_t >( this->pbuf, _pbuf, _n_pbuf);
            }
// sk:bdg            CBM_LogDebug( "allocated [@",(void *)this->pbuf, "] ",this->n_pbuf, " bytes  [count=",_count,",overhead=",MPI_BSEND_OVERHEAD,"]");
        }
    }
    return  0;
}
int
cbm::mpi::packed_comm_t::reallocate(
        char const *  _pbuf, int  _n_pbuf, int  _count)
{
//    CBM_LogDebug( "reallocating pack message buffer: ",this->n_pbuf," -> ",_n_pbuf);
    char *  new_pbuf = NULL;
    int  n_new_pbuf = 0;
    if ( _count > 0)
    {
        n_new_pbuf = mpi::malloc_contiguous_from_layout(
                (void **)&new_pbuf, this->layout, _count, _count*MPI_BSEND_OVERHEAD, this->comm);
        if ( new_pbuf)
        {
            cbm::mem_cpy< ldndc_byte_t >( new_pbuf, _pbuf, std::min( n_new_pbuf, _n_pbuf));
        }
// sk:bdg        CBM_LogWarn( "\treallocating pack message buffer: ",this->n_pbuf," -> ",n_new_pbuf);
    }
    if ( this->pbuf)
    {
        CBM_DefaultAllocator->deallocate( this->pbuf);
    }

    this->pbuf = new_pbuf;
    this->n_pbuf = n_new_pbuf;

    return  new_pbuf ? 0 : -1;
}

int
cbm::mpi::packed_comm_t::recv(
        int  _source, int  _tag)
{
    int  rc_recv = MPI_Recv( &this->n_pbuf, 1, MPI_INT,
            _source, _tag, this->comm, NULL);
// sk:bdg    CBM_LogDebug( "receiving packed message of size ",this->n_pbuf);

    rc_recv = MPI_Recv( static_cast< void * >( this->pbuf), this->n_pbuf, MPI_PACKED,
                _source, _tag, this->comm, NULL);
    this->pbufpos = 0;
    return  rc_recv;
}
int
cbm::mpi::packed_comm_t::isend(
        int  _target, int  _tag)
{
    int  target = _target;
    int  tag = _tag;
    return  this->isend_multi( &target, &tag, 1);
}
int
cbm::mpi::packed_comm_t::isend_multi(
        int *  _targets, int *  _tags, int  _n_targets)
{
    MPI_Request *  req = CBM_DefaultAllocator->allocate_type< MPI_Request >( _n_targets);
    if ( !req)
    {
        CBM_DefaultAllocator->deallocate( req);
        return  -1;
    }
    int  rc_isendmulti = 0;
    for ( int  t = 0;  t < _n_targets;  ++t)
    {
        int  rc_isend = MPI_Isend( &this->n_pbuf, 1, MPI_INT,
                _targets[t], _tags[t], this->comm, &req[t]);
        if ( rc_isend)
        {
            rc_isendmulti = -1;
        }
// sk:bdg        CBM_LogDebug( "sending packed message of size ",this->n_pbuf);
    }
    MPI_Waitall( _n_targets, req, MPI_STATUSES_IGNORE);

    for ( int  t = 0;  t < _n_targets;  ++t)
    {
        int  rc_isend = MPI_Isend( static_cast< void * >( this->pbuf), this->n_pbuf, MPI_PACKED,
                _targets[t], _tags[t], this->comm, &req[t]);
        if ( rc_isend)
        {
            rc_isendmulti = -1;
        }
    }
    MPI_Waitall( _n_targets, req, MPI_STATUSES_IGNORE);

    CBM_DefaultAllocator->deallocate( req);

    this->pbufpos = 0;
    return  rc_isendmulti;
}
int
cbm::mpi::packed_comm_t::pack(
        void **  _data)
{
    if ( !this->layout || !_data)
    {
        CBM_LogError( "mpi::packed_comm_t::pack(): ",
                "no layout, no data .. no packing :(");
        return  -1;
    }
// sk:bdg    CBM_LogWarn( "bpos=",this->pbufpos, "  bsize=",this->n_pbuf, "  breqsize=",this->pbufpos+this->n_pbuf_single);
    if ( !this->pbuf || (( this->pbufpos+this->n_pbuf_single) > this->n_pbuf))
    {
        crabmeat_assert( this->n_pbuf_single > 0);
        int  rc_realloc = this->reallocate( this->pbuf, this->n_pbuf, ( this->n_pbuf / this->n_pbuf_single)+1);
        if ( rc_realloc)
        {
            return  -1;
        }
    }

    int  rc_pack = 0;
    for ( ldndc_output_rank_t  r = 0;  r < this->layout->rank;  ++r)
    {
        int  d_count = static_cast< int >( this->layout->sizes[r]);
        if ( this->layout->types[r] == LDNDC_STRING)
        {
            d_count *= sizeof_CBM_STRING;
        }
        crabmeat_assert( d_count > 0);

        MPI_Datatype const  d_type = resolve_mpi_datatype( this->layout->types[r]);

        int  rc_packtype = MPI_Pack( _data[r], d_count, d_type, this->pbuf, this->n_pbuf, &this->pbufpos, this->comm);
// sk:bdg        CBM_LogDebug( "packing message elements [*",d_count,"]  new pos=",this->pbufpos);
        if ( rc_packtype)
        {
            CBM_LogError( "mpi::packed_comm_t::pack(): ",
                    "error packing message");
            rc_pack = -1;
            break;
        }
    }
    return  rc_pack;
}
int
cbm::mpi::packed_comm_t::unpack(
               void **  _data)
{
    if ( !this->pbuf || !this->layout || !_data)
    {
        return  -1;
    }

    int  rc_unpack = 0;
    for ( ldndc_output_rank_t  r = 0;  r < this->layout->rank;  ++r)
    {
        int  d_count = static_cast< int >( this->layout->sizes[r]);
        if ( this->layout->types[r] == LDNDC_STRING)
        {
            d_count *= sizeof_CBM_STRING;
        }
        crabmeat_assert( d_count > 0);

        MPI_Datatype const  d_type = resolve_mpi_datatype( this->layout->types[r]);

        int  rc_unpacktype = MPI_Unpack( this->pbuf, this->n_pbuf, &this->pbufpos, _data[r], d_count, d_type, this->comm);
// sk:bdg        CBM_LogDebug( "unpacking message elements [*",d_count,"]  new pos=",this->pbufpos, "  nbuf=",this->n_pbuf);
        if ( rc_unpacktype)
        {
            CBM_LogError( "mpi::packed_comm_t::unpack(): ",
                    "error unpacking message");
            rc_unpack = -1;
            break;
        }
    }
    return  rc_unpack;

}

#endif  /*  CRABMEAT_MPI  */

