
#ifndef CRABMEAT_DLLEXPORT_H_
#define CRABMEAT_DLLEXPORT_H_

#include  "crabmeat-compiler.h"


/* see https://gcc.gnu.org/wiki/Visibility */
#if defined(CRABMEAT_COMPILER_MSC)
#  define CRABMEAT_HELPER_DLL_IMPORT __declspec(dllimport)
#  define CRABMEAT_HELPER_DLL_EXPORT __declspec(dllexport)
#  define CRABMEAT_HELPER_DLL_NOEXPORT
#else
#  if (defined(CRABMEAT_COMPILER_GCC) && (__GNUC__ >= 4)) || defined(CRABMEAT_COMPILER_CLANG)
#    define CRABMEAT_HELPER_DLL_IMPORT __attribute__ ((visibility ("default")))
#    define CRABMEAT_HELPER_DLL_EXPORT __attribute__ ((visibility ("default")))
#    define CRABMEAT_HELPER_DLL_NOEXPORT  __attribute__ ((visibility ("hidden")))
#  else
#    define CRABMEAT_HELPER_DLL_IMPORT
#    define CRABMEAT_HELPER_DLL_EXPORT
#    define CRABMEAT_HELPER_DLL_NOEXPORT
#  endif
#endif

#ifdef CRABMEAT_TARGETS_SHARED_LIBRARIES
#  if defined(crabmeat_EXPORTS) || defined(pcrabmeat_EXPORTS)
#    define CBM_API CRABMEAT_HELPER_DLL_EXPORT
#  else
#    define CBM_API CRABMEAT_HELPER_DLL_IMPORT
#  endif
#  define CBM_LOCAL CRABMEAT_HELPER_DLL_NOEXPORT
#else
#  define CBM_API
#  define CBM_LOCAL
#endif


#endif /* !CRABMEAT_DLLEXPORT_H_ */
