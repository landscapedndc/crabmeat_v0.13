/*!
 * @brief
 *
 * @authors
 *    steffen klatt (created on: oct 09, 2011),
 *    edwin haas
 */

#ifndef  CBM_INPUT_OBJECT_H_
#define  CBM_INPUT_OBJECT_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "io/data-provider/ireaders/ireader.h"

namespace ldndc {
class  iobject_handler_base_t;
class  source_handle_srv_t;
class  CBM_API  input_source_srv_base_t  :  public  cbm::server_object_t
{
    protected:
        input_source_srv_base_t( lid_t const &);
    public:
        virtual  ~input_source_srv_base_t() = 0;
};

struct  source_info_t;
class  CBM_API  input_source_srv_t  :  public  input_source_srv_base_t
{
    LDNDC_SERVER_OBJECT(input_source_srv_t)
    public:
        input_source_srv_t();
        input_source_srv_t( lid_t const & /*stream id*/);
        ~input_source_srv_t();

        lerr_t  status() const;
        char const *  source_identifier() const;
        char const *  input_class_type() const;
        size_t  number_of_blocks() const;

        lerr_t  attempt_to_open_stream( source_info_t const *);
        lerr_t  open_stream();

        bool  check_input_exists( lid_t const & /*block id*/) const;

        lerr_t  source_handle_acquire(
            source_handle_srv_t * /*handle to set*/, lid_t const & /*block id*/);
        lerr_t  source_handle_release(
            source_handle_srv_t * /*handle to release*/);

        template< typename  _R >
            _R const *  reader() const
                { return  dynamic_cast< _R const * >( this->m_reader); }

        int  get_available_blocks( lid_t [], size_t);

        lerr_t  update( cbm::sclock_t const *);
        lerr_t  commit( cbm::sclock_t const *);
        lerr_t  reset();

    private:
        /* stream handler performs open/load/close/.. operations  (owner) */
        iobject_handler_base_t *  m_fhandler;
        /* regional stream reader extracts data from stream (owner) */
        iproc_landscape_provider_t *  m_reader;

        lerr_t  create_handler_and_reader_( source_info_t const *);
        void  free_handler_and_reader_();
        lerr_t  open_stream_();
        lerr_t  scan_stream_();
        void  close_stream_();
};


class  CBM_API  input_source_noreader_srv_t  :  public  input_source_srv_base_t
{
    LDNDC_SERVER_OBJECT(input_source_noreader_srv_t)
    public:
        input_source_noreader_srv_t();
        input_source_noreader_srv_t( lid_t const &);
};

}

#endif  /*  !CBM_INPUT_OBJECT_H_  */

