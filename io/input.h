/*!
 * @brief
 *    container for open input streams
 *
 * @authors
 *    Steffen Klatt (rewritten on: oct 09, 2011)
 *    Edwin Haas
 */

#ifndef  CBM_INPUT_H_
#define  CBM_INPUT_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "log/cbm_baselog.h"

#include  <map>
#include  <vector>

namespace ldndc {

class  input_source_srv_t;
class  source_handle_srv_t;
struct  source_info_t;

class CBM_API  raw_inputs_t  :  public  cbm::server_object_t
{
    LDNDC_SERVER_OBJECT(raw_inputs_t)

    /* input objects container related types */
    typedef  std::pair< std::string, input_source_srv_t * >  sources_pair_type;
    typedef  std::map< sources_pair_type::first_type, sources_pair_type::second_type >  sources_container_type;
    typedef  sources_container_type::iterator  sources_iterator_type;
    typedef  sources_container_type::const_iterator  sources_const_iterator_type;
    typedef  std::pair< sources_container_type::iterator, bool >  sources_container_insert_return_type;
    public:
        struct  source_block_mapping_t
        {
            cbm::source_descriptor_t  sd;
            lid_t *  bl;
            size_t  bl_size;
        };
    public:
        raw_inputs_t();
        ~raw_inputs_t();

        lid_t  mount_and_open_source( source_info_t const *);
        lerr_t  open_mounted_source( char const * /*identifier*/,
                char const * /*type*/);
        lerr_t  open_mounted_source( input_source_srv_t *);

// sk:later        void  unmount_source( char const * /*type*/, lid_t const & /*source id*/);
        lerr_t  unmount_all_sources();

        /*!
         * @brief
         *    retrieving core blocks
         */
        lerr_t  get_input_source( input_source_srv_t **,
                lid_t const &, char const * /*type*/);
        lerr_t  get_input_source( input_source_srv_t const **,
                lid_t const &, char const * /*type*/) const;

        lerr_t  get_input_source( input_source_srv_t **,
            cbm::source_descriptor_t const *, char const * /*type*/);
        lerr_t  get_input_source( input_source_srv_t const **,
            cbm::source_descriptor_t const *, char const * /*type*/) const;
    private:
        input_source_srv_t *  mount_and_open_source_(
                source_info_t const *);
        lerr_t  get_input_source_( input_source_srv_t **,
                lid_t const &, char const * /*type*/);
    public:

        /*!
         * @brief
         *    return number of opened streams. if argument
         *    is provided only the number of open streams
         *    of that type is returned, the sum over all
         *    types otherwise.
         */
        size_t  number_of_open_sources() const;
        size_t  number_of_open_sources(
            char const * /*type*/, size_t * = NULL /*block count*/) const;

        lerr_t  check_source_exists( lid_t const & /*source id*/,
                char const * /*type*/) const;
        /*!
         * @brief
         *    retrieving core blocks
         */
        lerr_t  source_handle_acquire( source_handle_srv_t *,
                cbm::source_descriptor_t const *,
                char const * /*type*/);
    public:
        /*!
         * @brief
         *    return number of core blocks available from
         *    opened streams. if argument is provided only
         *    the number of core blocks of that type is 
         *    returned, the sum over all types otherwise.
         */
        size_t  number_of_core_blocks() const;
        size_t  number_of_core_blocks( char const * /*type*/) const;

        int  get_available_blocks( source_block_mapping_t [],
                size_t, char const * /*type*/) const;

        lerr_t  check_input_exists( cbm::source_descriptor_t const *,
                char const * /*type*/) const;

        lid_t  available_source_id() const;
        std::string  make_source_key(
                char const * /*type*/, lid_t const &) const;
        lid_t  find_by_source_identifier( char const *  /*identifier*/) const;
        lid_t  find_by_source_identifier(
                cbm::source_descriptor_t const *) const;

        lerr_t  update( cbm::sclock_t const *);
        lerr_t  commit( cbm::sclock_t const *);
        lerr_t  reset();

    private:
        /* holds pointers to input objects by key <type>.<id>
         *
         * note that only associated input object are stored in the vector.
         * inputs not associated to input streams (readers) do not possess
         * input objects.
         *
         * note that multiple streams per type can be attached.
         */
        sources_container_type  m_sources;
        void  m_clear();
};

} /* namespace ldndc */

#endif  /*  !CBM_INPUT_H_  */

