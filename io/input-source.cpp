/*!
 * @brief
 *    set up facilities to read input data
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011),
 *    edwin haas
 */

#include  "io/input-source.h"
#include  "io/source-handle-srv.h"

#include  "io/fhandler/fhandlers.h"
#include  "io/ifactory.h"

#include  "io/source-info.h"

#include  "log/cbm_baselog.h"
#include  "string/cbm_string.h"

LDNDC_SERVER_OBJECT_DEFN(ldndc::input_source_srv_t)

ldndc::input_source_srv_base_t::input_source_srv_base_t(
        lid_t const &  _id)
        : cbm::server_object_t( _id)
{
}
ldndc::input_source_srv_base_t::~input_source_srv_base_t()
{
    /* no op */
}



ldndc::input_source_srv_t::input_source_srv_t()
    : input_source_srv_base_t( invalid_lid),
          m_fhandler( NULL), m_reader( NULL)
{
}
ldndc::input_source_srv_t::input_source_srv_t(
        lid_t const &  _stream_id)
    : input_source_srv_base_t( _stream_id),
          m_fhandler( NULL), m_reader( NULL)
{
}

ldndc::input_source_srv_t::~input_source_srv_t()
{
    this->free_handler_and_reader_();
}
void
ldndc::input_source_srv_t::free_handler_and_reader_()
{
    if ( this->m_reader)
    {
        iproc_factories_t  ifactory;
// FIXME  may not be ireader!
        ifactory.destroy_landscape_ireader( static_cast< iproc_landscape_reader_base_t * >( this->m_reader));
        this->m_reader = NULL;
    }
    if ( this->m_fhandler)
    {
        if ( this->m_fhandler->close() != LDNDC_ERR_OK)
        {
            CBM_LogError( "error closing input file");
        }
        iproc_factories_t  ifactory;
        ifactory.destroy_ihandler( this->m_fhandler);
        this->m_fhandler = NULL;
    }
}

lerr_t
ldndc::input_source_srv_t::status()
const
{
    return  this->m_reader ? this->m_reader->status() : LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
}
char const *
ldndc::input_source_srv_t::source_identifier()
const
{
    if ( this->m_fhandler && this->m_fhandler->get_source_info())
    {
        return  static_cast< char const * >(
            this->m_fhandler->get_source_info()->identifier.c_str());
    }
    return  NULL;
}
char const *
ldndc::input_source_srv_t::input_class_type()
const
{
    if ( this->m_fhandler && this->m_fhandler->get_source_info())
    {
        return  this->m_fhandler->get_source_info()->type.c_str();
    }
    return  NULL;
}
size_t
ldndc::input_source_srv_t::number_of_blocks()
const
{
    return  ( this->m_reader) ? this->m_reader->number_of_blocks() : invalid_t< size_t >::value;
}
int
ldndc::input_source_srv_t::get_available_blocks(
        lid_t  _list[], size_t  _max_size)
{
    if ( !this->m_reader)
    {
        return  -1;
    }
    return  this->m_reader->get_available_blocks( _list, _max_size);
}


bool
ldndc::input_source_srv_t::check_input_exists(
        lid_t const &  _block_id)
const
{
    if ( !this->m_reader)
    {
        CBM_LogError( "can't tell you if input exists .. ain't got no reader");
        return  false;
    }
    return  this->m_reader->check_input_exists( _block_id);
}

lerr_t
ldndc::input_source_srv_t::attempt_to_open_stream( 
        source_info_t const *  _source_info)
{
    crabmeat_assert( _source_info);
// sk:dbg    CBM_LogDebug( "attempting to open stream  ",*_source_info);

    lerr_t  rc_create = this->create_handler_and_reader_( _source_info);
    if ( rc_create)
    {
        CBM_LogError( "failed to create either handler or reader for input source  [source=",*_source_info,"]");
        return  LDNDC_ERR_FAIL;
    }
    return  this->open_stream();
}

lerr_t
ldndc::input_source_srv_t::open_stream()
{
    lerr_t  rc_open = this->open_stream_();
    if ( rc_open)
    {
        this->close_stream_();
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_scan = this->scan_stream_();
    if ( rc_scan)
    {
        this->close_stream_();
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::input_source_srv_t::create_handler_and_reader_(
        source_info_t const *  _source_info)
{
    /* factory for i/o format and input class type handlers and readers */
    iproc_factories_t  ifactory;

    this->m_fhandler = ifactory.construct_ihandler(
                    _source_info->format.c_str());
    if ( !this->m_fhandler)
    {
        CBM_LogError( "input_source_srv_t::create_handler_and_reader(): ",
                "nomem or handler for format not available  [format=",_source_info->format,"]");
        return  LDNDC_ERR_NOMEM;
    }
    this->m_fhandler->set_source_info( _source_info);

    this->m_reader = ifactory.construct_landscape_ireader(
            _source_info->type.c_str(), _source_info->format.c_str());
    if ( !this->m_reader)
    {
        CBM_LogError( "input_source_srv_t::create_handler_and_reader(): ",
                "nomem or reader for format not available  [format=",_source_info->format,",class=",_source_info->type,"]");
        ifactory.destroy_ihandler( this->m_fhandler);
        this->m_fhandler = NULL;

        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::input_source_srv_t::open_stream_()
{
    if ( !this->m_fhandler)
        { return  LDNDC_ERR_FAIL; }

    cbm::string_t const  T = this->m_fhandler->get_source_info()->type;
    cbm::string_t const  F = this->m_fhandler->get_source_info()->format;
    CBM_LogDebug( "attempt to open input data object of type ",T," as ",F,"...");

    lerr_t  rc_open = this->m_fhandler->open( NULL);
    RETURN_IF_NOT_OK(rc_open);
    CBM_LogInfo( "opening succeeded  [format=",F,",class=",T,"]");

    lerr_t  rc_load = this->m_fhandler->load();
    RETURN_IF_NOT_OK(rc_load);
    CBM_LogInfo( "loading succeeded  [format=",F,",class=",T,"]");

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::input_source_srv_t::scan_stream_()
{
    if ( !this->m_reader || !this->m_fhandler)
    {
        CBM_LogError( "input_source_srv_t::scan_():  attempt to scan stream without handler or reader");
        return  LDNDC_ERR_FAIL;
    }

    int  block_cnt = this->m_reader->scan_stream( this->m_fhandler);
    if ( block_cnt == 0)
        { /* no op */ }
    else if ( block_cnt < 0)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

void
ldndc::input_source_srv_t::close_stream_()
{
    if ( this->m_reader)
    {
// FIXME  may not be a reader!
        iproc_factories_t().destroy_landscape_ireader( static_cast< iproc_landscape_reader_base_t * >( this->m_reader));
        this->m_reader = NULL;
    }
    if ( this->m_fhandler)
    {
        this->m_fhandler->close();

        iproc_factories_t().destroy_ihandler( this->m_fhandler);
        this->m_fhandler = NULL;
    }
}


lerr_t
ldndc::input_source_srv_t::source_handle_acquire(
        source_handle_srv_t *  _s_hdl,
        lid_t const &  _id)
{
    crabmeat_assert( _s_hdl);
//    CBM_LogDebug( "input_source_srv_t::source_handle_acquire() IN:  \"",this->source_identifier(),"\"");

    if ( !this->m_reader || !this->m_fhandler)
    {
        CBM_LogError( "i have no data provider .. i am of no help to you. lets wave goodbye  [","?","]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( !this->check_input_exists( _id))
    {
        CBM_LogError( "block with given ID does not exist in source",
            "  [",((this->m_fhandler->get_source_info())?this->m_fhandler->get_source_info()->identifier.c_str():"?"),"]");
        return  LDNDC_ERR_INVALID_ID;
    }

    iproc_provider_t *  iproc = this->m_reader->acquire_provider( _id);
    if ( !iproc)
    {
        CBM_LogError( "failed to get reader for source",
            "  [",((this->m_fhandler->info())?this->m_fhandler->info()->identifier.c_str():"?"),"]");
        return  LDNDC_ERR_OBJECT_CREATE_FAILED;
    }

// sk:later    if ( have_dataprocessing_stack)
    {
        iproc_factories_t const  ifactory;
        char const *  ic = this->m_fhandler->info()->type.c_str();
 
        iproc_provider_t *  iproc_stack = NULL;
        iproc_stack = static_cast< iproc_provider_t * >( ifactory.construct_iproc_stack( iproc, ic));
        if ( !iproc_stack)
        {
            CBM_LogError( "failed to instantiate input data processing stack object.");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        this->m_reader->add_to_processing_stack( &iproc, iproc_stack);
    }
 
    /* set various things in source handle */
    _s_hdl->set_object_id( _id);
    _s_hdl->set_stream_id( this->object_id());
    lerr_t const  rc_setprovider =
            _s_hdl->set_provider( iproc, this->m_reader);
    if ( rc_setprovider)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    CBM_LogDebug( "input_source_srv_t::source_handle_acquire() OUT:",
        "  acquiring source handle: prov=",(void*)iproc,"[",iproc->object_id(),"]","  l-prov=",(void*)this->m_reader);

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::input_source_srv_t::source_handle_release(
        source_handle_srv_t *  _handle)
{
    if ( this->m_reader)
    {
        this->m_reader->release_provider( _handle->object_id());
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}


lerr_t
ldndc::input_source_srv_t::update(
        cbm::sclock_t const *  _lclock)
{
    if ( !this->m_reader)
    {
        return  LDNDC_ERR_OK;
    }

    int  rc_update = this->m_reader->collective_update_commence( _lclock);
    if ( rc_update)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::input_source_srv_t::commit(
        cbm::sclock_t const *  _lclock)
{
    if ( !this->m_reader)
    {
        return  LDNDC_ERR_OK;
    }

    int  rc_commit = this->m_reader->collective_update_complete( _lclock);
    if ( rc_commit)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::input_source_srv_t::reset()
{
// sk:todo    if ( !this->m_reader)
// sk:todo    {
// sk:todo        return  LDNDC_ERR_OK;
// sk:todo    }
// sk:todo
// sk:todo    int  rc_reset = this->m_reader->collective_reset();
// sk:todo    if ( rc_reset)
// sk:todo    {
// sk:todo        return  LDNDC_ERR_FAIL;
// sk:todo    }
    return  LDNDC_ERR_OK;
}



LDNDC_SERVER_OBJECT_DEFN(ldndc::input_source_noreader_srv_t)
ldndc::input_source_noreader_srv_t::input_source_noreader_srv_t()
        : ldndc::input_source_srv_base_t( invalid_lid)
{
}
ldndc::input_source_noreader_srv_t::input_source_noreader_srv_t(
        lid_t const &  _id)
        : ldndc::input_source_srv_base_t( _id)
{
}


