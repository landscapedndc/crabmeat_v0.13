/*!
 * @brief
 *    output processors factory
 *
 * @author
 *    steffen klatt (created on: nov 19, 2013)
 */

#include  "io/ofactory.h"

#include  "io/sink-info.h"

#include  "io/data-receiver/owriters/owriters.h"

#include  "utils/cbm_utils.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

bool
oproc_factories_t::landscape_receiver_available(
        sink_info_t const *  _sink_info)
const
{
    crabmeat_assert( _sink_info);

    switch ( _sink_info->rcv_type)
    {
        case 'O':
        {
            if ( find_owriter_factory( 'L', _sink_info->format.c_str()) != NULL)
                { return true; }
            return false;
        }

        default:
        {
            /*fall through*/
        }
    }
    CBM_LogFatal( "[BUG]  unknown receiver type",
        "  [identifier=",_sink_info->identifier,",type=",_sink_info->format,"]");
    return  false;
}
ldndc::oproc_landscape_receiver_t *
oproc_factories_t::construct_landscape_receiver(
        sink_info_t const *  _sink_info)
const
{
    crabmeat_assert( _sink_info);
    if ( this->landscape_receiver_available( _sink_info))
    {
        switch ( _sink_info->rcv_type)
        {
            case 'O':
            {
                return static_cast< oproc_landscape_receiver_t * >(
                    find_owriter_factory( 'L', _sink_info->format.c_str())->construct());
            }
            default:
            {
                /*fall through*/
            }
        }
    }
        CBM_LogWarn( "no such receiver",
            "  [identifier=",_sink_info->identifier,",format=",_sink_info->format,"]");
    return  NULL;
}
void
oproc_factories_t::destroy_landscape_receiver(
        oproc_landscape_receiver_t *  _ow)
const
{
    if ( !_ow)
        { return; }
    switch ( _ow->receiver_type())
    {
        case 'O':
        {
            find_owriter_factory( 'L', _ow->output_format_type())->destroy( _ow);
            break;
        }
        default:
        {
            CBM_LogError( "failed to free receiver because i was unable to obtain its factory");
        }
    }
}

} /* namespace ldndc */

