/*!
 * @brief
 *
 * @authors
 *    steffen klatt (created on: nov 02, 2013),
 *    edwin haas
 */

#include  "io/output-sink.h"
#include  "log/cbm_baselog.h"

#include  "io/data-receiver/receiver.h"
#include  "io/ofactory.h"


LDNDC_SERVER_OBJECT_DEFN(ldndc::output_sink_srv_base_t)
ldndc::output_sink_srv_base_t::output_sink_srv_base_t()
        : cbm::server_object_t()
{
}
ldndc::output_sink_srv_base_t::output_sink_srv_base_t(
                lid_t const &  _object_id)
        : cbm::server_object_t( _object_id)
{
}

ldndc::output_sink_srv_base_t::~output_sink_srv_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(ldndc::output_sink_srv_t)
ldndc::output_sink_srv_t::output_sink_srv_t()
        : ldndc::output_sink_srv_base_t(),
        lrcv_( NULL), refcnt_( 0)
{
}

ldndc::output_sink_srv_t::output_sink_srv_t(
                lid_t const &  _object_id)
        : ldndc::output_sink_srv_base_t( _object_id),
        lrcv_( NULL), refcnt_( 0)
{
}

ldndc::output_sink_srv_t::~output_sink_srv_t()
{
    if ( this->refcnt_ > 0)
    {
        CBM_LogError( "[BUG]  reference count for output sink is greater zero at destruct time  [refcnt=",this->refcnt_,",sink=",this->sink_info_.identifier,"]");
    }

    if ( this->lrcv_)
    {
        this->lrcv_->close();
        this->lrcv_->delete_instance();

        this->lrcv_ = NULL;
    }
}


lerr_t
ldndc::output_sink_srv_t::status()
const
{
    if ( this->lrcv_)
    {
        return  this->lrcv_->status();
    }
    return  LDNDC_ERR_OK;
}

#include  "utils/lutils-fs.h"
#include  "io/default-streams.h"
lerr_t
ldndc::output_sink_srv_t::open_stream(
                sink_info_t const *  _sink_info)
{
    if ( this->lrcv_)
    {
        CBM_LogWarn( "reopening output stream not allowed");
        return  LDNDC_ERR_FAIL;
    }

    if ( !_sink_info)
    {
        CBM_LogError( "invalid (NULL) sink information");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( _sink_info->format == "")
    {
        CBM_LogError( "invalid sink format");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    this->sink_info_ = *_sink_info;

    if ( ldndc::sink_is_black_hole( _sink_info))
    {
        /* not opening sink */
        return  LDNDC_ERR_OK;
    }

    oproc_factories_t  ofactory;
    this->lrcv_ = ofactory.construct_landscape_receiver( _sink_info);
    if ( !this->lrcv_)
    {
        CBM_LogError( "failed to create receiver  [sink=",*_sink_info,"]");
        return  LDNDC_ERR_OBJECT_CREATE_FAILED;
    }
    this->lrcv_->set_object_id( this->object_id());

    if ( !ldndc::sink_is_nodev( _sink_info) && !is_standard_sink( _sink_info->name.c_str()))
    {
        std::string  sink_dirname;
        cbm::dirname( &sink_dirname, _sink_info->full_path().c_str());
// sk:dbg        CBM_LogDebug( "dirname=",sink_dirname, "  fullpath=",_sink_info->full_path());
        if ( cbm::path_exists( sink_dirname.c_str()))
        {
            if ( !cbm::is_dir( sink_dirname.c_str()))
            {
                return  LDNDC_ERR_OBJECT_CREATE_FAILED;
            }
        }
        else
        {
            lerr_t  rc_mkdir = cbm::mkdirs( sink_dirname.c_str());
            if ( rc_mkdir)
            {
                CBM_LogError( "creating output directory failed",
                        "  [directory=",sink_dirname,"]");
            }
            else
            {
                CBM_LogInfo( "created output directory",
                        "  [directory=",sink_dirname,"]");
            }
            RETURN_IF_NOT_OK(rc_mkdir);
        }
    }

    return  this->lrcv_->open( _sink_info);
}

lerr_t
ldndc::output_sink_srv_t::close_stream()
{
    if ( this->refcnt_ == 0)
    {
        if ( this->lrcv_)
        {
            this->lrcv_->close();
            this->lrcv_->delete_instance();

            this->lrcv_ = NULL;
        }

        return  LDNDC_ERR_OK;
    }
        return  LDNDC_ERR_FAIL;
}

lerr_t
ldndc::output_sink_srv_t::flush_stream()
{
    if ( this->lrcv_)
    {
        return  this->lrcv_->flush();
    }
    return  LDNDC_ERR_OK;
}


ldndc::sink_handle_srv_t
ldndc::output_sink_srv_t::sink_handle_acquire(
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    sink_handle_srv_t  hdl = sink_handle_srv_t( this->object_id());

    if ( !this->lrcv_)
    {
        hdl.set_receiver( NULL, NULL);
    }
    else
    {
        oproc_receiver_t *  this_receiver =
            this->lrcv_->acquire_receiver( _receiver_descriptor);
        if ( !this_receiver)
        {
            hdl.set_object_id( invalid_lid);
        }
        hdl.set_receiver( this_receiver, this->lrcv_);
    }

#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(refcnt_)
#  pragma omp atomic
#endif
    ++this->refcnt_;
// sk:dbg    CBM_LogDebug( "received request for sink handle  [refcnt=",this->refcnt_,",sink=",this->sink_info_.identifier,"]");

    return  hdl;
}

lerr_t
ldndc::output_sink_srv_t::sink_handle_release(
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    if ( this->lrcv_)
    {
        this->lrcv_->release_receiver( _receiver_descriptor);
    }

#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(refcnt_)
#  pragma omp atomic
#endif
    --this->refcnt_;
// sk:dbg    CBM_LogDebug( "received release for sink handle  [refcnt=",this->refcnt_,",sink=",this->sink_info_.identifier,"]");
    if ( this->refcnt_ < 0)
    {
        CBM_LogError( "[BUG]  reference count for output sink below zero!");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::output_sink_srv_t::update(
        cbm::sclock_t const *  _lclock)
{
    if ( !this->lrcv_)
    {
        /* black hole */
        return  LDNDC_ERR_OK;
    }
    int  rc_update = this->lrcv_->collective_commit_commence( _lclock);
    if ( rc_update)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::output_sink_srv_t::commit(
        cbm::sclock_t const *  _lclock)
{
    if ( !this->lrcv_)
    {
        /* black hole */
        return  LDNDC_ERR_OK;
    }
    int  rc_commit = this->lrcv_->collective_commit_complete( _lclock);
    if ( rc_commit)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::output_sink_srv_t::reset()
{
// sk:todo    int  rc_reset = this->lrcv_->collective_reset();
// sk:todo    if ( rc_reset)
// sk:todo    {
// sk:todo        return  LDNDC_ERR_FAIL;
// sk:todo    }
    return  LDNDC_ERR_OK;
}

