/*!
 * @brief
 *    holds input for single simulation; it opens single
 *    streams or entire projects (prescribed by inputs)
 *
 *    if a work dispatcher is provided, inputs are split
 *    among available nodes.
 *
 * @author
 *    steffen klatt (created on: jul 27, 2011)
 */

#ifndef  LDNDC_IO_PROJECT_H_
#define  LDNDC_IO_PROJECT_H_

#include  "crabmeat-common.h"

#include  "input/ic-pool.h"

#include  "io/input.h"
#include  "io/source-handle-srv.h"

#include  "io/output.h"
#include  "io/sink-handle-srv.h"

#include  "time/cbm_time.h"
#include  "string/cbm_string.h"

#include  "input/ic-srv.h"
#include  "prjfile/cbm_prjfile.h"

#include  <vector>

namespace cbm
{
    class CBM_API project_t : public cbm::server_object_t
    {
        LDNDC_SERVER_OBJECT(project_t)
        public:
            project_t( lid_t const & = invalid_lid /*project id*/);
            ~project_t();

        public: /* INPUT */
            /*!
             * @brief
             *    read input from data source and make
             *    available to input class pool
             *
             * @note
             *    this can take a long time for large inputs, so
             *    use wisely.
             *
             * @returns
             *    stream ID on success, invalid ID @p invalid_lid
             *    otherwise
             */
            lid_t  attach_source( ldndc::source_info_t const *);
// sk:todo            /*!
// sk:todo             * @brief
// sk:todo             *    close a single input stream (do not use once input
// sk:todo             *    has started to be processed/used, i.e. input classes
// sk:todo             *    are referencing readers attached to the stream)
// sk:todo             */
// sk:todo            lerr_t  detach_source(
// sk:todo                    lid_t const & /*stream id*/);

            /*!
             * @brief
             *    make empty project
             */
            void  detach_all_sources();



            /*!
             * @brief
             *    get total number of input streams
             */
            size_t  number_of_open_sources() const;
            /*!
             * @brief
             *    get number of input streams of given type
             */
            size_t  number_of_open_sources( char const * /*type*/) const;

            /*!
             * @brief
             *    get total number of available core blocks
             */
            size_t  number_of_core_blocks() const;
            /*!
             * @brief    
             *    get number of available core blocks of given type
             */
            size_t  number_of_core_blocks( char const * /*type*/) const;

        public: /* OUTPUT */
            /*!
             * @brief
             *    prepare sink for sending output to it
             *
             * @return
             *    sink descriptor
             */
            lid_t  attach_sink(
                    char const * /*sink identifier*/, int /*sink index*/);
            /*!
             * @brief
             *    requests unmounting of all sinks in output
             *    object. this is not garantueed to succeed.
             */
            void  detach_all_sinks();

            /*!
             * @brief
             *    request server-side interface object for sink
             */
            ldndc::sink_handle_srv_t  sink_handle_acquire(
                    cbm::source_descriptor_t const *, char const * /*type*/,
                    lid_t const & /*sink descriptor*/);
            lerr_t  sink_handle_release(
                    lid_t const & /*sink descriptor*/,
                    lreceiver_descriptor_t const & /*receiver descriptor*/);

        public:
            /*!
             * @brief
             *    kick off stream loading. attaches all input streams
             *    specified in project file (input class
             *    sources). note that no previously opened streams are
             *    detached.
             */
            lerr_t  open_project();

// sk:later            /*!
// sk:later             * @brief
// sk:later             *    close project
// sk:later             */
// sk:later            lerr_t  close_project();

        private:
            /* pool of input classes provided by user */
            ldndc::input_class_pool_t  m_icpool;

            /* input subsystem (input reading mechanisms) */
            ldndc::raw_inputs_t  m_inputs;

            /* output subsystem (output writing mechanisms) */
            ldndc::raw_outputs_t  m_outputs;

            /* hide copy constructor and assigment operator */
            project_t( project_t const &);
            project_t &  operator=( project_t const &);

        /* mediator between raw-inputs and input-class-pool */
        public:
            /*!
             * @brief    
             *    reset source state to initial conditions
             */
            lerr_t  reset_io( cbm::source_descriptor_t const *,
                            char const * /*type*/);
            /*!
             * @brief    
             *    trigger collective i/o internal
             *    state reset
             */
            lerr_t  reset_io();

            /*!
             * @brief
             *    trigger source internal state update
             */
            lerr_t  update_io( cbm::source_descriptor_t const *,
                        char const * /*type*/, cbm::sclock_t const &);
            /*!
             * @brief
             *    notify i/o system about a collective
             *    update
             */
            lerr_t  update_io( cbm::sclock_t const &);
            /*!
             * @brief
             *    notify i/o system about a collective
             *    commit
             */
            lerr_t  commit_io( cbm::sclock_t const &);

            /*!
             * @brief
             *    retrieve input class from pool
             */
            ldndc::input_class_srv_base_t const *  get_input_class(
                    cbm::source_descriptor_t const * /*buffer*/,
                    char const * /*type*/) const;
            ldndc::input_class_srv_base_t *  get_input_class(
                    cbm::source_descriptor_t const * /*buffer*/,
                    char const *  /*type*/);

        private:
            lerr_t  try_find_input_(
                cbm::source_descriptor_t const *, char const * /*type*/);

            lerr_t  try_mount_input_class_(
                ldndc::source_handle_srv_base_t *, char const * /*type*/);

        public:
            lid_t  get_source_descriptor(
                    cbm::source_descriptor_t * /*buffer*/,
                    cbm::source_descriptor_t const *  /*kernel info*/,
                    char const *  /*type*/) const;
            /*!
             * @brief
             *    retrieve input class from pool but interpret
             *    id as kernel id rather than core block id (more
             *    correct: id is setup id instead of <type>'s id)
             */
            ldndc::input_class_srv_base_t const *  get_input_class_indirect(
                    cbm::source_descriptor_t const * /*kernel info*/,
                    char const *  /*type*/) const;
            /*!
             * @brief
             *    retrieve global input class from pool
             */
            ldndc::input_class_global_srv_base_t const *  get_input_class_global(
                    lid_t const &  /*stream id*/,
                    char const *  /*type*/) const;
            ldndc::input_class_global_srv_base_t *  get_input_class_global(
                    lid_t const &  /*stream id*/,
                    char const *  /*type*/);

        public:
            std::vector< cbm::source_descriptor_t >
                    object_id_list( char const * /*type*/) const;

        private:
            lerr_t  m_preparesourcehandle( ldndc::source_handle_srv_base_t *,
                cbm::source_descriptor_t const *);
    };
} /* namespace cbm */


#endif  /*  !LDNDC_IO_PROJECT_H_  */

