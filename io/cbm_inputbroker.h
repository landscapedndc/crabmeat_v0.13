/*!
 * @brief
 *    serves as a getter for input classes
 *    available within the given project
 *
 * @author
 *    steffen klatt (created on: jan 20, 2014)
 */

#ifndef  LDNDC_IO_INPUTCLASS_BROKER_H_
#define  LDNDC_IO_INPUTCLASS_BROKER_H_

#include  "crabmeat-common.h"

namespace  cbm {
    class  project_t; }

namespace  ldndc
{
    class  input_class_srv_base_t;
    class  input_class_global_srv_base_t;

    class  input_class_broker_t
    {
        public:
            input_class_broker_t();
            input_class_broker_t( cbm::project_t const * /*project*/,
                cbm::source_descriptor_t const * /*source descriptor*/);


            input_class_global_srv_base_t const *  get_input_class_global(
                lid_t const & /*stream id*/, char const * /*type*/) const;

            input_class_srv_base_t const *  get_input_class_indirect(
                char const * /*type*/) const;

            cbm::source_descriptor_t const *  source_descriptor()
                    const { return  &this->sd; }
        private:
            cbm::project_t const *  p;
        public:
            cbm::source_descriptor_t  sd;
    };
}  /*  namespace ldndc  */

#endif  /*  !LDNDC_IO_INPUTCLASS_BROKER_H_  */

