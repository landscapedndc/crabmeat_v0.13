/*!
 * @brief
 *    virtual reader implementation
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#include  "io/ifactory.h"
#include  "log/cbm_baselog.h"

#include  "io/fhandler/fhandlers.h"
#include  "io/data-provider/ireaders/ireaders.h"
#ifdef  _HAVE_UNITS_CONVERSION
#  include  "io/data-modifier/iuconv/iuconvs.h"
#endif
#ifdef  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK
#  include  "io/data-modifier/iaggr/iaggrs.h"
#endif
#ifdef  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK
#  include  "io/data-provider/isynths/isynths.h"
#endif


ldndc::iproc_factories_t::iproc_factories_t()
    { }

ldndc::iproc_factories_t::~iproc_factories_t()
    { }


    /* format handlers */
bool
ldndc::iproc_factories_t::ihandler_available( char const *  _if)
const
{
    crabmeat_assert( _if != NULL);
    if ( find_fhandler_factory( _if) != NULL)
        { return true; }
    return false;
}
ldndc::iobject_handler_base_t *
ldndc::iproc_factories_t::construct_ihandler( char const *  _if)
const
{
    if ( this->ihandler_available( _if))
    {
        return static_cast< ldndc::iobject_handler_base_t * >(
                    find_fhandler_factory( _if)->construct());
    }
    CBM_LogWarn( "no such format handler  [format=",_if,"]");
    return  NULL;
}
void
ldndc::iproc_factories_t::destroy_ihandler(
        ldndc::iobject_handler_base_t *  _ih)
const
{
    if ( !_ih)
        { return; }
    find_fhandler_factory( _ih->io_format_type())->destroy( _ih);
}


    /* input readers */
bool
ldndc::iproc_factories_t::landscape_ireader_available(
        char const *  _ic, char const *  _if)
const
{
    if ( find_ireader_factory( 'L', _if, _ic))
        { return true; }
    return false;
}
ldndc::iproc_landscape_reader_base_t *
ldndc::iproc_factories_t::construct_landscape_ireader(
        char const *  _ic, char const *  _if)
const
{
    if ( this->landscape_ireader_available( _ic, _if))
    {
        return static_cast< ldndc::iproc_landscape_reader_base_t * >(
                    find_ireader_factory( 'L', _if, _ic)->construct());
    }
    CBM_LogWarn( "no such landscape input reader  [format=",_if,",class=",_ic,"]");
    return NULL;
}
void
ldndc::iproc_factories_t::destroy_landscape_ireader(
        ldndc::iproc_landscape_reader_base_t *  _rdr)
const
{
    if ( !_rdr)
        { return; }
    find_ireader_factory( 'L', _rdr->io_format_type(),
                _rdr->input_class_type())->destroy( _rdr);
}

bool
ldndc::iproc_factories_t::ireader_available(
        char const *  _ic, char const *  _if)
const
{
    if ( find_ireader_factory( 'C', _if, _ic))
        { return true; }
    return false;
}
ldndc::iproc_reader_base_t *
ldndc::iproc_factories_t::construct_ireader(
        char const *  _ic, char const *  _if)
const
{
    if ( this->ireader_available( _ic, _if))
    {
        return static_cast< ldndc::iproc_reader_base_t * >(
                find_ireader_factory( 'C', _if, _ic)->construct());
    }
    CBM_LogWarn( "no such input reader  [format=",_if,",class=",_ic,"]");
    return  NULL;
}
void
ldndc::iproc_factories_t::destroy_ireader(
        ldndc::iproc_reader_base_t *  _rdr)
const
{
    if ( !_rdr)
        { return; }
    find_ireader_factory( 'C', _rdr->io_format_type(),
                _rdr->input_class_type())->destroy( _rdr);
}

#ifdef  _HAVE_UNITS_CONVERSION
bool
ldndc::iproc_factories_t::iuconverter_available(
        char const * /*type*/)
const
{
}
iproc_iuconverter_base_t *
ldndc::iproc_factories_t::construct_iuconverter(
        char const * /*type*/)
const
{
}
void
ldndc::iproc_factories_t::destroy_iuconverter(
        iproc_iuconverter_base_t *)
const
{
}
#endif  /*  _HAVE_UNITS_CONVERSION  */

#ifdef  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK
bool
ldndc::iproc_factories_t::iaggregator_available(
        char const *  _ic)
const
{
    crabmeat_assert( _ic);
    if ( find_iaggregator_factory( _ic) != NULL)
        { return true; }
    return false;
}
ldndc::iproc_iaggregator_base_t *
ldndc::iproc_factories_t::construct_iaggregator(
        char const *  _ic)
const
{
    if ( this->iaggregator_available( _ic))
    {
        return static_cast< ldndc::iproc_iaggregator_base_t * >(
                    find_iaggregator_factory( _ic)->construct());
    }
    CBM_LogWarn( "no such input aggregator  [class=",_ic,"]");
    return  NULL;
}
void
ldndc::iproc_factories_t::destroy_iaggregator(
        ldndc::iproc_iaggregator_base_t *  _iaggr)
const
{
    if ( !_iaggr)
        { return; }
    find_iaggregator_factory( _iaggr->input_class_type())->destroy( _iaggr);
}
#endif  /*  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK  */

#ifdef  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK
bool
ldndc::iproc_factories_t::isynthesizer_available(
        char const *  _ic)
const
{
    crabmeat_assert( _ic);
    if ( find_isynthesizer_factory( _ic) != NULL)
        { return true; }
    return false;
}
ldndc::iproc_isynthesizer_base_t *
ldndc::iproc_factories_t::construct_isynthesizer(
        char const *  _ic)
const
{
    if ( this->isynthesizer_available( _ic))
    {
        return static_cast< ldndc::iproc_isynthesizer_base_t * >(
                    find_isynthesizer_factory( _ic)->construct());
    }
    CBM_LogWarn( "no such input synthesizer  [class=",_ic,"]");
    return  NULL;
}
void
ldndc::iproc_factories_t::destroy_isynthesizer(
        ldndc::iproc_isynthesizer_base_t *  _isynth)
const
{
    if ( !_isynth)
        { return; }
    find_isynthesizer_factory( _isynth->input_class_type())->destroy( _isynth);
}
#endif  /*  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK  */

ldndc::iproc_provider_t *
ldndc::iproc_factories_t::construct_iproc_stack(
        ldndc::iproc_provider_t *  _ip, char const *  _ic)
const
{
    CRABMEAT_FIX_UNUSED(_ic);

    ldndc::iproc_provider_t *  input_proc = _ip;
    CRABMEAT_FIX_UNUSED(input_proc);
#ifdef  _HAVE_UNITS_CONVERSION
#  error  input data unit converters are not implemented
#endif  /*  _HAVE_UNITS_CONVERSION  */

#ifdef  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK
    if ( this->iaggregator_available( _ic))
    {
        CBM_LogDebug( "providing aggregator service .. [",_ic,"]");
        ldndc::iproc_iaggregator_base_t *  iaggr = this->construct_iaggregator( _ic);
        if ( !iaggr)
        {
            if ( _ip != input_proc)
                { input_proc->delete_instance(); }
            CBM_LogError( "failed to instantiate input aggregator object  [class=",_ic,"]");
            return  NULL;
        }
        iaggr->set_provider( input_proc);
        iaggr->set_object_id( input_proc->object_id());
        input_proc = static_cast< ldndc::iproc_provider_t * >( iaggr);
    }
    else
    {
        /* silently ignore */
    }
#endif  /*  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK */

#ifdef  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK
    if ( this->isynthesizer_available( _ic))
    {
        CBM_LogDebug( "providing synthesizer service ..  [",_ic,"]");
        ldndc::iproc_isynthesizer_base_t *  isynth = this->construct_isynthesizer( _ic);
        if ( !isynth)
        {
            if ( _ip != input_proc)
                { input_proc->delete_instance(); }
            CBM_LogError( "failed to instantiate input synthesizer object  [class=",_ic,"]");
            return  NULL;
        }
        isynth->set_provider( input_proc);
        isynth->set_object_id( input_proc->object_id());
        input_proc = static_cast< ldndc::iproc_provider_t * >( isynth);
    }
    else
    {
        /* silently ignore */
    }
#endif  /*  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK  */

    return  input_proc;
}
void
ldndc::iproc_factories_t::destroy_iproc_stack(
        ldndc::iproc_provider_t *  _ip)
const
{
    if ( !_ip)
        { return; }

// TODO  according to order of construction, delete using appropriate member
// shortcut for now ...
    _ip->delete_instance();
}

