/*!
 * @brief
 *    server-side interface to sink objects
 *
 * @authors
 *    steffen klatt (created on: nov 03, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_IO_SINKHANDLE_SRV_H_
#define  LDNDC_IO_SINKHANDLE_SRV_H_

#include  "crabmeat-common.h"

#include  "io/outputtypes.h"
#include  "io/data-receiver/receiver.h"

namespace ldndc {
struct  oproc_receiver_t;
struct  oproc_landscape_receiver_t;
class  CBM_API  sink_handle_srv_t  :  public  cbm::server_object_t
{
    LDNDC_SERVER_OBJECT(sink_handle_srv_t)
    public:
        sink_handle_srv_t();
        sink_handle_srv_t(
                lid_t const &);

        ~sink_handle_srv_t();

        /* aliases */
        lid_t  sink_descriptor() const;
        lreceiver_descriptor_t  receiver_descriptor() const;

    public:
        lerr_t  status() const;
        bool  is_devnull() const { return  this->lrcv_ == NULL; }

        lerr_t  flush_stream();

        /* static predefined layout */
        lerr_t  set_fixed_layout(
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*entities&layout*/);

        /* write record */
        lerr_t  write_record(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);

    public:
        /* set once and for all time a reference to
         * the output writers. should be called by the
         * creator of this object */
        void  set_receiver(
                oproc_receiver_t *,
                oproc_landscape_receiver_t *);
        bool  receiver_requires_input(
                input_class_set_t *);
        lerr_t  receiver_pass_required_input(
                input_class_set_t *);


    private:
        /* output writer (not owner) */
        oproc_receiver_t *  rcv_;
        /* landscape output writer (not owner) */
        oproc_landscape_receiver_t *  lrcv_;
};
}


#endif  /*  !LDNDC_IO_SINKHANDLE_SRV_H_  */

