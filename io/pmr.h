/*!
 * @brief
 *    base class for data provider, modifier and receiver
 *
 * @author
 *    steffen klatt (created on: nov 16, 2013)
 */

#ifndef  LDNDC_IO_PMR_H_
#define  LDNDC_IO_PMR_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

namespace ldndc {

struct  CBM_API  ioproc_base_t  :  cbm::server_object_t
{
    ioproc_base_t();
    virtual  ~ioproc_base_t() = 0;
};

struct  CBM_API  iproc_landscape_base_t  :  ioproc_base_t
{
    iproc_landscape_base_t();
    virtual  ~iproc_landscape_base_t() = 0;
};
struct  CBM_API  iproc_base_t  :  ioproc_base_t
{
    virtual  int  data_available() const = 0;

    iproc_base_t();
    virtual  ~iproc_base_t() = 0;
};

struct  CBM_API  oproc_landscape_base_t  :  ioproc_base_t
{
    oproc_landscape_base_t();
    virtual  ~oproc_landscape_base_t() = 0;
};
struct  CBM_API  oproc_base_t  :  ioproc_base_t
{
    oproc_base_t();
    virtual  ~oproc_base_t() = 0;
};

struct  CBM_API  iproc_collective_operations_interface_t
{
    /*!
     * @brief
     *    notifies the landscape input processor, that
     *    input processors are ready to update their content.
     *
     *    a call to this function is required if the
     *    input processor acts as a consumer of some output
     *    processor.
     */
    virtual  int  collective_update_commence(
            cbm::sclock_t const *) = 0;

    /*!
     * @brief
     *    notifies the landscape input processor, that all
     *    input processors have completed their reload update.
     *
     *    a call to this function is required if the
     *    input processor acts as a consumer of some output
     *    processor.
     */
    virtual  int  collective_update_complete(
            cbm::sclock_t const *) = 0;


    iproc_collective_operations_interface_t();
    virtual  ~iproc_collective_operations_interface_t() = 0;
};

struct  sink_client_t;
struct  CBM_API  oproc_collective_operations_interface_t
{
    virtual  int  collective_commit_commence(
            cbm::sclock_t const *) = 0;

    /*!
     * @brief
     *    notifies the landscape output processor, that the given
     *    output processor has completed sending data (that means,
     *    after this call control passes back to runtime
     *    control)
     *
     *    a call to this function is required if the output processor
     *    serves as a produces for some input processor.
     */
    virtual  int  collective_commit_complete(
            cbm::sclock_t const *) = 0;

    oproc_collective_operations_interface_t();
    virtual  ~oproc_collective_operations_interface_t() = 0;
};

/*!
 * @brief
 *    construct i/o processing object
 */
struct  CBM_API  ioproc_factory_base_t
{
    ioproc_factory_base_t();
    virtual  ~ioproc_factory_base_t() = 0;

    virtual  ioproc_base_t *  construct() const = 0;
    virtual  void  destroy( ioproc_base_t *) const = 0;
};
template < typename  _ioproc >
struct  ioproc_factory_t  :  public  ioproc_factory_base_t
{
    typedef  _ioproc  ioproc_t;

    ioproc_factory_t();
    virtual  ~ioproc_factory_t();

    ioproc_base_t *  construct()
    const
    {
        return  static_cast< ioproc_base_t * >(  ioproc_t::new_instance());
    }
    void  destroy( ioproc_base_t *  _iop)
    const
    {
        if ( !_iop)
            { return; }
        static_cast< ioproc_t * >(  _iop)->delete_instance();
    }
};
template < typename  _ioproc >
ioproc_factory_t< _ioproc >::ioproc_factory_t()
        : ldndc::ioproc_factory_base_t()
{
}
template < typename  _ioproc >
ioproc_factory_t< _ioproc >::~ioproc_factory_t()
{
}

}

#endif  /*  !LDNDC_IO_PMR_H_  */

