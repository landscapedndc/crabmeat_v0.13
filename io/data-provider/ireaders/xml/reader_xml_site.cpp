/*!
 * @author
 *  Steffen Klatt (created on: Jul 8, 2011)
 *  David Kraus
 */

#ifndef  LDNDC_IO_IREADER_SITE_XML_CPP_
#define  LDNDC_IO_IREADER_SITE_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_site.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
namespace ldndc {
char const *  iproc_landscape_reader_site_xml_t::ROOT_TAGS[iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcsite", "site"};

LDNDC_SERVER_OBJECT_DEFN(iproc_reader_site_xml_t)
iproc_reader_site_xml_t::iproc_reader_site_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_site_t()
{
}
iproc_reader_site_xml_t::~iproc_reader_site_xml_t()
{
}


double
iproc_reader_site_xml_t::get_saturated_hydraulic_conductivity_bottom()
const
{
    return  XML_ATTRIBUTE_VALUE( "sksbottom", invalid_dbl, "general", "misc");
}


double
iproc_reader_site_xml_t::get_water_table_depth()
const
{
    double  this_watertable = XML_ATTRIBUTE_VALUE( "watertable", invalid_dbl, "general", "misc");
    if ( cbm::is_invalid( this_watertable))
    {
        this_watertable = XML_ATTRIBUTE_VALUE( "wtdepth", invalid_dbl, "general", "misc");
    }
    return  this_watertable;
}


/* soil -> general */
double
iproc_reader_site_xml_t::get_soil_organic_carbon_content_05()
const
{
    return  XML_ATTRIBUTE_VALUE( "corg5", invalid_dbl, "soil", "general");
}

double
iproc_reader_site_xml_t::get_soil_organic_carbon_content_30()
const
{
    return  XML_ATTRIBUTE_VALUE( "corg30", invalid_dbl, "soil", "general");
}

double
iproc_reader_site_xml_t::get_soil_litter_height()
const
{
    double  this_litter_height = XML_ATTRIBUTE_VALUE( "litterheight", invalid_dbl, "soil", "general");
    /* backward compatibility */
    if ( cbm::is_invalid( this_litter_height))
    {
        this_litter_height = XML_ATTRIBUTE_VALUE( "lheight", invalid_dbl, "soil", "general");
    }
    return  this_litter_height;
}

std::string
iproc_reader_site_xml_t::get_soil_type()
const
{
    cbm::nstr  soil_s( XML_ATTRIBUTE_VALUE( "soil", invalid_string, "soil", "general"));
    /* backward compatibility */
    if ( cbm::is_invalid( soil_s.c_str()))
    {
        soil_s = XML_ATTRIBUTE_VALUE( "stype", invalid_string, "soil", "general");
    }
    return  soil_s.str();
}

std::string
iproc_reader_site_xml_t::get_humus_type()
const
{
    cbm::nstr  humus_s( XML_ATTRIBUTE_VALUE( "humus", invalid_string, "soil", "general"));
    return  humus_s.str();
}

lerr_t
iproc_reader_site_xml_t::soil_use_history(
        std::string *  _soil_use)
const
{
    crabmeat_assert( _soil_use);
    cbm::nstr  soil_use_s( XML_ATTRIBUTE_VALUE( "usehistory", invalid_string, "soil", "general"), CNAME_STRIP|CNAME_STRIP_TO_LOWER);
    _soil_use->assign( soil_use_s.str());

    return  LDNDC_ERR_OK;
}


/* soil -> layers -> layer */
size_t
iproc_reader_site_xml_t::get_number_of_strata()
const
{
    size_t  r( invalid_size);

    if ( invalid_size == r) {

        int  ri( invalid_int);
        (void)helper_xml_get_list_eq_size_( 
                helper_xml_walk_path_( this->get_handle(), 2, "soil", "layers"),
                "layer",
                &ri, NULL);
        if ( ri == invalid_int) {
            r = invalid_size;
        } else {
            r = (size_t)ri;
        }
    }
    return  r;
}

lerr_t
iproc_reader_site_xml_t::get_strata_height(
        double  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  this->read_strata_property( "depth", _arr, _n, _r, _s);
}

lerr_t
iproc_reader_site_xml_t::get_strata_splitheight(
    double  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  this->read_strata_property( "height", _arr, _n, _r, _s);
}

lerr_t
iproc_reader_site_xml_t::get_strata_split(
        int  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  helper_xml_get_list_eq_(
                        helper_xml_walk_path_( this->get_handle(), 2, "soil", "layers"),
                        "layer", NULL, "split",
                        NULL, invalid_size,
                        _arr, _n, 1,
            0 /*offset*/, 1 /*stride*/,
                   _r, _s,
            NULL, NULL);
}

lerr_t
iproc_reader_site_xml_t::read_strata_property(
               char const *  _property_name, double  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  helper_xml_get_list_eq_( 
                        helper_xml_walk_path_( this->get_handle(), 2, "soil", "layers"),
                        "layer", NULL, _property_name,
                        NULL, invalid_size,
                        _arr, _n, 1,
            0 /*offset*/, 1 /*stride*/,
            _r, _s,
            NULL, NULL);
}

/* used to map strata property name to xml attribute names */
iproc_reader_site_xml_t::string_dict_t const  iproc_reader_site_xml_t::STRATUM_PROPERTY_NAMES_XML[ldndc::site::STRATUM_PROPERTIES_CNT] =
{
    {"ph","ph"}, {"soil_skeleton","scel"}, {"bulk_density","bd"}, {"hydraulic_conductivity","sks"},
    {"organic_carbon","corg"}, {"organic_nitrogen","norg"},
    {"clay","clay"}, {"sand","sand"},
    {"wilting_point","wcmin"}, {"field_capacity","wcmax"},
    {"iron_content","iron",},
    {"maximum_water_filled_pore_space","wfps_max"},
    {"minimum_water_filled_pore_space","wfps_min"},
    {"total_pore_space","porosity"},
    {"macropores","macropores"},
    {"vangenuchten_n", "vangenuchten_n"},
    {"vangenuchten_alpha", "vangenuchten_alpha"},
    {"wc_init","wc_init"},
    {"temp_init","temp_init"},
    {"nh4_init","nh4_init"},
    {"no3_init","no3_init"},
    {"don_init","don_init"},
    {"doc_init","doc_init"},
    {"pom_init","pom_init"},
    {"aorg_init","aorg_init"}
};
lerr_t
iproc_reader_site_xml_t::get_strata_properties(
        double  _buf[], size_t  _n_strata, size_t  _n_properties,
        ldndc_string_t const  _property_names[])
const
{
    double *  buf = _buf;
    for ( size_t  p = 0;  p < _n_properties;  ++p)
    {
        unsigned int  prop_at = invalid_uint;
        for ( size_t  p_xml = 0;  p_xml < _n_properties;  ++p_xml)
        {
            if ( cbm::is_equal(
                STRATUM_PROPERTY_NAMES_XML[p_xml].key, _property_names[p]))
            {
                prop_at = static_cast< unsigned int >( p_xml);
                break;
            }
        }
        if ( prop_at == invalid_uint)
        {
            CBM_LogError( "iproc_reader_site_xml_t::get_strata_properties(): ",
                    "unknown strata property  [property=",_property_names[p],"]");
            return  LDNDC_ERR_FAIL;
        }

        lerr_t  rc_readprop = this->read_strata_property(
                STRATUM_PROPERTY_NAMES_XML[prop_at].mapped_key, buf+p*_n_strata, _n_strata, NULL, NULL);
        if ( rc_readprop)
        {
            CBM_LogError( "iproc_reader_site_xml_t::get_strata_properties(): ",
                    "failed to read strata property  [property=",_property_names[p],",key=",STRATUM_PROPERTY_NAMES_XML[prop_at].key,"]");
            return  LDNDC_ERR_FAIL;
        }
    }
    return  LDNDC_ERR_OK;
}

/*  *****  SITE XML READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_site_xml_t)
iproc_landscape_reader_site_xml_t::iproc_landscape_reader_site_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_site_t()
{
}


iproc_landscape_reader_site_xml_t::~iproc_landscape_reader_site_xml_t()
{
}


}

#endif  /*  !LDNDC_IO_IREADER_SITE_XML_CPP_  */

