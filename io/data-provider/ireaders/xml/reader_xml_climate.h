/*!
 * @brief
 *    concrete xml readers (climate)
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  CBM_IREADER_XML_CLIMATE_H_
#define  CBM_IREADER_XML_CLIMATE_H_

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/iif/iif_climate.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_climate_xml_t  :  public  iproc_landscape_reader_xml_t,  public  iproc_landscape_interface_climate_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_climate_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_landscape_interface_climate_t::input_class_type(); }

        static  char const *  ROOT_TAGS[IMODE_CNT];
        char const *  root_tag( input_stream_mode_e  _imode = IMODE_CNT)
        const
        {
            crabmeat_assert( _imode < IMODE_CNT);
            return  ROOT_TAGS[_imode];
        }

        char const *  core_tag() const { return  core_tag_(); }
    private:
        static  char const *  core_tag_() { return  iproc_landscape_reader_climate_xml_t::ROOT_TAGS[IMODE_SINGLE]; }

    public:
        iproc_landscape_reader_climate_xml_t();
        virtual  ~iproc_landscape_reader_climate_xml_t();

        std::string  time() const;
};
class  CBM_API  iproc_reader_climate_xml_t  :  public  iproc_reader_xml_t,  public  iproc_interface_climate_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_climate_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_climate_t::input_class_type(); }

        iproc_reader_climate_xml_t();
        virtual  ~iproc_reader_climate_xml_t();

    public:
        double  get_latitude() const;
        double  get_longitude() const;
        double  get_elevation() const;

        double  get_cloudiness() const;
        double  get_rainfall_intensity() const;
        double  get_wind_speed() const;

        double  get_annual_precipitation() const;
        double  get_temperature_average() const;
        double  get_temperature_amplitude() const;

    public:
        lerr_t  preset(
                char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read(
                element_type [], size_t, size_t,
                char const * [], size_t,
                int, int,
                int *, int *);

        private:
        /* stores position of record after last read */
        core_iobject_handle_t const *  lnode0_;
        core_iobject_handle_t const *  lnode_;
        unsigned int  lpos0_;
        unsigned int  lpos_;
};
}

#endif  /*  !CBM_IREADER_XML_CLIMATE_H_  */

