
#ifndef  LDNDC_IO_IREADER_EVENT_XML_CPP_
#define  LDNDC_IO_IREADER_EVENT_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_event.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
#include  "time/cbm_time.h"

#include  "event/events.h"
#include  "event/eventtypes.h"


namespace ldndc {
char const *  iproc_landscape_reader_event_xml_t::ROOT_TAGS[iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcevent", "event"};
}

#define  EV_ATTRS_READER_HEADER(__type__)                                    \
    CRABMEAT_FIX_UNUSED(_r);CRABMEAT_FIX_UNUSED(_s);                                \
    crabmeat_assert(_r && _s);                                            \
    iproc_handle_xml_types_t::core_iobject_handle_t const *  ev_node( helper_xml_walk_path_( _ev_node, 1, STRINGIFY(__type__)));    \
    if ( !ev_node)                                                \
    {                                                    \
        CBM_LogError( "no attribute block in input stream  [event type=",STRINGIFY(__type__),"]");        \
        return  LDNDC_ERR_FAIL;                                        \
    }                                                    \
    event::__event_attrib_class_name(__type__) *  attrs( static_cast< event::__event_attrib_class_name(__type__) * >( _attrs));    \
    event::__event_attrib_class_name(__type__) const *  attrs_defaults(                    \
                static_cast< event::__event_attrib_class_name(__type__) const * >( _attrs_default))/*;*/

#define  CHECK_ELEMENT(__elem__)                                        \
    ( NULL != helper_xml_walk_path_( ev_node, 1, __elem__))

#define  EV_ATTR_(__dst__,__attr__,__default__)                                    \
    __dst__ = XML_ATTRIBUTE_VALUE_AT( ev_node, __attr__, __default__, NULL);                \
    /*if ( ! cbm::is_equal( __dst__, __default__)) {*/                            \
    /*    ++*_r;*/                                            \
    /*}*/                                                    \
    ++*_s/*;*/

#define  EV_ATTR_LU_(__dst__,__attr__,__default__,__lu_case__)                            \
{                                                        \
    /* a lot of copying... */                                        \
    cbm::nstr  nstr_tmp( XML_ATTRIBUTE_VALUE_AT( ev_node, __attr__, __default__, NULL),            \
            CNAME_STRIP|(__lu_case__));                                \
    __dst__ = nstr_tmp.str();                                        \
}

#include  "io/data-provider/ireaders/shared/event/ireader_event.h"


/* expands to default event attribute reader handler name */
#define  __EV_READER_HANDLER_NAME(__type__)  event_helper_read_attribute_ ## __type__ ## _xml
/* expands to full default event attribute reader handler signature */
#define  __EV_READER_HANDLER_SIGNATURE(__type__)                                \
    lerr_t                                                    \
    __EV_READER_HANDLER_NAME(__type__)(                                    \
        event::event_attribute_t *  _attrs,                                \
        event::event_attribute_t const *  _attrs_default,                            \
        iproc_handle_xml_types_t::core_iobject_handle_t const *  _ev_node,                \
        int *  _r,  int *  _s)

/* holds the list of all event attribute readers */
#include  "io/data-provider/ireaders/xml/reader_xml_event/reader_xml_events.cpp"
#undef  EV_ATTR_
#undef  EV_ATTR_LU_
#undef  EV_ATTR_L_
#undef  EV_ATTR_U_
#undef  EV_ATTR
#undef  EV_ATTR_L
#undef  EV_ATTR_U
#undef  EV_ATTRS_READER_HEADER
#undef  CHECK_ELEMENT


namespace ldndc {

/*  *****  EVENT XML CORE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_event_xml_t)
iproc_reader_event_xml_t::iproc_reader_event_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_event_t()
{
    for ( size_t  k = 0;  k < event::EVENT_CNT;  ++k)
    {
        event_helper_attribute_handlers[k] = NULL;
    }

    /* set event attribute reader handlers (see reader_xml_events.cpp) */
    __EV_READER_HANDLERS_SET
}
#undef  __EV_READER_HANDLERS_SET
#undef  __EV_READER_HANDLER_SIGNATURE
#undef  __EV_READER_HANDLER_NAME
#undef  __EV_READER_HANDLER_ADD
iproc_reader_event_xml_t::~iproc_reader_event_xml_t()
{
}


lerr_t
iproc_reader_event_xml_t::get_event_attributes(
        event::event_attribute_t *  _attrs, event::event_attribute_t const *  _attrs_default,
        event::event_info_t *  _ev_info,
        int *  _r, int *  _s)
const
{
    if ( ! _ev_info->r_data)
           {
        CBM_LogError( "attempt to read event attribute for null node");
        return  LDNDC_ERR_FAIL;
    }

// sk:chgd    if ( ! (( _ev_info->type < event::EVENT_CNT) && (event_helper_attribute_handlers[_ev_info->type])))
// sk:chgd           {
// sk:chgd        CBM_LogError( "no helper function for given event type");
// sk:chgd        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
// sk:chgd    }
    if ( ! event_helper_attribute_handlers[_ev_info->type])
    {
        /* assume event without attributes */
        return  LDNDC_ERR_OK;
    }

    int  r( 0), s( 0);
    lerr_t  rc = event_helper_attribute_handlers[_ev_info->type](
            _attrs, _attrs_default, (core_iobject_handle_t const *)_ev_info->r_data, &r, &s);

    if ( _r) *_r = r;
    if ( _s) *_s = s;

    return  rc;
}




char const *  iproc_reader_event_xml_t::ROTATION_TAG = "rotation";
char const *  iproc_reader_event_xml_t::EVENT_TAG = "event";

void
iproc_reader_event_xml_t::attach_rotations_(
        cbm::event_info_tree_t *  _ev_tree, cbm::event_info_tree_t::iterator_t  _ev_tree_iter,
        core_iobject_handle_t const *  _r_node, core_iobject_handle_t const *  _e_node,
        int  _load_limit,
        int *  _r, int *  _s)
{
    this->last_loaded.hit_eof = 0;
    if ( _e_node)
           {
        /* attach events at current level */
        lerr_t  rc_attach = attach_events_( _ev_tree, _ev_tree_iter, _e_node, _load_limit, _r, _s);
        if ( rc_attach != LDNDC_ERR_CONT_ITERATION)
        {
            return;
        }
    }

    if ( !_r_node)
           {
        this->last_loaded.hit_eof = 1;
        return;
    }

    event::event_info_t  ev_info( event::MGNT_ROTATION);
    ev_info.t_exec_s = XML_ATTRIBUTE_VALUE_AT( _r_node, "time", ldndc_empty_string, NULL);

    cbm::event_info_tree_t::iterator_t  sub_ev_tree_iter = _ev_tree->append_child( _ev_tree_iter, ev_info);

    /* attach child rotation nodes of current rotation node */
    attach_rotations_( _ev_tree, sub_ev_tree_iter, _r_node->FirstChildElement( ROTATION_TAG), _r_node->FirstChildElement( EVENT_TAG), _load_limit, _r, _s);
    /* attach sibling rotation nodes of current rotation node */
    attach_rotations_( _ev_tree, _ev_tree_iter, _r_node->NextSiblingElement( ROTATION_TAG), NULL, _load_limit, _r, _s);
}

lerr_t
iproc_reader_event_xml_t::attach_events_(
        cbm::event_info_tree_t *  _ev_tree, cbm::event_info_tree_t::iterator_t &  _ev_tree_iter,
        core_iobject_handle_t const *  _r_node,
        int  _load_limit,
        int *  _r, int * _s)
{
    if ( !_r_node)
	{
        return  LDNDC_ERR_CONT_ITERATION;
    }
    if ( _load_limit == 0)
    {
        return  LDNDC_ERR_STOP_ITERATION;
    }
    int const  load_limit = _load_limit - 1;
	core_iobject_handle_t const *  e_node( _r_node);

	for ( ; e_node; e_node = e_node->NextSiblingElement( EVENT_TAG))
	{
        cbm::nstr  ev_type_s( 
                XML_ATTRIBUTE_VALUE_AT( e_node, "type", std::string( event::EVENT_NAMES[event::EVENT_NONE]), NULL),
                CNAME_STRIP|CNAME_STRIP_TO_LOWER);

        unsigned int  ev_type( event::EVENT_CNT);
        cbm::find_index( ev_type_s.c_str(), event::EVENT_NAMES, event::EVENT_CNT, &ev_type);

        ++*_s;
        if ( ev_type == event::EVENT_CNT)
		{
            ev_type = event::EVENT_NONE;
            CBM_LogWarn( "unknown event, skipping  [event=",ev_type_s.c_str(),"]");
            continue;
		}
        ++*_r;

// sk:dbg        CBM_LogDebug( "reading event \"",ev_type_s.c_str(),"\"");

        event::event_info_t  ev_info((event::event_type_e)ev_type);

        ev_info.t_exec_s = XML_ATTRIBUTE_VALUE_AT( e_node, "time", ldndc_empty_string, NULL);
        ev_info.r_data = (event::reader_eventhandle_t)e_node;

        _ev_tree->append_child( _ev_tree_iter, ev_info);

        if (( _load_limit > 0) && ( load_limit < *_r))
        {
            this->last_loaded.evn = e_node;
            this->last_loaded.rot = _r_node;

            return  LDNDC_ERR_STOP_ITERATION;
        }
    }

    this->last_loaded.hit_eof = 1;
    return  LDNDC_ERR_CONT_ITERATION;
}

int
iproc_reader_event_xml_t::readahead_size()
const
{
    return  XML_ATTRIBUTE_VALUE( "readaheadsize", this->invalid_readahead_size(), NULL);
}

lerr_t
iproc_reader_event_xml_t::get_event_tree(
        cbm::event_info_tree_t *  _ev_tree_root,
        int  _load_limit,
        int *  _r, int *  _s)
{
    if ( !_ev_tree_root)
    {
        return  LDNDC_ERR_FAIL;
    }

    if ( !this->data_available())
    {
        return  LDNDC_ERR_INPUT_EXHAUSTED;
    }

    int  r = 0;
    int  s = 0;

    /* kick off the event node attaching */
    core_iobject_handle_t const *  rot_0 = this->last_loaded.rot;
    rot_0 = ( rot_0) ? rot_0->NextSiblingElement( ROTATION_TAG) : this->get_handle()->FirstChildElement( ROTATION_TAG);
    core_iobject_handle_t const *  evn_0 = this->last_loaded.evn;
    evn_0 = ( evn_0) ? evn_0->NextSiblingElement( EVENT_TAG) : this->get_handle()->FirstChildElement( EVENT_TAG);

    this->attach_rotations_( _ev_tree_root, _ev_tree_root->begin(), rot_0, evn_0, _load_limit, &r, &s);

    if ( _r) { *_r = r; }
    if ( _s) { *_s = s; }

    return  LDNDC_ERR_OK;
}

int
iproc_reader_event_xml_t::data_available()
const
{
    return  ( this->last_loaded.hit_eof == 0) ? 1 : 0;
}


/*  *****  EVENT XML READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_event_xml_t)
iproc_landscape_reader_event_xml_t::iproc_landscape_reader_event_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_event_t()
{
}


iproc_landscape_reader_event_xml_t::~iproc_landscape_reader_event_xml_t()
{
}


std::string
iproc_landscape_reader_event_xml_t::time()
const
{
        return  XML_ATTRIBUTE_VALUE( "time", ldndc_empty_string, "global");
}

#undef  __EV_READER_HANDLER_NAME
#undef  __EV_READER_HANDLER_SIGNATURE

}

#endif  /*  !LDNDC_IO_IREADER_EVENT_XML_CPP_  */

