
#ifndef  LDNDC_IO_IREADER_SITEPARAMETERS_XML_CPP_
#define  LDNDC_IO_IREADER_SITEPARAMETERS_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_siteparameters.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc.h"

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_siteparameters.h"

namespace ldndc {
char const *  iproc_landscape_reader_siteparameters_xml_t::ROOT_TAGS[iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcsiteparameters", "siteparameters"};


LDNDC_SERVER_OBJECT_DEFN(iproc_reader_siteparameters_xml_t)
iproc_reader_siteparameters_xml_t::iproc_reader_siteparameters_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_siteparameters_t()
{
    this->data_arrived = 1;
}
iproc_reader_siteparameters_xml_t::~iproc_reader_siteparameters_xml_t()
{
}

bool
iproc_reader_siteparameters_xml_t::use_lresources()
const
{
    bool  use_lr = XML_ATTRIBUTE_VALUE( "useresources", true, NULL);
    if ( use_lr)
    {
        iproc_reader_siteparameters_lrsrc_t const  rsrc_rdr;
        use_lr = rsrc_rdr.is_open();
    }
    return  use_lr;
}

lerr_t
iproc_reader_siteparameters_xml_t::get_siteparameters_set(
        siteparameters::siteparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    int  p_read = 0;  int  p_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_siteparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_siteparameters_set(
                _props, _props_names, _props_types, _n_props, &p_read, &p_seen);
        if ( rc_read)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
        lerr_t  rc_get = helper_xml_get_list_eq_< siteparameters::siteparameter_t >( 
                        helper_xml_walk_path_( this->get_handle(), 0),
                        "par", "name", "value",
            _props_names, _props_types, _n_props,
                        _props, _n_props, 1,
            0 /*offset*/, 1 /*stride*/,
            _p_read, _p_seen, NULL, NULL, XML_RFLAG_KEY_STRIP|XML_RFLAG_KEY_TO_UPPER);

    if ( this->use_lresources())
    {
        if ( _p_read) *_p_read = p_read;
        if ( _p_seen) *_p_seen = p_seen;
    }
    else if ( rc_get)
    {
        CBM_LogError( "iproc_reader_siteparameters_xml_t::get_siteparameters_set(): ",
                "error reading site parameters  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }
    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}



/*  *****  SITE PARAMETER XML READER  *****  */

LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_siteparameters_xml_t)
iproc_landscape_reader_siteparameters_xml_t::iproc_landscape_reader_siteparameters_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_siteparameters_t()
{
}


iproc_landscape_reader_siteparameters_xml_t::~iproc_landscape_reader_siteparameters_xml_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_SITEPARAMETERS_XML_CPP_  */

