/*!
 * @brief
 *    concrete xml readers (Base)
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  CBM_IREADER_XML_H_
#define  CBM_IREADER_XML_H_

#include  "io/data-provider/ireaders/ireader.h"
#include  "io/fhandler/xml/fhandler_xml.h"
#include  "time/cbm_time.h"
#include  <map>

/*! flags for xml reader routine @fn helper_xml_get_list_eq_ */
enum
{
    XML_RFLAG_NONE                  = 0u,

    /*! forward mode, no reading or counting */
    XML_RFLAG_MODE_READ        = 1u << 0,
    /*! forward mode, no reading or counting */
    XML_RFLAG_MODE_COUNT        = 1u << 1,
    /*! forward mode, no reading or counting */
    XML_RFLAG_MODE_FORWARD        = 1u << 2,

    /*! update position handle */
    XML_RFLAG_UPDATE_POS            = 1u << 3,

    /*! normalize key attribute value (strip head and tail) */
    XML_RFLAG_KEY_STRIP             = 1u << 4,
    /*! normalize key attribute value (lower case) */
    XML_RFLAG_KEY_TO_LOWER          = 1u << 5,
    /*! normalize key attribute value (upper case) */
    XML_RFLAG_KEY_TO_UPPER          = 1u << 6,
    /*! ignore missing attributes (NOT USED) */
    XML_RFLAG_IGNORE_MISSING        = 1u << 7
};

namespace ldndc {

/* ###   XML INPUT READER   ### */
class  CBM_API  iproc_reader_xml_base_t  :  public  iproc_reader_base_t,  public  iproc_handle_xml_types_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_xml_base_t::format(); }
    protected:
        iproc_reader_xml_base_t();
        virtual  ~iproc_reader_xml_base_t() = 0;
};

class  CBM_API  iproc_reader_xml_t  :  public  iproc_reader_xml_base_t
{
    public:
        lerr_t  set_handle(
                core_iobject_handle_t *);
        core_iobject_handle_t *  get_handle();
        core_iobject_handle_t const *  get_handle() const;

        lid_t const &  object_id() const;

    protected:
        iproc_reader_xml_t();
        virtual  ~iproc_reader_xml_t() = 0;

    private:
        core_iobject_handle_t *  hdl_;
};


/* ###   XML LANDSCAPE INPUT READER   ### */
class  CBM_API  iproc_landscape_reader_xml_base_t  :  public  iproc_landscape_reader_base_t,  public  iproc_handle_xml_types_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_xml_base_t::format(); }
    public:
        virtual  char const *  root_tag( input_stream_mode_e = IMODE_CNT) const = 0;

        virtual  char const *  core_tag() const = 0;

    protected:
        iproc_landscape_reader_xml_base_t();
        virtual  ~iproc_landscape_reader_xml_base_t() = 0;
};

class  CBM_API  iproc_landscape_reader_xml_t  :  public  iproc_landscape_reader_xml_base_t
{
    public:
        lerr_t  status() const { return  this->status_; }

        int  scan_stream(
                iobject_handler_base_t * /*i/o format handler*/);
        int  rescan_stream();

        iproc_provider_t *  acquire_ireader(
                lid_t const & /*block id*/);
        void  release_ireader(
                lid_t const & /*block id*/);

        lerr_t  add_to_processing_stack(
                iproc_provider_t ** /*address of pointer to reader*/,
                iproc_provider_t * /*data processors*/);

        core_iobject_handle_t const *  get_handle() const { return  this->hdl; }
        input_stream_mode_e  input_stream_mode() const;

        int  get_available_blocks(
                lid_t [], size_t) const;
    public:
        size_t  size() const { return  this->number_of_blocks(); }
        size_t  number_of_blocks() const;
        bool    check_input_exists(
                lid_t const & /*block id*/) const;

    public:
        int  collective_update_commence(
                cbm::sclock_t const *);
        int  collective_update_complete(
                cbm::sclock_t const *);
    protected:
        iproc_landscape_reader_xml_t();
        virtual  ~iproc_landscape_reader_xml_t() = 0;

    protected:
        /* pointer to i/o handler */
        iobject_handler_xml_t *  fhandler;
        /* pointer to stream */
        iobject_handle_t *  hdl;
    private:
        lerr_t  status_;
        input_stream_mode_e  imode_;

        struct  node_info_t
        {
            lid_t  id;
            core_iobject_handle_t *  handle;
        };
        typedef  std::map< lid_t, node_info_t >  node_info_map_t;
        typedef  std::pair< std::map< lid_t, node_info_t >::iterator, bool >  node_info_map_insert_return_t;
        node_info_map_t  node_infos_;
        typedef  std::map< lid_t, iproc_provider_t * >  node_reader_map_t;
        typedef  std::pair< std::map< lid_t, iproc_provider_t * >::iterator, bool >  node_reader_map_insert_return_t;
        node_reader_map_t  node_readers_;

        core_iobject_handle_t *  core_handle_begin_() const;
        core_iobject_handle_t *  core_handle_next_(
                core_iobject_handle_t * /*current handle*/) const;

        int  scan_stream_(
                bool /*initial scan(false) or rescan(true)?*/);
        int  configure_node_(
                node_info_t &);
        int  reconfigure_node_(
                node_info_t &);
        int  insert_node_info_(
                node_info_t &);
        void  invalidate_node_handles_();
};
}

#endif  /*  !CBM_IREADER_XML_H_  */

