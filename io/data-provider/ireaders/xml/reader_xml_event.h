/*!
 * @brief
 *    concrete xml readers (event)
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  CBM_IREADER_XML_EVENT_H_
#define  CBM_IREADER_XML_EVENT_H_

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/input/event/event-tree.h"

#include  "io/iif/iif_event.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_event_xml_t  :  public  iproc_landscape_reader_xml_t,  public  iproc_landscape_interface_event_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_event_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_landscape_interface_event_t::input_class_type(); }

        static  char const *  ROOT_TAGS[IMODE_CNT];
        char const *  root_tag( input_stream_mode_e  _imode = IMODE_CNT)
        const
        {
            crabmeat_assert( _imode < IMODE_CNT);
            return  ROOT_TAGS[_imode];
        }

    public:
        char const *  core_tag() const { return  core_tag_(); }
    private:
        static  char const *  core_tag_() { return iproc_landscape_reader_event_xml_t::ROOT_TAGS[IMODE_SINGLE]; }

    public:
        iproc_landscape_reader_event_xml_t();
        virtual  ~iproc_landscape_reader_event_xml_t();

        std::string  time() const;
};
class  CBM_API  iproc_reader_event_xml_t  :  public  iproc_reader_xml_t,  public  iproc_interface_event_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_event_xml_t)
    typedef  lerr_t (*event_attribute_readers_xml)( event::event_attribute_t *, event::event_attribute_t const *, iproc_handle_xml_types_t::core_iobject_handle_t const *, int *, int *);

    static char const *  ROTATION_TAG;
    static char const *  EVENT_TAG;

    public:
        char const *  input_class_type() const { return  iproc_interface_event_t::input_class_type(); }

        iproc_reader_event_xml_t();
        virtual  ~iproc_reader_event_xml_t();

    public:
        int  data_available() const;

        int  readahead_size() const;
        lerr_t  get_event_tree(
                cbm::event_info_tree_t *,
                int /*load limit*/,
                int *, int *);
        lerr_t  get_event_attributes(
                event::event_attribute_t *, event::event_attribute_t const *,
                       event::event_info_t *,
                int *, int *) const;

    private:
        void  attach_rotations_(
                cbm::event_info_tree_t *, cbm::event_info_tree_t::iterator_t,
                core_iobject_handle_t const *, core_iobject_handle_t const *,
                int,
                int *, int *);
        lerr_t  attach_events_(
                cbm::event_info_tree_t *, cbm::event_info_tree_t::iterator_t &,
                core_iobject_handle_t const *,
                int,
                int *, int *);

        event_attribute_readers_xml  event_helper_attribute_handlers[event::EVENT_CNT];

        struct  event_xml_position_t
        {
            event_xml_position_t() : evn(NULL),rot(NULL),hit_eof(0) {}

            core_iobject_handle_t const *  evn;
            core_iobject_handle_t const *  rot;

            int  hit_eof;
        };
        struct event_xml_position_t  last_loaded;
};
}

#endif  /*  !CBM_IREADER_XML_EVENT_H_  */

