
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"

#include  "log/cbm_baselog.h"
#include  <cstdarg>

ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *
helper_xml_walk_path_valist_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                unsigned int  _l,
                va_list *  _p)
{
    crabmeat_assert( _node);

    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  node( _node);
        unsigned int  l( _l);

        while ( node && l)
    {
        char const *  elem( va_arg( *_p, char const *));

        if ( cbm::is_empty( elem) || cbm::is_equal( ".", elem))
        {
            // stay at current level
        }
               else if ( cbm::is_equal( "..", elem))
        {
            CBM_LogTodo( "check xml walk path, p=\"..\"");
            // go up one level
            // what if we are at the root node?!
            //if ( node != /*does not exist -->*/node->RootElement()) {
                node = node->Parent();
            //}
        }
        else
        {
            node = node->FirstChildElement( elem);
        }
                l--;
        }
        
        if ( l || ( ! node))
    {
        /* xml-path ends prematurely */
                return  NULL;
        }

    return  node;
}


ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *
helper_xml_walk_path_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                unsigned int  _l,
                ... /* <= path  :: BE REALLY CAREFUL  ;-) */)
{
        va_list  p;
        va_start( p, _l);
    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  node = helper_xml_walk_path_valist_( _node, _l, &p);
        va_end( p);

    return  node;
}


ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *
helper_xml_walk_path_sentinel_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                ... /* <= path  NULL terminated :: BE REALLY CAREFUL  ;-) */)
{
    crabmeat_assert( _node);

        va_list  p;
        va_start( p, _node);

    unsigned int  l( 0);

    /* find number of arguments given */
    while ( va_arg( p, char const *))
    {
        ++l;
    }
        va_end( p);

    /* empty path given */
        if ( l == 0)
    {
                return  _node;
        }

    va_start( p, _node);
    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  node( helper_xml_walk_path_valist_( _node, l, &p));
        va_end( p);

        return  node;
}

lerr_t
helper_xml_get_list_eq_size_(
        ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
        char const *  _name,
        int *  _r, int *  _s)
{
    return  helper_xml_get_list_eq_< ldndc::ltypewrap_t< float > >( 
                _node,
                _name, NULL, NULL, NULL,
                NULL, invalid_size,
                NULL, invalid_size, invalid_size,
                0 /*offset*/, 1 /*stride*/,
                       _r, _s,
                NULL, NULL);
}

ldndc::iproc_handle_xml_types_t::core_iobject_handle_t *
helper_xml_new_element_(
        char const *  _elem_name,
               ldndc::iproc_handle_xml_types_t::iobject_type_t *  _xmldoc)
{
#if    defined(_USE_LIB_XML_TINYXML)
    CRABMEAT_FIX_UNUSED(_xmldoc);
    return  new InputElementType( _elem_name);
#elif  defined(_USE_LIB_XML_TINYXML2)
    return  _xmldoc->NewElement( _elem_name);
#endif
}

