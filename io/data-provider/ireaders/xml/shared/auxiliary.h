
#include  "io/data-provider/ireaders/xml/reader_xml.h"

#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"

#include  "containers/cbm_bitset.h"
#include  "containers/cbm_typewrap.h"

#include  <cstdarg>

#define  LDNDC_XML_GLOBAL_ROOTTAG_PREFIX  "ldndc"

#if    defined(_USE_LIB_XML_TINYXML)
typedef  TiXmlElement  InputElementType;
#elif  defined(_USE_LIB_XML_TINYXML2)
typedef  tinyxml2::XMLElement  InputElementType;
#endif

/* convenience macros for getting attribute values */
/* helper macro (do not use directly!) */
#define  XML_ATTRIBUTE_VALUE_( __node__,__attr__,__default__,...)    \
    helper_xml_get_attribute_value_(                \
            helper_xml_walk_path_sentinel_(            \
                __node__,                \
                __VA_ARGS__),                \
            __attr__,                    \
            (__default__))/*;*/
/* query attribute value ( subtree is core's handle)
 *
 * due to "different" behavior of the ms c++ preprocessor
 * (<=2008) variadic macro expansion does not work
 * as expected:
 *
 * as a workaround we require a NULL path (or ".") in cases
 * where no further xml tree traversal is needed
 */
#define  XML_ATTRIBUTE_VALUE(__attr__,__default__,...)            \
    XML_ATTRIBUTE_VALUE_(this->get_handle(),__attr__,__default__,__VA_ARGS__,NULL /* sentinel */)
/* query attribute value, subtree must be specified */
#define  XML_ATTRIBUTE_VALUE_AT(__node__,__attr__,__default__,...)    \
    XML_ATTRIBUTE_VALUE_(__node__,__attr__,__default__,__VA_ARGS__,NULL /* sentinel */)

/*!
 * @brief
 *    read value of attribute @p _attr for element @p _node. if element
 *    or attribute do not exist, use default @p _default.
 *
 * @todo
 *    add error flag?
 */
template< typename _T >
_T
helper_xml_get_attribute_value_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                char const *  _attr,
                _T  _default)
{
        if ( _node)
    {
        _T  a_value( _default);

        char const *  attr = _node->ToElement()->Attribute( _attr);
        if ( attr || cbm::is_whitespace( attr))
        {
            lerr_t  rc_convert = cbm::s2n< _T >( attr, &a_value);
            if ( rc_convert)
            {
                CBM_LogError( "failed to convert attribute value  [attribute=",_attr,",value=",attr,"]");
            }
            else
            {
                return  a_value;
            }
        }
    }
    return  _default;
}

/*!
 * @brief
 *    xml helper function
 *
 *    walk along a path in an xml tree, until end of 
 *    path is reached or we hit a leaf in the tree
 *
 * @param _node
 *      root of the xml (sub)tree
 * @param _l
 *      number of path elements
 * @param _path
 *      a sequence of char const * items
 */
ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *
helper_xml_walk_path_valist_(
    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *,
    unsigned int, va_list *);

/*!
 * @brief
 *    xml helper function
 *
 *    walk along a path in an xml tree, until end of 
 *    path is reached or we hit a leaf in the tree
 *    @n
 *    different interface
 *
 * @param _node
 *      root of the xml (sub)tree
 * @param _l
 *      number of path elements
 * @param _path
 *      a sequence of char const * items
 */
ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *
helper_xml_walk_path_(
    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *,
    unsigned int, ... /* <= path  :: BE REALLY CAREFUL  ;-) */);

/*!
 * @brief
 *    xml helper function
 *
 *    walk along a path in an xml tree, until end of 
 *    path is reached or we hit a leaf in the tree
 *    @n
 *    different interface (NULL terminated path sequence)
 *
 * @param _node
 *      root of the xml (sub)tree
 * @param _path
 *      a sequence of char const * items terminated with NULL (e.g.
 *      '"a", "b", "c", NULL' finds element
 *      <$_node> <a><b><c attr="foo" /></b></a> </$_node>
 */
ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *
helper_xml_walk_path_sentinel_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *,
                ... /* <= path  NULL terminated :: BE REALLY CAREFUL  ;-) */);


/*!
 * @brief
 *
 * this function reads attribute values @p v_i of attribute @p _a from list L
 * (@ _node) whose elements are @p _e into the array @p _l (size known in
 * advance, @p _ls is the stride applied). the index in @p _l is determined by
 * the value of the index attribute @p _i with the help of the index lookup
 * array @p _il. if no index lookup array is given, i.e. @p _il == NULL, the 
 * siblings counter is used. the following short example xml shows the structure
 * of L (see also 'files' block in setup file):
 *
 * <_node>
 *   <_e  _i="i_0" _a="v_0" />
 *   <_e  _i="i_2" _a="v_2" />
 *   <_e  _i="i_1" _a="v_1" />
 * </_node>
 *
 * where @p type is the index attribute, attr's values @p v_i are read into
 * the result list @p _l. if the resulting index is larger than the array (@p
 * _ln) the element is ignored. the list L is represented by the tree node
 * 'list'. note that the element names are equal (hence the function name) and
 * known in advance.
 *
 * additional fields will be set if non-NULL: @p _r holds the number of
 * elements (that would have been (see below)) written to @p _l while @p _s
 * holds the total number of siblings visited.
 *
 * @p _rflags is an or'd list of flags XML_RFLAG_* defined in @file reader_xml.h.
 *
 * @p _lnode stores the final position visited to allow for incremental
 * loads. final position @p _lnode is only updated if flag XML_RFLAG_UPDATE_POS
 * was given. along with the node the node counter is passed back. the number of
 * items read is determined by the size @p _ln of the result array @p _l.
 * 
 * note that passing NULL for the result list, makes this function count
 * the number of sibling elements @p _e for tree node @p _node.
 *
 * TODO  explain @p _itl (index type list)
 * TODO  explain @p _offset, @p _stride
 */
template< typename _A, typename  _F, typename  _I, typename  _U >
lerr_t
helper_xml_get_list_eq_typegen_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                char const *  _e, char const *  _i, char const *  _a,
// sk:??                char const * const *  _il, char const * const  _itl, size_t  _iln,
                ldndc_string_t const *  _il, char const * const  _itl, size_t  _iln,
                _A *  _l, size_t  _ln, size_t  _ls,
        int  _offset, int  _stride,
                int *  _r, int *  _s,
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const ** _lnode, unsigned int *  _lj, unsigned int  _rflags)
{
    /* an element name must be provided */
    crabmeat_assert( _e);

    lerr_t  rc( LDNDC_ERR_OK);

        if ( ! _node)
    {
        CBM_LogVerbose( "requested path in xml structure does not exist (node=NULL)");
                return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
        }

        unsigned int  r( 0), s( 0), t( 0);
    unsigned int  j(( _lj) ? (( _lnode) ? *_lj : 0) : 0);

    unsigned int  normalize_attr(
            (( _rflags & XML_RFLAG_KEY_STRIP) ? CNAME_STRIP : CNAME_STRIP_NONE)
              | (( _rflags & XML_RFLAG_KEY_TO_LOWER) ? CNAME_STRIP_TO_LOWER : CNAME_STRIP_NONE)
              | (( _rflags & XML_RFLAG_KEY_TO_UPPER) ? CNAME_STRIP_TO_UPPER : CNAME_STRIP_NONE));

    char const *  attr_val( NULL);
    cbm::nstr  attr_val_n( NULL, normalize_attr);

    /* when j > 0, continue reading from *_lnode otherwise first child element of _node */
    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  node(( j==0) ? helper_xml_walk_path_( _node, 1, _e) : *_lnode);
        InputElementType const *  elem = ( node) ? node->ToElement() : NULL;

    int  offs( _offset);
    int  stride( 1/*_stride*/);

    /* skip @p offs number of records */
    while (( offs--)  &&  ( elem = elem->NextSiblingElement( _e)))
    {
               /* count records? yes! */
               ++j;
    }

    if ( _rflags & XML_RFLAG_MODE_FORWARD)
    {
        if ( _lnode)
        {
            *_lnode = elem;
        }
        /* suppress setting it to sibling below */
        _lnode = NULL;
    }
    else
    {
        /* recognize duplicates */
        ldndc::bitset  il_hits(( _i) ? _iln : 0);

        /* read records */
        for ( r = 0, s = 0, t = 0/*, elem*/;  elem;  elem = elem->NextSiblingElement( _e), ++j)
        {
            /* skip records due to stride */
            if ( --stride) { continue; } else { stride = _stride; }

            /* find element key */
            if ( _i)
            {
                if ( normalize_attr)
                {
                    attr_val_n = elem->Attribute( _i);
                    attr_val = attr_val_n.c_str();
                }
                else
                {
                    attr_val = elem->Attribute( _i);
                }
                if ( !attr_val)
                {
                    //CBM_LogInfo( "missing index attribute '",_i,"' for '",_e,"' element. input ignored.");
                    // TODO  do we want that?
                    continue;
                }
            }

            /* note that attr_val == null leaves t unchanged, i.e. a simple counting index (see below) */
            if ( cbm::find_index( attr_val, _il, _iln, &t) == LDNDC_ERR_OK)
            {
                if ( _i)
                {
                    if ( il_hits.test( (ldndc::bitset::size_type)t))
                    {
#if  defined(_USE_LIB_XML_TINYXML)
                        int const  line_nb( elem->Row());
#else
                        static char const *  line_nb = "<unknown>";
#endif
                        CBM_LogWarn( "duplicate entries for element (",_e,"): ",_i,"=",attr_val,", line=",line_nb);
                        continue;
                    }
                    else
                    {
                        il_hits.set( (ldndc::bitset::size_type)t);
                    }
                }

                if ( _a && elem->Attribute( _a))
                {
                    if ( t < _ln)
                    {
                        ++r;
                        if ( _l)
                        {
                            char const  _dtype(( _itl) ? _itl[t] : '\0');
                            /* multiply with stride to find final array index */
                            size_t const  t_l( _ls*t);
                            switch ( _dtype)
                            {
                                case '\0':
                                case 'f':
                                case 'd': {
                                          _F  _a_val;
                                          cbm::s2n< _F >( elem->Attribute( _a), &_a_val);
                                          _l[t_l] = _a_val;
                                          break;
                                      }
                                case 'i': {
                                          _I  _a_val;
                                          cbm::s2n< _I >( elem->Attribute( _a), &_a_val);
                                          _l[t_l] = _a_val;
                                          break;
                                      }
                                case 'u': {
                                          _U  _a_val;
                                          cbm::s2n< _U >( elem->Attribute( _a), &_a_val);
                                          _l[t_l] = _a_val;
                                          break;
                                      }
                                case 'b': {
                                          bool  _a_val;
                                          cbm::s2n< bool >( elem->Attribute( _a), &_a_val);
                                          _l[t_l] = _a_val;
                                          break;
                                      }
                                default:
                                      CBM_LogError( "unknown data type specifier  [dtype=",_dtype,"]");
                                      rc = LDNDC_ERR_INVALID_ARGUMENT;
                            }
                            if ( rc != LDNDC_ERR_OK)
                            {
                                break;
                            }
                            /* TODO  use in combination with bitset, i.e. increase
                             *     r only iff _l[i] has not been set yet...

                             *     keep in mind, though, that we assume
                             *     "error-free" and complete data sets, meaning
                             *     handling gaps is none of our business (e.g.
                             *     weather generator will be seperate tool)
                             */

                            /* when (theoretically) all elements in
                             * the result array have been touched
                             * (and we are not just counting, i.e.
                             * _l!=NULL) stop reading
                             *
                             * "theoretically" means, that we could have
                             * overwritten some previously set item,
                             * counting it twice or more often
                             */
                            if ( r == _ln)
                            {
                                ++s;
                                break;
                            }
                        }
                    }
                    else
                    {
                        /* in read mode
                         *    array not large enough to accommodate current element
                         *
                         * in count mode
                         *    no op
                         *
                         * in forward mode
                         *    should not get here ..
                         */
                    }
                }
                else
                {
                    // TODO  ++v /* no data items */
                    if ( _a)
                    {
                        //CBM_LogInfo( "missing '",_a,"' attribute for '",_e,"' element. input ignored.");
                    }
                }
            }
            else
            {
                // TODO  ++u /* unindexed items */
                if ( _i)
                {
                    CBM_LogWarn( "unknown entity (",_i,"=\"",attr_val,"\") seen in '",_e,"' element. input ignored.");
                }
            }
            /* increment index counter (t) and item counter (s) */
            ++t; ++s;
        }
    }

    if ( _r)
    {
        *_r = ( _a) ? r : s;
    }
    if ( _s)
    {
        *_s = s;
    }

        /* store final position */
    if ( _rflags & XML_RFLAG_UPDATE_POS)
    {
        if ( _lnode)
        {
            *_lnode = ( elem) ? elem->NextSiblingElement( _e) : NULL;
        }
        if ( _lj)
        {
            *_lj = j;
        }
    }

        return  rc;
}
template< typename _A >
lerr_t
helper_xml_get_list_eq_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                char const *  _e, char const *  _i, char const *  _a,
// sk:??                char const * const *  _il, /*no '_itl' here*/ size_t  _iln,
                ldndc_string_t const *  _il, /*no '_itl' here*/ size_t  _iln,
                _A  _l[], size_t  _ln, size_t  _ls,
        int  _offset, int  _stride,
                int *  _r, int *  _s,
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const ** _lnode, unsigned int *  _lj, unsigned int  _rflags=XML_RFLAG_NONE)
{
    return  helper_xml_get_list_eq_typegen_< _A, _A, _A, _A >(
            _node,
            _e, _i, _a,
            _il, NULL, _iln,
            static_cast< _A * >( _l), _ln, _ls,
            _offset, _stride,
            _r, _s,
            _lnode, _lj, _rflags);
}


template< class _U >
lerr_t
helper_xml_get_list_eq_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                char const *  _e, char const *  _i, char const *  _a,
// sk:??                char const * const *  _il, char const * const  _itl, size_t  _iln,
                ldndc_string_t const *  _il, char const * const  _itl, size_t  _iln,
                _U  _l[], size_t  _ln, size_t  _ls,
        int  _offset, int  _stride,
                int *  _r, int *  _s,
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const ** _lnode, unsigned int *  _lj, unsigned int  _rflags=XML_RFLAG_NONE)
{
    return  helper_xml_get_list_eq_typegen_< _U, typename _U::float_type, typename _U::int_type, typename _U::uint_type >(
            _node,
            _e, _i, _a,
            _il, _itl, _iln,
            static_cast< _U *>( _l), _ln, _ls,
            _offset, _stride,
            _r, _s,
            _lnode, _lj, _rflags);
}

template< typename _A >
lerr_t
helper_xml_forward_in_list_eq_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                char const *  _e,
        int  _offset, int  _stride,
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const ** _lnode,
        unsigned int *  _lj)
{
    return  helper_xml_get_list_eq_typegen_< _A, _A, _A, _A >(
            _node,
            _e, NULL, NULL,
            NULL, NULL, ldndc::invalid_t< size_t >::value,
            NULL, ldndc::invalid_t< size_t >::value, ldndc::invalid_t< size_t >::value,
            _offset, _stride,
            NULL, NULL,
            _lnode, _lj, XML_RFLAG_UPDATE_POS|XML_RFLAG_MODE_FORWARD);
}

/*!
 * @brief
 *    this function reads attribute values @p v_i of attribute @p _a from list L
 *    (@ _node) whose element names are taken from the list @p _el into the array
 *    @p _l (size known in advance). if the element does not exist a default is
 *    set. the index in @p _l corresponds to the index in _el. the following
 *    short example xml shows the structure of L (see also 'use' block in setup
 *    file):
 *
 *    <list>
 *       <e_0  _a="v_0" />
 *       <e_2  _a="v_2" />
 *       <e_1  _a="v_1" />
 *      </list>
 *
 *     for parameters @p _r and @p _s see @see helper_xml_get_list_eq_
 */
template< typename _A >
lerr_t
helper_xml_get_list_fix_(
                ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  _node,
                char const * const *  _el, size_t  _eln,
                char const *  _a,
                _A  _l[], size_t  _ln, _A  _d,
                int *  _r, int *  _s)
{
    /* elements read/assigned and seen */
    int  r = 0, s = 0;
    unsigned int  i( 0);

    /* note. even if node is NULL, we run through loop setting default value */
    for ( r = 0, s = 0, i = 0;  i < _eln;  ++i)
    {
        if ( _node  &&  _node->FirstChildElement( _el[i]))
        {
            InputElementType const * e( _node->FirstChildElement( _el[i]));
            ++s;
            if ( e->Attribute( _a))
            {
                if ( i < _ln)
                {
                    ++r;
                    if ( _l)
                    {
                        lerr_t  rc( cbm::s2n( e->Attribute( _a), &_l[i]));
                        if ( rc != LDNDC_ERR_OK)
                        {
                            return  LDNDC_ERR_INVALID_ARGUMENT;
                        }
                    }
                }
                else
                {
                    // array not large enough to accommodate current element
                    // element dropped
                }
            }
            else
            {
                if ( i < _ln)
                {
                    _l[i] = _d;
                }
                else
                {
                    // no op, see note below
                }
            }
        }
        else
        {
            if ( i < _ln)
            {
                _l[i] = _d;
            }
            else
            {
                /* note, i decided to not break here for the sake of
                 * counter s
                 */
            }
        }
    }

    if ( _r)
    {
        *_r = r;
    }
    if ( _s)
    {
        *_s = s;
    }

    return  LDNDC_ERR_OK;
}

/*!
 * @brief
 *    this functions counts the sibling elements directly below
 *    tree node @p _node having name @p _name
 *
 */
lerr_t
helper_xml_get_list_eq_size_(
        ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *,
        char const * /*name*/, int *, int *);

ldndc::iproc_handle_xml_types_t::core_iobject_handle_t *
helper_xml_new_element_( char const * /*element name*/,
               ldndc::iproc_handle_xml_types_t::iobject_type_t * /*xmldoc*/);

