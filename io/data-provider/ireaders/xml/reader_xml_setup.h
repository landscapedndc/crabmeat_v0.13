/*!
 * @brief
 *    concrete xml readers (setup)
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  CBM_IREADER_XML_SETUP_H_
#define  CBM_IREADER_XML_SETUP_H_

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/iif/iif_setup.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_setup_xml_t  :  public  iproc_landscape_reader_xml_t,  public  iproc_landscape_interface_setup_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_setup_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_landscape_interface_setup_t::input_class_type(); }

        static  char const *  ROOT_TAGS[IMODE_CNT];
        char const *  root_tag( input_stream_mode_e  _imode = IMODE_CNT)
        const
        {
            crabmeat_assert( _imode < IMODE_CNT);
            return  ROOT_TAGS[_imode];
        }

    public:
        char const *  core_tag() const { return  core_tag_(); }
    private:
        static  char const *  core_tag_() { return iproc_landscape_reader_setup_xml_t::ROOT_TAGS[IMODE_SINGLE]; }

    public:
        iproc_landscape_reader_setup_xml_t();
        virtual  ~iproc_landscape_reader_setup_xml_t();
};
class  CBM_API  iproc_reader_setup_xml_t  :  public  iproc_reader_xml_t,  public  iproc_interface_setup_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_setup_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_setup_t::input_class_type(); }

        iproc_reader_setup_xml_t();
        virtual  ~iproc_reader_setup_xml_t();

    public:
        std::string  get_site_name() const;
        lerr_t  get_section( cbm::string_t * /*buffer*/,
                        char const * /*section*/) const;

        lerr_t  blocks_consume( cbm::string_t &, int *, int *) const;
        lerr_t  blocks_produce( cbm::string_t &, int *, int *) const;

        lerr_t  get_centroid( cbm::geom_coord_t *) const;
        lerr_t  get_boundingbox( cbm::geom_bounding_box_t *) const;

        int  get_canopylayers() const;
        int  get_soillayers() const;
    
        double  get_elevation() const;
        double  get_latitude() const;
        double  get_longitude() const;

        double  get_area() const;
        double  get_volume() const;

        int  get_timezone() const;
        double  get_slope() const;
        double  get_aspect() const;

        size_t  get_number_of_neighbors() const;
        lerr_t  get_neighbors( cbm::neighbor_t [], size_t) const;
        cbm::geo_orientations_t  get_mooreboundaries() const;

        int  active_flag() const;
        int  quiet_flag() const;
        int  dispatchonany_flag() const;
        int  checkpointcreate_flag() const;
        int  checkpointrestore_flag() const;
    private:
        int  m_readflag( char const * /*flagname*/,
                    char const * /*default*/) const;


    private:
        lerr_t  blocks_consume_produce_(
                cbm::string_t &, int *, int *,
                char const * /*target name*/) const;
};
}

#endif  /*  !CBM_IREADER_XML_SETUP_H_  */

