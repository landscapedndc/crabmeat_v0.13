
#ifndef  LDNDC_IO_IREADER_CLIMATE_XML_CPP_
#define  LDNDC_IO_IREADER_CLIMATE_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_climate.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
char const *  ldndc::iproc_landscape_reader_climate_xml_t::ROOT_TAGS[ldndc::iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcclimate", "climate"};

#define  XML_CLIMATE__RECORD_ELEMENT_TAG  "record"

namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_climate_xml_t)
iproc_reader_climate_xml_t::iproc_reader_climate_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_climate_t(),
          lnode0_( NULL), lnode_( NULL),
          lpos0_( 0), lpos_( 0)
{
}
iproc_reader_climate_xml_t::~iproc_reader_climate_xml_t()
{
}


double
iproc_reader_climate_xml_t::get_latitude()
const
{
    return  XML_ATTRIBUTE_VALUE( "latitude", invalid_dbl, NULL);
}

double
iproc_reader_climate_xml_t::get_longitude()
const
{
    return  XML_ATTRIBUTE_VALUE( "longitude", invalid_dbl, NULL);
}

double
iproc_reader_climate_xml_t::get_elevation()
const
{
    return  XML_ATTRIBUTE_VALUE( "elevation", invalid_dbl, NULL);
}

double
iproc_reader_climate_xml_t::get_cloudiness()
const
{
    return  XML_ATTRIBUTE_VALUE( "cloudiness", invalid_dbl, NULL);
}
double
iproc_reader_climate_xml_t::get_rainfall_intensity()
const
{
    return  XML_ATTRIBUTE_VALUE( "rainfallintensity", invalid_dbl, NULL);
}
double
iproc_reader_climate_xml_t::get_wind_speed()
const
{
    return  XML_ATTRIBUTE_VALUE( "windspeed", invalid_dbl, NULL);
}

double
iproc_reader_climate_xml_t::get_annual_precipitation()
const
{
    return  XML_ATTRIBUTE_VALUE( "annualprecipitation", invalid_dbl, NULL);
}
double
iproc_reader_climate_xml_t::get_temperature_average()
const
{
    return  XML_ATTRIBUTE_VALUE( "temperatureaverage", invalid_dbl, NULL);
}
double
iproc_reader_climate_xml_t::get_temperature_amplitude()
const
{
    return  XML_ATTRIBUTE_VALUE( "temperatureamplitude", invalid_dbl, NULL);
}



// sk:?? /***  climate2d -> records  ***/
// sk:?? size_t
// sk:?? iproc_reader_climate_xml_t::get_records_list_size()
// sk:?? const
// sk:?? {
// sk:??     /* be careful, as this is quickly inconsistent */
// sk:??     size_t  r( XML_ATTRIBUTE_VALUE( "size", invalid_size, NULL));
// sk:?? 
// sk:??     if ( invalid_size == r)
// sk:??            {
// sk:??         int  ri( invalid_int);
// sk:??         (void)helper_xml_get_list_eq_size_( 
// sk:??                 helper_xml_walk_path_( this->get_handle(), 0),
// sk:??                 "record",
// sk:??                 &ri, NULL);
// sk:??         
// sk:??         r = ( ri == invalid_int) ? invalid_size : (size_t)ri;
// sk:??     }
// sk:??     return  r;
// sk:?? }

lerr_t
iproc_reader_climate_xml_t::preset(
        char const * [], size_t, config_type *  _config)
{
    crabmeat_assert( _config);
    CBM_LogDebug( "offset=",_config->offset);
    this->lpos0_ = 0;
    this->lnode0_ = NULL;

    lerr_t  rc_forward_in_list = helper_xml_forward_in_list_eq_< climate::record::item_type >(
            helper_xml_walk_path_( this->get_handle(), 0),
            XML_CLIMATE__RECORD_ELEMENT_TAG, 
            _config->offset, 1 /*stride*/,
            &this->lnode0_, &this->lpos0_);
    RETURN_IF_NOT_OK(rc_forward_in_list);

    return  this->reset();
}

lerr_t
iproc_reader_climate_xml_t::reset()
{
    this->lnode_ = this->lnode0_;
    crabmeat_assert( this->lnode_);

    CBM_LogDebug( "reset reader position.  p=", this->lpos_, "p0=", this->lpos0_);
    this->lpos_ = this->lpos0_;

    return  LDNDC_ERR_OK;
}

/***  station -> record  ***/
lerr_t
iproc_reader_climate_xml_t::read(
        climate::record::item_type  _rec[], size_t  _rec_cnt, size_t  _rec_size,
                char const *  _rec_item[], size_t  _rec_item_cnt,
        int  _offset, int  _stride,
                int *  _r, int * _s)
{
    /* TODO  not used yet... */
    if ( _offset < 0)
           {
        CBM_LogError( "negative offset for xml climate record reader not supported");
    }
    if ( _stride < 1)
           {
        CBM_LogFatal( "stride less or equal zero in xml climate record reader.");
    }

    /* for incremental loading, note, that updating the last position read
     * must only occur once after the final record item has been read
     *
     * this counter decrements each iteration, at 0 we update
     */
    size_t  update_lpos( _rec_item_cnt - 1);

    for ( size_t  j = 0;  j < _rec_item_cnt;  ++j, --update_lpos)
           {
        int  r_read( 0);

        lerr_t  rc = helper_xml_get_list_eq_(
                this->lnode_,
                XML_CLIMATE__RECORD_ELEMENT_TAG, NULL, _rec_item[j],
                NULL, invalid_size,
                &_rec[j], _rec_cnt, _rec_size,
                _offset, _stride,
                &r_read, NULL,
                &this->lnode_, &this->lpos_, ((update_lpos == 0) ? XML_RFLAG_UPDATE_POS : XML_RFLAG_NONE));

        if ( rc != LDNDC_ERR_OK)
               {
            CBM_LogError( "failed reading attribute values,  item=", _rec_item[j], "rc=", rc);
            return  rc;
        }

        /* update record item counter */
        if ( _r) *_r  = ( r_read > *_r) ? r_read : *_r;
        if ( _s) *_s += r_read;

    }

    return  LDNDC_ERR_OK;
}




LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_climate_xml_t)
iproc_landscape_reader_climate_xml_t::iproc_landscape_reader_climate_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_climate_t()
{
}


iproc_landscape_reader_climate_xml_t::~iproc_landscape_reader_climate_xml_t()
{
}


/* retrieve time offset and input streams resolution */
std::string
iproc_landscape_reader_climate_xml_t::time()
const
{
    return  XML_ATTRIBUTE_VALUE( "time", ldndc_empty_string, "global");
}

}

#endif  /*  !LDNDC_IO_IREADER_CLIMATE_XML_CPP_  */

