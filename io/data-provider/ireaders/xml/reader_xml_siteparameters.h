/*!
 * @brief
 *    concrete xml readers (siteparameters)
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  CBM_IREADER_XML_SITEPARAMETERS_H_
#define  CBM_IREADER_XML_SITEPARAMETERS_H_

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/iif/iif_siteparameters.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_siteparameters_xml_t  :  public  iproc_landscape_reader_xml_t,  public  iproc_landscape_interface_siteparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_siteparameters_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_landscape_interface_siteparameters_t::input_class_type(); }

        static  char const *  ROOT_TAGS[IMODE_CNT];
        char const *  root_tag( input_stream_mode_e  _imode = IMODE_CNT)
        const
        {
            crabmeat_assert( _imode < IMODE_CNT);
            return  ROOT_TAGS[_imode];
        }

    public:
        char const *  core_tag() const { return  core_tag_(); }
    private:
        static  char const *  core_tag_() { return iproc_landscape_reader_siteparameters_xml_t::ROOT_TAGS[IMODE_SINGLE]; }

    public:
        iproc_landscape_reader_siteparameters_xml_t();
        virtual  ~iproc_landscape_reader_siteparameters_xml_t();
};
class  CBM_API  iproc_reader_siteparameters_xml_t  :  public  iproc_reader_xml_t,  public  iproc_interface_siteparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_siteparameters_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_siteparameters_t::input_class_type(); }

        iproc_reader_siteparameters_xml_t();
        virtual  ~iproc_reader_siteparameters_xml_t();

    public:
        lerr_t  get_siteparameters_set(
                siteparameters::siteparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                int *, int *) const;
    private:
        bool  use_lresources() const;
};
}

#endif  /*  !CBM_IREADER_XML_SITEPARAMETERS_H_  */

