/*!
 * @brief
 *    concrete xml readers (site)
 *
 * @author
 *    Steffen Klatt (created on: Jul 8, 2011)
 *    David Kraus
 */

#ifndef  CBM_IREADER_XML_SITE_H_
#define  CBM_IREADER_XML_SITE_H_

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/iif/iif_site.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_site_xml_t  :  public  iproc_landscape_reader_xml_t,  public  iproc_landscape_interface_site_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_site_xml_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_site_t::input_class_type(); }

        static  char const *  ROOT_TAGS[IMODE_CNT];
        char const *  root_tag( input_stream_mode_e  _imode = IMODE_CNT)
        const
        {
            crabmeat_assert( _imode < IMODE_CNT);
            return  ROOT_TAGS[_imode];
        }

    public:
        char const *  core_tag() const
            { return  core_tag_(); }
    private:
        static  char const *  core_tag_()
            { return iproc_landscape_reader_site_xml_t::ROOT_TAGS[IMODE_SINGLE]; }

    public:
        iproc_landscape_reader_site_xml_t();
        virtual  ~iproc_landscape_reader_site_xml_t();
};
class  CBM_API  iproc_reader_site_xml_t  :  public  iproc_reader_xml_t,  public  iproc_interface_site_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_site_xml_t)
    public:
        char const *  input_class_type() const
            { return  iproc_interface_site_t::input_class_type(); }

        iproc_reader_site_xml_t();
        virtual  ~iproc_reader_site_xml_t();

    public:
        double  get_latitude() const;
        double  get_longitude() const;
        double  get_elevation() const;

        double  get_saturated_hydraulic_conductivity_bottom() const;
        double  get_water_table_depth() const;

        /* soil general section */
        double  get_soil_organic_carbon_content_05() const;
        double  get_soil_organic_carbon_content_30() const;
        double  get_soil_litter_height() const;
        std::string  get_soil_type() const;
        std::string  get_humus_type() const;

        lerr_t  soil_use_history( std::string *) const;

        /* soil layers section */
        size_t  get_number_of_strata() const;

        lerr_t  get_strata_height( double [], size_t, int *, int *) const;
        lerr_t  get_strata_splitheight( double [], size_t, int *, int *) const;
        lerr_t  get_strata_split( int [], size_t, int *, int *) const;
        lerr_t  get_strata_properties(
            double [] /*buffer*/, size_t /*number of strata*/,
            size_t /*number of properties*/,
            ldndc_string_t const [] /*property names*/) const;

    private:
        struct  string_dict_t
        {
            ldndc_string_t  key;
            ldndc_string_t  mapped_key;
        };
        static string_dict_t const  STRATUM_PROPERTY_NAMES_XML[site::STRATUM_PROPERTIES_CNT];

        lerr_t  read_strata_property( char const * /*property name*/,
                double [] /*buffer*/, size_t /*buffer size*/,
                int *, int *) const;
};
}

#endif  /*  !CBM_IREADER_XML_SITE_H_  */

