
#ifndef  LDNDC_IO_IREADER_SPECIESPARAMETERS_XML_CPP_
#define  LDNDC_IO_IREADER_SPECIESPARAMETERS_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_speciesparameters.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
#include  "speciesparameters/speciesparameterstypes.h"
#include  "string/cbm_string.h"

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_speciesparameters.h"

namespace ldndc {
char const *  iproc_landscape_reader_speciesparameters_xml_t::ROOT_TAGS[iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcspeciesparameters", "speciesparameters"};

char const *  iproc_reader_speciesparameters_xml_t::SPECIES_TAG = "species";
char const *  iproc_reader_speciesparameters_xml_t::SPECIES_MNEMONIC = "mnemonic";
char const *  iproc_reader_speciesparameters_xml_t::SPECIES_GROUP = "group";
char const *  iproc_reader_speciesparameters_xml_t::SPECIES_NAME = "name";
char const *  iproc_reader_speciesparameters_xml_t::SPECIES_PARAMETER = "par";

char const *  iproc_reader_speciesparameters_xml_t::SPECIES_PARAMETER_NAME = "name";
char const *  iproc_reader_speciesparameters_xml_t::SPECIES_PARAMETER_VALUE = "value";

char const *  iproc_reader_speciesparameters_xml_t::SPECIES_MNEMONIC_META = species::SPECIES_NAME_NONE;

/*  *****  SPECIES PARAMETER XML CORE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_speciesparameters_xml_t)
iproc_reader_speciesparameters_xml_t::iproc_reader_speciesparameters_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_speciesparameters_t()
{
    this->data_arrived = 1;
}
iproc_reader_speciesparameters_xml_t::~iproc_reader_speciesparameters_xml_t()
{
}

bool
iproc_reader_speciesparameters_xml_t::use_lresources()
const
{
    bool  use_lr = XML_ATTRIBUTE_VALUE( "useresources", true, NULL);
    if ( use_lr)
    {
        iproc_reader_speciesparameters_lrsrc_t const  rsrc_rdr;
        use_lr = rsrc_rdr.is_open();
    }
    return  use_lr;
}

iproc_reader_speciesparameters_xml_t::species_filing_e
iproc_reader_speciesparameters_xml_t::check_set_(
        ldndc_string_t *  _mnemonic,
        core_iobject_handle_t const *  _node)
const
{
    crabmeat_assert( _node);
    if ( !_node->ToElement()->Attribute( SPECIES_MNEMONIC))
    {
        /* missing mnemonic attribute */
        return  F_INVALID;
    }

    cbm::nstr  s_name( _node->ToElement()->Attribute( SPECIES_MNEMONIC));

    if ( cbm::is_empty( s_name.c_str()))
    {
        /* empty mnemonic attribute value */
        return  F_INVALID;
    }

    if ( cbm::is_equal( s_name.c_str(), SPECIES_MNEMONIC_META))
    {
        /* none type */
        if ( _mnemonic)
        {
            cbm::as_strcpy( _mnemonic, SPECIES_MNEMONIC_META);
        }
        return  F_NONE;
    }

    if ( _mnemonic)
    {
        int  rc_cpy = cbm::as_strcpy( _mnemonic, s_name.c_str());
        if ( rc_cpy != 0)
        {
            /* mnemonic too long */
            return  F_INVALID;
        }
    }

    return  F_REGULAR;
}

lerr_t
iproc_reader_speciesparameters_xml_t::get_species_default_properties(
        speciesparameters::speciesparameter_t  _props[],  ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    int  p_read = 0; int  p_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_speciesparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_species_default_properties(
                _props, _props_names, _props_types, _n_props, &p_read, &p_seen);
        if ( rc_read)
        {
            return LDNDC_ERR_FAIL;
        }
    }

    core_iobject_handle_t const *  node =
            helper_xml_walk_path_( this->get_handle(), 1, SPECIES_TAG);

    lerr_t  rc_read = LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    for ( /*, elem*/;  node;  node = node->NextSiblingElement( SPECIES_TAG))
    {
        if ( this->check_set_( NULL, node) == F_NONE)
        {
            rc_read = helper_xml_get_list_eq_< speciesparameters::speciesparameter_t >( 
                    node->ToElement(),
                    SPECIES_PARAMETER, SPECIES_PARAMETER_NAME, SPECIES_PARAMETER_VALUE,
                    _props_names, _props_types, _n_props,
                    _props, _n_props, 1,
                    0 /*offset*/, 1 /*stride*/,
                    _p_read, _p_seen,
                    NULL, NULL, XML_RFLAG_KEY_STRIP|XML_RFLAG_KEY_TO_UPPER);

            break;
        }
    }
    if (( rc_read == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) && this->use_lresources())
    {
        if ( _p_read) *_p_read = p_read;
        if ( _p_seen) *_p_seen = p_seen;
    }
    else if ( rc_read)
    {
        CBM_LogError( "iproc_reader_speciesparameters_xml_t::get_species_default_properties(): ",
                "error reading default species parameters  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


    
int
iproc_reader_speciesparameters_xml_t::species_child_(
        core_iobject_handle_t const *  _node)
const
{
    if ( !_node)
    {
        return  0;
    }
    core_iobject_handle_t const *  node = _node;
    int  m = 0;
    while ( node)
    {
        if ( this->check_set_( NULL, node) == F_REGULAR)
        {
            m += 1 + this->species_child_( node->FirstChildElement( SPECIES_TAG));
        }
        node = node->NextSiblingElement( SPECIES_TAG);
    }

    return  m;
}
size_t
iproc_reader_speciesparameters_xml_t::get_species_cnt()
const
{
// TODO  this is inconsistent: return total number, i.e. counting lresources if applicable
    if ( !this->get_handle())
    {
        return  invalid_t< size_t >::value;
    }
    core_iobject_handle_t const *  node =
        this->get_handle()->FirstChildElement( SPECIES_TAG);
    return  this->species_child_( node);
}


int
iproc_reader_speciesparameters_xml_t::set_single_species_meta_(
        speciesparameters::speciesparameters_meta_t *  _species,
        speciesparameters::speciesparameters_meta_t const *  _parent,
        core_iobject_handle_t const *  _node)
const
{
    crabmeat_assert( _node && _species);

    /* read remaining meta data */
    cbm::nstr  group( XML_ATTRIBUTE_VALUE_AT( _node->ToElement(),
        SPECIES_GROUP, ldndc_empty_string, NULL), CNAME_STRIP|CNAME_STRIP_TO_LOWER);
    if ( !_parent && cbm::is_empty( group.c_str()))
    {
        CBM_LogError( "missing species group attribute for top-ancestor.",
                    "  [species=",_species->mnemonic,"]");
        return  -1;
    }
    else if ( _parent && ( cbm::is_empty( group.c_str())
        || cbm::is_equal_i( group.c_str(), species::SPECIES_GROUP_NAMES[species::SPECIES_GROUP_NONE])))
    {
        _species->group = _parent->group;
    }
    else
    {
        unsigned int  i( 0);
        if ( cbm::find_index( group.c_str(), species::SPECIES_GROUP_NAMES, species::SPECIES_GROUP_CNT, &i) != LDNDC_ERR_OK)
        {
            CBM_LogError( "unknown species group  [object-id=",this->object_id(),",group=",group.c_str(),",species=",_species->mnemonic,"]");
            return  -1;
        }
        _species->group = (species::species_group_e)i;
        if (( _species->group == species::SPECIES_GROUP_ANY)
                && ( _parent && ( _parent->group != species::SPECIES_GROUP_ANY)))
        {
            CBM_LogError( "species group 'any' not allowed after group specialization",
                          "  [expect=",species::SPECIES_GROUP_NAMES[_parent->group],"]");
            return  -1;
        }
        else if ( _parent && ( _species->group != _parent->group)
                    && ( _parent->group != species::SPECIES_GROUP_ANY))
        {
            CBM_LogError( "species group must be identical along an inheritance line",
                          "  [mismatch for=",_species->mnemonic,",expect '",species::SPECIES_GROUP_NAMES[_parent->group],"']");
            return  -1;
        }
    }

    if ( _parent)
    {
        cbm::as_strcpy( &_species->parent, _parent->mnemonic);
    }
    else
    {
        cbm::as_strcpy( &_species->parent, SPECIES_MNEMONIC_META);
    }

    return  0;
}

int
iproc_reader_speciesparameters_xml_t::get_species_meta_(
        speciesparameters::parameterized_species_t *  _species, int *  _r, int  _r_limit,
        core_iobject_handle_t const *  _node, int  _r_parent)
const
{
    crabmeat_assert( _r);

    if ( *_r == -1)
    {
        /* errors occured */
        return  -1;
    }

    if ( !_node)
    {
        return  0;
    }

    int  s = 0;

    core_iobject_handle_t const *  node = _node;
    ldndc_string_t  s_name;

    while ( node)
    {
        species_filing_e  s_f = this->check_set_( &s_name, node);
        if ( s_f != F_REGULAR)
        {
            if ( s_f == F_INVALID)
            {
                ++s;
            }

            node = node->NextSiblingElement( SPECIES_TAG);
            continue;
        }

        /* increase seen blocks count */
        ++s;

        if ( *_r >= _r_limit)
        {
            /* "overflow" */
        }
        else
        {
            _species[*_r].m.group = species::SPECIES_GROUP_NONE;
            cbm::as_strcpy( &_species[*_r].m.parent, species::SPECIES_NAME_NONE);
            int  rc_cpy = cbm::as_strcpy( _species[*_r].m.mnemonic, s_name);
            if ( rc_cpy != 0)
            {
                CBM_LogError( "unexpected error while copying species mnemonic",
                              "  [species=",s_name,"]");
                *_r = -1;
                return  -1;
            }

            int  R = *_r;
            int  R_parent = _r_parent;
            for ( int  k = 0;  k < *_r;  ++k)
            {
                if ( cbm::is_equal(
                        _species[k].m.mnemonic, _species[*_r].m.mnemonic))
                {
                    CBM_LogVerbose( "refining species parameterization",
                        "  [object-id=",this->object_id(),",species=",_species[*_r].m.mnemonic,"]");

                    _species[k].merge_count += 1;
                    if ( _species[k].merge_count > 1)
                    {
                        CBM_LogError( "refining species multiple times (",_species[k].merge_count,").",
                                       " this might be caused by an illegal inheritance hierarchy",
                                    "  [object-id=",this->object_id(),",species=",_species[k].m.mnemonic,"]");
                        *_r = -1;
                        return  -1;
                    }
                    R = k;
                    --s;
                    if ( R_parent == -1)
                    {
                        int  k_parent = 0;
                        while ( k_parent < _r_limit)
                        {
                            if (( *_r != R_parent) && cbm::is_equal(
                                    _species[k_parent].m.mnemonic, _species[R].m.parent))
                            {
                                R_parent = k_parent;
                                break;
                            }
                            ++k_parent;
                        }
                    }
                    break;
                }
            }

            int  rc_set = this->set_single_species_meta_( &_species[R].m,
                    ( R_parent == -1 ) ? NULL : &_species[R_parent].m, node);
            if ( rc_set)
            {
                *_r = -1;
                return  -1;
            }
            if ( R == *_r)
            {
                ++*_r;
            }

            s += this->get_species_meta_(
                    _species, _r, _r_limit, node->FirstChildElement( SPECIES_TAG), R);
            if ( *_r == -1)
            {
                return  -1;
            }
        }

        node = node->NextSiblingElement( SPECIES_TAG);
    }

    return  s;
}
lerr_t
iproc_reader_speciesparameters_xml_t::get_species_meta(
        speciesparameters::parameterized_species_t *  _species, size_t  _s_cnt,
        int *  _r, int *  _s)
const
{
    if ( _s_cnt == 0)
    {
        return  LDNDC_ERR_OK;
    }
    if ( !this->get_handle())
    {
        return  LDNDC_ERR_FAIL;
    }

    core_iobject_handle_t const *  node =
            this->get_handle()->FirstChildElement( SPECIES_TAG);
    *_s = this->get_species_meta_(
            _species, _r, *_r+static_cast< int >( _s_cnt), node, -1);

    if ( *_r == -1)
    {
        return  LDNDC_ERR_FAIL;
    }
    if ( static_cast< size_t >( *_r) < _s_cnt)
    {
        CBM_LogError( "read less species than exspected  [expected=",_s_cnt,",read=",*_r,"]");
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    else if ( static_cast< size_t >( *_s) > _s_cnt)
    {
        CBM_LogWarn( "i have seen invalid species parameter blocks  [#=",*_s-_s_cnt,"]");
    }
    return  LDNDC_ERR_OK;
}

size_t
iproc_reader_speciesparameters_xml_t::check_species_properties_given(
        char const *  _species_name, ldndc::bitset *  _marker, size_t  _marker_cnt)
const
{
    core_iobject_handle_t const *  node( helper_xml_walk_path_( this->get_handle(), 1, SPECIES_TAG));
    InputElementType const *  elem = ( node) ? node->ToElement() : NULL;

    int  r( invalid_int);

    for ( ;  elem;  elem = elem->NextSiblingElement( SPECIES_TAG))
    {
        /* case sensitive */
        cbm::nstr  s_name( elem->Attribute( SPECIES_MNEMONIC), CNAME_STRIP);

        if ( cbm::is_equal( s_name.c_str(), _species_name))
        {
            (void)helper_xml_get_list_eq_size_( elem, SPECIES_PARAMETER, &r, NULL);
            if ( _marker  &&  _marker_cnt)
            {
                CBM_LogTodo( "sorry not implemented");
            }
            break;
        }
    }

    return  ( r == invalid_int) ? invalid_size : (size_t)r;
}


int
iproc_reader_speciesparameters_xml_t::get_species_properties_(
        speciesparameters::parameterized_species_t *  _species, size_t  _n_species,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        core_iobject_handle_t const *  _node)
const
{
    if ( !_node)
    {
        return  0;
    }

    core_iobject_handle_t const *  node = _node;
    ldndc_string_t  s_name;

    int  r = 0;

    while ( node)
    {
        species_filing_e  s_f = this->check_set_( &s_name, node);
        if ( s_f != F_REGULAR)
        {
            node = node->NextSiblingElement( SPECIES_TAG);
            continue;
        }

        for ( unsigned int  j = 0;  j < _n_species;  ++j)
        {
            if ( cbm::is_equal( s_name, _species[j].m.mnemonic))
            {
// sk:dbg                CBM_LogDebug( "reading properties for \"",s_name,"\"");
                /*lerr_t  rc =*/
                (void)helper_xml_get_list_eq_< speciesparameters::speciesparameter_t >( 
                        node->ToElement(),
                        SPECIES_PARAMETER, SPECIES_PARAMETER_NAME, SPECIES_PARAMETER_VALUE,
                        _props_names, _props_types, _n_props,
                        _species[j].p, _n_props, 1,
                        0 /*offset*/, 1 /*stride*/,
                        NULL, NULL,
                        NULL, NULL, XML_RFLAG_KEY_STRIP|XML_RFLAG_KEY_TO_UPPER);


                ++r;
                break;
            }
        }

        r += this->get_species_properties_(
                _species, _n_species, _props_names, _props_types, _n_props, node->FirstChildElement( SPECIES_TAG));

        node = node->NextSiblingElement( SPECIES_TAG);
    }

    return  r;
}

lerr_t
iproc_reader_speciesparameters_xml_t::get_all_species_properties(
        speciesparameters::parameterized_species_t **  _species, size_t *  _n_species,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _s_read, int *  _s_seen,
        int  /*_p_read*/[], int  /*_p_seen*/[])
const
{
    core_iobject_handle_t const *  node( helper_xml_walk_path_( this->get_handle(), 1, SPECIES_TAG));
    if ( !node)
    {
        return  LDNDC_ERR_OK;
    }
    if ( !_species || !_n_species)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    size_t const  species_count = this->get_species_cnt();
    if ( cbm::is_invalid( species_count))
    {
        return  LDNDC_ERR_FAIL;
    }
    CBM_LogDebug( "seeing ",species_count," species");
    size_t  species_count_max = species_count;

    if ( this->use_lresources())
    {
        iproc_reader_speciesparameters_lrsrc_t const  rsrc_rdr;
        size_t const  lrsrc_species_count = rsrc_rdr.get_species_cnt();
        if ( cbm::is_invalid( lrsrc_species_count))
        {
            return  LDNDC_ERR_FAIL;
        }
        species_count_max += lrsrc_species_count;
        CBM_LogDebug( "seeing additional species in Lresources=",lrsrc_species_count);
    }
    if ( species_count_max == 0)
    {
        *_n_species = 0;
        return  LDNDC_ERR_OK;
    }

    speciesparameters::parameterized_species_t *  species =
        CBM_DefaultAllocator->construct< speciesparameters::parameterized_species_t >( species_count_max);
    if ( !species)
    {
        return  LDNDC_ERR_NOMEM;
    }

    int  r = 0;
    int  s = 0;
    if ( this->use_lresources())
    {
        iproc_reader_speciesparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_rprops = rsrc_rdr.get_species_properties( species, rsrc_rdr.get_species_cnt(),
                _props_names, _props_types, _n_props, &r, &s, NULL, NULL);
        if ( rc_rprops)
        {
            CBM_DefaultAllocator->destroy( species, species_count_max);
            return  this->return_noinput( LDNDC_ERR_FAIL);
        }
    }

    lerr_t  rc_meta = this->get_species_meta( species, species_count, &r, &s);
    if ( rc_meta)
    {
        CBM_DefaultAllocator->destroy( species, species_count_max);
        return  this->return_noinput( LDNDC_ERR_FAIL);
    }
    if( r == 0)
    {
        CBM_LogDebug( "no species found");
        *_n_species = 0;
    }
    else if ( r == static_cast< int >( species_count_max))
    {
        CBM_LogDebug( "read ",r," species parameter sets");
        *_species = species;
        *_n_species = static_cast< size_t >( r);
    }
    else if ( r < static_cast< int >( species_count_max))
    {
        CBM_LogDebug( "merged species, resulting number smaller than maximum. reallocating. [r=",species_count_max,",r'=",r,"]");

        *_species = CBM_DefaultAllocator->construct< speciesparameters::parameterized_species_t >( r);
        if ( !*_species)
        {
            *_n_species = 0;
            return  this->return_noinput( LDNDC_ERR_NOMEM);
        }
        *_n_species = static_cast< size_t >( r);
        for ( int  i = 0;  i < r;  ++i)
        {
            (*_species)[i] = species[i];
        }
        CBM_DefaultAllocator->destroy( species, species_count_max);
    }
    else
    {
        return  this->return_noinput( LDNDC_ERR_FAIL);
    }

    lerr_t  rc_props = this->get_species_properties(
                *_species, *_n_species, _props_names, _props_types, _n_props,
                _s_read, _s_seen, NULL, NULL);
    if ( rc_props)
    {
        return  this->return_noinput( LDNDC_ERR_FAIL);
    }

    return  return_noinput( LDNDC_ERR_OK);
}

lerr_t
iproc_reader_speciesparameters_xml_t::get_species_properties(
                speciesparameters::parameterized_species_t  _species[], size_t  _n_species,
                ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
                int *  _s_read, int *  /*_s_seen*/,
                int  /*_p_read*/[], int  /*_p_seen*/[])
const
{
    core_iobject_handle_t const *  node =
            helper_xml_walk_path_( this->get_handle(), 1, SPECIES_TAG);
    if ( !node)
    {
        return  LDNDC_ERR_OK;
    }

    int  r = this->get_species_properties_(
                _species, _n_species, _props_names, _props_types, _n_props, node);
    if ( _s_read) { *_s_read = r; }
    if ( r < 0)
    {
        return  LDNDC_ERR_FAIL;
    }

    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}


/*  *****  SPECIES PARAMETER XML READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_speciesparameters_xml_t)
iproc_landscape_reader_speciesparameters_xml_t::iproc_landscape_reader_speciesparameters_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_speciesparameters_t()
{
}


iproc_landscape_reader_speciesparameters_xml_t::~iproc_landscape_reader_speciesparameters_xml_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_SPECIESPARAMETERS_XML_CPP_  */

