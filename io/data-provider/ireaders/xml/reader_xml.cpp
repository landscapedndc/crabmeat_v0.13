/*!
 * @brief
 *    concrete xml readers implementation
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
#include  "io/ifactory.h"

#include  "log/cbm_baselog.h"

namespace ldndc {
iproc_landscape_reader_xml_base_t::iproc_landscape_reader_xml_base_t()
        : iproc_landscape_reader_base_t(),
          iproc_handle_xml_types_t()
{
}


iproc_landscape_reader_xml_base_t::~iproc_landscape_reader_xml_base_t()
{
}


iproc_landscape_reader_xml_t::iproc_landscape_reader_xml_t()
        : iproc_landscape_reader_xml_base_t(),
          fhandler( NULL),
          hdl( NULL),
          status_( LDNDC_ERR_OK),
          imode_( iproc_landscape_reader_interface_t::IMODE_NONE)
{
}


iproc_landscape_reader_xml_t::~iproc_landscape_reader_xml_t()
{
    iproc_factories_t  ifactory;

    node_reader_map_t::iterator  ri = this->node_readers_.begin();
    for ( ;  ri != this->node_readers_.end();  ++ri)
    {
        if ( ri->second)
        {
            ri->second->delete_instance();
        }
    }
    this->node_infos_.clear();
}

int
iproc_landscape_reader_xml_t::scan_stream(
        iobject_handler_base_t *  _fhandler)
{
    bool const  is_rescan = this->fhandler != NULL;
    if ( !is_rescan)
    {
        crabmeat_assert( _fhandler);
        this->fhandler = dynamic_cast< iobject_handler_xml_t * >( _fhandler);
    }
    this->hdl = this->fhandler->get_iobject_handle();

    this->imode_ = iproc_landscape_reader_interface_t::IMODE_NONE;
    if ( !this->hdl)
    {
        CBM_LogFatal( "[BUG]  expecting valid xml handle");
    }

    for ( size_t  m = 0;  m < iproc_landscape_reader_interface_t::IMODE_CNT;  ++m)
    {
        iproc_landscape_reader_interface_t::input_stream_mode_e const  imode = static_cast< iproc_landscape_reader_interface_t::input_stream_mode_e >( m);
        if ( cbm::is_equal( this->hdl->Value(), this->root_tag( imode)))
        {
            this->imode_ = imode;
            break;
        }
    }

    /* judging by root tag, this is not a valid input stream */
    if ( this->imode_ == iproc_landscape_reader_interface_t::IMODE_NONE)
    {
        CBM_LogError( "invalid root tag, assuming incorrect input stream  [expected=",this->root_tag(iproc_landscape_reader_interface_t::IMODE_SINGLE),",got=",this->hdl->Value(),"]");
        return  -1;
    }

    return  this->scan_stream_( is_rescan);
}
int
iproc_landscape_reader_xml_t::rescan_stream()
{
    if ( !this->fhandler)
    {
        return  -1;
    }
    int const  rc_reload = this->fhandler->reload();
    if ( rc_reload)
    {
        return  -1;
    }

    return  this->scan_stream( this->fhandler);
}


size_t
iproc_landscape_reader_xml_t::number_of_blocks()
const
{
    return  this->node_infos_.size() + this->node_readers_.size();
}
bool
iproc_landscape_reader_xml_t::check_input_exists(
        lid_t const &  _block_id)
const
{
    crabmeat_assert( this->hdl);

// sk:dbg    CBM_LogDebug( "info-size=",this->node_infos_.size(), "  rdr-size=",this->node_readers_.size());

    return  ( this->node_infos_.find( _block_id) != this->node_infos_.end()) || ( this->node_readers_.find( _block_id) != this->node_readers_.end());
}

int
iproc_landscape_reader_xml_t::get_available_blocks(
        lid_t  _list[], size_t  _max_size)
const
{
    if ( !this->hdl)
    {
        return  -1;
    }

    size_t  c = 0;
    node_info_map_t::const_iterator  ni = this->node_infos_.begin();
    for ( ;  ni != this->node_infos_.end();  ++ni, ++c)
    {
        if ( c == _max_size)
        {
            return  static_cast< int >( _max_size);
        }
        _list[c] = ni->second.id;
    }

    node_reader_map_t::const_iterator  rdr = this->node_readers_.begin();
    for ( ;  rdr != this->node_readers_.end();  ++rdr)
    {
        if ( !rdr->second)
        {
            continue;
        }

        if ( c == _max_size)
        {
            return  static_cast< int >( _max_size);
        }
        _list[c] = rdr->first;

        ++c;
    }

    return  static_cast< int >( c);
}

iproc_provider_t *
iproc_landscape_reader_xml_t::acquire_ireader(
        lid_t const &  _id)
{
    /* find reader with given ID */
    if ( this->node_readers_.find( _id) != this->node_readers_.end())
    {
        return  this->node_readers_[_id];
    }
    if ( this->node_infos_.empty() || ( this->node_infos_.find( _id) == this->node_infos_.end()))
    {
        CBM_LogDebug( "no such node in input");
        return  NULL;
    }

    iproc_factories_t  ifactory;
    iproc_reader_xml_t *  this_reader = dynamic_cast< iproc_reader_xml_t * >( ifactory.construct_ireader( this->input_class_type(), this->io_format_type()));
    if ( !this_reader)
    {
        CBM_LogError( "nomem");
        return  NULL;
    }
    node_info_t  ni = this->node_infos_[_id];
    this_reader->set_object_id( ni.id);
    this_reader->set_handle( ni.handle);

    /* add to reader list */
    node_reader_map_insert_return_t  ins_status = this->node_readers_.insert( node_reader_map_t::value_type( _id, this_reader));
    if ( !ins_status.second)
    {
        CBM_LogError( "failed to insert reader into reader container");
        return  NULL;
    }
    /* remove from node info list */
    node_info_map_t::size_type  erase_count = this->node_infos_.erase( _id);
    if ( erase_count == 0)
    {
        CBM_LogWarn( "reader node info could not be erased from container. impossible situation.");
    }


    return  this_reader;
}
void
iproc_landscape_reader_xml_t::release_ireader(
        lid_t const &  /*_block_id*/)
{
// sk:TODO    iproc_provider_t *  rdr = this->acquire_ireader( _block_id);
// sk:TODO    if ( rdr)
// sk:TODO    {
// sk:TODO        ifactory.destroy_ireader( rdr);
// sk:TODO    }
}

lerr_t
iproc_landscape_reader_xml_t::add_to_processing_stack(
        iproc_provider_t **  _rdr_ptr,
        iproc_provider_t *  _iprocs)
{
// sk:TODO  provide for synthesizer only input
    if ( !_rdr_ptr)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    /* verify existence */
    iproc_provider_t const *  rdr = this->acquire_ireader( (*_rdr_ptr)->object_id());
    if ( rdr && ( rdr == *_rdr_ptr) && ( rdr != _iprocs))
    {
        this->node_readers_[(*_rdr_ptr)->object_id()] = _iprocs;
        *_rdr_ptr = _iprocs;
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}



iproc_handle_xml_types_t::core_iobject_handle_t *
iproc_landscape_reader_xml_t::core_handle_begin_()
const
{
    crabmeat_assert( this->hdl);
    switch ( this->input_stream_mode())
           {
        case IMODE_SINGLE:
        {
            return  this->hdl;
        }
        case IMODE_MULTI:
        {
            return  this->hdl->FirstChildElement( this->core_tag());
        }
        default:
        {
            CBM_LogError( "reader failed to get core node. bye bye.");
            return  NULL;
        }
    }
}

iproc_handle_xml_types_t::core_iobject_handle_t *
iproc_landscape_reader_xml_t::core_handle_next_(
        core_iobject_handle_t *  _hdl)
const
{
    switch ( this->input_stream_mode())
           {
        case IMODE_SINGLE:
        {
            break;
        }
        case IMODE_MULTI:
        {
            if ( _hdl)
                   {
                return  _hdl->ToElement()->NextSiblingElement( this->core_tag());
            }
            break;
        }
        default:
        {
            CBM_LogFatal( "reader failed to get core node. bye bye.");
        }
    }

    return  NULL;
}


iproc_landscape_reader_xml_t::input_stream_mode_e
iproc_landscape_reader_xml_t::input_stream_mode()
const
{
    return  this->imode_;
}


int
iproc_landscape_reader_xml_t::collective_update_commence(
        cbm::sclock_t const *)
{
    crabmeat_assert(this->fhandler);
    int  rc_reopen = 0;
    if ( this->fhandler->get_source_info()->is_mutable())
    {
        rc_reopen = this->rescan_stream();
    }
    return  rc_reopen;
}
int
iproc_landscape_reader_xml_t::collective_update_complete(
        cbm::sclock_t const *)
{
    crabmeat_assert(this->fhandler);
    if ( this->fhandler->get_source_info()->is_mutable())
    {
        this->fhandler->close();
    }
    return  0;
}


int
iproc_landscape_reader_xml_t::insert_node_info_(
        node_info_t &  _ni)
{
    node_info_map_insert_return_t  ins_status = this->node_infos_.insert( node_info_map_t::value_type( _ni.id, _ni));
    if ( !ins_status.second)
    {
        CBM_LogError( "failed to insert node information into map  [identifier=",this->fhandler->info()->identifier,"]");
        return  -1;
    }
    return  0;
}
int
iproc_landscape_reader_xml_t::configure_node_(
        node_info_t &  _ni)
{
    if ( this->node_infos_.find( _ni.id) != this->node_infos_.end())
    {
        CBM_LogError( "non-unique ID in input source  [identifier=",this->fhandler->info()->identifier,"]");
        return  -1;
    }
    return  this->insert_node_info_( _ni);
}
int
iproc_landscape_reader_xml_t::reconfigure_node_(
        node_info_t &  _ni)
{
    node_info_map_t::iterator  b_inf = this->node_infos_.find( _ni.id);
    if ( b_inf == this->node_infos_.end())
    {
        node_reader_map_t::iterator  b_rdr = this->node_readers_.find( _ni.id);
        if ( b_rdr == this->node_readers_.end())
        {
            /* insert as new block reader */
            int const  c_i = this->insert_node_info_( _ni);
            if ( c_i != 0)
            {
                return  -1;
            }
        }
        else
        {
            if ( b_rdr->second->to_reader())
            {
                static_cast< iproc_reader_xml_t * >( b_rdr->second->to_reader())->set_handle( _ni.handle);
            }
        }
    }
    else
    {
        b_inf->second.handle = _ni.handle;
    }

    return  0;
}
int
iproc_landscape_reader_xml_t::scan_stream_(
        bool  _is_rescan)
{
    CBM_LogDebug( "scanning source ",this->fhandler->info()->identifier," ...");
    this->invalidate_node_handles_();

    int  c = 0;
    core_iobject_handle_t *  this_hdl = this->core_handle_begin_();
    while ( this_hdl)
    {
        ++c;

        lid_t  this_id = XML_ATTRIBUTE_VALUE_AT( this_hdl, "id", 0, NULL);
        CBM_LogDebug( "finding core: ",this_id);

        node_info_t  ni;
        ni.id = this_id;
        ni.handle = this_hdl;

        int const  c_node = ( _is_rescan) ? this->reconfigure_node_( ni) : this->configure_node_( ni);
        if ( c_node)
        {
            c = -1;
            break;
        }

        this_hdl = this->core_handle_next_( this_hdl);
    }

// at this point we only support all readers to be valid
    for ( node_reader_map_t::iterator  b_rdr = this->node_readers_.begin();  b_rdr != node_readers_.end();  ++b_rdr)
    {
        if ( !b_rdr->second->data_available())
        {
            CBM_LogError( "currently, we only support all readers to be valid: i.e. for each reader a source block must exist");
            return  -1;
        }
    }


    return  c;
}

void
iproc_landscape_reader_xml_t::invalidate_node_handles_()
{
    /* blocks of old source no longer relevant */
    this->node_infos_.clear();
    /* readers handles are invalidated */
    for ( node_reader_map_t::iterator  b_rdr = this->node_readers_.begin();  b_rdr != this->node_readers_.end();  ++b_rdr)
    {
        if ( b_rdr->second->to_reader())
        {
            static_cast< iproc_reader_xml_t * >( b_rdr->second->to_reader())->set_handle( NULL);
        }
    }
}




iproc_reader_xml_base_t::iproc_reader_xml_base_t()
        : iproc_reader_base_t(),
          iproc_handle_xml_types_t()
{
}
iproc_reader_xml_base_t::~iproc_reader_xml_base_t()
{
}



iproc_reader_xml_t::iproc_reader_xml_t()
        : iproc_reader_xml_base_t(),
          hdl_( NULL)
{
}
iproc_reader_xml_t::~iproc_reader_xml_t()
{
}

lerr_t
iproc_reader_xml_t::set_handle(
        core_iobject_handle_t *  _hdl)
{
    /* no need for copy here, right!? */
    this->hdl_ = _hdl;
    this->data_arrived = ( _hdl) ? 1 : 0;
    
           return  LDNDC_ERR_OK;
}

iproc_reader_xml_t::core_iobject_handle_t *
iproc_reader_xml_t::get_handle()
{
    return  this->hdl_;
}
iproc_reader_xml_t::core_iobject_handle_t const *
iproc_reader_xml_t::get_handle()
const
{
    return  this->hdl_;
}

lid_t const &
iproc_reader_xml_t::object_id()
const
{
    if ( objectid_owner_t::object_id() != invalid_lid)
    {
        return  objectid_owner_t::object_id();
    }
        const_cast< iproc_reader_xml_t * > ( this)->set_object_id( XML_ATTRIBUTE_VALUE( "id", invalid_lid, NULL));
    return  objectid_owner_t::object_id();
}
}

#undef  XML_ATTRIBUTE_VALUE_
#undef  XML_ATTRIBUTE_VALUE
#undef  XML_ATTRIBUTE_VALUE_AT

