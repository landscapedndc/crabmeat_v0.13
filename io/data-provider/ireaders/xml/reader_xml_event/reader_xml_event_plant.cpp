
#include  "input/species/species-srv.h"
#include  "input/speciesparameters/speciesparameters-srv.h"
#include  "io/data-provider/ireaders/xml/reader_xml_speciesparameters.h"
#include  "memory/cbm_mem.h"

namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(plant)
{
    EV_ATTRS_READER_HEADER(plant);

    EV_ATTR_U( type, "type");
    if ( cbm::is_empty( attrs->type) || cbm::is_invalid( attrs->type))
    {
        CBM_LogError( "plant event is missing 'type' attribute or attribute not set");
        return  LDNDC_ERR_FAIL;
    }
    /* name defaults to type */
    EV_ATTR_U( name, "name");
    if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
    {
        attrs->name = attrs->type;
    }
    EV_ATTR( longname, "longname");
    if ( cbm::is_empty( attrs->longname) || cbm::is_invalid( attrs->longname))
    {
        /* long name defaults to name (string is not altered) */
        EV_ATTR( longname, "name");
        if ( cbm::is_empty( attrs->longname) || cbm::is_invalid( attrs->longname))
        {
            /* long name defaults to type (string is not altered) */
            EV_ATTR( longname, "type");
        }
    }

    EV_ATTR( location, "location");

    /* check for group specific properties set (first one is used, rest skipped!) */
    attrs->group = species::SPECIES_GROUP_NONE;
    for ( size_t  k = 0;  k < species::SPECIES_GROUP_CNT;  ++k)
    {
        if ( CHECK_ELEMENT( species::SPECIES_GROUP_NAMES[k]))
        {
// sk:dbg            CBM_LogDebug( "found species properties  [group=", species::SPECIES_GROUP_NAMES[k],"]");
            attrs->group = (species::species_group_e)k;
            break;
        }
    }
    iproc_handle_xml_types_t::core_iobject_handle_t const *  ev_node_bkp( ev_node);
    switch ( attrs->group)
           {
        case species::SPECIES_GROUP_NONE:
        {
            /* nothing to be read (found nothing or found "none" block ;-) ) */
            CBM_LogInfo( "no properties given, trying to find 'group' attribute...");
            std::string  g( invalid_str);
            unsigned int  i( invalid_uint);
            EV_ATTR_L_( g, "group", invalid_string);
            if ( cbm::find_index( g.c_str(), species::SPECIES_GROUP_NAMES, species::SPECIES_GROUP_CNT, &i) == LDNDC_ERR_OK)
                   {
                CBM_LogInfo( "found attribute group=\"",species::SPECIES_GROUP_NAMES[i],"\" for species ",attrs->name);
                attrs->group = (species::species_group_e)i;
            }
            /* indicate missing: possibly fall back to defaults later */
            attrs->properties = NULL;
            break;
        }
        case species::SPECIES_GROUP_CROP:
        {
            ev_node = helper_xml_walk_path_( ev_node, 1, species::SPECIES_GROUP_NAMES[attrs->group]);
            attrs->crop = species::crop_properties_default;
            /* read general species properties */
            EV_ATTR_( attrs->crop.fractional_cover, "fractionalcover", invalid_dbl);
            EV_ATTR_( attrs->crop.initial_biomass, "initialbiomass", invalid_dbl);
            /* read crop properties */
            std::string  cc( "");
            EV_ATTR_( cc, "covercrop", cbm::lstring_false);
            if ( !cbm::is_bool( cc))
            {
                CBM_LogError( "attribute value not boolean  [argument=covercrop,value=",cc,"]");
                return  LDNDC_ERR_INVALID_ARGUMENT;
            }
            attrs->crop.cover_crop = cbm::is_bool_true( cc);
            EV_ATTR_( attrs->crop.seedbed_duration, "seedbedduration", 0);
            EV_ATTR_( attrs->crop.seedling_number, "seedlingnumber", 0.0);
            break;
        }
        case species::SPECIES_GROUP_GRASS:
        {
                        ev_node = helper_xml_walk_path_( ev_node, 1, species::SPECIES_GROUP_NAMES[attrs->group]);
            attrs->grass = species::grass_properties_default;
            /* read general species properties */
            EV_ATTR_( attrs->grass.fractional_cover, "fractionalcover", invalid_dbl);
                        EV_ATTR_( attrs->grass.initial_biomass, "initialbiomass", invalid_dbl);
            /* read grass properties */
            std::string  cc( "");
            EV_ATTR_( cc, "covercrop", cbm::lstring_false);
            if ( !cbm::is_bool( cc))
            {
                CBM_LogError( "attribute value not boolean  [argument=covercrop,value=",cc,"]");
                return  LDNDC_ERR_INVALID_ARGUMENT;
            }
            attrs->grass.cover_crop = cbm::is_bool_true( cc);

            EV_ATTR_( attrs->grass.height_max, "hmax", invalid_dbl);
            EV_ATTR_( attrs->grass.height_max, "heightmax", attrs->grass.height_max);
            EV_ATTR_( attrs->grass.height_min, "hmin", invalid_dbl);
            EV_ATTR_( attrs->grass.height_min, "heightmin", attrs->grass.height_min);
            EV_ATTR_( attrs->grass.root_depth, "depth", invalid_dbl);
            EV_ATTR_( attrs->grass.root_depth, "rootingdepth", attrs->grass.root_depth);
            break;
        }
        case species::SPECIES_GROUP_WOOD:
        {
            ev_node = helper_xml_walk_path_( ev_node, 1, species::SPECIES_GROUP_NAMES[attrs->group]);
            attrs->wood = species::wood_properties_default;
            /* read general species properties */
            EV_ATTR_( attrs->wood.fractional_cover, "fractionalcover", invalid_dbl);
            EV_ATTR_( attrs->wood.initial_biomass, "initialbiomass", invalid_dbl);
            /* read wood properties */
            EV_ATTR_( attrs->wood.dbh, "dbh", invalid_dbl);
            EV_ATTR_( attrs->wood.height_max, "hmax", invalid_dbl);
            EV_ATTR_( attrs->wood.height_max, "heightmax", attrs->wood.height_max);
            EV_ATTR_( attrs->wood.height_min, "hmin", invalid_dbl);
            EV_ATTR_( attrs->wood.height_min, "heightmin", attrs->wood.height_min);
            EV_ATTR_( attrs->wood.reduc_fac_c, "fnit", invalid_dbl);
            EV_ATTR_( attrs->wood.root_depth, "depth", invalid_dbl);
            EV_ATTR_( attrs->wood.root_depth, "rootingdepth", attrs->wood.root_depth);
            EV_ATTR_( attrs->wood.number, "ntree", invalid_dbl);
            EV_ATTR_( attrs->wood.number, "treenumber", attrs->wood.number);
            EV_ATTR_( attrs->wood.volume, "vtree", invalid_dbl);
            EV_ATTR_( attrs->wood.volume, "treevolume", attrs->wood.volume);
            break;
        }
        default:
        {
            /* you should not get here */
            CBM_LogFatal( "ouch! this is embarrassing; ...and i thought we could not get here.");
            return  LDNDC_ERR_FAIL;
        }
    }
    /* reset position for further use */
    ev_node = ev_node_bkp;

    attrs->params = NULL;
    /* check for species parameters */
    if ( CHECK_ELEMENT( "params"))
           {
        /* source parameter block */
        iproc_handle_xml_types_t::core_iobject_handle_t const *  par_src( helper_xml_walk_path_( ev_node, 1, "params"));
        /* check for more than param block */
        if ( par_src->NextSiblingElement( "params"))
        {
            CBM_LogWarn( "seen more than one parameter block in event: using first");
        }

        /* prepare parameter block to be handled by species parameter core reader */
        iproc_handle_xml_types_t::iobject_type_t  par_doc;
        (void)par_doc.Parse( "<paramset/>");
        if ( !par_doc.RootElement())
        {
            return  LDNDC_ERR_OBJECT_INIT_INCOMPLETE;
        }
        iproc_handle_xml_types_t::core_iobject_handle_t *  par_dest( helper_xml_new_element_( "species", &par_doc));
        par_dest->ToElement()->SetAttribute( iproc_reader_speciesparameters_xml_t::SPECIES_MNEMONIC, attrs->type.c_str());

        par_doc.RootElement()->XML_LINK_END_CHILD( par_dest);

        iproc_handle_xml_types_t::core_iobject_handle_t const *  par_node( par_src->FirstChildElement( iproc_reader_speciesparameters_xml_t::SPECIES_PARAMETER));
        if ( !par_node)
        {
            return  LDNDC_ERR_OK;
        }

        while ( par_node)
        {
            par_dest->XML_LINK_END_CHILD( par_node->XML_CLONE_NODE( &par_doc));
            par_node = par_node->NextSiblingElement( iproc_reader_speciesparameters_xml_t::SPECIES_PARAMETER);
        }

        /* create reader */
        iproc_reader_speciesparameters_xml_t  spr;
        spr.set_handle( par_doc.RootElement());

        size_t  param_cnt( spr.check_species_properties_given( attrs->type.c_str()));
        if ( param_cnt != invalid_size)
               {
            /* only allocate if at least one parameter is given */
            attrs->params = CBM_DefaultAllocator->construct< speciesparameters::parameterized_species_t >();
            if ( !attrs->params)
            {
                CBM_LogError( "nomem");
                return  LDNDC_ERR_NOMEM;
            }
            speciesparameters::parameterized_species_t  p;
            p.m = speciesparameters::speciesparameters_meta_t( attrs->type.c_str(), NULL, attrs->group);
            *attrs->params = p;
            /* read species parameters */
            lerr_t  rc_read_properties = spr.get_species_properties(
                attrs->params, 1u,
                speciesparameters::SPECIESPARAMETERS_NAMES,
                speciesparameters::SPECIESPARAMETERS_TYPES,
                speciesparameters::SPECIESPARAMETERS_CNT,
                       NULL, NULL, NULL, NULL);
            RETURN_IF_NOT_OK(rc_read_properties);
        }
               else
               {
            attrs->params = NULL;
        }
    }
    /* reset position for further use (we keep this here for code that might follow..) */
    ev_node = ev_node_bkp;

    return  LDNDC_ERR_OK;
}
}

