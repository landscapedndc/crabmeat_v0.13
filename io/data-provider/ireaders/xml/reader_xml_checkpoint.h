/*!
 * @brief
 *    concrete xml readers
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  CBM_IREADER_XML_CHECKPOINT_H_
#define  CBM_IREADER_XML_CHECKPOINT_H_

#include  "io/data-provider/ireaders/xml/reader_xml.h"
#include  "io/iif/iif_checkpoint.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_checkpoint_xml_t
    :  public  iproc_landscape_reader_xml_t,  public  iproc_landscape_interface_checkpoint_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_checkpoint_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_landscape_interface_checkpoint_t::input_class_type(); }

        static  char const *  ROOT_TAGS[IMODE_CNT];
        char const *  root_tag( input_stream_mode_e  _imode = IMODE_CNT)
        const
        {
            crabmeat_assert( _imode < IMODE_CNT);
            return  ROOT_TAGS[_imode];
        }

        char const *  core_tag() const { return  core_tag_(); }
    private:
        static  char const *  core_tag_() { return  iproc_landscape_reader_checkpoint_xml_t::ROOT_TAGS[IMODE_SINGLE]; }

    public:
        iproc_landscape_reader_checkpoint_xml_t();
        virtual  ~iproc_landscape_reader_checkpoint_xml_t();
};
class  CBM_API  iproc_reader_checkpoint_xml_t  :  public  iproc_reader_xml_t,  public  iproc_interface_checkpoint_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_checkpoint_xml_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_checkpoint_t::input_class_type(); }

        iproc_reader_checkpoint_xml_t();
        virtual  ~iproc_reader_checkpoint_xml_t();

    public:
        lerr_t  retrieve_record(
                ldndc::checkpoint::checkpoint_buffer_t * /*buffer*/);
};
}


#endif  /*  !CBM_IREADER_XML_CHECKPOINT_H_  */

