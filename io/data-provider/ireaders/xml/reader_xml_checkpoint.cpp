
#ifndef  LDNDC_IO_IREADER_CHECKPOINT_XML_CPP_
#define  LDNDC_IO_IREADER_CHECKPOINT_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_checkpoint.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"

namespace ldndc {
char const *  iproc_landscape_reader_checkpoint_xml_t::ROOT_TAGS[iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndccheckpoint", "checkpoint"};

LDNDC_SERVER_OBJECT_DEFN(iproc_reader_checkpoint_xml_t)
iproc_reader_checkpoint_xml_t::iproc_reader_checkpoint_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_checkpoint_t()
{
}
iproc_reader_checkpoint_xml_t::~iproc_reader_checkpoint_xml_t()
{
}


lerr_t
iproc_reader_checkpoint_xml_t::retrieve_record(
        ldndc::checkpoint::checkpoint_buffer_t *  _buf)
{
    if ( !_buf || !_buf->entity_class || !_buf->entity_name)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    _buf->buffer = NULL;
    _buf->n_buffer = 0;

    core_iobject_handle_t const *  node = helper_xml_walk_path_( 
        this->get_handle(), 2, _buf->entity_class, _buf->entity_name);
    if ( !node)
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }

    sink_record_meta_t  m;
    m.timestamp_from_scalar( _buf->timestamp);

    node = node->FirstChildElement( "value");
    for ( ;  node;  node = node->NextSiblingElement( "value"))
    {
        char const *  timestamp =
            node->ToElement()->Attribute( "time");
        unsigned int  year=0, month=0, day=0, subday=0;
        sscanf( timestamp, "%4u-%02u-%02u-%02u",
                &year, &month, &day, &subday);

        if ( year==m.t.reg.year && month==m.t.reg.month
            && day==m.t.reg.day && subday==m.t.reg.subday)
        {
            if ( node->ToElement()->GetText())
            {
                _buf->buffer = cbm::strdup(
                        node->ToElement()->GetText());
                _buf->n_buffer = cbm::strlen( _buf->buffer);
                return  LDNDC_ERR_OK;
            }
        }
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}


/*  *****  CHECKPOINT XML READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_checkpoint_xml_t)
iproc_landscape_reader_checkpoint_xml_t::iproc_landscape_reader_checkpoint_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_checkpoint_t()
{
}


iproc_landscape_reader_checkpoint_xml_t::~iproc_landscape_reader_checkpoint_xml_t()
{
}


}

#endif  /*  !LDNDC_IO_IREADER_CHECKPOINT_XML_CPP_  */

