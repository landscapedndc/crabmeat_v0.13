
#ifndef  LDNDC_IO_IREADER_SETUP_XML_CPP_
#define  LDNDC_IO_IREADER_SETUP_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_setup.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
#include  "geom/cbm_geom.h"

#include  "xml/xml2json/xml2json.hpp"

char const *  ldndc::iproc_landscape_reader_setup_xml_t::ROOT_TAGS[ldndc::iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcsetup", "setup"};

namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_setup_xml_t)
iproc_reader_setup_xml_t::iproc_reader_setup_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_setup_t()
{
}
iproc_reader_setup_xml_t::~iproc_reader_setup_xml_t()
{
}


std::string
iproc_reader_setup_xml_t::get_site_name()
const
{
    return  XML_ATTRIBUTE_VALUE( "name", ldndc_empty_string, NULL);
}

lerr_t
iproc_reader_setup_xml_t::get_section(
        cbm::string_t *  _buffer, char const *  _section) const
{
    crabmeat_assert( _buffer);
    crabmeat_assert( _section);

    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  node =
        helper_xml_walk_path_( this->get_handle(), 1, _section);
    if ( node)
    {
        /*convert xml block to string */
        tinyxml2::XMLPrinter  xml_printer;
        node->Accept( &xml_printer);
// sk:dbg        printf( "\nXML:\n'%s'\n\n", xml_printer.CStr());

        /*convert xml string to json string */
        *_buffer = xml2json( xml_printer.CStr());
// sk:dbg        printf( "\nJSON:\n'%s'\n\n", _buffer->c_str());
    }
    else
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
iproc_reader_setup_xml_t::blocks_consume(
        cbm::string_t &  _targets, int *  _r, int *  _s)
const
{
    return  this->blocks_consume_produce_( _targets, _r, _s, "use");
}
lerr_t
iproc_reader_setup_xml_t::blocks_produce(
        cbm::string_t &  _targets, int *  _r, int *  _s)
const
{
    return  this->blocks_consume_produce_( _targets, _r, _s, "provide");
}
lerr_t
iproc_reader_setup_xml_t::blocks_consume_produce_(
        cbm::string_t &  _targets, int *  _r, int *  _s,
        char const *  _target_name)
const
{
    _targets.clear();

    int  r = 0, s = 0;

    ldndc::iproc_handle_xml_types_t::core_iobject_handle_t const *  xml_node =
        helper_xml_walk_path_( this->get_handle(), 1, _target_name);
    tinyxml2::XMLElement const *  node = NULL;
    if ( xml_node)
    {
        node = xml_node->ToElement();
        if ( node)
            { node = node->FirstChildElement(); }
    }
    while ( node)
    {
        if ( node->Attribute( "id"))
            { r+= 1; }
        else
            { continue; /* invalid entry */}

        _targets << node->Value();

        _targets << " ";
        if ( node->Attribute( "source"))
            { _targets << node->Attribute( "source"); }
        else
            { _targets << invalid_string; }

        _targets << " ";
        if ( node->Attribute( "id"))
            { _targets << node->Attribute( "id"); }
        else
            { _targets << invalid_lid; }

        s += 1;

        node = node->NextSiblingElement();
    }

    if ( _r)
        { *_r = r; }
    if ( _s)
        { *_s = s; }

    return  LDNDC_ERR_OK;
}


/***  discretization  ***/
int
iproc_reader_setup_xml_t::get_canopylayers()
const
{
    return  XML_ATTRIBUTE_VALUE( "canopylayers", invalid_int, "discretization");
}

int
iproc_reader_setup_xml_t::get_soillayers()
const
{
    return  XML_ATTRIBUTE_VALUE( "soillayers", invalid_int, "discretization");
}

/***  geographic  ***/
double
iproc_reader_setup_xml_t::get_elevation()
const
{
    return  XML_ATTRIBUTE_VALUE( "elevation", invalid_dbl, "location");
}

double
iproc_reader_setup_xml_t::get_latitude()
const
{
    return  XML_ATTRIBUTE_VALUE( "latitude", invalid_dbl,  "location");
}

double
iproc_reader_setup_xml_t::get_longitude()
const
{
    return  XML_ATTRIBUTE_VALUE( "longitude", invalid_dbl, "location");
}

int
iproc_reader_setup_xml_t::get_timezone()
const
{
    int  timezone = XML_ATTRIBUTE_VALUE( "timezone", invalid_int, "location");
    if ( cbm::is_invalid( timezone))
    {
        timezone = XML_ATTRIBUTE_VALUE( "zone", invalid_int, "location");
        if ( cbm::is_valid( timezone))
        {
            CBM_LogWarn( "setup input: attribute name \"zone\" is deprecated; use \"timezone\"");
        }
    }
    return timezone;
}

double
iproc_reader_setup_xml_t::get_slope()
const
{
    return  XML_ATTRIBUTE_VALUE( "slope", invalid_dbl, "location");
}

double
iproc_reader_setup_xml_t::get_aspect()
const
{
    return  XML_ATTRIBUTE_VALUE( "aspect", invalid_dbl, "location");
}

lerr_t
iproc_reader_setup_xml_t::get_centroid(
        cbm::geom_coord_t *  _p)
const
{
    _p->x = XML_ATTRIBUTE_VALUE( "x", invalid_dbl, "topology");
    _p->y = XML_ATTRIBUTE_VALUE( "y", invalid_dbl, "topology");
    _p->z = XML_ATTRIBUTE_VALUE( "z", invalid_dbl, "topology");

    return  LDNDC_ERR_OK;
}

lerr_t
iproc_reader_setup_xml_t::get_boundingbox(
            cbm::geom_bounding_box_t *  _bb)
const
{
    cbm::geom_coord_t  c;
    this->get_centroid( &c);
    if ( cbm::is_invalid( c.x))
        { c.x = 0.0; }
    if ( cbm::is_invalid( c.y))
        { c.y = 0.0; }
    if ( cbm::is_invalid( c.z))
        { c.z = 0.0; }

    lerr_t  rc_bbox = LDNDC_ERR_OK;

    cbm::geom_coord_t  len;
    len.x = XML_ATTRIBUTE_VALUE( "dx", invalid_dbl, "topology");
    if ( cbm::is_invalid( len.x))
        { len.x = 1.0; }
    if ( len.x < 0.0)
        { rc_bbox = LDNDC_ERR_INVALID_ARGUMENT; }
    len.y = XML_ATTRIBUTE_VALUE( "dy", invalid_dbl, "topology");
    if ( cbm::is_invalid( len.y))
        { len.y = 1.0; }
    if ( len.y < 0.0)
        { rc_bbox = LDNDC_ERR_INVALID_ARGUMENT; }
    len.z = XML_ATTRIBUTE_VALUE( "dz", invalid_dbl, "topology");
    if ( cbm::is_invalid( len.z))
        { len.z = 1.0; }
    if ( len.z < 0.0)
        { rc_bbox = LDNDC_ERR_INVALID_ARGUMENT; }

    _bb->ul.x = c.x-len.x/2.0;
    _bb->ul.y = c.y-len.y/2.0;
    _bb->ul.z = c.z+len.z/2.0;

    _bb->lr.x = c.x+len.x/2.0;
    _bb->lr.y = c.y+len.y/2.0;
    _bb->lr.z = c.z-len.z/2.0;

    return  rc_bbox;
}

double
iproc_reader_setup_xml_t::get_area()
const
{
    return  XML_ATTRIBUTE_VALUE( "area", invalid_dbl, "topology");
}

double
iproc_reader_setup_xml_t::get_volume()
const
{
    return  XML_ATTRIBUTE_VALUE( "volume", invalid_dbl, "topology");
}

size_t
iproc_reader_setup_xml_t::get_number_of_neighbors()
const
{
    int  n_neighbors = invalid_int;
    if ( helper_xml_walk_path_( this->get_handle(), 2, "topology", "neighbors") == NULL)
    {
        return  invalid_size;
    }

    (void)helper_xml_get_list_eq_size_( 
            helper_xml_walk_path_( this->get_handle(), 2, "topology", "neighbors"),
            "neighbor", &n_neighbors, NULL);

    return  ( n_neighbors == invalid_int) ? invalid_size : (size_t)n_neighbors;
}
lerr_t
iproc_reader_setup_xml_t::get_neighbors(
        cbm::neighbor_t  _neighbors[], size_t  _n)
const
{
    lid_t *  neighbor_id = CBM_DefaultAllocator->allocate_init_type< lid_t >( _n, invalid_lid);
    if ( !neighbor_id)
    {
        return  LDNDC_ERR_FAIL;
    }
    std::string *  neighbor_source = CBM_DefaultAllocator->construct< std::string >( _n);
    if ( !neighbor_source)
    {
        CBM_DefaultAllocator->deallocate( neighbor_id);
    }
    double *  neighbor_intersect = CBM_DefaultAllocator->allocate_init_type< double >( _n, 0.0);
    if ( !neighbor_intersect)
    {
        CBM_DefaultAllocator->deallocate( neighbor_id);
        CBM_DefaultAllocator->destroy( neighbor_source);
        return  LDNDC_ERR_FAIL;
    }

    int  r = 0, s = 0;

    lerr_t  rc_id = helper_xml_get_list_eq_(
            helper_xml_walk_path_( this->get_handle(), 2, "topology", "neighbors"),
            "neighbor", NULL, "id",
            NULL, invalid_size,
            neighbor_id, _n, 1,
            0, 1,
            &r, &s, NULL, NULL, XML_RFLAG_KEY_STRIP);
        lerr_t  rc_source = helper_xml_get_list_eq_(
                        helper_xml_walk_path_( this->get_handle(), 2, "topology", "neighbors"),
            "neighbor", NULL, "source",
            NULL, invalid_size,
                        neighbor_source, _n, 1,
            0, 1,
                        &r, &s, NULL, NULL, XML_RFLAG_KEY_STRIP);
    lerr_t  rc_intersect = helper_xml_get_list_eq_(
            helper_xml_walk_path_( this->get_handle(), 2, "topology", "neighbors"),
            "neighbor", NULL, "intersect",
            NULL, invalid_size,
            neighbor_intersect, _n, 1,
            0, 1,
            &r, &s, NULL, NULL, XML_RFLAG_KEY_STRIP);

    lerr_t  rc = LDNDC_ERR_OK;
    if ( rc_id || rc_source || rc_intersect)
    {
        rc = LDNDC_ERR_FAIL;
    }
    else
    {
        for ( size_t  n = 0;  n < _n;  ++n)
        {
            int  rc_set = cbm::set_source_descriptor(
                    &_neighbors[n].desc, neighbor_id[n],
                    ( neighbor_source[n].empty() ? "setup" : neighbor_source[n].c_str()));
            if ( rc_set)
            {
                CBM_LogWarn( "setting source identifier failed  [source-identifier=",neighbor_source[n],"]");
                rc = LDNDC_ERR_FAIL;
            }
            _neighbors[n].intersect = neighbor_intersect[n];
        }
    }
    CBM_DefaultAllocator->deallocate( neighbor_id);
    CBM_DefaultAllocator->destroy( neighbor_source);
    CBM_DefaultAllocator->deallocate( neighbor_intersect);

    return  rc;
}

cbm::geo_orientations_t
iproc_reader_setup_xml_t::get_mooreboundaries()
const
{
    cbm::string_t  boundaries =
        XML_ATTRIBUTE_VALUE( "mooreboundaries", invalid_string, "topology", "neighbors");
    cbm::geo_orientations_t  orientation( cbm::ORIENT_NONE);
    lerr_t  rc_orient =
        cbm::geo_orientation_t::mooreboundaries_from_string( boundaries, &orientation);
    if ( rc_orient)
    {
            CBM_LogError( "error when parsing Moore-boundaries list",
                  "  [id=",this->object_id(),",moore-boundaries=",boundaries,"]");
    }
    return  orientation;
}

int
iproc_reader_setup_xml_t::m_readflag(
            char const *  _flagname, char const *  _default)
const
{
    std::string  flag_value(
        XML_ATTRIBUTE_VALUE( _flagname, std::string( _default), NULL));
    if ( cbm::is_bool( flag_value))
    {
        if ( cbm::is_bool_true( flag_value))
        {
            /* switched on */
            return  1;
        }
        /* switched off */
        return  0;
    }
    else
    {
        /* invalid boolean expression */
        CBM_LogError( "invalid boolean string in",
            " '",_flagname,"' attribute  [value=",flag_value,"]");
        return  -1;
    }
}

/* miscellaneous options */
int
iproc_reader_setup_xml_t::active_flag()
const
{
    return  this->m_readflag( "active", "yes");
}
int
iproc_reader_setup_xml_t::dispatchonany_flag()
const
{
    return  this->m_readflag( "dispatchonany", "off");
}
int
iproc_reader_setup_xml_t::quiet_flag()
const
{
    return  this->m_readflag( "quiet", "off");
}
int
iproc_reader_setup_xml_t::checkpointcreate_flag()
const
{
    return  this->m_readflag( "checkpointcreate", "yes");
}
int
iproc_reader_setup_xml_t::checkpointrestore_flag()
const
{
    return  this->m_readflag( "checkpointrestore", "yes");
}

}

#define  IMODE_SWITCH_HANDLE(__p__)                            \
    core_iobject_handle_t const *  node( NULL);                    \
    switch ( input_stream_mode()) {                            \
        case  reader_interface_t::IMODE_SINGLE:                    \
            node = helper_xml_walk_path_(                    \
                    this->get_handle(), 1, __p__);            \
            break;                                \
        case  reader_interface_t::IMODE_MULTI:                    \
            node = helper_xml_walk_path_(                    \
                    this->get_handle(), 2, "global", __p__);    \
            break;                                \
        default:                                \
            CBM_LogFatal( "you did nasty stuff. bye bye.");            \
    }                                        \
    CRABMEAT_NOOP/*;*/


namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_setup_xml_t)
iproc_landscape_reader_setup_xml_t::iproc_landscape_reader_setup_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_setup_t()
{
}

iproc_landscape_reader_setup_xml_t::~iproc_landscape_reader_setup_xml_t()
{
}

}

#ifdef  IMODE_SWITCH_HANDLE
#  undef  IMODE_SWITCH_HANDLE
#endif

#endif  /*  !LDNDC_IO_IREADER_SETUP_XML_CPP_  */

