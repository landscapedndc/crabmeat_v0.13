
#ifndef  LDNDC_IO_IREADER_SOILPARAMETERS_XML_CPP_
#define  LDNDC_IO_IREADER_SOILPARAMETERS_XML_CPP_

#include  "io/data-provider/ireaders/xml/reader_xml_soilparameters.h"
#include  "io/data-provider/ireaders/xml/shared/auxiliary.h"
#include  "soillayers/soillayerstypes.h"

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_soilparameters.h"

namespace ldndc {
char const *  iproc_landscape_reader_soilparameters_xml_t::ROOT_TAGS[iproc_landscape_reader_interface_t::IMODE_CNT] = { "ldndcsoilparameters", "soilparameters"};


/*  *****  SOIL PARAMETER XML CORE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_soilparameters_xml_t)
iproc_reader_soilparameters_xml_t::iproc_reader_soilparameters_xml_t()
        : iproc_reader_xml_t(),
          iproc_interface_soilparameters_t()
{
}
iproc_reader_soilparameters_xml_t::~iproc_reader_soilparameters_xml_t()
{
}

bool
iproc_reader_soilparameters_xml_t::use_lresources()
const
{
        bool  use_lr = XML_ATTRIBUTE_VALUE( "useresources", true, NULL);
    if ( use_lr)
    {
        iproc_reader_soilparameters_lrsrc_t const  rsrc_rdr;
        use_lr = rsrc_rdr.is_open();
    }
    return  use_lr;
}

lerr_t
iproc_reader_soilparameters_xml_t::get_humus_default_properties(
        humusparameters::humusparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    int  p_read = 0; int  p_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_soilparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_humus_default_properties(
                _props, _props_names, _props_types, _n_props, &p_read, &p_seen);
        if ( rc_read)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
    lerr_t  rc_read = this->get_default_properties_(
            _props, _props_names, _props_types, _n_props, _p_read, _p_seen);
    if (( rc_read == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) && this->use_lresources())
    {
        /* that's ok */
        if ( _p_read) *_p_read = p_read;
        if ( _p_seen) *_p_seen = p_seen;
    }
    else if ( rc_read)
    {
        CBM_LogError( "iproc_reader_soilparameters_xml_t::get_humus_default_properties(): ",
                "error reading default humus parameters  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
iproc_reader_soilparameters_xml_t::get_soil_default_properties(
        soilparameters::soilparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    int  p_read = 0; int  p_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_soilparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_soil_default_properties(
                _props, _props_names, _props_types, _n_props, &p_read, &p_seen);
        if ( rc_read)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
    lerr_t  rc_read = this->get_default_properties_(
            _props, _props_names, _props_types, _n_props, _p_read, _p_seen);
    if (( rc_read == LDNDC_ERR_ATTRIBUTE_NOT_FOUND) && this->use_lresources())
    {
        /* that's ok */
        if ( _p_read) *_p_read = p_read;
        if ( _p_seen) *_p_seen = p_seen;
    }
    else if ( rc_read)
    {
        CBM_LogError( "iproc_reader_soilparameters_xml_t::get_soil_default_properties(): ",
                "error reading default soil parameters  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

static char const *  MNEMONIC_ATTR = "mnemonic";


template < typename _H >
struct  SOILPAR_TAGS
{
};

template < >
struct  SOILPAR_TAGS< humusparameters::humusparameter_t >
{
    static char const *  TYPE() { return "humuses"; }
    static char const *  SET() { return "humus"; }
    static char const *  NONE() { return soillayers::HUMUS_MNEMONIC[soillayers::HUMUS_NONE]; }
};
template < >
struct  SOILPAR_TAGS< soilparameters::soilparameter_t >
{
    static char const *  TYPE() { return "soils"; }
    static char const *  SET() { return "soil"; }
    static char const *  NONE() { return soillayers::SOIL_MNEMONIC[soillayers::SOIL_NONE]; }
};

template < typename _H >
lerr_t
iproc_reader_soilparameters_xml_t::get_default_properties_(
        _H  _props[],  ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    core_iobject_handle_t const *  node( helper_xml_walk_path_( this->get_handle(), 2, SOILPAR_TAGS< _H >::TYPE(), SOILPAR_TAGS< _H >::SET()));
        InputElementType const *  elem(( node) ? node->ToElement() : NULL);

    lerr_t  rc( LDNDC_ERR_ATTRIBUTE_NOT_FOUND);

        for ( /*, elem*/;  elem;  elem = elem->NextSiblingElement( SOILPAR_TAGS< _H >::SET()))
    {
        cbm::nstr  t_name( elem->Attribute( MNEMONIC_ATTR), CNAME_STRIP|CNAME_STRIP_TO_UPPER);
        if ( cbm::is_equal( t_name.c_str(), SOILPAR_TAGS< _H >::NONE()))
               {
            rc = helper_xml_get_list_eq_< _H >( 
                    elem,
                    "par", "name", "value",
                    _props_names, _props_types, _n_props,
                    _props, _n_props, 1,
                    0 /*offset*/, 1 /*stride*/,
                    _p_read, _p_seen,
                    NULL, NULL, XML_RFLAG_KEY_STRIP|XML_RFLAG_KEY_TO_UPPER);

            break;
        }
    }

    return  rc;
}

lerr_t
iproc_reader_soilparameters_xml_t::get_humus_properties(
        humusparameters::humusparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        char const *  _humus_names[], size_t  _n_humus_names,
        int *  _s_read, int *  _s_seen,
        int  [], int  [])
const
{
    int  s_read = 0; int  s_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_soilparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_humus_properties(
                _props, _props_names, _props_types, _n_props, _humus_names, _n_humus_names, &s_read, &s_seen, NULL, NULL);
        if ( rc_read)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
    lerr_t  rc_humus = get_properties_(
            _props, _props_names, _props_types, _n_props, _humus_names, _n_humus_names, _s_read, _s_seen, NULL, NULL);
    if ( rc_humus)
    {
        CBM_LogError( "iproc_reader_soilparameters_xml_t::get_humus_properties(): ",
                "error reading humus parameters  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }
    else if ( this->use_lresources())
    {
        if ( _s_read) *_s_read = s_read;
        if ( _s_seen) *_s_seen = s_seen;
    }
    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}

lerr_t
iproc_reader_soilparameters_xml_t::get_soil_properties(
        soilparameters::soilparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        char const *  _soil_names[], size_t  _n_soil_names,
        int *  _s_read, int *  _s_seen,
        int  _p_read[], int  _p_seen[])
const
{
    int  s_read = 0; int  s_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_soilparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_soil_properties(
                _props, _props_names, _props_types, _n_props, _soil_names, _n_soil_names, &s_read, &s_seen, _p_read, _p_seen);
        if ( rc_read)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
    lerr_t  rc_soil = get_properties_(
            _props, _props_names, _props_types, _n_props, _soil_names, _n_soil_names, _s_read, _s_seen, _p_read, _p_seen);
    if ( rc_soil)
    {
        CBM_LogError( "iproc_reader_soilparameters_xml_t::get_soil_properties(): ",
                "error reading soil parameters  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }
    else if ( this->use_lresources())
    {
        if ( _s_read) *_s_read = s_read;
        if ( _s_seen) *_s_seen = s_seen;
    }
    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}

template < typename  _H >
lerr_t
iproc_reader_soilparameters_xml_t::get_properties_(
        _H  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        char const *  _names[], size_t  _n_names,
        int *  _s_read, int *  _s_seen,
        int  _p_read[], int  _p_seen[])
const
{
    // TODO  keep track of seen soil, so that multiple occurences can be caught (not necessarily bad...)

    core_iobject_handle_t const *  node( helper_xml_walk_path_( this->get_handle(), 2, SOILPAR_TAGS< _H >::TYPE(), SOILPAR_TAGS< _H >::SET()));
        InputElementType const *  elem = ( node) ? node->ToElement() : NULL;

    int  r, s;
    unsigned int  si;

        for ( r = 0, s = 0 /*, elem*/;  elem;  elem = elem->NextSiblingElement( SOILPAR_TAGS< _H >::SET()), ++s)
           {
        si = invalid_uint;
        cbm::nstr  t_name( elem->Attribute( MNEMONIC_ATTR), CNAME_STRIP|CNAME_STRIP_TO_UPPER);
        cbm::find_index( t_name.c_str(), _names, _n_names, &si);
        if ( si >= _n_names)
               {
            continue;
        }

        ++r;

        if ( _p_read) _p_read[si] = 0;
        if ( _p_seen) _p_seen[si] = 0;
    
        /*lerr_t  rc =*/
        (void)helper_xml_get_list_eq_< _H >( 
                elem,
                "par", "name", "value",
                _props_names, _props_types, _n_props,
                &(_props[si * _n_props]), _n_props, 1,
                0 /*offset*/, 1 /*stride*/,
                       (( _p_read) ? &(_p_read[si]) : NULL), (( _p_seen) ? &(_p_seen[si]) : NULL),
                       NULL, NULL, XML_RFLAG_KEY_STRIP|XML_RFLAG_KEY_TO_UPPER);
    }

    if ( _s_read) *_s_read = r;
    if ( _s_seen) *_s_seen = s;


    return  LDNDC_ERR_OK;
}



/*  *****  SOIL PARAMETER XML READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_soilparameters_xml_t)
iproc_landscape_reader_soilparameters_xml_t::iproc_landscape_reader_soilparameters_xml_t()
        : iproc_landscape_reader_xml_t(),
          iproc_landscape_interface_soilparameters_t()
{
}


iproc_landscape_reader_soilparameters_xml_t::~iproc_landscape_reader_soilparameters_xml_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_SOILPARAMETERS_XML_CPP_  */

