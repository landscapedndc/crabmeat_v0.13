/*!
 * @brief
 *    virtual reader implementation
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#include  "io/data-provider/ireaders/ireader.h"

char const *  ldndc::iproc_landscape_reader_interface_t::IMODE_NAMES[IMODE_CNT] = { "multi", "single"};

ldndc::iproc_reader_interface_t::iproc_reader_interface_t()
{
}
ldndc::iproc_reader_interface_t::~iproc_reader_interface_t()
{
}


ldndc::iproc_reader_base_t::iproc_reader_base_t()
        : ldndc::iproc_reader_interface_t(),
          ldndc::iproc_provider_t(),
          data_arrived( 0)
{
}
ldndc::iproc_reader_base_t::~iproc_reader_base_t()
{
}
ldndc::iproc_landscape_reader_interface_t::iproc_landscape_reader_interface_t()
{
}
ldndc::iproc_landscape_reader_interface_t::~iproc_landscape_reader_interface_t()
{
}


ldndc::iproc_landscape_reader_base_t::iproc_landscape_reader_base_t()
        : ldndc::iproc_landscape_reader_interface_t(),
          ldndc::iproc_landscape_provider_t()
{
}
ldndc::iproc_landscape_reader_base_t::~iproc_landscape_reader_base_t()
{
}



