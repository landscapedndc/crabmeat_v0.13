

#ifndef  LDNDC_IO_IREADER_EVENT_SHARED_H_
#define  LDNDC_IO_IREADER_EVENT_SHARED_H_


#define  EV_ATTR_L_(__dst__,__attr__,__default__)                                \
    EV_ATTR_LU_(__dst__,__attr__,__default__,CNAME_STRIP_TO_LOWER)
#define  EV_ATTR_U_(__dst__,__attr__,__default__)                                \
    EV_ATTR_LU_(__dst__,__attr__,__default__,CNAME_STRIP_TO_UPPER)

/* read attribute */
#define  EV_ATTR(__dst__,__attr__)                                        \
    EV_ATTR_( attrs->__dst__, __attr__, attrs_defaults->__dst__)

/* read attribute lowercased */
#define  EV_ATTR_L(__dst__,__attr__)                                        \
    EV_ATTR_L_( attrs->__dst__, __attr__, attrs_defaults->__dst__)
/* read attribute uppercased */
#define  EV_ATTR_U(__dst__,__attr__)                                        \
    EV_ATTR_U_( attrs->__dst__, __attr__, attrs_defaults->__dst__)


/* adds handler to handler list (one handler per event type) */
#define  __EV_READER_HANDLER_ADD(__type__)                                    \
    event_helper_attribute_handlers[event::__EVENT_ENUM_ ## __type__] = &__EV_READER_HANDLER_NAME(__type__)/*;*/


#endif  /*  !LDNDC_IO_IREADER_EVENT_SHARED_H_  */

