
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(graze)
{
    EV_ATTRS_READER_HEADER(graze);

    std::string  livestock_name;
    EV_ATTR_L_( livestock_name, "livestock", invalid_string);
    if ( cbm::is_invalid( livestock_name))
    {
        attrs->type = event::__event_attrib_class_name(graze)::LIVESTOCK_USERDEFINED;
    }

    if ( attrs->type != event::__event_attrib_class_name(graze)::LIVESTOCK_USERDEFINED)
    {
        attrs->type = (event::__event_attrib_class_name(graze)::livestock_type_e)cbm::find_index(
            livestock_name.c_str(), event::__event_attrib_class_name(graze)::LIVESTOCK_NAMES, event::__event_attrib_class_name(graze)::LIVESTOCK_CNT);
        if ( attrs->type >= event::__event_attrib_class_name(graze)::LIVESTOCK_CNT)
        {
            CBM_LogError( "livestock name does not match any predefined type; ",
                    "see documentation for predefined types  [livestock=",livestock_name,"]");
            attrs->type = event::__event_attrib_class_name(graze)::LIVESTOCK_NONE;
            return  LDNDC_ERR_FAIL;
        }

        attrs->properties = event::__event_attrib_class_name(graze)::LIVESTOCK_DEFAULTS[attrs->type];
    }

    EV_ATTR_( attrs->properties.dung_carbon, "dungcarbon", attrs->properties.dung_carbon);
    EV_ATTR_( attrs->properties.dung_nitrogen, "dungnitrogen", attrs->properties.dung_nitrogen);
    EV_ATTR_( attrs->properties.urine_nitrogen, "urinenitrogen", attrs->properties.urine_nitrogen);
           EV_ATTR_( attrs->properties.demand_carbon, "demandcarbon", attrs->properties.demand_carbon);

    EV_ATTR( head_count, "headcount");

    EV_ATTR( grazing_hours, "hours"); /*backward compatibility*/
    EV_ATTR( grazing_hours, "grazinghours");

    if (( attrs->type == event::__event_attrib_class_name(graze)::LIVESTOCK_USERDEFINED)
        && ( cbm::is_invalid( attrs->properties.dung_carbon)
            || cbm::is_invalid( attrs->properties.dung_nitrogen)
            || cbm::is_invalid( attrs->properties.urine_nitrogen)
            || cbm::is_invalid( attrs->properties.demand_carbon)))
    {
        CBM_LogWarn( "graze event with user-defined livestock type has incomplete properties");
    }


    return  LDNDC_ERR_OK;
}
}

