
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(harvest)
{
    EV_ATTRS_READER_HEADER(harvest);

    lerr_t  rc = LDNDC_ERR_OK;

    /* name */
    EV_ATTR_U( name, "name");
    if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
    {
        EV_ATTR_U( name, "type");
        if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
        {
            CBM_LogError( "harvest event is missing 'name' attribute or attribute not set");
            rc = LDNDC_ERR_FAIL;
        }
    }

    EV_ATTR( height, "height");
    if ( cbm::is_invalid( attrs->height))
    {
        EV_ATTR( height, "stubbleheight");
    }

    EV_ATTR( remains_relative, "remains_relative");
    if ( cbm::is_invalid( attrs->remains_relative))
    {
        //for backwards compatibility
        EV_ATTR( remains_relative, "remains");
    }
    EV_ATTR( remains_absolute, "remains_absolute");

    if ( cbm::flt_greater( attrs->remains_relative, 1.0))
    {
        CBM_LogError( "harvest event attribute 'remains_relative' greater 1",
                      "  [remains_relative=",attrs->remains_relative,"]");
        rc = LDNDC_ERR_FAIL;
    }
    else if ( cbm::is_valid( attrs->remains_relative) &&
              cbm::flt_less( attrs->remains_relative, 0.0))
    {
        CBM_LogError( "harvest event attribute 'remains_relative' less 0",
                      "  [remains_relative=",attrs->remains_relative,"]");
        rc = LDNDC_ERR_FAIL;
    }
    else
    {
        /* all ok */
    }

    EV_ATTR( export_stem_wood, "exportstemwood");
    EV_ATTR( export_branch_wood, "exportbranchwood");
    EV_ATTR( export_root_wood, "exportrootwood");

    EV_ATTR( mulching, "mulching");

    return  rc;
}
}

