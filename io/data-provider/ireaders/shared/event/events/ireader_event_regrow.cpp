/*!
 * @brief
 *      event xml reader "regrow"
 *
 * @author
 *      steffen klatt (created on: apr 29, 2013)
 */

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  regrow
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(__this_event_name)
{
    EV_ATTRS_READER_HEADER(__this_event_name);

    EV_ATTR_U( name, "name");
    if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
    {
        CBM_LogError( "regrow attributes:  seeing invalid species name in ",STRINGIFY(__this_event_name)," event  [",attrs->name,"]");
        return  LDNDC_ERR_FAIL;
    }

    EV_ATTR( tree_number, "treenumber");
    EV_ATTR( tree_number_resize_factor, "treenumberresizefactor");
    if ( cbm::is_valid( attrs->tree_number) && cbm::is_valid( attrs->tree_number_resize_factor))
    {
        CBM_LogError( "regrow attributes:  invalid use of mutually exclusive attributes \"treenumber\" and \"treenumberresizefactor\"",
                 "  [treenumber=",attrs->tree_number,",treenumberresizefactor=",attrs->tree_number_resize_factor,"]");
        return  LDNDC_ERR_FAIL;
    }
    EV_ATTR( export_aboveground_biomass, "exportabovegroundbiomass");
    EV_ATTR( height_max, "heightmax");

    return  LDNDC_ERR_OK;
}
}

