
#include  "input/species/species-srv.h"
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(reparameterizespecies)
{
    EV_ATTRS_READER_HEADER(reparameterizespecies);

    EV_ATTR_U( type, "type");
    if ( cbm::is_empty( attrs->type) || cbm::is_invalid( attrs->type))
           {
        CBM_LogError( "plant event is missing 'type' attribute or attribute not set");
        return  LDNDC_ERR_FAIL;
    }
    /* no defaults */
    EV_ATTR_U( name, "name");
    if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
           {
        CBM_LogError( "plant event is missing 'name' attribute or attribute not set");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
}

