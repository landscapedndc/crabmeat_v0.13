/*!
 * @brief
 *      event xml reader "throw" (e.g. windthrow)
 *
 * @author
 *      steffen klatt (created on: apr 29, 2013)
 */

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  throw
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(__this_event_name)
{
    EV_ATTRS_READER_HEADER(__this_event_name);

    EV_ATTR_U( name, "name");
    if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
    {
        CBM_LogError( "seeing invalid species name in",STRINGIFY(__this_event_name),"event  [",attrs->name,"]");
        return  LDNDC_ERR_FAIL;
    }

    std::string  reduction_number;
    EV_ATTR_L_( reduction_number, "reductionnumber", ldndc_empty_string);
    if (( reduction_number.size() > 1) && ( reduction_number[0] == '!'))
    {
        attrs->reduction_number_is_absolute = true;
        reduction_number = reduction_number.substr( 1);
    }
    lerr_t  rc_number = cbm::s2n( reduction_number, &attrs->reduction_number);
    if ( rc_number)
        { return  LDNDC_ERR_FAIL; }

    std::string  reduction_volume;
    EV_ATTR_L_( reduction_volume, "reductionvolume", ldndc_empty_string);
    if (( reduction_volume.size() > 1) && ( reduction_volume[0] == '!'))
    {
        attrs->reduction_volume_is_absolute = true;
        reduction_volume = reduction_volume.substr( 1);
    }
    lerr_t  rc_volume = cbm::s2n( reduction_volume, &attrs->reduction_volume);
    if ( rc_volume)
        { return  LDNDC_ERR_FAIL; }

    EV_ATTR( reason, "reason");

    return  LDNDC_ERR_OK;
}
}

