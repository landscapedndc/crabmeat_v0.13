
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(flood)
{
    EV_ATTRS_READER_HEADER(flood);

    EV_ATTR( bundheight, "bundheight");
    EV_ATTR( percolationrate, "percolationrate");
    EV_ATTR( watertable, "watertable");
    EV_ATTR( irrigationheight, "irrigationheight");
    EV_ATTR( irrigationamount, "irrigationamount");
    EV_ATTR( unlimitedwater, "unlimitedwater");
    EV_ATTR( saturationlevel, "saturationlevel");
    EV_ATTR( soildepth, "soildepth");

    return  LDNDC_ERR_OK;
}
}

