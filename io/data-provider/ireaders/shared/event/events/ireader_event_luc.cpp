#include  "string/lstring.h"

namespace ldndc {
/* higher level error checking is not readers job... */
__EV_READER_HANDLER_SIGNATURE(luc)
{
    EV_ATTRS_READER_HEADER(luc);
    CRABMEAT_FIX_UNUSED(attrs_defaults);

    std::string  luc_cmd;
    EV_ATTR_L_( luc_cmd, "command", invalid_string);
    if ( cbm::is_invalid( luc_cmd))
           {
        CBM_LogError( "luc event is missing 'command' attribute");
        return  LDNDC_ERR_FAIL;
    }

    cbm::string_t  luc_command_args( luc_cmd);
    cbm::tokenizer  luc_command_args_tok( luc_command_args.tokenize( ' '));
    if ( luc_command_args_tok.size() < 2)
    {
        CBM_LogError( "luc event \"",luc_command_args_tok[0],"\" is missing command arguments");
        return  LDNDC_ERR_FAIL;
    }

    std::string  luc_command = luc_command_args_tok[0];
    /* set event command arguments */
    std::string  luc_args = luc_command_args_tok[1];
    if ( luc_command_args_tok.size() > 2)
    {
        for ( size_t  i = 2;  i < luc_command_args_tok.size();  ++i)
        {
            luc_args += event::__event_attrib_class_name(luc)::LUC_ARGS_DELIM;
            luc_args += luc_command_args_tok[i];
        }
    }

    unsigned int  luc_command_e( invalid_uint);
    cbm::find_index( luc_command.c_str(), event::__event_attrib_class_name(luc)::COMMAND_NAMES,
            event::__event_attrib_class_name(luc)::LUC_COMMAND_CNT, &luc_command_e);
    if ( cbm::is_invalid( luc_command_e))
    {
        CBM_LogError( "unrecognized command  [command="+luc_command+"]");
        return  LDNDC_ERR_FAIL;
    }
    attrs->command = (event::__event_attrib_class_name(luc)::command_e)luc_command_e;
    attrs->args = luc_args;

    return  LDNDC_ERR_OK;
}
}

