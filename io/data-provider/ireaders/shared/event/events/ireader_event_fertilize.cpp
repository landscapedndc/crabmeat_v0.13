
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(fertilize)
{
    EV_ATTRS_READER_HEADER(fertilize);

    std::string  fertilizer_type;
    EV_ATTR_L_( fertilizer_type, "type", invalid_string);
    if ( cbm::is_invalid( fertilizer_type))
    {
        CBM_LogError( "fertilizer event is missing 'type' attribute");
        return  LDNDC_ERR_FAIL;
    }
    attrs->type = fertilizer_type;

    std::string  fertilizer_location;
    EV_ATTR_L_( fertilizer_location, "location", invalid_string);
    attrs->location = fertilizer_location;

    EV_ATTR( amount, "amount");
    EV_ATTR( ni_amount, "ni_amount");
    EV_ATTR( ui_amount, "ui_amount");
    EV_ATTR( depth, "depth");

    return  LDNDC_ERR_OK;
}
}

