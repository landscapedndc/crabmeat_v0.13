
namespace ldndc 
{
__EV_READER_HANDLER_SIGNATURE(manure)
{
    EV_ATTRS_READER_HEADER(manure);

    std::string  manure_type;
    EV_ATTR_L_( manure_type, "type", invalid_string);
    if ( cbm::is_invalid( manure_type))
    {
        CBM_LogError( "manure event is missing 'type' attribute");
        return  LDNDC_ERR_FAIL;
    }
    attrs->type = manure_type;

    EV_ATTR( depth, "depth");
    EV_ATTR( c, "c");
    EV_ATTR( cn, "cn");
    EV_ATTR( avail_c, "availc");
    EV_ATTR( avail_n, "availn");
    EV_ATTR( ph, "ph");
    EV_ATTR( volume, "volume");
    EV_ATTR( nh4_fraction, "nh4fraction");
    EV_ATTR( no3_fraction, "no3fraction");
    EV_ATTR( urea_fraction, "ureafraction");
    EV_ATTR( don_fraction, "donfraction");
    EV_ATTR( cellulose_fraction, "cellulosefraction");
    EV_ATTR( lignin_fraction, "ligninfraction");

    return  LDNDC_ERR_OK;
}
}

