/*!
 * @brief
 *      event xml reader "fire"
 *
 * @author
 *      steffen klatt (created on: apr 29, 2013),
 */

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  fire
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(__this_event_name)
{
    EV_ATTRS_READER_HEADER(__this_event_name);
    
    EV_ATTR( burned_area, "burnedarea");

    return  LDNDC_ERR_OK;
}
}

