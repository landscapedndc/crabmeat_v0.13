
#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  cut
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(__this_event_name)
{
    EV_ATTRS_READER_HEADER(__this_event_name);

    lerr_t  rc = LDNDC_ERR_OK;

    EV_ATTR_U( name, "name");

    EV_ATTR( height, "height");
    if ( cbm::is_invalid( attrs->height))
    {
        EV_ATTR( height, "stubbleheight");
    }

    EV_ATTR( remains_relative, "remains_relative");
    EV_ATTR( remains_absolute, "remains_absolute");
    if ( cbm::is_invalid( attrs->remains_absolute))
    {
        //for backwards compatibility
        EV_ATTR( remains_absolute, "remains");
    }

    if ( cbm::flt_greater( attrs->remains_relative, 1.0))
    {
        CBM_LogError( "harvest event attribute 'remains_relative' greater 1",
                      "  [remains_relative=",attrs->remains_relative,"]");
        rc = LDNDC_ERR_FAIL;
    }
    else if ( cbm::is_valid( attrs->remains_relative) &&
              cbm::flt_less( attrs->remains_relative, 0.0))
    {
        CBM_LogError( "harvest event attribute 'remains_relative' less 0",
                      "  [remains_relative=",attrs->remains_relative,"]");
        rc = LDNDC_ERR_FAIL;
    }
    else
    {
        /* all ok */
    }

    EV_ATTR( remains_absolute_fruit, "remains_absolute_fruit");
    EV_ATTR( remains_absolute_foliage, "remains_absolute_foliage");
    EV_ATTR( remains_absolute_living_structural_tissue, "remains_absolute_living_structural_tissue");
    EV_ATTR( remains_absolute_dead_structural_tissue, "remains_absolute_dead_structural_tissue");
    EV_ATTR( remains_absolute_root, "remains_absolute_root");

    EV_ATTR( export_fruit, "export_fruit");
    EV_ATTR( export_foliage, "export_foliage");
    EV_ATTR( export_living_structural_tissue, "export_living_structural_tissue");
    EV_ATTR( export_dead_structural_tissue, "export_dead_structural_tissue");
    EV_ATTR( export_root, "export_root");

    EV_ATTR( mulching, "mulching");

    return  rc;
}
}

