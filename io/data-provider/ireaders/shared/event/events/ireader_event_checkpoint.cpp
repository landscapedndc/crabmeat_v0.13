
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(checkpoint)
{
    EV_ATTRS_READER_HEADER(checkpoint);

    std::string  checkpoint_command;
    EV_ATTR_L_( checkpoint_command, "command", invalid_string);
    if ( cbm::is_invalid( checkpoint_command))
    {
        CBM_LogError( "checkpoint event is missing 'command' attribute");
        return  LDNDC_ERR_FAIL;
    }
    attrs->command = (event::__event_attrib_class_name(checkpoint)::command_e)cbm::find_index( 
            checkpoint_command.c_str(), event::__event_attrib_class_name(checkpoint)::CHECKPOINT_COMMAND_NAMES,
            event::__event_attrib_class_name(checkpoint)::CHECKPOINT_COMMAND_CNT);
    if ( attrs->command == event::__event_attrib_class_name(checkpoint)::CHECKPOINT_COMMAND_CNT)
    {
        CBM_LogError( "unknown checkpoint command  [command=",checkpoint_command,"]");
        return  LDNDC_ERR_FAIL;
    }

    std::string  checkpoint_command_options;
    EV_ATTR_L_( checkpoint_command_options, "options", attrs_defaults->options.str());
    attrs->options = checkpoint_command_options;

    return  LDNDC_ERR_OK;
}
}

