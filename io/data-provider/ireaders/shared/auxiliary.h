/*!
 * @brief
 *    common ireader routines
 *
 * @author
 *    steffen klatt (created on: apr 06, 2014)
 */

#ifndef  LDNDC_IO_IREADER_AUXILIARY_H_
#define  LDNDC_IO_IREADER_AUXILIARY_H_

#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"

namespace ldndc {

template < typename  _P >
struct  iproc_reader_parameters_auxiliary_t
{
        static  lerr_t  assign_value(
                        _P * /*property*/, char const * /*property value*/, char const /*property type*/,
                        int * /*is valid*/);
};
}

template < typename  _P >
lerr_t
ldndc::iproc_reader_parameters_auxiliary_t< _P >::assign_value(
                _P *  _prop, char const *  _propvalue, char const  _proptype,
                int *  _isvalid)
{
        *_isvalid = 1;
    if ( !_propvalue || cbm::is_empty( _propvalue) || cbm::is_whitespace( _propvalue))
    {
                *_isvalid = 0;
    }

    switch ( _proptype)
    {
        case 'f':
        case 'd':
            {
                                if ( !*_isvalid)
                                {
                                        *_prop = invalid_t< typename _P::float_type >::value;
                                        break;
                                }

                typename _P::float_type  p_val_f;
                cbm::s2n< typename _P::float_type >( _propvalue, &p_val_f);
                *_prop = p_val_f;

                if ( cbm::is_invalid< typename _P::float_type >( *_prop))
                {
                                        *_isvalid = 0;
                }
                break;
            }
        case 'b':
            {
                                if ( !*_isvalid)
                                {
                                        *_prop = false;
                                        break;
                                }

                                if ( !cbm::is_bool( _propvalue))
                                {
                                        CBM_LogError( "iproc_reader_parameters_auxiliary_t::assign_value(): ",
                                                        "invalid boolean value  [value=",_propvalue,"]");
                                        *_isvalid = 0;
                                        break;
                                }

                typename _P::bool_type  p_val_b;
                cbm::s2n< typename _P::bool_type >( _propvalue, &p_val_b);
                *_prop = p_val_b;

                break;
            }
        case 'i':
            {
                                if ( !*_isvalid)
                                {
                                        *_prop = invalid_t< typename _P::int_type >::value;
                                        break;
                                }

                typename _P::int_type  p_val_i;
                cbm::s2n< typename _P::int_type >( _propvalue, &p_val_i);
                *_prop = p_val_i;

                if ( cbm::is_invalid< typename _P::int_type >( *_prop))
                {
                                        *_isvalid = 0;
                }
                break;
            }
        case 'u':
            {
                                if ( !*_isvalid)
                                {
                                        *_prop = invalid_t< typename _P::uint_type >::value;
                                        break;
                                }

                typename _P::uint_type  p_val_u;
                cbm::s2n< typename _P::uint_type >( _propvalue, &p_val_u);
                *_prop = p_val_u;

                if ( cbm::is_invalid< typename _P::uint_type>( *_prop))
                {
                                        *_isvalid = 0;
                }
                break;
            }
        default:
            {
                CBM_LogError( "iproc_reader_parameters_auxiliary_t::assign_value(): ",
                                                "unsupported datatype  [datatype=",_proptype,",value=",_propvalue,"]");
                return  LDNDC_ERR_FAIL;
            }
    }

    return  LDNDC_ERR_OK;
}

#endif  /*  !LDNDC_IO_IREADER_AUXILIARY_H_ */

