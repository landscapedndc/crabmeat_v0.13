/*!
 * @brief
 *    virtual input reader
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_INPUTREADER_H_
#define  LDNDC_IO_INPUTREADER_H_

#include  "io/data-provider/provider.h"
#include  "io/fhandler/fhandler.h"

namespace ldndc {

/***  INPUT READER INTERFACES  ***/
/* the core input reader classes are used
 *    - to read entity specific data from input sources
 */
class  CBM_API  iproc_reader_interface_t
{
    protected:
        iproc_reader_interface_t();
        virtual  ~iproc_reader_interface_t() = 0;
};
class CBM_API iproc_reader_base_t
    :  public  iproc_reader_interface_t, public  iproc_provider_t
{
    public:
        virtual  char const *  input_class_type() const = 0;
        virtual  char const *  io_format_type() const = 0;

        virtual  iproc_reader_base_t *  to_reader()
            { return  this; }
        virtual  int  data_available() const
            { return  this->data_arrived; }

    public:
        iproc_reader_base_t();
        virtual  ~iproc_reader_base_t() = 0;

    protected:
        mutable  int  data_arrived;
};


/***  LANDSCAPE INPUT READER INTERFACES  ***/
/* the input reader classes are used
 *    - and extract the data blocks specific to cells (to feed the core input readers)
 *    - to read global data from input sources
 */
class  CBM_API  iproc_landscape_reader_interface_t
{
    public:
        enum  input_stream_mode_e
        {
            IMODE_MULTI    = 0,        /*!< input stream has multi core format */
            IMODE_SINGLE,            /*!< input stream has single core format, i.e. 1d version */

            IMODE_CNT,
            IMODE_NONE
        };
        static char const *  IMODE_NAMES[IMODE_CNT];

    public:
        virtual  iproc_provider_t *  acquire_ireader(
                lid_t const & /*block id*/) = 0;
        virtual  void  release_ireader(
                lid_t const & /*block id*/) = 0;

    protected:
        iproc_landscape_reader_interface_t();
        virtual  ~iproc_landscape_reader_interface_t() = 0;
};
class CBM_API iproc_landscape_reader_base_t
    :  public  iproc_landscape_reader_interface_t, public  iproc_landscape_provider_t
{
    public:
        virtual  char const *  input_class_type() const = 0;
        virtual  char const *  io_format_type() const = 0;

    public:
        iproc_provider_t *  acquire_provider(
                lid_t const &  _block_id)
        {
            return  this->acquire_ireader( _block_id);
        }
        void  release_provider(
                lid_t const &  _block_id)
        {
            return  this->release_ireader( _block_id);
        }


    public:
        iproc_landscape_reader_base_t();
        virtual  ~iproc_landscape_reader_base_t() = 0;
};

}

#endif  /*  !LDNDC_IO_INPUTREADER_H_  */

