/*!
 * @brief
 *    Lresources soilparameters readers
 *
 * @author
 *    steffen klatt (created on: apr 04, 2014)
 */

#ifndef  LDNDC_IO_IREADER_SOILPARAMETERS_LRSRC_H_
#define  LDNDC_IO_IREADER_SOILPARAMETERS_LRSRC_H_

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc.h"
#include  "io/iif/iif_soilparameters.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_soilparameters_lrsrc_t :  public  iproc_landscape_reader_lrsrc_t,  public  iproc_landscape_interface_soilparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_soilparameters_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_soilparameters_t::input_class_type(); }

    public:
        iproc_landscape_reader_soilparameters_lrsrc_t();
        virtual  ~iproc_landscape_reader_soilparameters_lrsrc_t();
};
class  CBM_API  iproc_reader_soilparameters_lrsrc_t  :  public  iproc_reader_lrsrc_t,  public  iproc_interface_soilparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_soilparameters_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_interface_soilparameters_t::input_class_type(); }
    public:
        iproc_reader_soilparameters_lrsrc_t();
        virtual  ~iproc_reader_soilparameters_lrsrc_t();

    public:
        static char const *  SOIL_TAG;
        static char const *  SOIL_GROUP;
        static char const *  SOIL_MNEMONIC;
        static char const *  SOIL_MNEMONIC_META;
        static char const *  SOIL_NAME;

        static char const *  HUMUS_TAG;
        static char const *  HUMUS_GROUP;
        static char const *  HUMUS_MNEMONIC;
        static char const *  HUMUS_MNEMONIC_META;
        static char const *  HUMUS_NAME;

        static char const *  PARAMETER;
        static char const *  PARAMETER_VALUE;

    public:
        lerr_t  get_humus_default_properties(
                humusparameters::humusparameter_t [],  ldndc_string_t const [], char const  [], size_t,
                       int *, int *) const;

        lerr_t  get_humus_properties(
                humusparameters::humusparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                char const * [], size_t,
                int *, int *,
                int [], int []) const;

        lerr_t  get_soil_default_properties(
                soilparameters::soilparameter_t [],  ldndc_string_t const [], char const  [], size_t,
                       int *, int *) const;

        lerr_t  get_soil_properties(
                soilparameters::soilparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                char const * [], size_t,
                int *, int *,
                int [], int []) const;

    private:
        template < typename  _P >
        lerr_t  set_properties_(
                _P *, char const *,
                char const * /*names*/[], size_t /*number of names*/,
                ldndc_string_t const [], char const [], size_t,
                int *, int *) const;
        template < typename _P >
        lerr_t  set_single_properties_(
                _P [], char const * /*group name*/, char const * /*type name*/,
                ldndc_string_t const [], char const  [], size_t,
                       int *, int *) const;
};
}

#endif  /*  !LDNDC_IO_IREADER_SOILPARAMETERS_LRSRC_H_  */

