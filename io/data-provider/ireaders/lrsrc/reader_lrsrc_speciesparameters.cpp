
#ifndef  LDNDC_IO_IREADER_SPECIESPARAMETERS_LRSRC_CPP_
#define  LDNDC_IO_IREADER_SPECIESPARAMETERS_LRSRC_CPP_

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_speciesparameters.h"
#include  "speciesparameters/speciesparameterstypes.h"
#include  "string/lstring-compare.h"
#include  "string/lstring-nstr.h"

#include  "io/data-provider/ireaders/shared/auxiliary.h"

namespace ldndc {

char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_TAG = "species";
char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_MNEMONIC = "mnemonic";
char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_GROUP = "group";
char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_PARENT = "parent";
char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_NAME = "name";

char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_PARAMETER = "parameter";
char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_PARAMETER_VALUE = "value";

char const *  iproc_reader_speciesparameters_lrsrc_t::SPECIES_MNEMONIC_META = species::internal_SPECIES_GROUP_ANY;

/*  *****  SPECIES PARAMETER LRESOURCES READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_speciesparameters_lrsrc_t)
iproc_reader_speciesparameters_lrsrc_t::iproc_reader_speciesparameters_lrsrc_t()
        : iproc_reader_lrsrc_t(),
          iproc_interface_speciesparameters_t()
{
    this->data_arrived = 1;
}
iproc_reader_speciesparameters_lrsrc_t::~iproc_reader_speciesparameters_lrsrc_t()
{
}

lerr_t
iproc_reader_speciesparameters_lrsrc_t::get_species_default_properties(
        speciesparameters::speciesparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    return  this->set_single_species_properties_(
        _props, SPECIES_MNEMONIC_META, _props_names, _props_types, _n_props, _p_read, _p_seen);
}

lerr_t
iproc_reader_speciesparameters_lrsrc_t::set_single_species_properties_(
        speciesparameters::speciesparameter_t  _props[], char const *  _species_name,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _r, int *  _s)
const
{
    typedef  speciesparameters::speciesparameter_t  pt;

    lerr_t  rc = LDNDC_ERR_OK;

    int  r = 0;
    int  s = 0;
    for ( size_t  p = 0;  p < _n_props;  ++p)
    {
        cbm::Lresources_t::key_type  key;
        cbm::Lresources_t::make_key( &key, "%s.%s.%s.%s.%s",
                SPECIES_TAG, _species_name, SPECIES_PARAMETER, _props_names[p], SPECIES_PARAMETER_VALUE);
        char const *  p_val = CBM_DefaultResources.find( key.name);
        if ( !p_val || cbm::is_empty( p_val) || cbm::is_whitespace( p_val))
        {
            CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::set_single_species_properties(): ",
                    "missing parameter in Lresources, please update  [species=",_species_name,",parameter=",_props_names[p],"]");
            rc = LDNDC_ERR_RUNTIME_ERROR;
            continue;
        }

        ++r;
        int  isvalid = -1;
        lerr_t  rc_assign = iproc_reader_parameters_auxiliary_t< pt >::assign_value(
                &_props[p], p_val, _props_types[p], &isvalid);
        if ( rc_assign)
        {
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        if ( isvalid == 1)
        {
            ++s;
        }
        else if ( isvalid == 0)
        {
            CBM_LogWarn( "iproc_reader_speciesparameters_lrsrc_t::set_single_species_properties(): ",
                    "missing or invalid parameter  [species=",_species_name,",parameter=",_props_names[p],",value=",p_val,"]");
        }
        else
        {
            CBM_LogFatal( "[BUG]");
        }
    }

    /* parameters read */
    if ( _r)
    {
        *_r = r;
    }
    /* parameters valid */
    if ( _s)
    {
        *_s = s;
    }

    return  rc;
}

size_t
iproc_reader_speciesparameters_lrsrc_t::get_species_cnt()
const
{
    size_t  species_cnt = 0;
    lerr_t  rc_query = CBM_DefaultResources.query( "species.count", &species_cnt);
    if ( rc_query)
    {
        return  invalid_t< size_t >::value;
    }

    return  species_cnt;
}

int
iproc_reader_speciesparameters_lrsrc_t::set_single_species_meta_(
        speciesparameters::speciesparameters_meta_t *  _species)
const
{
    crabmeat_assert( _species);

    /* read remaining meta data */
    /*   group */
    cbm::Lresources_t::key_type  key;
    cbm::Lresources_t::make_key( &key, "%s.%s.%s",
            SPECIES_TAG, _species->mnemonic, SPECIES_GROUP);
    char const *  species_group = CBM_DefaultResources.find( key.name);
    if ( !species_group)
    {
        species_group = ldndc_empty_cstring;
    }
    cbm::nstr  group( species_group, CNAME_STRIP|CNAME_STRIP_TO_LOWER);
    if ( !species_group || cbm::is_empty( group.c_str()))
    {
        CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::set_single_species_meta(): ",
                "missing species group attribute. [species=",_species->mnemonic,"]");
        return  -1;
    }

    unsigned int  i = 0;
    if ( cbm::find_index( group.c_str(),
            species::SPECIES_GROUP_NAMES, species::SPECIES_GROUP_CNT, &i) != LDNDC_ERR_OK)
    {
        CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::set_single_species_meta(): ",
                "unknown species group  [object-id=",this->object_id(),",group=",group.c_str(),",species=",_species->mnemonic,"]");
        return  -1;
    }
    _species->group = (species::species_group_e)i;

    /*   parent */
    cbm::Lresources_t::make_key( &key, "%s.%s.%s",
            SPECIES_TAG, _species->mnemonic, SPECIES_PARENT);
    char const *  species_parent = CBM_DefaultResources.find( key.name);
    if ( species_parent && !cbm::is_empty( species_parent)
            && !cbm::is_equal_i( species_parent, species::SPECIES_NAME_NONE)
            && !cbm::is_invalid( species_parent)
            && !cbm::is_equal_i( species_group, species::SPECIES_GROUP_NAMES[species::SPECIES_GROUP_NONE]))
    {
        cbm::Lresources_t::make_key( &key, "%s.%s.%s",
			    SPECIES_TAG, species_parent, SPECIES_GROUP);
	    char const *  species_parent_group = CBM_DefaultResources.find( key.name);
        if ( !species_parent_group || ( !cbm::is_equal_i( species_group, species_parent_group)
                && ( !cbm::is_equal_i( species_parent_group, species::SPECIES_GROUP_NAMES[species::SPECIES_GROUP_ANY]))))
        {
            CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::set_single_species_meta(): ",
                    "species group must be identical along an inheritance line",
                    "  [mismatch for=",_species->mnemonic,",expect=",species_group,",got=",species_parent_group,"]");
            return  -1;
        }
        cbm::as_strcpy( &_species->parent, species_parent);
    }

    return  0;
}
int
iproc_reader_speciesparameters_lrsrc_t::get_species_meta_(
        speciesparameters::parameterized_species_t *  _species, size_t  _s_cnt,
        int *  _r, int *  _s)
const
{
    cbm::Lresources_t::key_list_t  species;
    cbm::Lresources_t::key_type  species_key;
    cbm::Lresources_t::make_key( &species_key, "^%s.[^\\.]+.%s*$", SPECIES_TAG, SPECIES_MNEMONIC);
    CBM_DefaultResources.find_matching( &species, species_key.name);

    *_r = 0;
    *_s = species.count();
    for ( int  s = 0;  s < species.count(); ++s)
    {
        char const *  s_name = CBM_DefaultResources.find( species[s]);
        if ( !s_name)
        {
            CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::get_species_meta(): ",
                    "a species got lost :(  [species=",species[s],"]");
            continue;
        }
        if ( *_r >= static_cast< int >( _s_cnt))
        {
            /* "overflow" */
            continue;
        }

        _species[*_r].m.group = species::SPECIES_GROUP_NONE;
        cbm::as_strcpy( &_species[*_r].m.parent, species::SPECIES_NAME_NONE);
        int  rc_cpy = cbm::as_strcpy( &_species[*_r].m.mnemonic, s_name);
        if ( rc_cpy != 0)
        {
            CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::get_species_meta(): ",
                    "unexpected error while copying species mnemonic  [species=",s_name,"]");
            return  -1;
        }

        bool  not_seen = true;
        size_t const  R = *_r;
        for ( size_t  k = 0;  k < R;  ++k)
        {
            if ( cbm::is_equal( _species[k].m.mnemonic, _species[*_r].m.mnemonic))
            {
                CBM_LogWarn( "iproc_reader_speciesparameters_lrsrc_t::get_species_meta(): ",
                        "data file contains multiple blocks for species  [object-id=",this->object_id(),",species=",_species[*_r].m.mnemonic,"]");
                not_seen = false;
                break;
            }
        }

        if ( not_seen)
        {
            int  rc_set = this->set_single_species_meta_( &_species[*_r].m);
            if ( rc_set)
            {
                return  -1;
            }
            ++*_r;
        }
    }
 
    return  0;
}
lerr_t
iproc_reader_speciesparameters_lrsrc_t::get_species_meta(
        speciesparameters::parameterized_species_t *  _species, size_t  _s_cnt,
        int *  _r, int *  _s)
const
{
    if ( _s_cnt == 0)
    {
        return  LDNDC_ERR_OK;
    }
    int  r = 0;
    int  s = 0;
    int  rc_meta = this->get_species_meta_( _species, _s_cnt, &r, &s);

    if ( _r) *_r = r;
    if ( _s) *_s = s;

    if ( rc_meta == -1)
    {
        return  LDNDC_ERR_FAIL;
    }
    if ( static_cast< size_t >( r) < _s_cnt)
    {
        CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::get_species_meta(): ",
                "read less species than exspected  [expected=",_s_cnt,",read=",r,"]");
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    else if ( static_cast< size_t >( s) > _s_cnt)
    {
        CBM_LogWarn( "iproc_reader_speciesparameters_lrsrc_t::get_species_meta(): ",
                "i have seen invalid species parameter blocks  [#=",s-_s_cnt,"]");
    }
    return  LDNDC_ERR_OK;
}

size_t
iproc_reader_speciesparameters_lrsrc_t::check_species_properties_given(
        char const *  _species_name, ldndc::bitset *  /*_marker*/, size_t  /*_marker_cnt*/)
const
{
    cbm::Lresources_t::key_type  key;
    cbm::Lresources_t::make_key( &key, "%s.%s.%s", SPECIES_TAG, _species_name, SPECIES_MNEMONIC);
    if ( CBM_DefaultResources.find( key.name))
    {
        return  speciesparameters::SPECIESPARAMETERS_CNT;
    }
    return  invalid_size;
}


int
iproc_reader_speciesparameters_lrsrc_t::get_species_properties_(
        speciesparameters::parameterized_species_t *  _species, size_t  _s_cnt,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props)
const
{
    if ( !_species || ( _s_cnt == 0))
    {
        return  LDNDC_ERR_OK;
    }

    int  r = 0;

    size_t  j = 0;
    while ( j < _s_cnt)
    {
        lerr_t  rc_set = this->set_single_species_properties_( _species[j].p, _species[j].m.mnemonic,
                _props_names, _props_types, _n_props, NULL, NULL);

        if ( rc_set == LDNDC_ERR_OK)
        {
            ++r;
        }
        ++j;
    }
    return  r;
}

lerr_t
iproc_reader_speciesparameters_lrsrc_t::get_all_species_properties(
        speciesparameters::parameterized_species_t **  _species, size_t *  _s_cnt,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _s_read, int *  _s_seen,
        int  /*_p_read*/[], int  /*_p_seen*/[])
const
{
    if ( !_species || !_s_cnt)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    size_t const  species_count = this->get_species_cnt();
    CBM_LogDebug( "seeing ",species_count," species");
    if ( species_count == 0)
    {
        return  LDNDC_ERR_OK;
    }

    speciesparameters::parameterized_species_t *  species =
        CBM_DefaultAllocator->construct< speciesparameters::parameterized_species_t >( species_count);
    if ( !species)
    {
        return  LDNDC_ERR_NOMEM;
    }

    int  r = 0;
    int  s = 0;
    lerr_t  rc_meta = this->get_species_meta( species, species_count, &r, &s);
    if ( rc_meta)
    {
        CBM_DefaultAllocator->destroy( species, species_count);
        return  LDNDC_ERR_FAIL;
    }
    if( r == 0)
    {
        CBM_LogDebug( "no species found");
        *_s_cnt = 0;
        if ( _s_read) { *_s_read = 0; }
        if ( _s_seen) { *_s_seen = 0; }
    }
    else if ( r == static_cast< int >( species_count))
    {
        CBM_LogDebug( "read ",r," species parameter sets");
        *_species = species;
        *_s_cnt = static_cast< size_t >( r);

        int  r_p = this->get_species_properties_( species, species_count, _props_names, _props_types, _n_props);
        if ( r_p != r)
        {
            CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::get_all_species_properties(): ",
                    "species vanished while reading their properties  [expected=",r,",read=",r_p,"]");
            return  LDNDC_ERR_FAIL;
        }
        if ( _s_read) { *_s_read = r; }
        if ( _s_seen) { *_s_seen = s; }
    }
    else
    {
        CBM_LogError( "iproc_reader_speciesparameters_lrsrc_t::get_all_species_properties(): ",
                "found less species than expected  [expected=",species_count,",read=",r,"]");
        return  LDNDC_ERR_FAIL;
    }

    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}
lerr_t
iproc_reader_speciesparameters_lrsrc_t::get_species_properties(
                speciesparameters::parameterized_species_t  _species[], size_t  _s_cnt,
                ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
                int *  _s_read, int *  _s_seen,
                int  /*_p_read*/[], int  /*_p_seen*/[])
const
{
    lerr_t  rc_meta = this->get_species_meta( _species, _s_cnt, _s_read, _s_seen);
    if ( rc_meta)
    {
        return  LDNDC_ERR_FAIL;
    }

        int  r = this->get_species_properties_( _species, _s_cnt, _props_names, _props_types, _n_props);
        if ( _s_read) { *_s_read = r; }

        if( static_cast< size_t >( r) < _s_cnt)
        {
                return  LDNDC_ERR_FAIL;
        }

    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}


/*  *****  SPECIES PARAMETER LRESOURCES READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_speciesparameters_lrsrc_t)
iproc_landscape_reader_speciesparameters_lrsrc_t::iproc_landscape_reader_speciesparameters_lrsrc_t()
        : iproc_landscape_reader_lrsrc_t(),
          iproc_landscape_interface_speciesparameters_t()
{
}


iproc_landscape_reader_speciesparameters_lrsrc_t::~iproc_landscape_reader_speciesparameters_lrsrc_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_SPECIESPARAMETERS_LRSRC_CPP_  */

