
#ifndef  LDNDC_IO_IREADER_SITEPARAMETERS_LRSRC_CPP_
#define  LDNDC_IO_IREADER_SITEPARAMETERS_LRSRC_CPP_

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_siteparameters.h"
#include  "io/data-provider/ireaders/shared/auxiliary.h"

namespace ldndc {

/*  *****  SITEPARAMETER LRSRC READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_siteparameters_lrsrc_t)
iproc_reader_siteparameters_lrsrc_t::iproc_reader_siteparameters_lrsrc_t()
        : iproc_reader_lrsrc_t(),
          iproc_interface_siteparameters_t()
{
    this->data_arrived = 1;
}
iproc_reader_siteparameters_lrsrc_t::~iproc_reader_siteparameters_lrsrc_t()
{
}

lerr_t
iproc_reader_siteparameters_lrsrc_t::get_siteparameters_set(
        siteparameters::siteparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const _props_types[], size_t  _n_props,
        int *  _r, int *  _s)
const
{
    typedef  siteparameters::siteparameter_t  pt;

    lerr_t  rc = LDNDC_ERR_OK;

    int  r = 0;
    int  s = 0;
    for ( size_t  p = 0;  p < _n_props;  ++p)
    {
        cbm::Lresources_t::key_type  key;
        cbm::Lresources_t::make_key( &key, "site.parameter.%s.value", _props_names[p]);


        char const *  p_val = CBM_DefaultResources.find( key.name);
        if ( !p_val || cbm::is_empty( p_val) || cbm::is_whitespace( p_val))
        {
            CBM_LogError( "iproc_reader_siteparameters_lrsrc_t::get_siteparameters_set(): ",
                    "missing parameter in Lresources, please update  [parameter=",_props_names[p],"]");
            rc = LDNDC_ERR_RUNTIME_ERROR;
            continue;
        }

        ++r;
        int  isvalid = -1;
        lerr_t  rc_assign = iproc_reader_parameters_auxiliary_t< pt >::assign_value(
                &_props[p], p_val, _props_types[p], &isvalid);
        if ( rc_assign)
        {
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        if ( isvalid == 1)
        {
            ++s;
        }
        else if ( isvalid == 0)
        {
            CBM_LogWarn( "iproc_reader_siteparameters_lrsrc_t::get_siteparameters_set(): ",
                    "missing or invalid parameter  [parameter=",_props_names[p],",value=",p_val,"]");
        }
        else
        {
            CBM_LogFatal( "[BUG]");
        }
    }

    /* parameters read */
    if ( _r)
    {
        *_r = r;
    }
    /* parameters valid */
    if ( _s)
    {
        *_s = s;
    }

    this->data_arrived = 0;
    return  rc;
}



/*  *****  SITEPARAMETER LRSRC LANDSCAPE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_siteparameters_lrsrc_t)
iproc_landscape_reader_siteparameters_lrsrc_t::iproc_landscape_reader_siteparameters_lrsrc_t()
        : iproc_landscape_reader_lrsrc_t(),
          iproc_landscape_interface_siteparameters_t()
{
}
iproc_landscape_reader_siteparameters_lrsrc_t::~iproc_landscape_reader_siteparameters_lrsrc_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_SITEPARAMETERS_LRSRC_CPP_  */

