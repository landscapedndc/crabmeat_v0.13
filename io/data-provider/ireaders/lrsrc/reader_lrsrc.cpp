/*!
 * @brief
 *    concrete Lresources readers (implementation)
 *
 * @author
 *    steffen klatt (created on: mar 16, 2014)
 */

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc.h"
#include  "resources/Lresources.h"
#include  "io/ifactory.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

lid_t const  LRSRC_DEFAULT_BLOCK_ID = 0;

iproc_landscape_reader_lrsrc_base_t::iproc_landscape_reader_lrsrc_base_t()
        : iproc_landscape_reader_base_t()
{
}


iproc_landscape_reader_lrsrc_base_t::~iproc_landscape_reader_lrsrc_base_t()
{
}


iproc_landscape_reader_lrsrc_t::iproc_landscape_reader_lrsrc_t()
        : iproc_landscape_reader_lrsrc_base_t(),
          rdr( NULL)
{
}


iproc_landscape_reader_lrsrc_t::~iproc_landscape_reader_lrsrc_t()
{
    if ( this->rdr)
    {
        iproc_factories_t  ifactory;
        ifactory.destroy_ireader( this->rdr);
        this->rdr = NULL;
    }
}

int
iproc_landscape_reader_lrsrc_t::scan_stream(
        iobject_handler_base_t *)
{
    return  1;
}
int
iproc_landscape_reader_lrsrc_t::rescan_stream()
{
    return  this->scan_stream( NULL);
}


size_t
iproc_landscape_reader_lrsrc_t::number_of_blocks()
const
{
    return  1;
}
bool
iproc_landscape_reader_lrsrc_t::check_input_exists(
        lid_t const &  _id)
const
{
    return  _id == LRSRC_DEFAULT_BLOCK_ID;
}

int
iproc_landscape_reader_lrsrc_t::get_available_blocks(
        lid_t  _list[], size_t  _max_size)
const
{
    if ( _max_size < 1)
    {
        return  0;
    }
    _list[0] = LRSRC_DEFAULT_BLOCK_ID;
    return  1;
}

iproc_provider_t *
iproc_landscape_reader_lrsrc_t::acquire_ireader(
        lid_t const &)
{
    if ( this->rdr)
    {
        return  this->rdr;
    }

    iproc_factories_t  ifactory;
    this->rdr = 
        dynamic_cast< iproc_reader_lrsrc_t * >( ifactory.construct_ireader( this->input_class_type(), this->io_format_type()));
    if ( this->rdr)
    {
        this->rdr->set_object_id( LRSRC_DEFAULT_BLOCK_ID);
    }

    return  this->rdr;
}
void
iproc_landscape_reader_lrsrc_t::release_ireader(
        lid_t const &)
{
    if ( !this->rdr)
    {
        return;
    }

    iproc_factories_t  ifactory;
    ifactory.destroy_ireader( this->rdr);
    this->rdr = NULL;
}

lerr_t
iproc_landscape_reader_lrsrc_t::add_to_processing_stack(
        iproc_provider_t **,
        iproc_provider_t *)
{
    return  LDNDC_ERR_FAIL;
}

int
iproc_landscape_reader_lrsrc_t::collective_update_commence(
        cbm::sclock_t const *)
{
    return  0;
}
int
iproc_landscape_reader_lrsrc_t::collective_update_complete(
        cbm::sclock_t const *)
{
    return  0;
}

iproc_reader_lrsrc_base_t::iproc_reader_lrsrc_base_t()
        : iproc_reader_base_t()
{
}
iproc_reader_lrsrc_base_t::~iproc_reader_lrsrc_base_t()
{
}



iproc_reader_lrsrc_t::iproc_reader_lrsrc_t()
        : iproc_reader_lrsrc_base_t()
{
}
iproc_reader_lrsrc_t::~iproc_reader_lrsrc_t()
{
}

lid_t const &
iproc_reader_lrsrc_t::object_id()
const
{
    return  LRSRC_DEFAULT_BLOCK_ID;
}
bool
iproc_reader_lrsrc_t::is_open()
const
{
    return  CBM_DefaultResources.is_open();
}
}

