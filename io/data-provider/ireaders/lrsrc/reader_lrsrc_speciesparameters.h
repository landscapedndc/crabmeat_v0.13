/*!
 * @brief
 *    Lresources speciesparameters reader
 *
 * @author
 *    steffen klatt (created on: mar 16, 2014)
 */

#ifndef  LDNDC_IO_IREADER_SPECIESPARAMETERS_LRSRC_H_
#define  LDNDC_IO_IREADER_SPECIESPARAMETERS_LRSRC_H_

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc.h"
#include  "io/iif/iif_speciesparameters.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_speciesparameters_lrsrc_t  :  public  iproc_landscape_reader_lrsrc_t,  public  iproc_landscape_interface_speciesparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_speciesparameters_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_speciesparameters_t::input_class_type(); }

    public:
        iproc_landscape_reader_speciesparameters_lrsrc_t();
        virtual  ~iproc_landscape_reader_speciesparameters_lrsrc_t();
};
class  CBM_API  iproc_reader_speciesparameters_lrsrc_t  :  public  iproc_reader_lrsrc_t,  public  iproc_interface_speciesparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_speciesparameters_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_interface_speciesparameters_t::input_class_type(); }

        static char const *  SPECIES_TAG;
        static char const *  SPECIES_MNEMONIC;
        static char const *  SPECIES_GROUP;
        static char const *  SPECIES_PARENT;
        static char const *  SPECIES_NAME;

        static char const *  SPECIES_PARAMETER;
        static char const *  SPECIES_PARAMETER_VALUE;

        static char const *  SPECIES_MNEMONIC_META;

    public:
        iproc_reader_speciesparameters_lrsrc_t();
        virtual  ~iproc_reader_speciesparameters_lrsrc_t();

    public:
        lerr_t  get_species_default_properties(
                speciesparameters::speciesparameter_t [], ldndc_string_t const [], char const [], size_t, int *, int *) const;

        size_t  get_species_cnt() const;
        size_t  check_species_properties_given(
                char const *, ldndc::bitset * = NULL, size_t = 0) const;
        lerr_t  get_species_properties(
                speciesparameters::parameterized_species_t [], size_t,
                ldndc_string_t const [], char const [], size_t,
                int *, int *, int [], int []) const;
        lerr_t  get_all_species_properties(
                speciesparameters::parameterized_species_t **, size_t *,
                ldndc_string_t const [], char const [], size_t,
                int *, int *, int [], int []) const;

    private:
        int  set_single_species_meta_(
                speciesparameters::speciesparameters_meta_t * /*species buffer*/) const;

        lerr_t  get_species_meta(
                speciesparameters::parameterized_species_t *, size_t, int *, int *) const;
        int  get_species_meta_(
                speciesparameters::parameterized_species_t * /*buffer*/, size_t /*buffer size*/,
                int *, int *) const;
        int  get_species_properties_(
                speciesparameters::parameterized_species_t *  /*species buffer*/, size_t  /*buffer size*/,
                ldndc_string_t const /*property names*/[], char const  /*property types*/[], size_t  /*property count*/) const;
        lerr_t  set_single_species_properties_(
                speciesparameters::speciesparameter_t [] /*properties buffer*/, char const * /*species type*/,
                ldndc_string_t const [] /*property names*/, char const [] /*property types*/, size_t /*number of properties*/,
                int * /*read*/, int * /*seen*/) const;
};
}

#endif  /*  !LDNDC_IO_IREADER_SPECIESPARAMETERS_LRSRC_H_  */

