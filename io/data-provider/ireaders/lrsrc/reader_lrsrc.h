/*!
 * @brief
 *    concrete Lresources readers
 *
 * @author
 *    steffen klatt (created on: mar 16, 2014)
 */

#ifndef  LDNDC_IO_IREADER_LRSRC_H_
#define  LDNDC_IO_IREADER_LRSRC_H_

#include  "io/data-provider/ireaders/ireader.h"
#include  "io/fhandler/lrsrc/fhandler_lrsrc.h"

#include  "time/cbm_time.h"

namespace ldndc {

extern lid_t const  LRSRC_DEFAULT_BLOCK_ID;

/* ###   LRESOURCES INPUT READER   ### */
class  CBM_API  iproc_reader_lrsrc_base_t
    :  public  iproc_reader_base_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_lrsrc_base_t::format(); }
    protected:
        iproc_reader_lrsrc_base_t();
        virtual  ~iproc_reader_lrsrc_base_t() = 0;
};

class  CBM_API  iproc_reader_lrsrc_t  :  public  iproc_reader_lrsrc_base_t
{
    public:
        lid_t const &  object_id() const;
        bool  is_open() const;

    protected:
        iproc_reader_lrsrc_t();
        virtual  ~iproc_reader_lrsrc_t() = 0;
};


/* ###   LRESOURCES LANDSCAPE INPUT READER   ### */
class  CBM_API  iproc_landscape_reader_lrsrc_base_t
    :  public  iproc_landscape_reader_base_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_lrsrc_base_t::format(); }
    protected:
        iproc_landscape_reader_lrsrc_base_t();
        virtual  ~iproc_landscape_reader_lrsrc_base_t() = 0;
};

class  CBM_API  iproc_landscape_reader_lrsrc_t  :  public  iproc_landscape_reader_lrsrc_base_t
{
    public:
        lerr_t  status() const { return  LDNDC_ERR_OK; }

        int  scan_stream(
                iobject_handler_base_t * /*i/o format handler*/);
        int  rescan_stream();

        iproc_provider_t *  acquire_ireader(
                lid_t const & /*block id*/);
        void  release_ireader(
                lid_t const & /*block id*/);

        lerr_t  add_to_processing_stack(
                iproc_provider_t ** /*address of pointer to reader*/,
                iproc_provider_t * /*data processors*/);

        int  get_available_blocks(
                lid_t [], size_t) const;
    public:
        size_t  size() const { return  this->number_of_blocks(); }
        size_t  number_of_blocks() const;
        bool    check_input_exists(
                lid_t const & /*block id*/) const;

    public:
        int  collective_update_commence(
                cbm::sclock_t const *);
        int  collective_update_complete(
                cbm::sclock_t const *);
    protected:
        iproc_landscape_reader_lrsrc_t();
        virtual  ~iproc_landscape_reader_lrsrc_t() = 0;

    private:
        iproc_reader_lrsrc_t *  rdr;
};
}

#endif  /*  !LDNDC_IO_IREADER_LRSRC_H_  */

