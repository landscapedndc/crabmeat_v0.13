
#ifndef  LDNDC_IO_IREADER_ANYCLASS_LRSRC_CPP_
#define  LDNDC_IO_IREADER_ANYCLASS_LRSRC_CPP_

#include  "resources/Lresources.h"
#include  "io/outputtypes.h"

namespace ldndc {

/*  *****  ANYCLASS LRESOURCES CORE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_anyclass_lrsrc_t)
iproc_reader_anyclass_lrsrc_t::iproc_reader_anyclass_lrsrc_t()
        : iproc_reader_lrsrc_t(),
          iproc_interface_anyclass_t()
{
    this->data_arrived = 1;
}
iproc_reader_anyclass_lrsrc_t::~iproc_reader_anyclass_lrsrc_t()
{
}

int
iproc_reader_anyclass_lrsrc_t::read_by_keys(
        void **  _data, sink_entity_layout_t const *  _meta)
const
{
    if ( !_data || !_meta)
    {
        CBM_LogError( "iproc_reader_anyclass_lrsrc_t::read_by_keys(): ",
                "invalid arguments");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    lerr_t  rc_read = LDNDC_ERR_OK;

    char  v_buf[Lresources_t::VALUE_MAXSIZE];

    int const  r_end = _meta->layout.rank;
    int  e = 0;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = _meta->layout.types[r];
        int const  this_size = _meta->layout.sizes[r];

        void *  c_data = _data[r];
        if ( !c_data)
        {
            continue;
        }

        for ( int  ent = 0;  ent < this_size;  ++ent, ++e)
        {
            char const *  entitykey = _meta->ents.ids[e];
            if ( !entitykey)
            {
                continue;
            }
            int const  v_size = ( _meta->layout.entsizes) ? _meta->layout.entsizes[e] : 1;
            if ( v_size == 0)
            {
                continue;
            }
            char const *  v_data = Lresources.find( entitykey);
            if ( !v_data || ( cbm::strnlen( v_data, Lresources.max_valuesize()) == 0))
            {
                CBM_LogWarn( "iproc_reader_anyclass_lrsrc_t::read_by_keys(): ",
                        "key not found in input  [entity=",entitykey,"]");
                continue;
            }
            cbm::strncpy( v_buf, v_data, Lresources.max_valuesize());

            int  n_val = 0;

            if ( this_datatype == LDNDC_FLOAT64)
            {
                ldndc_flt64_t *  c_ptr = (ldndc_flt64_t *)c_data;
                n_val = cbm::s2n_c< ldndc_flt64_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_INT32)
            {
                ldndc_int32_t *  c_ptr = (ldndc_int32_t *)c_data;
                n_val = cbm::s2n_c< ldndc_int32_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_UINT32)
            {
                ldndc_uint32_t *  c_ptr = (ldndc_uint32_t *)c_data;
                n_val = cbm::s2n_c< ldndc_uint32_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_FLOAT32)
            {
                ldndc_flt32_t *  c_ptr = (ldndc_flt32_t *)c_data;
                n_val = cbm::s2n_c< ldndc_flt32_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_STRING)
            {
                ldndc_string_t *  c_ptr = (ldndc_string_t *)c_data;
                n_val = cbm::s2n_c< ldndc_string_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_INT64)
            {
                ldndc_int64_t *  c_ptr = (ldndc_int64_t *)c_data;
                n_val = cbm::s2n_c< ldndc_int64_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_UINT64)
            {
                ldndc_uint64_t *  c_ptr = (ldndc_uint64_t *)c_data;
                n_val = cbm::s2n_c< ldndc_uint64_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_BOOL)
            {
                ldndc_bool_t *  c_ptr = (ldndc_bool_t *)c_data;
                n_val = cbm::s2n_c< ldndc_bool_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_CHAR)
            {
                ldndc_char_t *  c_ptr = (ldndc_char_t *)c_data;
                n_val = cbm::s2n_c< ldndc_char_t >( c_ptr, v_size, v_buf);
                c_data = c_ptr + v_size;
            }
            else
            {
                CBM_LogError( "iproc_reader_anyclass_lrsrc_t::read_by_keys(): ",
                        "datatype not supported  [entity=",entitykey,",datatype=",this_datatype,"]");
                n_val = -1;
            }

            if ( n_val < 0)
            {
                CBM_LogError( "iproc_reader_anyclass_lrsrc_t::read_by_keys(): ",
                        "failed to convert data  [entity=",entitykey,"]");
                rc_read = LDNDC_ERR_FAIL;
            }
            else if ( n_val != _meta->layout.entsizes[e])
            {
                CBM_LogWarn( "iproc_reader_anyclass_lrsrc_t::read_by_keys(): ",
                        "incomplete data line for key  [entity=",entitykey,"]");
            }
        }
    }

    return  rc_read;
}


/*  *****  ANYCLASS LRESOURCES READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_anyclass_lrsrc_t)
iproc_landscape_reader_anyclass_lrsrc_t::iproc_landscape_reader_anyclass_lrsrc_t()
        : iproc_landscape_reader_lrsrc_t(),
          iproc_landscape_interface_anyclass_t()
{
}


iproc_landscape_reader_anyclass_lrsrc_t::~iproc_landscape_reader_anyclass_lrsrc_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_ANYCLASS_LRSRC_CPP_  */

