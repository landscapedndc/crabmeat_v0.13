/*!
 * @brief
 *    concrete Lresources anyclass reader
 *
 * @author
 *    steffen klatt (created on: mar 20, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_IO_IREADER_ANYCLASS_LRSRC_H_
#define  LDNDC_IO_IREADER_ANYCLASS_LRSRC_H_

#include  "io/iif/iif_anyclass.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_anyclass_lrsrc_t  :  public  iproc_landscape_reader_lrsrc_t,  public  iproc_landscape_interface_anyclass_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_anyclass_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_anyclass_t::input_class_type(); }

    public:
        iproc_landscape_reader_anyclass_lrsrc_t();
        virtual  ~iproc_landscape_reader_anyclass_lrsrc_t();
};
class  CBM_API  iproc_reader_anyclass_lrsrc_t  :  public  iproc_reader_lrsrc_t,  public  iproc_interface_anyclass_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_anyclass_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_interface_anyclass_t::input_class_type(); }

    public:
        iproc_reader_anyclass_lrsrc_t();
        virtual  ~iproc_reader_anyclass_lrsrc_t();

    public:
        int  read_by_keys(
                void **, sink_entity_layout_t const *) const;
};
}

#endif  /*  !LDNDC_IO_IREADER_ANYCLASS_LRSRC_H_  */

