
#ifndef  LDNDC_IO_IREADER_SOILPARAMETERS_LRSRC_CPP_
#define  LDNDC_IO_IREADER_SOILPARAMETERS_LRSRC_CPP_

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_soilparameters.h"
#include  "soillayers/soillayerstypes.h"
#include  "io/data-provider/ireaders/shared/auxiliary.h"

#include  "string/cbm_string.h"

namespace ldndc {

char const *  iproc_reader_soilparameters_lrsrc_t::SOIL_TAG = "soil";
char const *  iproc_reader_soilparameters_lrsrc_t::SOIL_GROUP = "soils";
char const *  iproc_reader_soilparameters_lrsrc_t::SOIL_MNEMONIC = "mnemonic";
char const *  iproc_reader_soilparameters_lrsrc_t::SOIL_MNEMONIC_META = "METASOIL";
char const *  iproc_reader_soilparameters_lrsrc_t::SOIL_NAME = "name";

char const *  iproc_reader_soilparameters_lrsrc_t::HUMUS_TAG = "humus";
char const *  iproc_reader_soilparameters_lrsrc_t::HUMUS_GROUP = "humuses";
char const *  iproc_reader_soilparameters_lrsrc_t::HUMUS_MNEMONIC = "mnemonic";
char const *  iproc_reader_soilparameters_lrsrc_t::HUMUS_MNEMONIC_META = "METAHUMUS";
char const *  iproc_reader_soilparameters_lrsrc_t::HUMUS_NAME = "name";

char const *  iproc_reader_soilparameters_lrsrc_t::PARAMETER = "parameter";
char const *  iproc_reader_soilparameters_lrsrc_t::PARAMETER_VALUE = "value";

/*  *****  SOIL PARAMETERS LRSRC READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_soilparameters_lrsrc_t)
iproc_reader_soilparameters_lrsrc_t::iproc_reader_soilparameters_lrsrc_t()
        : iproc_reader_lrsrc_t(),
          iproc_interface_soilparameters_t()
{
    this->data_arrived = 1;
}
iproc_reader_soilparameters_lrsrc_t::~iproc_reader_soilparameters_lrsrc_t()
{
}

lerr_t
iproc_reader_soilparameters_lrsrc_t::get_humus_default_properties(
        humusparameters::humusparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    return  this->set_single_properties_< humusparameters::humusparameter_t >(
            _props, HUMUS_TAG, HUMUS_MNEMONIC_META,
            _props_names, _props_types, _n_props,
                   _p_read, _p_seen);
}

lerr_t
iproc_reader_soilparameters_lrsrc_t::get_soil_default_properties(
        soilparameters::soilparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    return  this->set_single_properties_< soilparameters::soilparameter_t >(
            _props, SOIL_TAG, SOIL_MNEMONIC_META,
            _props_names, _props_types, _n_props,
            _p_read, _p_seen);
}

lerr_t
iproc_reader_soilparameters_lrsrc_t::get_humus_properties(
        humusparameters::humusparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        char const *  _humus_names[], size_t  _n_humuses,
        int *  _read, int *  _seen,
        int [], int [])
const
{
    lerr_t  rc_humus = this->set_properties_< humusparameters::humusparameter_t >(
            _props, HUMUS_TAG,
            _humus_names, _n_humuses,
            _props_names, _props_types, _n_props,
            _read, _seen);
    if ( rc_humus)
    {
        return  LDNDC_ERR_FAIL;
    }
    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}

lerr_t
iproc_reader_soilparameters_lrsrc_t::get_soil_properties(
        soilparameters::soilparameter_t  _props[],
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        char const *  _soil_names[], size_t  _n_soils,
        int *  _read, int *  _seen,
        int [], int [])
const
{
    lerr_t  rc_soil = this->set_properties_< soilparameters::soilparameter_t >(
            _props, SOIL_TAG,
            _soil_names, _n_soils,
            _props_names, _props_types, _n_props,
            _read, _seen);
    if ( rc_soil)
    {
        return  LDNDC_ERR_FAIL;
    }
    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}

template < typename  _P >
lerr_t
iproc_reader_soilparameters_lrsrc_t::set_properties_(
        _P *  _props, char const *  _tag,
        char const *  _names[], size_t  _n_names,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _read, int *  _seen)
const
{
    if ( !_props || !_tag || ( _n_names == 0))
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc = LDNDC_ERR_OK;

    int  r = 0;
    int  s = 0;
    size_t  j = 0;
    while ( j < _n_names)
    {
        int  p_read = 0;
        int  p_valid = 0;

        lerr_t  rc_set = this->set_single_properties_< _P >(
                &_props[j*_n_props], _tag, _names[j],
                _props_names, _props_types, _n_props,
                &p_read, &p_valid);
        if ( rc_set)
        {
            rc = LDNDC_ERR_FAIL;
        }
        else if ( p_valid == static_cast< int >( _n_props))
        {
            ++r;
        }
        else if ( p_valid > 0)
        {
            ++s;
        }
        ++j;
    }

    if ( _read)
    {
        *_read = r;
    }
    if ( _seen)
    {
        *_seen = s;
    }


    return  rc;
}

template < typename  _P >
lerr_t
iproc_reader_soilparameters_lrsrc_t::set_single_properties_(
        _P  _props[], char const *  _tag, char const *  _typename,
        ldndc_string_t const  _props_names[], char const  _props_types[], size_t  _n_props,
        int *  _r, int *  _s)
const
{
    lerr_t  rc = LDNDC_ERR_OK;

    int  r = 0;
    int  s = 0;
    for ( size_t  p = 0;  p < _n_props;  ++p)
    {
        cbm::Lresources_t::key_type  key;
        cbm::Lresources_t::make_key( &key, "%s.%s.%s.%s.%s",
                _tag, _typename, PARAMETER, _props_names[p], PARAMETER_VALUE);
        char const *  p_val = CBM_DefaultResources.find( key.name);
        if ( !p_val || cbm::is_empty( p_val) || cbm::is_whitespace( p_val))
        {
            CBM_LogError( "iproc_reader_soilparameters_lrsrc_t::set_single_properties(): ",
                    "missing parameter in Lresources, please update  [",_tag,"=",_typename,",parameter=",_props_names[p],"]");
            rc = LDNDC_ERR_RUNTIME_ERROR;
            continue;
        }

        ++r;
        int  isvalid = -1;
        lerr_t  rc_assign = iproc_reader_parameters_auxiliary_t< _P >::assign_value(
                &_props[p], p_val, _props_types[p], &isvalid);
        if ( rc_assign)
        {
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        if ( isvalid == 1)
        {
            ++s;
        }
        else if ( isvalid == 0)
        {
            CBM_LogWarn( "iproc_reader_soilparameters_lrsrc_t::set_single_properties(): ",
                    "missing or invalid parameter  [soil=",_typename,",parameter=",_props_names[p],",value=",p_val,"]");
        }
        else
        {
            CBM_LogFatal( "[BUG]");
        }
    }

    /* parameters read */
    if ( _r)
    {
        *_r = r;
    }
    /* parameters valid */
    if ( _s)
    {
        *_s = s;
    }

    return  rc;
}



/*  *****  SOIL PARAMETERS LRSRC LANDSCAPE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_soilparameters_lrsrc_t)
iproc_landscape_reader_soilparameters_lrsrc_t::iproc_landscape_reader_soilparameters_lrsrc_t()
        : iproc_landscape_reader_lrsrc_t(),
          iproc_landscape_interface_soilparameters_t()
{
}


iproc_landscape_reader_soilparameters_lrsrc_t::~iproc_landscape_reader_soilparameters_lrsrc_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_SOILPARAMETERS_LRSRC_CPP_  */

