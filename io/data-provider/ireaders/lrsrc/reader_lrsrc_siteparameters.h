/*!
 * @brief
 *    Lresources siteparameters readers
 *
 * @author
 *    steffen klatt (created on: mar 26, 2014)
 */

#ifndef  LDNDC_IO_IREADER_SITEPARAMETERS_LRSRC_H_
#define  LDNDC_IO_IREADER_SITEPARAMETERS_LRSRC_H_

#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc.h"
#include  "io/iif/iif_siteparameters.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_siteparameters_lrsrc_t :  public  iproc_landscape_reader_lrsrc_t,  public  iproc_landscape_interface_siteparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_siteparameters_lrsrc_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_siteparameters_t::input_class_type(); }

    public:
        iproc_landscape_reader_siteparameters_lrsrc_t();
        virtual  ~iproc_landscape_reader_siteparameters_lrsrc_t();
};
class  CBM_API  iproc_reader_siteparameters_lrsrc_t  :  public  iproc_reader_lrsrc_t,  public  iproc_interface_siteparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_siteparameters_lrsrc_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_siteparameters_t::input_class_type(); }
    public:
        iproc_reader_siteparameters_lrsrc_t();
        virtual  ~iproc_reader_siteparameters_lrsrc_t();

    public:
        lerr_t  get_siteparameters_set(
                siteparameters::siteparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                int *, int *) const;
};
}

#endif  /*  !LDNDC_IO_IREADER_SITEPARAMETERS_LRSRC_H_  */

