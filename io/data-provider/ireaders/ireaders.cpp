/*!
 * @brief
 *    input readers factory array (implementation)
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#include  "io/data-provider/ireaders/ireaders.h"

#ifdef  LDNDC_IO_SQLITE3
#  include  "io/data-provider/ireaders/sqlite3/reader_sqlite3_anyclass.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_anyclass_sqlite3_t >  ireader_landscape_anyclass_sqlite3_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_anyclass_sqlite3_t >  ireader_anyclass_sqlite3_factory;
#endif
#ifdef  LDNDC_IO_TXT
#  include  "io/data-provider/ireaders/txt/reader_txt_airchemistry.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_airchemistry_txt_t >  ireader_landscape_airchemistry_txt_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_airchemistry_txt_t >  ireader_airchemistry_txt_factory;
#  include  "io/data-provider/ireaders/txt/reader_txt_climate.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_climate_txt_t >  ireader_landscape_climate_txt_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_climate_txt_t >  ireader_climate_txt_factory;
#  include  "io/data-provider/ireaders/txt/reader_txt_event.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_event_txt_t >  ireader_landscape_event_txt_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_event_txt_t >  ireader_event_txt_factory;
#  include  "io/data-provider/ireaders/txt/reader_txt_geometry.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_geometry_txt_t >  ireader_landscape_geometry_txt_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_geometry_txt_t >  ireader_geometry_txt_factory;
#  include  "io/data-provider/ireaders/txt/reader_txt_groundwater.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_groundwater_txt_t >  ireader_landscape_groundwater_txt_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_groundwater_txt_t >  ireader_groundwater_txt_factory;
#  include  "io/data-provider/ireaders/txt/reader_txt_siteparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_siteparameters_txt_t >  ireader_landscape_siteparameters_txt_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_siteparameters_txt_t >  ireader_siteparameters_txt_factory;
#endif
#ifdef  LDNDC_IO_XML
#  include  "io/data-provider/ireaders/xml/reader_xml_checkpoint.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_checkpoint_xml_t >  ireader_landscape_checkpoint_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_checkpoint_xml_t >  ireader_checkpoint_xml_factory;
#  include  "io/data-provider/ireaders/xml/reader_xml_event.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_event_xml_t >  ireader_landscape_event_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_event_xml_t >  ireader_event_xml_factory;
#  include  "io/data-provider/ireaders/xml/reader_xml_setup.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_setup_xml_t >  ireader_landscape_setup_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_setup_xml_t >  ireader_setup_xml_factory;
#  include  "io/data-provider/ireaders/xml/reader_xml_site.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_site_xml_t >  ireader_landscape_site_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_site_xml_t >  ireader_site_xml_factory;
#  include  "io/data-provider/ireaders/xml/reader_xml_siteparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_siteparameters_xml_t >  ireader_landscape_siteparameters_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_siteparameters_xml_t >  ireader_siteparameters_xml_factory;
#  include  "io/data-provider/ireaders/xml/reader_xml_soilparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_soilparameters_xml_t >  ireader_landscape_soilparameters_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_soilparameters_xml_t >  ireader_soilparameters_xml_factory;
#  include  "io/data-provider/ireaders/xml/reader_xml_speciesparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_speciesparameters_xml_t >  ireader_landscape_speciesparameters_xml_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_speciesparameters_xml_t >  ireader_speciesparameters_xml_factory;
#endif

#ifdef  LDNDC_IO_RESOURCES
#  include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_siteparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_siteparameters_lrsrc_t >  ireader_landscape_siteparameters_lrsrc_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_siteparameters_lrsrc_t >  ireader_siteparameters_lrsrc_factory;
#  include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_soilparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_soilparameters_lrsrc_t >  ireader_landscape_soilparameters_lrsrc_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_soilparameters_lrsrc_t >  ireader_soilparameters_lrsrc_factory;
#  include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_speciesparameters.h"
static ldndc::ioproc_factory_t< ldndc::iproc_landscape_reader_speciesparameters_lrsrc_t >  ireader_landscape_speciesparameters_lrsrc_factory;
static ldndc::ioproc_factory_t< ldndc::iproc_reader_speciesparameters_lrsrc_t >  ireader_speciesparameters_lrsrc_factory;
#endif

struct  ireader_factory_item_t
{
    ldndc::ioproc_factory_base_t const *  factory;
    char  kind; /* 'L':=landscape, 'C':=cell */
    char const *  role;
    char const *  format;
};

static ireader_factory_item_t const  __cbm_ireader_factories[] =
{
#ifdef  LDNDC_IO_SQLITE3
    { &ireader_anyclass_sqlite3_factory, 'C', "any", "sqlite3" },
    { &ireader_landscape_anyclass_sqlite3_factory, 'L', "any", "sqlite3" },
#endif
#ifdef  LDNDC_IO_TXT
    { &ireader_airchemistry_txt_factory, 'C', "airchemistry", "txt" },
    { &ireader_landscape_airchemistry_txt_factory, 'L', "airchemistry", "txt" },
    { &ireader_climate_txt_factory, 'C', "climate", "txt" },
    { &ireader_landscape_climate_txt_factory, 'L', "climate", "txt" },
    { &ireader_event_txt_factory, 'C', "event", "txt" },
    { &ireader_landscape_event_txt_factory, 'L', "event", "txt" },
    { &ireader_geometry_txt_factory, 'C', "geometry", "txt" },
    { &ireader_landscape_geometry_txt_factory, 'L', "geometry", "txt" },
    { &ireader_groundwater_txt_factory, 'C', "groundwater", "txt" },
    { &ireader_landscape_groundwater_txt_factory, 'L', "groundwater", "txt" },
    { &ireader_siteparameters_txt_factory, 'C', "siteparameters", "txt" },
    { &ireader_landscape_siteparameters_txt_factory, 'L', "siteparameters", "txt" },
#endif
#ifdef  LDNDC_IO_XML
    { &ireader_checkpoint_xml_factory, 'C', "checkpoint", "xml" },
    { &ireader_landscape_checkpoint_xml_factory, 'L', "checkpoint", "xml" },
    { &ireader_event_xml_factory, 'C', "event", "xml" },
    { &ireader_landscape_event_xml_factory, 'L', "event", "xml" },
    { &ireader_setup_xml_factory, 'C', "setup", "xml" },
    { &ireader_landscape_setup_xml_factory, 'L', "setup", "xml" },
    { &ireader_site_xml_factory, 'C', "site", "xml" },
    { &ireader_landscape_site_xml_factory, 'L', "site", "xml" },
    { &ireader_siteparameters_xml_factory, 'C', "siteparameters", "xml" },
    { &ireader_landscape_siteparameters_xml_factory, 'L', "siteparameters", "xml" },
    { &ireader_soilparameters_xml_factory, 'C', "soilparameters", "xml" },
    { &ireader_landscape_soilparameters_xml_factory, 'L', "soilparameters", "xml" },
    { &ireader_speciesparameters_xml_factory, 'C', "speciesparameters", "xml" },
    { &ireader_landscape_speciesparameters_xml_factory, 'L', "speciesparameters", "xml" },
#endif
#ifdef  LDNDC_IO_RESOURCES
    { &ireader_siteparameters_lrsrc_factory, 'C', "siteparameters", "lrsrc" },
    { &ireader_landscape_siteparameters_lrsrc_factory, 'L', "siteparameters", "lrsrc" },
    { &ireader_soilparameters_lrsrc_factory, 'C', "soilparameters", "lrsrc" },
    { &ireader_landscape_soilparameters_lrsrc_factory, 'L', "soilparameters", "lrsrc" },
    { &ireader_speciesparameters_lrsrc_factory, 'C', "speciesparameters", "lrsrc" },
    { &ireader_landscape_speciesparameters_lrsrc_factory, 'L', "speciesparameters", "lrsrc" },
#endif
    { NULL, '-', NULL, NULL } /* sentinel */
};

ldndc::ioproc_factory_base_t const *  ldndc::find_ireader_factory( char  _kind,
            char const * _format, char const *  _role)
{
    CBM_Assert( _format);
    if (( _kind!='C' && _kind!='L') || !_format || !_role )
        { return  NULL; }

    int  r = 0;
    while ( __cbm_ireader_factories[r].factory)
    {
        if (( __cbm_ireader_factories[r].kind == _kind)
            && cbm::is_equal( __cbm_ireader_factories[r].role, _role)
            &&  cbm::is_equal( __cbm_ireader_factories[r].format, _format))
            { return  __cbm_ireader_factories[r].factory; }
        ++r;
    }
    return  NULL;
}

