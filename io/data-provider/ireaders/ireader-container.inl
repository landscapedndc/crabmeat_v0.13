/*!
 * @brief
 *    input reader container (implementation)
 *
 * @author
 *    steffen klatt (created on: jan 31, 2014),
 *    edwin haas
 */

#include  "io/ifactory.h"
#include  "log/cbm_baselog.h"

template< typename  _lireader_t >
ldndc::iproc_ireader_container_t< _lireader_t >::iproc_ireader_container_t()
{
    cbm::omp::init_lock( &this->lock_);
}
template< typename  _lireader_t >
ldndc::iproc_ireader_container_t< _lireader_t >::~iproc_ireader_container_t< _lireader_t >()
{
    iproc_factories_t  ifactory;

    node_ireader_container_t::iterator  ri = this->node_ireaders_.begin();
    for ( ;  ri != this->node_ireaders_.end();  ++ri)
    {
        if ( ri->second)
        {
            ri->second->delete_instance();
        }
    }
    this->node_infos_.clear();

    cbm::omp::destroy_lock( &this->lock_);
}


template < typename  _lireader_t >
void
ldndc::iproc_ireader_container_t< _lireader_t >::lock(
                char const *  _name)
{
    cbm::omp::set_lock( &this->lock_, ( _name) ? _name : "ireader-container");
}
template < typename  _lireader_t >
void
ldndc::iproc_ireader_container_t< _lireader_t >::unlock(
                char const *  _name)
{
    cbm::omp::unset_lock( &this->lock_, ( _name) ? _name : "ireader-container");
}


template< typename  _lireader_t >
size_t
ldndc::iproc_ireader_container_t< _lireader_t >::size()
const
{
    return  this->block_size() + this->reader_size();
}
template< typename  _lireader_t >
size_t
ldndc::iproc_ireader_container_t< _lireader_t >::block_size()
const
{
    return  this->node_infos_.size();
}
template< typename  _lireader_t >
size_t
ldndc::iproc_ireader_container_t< _lireader_t >::reader_size()
const
{
    return  this->node_ireaders_.size();
}
template< typename  _lireader_t >
bool
ldndc::iproc_ireader_container_t< _lireader_t >::check_input_exists(
        lid_t const &  _block_id)
const
{
    bool const  rdr_exists = ( this->node_infos_.find( _block_id) != this->node_infos_.end()) || 
        ( this->node_ireaders_.find( _block_id) != this->node_ireaders_.end());

    return  rdr_exists;
}

template< typename  _lireader_t >
int
ldndc::iproc_ireader_container_t< _lireader_t >::get_available_blocks(
        lid_t  _list[], size_t  _max_size)
const
{
    size_t  c = 0;
    typename node_info_container_t::const_iterator  ni = this->node_infos_.begin();
    for ( ;  ni != this->node_infos_.end();  ++ni, ++c)
    {
        if ( c == _max_size)
        {
            return  static_cast< int >( _max_size);
        }
        _list[c] = ni->second.id;
    }

    typename node_ireader_container_t::const_iterator  rdr = this->node_ireaders_.begin();
    for ( ;  rdr != this->node_ireaders_.end();  ++rdr)
    {
        if ( !rdr->second)
        {
            continue;
        }

        if ( c == _max_size)
        {
            return  static_cast< int >( _max_size);
        }
        _list[c] = rdr->first;

        ++c;
    }

    return  static_cast< int >( c);
}


template< typename  _lireader_t >
ldndc::iproc_provider_t *
ldndc::iproc_ireader_container_t< _lireader_t >::acquire_ireader(
        lid_t const &  _id,
        char const *  _type, char const *  _io_format)
{
    this->lock( "ireader-container: acquire reader");

    /* find reader with given ID */
    if ( this->node_ireaders_.find( _id) != this->node_ireaders_.end())
    {
        return  this->unlock_and_return_< iproc_provider_t * >( this->node_ireaders_[_id], "ireader-container: acquire reader");
    }
    typename node_info_container_t::iterator  ni = this->node_infos_.find( _id);
    if ( this->node_infos_.empty() || ( ni == this->node_infos_.end()))
    {
        CBM_LogError( "iproc_ireader_container_t< _lireader_t >::acquire_ireader(): no such node in input");
        return  this->unlock_and_return_< iproc_provider_t * >( NULL, "ireader-container: acquire reader");
    }

    iproc_factories_t  ifactory;
    ireader_t *  this_reader = dynamic_cast< ireader_t * >( ifactory.construct_ireader( _type, _io_format));
    if ( !this_reader)
    {
        CBM_LogError( "iproc_ireader_container_t< _lireader_t >::acquire_ireader(): nomem");
        return  this->unlock_and_return_< iproc_provider_t * >( NULL, "ireader-container: acquire reader");
    }
    this_reader->set_object_id( ni->second.id);
    this_reader->set_handle( ni->second.handle);

    /* add to reader list */
    node_ireader_container_insert_return_t  ins_status = this->node_ireaders_.insert( node_ireader_container_t::value_type( _id, this_reader));
    if ( !ins_status.second)
    {
        CBM_LogError( "iproc_ireader_container_t< _lireader_t >::acquire_ireader(): failed to insert reader into reader container");
        return  this->unlock_and_return_< iproc_provider_t * >( NULL, "ireader-container: acquire reader");
    }
    /* remove from node info list */
    typename node_info_container_t::size_type  erase_count = this->node_infos_.erase( _id);
    if ( erase_count == 0)
    {
        CBM_LogWarn( "iproc_ireader_container_t< _lireader_t >::acquire_ireader(): reader node info could not be erased from container. impossible situation.");
    }

    return  this->unlock_and_return_< iproc_provider_t * >( this_reader, "ireader-container: acquire reader");
}
template< typename  _lireader_t >
void
ldndc::iproc_ireader_container_t< _lireader_t >::release_ireader(
        lid_t const &  /*_block_id*/)
{
    this->lock( "ireader-container: release reader");

    CBM_LogWarn( "iproc_ireader_container_t< _lireader_t >::release_ireader(): not implemented");
// sk:TODO    iproc_provider_t *  rdr = this->acquire_ireader( _block_id);
// sk:TODO    if ( rdr)
// sk:TODO    {
// sk:TODO        ifactory.destroy_ireader( rdr);
// sk:TODO    }

    this->unlock( "ireader-container: release reader");
}

template< typename  _lireader_t >
lerr_t
ldndc::iproc_ireader_container_t< _lireader_t >::add_to_processing_stack(
        iproc_provider_t **  _rdr_ptr,
        iproc_provider_t *  _iprocs)
{
    lerr_t  rc_stack = LDNDC_ERR_FAIL;
    this->lock( "ireader-container: add to proc stack");

    /* verify existence */
    typename node_ireader_container_t::iterator  rdr_it = this->node_ireaders_.find( (*_rdr_ptr)->object_id());
    if ( rdr_it == this->node_ireaders_.end())
    {
        this->unlock( "ireader-container: add to proc stack");
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }

    iproc_provider_t const *  rdr = rdr_it->second;
    if ( rdr && ( rdr == *_rdr_ptr) && ( rdr != _iprocs))
    {
        this->node_ireaders_[(*_rdr_ptr)->object_id()] = _iprocs;
        *_rdr_ptr = _iprocs;
        rc_stack = LDNDC_ERR_OK;
    }

    this->unlock( "ireader-container: add to proc stack");
    return  rc_stack;
}


template< typename  _lireader_t >
int
ldndc::iproc_ireader_container_t< _lireader_t >::insert_node_info_(
        node_info_t &  _ni)
{
    node_info_container_insert_return_t  ins_status = this->node_infos_.insert( typename node_info_container_t::value_type( _ni.id, _ni));
    if ( !ins_status.second)
    {
        CBM_LogError( "iproc_ireader_container_t< _lireader_t >::insert_node_info_(): ",
                "failed to insert node information into map  [identifier=","?"/*this->fhandler->get_source_info()->identifier*/,"]");
        return  -1;
    }
    return  0;
}
template< typename  _lireader_t >
int
ldndc::iproc_ireader_container_t< _lireader_t >::configure_node(
        node_info_t &  _ni)
{
    if ( this->node_infos_.find( _ni.id) != this->node_infos_.end())
    {
        CBM_LogError( "iproc_ireader_container_t< _lireader_t >::configure_node_(): ",
                "non-unique ID in input source  [identifier=","?"/*this->fhandler->get_source_info()->identifier*/,"]");
        return  -1;
    }
    return  this->insert_node_info_( _ni);
}
template< typename  _lireader_t >
int
ldndc::iproc_ireader_container_t< _lireader_t >::reconfigure_node(
        node_info_t &  _ni)
{
    int  rc_reconf = 0;

    typename node_info_container_t::iterator  b_inf = this->node_infos_.find( _ni.id);
    if ( b_inf == this->node_infos_.end())
    {
        typename node_ireader_container_t::iterator  b_rdr = this->node_ireaders_.find( _ni.id);
        if ( b_rdr == this->node_ireaders_.end())
        {
            /* insert as new block reader */
            int const  c_i = this->insert_node_info_( _ni);
            if ( c_i != 0)
            {
                rc_reconf = -1;
            }
        }
        else
        {
            if ( b_rdr->second->to_reader())
            {
                static_cast< ireader_t * >( b_rdr->second->to_reader())->set_handle( _ni.handle);
            }
        }
    }
    else
    {
        b_inf->second.handle = _ni.handle;
    }

    return  rc_reconf;
}

template< typename  _lireader_t >
void
ldndc::iproc_ireader_container_t< _lireader_t >::invalidate_node_handles()
{
    /* blocks of old source no longer relevant */
    this->node_infos_.clear();
    /* readers handles are invalidated */
    for ( typename node_ireader_container_t::iterator  b_rdr = this->node_ireaders_.begin();  b_rdr != this->node_ireaders_.end();  ++b_rdr)
    {
        if ( b_rdr->second->to_reader())
        {
            static_cast< ireader_t * >( b_rdr->second->to_reader())->set_handle( node_handle_t());
        }
    }
}

