
lerr_t
__reader__::read(
        element_type  _rec[], size_t  _rec_cnt, size_t  _rec_size,
        char const *  /*_rec_item*/[], size_t  /*_rec_item_cnt*/,
        int  _offset, int  _stride,
                int *  _r, int * _s)
{
#ifdef  CRABMEAT_SANITY_CHECKS
    if (( _stride < 1)  ||  ( _offset < 0))
    {
        if ( _r) *_r = 0;
        if ( _s) *_s = 0; 

        CBM_LogError( STRINGIFY(__reader__),"::read(): ",
                "invalid stride or offset  [stride=", _stride, ",offset=", _offset, "]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( !this->tm)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
#endif /* CRABMEAT_SANITY_CHECKS */

    this->tm->offset = 0; /* _offset */
    this->tm->stride = 1; /* _stride */

    this->tm->record_stride = _rec_size;

    /* read records from stream */
    int  r = this->miner.read_table( this->tm, static_cast< element_type * >( _rec), _rec_cnt);
    if ( _r) *_r = this->tm->records_read;
    if ( _s) *_s = this->tm->records_seen;

    if ( r < 0)
    {
        CBM_LogError( STRINGIFY(__reader__),"::read():",
            " problems reading from ",STRINGIFY(__class__)," input stream",
              "  [stream=",this->input_class_type(),",id=",this->object_id(),"]");
    }


        return  ( r < 0) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

