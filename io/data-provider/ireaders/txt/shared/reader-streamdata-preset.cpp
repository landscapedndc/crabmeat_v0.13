
lerr_t
__reader__::preset(
        char const **  _rec_item, size_t  _rec_item_cnt,
               config_type *  _config)
{
    crabmeat_assert( _config);

    /* get current stream pointer */
    core_iobject_handle_t::ifstream *  data_stream( this->get_handle()->get_stream());
    if ( !data_stream)
    {
        CBM_LogWarn( "got empty stream?!  [class=",this->input_class_type(),"]");
        return  LDNDC_ERR_INPUT_EXHAUSTED;
    }

    /* if header was not processed, do it now (this allocates the map) */
    if ( !this->tm)
    {
        fhandler_txt_miner_t::table_miner_handle_t  m_hdl = this->miner.new_table_miner( &this->tm, "data", _rec_item, _rec_item_cnt);
        if ( !this->tm)
        {
            return  LDNDC_ERR_OBJECT_CREATE_FAILED;
        }
        this->miner.rewind_table( m_hdl);
// sk:dbg        CBM_LogDebug( "hdl=",m_hdl, "  tm-offs=",this->tm->p_top, "  tm-cur=",this->tm->p_cur, "  tm-end=",this->tm->p_bot);

        this->tm->offset = 0;
        this->tm->stride = ( _config->stride < 1) ? 1 : _config->stride;

        if ( _config->offset > 0)
        {
// sk:dbg            CBM_LogDebug( "skipping ",_config->offset," records  [class=",this->input_class_type(),"]");
            int  r = this->miner.read_table< element_type >( this->tm, NULL, _config->offset);
            if ( r < 0)
            {
                return  LDNDC_ERR_FAIL;
            }
// sk:dbg            CBM_LogDebug( "r=",r,"  pos=",this->tm->p_top, "  newpos=",this->tm->p_cur);
            this->tm->p_top = this->tm->p_cur;
        }
        else
        {
            /* negative offsets not handled by readers */
        }
    }

    return  LDNDC_ERR_OK;
}

