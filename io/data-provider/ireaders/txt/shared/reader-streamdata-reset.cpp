
lerr_t
__reader__::reset()
{
    int const  rc_reset = this->miner.reset();
    return  ( rc_reset) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

