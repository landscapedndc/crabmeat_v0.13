/*!
 * @brief
 *      event xml reader "mortality"
 *
 * @author
 *      steffen klatt (created on: apr 29, 2013)
 */

#ifdef  __this_event_name
#  undef  __this_event_name
#endif
#define  __this_event_name  mortality
namespace ldndc {
__EV_READER_HANDLER_SIGNATURE(__this_event_name)
{
    EV_ATTRS_READER_HEADER(__this_event_name);

    EV_ATTR_U( name, "name");
    if ( cbm::is_empty( attrs->name) || cbm::is_invalid( attrs->name))
    {
        CBM_LogError( "seeing invalid species name in",STRINGIFY(__this_event_name),"event  [",attrs->name,"]");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
}

