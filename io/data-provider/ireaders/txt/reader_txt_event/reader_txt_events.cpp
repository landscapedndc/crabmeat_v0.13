/* vim: set ft=cpp noexpandtab tabstop=8 shiftwidth=8 nowrap smartindent : */
/*!
 * @brief
 *      pull in all event txt readers
 *
 * @author
 *      steffen klatt (created on: ?)
 */



/* add new (txt) event attribute reader handlers header files here */

#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_checkpoint.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_cut.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_defoliate.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_fertilize.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_flood.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_fire.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_graze.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_harvest.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_irrigate.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_luc.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_manure.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_plant.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_regrow.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_reparameterizespecies.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_thin.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_throw.cpp"
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_event_till.cpp"


/* adding event attribute reader handlers here
 *
 * doing it like this ensures correct array position even
 * if enumeration values change (enums are cmake generated)
 */
#define  __EV_READER_HANDLERS_SET            \
    __EV_READER_HANDLER_ADD(checkpoint);               \
    __EV_READER_HANDLER_ADD(cut);               \
    __EV_READER_HANDLER_ADD(defoliate);        \
    __EV_READER_HANDLER_ADD(fertilize);         \
    __EV_READER_HANDLER_ADD(fire);            \
    __EV_READER_HANDLER_ADD(flood);             \
    __EV_READER_HANDLER_ADD(graze);             \
    __EV_READER_HANDLER_ADD(harvest);           \
    __EV_READER_HANDLER_ADD(irrigate);          \
    __EV_READER_HANDLER_ADD(luc);               \
    __EV_READER_HANDLER_ADD(manure);            \
    __EV_READER_HANDLER_ADD(plant);             \
    __EV_READER_HANDLER_ADD(regrow);        \
    __EV_READER_HANDLER_ADD(reparameterizespecies);    \
    __EV_READER_HANDLER_ADD(thin);            \
    __EV_READER_HANDLER_ADD(throw);            \
    __EV_READER_HANDLER_ADD(till);

