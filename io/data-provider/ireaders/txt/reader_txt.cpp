/*!
 * @brief
 *    concrete text file readers implementation
 *
 *    format (things in brackets ([,]) are optional,
 *    attributes must be quoted if they contain spaces,
 *    lines starting with # are treated as comments):
 *    [
 *    [
 *    %global
 *            time = <time>  # example: time = "2000-01-01 -> 2001-21-31"
 *    ]
 *    %<input class name>
 *        id = <ID>
 *    [
 *    [
 *    %attributes
 *            longitude = <longitude>
 *            latitude = <latitude>
 *            elevation = <elevation>
 *    ]
 *    [
 *    %units
 *            <<input class type>-record-item>  <<input class type>-record-item-unit>
 *            ...
 *    ]
 *    %data]] <header names>
 *    <DATA>
 *
 *    %<tag>
 *        <key> = <value>
 *    %<table tag>
 *    <header declaration>
 *    <DATA>
 *    %<table tag>
 *    <header declaration>
 *    <DATA>
 *    %... a maximum of TABLE_MINER_MAX tables can appear in a single block
 *
 *    [more blocks can follow]
 *
 *
 * @note
 *    the existence of the "%data" tag determines input mode (single vs multi)
 *
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011),
 */

#include  "io/data-provider/ireaders/txt/reader_txt.h"

#include  "string/lstring-compare.h"

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"

namespace ldndc {
/***  INPUT LANDSCAPE READER TXT BASE  ***/
iproc_landscape_reader_txt_base_t::iproc_landscape_reader_txt_base_t()
        : iproc_landscape_reader_base_t(),
          iproc_handle_txt_types_t()
{
}
iproc_landscape_reader_txt_base_t::~iproc_landscape_reader_txt_base_t()
{
}

/***  INPUT LANDSCAPE READER TXT  ***/
iproc_landscape_reader_txt_t::cuefile_t::cuefile_t()
{
    this->filename = invalid_string;
    this->exists = 0;
    this->tagpos = txt_invalid_pos;
}
iproc_landscape_reader_txt_t::iproc_landscape_reader_txt_t()
        : iproc_landscape_reader_txt_base_t(),
          fhandler( NULL),
          status_( LDNDC_ERR_OK),
          imode_( iproc_landscape_reader_interface_t::IMODE_NONE)
{
}


iproc_landscape_reader_txt_t::~iproc_landscape_reader_txt_t()
{
    this->cuefile_close_();
}


int
iproc_landscape_reader_txt_t::scan_stream(
        iobject_handler_base_t *  _fhandler)
{
    bool const  is_rescan = this->fhandler != NULL;

    if ( !is_rescan)
    {
        crabmeat_assert( _fhandler);
        this->fhandler = dynamic_cast< iobject_handler_txt_t * >( _fhandler);
    }

// sk:todo    if ( !this->fhandler->is_open() && this->fhandler->info()->is_mutable())
// sk:todo    {
// sk:todo        return  0;
// sk:todo    }

    /* scan stream and store handles */
    this->shandle.ifs = this->fhandler->get_iobject_handle();

    lerr_t  rc_stream_info = this->initialize_stream_handle_( &this->shandle);
    if ( rc_stream_info)
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::scan_stream(): ",
            "failed to determine stream properties  [identifier=",_fhandler->info()->identifier,"]");
        return  -1;
    }

    this->cuefile_open_();

    int const  c = this->scan_stream_( is_rescan);

    this->cuefile_close_();

    return  c;
}
int
iproc_landscape_reader_txt_t::rescan_stream()
{
    if ( !this->fhandler)
    {
        return  -1;
    }
    int const  rc_reload = this->fhandler->reload();
    if ( rc_reload)
    {
        return  -1;
    }

    return  this->scan_stream( this->fhandler);
}


size_t
iproc_landscape_reader_txt_t::number_of_blocks()
const
{
    return  this->ireaders_.size();
}
bool
iproc_landscape_reader_txt_t::check_input_exists(
        lid_t const &  _block_id)
const
{
    return  this->ireaders_.check_input_exists( _block_id);
}

int
iproc_landscape_reader_txt_t::get_available_blocks(
        lid_t  _list[], size_t  _max_size)
const
{
    if ( !this->shandle.ifs)
    {
        return  -1;
    }
    return  this->ireaders_.get_available_blocks( _list, _max_size);
}


iproc_provider_t *
iproc_landscape_reader_txt_t::acquire_ireader(
        lid_t const &  _id)
{
    return  static_cast< iproc_provider_t * >( this->ireaders_.acquire_ireader( _id, this->input_class_type(), this->io_format_type()));
}
void
iproc_landscape_reader_txt_t::release_ireader(
        lid_t const &  _id)
{
    this->ireaders_.release_ireader( _id);
}

lerr_t
iproc_landscape_reader_txt_t::add_to_processing_stack(
        iproc_provider_t **  _rdr_ptr,
        iproc_provider_t *  _iprocs)
{
// sk:TODO  provide for synthesizer only input
    if ( !_rdr_ptr)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    return  this->ireaders_.add_to_processing_stack( _rdr_ptr, _iprocs);
}





lerr_t
iproc_landscape_reader_txt_t::detect_stream_size_(
        ioproc_handle_txt_t *  _hdl)
{
// sk:off    /* to test if the STL stream position data type is large enough
// sk:off     * to handle the input stream, we jump to the maximum possible
// sk:off     * position (MAX_POS) and check if its still a valid position.
// sk:off     * if it is, it might well be the last (that does not set eof,
// sk:off     * right?) so we advance one character further to make sure. in
// sk:off     * case it is too large, we assume it is safest to abort.
// sk:off     *
// sk:off     * credit to K.H. Rahn
// sk:off     */
// sk:off        _iobj_handle->seekg( core_iobject_handle_t::MAX_POS, std::ios::beg);
// sk:off    CBM_LogVerbose( "fpos=",_iobj_handle->tellg(), "  state=",_iobj_handle->rdstate(), "  open=",_iobj_handle->is_open());
// sk:off    if ( !_iobj_handle->fail())
// sk:off    {
// sk:off        /* check if file has exactly size MAX_POS */
// sk:off        _iobj_handle->seekg( 1, std::ios::cur);
// sk:off        CBM_LogVerbose( "fpos=",_iobj_handle->tellg());
// sk:off    }
// sk:off    if ( !_iobj_handle->fail())
// sk:off    {
// sk:off        CBM_LogError( "input stream too large. stream position integral type not large enough?!  [max=",core_iobject_handle_t::MAX_POS,"]");
// sk:off        this->status_ = LDNDC_ERR_FAIL;
// sk:off        return  LDNDC_ERR_REQUEST_MISMATCH;
// sk:off    }
// sk:off
// sk:off    /* unset eof flag */
// sk:off    _iobj_handle->clear( _iobj_handle->rdstate() ^ std::ios::failbit);

    /* did something else go wrong ... ? */
    if ( !_hdl->ifs->good())
    {
        this->status_ = LDNDC_ERR_FAIL;
        CBM_LogError( "iproc_landscape_reader_txt_t::detect_stream_size_(): error while trying to get stream dimensions");
        return  LDNDC_ERR_FAIL;
    }

    /* set total stream size */
    _hdl->ifs->seekg( 0, std::ios::end);
    _hdl->pe = _hdl->ifs->tellg();

    if ( _hdl->pe < 3) /* have at least "<column name>\n<value>" */
    {
        this->status_ = LDNDC_ERR_FAIL;
        CBM_LogError( "iproc_landscape_reader_txt_t::detect_stream_size_(): empty or almost empty stream.");
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }

    /* reset stream position to beginning */
    _hdl->ifs->seekg( 0, std::ios_base::beg);
    return  LDNDC_ERR_OK;
}


lerr_t
iproc_landscape_reader_txt_t::initialize_stream_handle_(
        ioproc_handle_txt_t *  _hdl)
{
    crabmeat_assert( _hdl);

    _hdl->pb = 0;
    _hdl->pd = 0;
    /* set total stream size */
    lerr_t  rc = this->detect_stream_size_( _hdl);
    if ( rc != LDNDC_ERR_OK)
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::initialize_stream_handle_(): failed to detect stream dimensions");

        this->status_ = rc;
        return  rc;
    }

    lerr_t  rc_imode = this->detect_input_mode_( _hdl);
    RETURN_IF_NOT_OK(rc_imode);
    if ( this->imode_ == iproc_landscape_reader_interface_t::IMODE_SINGLE)
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_global = this->detect_global_block_range_( _hdl);
    RETURN_IF_NOT_OK(rc_global);

    return  LDNDC_ERR_OK;
}

lerr_t
iproc_landscape_reader_txt_t::detect_input_mode_(
        ioproc_handle_txt_t *  _hdl)
{
    fhandler_txt_miner_t  miner( _hdl);

    txt_streampos_t const  p_core = miner.find_tag( ldndc_txt_data, ldndc_txt_data);
    if ( p_core == txt_invalid_pos)
    {
        this->imode_ = iproc_landscape_reader_interface_t::IMODE_SINGLE;
    }
    else
    {
        this->imode_ = iproc_landscape_reader_interface_t::IMODE_MULTI;
    }
    CBM_LogVerbose( "iproc_landscape_reader_txt_t::detect_input_mode_(): assuming i-mode \"", iproc_landscape_reader_interface_t::IMODE_NAMES[this->imode_], "\"");

    return  LDNDC_ERR_OK;
}

lerr_t
iproc_landscape_reader_txt_t::detect_global_block_range_(
        ioproc_handle_txt_t *  _hdl)
{
    crabmeat_assert( this->input_stream_mode() == iproc_landscape_reader_interface_t::IMODE_MULTI);

    fhandler_txt_miner_t  miner( _hdl);

    /* search for global block begin */
    txt_streampos_t  p_global = miner.find_tag( ldndc_txt_global, ldndc_txt_block);
    if ( p_global != txt_invalid_pos)
    {
        _hdl->pb = p_global;
    }
    /* search for first core block, i.e. global block end */
    miner.handle.pb = _hdl->pb;
    txt_streampos_t  p_block = miner.find_tag( ldndc_txt_block, ldndc_txt_block);
    if ( p_block != txt_invalid_pos)
    {
        _hdl->pd = p_block;
    }
    else
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::detect_global_block_range_(): expected to find block tag: \"%", ldndc_txt_block, "\"");
        this->status_ = LDNDC_ERR_FAIL;
        return  LDNDC_ERR_FAIL;
    }

    /* if global and data block exist, check if we have cue */
    if (( p_global != txt_invalid_pos) && ( p_block != txt_invalid_pos))
    {
        this->cf.tagpos = miner.find_tag( ldndc_txt_cueline, ldndc_txt_block);

        GLOBAL_ATTRIBUTE_VALUE( this->cf.filename, ldndc_txt_cuefile, invalid_string);
        cbm::format_expand( &this->cf.filename);
    }
    else
    {
        /* no global block */
        crabmeat_assert( _hdl->pb == _hdl->pd);
    }

    return  LDNDC_ERR_OK;
}

int
iproc_landscape_reader_txt_t::block_find_(
        ioproc_handle_txt_t *  _hdl)
{
    if ( this->input_stream_mode() == iproc_landscape_reader_interface_t::IMODE_SINGLE)
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::block_find_(): do not call in single mode");
        return  -1;
    }

    /** use cue file **/
    if ( this->cf.exists)
    {
        return  this->set_handle_from_cuefile_( _hdl);
    }

    /* multi mode */
    int  rc = this->block_find_multi_( _hdl);
    return  rc;
}

int
iproc_landscape_reader_txt_t::block_find_multi_(
        ioproc_handle_txt_t *  _hdl)
{
    crabmeat_assert( _hdl);
    fhandler_txt_miner_t  miner( _hdl);

    /* assume last block's end is this one's start */
    if ( _hdl->pb == txt_invalid_pos || _hdl->pb > this->stream_size())
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::block_find_multi_(): invalid block offset");
        return  -1;
    }
    if ( _hdl->pb == this->stream_size())
    {
        return  1;
    }

    /* find %data tag */
    txt_streampos_t  pd = miner.find_tag( ldndc_txt_data, ldndc_txt_block, _hdl->pb+1);
    if ( pd == txt_invalid_pos || pd < _hdl->pb || pd >= this->stream_size())
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::block_find_multi_(): block missing %" ldndc_txt_data " tag  [class=",ldndc_txt_block,",dataposition=",pd,",offset=",_hdl->pb,"]");
        return  -1;
    }
    _hdl->pd = pd;

    /* find next block's start and use as this one's end */
    txt_streampos_t  pe = miner.find_tag( ldndc_txt_block, NULL, pd);
    pe = ( pe == txt_invalid_pos) ? this->stream_size() : pe;
    if ( pe < pd || pe > this->stream_size())
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::block_find_multi_(): block missing %" ldndc_txt_data " tag  [class=",ldndc_txt_block,"]");
        return  -1;
    }
    _hdl->pe = pe;

    this->add_handle_to_cuefile_( _hdl);

    return  0;
}
int
iproc_landscape_reader_txt_t::block_find_single_(
        ioproc_handle_txt_t *  _hdl)
{
    fhandler_txt_miner_helper_t  miner( _hdl);

    txt_streampos_t  p = _hdl->pb;
    miner.eat_blanks( &p);

    _hdl->pd = p;
    _hdl->pb = p;

    return  0;
}


int
iproc_landscape_reader_txt_t::block_find_first_(
        ioproc_handle_txt_t *  _hdl)
{
    crabmeat_assert( _hdl);

    _hdl->ifs = this->shandle.ifs;
    _hdl->pb = this->stream_dataoffset();
    _hdl->pe = this->stream_size();

    if ( this->input_stream_mode() == iproc_landscape_reader_interface_t::IMODE_SINGLE)
    {
        /* single mode */
        return  this->block_find_single_( _hdl);
    }

    return  this->block_find_( _hdl);
}

int
iproc_landscape_reader_txt_t::block_find_next_(
        ioproc_handle_txt_t *  _hdl)
{
    crabmeat_assert( _hdl);

    _hdl->pb = _hdl->pe;
    _hdl->pe = this->stream_size();

    if ( this->input_stream_mode() == iproc_landscape_reader_interface_t::IMODE_SINGLE)
    {
        return  1;
    }

    if (( !_hdl->ifs) || ( _hdl->pb == this->stream_size()))
    {
        return  1;
    }

    /* find next block following end of last one */
    _hdl->pe += 1;
    return  this->block_find_( _hdl);
}

void
iproc_landscape_reader_txt_t::cuefile_open_()
{
    /* double call guard */
    if ( this->cf.file.is_open())
    {
        return;
    }
    /* no cue file name given. ignore */
    if ( cbm::is_invalid( this->cf.filename) || cbm::is_empty( this->cf.filename))
    {
        return;
    }
    if ( this->cf.tagpos == ioproc_handle_txt_t::INVALID_POS)
    {
        CBM_LogWarn( "iproc_landscape_reader_txt_t::cuefile_open(): ",
                "found cue filename but no cueline-tag  [cueline-tag=",TXT_HEADER_CUELINE_TAG,"]");
        return;
    }
    /* cue file name given and file does exist: read from it */
    this->cf.file.open( this->cf.filename.c_str(), std::fstream::in|std::fstream::binary);
    if ( this->cf.file.is_open())
    {
        CBM_LogInfo( "iproc_landscape_reader_txt_t::cuefile_open(): ",
                "found cue file. using '",this->cf.filename,"'");
        this->cf.exists = 1;
        return;
    }
    /* cue file name given but file does not exist: fill it */
    this->cf.file.open( this->cf.filename.c_str(), std::fstream::out|std::fstream::binary);
    if ( this->cf.file.is_open())
    {
        this->cf.exists = 0;
        CBM_LogInfo( "iproc_landscape_reader_txt_t::cuefile_open(): ",
                "cue file requested but does not exist. creating it  [file=",this->cf.filename,"]");
    }
    else
    {
        CBM_LogWarn( "iproc_landscape_reader_txt_t::cuefile_open(): ",
                "cue file given could neither be opened for reading nor for writing, silently skipping  [file=",this->cf.filename,"]");
    }
}
void
iproc_landscape_reader_txt_t::cuefile_close_()
{
    /* if using cue file, check if we hit eof */
    if ( this->cf.file.is_open()  && ( ! this->cf.file.good()))
    {
        this->cf.file.close();
    }
}

int
iproc_landscape_reader_txt_t::set_handle_from_cuefile_(
        ioproc_handle_txt_t *  _hdl)
{
    /* cue file open and has data (well, we hope...) ? */
    crabmeat_assert( this->cf.exists);

    size_t  p_b, p_d, p_e;
    this->cf.file >> p_b >> p_d >> p_e;
    if ( !this->cf.file.good())
    {
        return  1;
    }

    /* small insanity check that however cannot catch
     * mismatch in data stream and cue file */
    if ( this->shandle.ifs->seekg( p_e, std::ios_base::beg)  &&  ( this->shandle.ifs->good()))
    {
        _hdl->ifs = this->shandle.ifs;
        _hdl->pd = p_d + this->cf.tagpos;
        _hdl->pb = p_b + this->cf.tagpos;
        _hdl->pe = p_e + this->cf.tagpos;

        return  0;
    }

    return  -1;
}
void
iproc_landscape_reader_txt_t::add_handle_to_cuefile_(
        ioproc_handle_txt_t const *  _hdl)
{
    /* cue file is open and wants to be filled */
    if ( this->cf.file.is_open())
    {
        this->cf.file
                    << _hdl->pb - this->cf.tagpos
            << '\t' << _hdl->pd - this->cf.tagpos
            << '\t' << _hdl->pe - this->cf.tagpos
            << '\n';
        this->cf.file.flush();
    }
}

iproc_landscape_reader_interface_t::input_stream_mode_e
iproc_landscape_reader_txt_t::input_stream_mode()
const
{
    return  this->imode_;
}



int
iproc_landscape_reader_txt_t::collective_update_commence(
        cbm::sclock_t const *)
{
    crabmeat_assert(this->fhandler);
    int  rc_reopen = 0;
    if ( this->fhandler->get_source_info()->is_mutable())
    {
        rc_reopen = this->rescan_stream();
    }
    return  rc_reopen;
}
int
iproc_landscape_reader_txt_t::collective_update_complete(
        cbm::sclock_t const *)
{
    crabmeat_assert(this->fhandler);
    if ( this->fhandler->get_source_info()->is_mutable())
    {
        this->fhandler->close();
    }
    return  0;
}


int
iproc_landscape_reader_txt_t::scan_stream_(
        bool  _is_rescan)
{
    CBM_LogDebug( "iproc_landscape_reader_txt_t::scan_stream_(): ",(_is_rescan?"re":""),"scan stream");
    this->ireaders_.invalidate_node_handles();

    int  c = 0;

    ioproc_handle_txt_t  this_hdl;
           int  rc_find = this->block_find_first_( &this_hdl);
    if ( rc_find < 0)
    {
        CBM_LogError( "iproc_landscape_reader_txt_t::scan_stream_(): errors occured while scanning for first block  [identifier=",this->fhandler->get_source_info()->identifier,"]");
        c = -1;
    }
    CBM_LogDebug( "iproc_landscape_reader_txt_t::scan_stream_(): ",(_is_rescan?"re":""),"scan for blocks");
    while ( rc_find == 0)
    {
        ++c;

        fhandler_txt_miner_t  miner( &this_hdl);
        iproc_ireader_container_t< iproc_landscape_reader_txt_t >::node_info_t  ni;
        ni.id = miner.read_attribute( ldndc_txt_block, ldndc_txt_id, 0 /*==default ID*/);
        ni.handle = this_hdl;
// sk:dbg        CBM_LogDebug( "iproc_landscape_reader_txt_t::scan_stream_(): finding core: ",ni.id, "  range=[",this_hdl.pb, "(",this_hdl.pd,"),",this_hdl.pe,"]");

        int const  c_node = ( _is_rescan) ? this->ireaders_.reconfigure_node( ni) : this->ireaders_.configure_node( ni);
        if ( c_node)
        {
            c = -1;
            break;
        }

        rc_find = this->block_find_next_( &this_hdl);
        if ( rc_find < 0)
        {
            /* errors occured */
            CBM_LogError( "iproc_landscape_reader_txt_t::scan_stream_(): errors occured during scanning source  [identifier=",this->fhandler->get_source_info()->identifier,"]");
            c = -1;
            break;
        }
    }

    CBM_LogDebug( "iproc_landscape_reader_txt_t::scan_stream_(): found #blocks=",c,"  #readers=",this->ireaders_.reader_size());
// at this point we only support all readers to be valid
// sk:off    for ( node_reader_map_t::iterator  b_rdr = this->node_readers_.begin();  b_rdr != node_readers_.end();  ++b_rdr)
// sk:off    {
// sk:off        if ( !b_rdr->second->data_available())
// sk:off        {
// sk:off            CBM_LogError( "iproc_landscape_reader_txt_t::scan_stream_(): currently, we only support all readers to be valid: i.e. for each reader a source block must exist");
// sk:off            return  -1;
// sk:off        }
// sk:off    }

    return  c;
}



/***  INPUT READER TXT BASE  ***/
iproc_reader_txt_base_t::iproc_reader_txt_base_t()
        : iproc_reader_base_t(),
          iproc_handle_txt_types_t()
{
}
iproc_reader_txt_base_t::~iproc_reader_txt_base_t()
{
}

/***  INPUT READER TXT  ***/

iproc_reader_txt_t::iproc_reader_txt_t()
        : iproc_reader_txt_base_t()
{
}
iproc_reader_txt_t::~iproc_reader_txt_t()
{
}

lerr_t
iproc_reader_txt_t::set_handle(
        ioproc_handle_txt_t const &  _block_handle)
{
    /* note: need copy of core handle here since it is created from a temporary */
    this->miner.handle = _block_handle;
    this->data_arrived = ( this->miner.handle.ifs) ? 1 : 0;

    return  LDNDC_ERR_OK;
}

iproc_reader_txt_t::core_iobject_handle_t const *
iproc_reader_txt_t::get_handle()
const
{
    return  &this->miner.handle;
}

}

