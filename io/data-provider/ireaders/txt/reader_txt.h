/*!
 * @brief
 *    txt input readers
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IREADER_TXT_H_
#define  LDNDC_IO_IREADER_TXT_H_

#include  "io/data-provider/ireaders/ireader.h"
#include  "io/fhandler/txt/fhandler_txt.h"
#include  "io/fhandler/txt/fhandler_txt_miner.h"

#include  "io/data-provider/ireaders/ireader-container.h"
#include  "time/cbm_time.h"

namespace ldndc {

/* ###   TXT INPUT READER   ### */
class  CBM_API  iproc_reader_txt_base_t
    :  public  iproc_reader_base_t,  public  iproc_handle_txt_types_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_txt_base_t::format(); }
    protected:
        iproc_reader_txt_base_t();
        virtual  ~iproc_reader_txt_base_t() = 0;
};

class  CBM_API  iproc_reader_txt_t  :  public  iproc_reader_txt_base_t
{
    public:
        typedef  ioproc_handle_txt_t  node_handle_t;
        lerr_t  set_handle(
                ioproc_handle_txt_t const &);

    protected:
        iproc_reader_txt_t();
        virtual  ~iproc_reader_txt_t() = 0;

        ioproc_handle_txt_t const *  get_handle() const;

    protected:
        mutable  fhandler_txt_miner_t  miner;
};


/* ###  TXT INPUT LANDSCAPE READER  ### */
class  CBM_API  iproc_landscape_reader_txt_base_t
    :  public  iproc_landscape_reader_base_t,  public  iproc_handle_txt_types_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_txt_base_t::format(); }
    public:
        virtual  char const *  core_tag() const = 0;

    protected:
        iproc_landscape_reader_txt_base_t();
        virtual  ~iproc_landscape_reader_txt_base_t() = 0;
};


class  CBM_API  iproc_landscape_reader_txt_t  :  public  iproc_landscape_reader_txt_base_t
{
    public:
        typedef  iproc_reader_txt_t  ireader_t;
        typedef  ioproc_handle_txt_t  node_handle_t;

        iproc_landscape_reader_interface_t::input_stream_mode_e  input_stream_mode() const;
    public:
        lerr_t  status() const { return  this->status_; }

        virtual  int  scan_stream(
                iobject_handler_base_t * /*i/o format handler*/);
        virtual  int  rescan_stream();

        virtual  iproc_provider_t *  acquire_ireader(
                lid_t const & /*block id*/);
        virtual  void  release_ireader(
                lid_t const & /*block id*/);

        virtual  lerr_t  add_to_processing_stack(
                iproc_provider_t ** /*address of pointer to reader*/,
                iproc_provider_t * /*data processors*/);

    public:
        virtual  size_t  number_of_blocks() const;
        virtual  bool    check_input_exists(
                lid_t const & /*block id*/) const;

        virtual  int  get_available_blocks(
                lid_t [], size_t) const;

    public:
        virtual  int  collective_update_commence(
                cbm::sclock_t const *);
        virtual  int  collective_update_complete(
                cbm::sclock_t const *);

    protected:
        iproc_landscape_reader_txt_t();
        virtual  ~iproc_landscape_reader_txt_t() = 0;

    public:
        /* return number of blocks in stream */
        size_t  size() const { return  this->number_of_blocks(); }
        /* return stream size */
        txt_streampos_t  stream_size() const { return  this->shandle.pe; }
        /* return global section start position */
        txt_streampos_t  stream_offset() const { return  this->shandle.pb; }
        /* return global section end position */
        txt_streampos_t  stream_dataoffset() const { return  this->shandle.pd; }

    protected:
        /* pointer to i/o handler */
        iobject_handler_txt_t *  fhandler;
        /* stream handle */
        ioproc_handle_txt_t  shandle;
    private:
        lerr_t  status_;
        iproc_landscape_reader_interface_t::input_stream_mode_e  imode_;

        iproc_ireader_container_t< iproc_landscape_reader_txt_t >  ireaders_;

        struct  cuefile_t
        {
            cuefile_t();

            std::string  filename;
            std::fstream  file;
            int  exists;
            txt_streampos_t  tagpos;
        };
        cuefile_t  cf;

        /*!
         * @brief
         *    set stream info: offset, size, etc
         */
        lerr_t  initialize_stream_handle_(
                ioproc_handle_txt_t * /*stream handle*/);
        /*!
         * @brief
         *    sets the stream's total size.
         *
         * @todo
         *    it also attempts to determine if input stream
         *    is too large for stream position type (see STL)
         */
        lerr_t  detect_stream_size_(
                ioproc_handle_txt_t * /*stream handle*/);
        /*!
         * @brief
         *    try to detect input mode (single vs multi)
         */
        lerr_t  detect_input_mode_(
                ioproc_handle_txt_t * /*stream handle*/);
        lerr_t  detect_global_block_range_(
                ioproc_handle_txt_t * /*stream handle*/);

        /* helper functions */
        int  block_find_(
                ioproc_handle_txt_t * /*buffer*/);
        int  block_find_single_(
                ioproc_handle_txt_t * /*buffer*/);
        int  block_find_multi_(
                ioproc_handle_txt_t * /*buffer*/);
        int  block_find_first_(
                ioproc_handle_txt_t * /*buffer*/);
        int  block_find_next_(
                ioproc_handle_txt_t * /*buffer*/);

        int  scan_stream_(
                bool /*initial scan(false) or rescan(true)?*/);

        /* checks existence of cue file, opens it read-only
         * if it exists, write-only if it does not exist.
         */
        void  cuefile_open_();
        void  cuefile_close_();
        int  set_handle_from_cuefile_(
                ioproc_handle_txt_t * /*buffer*/);
        void  add_handle_to_cuefile_(
                ioproc_handle_txt_t const * /*handle*/);
};
}

#endif  /*  !LDNDC_IO_IREADER_TXT_H_  */

