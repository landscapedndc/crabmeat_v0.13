/*!
 * @brief
 *    txt climate input readers
 *
 * @author
 *    steffen klatt (created on: jan 24, 2014)
 */

#ifndef  LDNDC_IO_IREADER_TXT_CLIMATE_H_
#define  LDNDC_IO_IREADER_TXT_CLIMATE_H_

#include  "io/data-provider/ireaders/txt/reader_txt.h"
#include  "io/fhandler/txt/fhandler_txt_miner.h"

#include  "io/iif/iif_climate.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_climate_txt_t  :  public  iproc_landscape_reader_txt_t,  public  iproc_landscape_interface_climate_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_climate_txt_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_climate_t::input_class_type(); }
    public:
        char const *  core_tag() const { return  CORE_TAG(); }
        static  char const *  CORE_TAG()
            { return  iproc_landscape_interface_climate_t::input_class_type(); }

    public:
        iproc_landscape_reader_climate_txt_t();
        virtual  ~iproc_landscape_reader_climate_txt_t();

        std::string  time() const;
};
class  CBM_API  iproc_reader_climate_txt_t  :  public  iproc_reader_txt_t,  public  iproc_interface_climate_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_climate_txt_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_climate_t::input_class_type(); }
    public:
        iproc_reader_climate_txt_t();
        virtual  ~iproc_reader_climate_txt_t();

        double  get_latitude() const;
        double  get_longitude() const;
        double  get_elevation() const;

        double  get_cloudiness() const;
        double  get_rainfall_intensity() const;
        double  get_wind_speed() const;

        double  get_annual_precipitation() const;
        double  get_temperature_average() const;
        double  get_temperature_amplitude() const;

    public:
        lerr_t  preset(
                char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read(
                element_type [], size_t, size_t,
                char const * [], size_t,
                int, int,
                int *, int *);
    private:
        fhandler_txt_miner_table_t *  tm;
};
}

#endif  /*  !LDNDC_IO_IREADER_TXT_CLIMATE_H_  */

