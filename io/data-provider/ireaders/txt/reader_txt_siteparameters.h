/*!
 * @brief
 *    txt siteparameters input readers
 *
 * @author
 *    steffen klatt (created on: jan 24, 2014)
 */

#ifndef  LDNDC_IO_IREADER_TXT_SITEPARAMETERS_H_
#define  LDNDC_IO_IREADER_TXT_SITEPARAMETERS_H_

#include  "io/data-provider/ireaders/txt/reader_txt.h"
#include  "io/fhandler/txt/fhandler_txt_miner.h"

#include  "io/iif/iif_siteparameters.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_siteparameters_txt_t :  public  iproc_landscape_reader_txt_t,  public  iproc_landscape_interface_siteparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_siteparameters_txt_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_siteparameters_t::input_class_type(); }
    public:
        char const *  core_tag() const { return  CORE_TAG(); }
        static  char const *  CORE_TAG()
            { return  iproc_landscape_interface_siteparameters_t::input_class_type(); }

    public:
        iproc_landscape_reader_siteparameters_txt_t();
        virtual  ~iproc_landscape_reader_siteparameters_txt_t();
};
class  CBM_API  iproc_reader_siteparameters_txt_t  :  public  iproc_reader_txt_t,  public  iproc_interface_siteparameters_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_siteparameters_txt_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_siteparameters_t::input_class_type(); }
    public:
        iproc_reader_siteparameters_txt_t();
        virtual  ~iproc_reader_siteparameters_txt_t();

    public:
        lerr_t  get_siteparameters_set(
                siteparameters::siteparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                int *, int *) const;
    private:
        bool  use_lresources() const;
};
}

#endif  /*  !LDNDC_IO_IREADER_TXT_SITEPARAMETERS_H_  */

