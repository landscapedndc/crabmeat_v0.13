
#ifndef  LDNDC_IO_IREADER_EVENT_TXT_CPP_
#define  LDNDC_IO_IREADER_EVENT_TXT_CPP_

#include  "io/data-provider/ireaders/txt/reader_txt_event.h"
#include  "time/cbm_time.h"

#include  "event/events.h"
#include  "event/eventtypes.h"

#include  "math/cbm_math.h"

#include  "string/lstring-tokenizer.h"


#define  EV_ATTRS_READER_HEADER(__type__)                                    \
    CRABMEAT_FIX_UNUSED(_r);CRABMEAT_FIX_UNUSED(_s);                                \
    crabmeat_assert(_r && _s);                                            \
    txt_streampos_t  ev_node = (txt_streampos_t)_ev_node;                            \
    if ( !ev_node || ev_node == txt_invalid_pos)                                                \
    {                                                    \
        CBM_LogError( "no attribute block in input stream  [event type=",STRINGIFY(__type__),"]");        \
        return  LDNDC_ERR_FAIL;                                        \
    }                                                    \
    event::__event_attrib_class_name(__type__) *  attrs( static_cast< event::__event_attrib_class_name(__type__) * >( _attrs));    \
    event::__event_attrib_class_name(__type__) const *  attrs_defaults(                    \
                static_cast< event::__event_attrib_class_name(__type__) const * >( _attrs_default))/*;*/

#define  EV_ATTR_(__dst__,__attr__,__default__)                                    \
    __dst__ = ATTRIBUTE_VALUE_AT_( _miner, ev_node, __attr__, __default__);                        \
    ++*_s/*;*/

#define  EV_ATTR_LU_(__dst__,__attr__,__default__,__lu_case__)                            \
{                                                        \
    /* a lot of copying... */                                        \
    cbm::nstr  nstr_tmp( ATTRIBUTE_VALUE_AT_( _miner, ev_node, __attr__, __default__),                \
            CNAME_STRIP|(__lu_case__));                                \
    __dst__ = nstr_tmp.str();                                        \
}

#include  "io/data-provider/ireaders/shared/event/ireader_event.h"


/* expands to default event attribute reader handler name */
#define  __EV_READER_HANDLER_NAME(__type__)  event_helper_read_attribute_ ## __type__ ## _txt
/* expands to full default event attribute reader handler signature */
#define  __EV_READER_HANDLER_SIGNATURE(__type__)                                \
    lerr_t                                                    \
    __EV_READER_HANDLER_NAME(__type__)(                                    \
        fhandler_txt_miner_t *  _miner,                                    \
        event::event_attribute_t *  _attrs,                                \
        event::event_attribute_t const *  _attrs_default,                        \
        txt_streampos_t  _ev_node,                                    \
        int *  _r,  int *  _s)

/* holds the list of all event attribute readers */
#include  "io/data-provider/ireaders/txt/reader_txt_event/reader_txt_events.cpp"
#undef  EV_ATTR_
#undef  EV_ATTR_LU_
#undef  EV_ATTR_L_
#undef  EV_ATTR_U_
#undef  EV_ATTR
#undef  EV_ATTR_L
#undef  EV_ATTR_U
#undef  EV_ATTRS_READER_HEADER

namespace ldndc {

/*  *****  EVENT TXT READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_event_txt_t)
iproc_reader_event_txt_t::iproc_reader_event_txt_t()
        : iproc_reader_txt_t(),
          iproc_interface_event_t()
{
    for ( size_t  k = 0;  k < event::EVENT_CNT;  ++k)
    {
        event_helper_attribute_handlers[k] = NULL;
    }

    /* set event attribute reader handlers (see reader_txt_events.cpp) */
    __EV_READER_HANDLERS_SET
}
#undef  __EV_READER_HANDLERS_SET
#undef  __EV_READER_HANDLER_ADD
#undef  __EV_READER_HANDLER_SIGNATURE
#undef  __EV_READER_HANDLER_NAME
iproc_reader_event_txt_t::~iproc_reader_event_txt_t()
{
}


lerr_t
iproc_reader_event_txt_t::get_event_attributes(
        event::event_attribute_t *  _attrs, event::event_attribute_t const *  _attrs_default,
        event::event_info_t *  _ev_info,
        int *  _r, int *  _s)
const
{
    if ( ! _ev_info->r_data)
           {
        CBM_LogError( "iproc_reader_event_txt_t::get_event_attributes(): attempt to read event attribute for null node");
        return  LDNDC_ERR_FAIL;
    }

    if ( ! event_helper_attribute_handlers[_ev_info->type])
    {
        /* assume event without attributes */
        return  LDNDC_ERR_OK;
    }

    int  r( 0), s( 0);
    lerr_t  rc = event_helper_attribute_handlers[_ev_info->type](
            &this->miner, _attrs, _attrs_default, (txt_streampos_t)_ev_info->r_data, &r, &s);

    if ( _r) *_r = r;
    if ( _s) *_s = s;

    return  rc;
}



char const *  iproc_reader_event_txt_t::ROTATION_TAG = "rotation";
char const *  iproc_reader_event_txt_t::EVENT_TAG = "event";
char const  iproc_reader_event_txt_t::EVENT_GLUE = '@';

bool
iproc_reader_event_txt_t::is_valid_event(
        event::event_type_e *  _buf,
        char const *  _event_name)
const
{
    cbm::tokenizer  ev_name_tok( _event_name, EVENT_GLUE, CNAME_STRIP_TO_LOWER|CNAME_STRIP, CNAME_WHITESPACE);
    if ( !ev_name_tok.is_ok() || ev_name_tok.token_cnt() < 2)
    {
        return  false;
    }

    unsigned int  ev_type = event::EVENT_NONE;
    cbm::find_index( ev_name_tok[1], event::EVENT_NAMES, event::EVENT_CNT, &ev_type);
    if ( ev_type == event::EVENT_NONE)
    {
        return  false;
    }
    if ( _buf)
    {
        *_buf = static_cast< event::event_type_e >( ev_type);
    }

    return  true;
}

lerr_t
iproc_reader_event_txt_t::attach_events_(
        cbm::event_info_tree_t *  _ev_tree,
        int  _load_limit,
        int *  _r, int * _s)
{
    if ( _load_limit == 0)
    {
        return  LDNDC_ERR_STOP_ITERATION;
    }

    int  n_ev = 0;
    int  n_tags = 0;
    int const  load_limit = ( _load_limit == -1) ? INT_MAX : _load_limit;

    std::string  event_name;
    txt_streampos_t  event_pos = this->last_loaded.event_pos;

    while ( n_ev < load_limit)
    {
        event_pos = this->miner.find_next_tag( &event_name, event_pos+1);
        if ( event_pos == txt_invalid_pos)
        {
            this->last_loaded.hit_eof = 1;
            break;
        }
        ++n_tags;

        event::event_type_e  event_type = event::EVENT_NONE;
        if ( !this->is_valid_event( &event_type, event_name.c_str()))
        {
            CBM_LogWarn( "iproc_reader_event_txt_t::attach_events_(): unknown event, skipping  [event=",event_name,"]");
            continue;
        }
// sk:dbg        CBM_LogDebug( "iproc_reader_event_txt_t::attach_events_(): event read[",event_pos,"]=\"",event_name,"\"  type=",event::EVENT_NAMES[event_type],"");

        event::event_info_t  event_info( event_type);
        event_info.t_exec_s = this->miner.read_attribute_at( event_pos, "time", ldndc_empty_string);
        event_info.r_data = event_pos;

        _ev_tree->append_child( _ev_tree->begin(), event_info);

        ++n_ev;
    }

    if ( _r) { *_r = n_ev; }
    if ( _s) { *_s = n_tags; }

    this->last_loaded.event_pos = event_pos+1;


    return  LDNDC_ERR_OK;
}

int
iproc_reader_event_txt_t::readahead_size()
const
{
    return  ATTRIBUTE_VALUE( "event", "readaheadsize", this->invalid_readahead_size());
}

lerr_t
iproc_reader_event_txt_t::get_event_tree(
        cbm::event_info_tree_t *  _ev_tree_root,
        int  _load_limit,
        int *  _r, int *  _s)
{
    if ( !_ev_tree_root)
	{
        return  LDNDC_ERR_FAIL;
    }

    if ( iproc_reader_txt_t::data_available())
    {
        this->last_loaded.hit_eof = 0;
        this->last_loaded.event_pos = txt_invalid_pos;
    }

    if ( !this->data_available())
    {
        return  LDNDC_ERR_INPUT_EXHAUSTED;
    }

    int  r = 0;
    int  s = 0;

    /* kick off the event node attaching */
    if ( this->last_loaded.event_pos == txt_invalid_pos)
    {
        this->last_loaded.event_pos = this->miner.handle.pd;
    }
    this->attach_events_( _ev_tree_root, _load_limit, &r, &s);

    if ( _r) { *_r = r; }
    if ( _s) { *_s = s; }

    return  LDNDC_ERR_OK;
}

int
iproc_reader_event_txt_t::data_available()
const
{
    if ( iproc_reader_txt_t::data_available())
    {
        return  1;
    }
    return  ( this->last_loaded.hit_eof == 0) ? 1 : 0;
}


/*  *****  EVENT TXT READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_event_txt_t)
iproc_landscape_reader_event_txt_t::iproc_landscape_reader_event_txt_t()
        : iproc_landscape_reader_txt_t(),
          iproc_landscape_interface_event_t()
{
}


iproc_landscape_reader_event_txt_t::~iproc_landscape_reader_event_txt_t()
{
}


std::string
iproc_landscape_reader_event_txt_t::time()
const
{
    std::string  global_time;
        GLOBAL_ATTRIBUTE_VALUE( global_time, "time", ldndc_empty_string);
    return  global_time;
}

#undef  __EV_READER_HANDLER_NAME
#undef  __EV_READER_HANDLER_SIGNATURE

}

#endif  /*  !LDNDC_IO_IREADER_EVENT_TXT_CPP_  */

