
#ifndef  LDNDC_IO_IREADER_TXT_AIRCHEMISTRY_CPP_
#define  LDNDC_IO_IREADER_TXT_AIRCHEMISTRY_CPP_

#include  "io/data-provider/ireaders/txt/reader_txt_airchemistry.h"
namespace ldndc {

    /*  *****  AIRCHEMISTRY TXT READER  *****  */
    LDNDC_SERVER_OBJECT_DEFN(ldndc::iproc_reader_airchemistry_txt_t)
    iproc_reader_airchemistry_txt_t::iproc_reader_airchemistry_txt_t()
    : iproc_reader_txt_t(),
    iproc_interface_airchemistry_t(),
    tm( NULL)
    {
    }
    iproc_reader_airchemistry_txt_t::~iproc_reader_airchemistry_txt_t()
    {
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_ch4()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "ch4", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_co2()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "co2", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_nh3()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "nh3", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_nh4()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "nh4", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_nh4dry()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "nh4dry", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_no()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "no", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_no2()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "no2", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_no3()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "no3", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_no3dry()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "no3dry", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_o2()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "o2", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_o3()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "o3", invalid_dbl);
    }

#ifdef  M2D_CONTRIB_WOCHELE
    double
    iproc_reader_airchemistry_txt_t::get_average_bc()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "bc", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_cl()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "cl", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_na()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "na", invalid_dbl);
    }

    double
    iproc_reader_airchemistry_txt_t::get_average_so4()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "so4", invalid_dbl);
    }
#endif


#define  __class__  airchemistry
#define  __reader__  iproc_reader_airchemistry_txt_t
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-preset.cpp"
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-reset.cpp"
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-read.cpp"
#undef  __reader__
#undef  __class__




    /*  *****  AIRCHEMISTRY TXT LANDSCAPE READER  *****  */
    LDNDC_SERVER_OBJECT_DEFN(ldndc::iproc_landscape_reader_airchemistry_txt_t)
    iproc_landscape_reader_airchemistry_txt_t::iproc_landscape_reader_airchemistry_txt_t()
    : iproc_landscape_reader_txt_t(),
    iproc_landscape_interface_airchemistry_t()
    {
    }


    iproc_landscape_reader_airchemistry_txt_t::~iproc_landscape_reader_airchemistry_txt_t()
    {
    }


    std::string
    iproc_landscape_reader_airchemistry_txt_t::time()
    const
    {
        std::string  global_time;
        GLOBAL_ATTRIBUTE_VALUE( global_time, "time", ldndc_empty_string);
        return  global_time;
    }
    
    // sk:todo bool
    // sk:todo __LDNDC_READER_CLASS_CLASS(airchemistry,txt)::leapyear()
    // sk:todo const
    // sk:todo {
    // sk:todo     return  GLOBAL_ATTRIBUTE_VALUE( "leapyear", true);
    // sk:todo }
    
}

#endif  /*  !LDNDC_IO_IREADER_TXT_AIRCHEMISTRY_CPP_  */

