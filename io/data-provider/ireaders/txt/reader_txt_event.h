/*!
 * @brief
 *    txt event input readers
 *
 * @author
 *    steffen klatt (created on: jan 24, 2014)
 */

#ifndef  LDNDC_IO_IREADER_TXT_EVENT_H_
#define  LDNDC_IO_IREADER_TXT_EVENT_H_

#include  "io/data-provider/ireaders/txt/reader_txt.h"
#include  "io/input/event/event-tree.h"
#include  "io/fhandler/txt/fhandler_txt_miner.h"

#include  "io/iif/iif_event.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_event_txt_t  :  public  iproc_landscape_reader_txt_t,  public  iproc_landscape_interface_event_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_event_txt_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_event_t::input_class_type(); }
    public:
        char const *  core_tag() const { return  CORE_TAG(); }
    private:
        static  char const *  CORE_TAG()
            { return  iproc_landscape_interface_event_t::input_class_type(); }

    public:
        iproc_landscape_reader_event_txt_t();
        virtual  ~iproc_landscape_reader_event_txt_t();

        std::string  time() const;
};
class  CBM_API  iproc_reader_event_txt_t  :  public  iproc_reader_txt_t,  public  iproc_interface_event_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_event_txt_t)
    typedef  ioproc_handle_txt_t::streampos  streampos;
    typedef  lerr_t (*event_attribute_readers_txt)( fhandler_txt_miner_t *, event::event_attribute_t *, event::event_attribute_t const *, txt_streampos_t, int *, int *);

    static char const *  ROTATION_TAG;
    static char const *  EVENT_TAG;
    static char const  EVENT_GLUE;

    public:
        char const *  input_class_type() const { return  iproc_interface_event_t::input_class_type(); }

        iproc_reader_event_txt_t();
        virtual  ~iproc_reader_event_txt_t();

    public:
        int  data_available() const;

        int  readahead_size() const;
        lerr_t  get_event_tree(
                cbm::event_info_tree_t *,
                int /*load limit*/,
                int *, int *);
        lerr_t  get_event_attributes(
                event::event_attribute_t *, event::event_attribute_t const *,
                       event::event_info_t *,
                int *, int *) const;

    private:
        bool  is_valid_event(
                event::event_type_e * /*type buffer*/,
                char const * /*event name*/) const;

        lerr_t  attach_events_(
                cbm::event_info_tree_t *,
                int,
                int *, int *);

        event_attribute_readers_txt  event_helper_attribute_handlers[event::EVENT_CNT];

        struct  event_txt_position_t
        {
            event_txt_position_t() : event_pos(txt_invalid_pos),hit_eof(0) {}

            txt_streampos_t  event_pos;
            int  hit_eof;
        };
        struct event_txt_position_t  last_loaded;
};
}

#endif  /*  !LDNDC_IO_IREADER_TXT_EVENT_H_  */

