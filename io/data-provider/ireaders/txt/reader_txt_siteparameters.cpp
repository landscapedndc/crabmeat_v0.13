
#ifndef  LDNDC_IO_IREADER_TXT_SITEPARAMETERS_CPP_
#define  LDNDC_IO_IREADER_TXT_SITEPARAMETERS_CPP_

#include  "io/data-provider/ireaders/txt/reader_txt_siteparameters.h"
#include  "io/data-provider/ireaders/lrsrc/reader_lrsrc_siteparameters.h"

namespace ldndc {

/*  *****  SITEPARAMETER TXT READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_siteparameters_txt_t)
iproc_reader_siteparameters_txt_t::iproc_reader_siteparameters_txt_t()
        : iproc_reader_txt_t(),
          iproc_interface_siteparameters_t()
{
    this->data_arrived = 1;
}
iproc_reader_siteparameters_txt_t::~iproc_reader_siteparameters_txt_t()
{
}

#define  TXT_SITEPARAMETER_VALUE(__name__,__default__)                        \
    this->miner.read_attribute_at(this->miner.handle.pd,__name__,__default__,&ack)/*;*/

bool
iproc_reader_siteparameters_txt_t::use_lresources()
const
{
    bool  use_lr = ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "useresources", true);
    if ( use_lr)
    {
        iproc_reader_siteparameters_lrsrc_t const  rsrc_rdr;
        use_lr = rsrc_rdr.is_open();
    }
    return  use_lr;
}

lerr_t
iproc_reader_siteparameters_txt_t::get_siteparameters_set(
        siteparameters::siteparameter_t  _props[],
               ldndc_string_t const  _props_names[], char const _props_types[], size_t  _n_props,
        int *  _p_read, int *  _p_seen)
const
{
    int  p_read = 0;  int  p_seen = 0;
    if ( this->use_lresources())
    {
        iproc_reader_siteparameters_lrsrc_t const  rsrc_rdr;
        lerr_t  rc_read = rsrc_rdr.get_siteparameters_set(
                _props, _props_names, _props_types, _n_props, &p_read, &p_seen);
        if ( rc_read)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    int  r = 0; int  s = 0;
    for ( size_t  k = 0;  k < _n_props;  ++k)
    {
        std::string  name_lowercase = cbm::to_lower( _props_names[k]);

        int  ack = 0;
        switch ( _props_types[k])
        {
            case 'd':
            case 'f':
            {
                _props[k] = TXT_SITEPARAMETER_VALUE( name_lowercase.c_str(), ldndc::invalid_t< siteparameters::siteparameter_t::float_type >::value);
                if ( ack == 1)
                {
                    ++r;
                    if ( cbm::is_valid< siteparameters::siteparameter_t::float_type >( _props[k]))
                    {
                        ++s;
                    }
                }
                break;
            }
            case 'i':
            {
                _props[k] = TXT_SITEPARAMETER_VALUE( name_lowercase.c_str(), ldndc::invalid_t< siteparameters::siteparameter_t::int_type >::value);
                if ( ack == 1)
                {
                    ++r;
                    if ( cbm::is_valid< siteparameters::siteparameter_t::int_type >( _props[k]))
                    {
                        ++s;
                    }
                }
                break;
            }
            case 'u':
            {
                _props[k] = TXT_SITEPARAMETER_VALUE( name_lowercase.c_str(), ldndc::invalid_t< siteparameters::siteparameter_t::uint_type >::value);
                if ( ack == 1)
                {
                    ++r;
                    if ( cbm::is_valid< siteparameters::siteparameter_t::uint_type >( _props[k]))
                    {
                        ++s;
                    }
                }
                break;
            }
            case 'b':
            {
                std::string  p_k = TXT_SITEPARAMETER_VALUE( name_lowercase.c_str(), invalid_string);
                if ( ack == 1)
                {
                    siteparameters::siteparameter_t::bool_type  p_k_val;
                    lerr_t  rc_convert = cbm::s2n< siteparameters::siteparameter_t::bool_type >( p_k, &p_k_val);
                    if ( rc_convert)
                    {
                        CBM_LogError( "iproc_reader_siteparameters_txt_t::get_siteparameters_set(): ",
                                "not understanding parameter value  [value=",p_k,",type=",_props_types[k],",parameter=",_props_names[k],"]");
                    }
                    else
                    {
                        _props[k] = p_k_val;
                        ++r;
                    }
                }
                break;
            }

            default:
                CBM_LogError( "[BUG]  iproc_reader_siteparameters_txt_t::get_siteparameters_set(): ",
                        "unknown site parameter data type  [type=",_props_types[k],",parameter=",_props_names[k],"]");
                return  LDNDC_ERR_FAIL;
        }
    }

    if ( this->use_lresources())
    {
        /* parameters read */
        if ( _p_read) *_p_read = p_read;
        /* parameters valid */
        if ( _p_seen) *_p_seen = p_seen;

    }
    else
    {
        /* parameters read */
        if ( _p_read) *_p_read = r;
        /* parameters valid */
        if ( _p_seen) *_p_seen = s;
    }

    this->data_arrived = 0;
    return  LDNDC_ERR_OK;
}



/*  *****  SITEPARAMETER TXT LANDSCAPE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_siteparameters_txt_t)
iproc_landscape_reader_siteparameters_txt_t::iproc_landscape_reader_siteparameters_txt_t()
        : iproc_landscape_reader_txt_t(),
          iproc_landscape_interface_siteparameters_t()
{
}
iproc_landscape_reader_siteparameters_txt_t::~iproc_landscape_reader_siteparameters_txt_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_TXT_SITEPARAMETERS_CPP_  */

