
#ifndef  LDNDC_IO_IREADER_TXT_CLIMATE_CPP_
#define  LDNDC_IO_IREADER_TXT_CLIMATE_CPP_

#include  "io/data-provider/ireaders/txt/reader_txt_climate.h"
namespace ldndc {

/*  *****  CLIMATE TXT READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(ldndc::iproc_reader_climate_txt_t)
iproc_reader_climate_txt_t::iproc_reader_climate_txt_t()
        : iproc_reader_txt_t(),
          iproc_interface_climate_t(),
          tm( NULL)
{
}
iproc_reader_climate_txt_t::~iproc_reader_climate_txt_t()
{
}


double
iproc_reader_climate_txt_t::get_latitude()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "latitude", invalid_dbl);
}

double
iproc_reader_climate_txt_t::get_longitude()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "longitude", invalid_dbl);
}

double
iproc_reader_climate_txt_t::get_elevation()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "elevation", invalid_dbl);
}

double
iproc_reader_climate_txt_t::get_cloudiness()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "cloudiness", invalid_dbl);
}
double
iproc_reader_climate_txt_t::get_rainfall_intensity()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "rainfallintensity", invalid_dbl);
}
double
iproc_reader_climate_txt_t::get_wind_speed()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "windspeed", invalid_dbl);
}

double
iproc_reader_climate_txt_t::get_annual_precipitation()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "annualprecipitation", invalid_dbl);
}
double
iproc_reader_climate_txt_t::get_temperature_average()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "temperatureaverage", invalid_dbl);
}
double
iproc_reader_climate_txt_t::get_temperature_amplitude()
const
{
    return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "temperatureamplitude", invalid_dbl);
}


#define  __class__  climate
#define  __reader__  iproc_reader_climate_txt_t
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-preset.cpp"
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-reset.cpp"
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-read.cpp"
#undef  __reader__
#undef  __class__




/*  *****  CLIMATE TXT LANDSCAPE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(ldndc::iproc_landscape_reader_climate_txt_t)
iproc_landscape_reader_climate_txt_t::iproc_landscape_reader_climate_txt_t()
    : iproc_landscape_reader_txt_t(),
      iproc_landscape_interface_climate_t()
{
}
iproc_landscape_reader_climate_txt_t::~iproc_landscape_reader_climate_txt_t()
{
}


std::string
iproc_landscape_reader_climate_txt_t::time()
const
{
    std::string  global_time;
    GLOBAL_ATTRIBUTE_VALUE( global_time, "time", ldndc_empty_string);
    return  global_time;
}

// sk:todo bool
// sk:todo iproc_landscape_reader_climate_txt_t::leapyear()
// sk:todo const
// sk:todo {
// sk:todo     return  GLOBAL_ATTRIBUTE_VALUE( "leapyear", true);
// sk:todo }

}

#endif  /*  !LDNDC_IO_IREADER_TXT_CLIMATE_CPP_  */

