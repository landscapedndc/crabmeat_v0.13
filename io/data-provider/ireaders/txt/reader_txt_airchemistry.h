/*!
 * @brief
 *    txt air chemistry input readers
 *
 * @author
 *    steffen klatt (created on: jan 24, 2014)
 */

#ifndef  LDNDC_IO_IREADER_TXT_AIRCHEMISTRY_H_
#define  LDNDC_IO_IREADER_TXT_AIRCHEMISTRY_H_

#include  "io/data-provider/ireaders/txt/reader_txt.h"
#include  "io/iif/iif_airchemistry.h"
namespace ldndc {
class  CBM_API  iproc_landscape_reader_airchemistry_txt_t  :  public  iproc_landscape_reader_txt_t,  public  iproc_landscape_interface_airchemistry_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_airchemistry_txt_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_airchemistry_t::input_class_type(); }
    public:
        char const *  core_tag() const { return  CORE_TAG(); }
        static  char const *  CORE_TAG()
            { return iproc_landscape_interface_airchemistry_t::input_class_type(); }

    public:
        iproc_landscape_reader_airchemistry_txt_t();
        virtual  ~iproc_landscape_reader_airchemistry_txt_t();

        std::string  time() const;
};
class  CBM_API  iproc_reader_airchemistry_txt_t  :  public  iproc_reader_txt_t,  public  iproc_interface_airchemistry_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_airchemistry_txt_t)
    public:
        char const *  input_class_type() const { return  iproc_interface_airchemistry_t::input_class_type(); }
    public:
        iproc_reader_airchemistry_txt_t();
        virtual  ~iproc_reader_airchemistry_txt_t();

    public:
        /* info getters here */
        double  get_average_ch4() const;
        double  get_average_co2() const;
        double  get_average_n() const;
        double  get_average_nh3() const;
        double  get_average_nh4() const;
        double  get_average_nh4dry() const;
        double  get_average_no() const;
        double  get_average_no2() const;
        double  get_average_no3() const;
        double  get_average_no3dry() const;
        double  get_average_o2() const;
        double  get_average_o3() const;

    public:
        lerr_t  preset(
                char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read(
                element_type [], size_t, size_t,
                char const * [], size_t,
                int, int,
                int *, int *);

    private:
        fhandler_txt_miner_table_t *  tm;
};
}

#endif  /*  !LDNDC_IO_IREADER_TXT_AIRCHEMISTRY_H_  */

