
#ifndef  LDNDC_IO_IREADER_TXT_GROUNDWATER_CPP_
#define  LDNDC_IO_IREADER_TXT_GROUNDWATER_CPP_

#include  "io/data-provider/ireaders/txt/reader_txt_groundwater.h"
namespace ldndc {

    /*  *****  GROUNDWATER TXT READER  *****  */
    LDNDC_SERVER_OBJECT_DEFN(ldndc::iproc_reader_groundwater_txt_t)
    iproc_reader_groundwater_txt_t::iproc_reader_groundwater_txt_t()
    : iproc_reader_txt_t(),
    iproc_interface_groundwater_t(),
    tm( NULL)
    {
    }
    iproc_reader_groundwater_txt_t::~iproc_reader_groundwater_txt_t()
    {
    }

    double
    iproc_reader_groundwater_txt_t::get_average_watertable()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "watertable", invalid_dbl);
    }

    double
    iproc_reader_groundwater_txt_t::get_average_no3()
    const
    {
        return  ATTRIBUTE_VALUE( TXT_HEADER_ATTRIB_TAG, "no3", invalid_dbl);
    }

#define  __class__  groundwater
#define  __reader__  iproc_reader_groundwater_txt_t
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-preset.cpp"
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-reset.cpp"
#include  "io/data-provider/ireaders/txt/shared/reader-streamdata-read.cpp"
#undef  __reader__
#undef  __class__




    /*  *****  GROUNDWATER TXT LANDSCAPE READER  *****  */
    LDNDC_SERVER_OBJECT_DEFN(ldndc::iproc_landscape_reader_groundwater_txt_t)
    iproc_landscape_reader_groundwater_txt_t::iproc_landscape_reader_groundwater_txt_t()
    : iproc_landscape_reader_txt_t(),
    iproc_landscape_interface_groundwater_t()
    {
    }


    iproc_landscape_reader_groundwater_txt_t::~iproc_landscape_reader_groundwater_txt_t()
    {
    }


    std::string
    iproc_landscape_reader_groundwater_txt_t::time()
    const
    {
        std::string  global_time;
        GLOBAL_ATTRIBUTE_VALUE( global_time, "time", ldndc_empty_string);
        return  global_time;
    }
    
    // sk:todo bool
    // sk:todo __LDNDC_READER_CLASS_CLASS(groundwater,txt)::leapyear()
    // sk:todo const
    // sk:todo {
    // sk:todo     return  GLOBAL_ATTRIBUTE_VALUE( "leapyear", true);
    // sk:todo }
    
}

#endif  /*  !LDNDC_IO_IREADER_TXT_GROUNDWATER_CPP_  */

