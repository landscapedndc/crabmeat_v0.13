/*!
 * @brief
 *    txt geometry input readers
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_IREADER_TXT_GEOMETRY_CPP_
#define  LDNDC_IO_IREADER_TXT_GEOMETRY_CPP_

#include  "io/data-provider/ireaders/txt/reader_txt_geometry.h"
namespace ldndc {

/*  *****  GEOMETRY TXT READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_reader_geometry_txt_t)
iproc_reader_geometry_txt_t::iproc_reader_geometry_txt_t()
        : iproc_reader_txt_t(),
          iproc_interface_geometry_t()
{
}
iproc_reader_geometry_txt_t::~iproc_reader_geometry_txt_t()
{
}


lerr_t
iproc_reader_geometry_txt_t::read_shape(
        cbm::geom_shape_t *  _shape)
{
    fhandler_txt_miner_table_t *  tbl_miner = NULL;

    fhandler_txt_miner_t::table_miner_handle_t  m_hdl = this->setup_table_miner_( &tbl_miner);
    if ( m_hdl == fhandler_txt_miner_t::table_miner_invalid_handle)
    {
        return  LDNDC_ERR_FAIL;
    }
    crabmeat_assert( tbl_miner);

    lerr_t  rc = LDNDC_ERR_OK;

    rc = this->read_number_of_vertices_( &_shape->n_vertices);
    if ( rc)
    {
        this->miner.delete_table_miner( m_hdl);
        return  LDNDC_ERR_FAIL;
    }

    rc = this->read_vertices_( _shape, tbl_miner);
    if ( rc)
    {
        this->miner.delete_table_miner( m_hdl);
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

fhandler_txt_miner_t::table_miner_handle_t
iproc_reader_geometry_txt_t::setup_table_miner_(
        fhandler_txt_miner_table_t **  _tbl_miner)
{
    fhandler_txt_miner_t::table_miner_handle_t  m_hdl =
               this->miner.new_table_miner( _tbl_miner, "data", geometry::RECORD_ITEM_NAMES, geometry::record::RECORD_SIZE);
    if ( !_tbl_miner)
    {
        return  fhandler_txt_miner_t::table_miner_invalid_handle;
    }
    this->miner.rewind_table( m_hdl);

    return  m_hdl;
}

lerr_t
iproc_reader_geometry_txt_t::read_number_of_vertices_(
        size_t *  _n_vertices)
{
    size_t  n_vertices = ATTRIBUTE_VALUE( "geometry", "vertices", invalid_size);
    if ( n_vertices == invalid_size)
    {
        CBM_LogError( "iproc_reader_geometry_txt_t::read_number_of_vertices_(): ",
                "we currently require the number of vertices to be provided  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }
    if ( n_vertices == 0)
    {
        CBM_LogError( "iproc_reader_geometry_txt_t::read_number_of_vertices_(): ",
                "no vertices: invalid shape  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }

    *_n_vertices = n_vertices;
    return  LDNDC_ERR_OK;
}

lerr_t
iproc_reader_geometry_txt_t::read_vertices_(
        cbm::geom_shape_t *  _shape,
        fhandler_txt_miner_table_t *  _tbl_miner)
{
    if (( _shape->n_vertices == 0) || ( _shape->n_vertices == invalid_size))
    {
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc = LDNDC_ERR_OK;

    rc = this->allocate_vertices_( _shape);
    if ( rc)
    {
        this->deallocate_vertices_( _shape);
        return  LDNDC_ERR_FAIL;
    }

    double *  buf = NULL;
    rc = this->allocate_tablebuffer_( &buf, geometry::record::RECORD_SIZE*_shape->n_vertices);
    if ( rc)
    {
        this->deallocate_vertices_( _shape);
        this->deallocate_tablebuffer_( buf);
        return  LDNDC_ERR_FAIL;
    }

    this->miner.read_table( _tbl_miner, buf, _shape->n_vertices);
    if ( static_cast< size_t >( _tbl_miner->records_read) < static_cast< size_t >( _shape->n_vertices))
    {
        CBM_LogError( "iproc_reader_geometry_txt_t::read_vertices_(): ",
                "read less vertices than expected  [",_tbl_miner->records_read," vs ",_shape->n_vertices,"]");
        this->free_shape( _shape);
        return  LDNDC_ERR_FAIL;
    }

    this->copy_coords_to_shape_( _shape, buf);
    this->deallocate_tablebuffer_( buf);

    return  LDNDC_ERR_OK;
}

void
iproc_reader_geometry_txt_t::copy_coords_to_shape_(
           cbm::geom_shape_t *  _shape,
           double *  _buf)
{
    crabmeat_assert( _shape && _shape->vertices);
    crabmeat_assert( _buf);

    for ( size_t  v = 0;  v < _shape->n_vertices;  ++v, _buf+=geometry::record::RECORD_SIZE)
    {
        _shape->vertices[v].x = _buf[geometry::record::RECORD_ITEM_X];
        _shape->vertices[v].y = _buf[geometry::record::RECORD_ITEM_Y];
        _shape->vertices[v].z = _buf[geometry::record::RECORD_ITEM_Z];
// sk:dbg        CBM_LogDebug( v,"\tx=",_shape->vertices[v].x,"  y=",_shape->vertices[v].y,"  z=",_shape->vertices[v].z);
    }
}
lerr_t
iproc_reader_geometry_txt_t::allocate_vertices_(
        cbm::geom_shape_t *  _shape)
{
    _shape->vertices = CBM_DefaultAllocator->construct< cbm::geom_coord_t >( _shape->n_vertices);
    if ( !_shape->vertices)
    {
        CBM_LogError( "iproc_reader_geometry_txt_t::allocate_vertices_(): nomem");
        return  LDNDC_ERR_NOMEM;
    }
    return  LDNDC_ERR_OK;
}
void
iproc_reader_geometry_txt_t::deallocate_vertices_(
        cbm::geom_shape_t *  _shape)
{
    CBM_DefaultAllocator->destroy( _shape->vertices);
}
lerr_t
iproc_reader_geometry_txt_t::allocate_tablebuffer_(
        double **  _buf, size_t  _size)
{
    *_buf = CBM_DefaultAllocator->allocate_type< double >( _size);
    if ( !*_buf)
    {
        CBM_LogError( "iproc_reader_geometry_txt_t::allocate_tablebuffer_(): nomem");
        return  LDNDC_ERR_NOMEM;
    }
    return  LDNDC_ERR_OK;
}
void
iproc_reader_geometry_txt_t::deallocate_tablebuffer_(
        double *  _buf)
{
    CBM_DefaultAllocator->deallocate( _buf);
}


void
iproc_reader_geometry_txt_t::free_shape(
        cbm::geom_shape_t *  _shape)
{
    if ( !_shape)
        { return; }

    if ( _shape->vertices)
    {
        this->deallocate_vertices_( _shape);
        _shape->vertices = NULL;
    }
    _shape->n_vertices = 0;
}

void
iproc_reader_geometry_txt_t::set_mapinfo(
        cbm::geom_mapinfo_t const *  _mapinfo)
{
    crabmeat_assert( _mapinfo);
    this->mapinfo = *_mapinfo;
}
void
iproc_reader_geometry_txt_t::get_mapinfo(
        cbm::geom_mapinfo_t *  _mapinfo)
const
{
    if ( _mapinfo)
        { *_mapinfo = this->mapinfo; }
}


/*  *****  GEOMETRY TXT LANDSCAPE READER  *****  */
LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_geometry_txt_t)
iproc_landscape_reader_geometry_txt_t::iproc_landscape_reader_geometry_txt_t()
        : iproc_landscape_reader_txt_t(),
          iproc_landscape_interface_geometry_t()
{
}
iproc_landscape_reader_geometry_txt_t::~iproc_landscape_reader_geometry_txt_t()
{
}

iproc_provider_t *
iproc_landscape_reader_geometry_txt_t::acquire_ireader(
        lid_t const &  _id)
{
    iproc_reader_geometry_txt_t *  rdr = 
        static_cast< iproc_reader_geometry_txt_t * >( iproc_landscape_reader_txt_t::acquire_ireader( _id));

    cbm::geom_mapinfo_t  geom_layout;
    this->read_mapinfo( &geom_layout);
    rdr->set_mapinfo( &geom_layout);

    return  static_cast< iproc_provider_t * >( rdr);
}

void
iproc_landscape_reader_geometry_txt_t::read_mapinfo(
        cbm::geom_mapinfo_t *  _mapinfo)
{
    crabmeat_assert( _mapinfo);

    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->bb.ul.x, "upperleftx", invalid_dbl);
    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->bb.ul.y, "upperlefty", invalid_dbl);
    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->bb.lr.x, "lowerrightx", invalid_dbl);
    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->bb.lr.y, "lowerrighty", invalid_dbl);

    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->dim.n_x, "dimx", invalid_int);
    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->dim.n_y, "dimy", invalid_int);

    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->extent_x, "extentx", invalid_dbl);
    GLOBAL_ATTRIBUTE_VALUE( _mapinfo->extent_y, "extenty", invalid_dbl);

    _mapinfo->parts = this->size();

    CBM_LogDebug( "bb=",_mapinfo->bb.to_string(),
            "  dimx=",_mapinfo->dim.n_x,
            "  dimy=",_mapinfo->dim.n_y,
            "  extentx=",_mapinfo->extent_x,
            "  extenty=",_mapinfo->extent_y,
            "  size=",_mapinfo->parts);
}

}

#endif  /*  !LDNDC_IO_IREADER_TXT_GEOMETRY_CPP_  */

