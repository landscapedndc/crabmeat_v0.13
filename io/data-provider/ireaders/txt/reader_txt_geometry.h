/*!
 * @brief
 *    txt geometry input readers
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_IREADER_TXT_GEOMETRY_H_
#define  LDNDC_IO_IREADER_TXT_GEOMETRY_H_

#include  "io/data-provider/ireaders/txt/reader_txt.h"
#include  "io/fhandler/txt/fhandler_txt_miner.h"

#include  "io/iif/iif_geometry.h"
namespace ldndc {
class  CBM_API  iproc_reader_geometry_txt_t  :  public  iproc_reader_txt_t,  public  iproc_interface_geometry_t
{
    LDNDC_SERVER_OBJECT(iproc_reader_geometry_txt_t)
    public:
        char const *  input_class_type() const
            { return  iproc_interface_geometry_t::input_class_type(); }
    public:
        iproc_reader_geometry_txt_t();
        virtual  ~iproc_reader_geometry_txt_t();

    public:
        lerr_t  read_shape(
                cbm::geom_shape_t *);
        void  free_shape(
                cbm::geom_shape_t *);

    public:
        void  set_mapinfo(
                cbm::geom_mapinfo_t const *);
        void  get_mapinfo(
                cbm::geom_mapinfo_t *) const;
    private:
        cbm::geom_mapinfo_t  mapinfo;

        fhandler_txt_miner_t::table_miner_handle_t  setup_table_miner_(
                fhandler_txt_miner_table_t ** /*table miner*/);
        lerr_t  read_number_of_vertices_(
                size_t * /*result*/);
        lerr_t  read_vertices_(
                cbm::geom_shape_t * /*shape*/,
                fhandler_txt_miner_table_t * /*table miner*/);

        void  copy_coords_to_shape_(
                cbm::geom_shape_t * /*shape*/,
                double * /*buffer*/);
        lerr_t  allocate_vertices_(
                cbm::geom_shape_t * /*shape*/);
        void  deallocate_vertices_(
                cbm::geom_shape_t * /*shape*/);
        lerr_t  allocate_tablebuffer_(
                double ** /*pointer to buffer*/, size_t /*size*/);
        void  deallocate_tablebuffer_(
                double * /*buffer*/);
};
class  CBM_API  iproc_landscape_reader_geometry_txt_t :  public  iproc_landscape_reader_txt_t,  public  iproc_landscape_interface_geometry_t
{
    LDNDC_SERVER_OBJECT(iproc_landscape_reader_geometry_txt_t)
    public:
        char const *  input_class_type() const
            { return  iproc_landscape_interface_geometry_t::input_class_type(); }
    public:
        char const *  core_tag() const { return  CORE_TAG(); }
        static  char const *  CORE_TAG()
            { return  iproc_landscape_interface_geometry_t::input_class_type(); }

    public:
        virtual  iproc_provider_t *  acquire_ireader(
                lid_t const & /*block id*/);

        virtual  void  read_mapinfo(
                cbm::geom_mapinfo_t *);
    public:
        iproc_landscape_reader_geometry_txt_t();
        virtual  ~iproc_landscape_reader_geometry_txt_t();
};
}

#endif  /*  !LDNDC_IO_IREADER_TXT_GEOMETRY_H_  */

