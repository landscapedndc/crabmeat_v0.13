/*!
 * @brief
 *    concrete sqlite3 database readers implementation
 *
 * @author
 *    steffen klatt (created on: apr 18, 2014)
 */

#include  "io/data-provider/ireaders/sqlite3/reader_sqlite3.h"
#include  "io/outputtypes.h"

#include  "string/lstring-compare.h"

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"

namespace ldndc {
/***  INPUT LANDSCAPE READER SQLITE3 BASE  ***/
iproc_landscape_reader_sqlite3_base_t::iproc_landscape_reader_sqlite3_base_t()
        : iproc_landscape_reader_base_t()
{
}
iproc_landscape_reader_sqlite3_base_t::~iproc_landscape_reader_sqlite3_base_t()
{
}

/***  INPUT LANDSCAPE READER SQLITE3  ***/
iproc_landscape_reader_sqlite3_t::iproc_landscape_reader_sqlite3_t()
        : iproc_landscape_reader_sqlite3_base_t(),
          fhandler( NULL),
          status_( LDNDC_ERR_OK)
{
    handle.db = NULL;
}


iproc_landscape_reader_sqlite3_t::~iproc_landscape_reader_sqlite3_t()
{
}


int
iproc_landscape_reader_sqlite3_t::scan_stream(
        iobject_handler_base_t *  _fhandler)
{
    bool const  is_rescan = this->fhandler != NULL;

    if ( !is_rescan)
    {
        crabmeat_assert( _fhandler);
        this->fhandler = dynamic_cast< iobject_handler_sqlite3_t * >( _fhandler);
    }

    if ( !this->fhandler->is_open() && this->fhandler->info()->is_mutable())
    {
        return  0;
    }

    /* scan stream and store handles */
    this->handle = this->fhandler->get_iobject_handle();

    return  this->scan_stream( is_rescan);
}
int
iproc_landscape_reader_sqlite3_t::rescan_stream()
{
    if ( !this->fhandler)
    {
        return  -1;
    }
    int const  rc_reload = this->fhandler->reload();
    if ( rc_reload)
    {
        return  -1;
    }

    return  this->scan_stream( this->fhandler);
}


size_t
iproc_landscape_reader_sqlite3_t::number_of_blocks()
const
{
    return  this->ireaders.size();
}
bool
iproc_landscape_reader_sqlite3_t::check_input_exists(
        lid_t const &  _block_id)
const
{
    return  this->ireaders.check_input_exists( _block_id);
}

int
iproc_landscape_reader_sqlite3_t::get_available_blocks(
        lid_t  _list[], size_t  _max_size)
const
{
    if ( !this->handle.db)
    {
        return  -1;
    }
    return  this->ireaders.get_available_blocks( _list, _max_size);
}


iproc_provider_t *
iproc_landscape_reader_sqlite3_t::acquire_ireader(
        lid_t const &  _id)
{
    return  static_cast< iproc_provider_t * >(
            this->ireaders.acquire_ireader( _id, this->input_class_type(), this->io_format_type()));
}
void
iproc_landscape_reader_sqlite3_t::release_ireader(
        lid_t const &  _id)
{
    this->ireaders.release_ireader( _id);
}

lerr_t
iproc_landscape_reader_sqlite3_t::add_to_processing_stack(
        iproc_provider_t **  _rdr_ptr,
        iproc_provider_t *  _iprocs)
{
// sk:TODO  provide for synthesizer only input
    if ( !_rdr_ptr)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    return  this->ireaders.add_to_processing_stack( _rdr_ptr, _iprocs);
}

iproc_landscape_reader_interface_t::input_stream_mode_e
iproc_landscape_reader_sqlite3_t::input_stream_mode()
const
{
    return  IMODE_MULTI;
}



int
iproc_landscape_reader_sqlite3_t::collective_update_commence(
        cbm::sclock_t const *)
{
    if ( this->fhandler && !this->fhandler->is_open())
    {
        CBM_LogDebug( "rescanning stream  [",*this->fhandler->info(),"]");
        int  rc_rescan = this->rescan_stream();
        if ( rc_rescan < 0)
        {
            return  -1;
        }
    }
    return  0;
}
int
iproc_landscape_reader_sqlite3_t::collective_update_complete(
        cbm::sclock_t const *)
{
    return  0;
}


}
extern "C"
int
__iproc_landscape_reader_sqlite3_t_configure(
        void *  _obj, int  _n_col, char **  _col_values, char**  /*column names*/)
{
    if ( _n_col == 0)
    {
        return  0;
    }
    if ( _n_col != 1)
    {
        return  -1;
    }
    ldndc::iproc_landscape_reader_sqlite3_t *  sqlite3_ireader =
        reinterpret_cast< ldndc::iproc_landscape_reader_sqlite3_t * >( _obj);
    return  sqlite3_ireader->configure_node( atoi( *_col_values));
}
namespace ldndc {
int
iproc_landscape_reader_sqlite3_t::configure_node(
        int  _id)
{
    iproc_ireader_container_t< iproc_landscape_reader_sqlite3_t >::node_info_t  ni;
    ni.id = _id;
    ni.handle = this->handle;

        return  this->ireaders.configure_node( ni);
}
}
extern "C"
int
__iproc_landscape_reader_sqlite3_t_reconfigure( 
        void *  _obj, int  _n_col, char **  _col_values, char**  /*column names*/)
{
    if ( _n_col == 0)
    {
        return  0;
    }
    if ( _n_col != 1)
    {
        return  -1;
    }
    ldndc::iproc_landscape_reader_sqlite3_t *  sqlite3_ireader =
        reinterpret_cast< ldndc::iproc_landscape_reader_sqlite3_t * >( _obj);
    return  sqlite3_ireader->reconfigure_node( atoi( *_col_values));
}
namespace ldndc {
int
iproc_landscape_reader_sqlite3_t::reconfigure_node(
        int  _id)
{
    iproc_ireader_container_t< iproc_landscape_reader_sqlite3_t >::node_info_t  ni;
    ni.id = _id;
    ni.handle = this->handle;

    return  this->ireaders.reconfigure_node( ni);
}
int
iproc_landscape_reader_sqlite3_t::scan_stream(
        bool  _is_rescan)
{
    CBM_LogDebug( "iproc_landscape_reader_sqlite3_t::scan_stream(): ",
            (_is_rescan?"re":""),"scan stream");
    this->ireaders.invalidate_node_handles();

    std::stringstream  ID_query_s;
    ID_query_s << "SELECT DISTINCT " << io::record_meta_ids[io::CLIENT_ID] << " FROM " << this->handle.tbl << ";";
    std::string const  ID_query( ID_query_s.str());
    sqlite3_callback  ID_callback =
        ( _is_rescan) ? __iproc_landscape_reader_sqlite3_t_reconfigure : __iproc_landscape_reader_sqlite3_t_configure;
    int  rc_exec = sqlite3_exec(
            this->handle.db, ID_query.c_str(), ID_callback, (void*)this, NULL);
    if ( rc_exec)
    {
        CBM_LogError( "iproc_landscape_reader_sqlite3_t::scan_stream(): ",
                "failed to scan database for IDs  [tbl=",this->handle.tbl,",source=",*this->fhandler->info(),"]");
        CBM_LogError( "error message: ",sqlite3_errmsg( this->handle.db));
        return  -1;
    }

    CBM_LogDebug( "iproc_landscape_reader_sqlite3_t::scan_stream(): ", "#blocks=",this->ireaders.size());
    return  this->ireaders.size();
}



/***  INPUT READER SQLITE3 BASE  ***/
iproc_reader_sqlite3_base_t::iproc_reader_sqlite3_base_t()
        : iproc_reader_base_t()
{
}
iproc_reader_sqlite3_base_t::~iproc_reader_sqlite3_base_t()
{
}

/***  INPUT READER SQLITE3  ***/

iproc_reader_sqlite3_t::iproc_reader_sqlite3_t()
        : iproc_reader_sqlite3_base_t()
{
}
iproc_reader_sqlite3_t::~iproc_reader_sqlite3_t()
{
}

lerr_t
iproc_reader_sqlite3_t::set_handle(
        iproc_reader_sqlite3_t::node_handle_t const &  _block_handle)
{
    this->handle = _block_handle;
    this->data_arrived = ( _block_handle.db) ? 1 : 0;

    return  LDNDC_ERR_OK;
}

} /* namespace ldndc */

