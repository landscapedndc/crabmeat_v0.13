/*!
 * @brief
 *    sqlite3 input readers
 *
 * @author
 *    steffen klatt (created on: apr 18, 2014)
 */

#ifndef  LDNDC_IO_IREADER_SQLITE3_H_
#define  LDNDC_IO_IREADER_SQLITE3_H_

#include  "io/data-provider/ireaders/ireader.h"
#include  "io/fhandler/sqlite3/fhandler_sqlite3.h"

#include  "io/data-provider/ireaders/ireader-container.h"
#include  "time/cbm_time.h"

namespace ldndc {

/* ###   SQLITE3 INPUT READER   ### */
class  CBM_API  iproc_reader_sqlite3_base_t  :  public  iproc_reader_base_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_sqlite3_t::format(); }
    protected:
        iproc_reader_sqlite3_base_t();
        virtual  ~iproc_reader_sqlite3_base_t() = 0;
};

class  CBM_API  iproc_reader_sqlite3_t  :  public  iproc_reader_sqlite3_base_t
{
    public:
        typedef  ioproc_handle_sqlite3_t  node_handle_t;
        lerr_t  set_handle(
                node_handle_t const &);

    protected:
        iproc_reader_sqlite3_t();
        virtual  ~iproc_reader_sqlite3_t() = 0;

        node_handle_t  handle;
};


/* ###  SQLITE3 INPUT LANDSCAPE READER  ### */
class  CBM_API  iproc_landscape_reader_sqlite3_base_t  :  public  iproc_landscape_reader_base_t
{
    public:
        char const *  io_format_type() const
            { return  iobject_handler_sqlite3_t::format(); }

    protected:
        iproc_landscape_reader_sqlite3_base_t();
        virtual  ~iproc_landscape_reader_sqlite3_base_t() = 0;
};


class  CBM_API  iproc_landscape_reader_sqlite3_t  :  public  iproc_landscape_reader_sqlite3_base_t
{
    public:
        typedef  iproc_reader_sqlite3_t  ireader_t;
        typedef  ioproc_handle_sqlite3_t  node_handle_t;

        iproc_landscape_reader_interface_t::input_stream_mode_e  input_stream_mode() const;
    public:
        lerr_t  status() const { return  this->status_; }

        virtual  int  scan_stream(
                iobject_handler_base_t * /*i/o format handler*/);
        virtual  int  rescan_stream();

        virtual  iproc_provider_t *  acquire_ireader(
                lid_t const & /*block id*/);
        virtual  void  release_ireader(
                lid_t const & /*block id*/);

        virtual  lerr_t  add_to_processing_stack(
                iproc_provider_t ** /*address of pointer to reader*/,
                iproc_provider_t * /*data processors*/);

    public:
        virtual  size_t  number_of_blocks() const;
        virtual  bool    check_input_exists(
                lid_t const & /*block id*/) const;

        virtual  int  get_available_blocks(
                lid_t [], size_t) const;

    public:
        virtual  int  collective_update_commence(
                cbm::sclock_t const *);
        virtual  int  collective_update_complete(
                cbm::sclock_t const *);

    protected:
        iproc_landscape_reader_sqlite3_t();
        virtual  ~iproc_landscape_reader_sqlite3_t() = 0;

    public:
        /* return number of blocks in stream */
        size_t  size() const { return  this->number_of_blocks(); }

    protected:
        /* pointer to i/o handler */
        iobject_handler_sqlite3_t *  fhandler;
        ioproc_handle_sqlite3_t  handle;

    private:
        lerr_t  status_;

        iproc_ireader_container_t< iproc_landscape_reader_sqlite3_t >  ireaders;

    public:
        int  configure_node(
                int /*object id*/);
        int  reconfigure_node(
                int /*object id*/);
        int  scan_stream(
                bool /*initial scan(false) or rescan(true)?*/);
};

}

#endif  /*  !LDNDC_IO_IREADER_SQLITE3_H_  */

