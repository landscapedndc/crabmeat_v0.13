
#ifndef  LDNDC_IO_IREADER_ANYCLASS_SQLITE3_CPP_
#define  LDNDC_IO_IREADER_ANYCLASS_SQLITE3_CPP_

#include  "io/data-provider/ireaders/sqlite3/reader_sqlite3_anyclass.h"
#include  "log/cbm_baselog.h"
#include  "io/outputtypes.h"

namespace ldndc {

LDNDC_SERVER_OBJECT_DEFN(iproc_reader_anyclass_sqlite3_t)
iproc_reader_anyclass_sqlite3_t::iproc_reader_anyclass_sqlite3_t()
        : iproc_reader_sqlite3_t(),
          iproc_interface_anyclass_t()
{
    this->data_arrived = 1;
}
iproc_reader_anyclass_sqlite3_t::~iproc_reader_anyclass_sqlite3_t()
{
}

// sk:dbg extern "C"
// sk:dbg int
// sk:dbg __n2o_f(
// sk:dbg         void *, int  _n_col, char **  _col_values, char**  /*column names*/)
// sk:dbg {
// sk:dbg     if ( _n_col == 0) return  -1;
// sk:dbg     for ( int  i = 0;  i < _n_col;  ++i)
// sk:dbg     {
// sk:dbg         CBM_LogDebug( "n2o[",i,"]=",_col_values[i]);
// sk:dbg     }
// sk:dbg     return  0;
// sk:dbg }


int
iproc_reader_anyclass_sqlite3_t::read_by_keys(
        void **  _data, sink_entity_layout_t const *  _meta)
const
{
    if ( !_data || !_meta)
    {
        CBM_LogError( "iproc_reader_anyclass_sqlite3_t::read_by_keys(): ",
                "invalid arguments");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    lerr_t  rc_read = LDNDC_ERR_OK;

// sk:dbg    sqlite3_exec( this->handle.db, "SELECT n2o FROM solutessink WHERE id=0 AND timestep=0;", __n2o_f, NULL, NULL);

    sqlite3_stmt *  sql_stmt = NULL;
    int  rc_prepare = this->prepare( &sql_stmt, _meta, 0 /*FIXME  ==t*/);
    if ( rc_prepare)
    {
        return  LDNDC_ERR_FAIL;
    }
    CBM_LogDebug( "column-count=",sqlite3_column_count( sql_stmt));

    int const  r_end = _meta->layout.rank;
    int  e = 0;
    int  j = 0;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = _meta->layout.types[r];
        int const  this_size = _meta->layout.sizes[r];

        void *  c_data = _data[r];
        if ( !c_data)
        {
            continue;
        }

        for ( int  ent = 0;  ent < this_size;  ++ent, ++e)
        {
            char const *  entityid = _meta->ents.ids[e];
            if ( !entityid)
            {
                continue;
            }
            int const  v_size = ( _meta->layout.entsizes) ? _meta->layout.entsizes[e] : 1;
            if ( v_size == 0)
            {
                continue;
            }

            int  n_val = 0;
            int  rc_step = SQLITE_OK;

            if ( this_datatype == LDNDC_FLOAT64)
            {
                ldndc_flt64_t *  c_ptr = (ldndc_flt64_t *)c_data;
                for ( j = 0;  ( rc_step != SQLITE_DONE) && ( j < v_size);  ++j, ++c_ptr, ++n_val)
                {
                    CBM_LogDebug( "reading item ",(void*)c_ptr, " for ",entityid, "  recsize=",v_size);
                    rc_step = sqlite3_step( sql_stmt);
                    if ( rc_step != SQLITE_ROW && rc_step != SQLITE_DONE)
                    {
                        CBM_LogError( "iproc_reader_anyclass_sqlite3_t::read_by_keys(): ",
                               "failed to read column  [column=",_meta->ents.ids[e],"]\nSQL error: ", sqlite3_errmsg( this->handle.db));
                        CBM_LogDebug( "value=",sqlite3_column_double( sql_stmt, e));
                        break;
                    }
                    else if ( rc_step == SQLITE_ROW)
                    {
                        *c_ptr = sqlite3_column_double( sql_stmt, e);
                        CBM_LogDebug( "assigning ", *c_ptr);
                    }
                }
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_INT32)
            {
                ldndc_int32_t *  c_ptr = (ldndc_int32_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_UINT32)
            {
                ldndc_uint32_t *  c_ptr = (ldndc_uint32_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_FLOAT32)
            {
                ldndc_flt32_t *  c_ptr = (ldndc_flt32_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_STRING)
            {
                ldndc_string_t *  c_ptr = (ldndc_string_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_INT64)
            {
                ldndc_int64_t *  c_ptr = (ldndc_int64_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_UINT64)
            {
                ldndc_uint64_t *  c_ptr = (ldndc_uint64_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_BOOL)
            {
                ldndc_bool_t *  c_ptr = (ldndc_bool_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else if ( this_datatype == LDNDC_CHAR)
            {
                ldndc_char_t *  c_ptr = (ldndc_char_t *)c_data;
                c_data = c_ptr + v_size;
            }
            else
            {
                CBM_LogError( "iproc_reader_anyclass_sqlite3_t::read_by_keys(): ",
                        "datatype not supported  [entity=",entityid,",datatype=",this_datatype,"]");
                n_val = -1;
            }

            if ( n_val < 0)
            {
                CBM_LogError( "iproc_reader_anyclass_sqlite3_t::read_by_keys(): ",
                        "failed to convert data  [entity=",entityid,"]");
                rc_read = LDNDC_ERR_FAIL;
            }
            else if ( n_val != v_size)
            {
                CBM_LogWarn( "iproc_reader_anyclass_sqlite3_t::read_by_keys(): ",
                        "incomplete data line for key  [entity=",entityid,"]");
            }
        }
    }

    sqlite3_finalize( sql_stmt);
    return  rc_read;
}

int
iproc_reader_anyclass_sqlite3_t::prepare(
        sqlite3_stmt **  _stmt, sink_entity_layout_t const *  /*_meta*/, int  /*_t*/)
const
{
    // TODO  build from entity layout
    std::string  stmt_query = "SELECT n2o FROM "+this->handle.tbl+" WHERE id="+cbm::n2s( this->object_id())+" AND timestep=0;";
    int  rc_stmt = sqlite3_prepare_v2(
            this->handle.db, stmt_query.c_str(), stmt_query.size(), _stmt, NULL);
    CBM_LogDebug( "query-stmt=",stmt_query);
    if ( rc_stmt != SQLITE_OK)
    {
        CBM_LogError( "iproc_reader_anyclass_sqlite3_t::prepare(): ",
                "failed to prepare sql statement\nSQL error: ",sqlite3_errmsg(this->handle.db));
        return  -1;
    }

// sk:off    int  rc_bind = sqlite3_bind_int( _stmt, 1, _t);
// sk:off    if ( rc_bind != SQLITE_OK)
// sk:off           {
// sk:off        CBM_LogError( "iproc_reader_anyclass_sqlite3_t::prepare(): ",
// sk:off                "failed to bind sql statement\nSQL error: ", sqlite3_errmsg(this->handle.db));
// sk:off        sqlite3_finalize( _stmt);
// sk:off        return  -1;
// sk:off    }

    return  0;
}


LDNDC_SERVER_OBJECT_DEFN(iproc_landscape_reader_anyclass_sqlite3_t)
iproc_landscape_reader_anyclass_sqlite3_t::iproc_landscape_reader_anyclass_sqlite3_t()
        : iproc_landscape_reader_sqlite3_t(),
          iproc_landscape_interface_anyclass_t()
{
}


iproc_landscape_reader_anyclass_sqlite3_t::~iproc_landscape_reader_anyclass_sqlite3_t()
{
}

}

#endif  /*  !LDNDC_IO_IREADER_ANYCLASS_SQLITE3_CPP_  */

