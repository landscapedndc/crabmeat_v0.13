/*!
 * @brief
 *    input readers factory array
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_INPUTREADERS_H_
#define  LDNDC_IO_INPUTREADERS_H_

#include  "io/data-provider/ireaders/ireader.h"

namespace ldndc {

extern CBM_API ldndc::ioproc_factory_base_t const *
        find_ireader_factory( char /*kind {'L','C'}*/,
                char const * /*format*/, char const * /*role/class*/);

} /* namespace ldndc */


#endif  /*  !LDNDC_IO_INPUTREADERS_H_  */

