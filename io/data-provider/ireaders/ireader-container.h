/*!
 * @brief
 *    input reader container
 *
 * @author
 *    steffen klatt (created on: jan 31, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_IO_IREADERCONTAINER_H_
#define  LDNDC_IO_IREADERCONTAINER_H_

#include  "crabmeat-common.h"
#include  "io/data-provider/ireaders/ireader.h"

#include  "openmp/cbm_omp.h"

#include  <map>

namespace ldndc {

template < typename  _lireader_t >
class  CBM_API  iproc_ireader_container_t
{
    typedef  _lireader_t  landscape_ireader_t;
    typedef  typename _lireader_t::ireader_t  ireader_t;
    typedef  typename _lireader_t::node_handle_t  landscape_node_handle_t;
    typedef  typename _lireader_t::ireader_t::node_handle_t  node_handle_t;
    public:
        struct  node_info_t
        {
            lid_t  id;
            node_handle_t  handle;
        };

    public:
        iproc_ireader_container_t();
        ~iproc_ireader_container_t();

        void  lock( char const * = NULL /*lock name*/);
        void  unlock( char const * = NULL /*lock name*/);

    public:
        iproc_provider_t *  acquire_ireader( lid_t const & /*block id*/,
            char const * /*type*/, char const * /*format*/);
        void  release_ireader(
            lid_t const & /*block id*/);

        size_t  size() const;
        size_t  block_size() const;
        size_t  reader_size() const;

        bool  check_input_exists(
            lid_t const & /*block id*/) const;
        int  get_available_blocks(
            lid_t [] /*block id buffer*/, size_t /*size of buffer*/) const;

    public:
        lerr_t  add_to_processing_stack(
            iproc_provider_t ** /*address of pointer to reader*/,
            iproc_provider_t * /*data processors*/);

        int  configure_node( node_info_t &);
        int  reconfigure_node( node_info_t &);

        void  invalidate_node_handles();

    private:
        typedef  std::map< lid_t, node_info_t >  node_info_container_t;
        typedef  std::pair< typename node_info_container_t::iterator, bool >  node_info_container_insert_return_t;
        node_info_container_t  node_infos_;

        typedef  std::map< lid_t, iproc_provider_t * >  node_ireader_container_t;
        typedef  std::pair< typename node_ireader_container_t::iterator, bool >  node_ireader_container_insert_return_t;
        node_ireader_container_t  node_ireaders_;

        cbm::omp::omp_lock_t  lock_;
        template < typename  _T >
            _T  unlock_and_return_( _T  _ret_value,
                char const *  _name = NULL /*lock name*/)
        {
            this->unlock( _name);
            return  _ret_value;
        }

        int  insert_node_info_( node_info_t &);
};

}

#include  "io/data-provider/ireaders/ireader-container.inl"

#endif  /*  !LDNDC_IO_IREADERCONTAINER_H_  */

