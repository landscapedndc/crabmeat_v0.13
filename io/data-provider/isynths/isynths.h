/*!
 * @brief
 *    input synthesizer factory array
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#ifndef  CBM_IO_ISYNTHESIZERS_H_
#define  CBM_IO_ISYNTHESIZERS_H_

#include  "io/data-provider/isynths/isynth.h"

namespace ldndc {

extern  ldndc::ioproc_factory_base_t const CBM_API *
    find_isynthesizer_factory( char const * /*role/class*/);

} /* namespace ldndc */

#endif  /*  !CBM_IO_ISYNTHESIZERS_H_ */

