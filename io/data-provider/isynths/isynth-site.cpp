
/*  *****  SITE CORE DATA SYNTHESIZER  *****  */
#include  "io/data-provider/isynths/isynth-site.h"

namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(iproc_isynthesizer_site_t)
iproc_isynthesizer_site_t::iproc_isynthesizer_site_t()
        : iproc_isynthesizer_base_t(),
          iproc_interface_site_t(),
          rdr_( NULL)
{
}
iproc_isynthesizer_site_t::~iproc_isynthesizer_site_t()
{
    if ( this->rdr_)
    {
        dynamic_cast< iproc_provider_t * >( this->rdr_)->delete_instance();
    }
}

iproc_reader_base_t *
iproc_isynthesizer_site_t::to_reader()
{
    return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
}
void
iproc_isynthesizer_site_t::set_provider(
        iproc_provider_t *  _rdr)
{
    crabmeat_assert( _rdr);
    this->rdr_ = dynamic_cast< iproc_interface_site_t * >( _rdr);
}
}

/***  general -> location  ***/
#include  "site/sitetypes.h"
#include  "utils/cbm_utils.h"

namespace ldndc {
/***  general -> misc  ***/

double
iproc_isynthesizer_site_t::get_saturated_hydraulic_conductivity_bottom()
const
{
    return  this->rdr_->get_saturated_hydraulic_conductivity_bottom();
}

double
iproc_isynthesizer_site_t::get_water_table_depth()
const
{
    double  watertable = this->rdr_->get_water_table_depth();
    if ( watertable < 0.0)
    {
        CBM_LogVerbose( "site data: reset invalid water table [old=",watertable,",new=",site::site_info_defaults.watertable,"]");
        watertable = site::site_info_defaults.watertable;
    }
    return  watertable;
}


/* soil -> general */
double
iproc_isynthesizer_site_t::get_soil_organic_carbon_content_05()
const
{
        return  this->rdr_->get_soil_organic_carbon_content_05();
}

double
iproc_isynthesizer_site_t::get_soil_organic_carbon_content_30()
const
{
        return  this->rdr_->get_soil_organic_carbon_content_30();
}

double
iproc_isynthesizer_site_t::get_soil_litter_height()
const
{
        return  this->rdr_->get_soil_litter_height();
}

std::string
iproc_isynthesizer_site_t::get_soil_type()
const
{
    return  this->rdr_->get_soil_type();
}

std::string
iproc_isynthesizer_site_t::get_humus_type()
const
{
    return  this->rdr_->get_humus_type();
}

lerr_t
iproc_isynthesizer_site_t::soil_use_history(
        std::string *  _soil_use)
const
{
    return  this->rdr_->soil_use_history( _soil_use);
}


/* soil -> layers -> layer */
size_t
iproc_isynthesizer_site_t::get_number_of_strata()
const
{
    return  this->rdr_->get_number_of_strata();
}

lerr_t
iproc_isynthesizer_site_t::get_strata_height( double  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  this->rdr_->get_strata_height( _arr, _n, _r, _s);
}

lerr_t
iproc_isynthesizer_site_t::get_strata_splitheight( double  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  this->rdr_->get_strata_splitheight( _arr, _n, _r, _s);
}

lerr_t
iproc_isynthesizer_site_t::get_strata_split( int  _arr[], size_t  _n, int *  _r, int *  _s)
const
{
    return  this->rdr_->get_strata_split( _arr, _n, _r, _s);
}


lerr_t
iproc_isynthesizer_site_t::get_strata_properties(
        double  _buffer[], size_t  _n_strata, size_t  _n_properties,
        ldndc_string_t const  _property_names[])
const
{
    return  this->rdr_->get_strata_properties( _buffer, _n_strata, _n_properties, _property_names);
}

}

