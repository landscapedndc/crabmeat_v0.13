
/*  *****  GROUNDWATER DATA SYNTHESIZER  *****  */
#include  "io/data-provider/isynths/isynth-groundwater.h"
#include  "log/cbm_baselog.h"

namespace ldndc {
    LDNDC_SERVER_OBJECT_DEFN(iproc_isynthesizer_groundwater_t)
    iproc_isynthesizer_groundwater_t::iproc_isynthesizer_groundwater_t()
    : iproc_isynthesizer_base_t(),
    iproc_interface_groundwater_t(),
    rdr_( NULL)
    {
        this->m.incomplete_block = 0;
        this->m.exhausted = 0;
    }
    iproc_isynthesizer_groundwater_t::~iproc_isynthesizer_groundwater_t()
    {
        if ( this->rdr_)
        {
            dynamic_cast< iproc_provider_t * >( this->rdr_)->delete_instance();
        }
    }

    iproc_reader_base_t *
    iproc_isynthesizer_groundwater_t::to_reader()
    {
        return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
    }
    void
    iproc_isynthesizer_groundwater_t::set_provider(
                                                    iproc_provider_t *  _rdr)
    {
        if ( _rdr)
        {
            this->rdr_ = dynamic_cast< iproc_interface_groundwater_t * >( _rdr);
        }
        else
        {
            this->rdr_ = NULL;
        }
    }
}

#include  "groundwater/groundwatertypes.h"
#include  "utils/cbm_utils.h"
namespace ldndc {
    double
    iproc_isynthesizer_groundwater_t::get_average_no3()
    const
    {
        double  this_no3 = ( this->rdr_) ? this->rdr_->get_average_no3() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_no3))
        {
            CBM_LogVerbose( "groundwater data: reset invalid NO3",
                  "  [old=",this_no3,",new=",groundwater::groundwater_info_defaults.no3,"]");
            this_no3 = groundwater::groundwater_info_defaults.no3;
        }
        return  this_no3;
    }

    double
    iproc_isynthesizer_groundwater_t::get_average_watertable()
    const
    {
        double  this_watertable = ( this->rdr_) ? this->rdr_->get_average_watertable() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_watertable))
        {
            CBM_LogVerbose( "groundwater data: reset invalid WATERTABLE",
                  "  [old=",this_watertable,",new=",groundwater::groundwater_info_defaults.watertable,"]");
            this_watertable = groundwater::groundwater_info_defaults.watertable;
        }
        return  this_watertable;
    }

    lerr_t
    iproc_isynthesizer_groundwater_t::preset(
            char const *  _rec_item[], size_t  _rec_item_cnt, config_type *  _config)
    {
        if ( this->rdr_)
        {
            lerr_t  rc_preset = this->rdr_->preset( _rec_item, _rec_item_cnt, _config);
            if ( rc_preset)
            {
                return  rc_preset;
            }
        }

        groundwater::groundwater_info_t  b_cond;
        b_cond.no3 = this->get_average_no3();
        b_cond.watertable = this->get_average_watertable();


        crabmeat_assert( _config);
        this->m_Rk = _config->simulation_time.time_resolution();
        this->m_Rr = _config->streamdata_time.time_resolution();
        ldate_t const  t = _config->simulation_time.from();
        this->synth_time_ = ldate_t( t.year(), t.month(), t.day(), 1, 1);
        this->synth_time_0_ = this->synth_time_;
        ltime_t  t_1( this->synth_time_, this->synth_time_);
        ltime_t  t_r( t_1, t.time_resolution());
        
        this->s_r1_rk_ = synthesizer_t( &b_cond, &t_1, &t_r);
        
        return  LDNDC_ERR_OK;
    }
    
    lerr_t
    iproc_isynthesizer_groundwater_t::reset()
    {
        this->m.exhausted = 0;
        this->m.incomplete_block = 0;
        
        this->synth_time_ = this->synth_time_0_;
        
        if ( this->rdr_)
        {
            return  this->rdr_->reset();
        }
        return  LDNDC_ERR_OK;
    }
}


#define  __class__  groundwater
#define  __provider__  iproc_isynthesizer_groundwater_t
#define  __streamdata_info__  groundwater::groundwater_streamdata_info_t
#define  __datafilter_list__  groundwater::GROUNDWATER_DATAFILTER_LIST
#include  "io/data-provider/isynths/shared/isynth-streamdata-read.cpp"
#undef  __class__
#undef  __provider__
#undef  __streamdata_info__
#undef  __datafilter_list__

