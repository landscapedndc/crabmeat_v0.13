
/***  CLIMATE CORE DATA SYNTHESIZER ***/
#include  "io/data-provider/isynths/isynth-climate.h"

namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(iproc_isynthesizer_climate_t)
iproc_isynthesizer_climate_t::iproc_isynthesizer_climate_t()
        : iproc_isynthesizer_base_t(),
          iproc_interface_climate_t(),
          rdr_( NULL)
{
    this->m.incomplete_block = 0;
    this->m.exhausted = 0;
}
iproc_isynthesizer_climate_t::~iproc_isynthesizer_climate_t()
{
    if ( this->rdr_)
    {
        dynamic_cast< iproc_provider_t * >( this->rdr_)->delete_instance();
    }
}

iproc_reader_base_t *
iproc_isynthesizer_climate_t::to_reader()
{
    return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
}
void
iproc_isynthesizer_climate_t::set_provider(
        iproc_provider_t *  _rdr)
{
    if ( _rdr)
    {
        this->rdr_ = dynamic_cast< iproc_interface_climate_t * >( _rdr);
    }
    else
    {
        this->rdr_ = NULL;
    }
}
}

#include  "climate/climatetypes.h"
#include  "utils/cbm_utils.h"
namespace ldndc {
double
iproc_isynthesizer_climate_t::get_latitude()
const
{
    double  latitude = ( this->rdr_) ? this->rdr_->get_latitude() : ldndc::invalid_t< double >::value;
    if ( cbm::is_invalid( latitude) || ( latitude < -90) || ( latitude > 90.0))
    {
        CBM_LogVerbose( "climate data: reset invalid latitude  [old=",latitude,",new=",climate::climate_info_defaults.latitude,"]");
        latitude = climate::climate_info_defaults.latitude;
    }
    return  latitude;
}

double
iproc_isynthesizer_climate_t::get_longitude()
const
{
    double  longitude = ( this->rdr_) ? this->rdr_->get_longitude() : ldndc::invalid_t< double >::value;
    /* NOTE  "invalid value" is actually valid, however, what are the chances ;-) */
    if ( cbm::is_invalid( longitude) || ( longitude < -360.0) || ( longitude > 360.0))
    {
        CBM_LogVerbose( "climate data: reset invalid longitude  [old=",longitude,",new=",climate::climate_info_defaults.longitude,"]");
        longitude = climate::climate_info_defaults.longitude;
    }
    else if ( longitude < 0.0)
    {
        longitude = 360.0 + longitude;
    }
    return  longitude;
}

double
iproc_isynthesizer_climate_t::get_elevation()
const
{
    double  elevation = ( this->rdr_) ? this->rdr_->get_elevation() : ldndc::invalid_t< double >::value;
    if (( elevation < 0.0) || ( elevation > 8000.0))
    {
        CBM_LogVerbose( "climate data: reset invalid elevation  [old=",elevation,",new=",climate::climate_info_defaults.elevation,"]");
        elevation = climate::climate_info_defaults.elevation;
    }
    return  elevation;
}

double
iproc_isynthesizer_climate_t::get_cloudiness()
const
{
    double  cloudiness = ( this->rdr_) ? this->rdr_->get_cloudiness() : ldndc::invalid_t< double >::value;
    if (( cloudiness < 0.0) || ( cloudiness > 1.0))
    {
        CBM_LogVerbose( "climate data: reset invalid cloudiness  [old=",cloudiness,",new=",climate::climate_info_defaults.cloudiness,"]");
        cloudiness = climate::climate_info_defaults.cloudiness;
    }
    return  cloudiness;

}
double
iproc_isynthesizer_climate_t::get_rainfall_intensity()
const
{
    double  rainfall_intensity = ( this->rdr_) ? this->rdr_->get_rainfall_intensity() : ldndc::invalid_t< double >::value;
    if ( rainfall_intensity < 0.0)
    {
        CBM_LogVerbose( "climate data: reset invalid average rainfall intensity  [old=",rainfall_intensity,",new=",climate::climate_info_defaults.rainfall_intensity,"]");
        rainfall_intensity = climate::climate_info_defaults.rainfall_intensity;
    }
    return  rainfall_intensity;
}
double
iproc_isynthesizer_climate_t::get_wind_speed()
const
{
    double  windspeed = ( this->rdr_) ? this->rdr_->get_wind_speed() : ldndc::invalid_t< double >::value;
    if ( windspeed < 0.0)
    {
        CBM_LogVerbose( "climate data: reset invalid average wind speed  [old=",windspeed,",new=",climate::climate_info_defaults.windspeed,"]");
        windspeed = climate::climate_info_defaults.windspeed;
    }
    return  windspeed;
}

double
iproc_isynthesizer_climate_t::get_annual_precipitation()
const
{
    double  annual_precipitation = ( this->rdr_) ? this->rdr_->get_annual_precipitation() : ldndc::invalid_t< double >::value;
    if ( annual_precipitation < 0.0)
    {
        CBM_LogVerbose( "climate data: reset invalid average annual precipitation  [old=",annual_precipitation,",new=",climate::climate_info_defaults.precip_sum,"]");
        annual_precipitation = climate::climate_info_defaults.precip_sum;
    }
    return  annual_precipitation;
}
double
iproc_isynthesizer_climate_t::get_temperature_average()
const
{
    double  temperature_average = ( this->rdr_) ? this->rdr_->get_temperature_average() : ldndc::invalid_t< double >::value;
    if ( cbm::is_invalid( temperature_average))
    {
        CBM_LogVerbose( "climate data: reset invalid average temperature  [old=",temperature_average,",new=",climate::climate_info_defaults.temp,"]");
        temperature_average = climate::climate_info_defaults.temp;
    }
    return  temperature_average;
}
double
iproc_isynthesizer_climate_t::get_temperature_amplitude()
const
{
    double  temperature_amplitude = ( this->rdr_) ? this->rdr_->get_temperature_amplitude() : ldndc::invalid_t< double >::value;
    if ( temperature_amplitude < 0.0)
    {
        CBM_LogVerbose( "climate data: reset invalid temperature amplitude  [old=",temperature_amplitude,",new=",climate::climate_info_defaults.temp_amplitude,"]");
        temperature_amplitude = climate::climate_info_defaults.temp_amplitude;
    }
    return  temperature_amplitude;
}


#include  "time/cbm_time.h"
lerr_t
iproc_isynthesizer_climate_t::preset(
        char const *  _rec_item[], size_t  _rec_item_cnt, config_type *  _config)
{
    if ( this->rdr_)
    {
        lerr_t  rc_preset = this->rdr_->preset( _rec_item, _rec_item_cnt, _config);
        if ( rc_preset)
        {
            return  rc_preset;
        }
    }

    CBM_LogVerbose( "kernel ",this->object_id(),
        " runs in synthesize mode  [class=",this->input_class_type(),"]");

    /* boundary conditions */
    climate::climate_info_t  b_cond;
    b_cond.elevation = this->get_elevation();
    b_cond.latitude = this->get_latitude();
    b_cond.longitude = this->get_longitude();

    b_cond.cloudiness = this->get_cloudiness();
    b_cond.rainfall_intensity = this->get_rainfall_intensity();

    b_cond.windspeed = this->get_wind_speed();
    b_cond.temp = this->get_temperature_average();
    b_cond.temp_amplitude = this->get_temperature_amplitude();
    b_cond.precip_sum = this->get_annual_precipitation();


    crabmeat_assert( _config);
    this->m_Rk = _config->simulation_time.time_resolution();
    this->m_Rr = _config->streamdata_time.time_resolution();
    ldate_t const  t = _config->simulation_time.from();
    this->synth_time_ = ldate_t( t.year(), t.month(), t.day(), 1, 1);
    this->synth_time_0_ = this->synth_time_;
    ltime_t  t_1( this->synth_time_, this->synth_time_);
    ltime_t  t_r( t_1, t.time_resolution());

    this->s_r1_rk_ = synthesizer_t( &b_cond, &t_1, &t_r);

    return  LDNDC_ERR_OK;
}


lerr_t
iproc_isynthesizer_climate_t::reset()
{
    this->m.exhausted = 0;
    this->m.incomplete_block = 0;

    this->synth_time_ = this->synth_time_0_;

    if ( this->rdr_)
    {
        return  this->rdr_->reset();
    }
    return  LDNDC_ERR_OK;
}
}


#define  __class__  climate
#define  __provider__  iproc_isynthesizer_climate_t
#define  __streamdata_info__  climate::climate_streamdata_info_t
#define  __datafilter_list__  climate::CLIMATE_DATAFILTER_LIST
#include  "io/data-provider/isynths/shared/isynth-streamdata-read.cpp"
#undef  __class__
#undef  __provider__
#undef  __streamdata_info__
#undef  __datafilter_list__

