/*!
 * @brief
 *    data synthesizing layer for inputs (implementation)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#include  "io/data-provider/isynths/isynth.h"

namespace ldndc {
iproc_isynthesizer_base_t::iproc_isynthesizer_base_t()
        : iproc_provider_t()
{
}
iproc_isynthesizer_base_t::~iproc_isynthesizer_base_t()
{
}
}

