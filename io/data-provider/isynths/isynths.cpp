/*!
 * @brief
 *    input synthesizer factory (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#include  "io/data-provider/isynths/isynths.h"

#include  "io/data-provider/isynths/isynth-airchemistry.h"
static ldndc::ioproc_factory_t< ldndc::iproc_isynthesizer_airchemistry_t >  isynthesizer_airchemistry_factory;
#include  "io/data-provider/isynths/isynth-climate.h"
static ldndc::ioproc_factory_t< ldndc::iproc_isynthesizer_climate_t >  isynthesizer_climate_factory;
#include  "io/data-provider/isynths/isynth-groundwater.h"
static ldndc::ioproc_factory_t< ldndc::iproc_isynthesizer_groundwater_t >  isynthesizer_groundwater_factory;
#include  "io/data-provider/isynths/isynth-site.h"
static ldndc::ioproc_factory_t< ldndc::iproc_isynthesizer_site_t >  isynthesizer_site_factory;

struct  isynthesizer_factory_item_t
{
    ldndc::ioproc_factory_base_t const *  factory;
    char const *  role;
};

static isynthesizer_factory_item_t const  __cbm_isynthesizers_factories[] =
{
    { &isynthesizer_airchemistry_factory, "airchemistry" },
    { &isynthesizer_climate_factory, "climate" },
    { &isynthesizer_groundwater_factory, "groundwater" },
    { &isynthesizer_site_factory, "site" },

    { NULL, NULL} /* sentinel */
};

ldndc::ioproc_factory_base_t const *  ldndc::find_isynthesizer_factory(
                char const *  _role)
{
    if ( !_role)
        { return  NULL; }

    int  r = 0;
    while ( __cbm_isynthesizers_factories[r].factory)
    {
        if ( cbm::is_equal( __cbm_isynthesizers_factories[r].role, _role))
            { return  __cbm_isynthesizers_factories[r].factory; }
        ++r;
    }
    return  NULL;
}

