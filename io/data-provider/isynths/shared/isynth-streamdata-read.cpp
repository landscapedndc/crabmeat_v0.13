
#include  "containers/linkarray.h"
#include  "datafilters/aggregator.h"
namespace ldndc {
lerr_t
__provider__::synthesize_r1_rk_(
        element_type *  _b1, int  _r1,
        element_type *  _br, int  _rr,
        size_t  _n1, size_t  _rec_size)
{
    /* :-) */
    size_t const  rr = _rr/_r1;

    ldate_t  s_t = this->synth_time_;
    lperiod_t  r1 = lperiod_one_day * _r1;

    element_type *  b1 = _b1;
    element_type *  br = _br;

    for ( size_t  r = 0;  r < _n1;  ++r)
    {
        for ( size_t  t = 0;  t < rr;  ++t)
        {
// sk:dbg            CBM_LogDebug( "synth-time=",s_t.to_string());
            /* subday update */
            lerr_t  rc_conf = this->s_r1_rk_.configure( &s_t, static_cast< unsigned int >( t)+1);
            if ( rc_conf)
            {
                return  rc_conf;
            }

            lerr_t  rc_synth = this->s_r1_rk_.synthesize_record( br, br, b1, b1, &s_t);
            if ( rc_synth)
            {
                return  rc_synth;
            }

            if ( br)
                { br += _rec_size; }
        }

        s_t += r1;

        b1 += _rec_size;
    }

// sk:dbg    CBM_LogDebug( "done r1/rr synthesizing  [r1=",_r1,",rr=",_rr,"]");
    return  LDNDC_ERR_OK;
}

#include  "utils/cbm_utils.h"
lerr_t
__provider__::read(
        element_type  _rec[], size_t  _rec_cnt, size_t  _rec_size,
        char const *  _rec_item[], size_t  _rec_item_cnt,
        int  _offset, int  /*_stride*/,
        int *  _r, int * _s)
{
    if ( _rec_cnt == 0)
    {
        if ( _r) { *_r = 0; }
        if ( _s) { *_s = 0; }
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_synth = LDNDC_ERR_OK;

    ldndc::linkarray_operations_t< __streamdata_info__ >  rec_ops( _rec_size, ldndc::linkarray_operations_t< __streamdata_info__ >::LINKARRAY_SIZE_FINAL|ldndc::linkarray_operations_t< __streamdata_info__ >::LINKARRAY_LINK_RECORDS);
    ldndc::linkarray_operations_t< __streamdata_info__ >::buffer_header_t  r_hdr;
    rec_ops.read_buffer_header( &r_hdr, _rec);

    int  Rr = r_hdr.hx.res;
    if ( Rr < -1)
    {
        CBM_LogError( "sorry, we are currently not able to handle climate input data with supdaily resolution .. ?!?");
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    Rr = ( Rr < 0) ? -Rr : Rr;
    crabmeat_assert(Rr==this->m_Rr);

    /* get data from stream if reader exists */
    int  r = 0;
    int  r_get = ((signed int)_rec_cnt/Rr)*Rr;
    if ( r_get == 0)
    {
        CBM_LogError( "climate synthesizer: buffer does not have sufficient space  [size=",_rec_cnt,",min=",Rr,"]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    if ( _offset < 0)
    {
        if ( Rr > -_offset)
        {
            CBM_LogError( "PENG..");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        r = std::min( (-_offset/Rr)*Rr, r_get);
// sk:dbg        CBM_LogDebug( "synth-load  Rr=",Rr, "  #records=",r, " (",r_get,")  req=",_rec_cnt, "  offset=",_offset);
    }
    else if ( this->rdr_ && !this->m.exhausted)
    {
        lerr_t  rc_read = this->rdr_->read( _rec, (size_t)r_get, _rec_size, _rec_item, _rec_item_cnt, 0, 1, &r, _s);
        if ( rc_read || r == 0)
        {
            if ( _r)
                { *_r = 0; }
            this->m.exhausted = 1;
            return  rc_read;
        }
        if (( r % Rr) != 0)
        {
            if ( this->m.incomplete_block == 1)
            {
                CBM_LogError( "reading non-blocksize chunks not supported currently");
                if ( _r)
                    { *_r = r; }
                return  LDNDC_ERR_FAIL;
            }
            this->m.incomplete_block = 1;
        }
    }
    else
    {
        r = r_get;
    }
    if ( _r)
    {
        *_r = r;
    }
// sk:dbg    CBM_LogWarn( STRINGIFY(__provider__),": read records  [r=",r,",requested=",_rec_cnt,",offs=",_offset,"]");

    /* NOTE  we only synthesize for R1, Rk leaving Rr in its original state (unless Rr==R1 or Rr==Rk) */

    ldndc::linkarray_operations_t< __streamdata_info__ >::buffer_header_t  b_hdr;

    /* find Rr (stream data resolution) */
    element_type *  br = _rec; /* by definition.. :-| */

    /* find R1*/
    element_type *  b1 = NULL;
    if ( Rr == 1)
        { b1 = br; }
    else
    {
        int  Rj = Rr;
        element_type *  bi = br;
        element_type *  bj = br;
        while ( Rj != 1)
        {
            rec_ops.find_lower( &bj, bi);

            if ( !bj)
                { break; }

            rec_ops.read_buffer_header( &b_hdr, bj);
            Rj = b_hdr.hx.res;

            bi = bj;
        }

        if ( !bj)
        {
            CBM_LogFatal( "no R1 buffer available .. NOT allocating temporary  [exists by definition!]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        else
        {
// sk:dbg            CBM_LogDebug( "R1 buffer found. using it.");
            b1 = bj;
        }
    }
    crabmeat_assert(b1);

    /* find bk (for simulation time resolution Rk) */
    int  Rk = this->m_Rk;
    element_type *  bk = NULL;
    if ( Rk == 1)
        { bk = b1; }
    else if ( Rk == Rr)
        { bk = br; }
    else
    {
        int  Rj = Rr;
        element_type *  bi = b1;
        element_type *  bj = b1;
        while ( Rj != Rk)
        {
            rec_ops.find_higher( &bj, bi);

            if ( !bj)
                { break; }

            rec_ops.read_buffer_header( &b_hdr, bj);
            Rj = b_hdr.hx.res;

            bi = bj;
        }

        if ( !bj)
        {
            CBM_LogFatal( "no Rk buffer available");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        else
        {
// sk:dbg            CBM_LogDebug( "Rk buffer found. using it.");
            bk = bj;
        }
    }
    crabmeat_assert(bk);

// sk:dbg    rec_ops.read_buffer_header( &b_hdr, b1);
// sk:dbg    CBM_LogError( "|b1|=",b_hdr.capacity, " R1=",b_hdr.hx.res, "  buf=",(void*)b1);
// sk:dbg    rec_ops.read_buffer_header( &b_hdr, bk);
// sk:dbg    CBM_LogError( "|bk|=",b_hdr.capacity, " Rk=",b_hdr.hx.res, "  buf=",(void*)bk);
// sk:dbg    rec_ops.read_buffer_header( &b_hdr, br);
// sk:dbg    CBM_LogError( "|br|=",b_hdr.capacity, " Rr=",b_hdr.hx.res, "  buf=",(void*)br);

    data_aggregate_t< element_type >  aggr( __datafilter_list__, __class__::record::RECORD_SIZE);

    if ( _offset < 0)
    {
        /* no aggregate */
// sk:dbg        CBM_LogDebug( "no input data available");
    }
    else
    {
    /* aggregate */
    if ( Rr == Rk)
    {
        crabmeat_assert(br==bk);
        if ( Rk != 1)
        {
            crabmeat_assert(bk!=b1);
// sk:dbg            CBM_LogDebug( "aggregate  [",Rk," -> ",1,"]", "  rk=",r/Rk, " r1=",r/Rr);
            rc_synth = aggr.filter( r/Rr, b1, _rec_size, br, _rec_size, Rr);
            if ( rc_synth)
                { return  rc_synth; }
        }
    }
    else if ( Rr > Rk)
    {
        crabmeat_assert(br!=bk);
        /* aggregate Rr->Rk */
// sk:dbg        CBM_LogDebug( "aggregate  [",Rr," -> ",Rk,"]", "  rr=",r, " rk=",r/(Rr/Rk));
        rc_synth = aggr.filter( r/(Rr/Rk), bk, _rec_size, br, _rec_size, Rr/Rk);
        if ( rc_synth)
            { return  rc_synth; }
        if ( Rk != 1)
        {
            crabmeat_assert(bk!=b1);
// sk:dbg            CBM_LogDebug( "aggregate  [",Rk," -> ",1,"]", "  rk=",r/Rk, " r1=",r/Rr);
            rc_synth = aggr.filter( r/Rr, b1, _rec_size, bk, _rec_size, Rk);
            if ( rc_synth)
                { return  rc_synth; }
        }
    }
    else if ( Rr < Rk)
    {
        crabmeat_assert(br!=bk);
        /* aggregate Rr->R1 */
        if ( Rr != 1)
        {
            crabmeat_assert(br!=b1);
// sk:dbg            CBM_LogDebug( "aggregate  [",Rr," -> 1]", "  rr=",r, " r1=",r/Rr);
            rc_synth = aggr.filter( r/Rr, b1, _rec_size, br, _rec_size, Rr);
            if ( rc_synth)
                { return  rc_synth; }
        }
        /* clear bk */
// sk:dbg        CBM_LogDebug( "clear bk  [",(r*Rk)/Rr,"]");
        rec_ops.mem_set( bk, (r*Rk)/Rr, ldndc::invalid_t< element_type >::value);
    }
    else
    {
        /* i'll be damned.. */
        crabmeat_assert(0);
    }
    }

    /* synthesize */
    if ( b1 == bk)
    {
// sk:dbg        CBM_LogDebug( "synthesize  [",1,"|",Rk,",r=",r,"]");
        /* synthesize (gapfill) for R1 */
        rc_synth = this->synthesize_r1_rk_( b1, 1, NULL, 1, r/Rr, _rec_size);
        if ( rc_synth)
            { return  rc_synth; }
    }
    else
    {
// sk:dbg        CBM_LogDebug( "synthesize  [",1,"|",Rk,",r=",r/Rr,"]");
        /* synthesize (gapfill) for R1, Rk */
        rc_synth = this->synthesize_r1_rk_( b1, 1, bk, Rk, r/Rr, _rec_size);
        if ( rc_synth)
            { return  rc_synth; }
    }

    /* update synthesizer time */
    this->synth_time_ += lperiod_one_day * (r/Rr);

    return  rc_synth;
}
}

