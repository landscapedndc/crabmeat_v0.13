/*!
 * @brief
 *    data synthesizing layer for core readers (climate)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#ifndef  LDNDC_IO_ISYNTHESIZER_CLIMATE_H_
#define  LDNDC_IO_ISYNTHESIZER_CLIMATE_H_

#include  "io/data-provider/isynths/isynth.h"
#include  "io/iif/iif_climate.h"

#include  "synthesizers/climate/synth-climate.h"

namespace ldndc {
class  CBM_API  iproc_isynthesizer_climate_t  :  public  iproc_isynthesizer_base_t,  public  iproc_interface_climate_t
{
    LDNDC_SERVER_OBJECT(iproc_isynthesizer_climate_t)
    typedef  climate::record::item_type  element_type;
    typedef  climate::synth::lsynth_climate_t  synthesizer_t;
    public:
        char const *  input_class_type() const
            { return  iproc_interface_climate_t::input_class_type(); }
        enum
            { IS_IMPLEMENTED = 1 };
    public:
        iproc_isynthesizer_climate_t();
        virtual  ~iproc_isynthesizer_climate_t();

        iproc_reader_base_t *  to_reader();
        void  set_provider(
                iproc_provider_t *);

    public:
        double  get_latitude() const;
        double  get_longitude() const;
        double  get_elevation() const;

        double  get_cloudiness() const;
        double  get_rainfall_intensity() const;
        double  get_wind_speed() const;

        double  get_annual_precipitation() const;
        double  get_temperature_average() const;
        double  get_temperature_amplitude() const;

    public:
        lerr_t  preset(
                char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read(
                element_type [], size_t, size_t,
                char const * [], size_t,
                int, int,
                int *, int *);

    private:
        iproc_interface_climate_t *  rdr_;
    private:
        /* synthesizer time, updated after 
         * each synthesizing (synth-time
         * resolution always 1)
         */
        ldate_t  synth_time_, synth_time_0_;
        int  m_Rk, m_Rr;
        synthesizer_t  s_r1_rk_;
// sk:off        synthesizer_t  s_r1_rr_;

        struct
        {
            bool  incomplete_block:1;
            bool  exhausted:1;
        }  m;

    private:
        lerr_t  synthesize_r1_rk_(
                element_type * /*b1*/, int /*r1*/, element_type * /*br*/, int /*rr*/, size_t /*n(b1)*/, size_t /*record size*/);
};
}


#endif  /*  !LDNDC_IO_ISYNTHESIZER_CLIMATE_H_  */

