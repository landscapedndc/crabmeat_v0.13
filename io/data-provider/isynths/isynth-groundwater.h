/*!
 * @brief
 *    data synthesizing layer for core readers (air chemistry)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#ifndef  LDNDC_IO_ISYNTHESIZER_GROUNDWATER_H_
#define  LDNDC_IO_ISYNTHESIZER_GROUNDWATER_H_

#include  "io/data-provider/isynths/isynth.h"
#include  "io/iif/iif_groundwater.h"

#include  "synthesizers/groundwater/synth-groundwater.h"

namespace ldndc {
class  CBM_API  iproc_isynthesizer_groundwater_t  :  public  iproc_isynthesizer_base_t,  public  iproc_interface_groundwater_t
{
    LDNDC_SERVER_OBJECT(iproc_isynthesizer_groundwater_t)
    typedef  groundwater::record::item_type  element_type;
    typedef  groundwater::synth::lsynth_groundwater_t  synthesizer_t;
    public:
        char const *  input_class_type() const
            { return  iproc_interface_groundwater_t::input_class_type(); }
        enum
            { IS_IMPLEMENTED = 1 };
    public:
        iproc_isynthesizer_groundwater_t();
        virtual  ~iproc_isynthesizer_groundwater_t();

        iproc_reader_base_t *  to_reader();
        void  set_provider(
                iproc_provider_t *);

    public:
        /* info getters here */
        double  get_average_no3() const;
        double  get_average_watertable() const;

    public:
        lerr_t  preset(
                char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read(
                element_type [], size_t, size_t,
                char const * [], size_t,
                int, int,
                int *, int *);

    private:
        iproc_interface_groundwater_t *  rdr_;
    private:
        /* synthesizer time, updated after 
         * each synthesizing (synth-time
         * resolution always 1)
         */
        ldate_t  synth_time_, synth_time_0_;
        int  m_Rk, m_Rr;
        synthesizer_t  s_r1_rk_;
// sk:off        synthesizer_t  s_r1_rr_;

        struct
        {
            bool  incomplete_block:1;
            bool  exhausted:1;
        }  m;

    private:
        lerr_t  synthesize_r1_rk_(
                element_type * /*b1*/, int /*r1*/, element_type * /*br*/, int /*rr*/, size_t /*n(b1)*/, size_t /*record size*/);
};
}


#endif  /*  !LDNDC_IO_ISYNTHESIZER_GROUNDWATER_H_  */

