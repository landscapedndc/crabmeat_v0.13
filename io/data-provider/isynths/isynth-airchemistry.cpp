
/*  *****  AIR CHEMISTRY CORE DATA SYNTHESIZER  *****  */
#include  "io/data-provider/isynths/isynth-airchemistry.h"
#include  "log/cbm_baselog.h"

namespace ldndc {
    LDNDC_SERVER_OBJECT_DEFN(iproc_isynthesizer_airchemistry_t)
    iproc_isynthesizer_airchemistry_t::iproc_isynthesizer_airchemistry_t()
    : iproc_isynthesizer_base_t(),
    iproc_interface_airchemistry_t(),
    rdr_( NULL)
    {
        this->m.incomplete_block = 0;
        this->m.exhausted = 0;
    }
    iproc_isynthesizer_airchemistry_t::~iproc_isynthesizer_airchemistry_t()
    {
        if ( this->rdr_)
        {
            dynamic_cast< iproc_provider_t * >( this->rdr_)->delete_instance();
        }
    }

    iproc_reader_base_t *
    iproc_isynthesizer_airchemistry_t::to_reader()
    {
        return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
    }
    void
    iproc_isynthesizer_airchemistry_t::set_provider(
                                                    iproc_provider_t *  _rdr)
    {
        if ( _rdr)
        {
            this->rdr_ = dynamic_cast< iproc_interface_airchemistry_t * >( _rdr);
        }
        else
        {
            this->rdr_ = NULL;
        }
    }
}

#include  "airchemistry/airchemistrytypes.h"
#include  "utils/cbm_utils.h"
namespace ldndc {
    double
    iproc_isynthesizer_airchemistry_t::get_average_ch4()
    const
    {
        double  this_ch4 = ( this->rdr_) ? this->rdr_->get_average_ch4() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_ch4))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid CH4  [old=",this_ch4,",new=",airchemistry::airchemistry_info_defaults.ch4,"]");
            this_ch4 = airchemistry::airchemistry_info_defaults.ch4;
        }
        return  this_ch4;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_co2()
    const
    {
        double  this_co2 = ( this->rdr_) ? this->rdr_->get_average_co2() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_co2))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid CO2  [old=",this_co2,",new=",airchemistry::airchemistry_info_defaults.co2,"]");
            this_co2 = airchemistry::airchemistry_info_defaults.co2;
        }
        return  this_co2;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_nh3()
    const
    {
        double  this_nh3 = ( this->rdr_) ? this->rdr_->get_average_nh3() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_nh3))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NH3  [old=",this_nh3,",new=",airchemistry::airchemistry_info_defaults.nh3,"]");
            this_nh3 = airchemistry::airchemistry_info_defaults.nh3;
        }
        return  this_nh3;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_nh4()
    const
    {
        double  this_nh4 = ( this->rdr_) ? this->rdr_->get_average_nh4() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_nh4))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NH4  [old=",this_nh4,",new=",airchemistry::airchemistry_info_defaults.nh4,"]");
            this_nh4 = airchemistry::airchemistry_info_defaults.nh4;
        }
        return  this_nh4;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_nh4dry()
    const
    {
        double  this_nh4dry = ( this->rdr_) ? this->rdr_->get_average_nh4dry() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_nh4dry))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NH4_DRY  [old=",this_nh4dry,",new=",airchemistry::airchemistry_info_defaults.nh4dry,"]");
            this_nh4dry = airchemistry::airchemistry_info_defaults.nh4dry;
        }
        return  this_nh4dry;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_no()
    const
    {
        double  this_no = ( this->rdr_) ? this->rdr_->get_average_no() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_no))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NO  [old=",this_no,",new=",airchemistry::airchemistry_info_defaults.no,"]");
            this_no = airchemistry::airchemistry_info_defaults.no;
        }
        return  this_no;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_no2()
    const
    {
        double  this_no2 = ( this->rdr_) ? this->rdr_->get_average_no2() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_no2))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NO2  [old=",this_no2,",new=",airchemistry::airchemistry_info_defaults.no2,"]");
            this_no2 = airchemistry::airchemistry_info_defaults.no2;
        }
        return  this_no2;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_no3()
    const
    {
        double  this_no3 = ( this->rdr_) ? this->rdr_->get_average_no3() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_no3))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NO3  [old=",this_no3,",new=",airchemistry::airchemistry_info_defaults.no3,"]");
            this_no3 = airchemistry::airchemistry_info_defaults.no3;
        }
        return  this_no3;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_no3dry()
    const
    {
        double  this_no3dry = ( this->rdr_) ? this->rdr_->get_average_no3dry() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_no3dry))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid NO3_DRY  [old=",this_no3dry,",new=",airchemistry::airchemistry_info_defaults.no3dry,"]");
            this_no3dry = airchemistry::airchemistry_info_defaults.no3dry;
        }
        return  this_no3dry;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_o2()
    const
    {
        double  this_o2 = ( this->rdr_) ? this->rdr_->get_average_o2() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_o2))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid O2  [old=",this_o2,",new=",airchemistry::airchemistry_info_defaults.o2,"]");
            this_o2 = airchemistry::airchemistry_info_defaults.o2;
        }
        return  this_o2;
    }

    double
    iproc_isynthesizer_airchemistry_t::get_average_o3()
    const
    {
        double  this_o3 = ( this->rdr_) ? this->rdr_->get_average_o3() : ldndc::invalid_t< double >::value;
        if ( cbm::is_invalid( this_o3))
        {
            CBM_LogVerbose( "air chemistry data: reset invalid O3  [old=",this_o3,",new=",airchemistry::airchemistry_info_defaults.o3,"]");
            this_o3 = airchemistry::airchemistry_info_defaults.o3;
        }
        return  this_o3;
    }

    lerr_t
    iproc_isynthesizer_airchemistry_t::preset(
                                              char const *  _rec_item[], size_t  _rec_item_cnt, config_type *  _config)
    {
        if ( this->rdr_)
        {
            lerr_t  rc_preset = this->rdr_->preset( _rec_item, _rec_item_cnt, _config);
            if ( rc_preset)
            {
                return  rc_preset;
            }
        }

        airchemistry::airchemistry_info_t  b_cond;
        b_cond.ch4 = this->get_average_ch4();
        b_cond.co2 = this->get_average_co2();
        b_cond.nh3 = this->get_average_nh3();
        b_cond.nh4 = this->get_average_nh4();
        b_cond.nh4dry = this->get_average_nh4dry();
        b_cond.no = this->get_average_no();
        b_cond.no2 = this->get_average_no2();
        b_cond.no3 = this->get_average_no3();
        b_cond.no3dry = this->get_average_no3dry();
        b_cond.o2 = this->get_average_o2();
        b_cond.o3 = this->get_average_o3();


        crabmeat_assert( _config);
        this->m_Rk = _config->simulation_time.time_resolution();
        this->m_Rr = _config->streamdata_time.time_resolution();
        ldate_t const  t = _config->simulation_time.from();
        this->synth_time_ = ldate_t( t.year(), t.month(), t.day(), 1, 1);
        this->synth_time_0_ = this->synth_time_;
        ltime_t  t_1( this->synth_time_, this->synth_time_);
        ltime_t  t_r( t_1, t.time_resolution());
        
        this->s_r1_rk_ = synthesizer_t( &b_cond, &t_1, &t_r);
        
        return  LDNDC_ERR_OK;
    }
    
    lerr_t
    iproc_isynthesizer_airchemistry_t::reset()
    {
        this->m.exhausted = 0;
        this->m.incomplete_block = 0;
        
        this->synth_time_ = this->synth_time_0_;
        
        if ( this->rdr_)
        {
            return  this->rdr_->reset();
        }
        return  LDNDC_ERR_OK;
    }
}


#define  __class__  airchemistry
#define  __provider__  iproc_isynthesizer_airchemistry_t
#define  __streamdata_info__  airchemistry::airchemistry_streamdata_info_t
#define  __datafilter_list__  airchemistry::AIRCHEMISTRY_DATAFILTER_LIST
#include  "io/data-provider/isynths/shared/isynth-streamdata-read.cpp"
#undef  __class__
#undef  __provider__
#undef  __streamdata_info__
#undef  __datafilter_list__

