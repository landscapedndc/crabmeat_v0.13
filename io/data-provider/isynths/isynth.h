/*!
 * @brief
 *    data synthesizing layer for inputs (base type)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#ifndef  LDNDC_IO_ISYNTHESIZER_H_
#define  LDNDC_IO_ISYNTHESIZER_H_

#include  "io/data-provider/provider.h"

namespace ldndc {
class  CBM_API  iproc_isynthesizer_base_t  :  public  iproc_provider_t
{
    public:
        virtual  char const *  input_class_type() const = 0;
        virtual  void  set_provider( iproc_provider_t *) = 0;

        virtual  int  data_available() const { return  1; }

    protected:
        iproc_isynthesizer_base_t();
        virtual  ~iproc_isynthesizer_base_t() = 0;
};
}

#endif  /*  !LDNDC_IO_ISYNTHESIZER_H_  */

