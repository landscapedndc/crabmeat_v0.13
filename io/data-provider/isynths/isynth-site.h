/*!
 * @brief
 *    data synthesizing layer for core readers (site)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#ifndef  LDNDC_IO_ISYNTHESIZER_SITE_H_
#define  LDNDC_IO_ISYNTHESIZER_SITE_H_

#include  "io/data-provider/isynths/isynth.h"
#include  "io/iif/iif_site.h"
namespace ldndc {
class  CBM_API  iproc_isynthesizer_site_t
        :  public  iproc_isynthesizer_base_t,  public  iproc_interface_site_t
{
    LDNDC_SERVER_OBJECT(iproc_isynthesizer_site_t)
    public:
        char const *  input_class_type() const
            { return  iproc_interface_site_t::input_class_type(); }
        enum
            { IS_IMPLEMENTED = 1 };
    public:
        iproc_isynthesizer_site_t();
        virtual  ~iproc_isynthesizer_site_t();

        iproc_reader_base_t *  to_reader();
        void  set_provider( iproc_provider_t *);

    public:
        double  get_saturated_hydraulic_conductivity_bottom() const;
        double  get_water_table_depth() const;

        /* soil general section */
        double  get_soil_organic_carbon_content_05() const;
        double  get_soil_organic_carbon_content_30() const;
        double  get_soil_litter_height() const;
        std::string  get_soil_type() const;
        std::string  get_humus_type() const;

        lerr_t  soil_use_history( std::string *) const;

        /* soil layers section */
        size_t  get_number_of_strata() const;

        lerr_t  get_strata_height( double [], size_t, int *, int *) const;
        lerr_t  get_strata_splitheight( double [], size_t, int *, int *) const;
        lerr_t  get_strata_split( int [], size_t, int *, int *) const;

        lerr_t  get_strata_properties( double [] /*buffer*/,
                                       size_t /*number of strata*/,
                                       size_t /*number of properties*/,
                                       ldndc_string_t const [] /*property names*/) const;
    private:
        iproc_interface_site_t *  rdr_;
};
}


#endif  /*  !LDNDC_IO_ISYNTHESIZER_SITE_H  */

