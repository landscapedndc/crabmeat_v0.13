/*!
 * @brief
 *    interface declarations of generic data providers
 *
 * @author
 *    steffen klatt (created on: nov 16, 2013)
 */

#ifndef  LDNDC_IO_DATAPROVIDER_H_
#define  LDNDC_IO_DATAPROVIDER_H_

#include  "crabmeat-common.h"
#include  "io/pmr.h"

namespace ldndc {
class  iproc_reader_base_t;
struct  CBM_API  iproc_provider_interface_t
{
    iproc_provider_interface_t();
    virtual  ~iproc_provider_interface_t() = 0;
};
struct  CBM_API  iproc_provider_t  :  iproc_base_t
{
    virtual  iproc_reader_base_t *  to_reader() = 0;

    iproc_provider_t();
    virtual  ~iproc_provider_t() = 0;
};
class  iobject_handler_base_t;
struct  CBM_API  iproc_landscape_provider_t  :  iproc_landscape_base_t,  iproc_collective_operations_interface_t
{
    /*!
     * @brief
     *    return internal status of an object of
     *    this type (signals errors)
     */
    virtual  lerr_t  status() const = 0;

    /*!
     * @brief
     *    get a handle for a single-block data provider
     */
    virtual  iproc_provider_t *  acquire_provider(
            lid_t const & /*block id*/) = 0;
    /*!
     * @brief
     *    notify provider about unused handles
     */
    virtual  void  release_provider(
            lid_t const & /*block id*/) = 0;

    /*!
     * @brief
     *    call this function to prepare structure (e.g.
     *    find all blocks in input stream)
     */
    virtual  int  scan_stream(
            iobject_handler_base_t *) = 0;
    virtual  int  rescan_stream() = 0;

    /*!
     * @brief
     *    query number of blocks in this stream (may
     *    vary over time)
     */
    virtual  size_t  number_of_blocks() const = 0;
    /*!
     * @brief
     *    query if block with given ID exists in this stream
     */
    virtual  bool    check_input_exists(
            lid_t const & /*block id*/) const = 0;

    /*!
     * @brief
     *    write IDs of blocks in this stream to
     *    given buffer (it stops writing after buffer-size
     *    many IDs have been written)
     */
    virtual  int  get_available_blocks(
            lid_t [] /*buffer*/, size_t /*buffer size*/) const = 0;


    virtual  lerr_t  add_to_processing_stack(
            iproc_provider_t ** /*address of pointer to provider*/,
            iproc_provider_t * = NULL /*data processors*/) = 0;


    iproc_landscape_provider_t();
    virtual  ~iproc_landscape_provider_t() = 0;
};
}

#endif  /*  !LDNDC_IO_DATAPROVIDER_H_  */

