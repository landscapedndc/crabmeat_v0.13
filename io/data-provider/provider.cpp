/*!
 * @brief
 *    data provider base (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 16, 2013)
 */

#include  "io/data-provider/provider.h"

ldndc::iproc_landscape_provider_t::iproc_landscape_provider_t()
        : iproc_landscape_base_t(),
          iproc_collective_operations_interface_t()
{
}
ldndc::iproc_landscape_provider_t::~iproc_landscape_provider_t()
{
}



ldndc::iproc_provider_interface_t::iproc_provider_interface_t()
{
}
ldndc::iproc_provider_interface_t::~iproc_provider_interface_t()
{
}

ldndc::iproc_provider_t::iproc_provider_t()
        : iproc_base_t()
{
}
ldndc::iproc_provider_t::~iproc_provider_t()
{
}

