/*!
 * @brief
 *    abstraction layer in front of output writer
 *
 * @authors
 *    steffen klatt (created on: nov 02, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_OUTPUT_OBJECT_H_
#define  LDNDC_OUTPUT_OBJECT_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "io/sink-info.h"
#include  "io/sink-handle-srv.h"

namespace ldndc {
class  CBM_API  output_sink_srv_base_t  :  public  cbm::server_object_t
{
    protected:
        output_sink_srv_base_t();
        output_sink_srv_base_t(
                lid_t const &);
    public:
        virtual  ~output_sink_srv_base_t() = 0;
};

struct  oproc_landscape_receiver_t;
class  CBM_API  output_sink_srv_t  :  public  output_sink_srv_base_t
{
    LDNDC_SERVER_OBJECT(output_sink_srv_t)
    public:
        output_sink_srv_t();
        output_sink_srv_t(
                lid_t const & /*stream id*/);

        ~output_sink_srv_t();

        lerr_t  status() const;

        lerr_t  open_stream(
            sink_info_t const * /*sink info*/);
        lerr_t  close_stream();
        lerr_t  flush_stream();


        sink_handle_srv_t  sink_handle_acquire(
                lreceiver_descriptor_t const & /*receiver descriptor*/);
        lerr_t  sink_handle_release(
                lreceiver_descriptor_t const & /*receiver descriptor*/);

        sink_info_t const *  operator->() const
        {
            return  &this->sink_info_;
        }

        lerr_t  update( cbm::sclock_t const *);
        lerr_t  commit( cbm::sclock_t const *);
        lerr_t  reset();

    private:
        /* output writer (this class is owner) */
        oproc_landscape_receiver_t *  lrcv_;
        int  refcnt_;

        sink_info_t  sink_info_;
};
}

#endif  /*  !LDNDC_OUTPUT_OBJECT_H_  */

