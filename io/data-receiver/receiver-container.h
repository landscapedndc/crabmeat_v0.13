/*!
 * @brief
 *    data receiver container
 *
 * @author
 *    steffen klatt (created on: jan 10, 2014)
 */

#ifndef  LDNDC_IO_DATARECEIVERCONTAINER_H_
#define  LDNDC_IO_DATARECEIVERCONTAINER_H_

#include  "crabmeat-common.h"
#include  "io/data-receiver/receiver.h"
#include  "io/sink-info.h"

#include  "openmp/cbm_omp.h"

#include  <vector>

namespace ldndc {

template < typename  _lreceiver_t >
class  CBM_API  oproc_receiver_container_t
{
    typedef  _lreceiver_t  landscape_receiver_t;
    typedef  typename _lreceiver_t::receiver_t  receiver_t;

    struct  _receiver_t
    {
        receiver_t *  receiver;
        int  ref_count;
    };

    public:
        size_t  size() const { return  this->m_nbreceivers; }

        receiver_t *  acquire_receiver(
                lreceiver_descriptor_t const & /*receiver descriptor*/);

        void  release_receiver(
                lreceiver_descriptor_t const & /*receiver descriptor*/,
                typename landscape_receiver_t::release_op_t const * /*release operation*/);

        receiver_t *  operator[]( size_t  _k)
            { return  this->m_receivers[_k].receiver; }
        receiver_t const *  operator[]( size_t  _k) const
            { return  this->m_receivers[_k].receiver; }

    public:
        oproc_receiver_container_t();
        virtual  ~oproc_receiver_container_t();

        void  lock(
                char const * = NULL /*name*/);
        void  unlock(
                char const * = NULL /*name*/);

    private:
        _receiver_t *  m_findreceiver(
                lreceiver_descriptor_t const & /*receiver descriptor*/);

        std::vector< _receiver_t >  m_receivers;
        size_t  m_nbreceivers;

        cbm::omp::omp_lock_t  m_lock;
};

}

#include  "io/data-receiver/receiver-container.inl"

#endif  /*  !LDNDC_IO_DATARECEIVERCONTAINER_H_  */

