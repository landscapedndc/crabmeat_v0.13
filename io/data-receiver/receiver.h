/*!
 * @brief
 *    data receiver base
 *
 * @author
 *    steffen klatt (created on: nov 19, 2013)
 */

#ifndef  LDNDC_IO_DATARECEIVER_H_
#define  LDNDC_IO_DATARECEIVER_H_

#include  "crabmeat-common.h"
#include  "io/pmr.h"
#include  "io/sink-info.h"

#include  "io/outputtypes.h"
#include  "cbm_types.h"

typedef  ldndc_int64_t  lreceiver_descriptor_t;

namespace ldndc {

struct  input_class_set_t;
struct  CBM_API  oproc_receiver_t  :  oproc_base_t
{
    virtual  lerr_t  status() const = 0;

    typedef  lreceiver_descriptor_t  descriptor_t;
    virtual  descriptor_t  descriptor() const = 0;

    virtual  char  receiver_type() const = 0;
    virtual  char const *  output_format_type() const = 0;

    virtual  sink_layout_t const *  layout(
            sink_layout_hint_t const * /*hint*/) const = 0;
    virtual  sink_entities_t const *  entities(
            sink_layout_hint_t const * /*hint*/) const = 0;

    /*!
     * @brief
     *    notifies the receiver that no more data
     *    will arrive.
     */
// sk:TODO    virtual  lerr_t  is_complete() = 0;

    virtual  lerr_t  write_record(
            sink_client_t const * /*client identifier*/,
            sink_record_srv_t const * /*record*/) = 0;

    oproc_receiver_t();
    virtual  ~oproc_receiver_t() = 0;
};

struct  CBM_API  oproc_landscape_receiver_t  :  oproc_landscape_base_t, oproc_collective_operations_interface_t
{
    virtual  lerr_t  status() const = 0;

    typedef  lid_t  descriptor_t;
    virtual  descriptor_t  descriptor() const { return  this->object_id(); }

    virtual  char  receiver_type() const = 0;
    virtual  char const *  output_format_type() const = 0;

    virtual  lerr_t  open(
                ldndc::sink_info_t const *) = 0;
    virtual  lerr_t  reopen() = 0;

    virtual  lerr_t  close() = 0;
    virtual  lerr_t  flush() = 0;
    virtual  lerr_t  flush(
            oproc_receiver_t * /*receiver to flush, NULL flushes all*/) = 0;

    virtual  bool  flag_required_inputs(
            input_class_set_t *) const = 0;
    virtual  lerr_t  pull_required_inputs(
            input_class_set_t *, oproc_receiver_t *) = 0;

    virtual  void  mark_as_dirty() = 0;

    virtual  oproc_receiver_t *  acquire_receiver(
            lreceiver_descriptor_t const & /*receiver descriptor*/) = 0;
    virtual  void  release_receiver(
            lreceiver_descriptor_t const & /*receiver descriptor*/) = 0;

    virtual  lerr_t  set_fixed_layout(
            oproc_receiver_t * /*target receiver*/,
            sink_record_meta_srv_t const * /*record meta info*/,
            sink_entity_layout_t const * /*entities&layout*/) = 0;


    virtual  lerr_t  dispatch_write(
            oproc_receiver_t * /*target receiver*/,
            sink_client_t const * /*client identifier*/,
            sink_record_srv_t const * /*record*/);

    oproc_landscape_receiver_t();
    virtual  ~oproc_landscape_receiver_t() = 0;
};

}

#endif  /*  !LDNDC_IO_DATARECEIVER_H_  */

