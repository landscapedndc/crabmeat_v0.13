/*!
 * @brief
 *    data receiver container (implementation)
 *
 * @author
 *    steffen klatt (created on: jan 10, 2014)
 */

template < typename  _lreceiver_t >
ldndc::oproc_receiver_container_t< _lreceiver_t >::oproc_receiver_container_t()
        : m_nbreceivers( 0)
{
    cbm::omp::init_lock( &this->m_lock);
}
template < typename  _lreceiver_t >
ldndc::oproc_receiver_container_t< _lreceiver_t >::~oproc_receiver_container_t< _lreceiver_t >()
{
    this->release_receiver( invalid_t< lreceiver_descriptor_t >::value /*==all*/, NULL);
    cbm::omp::destroy_lock( &this->m_lock);
}

template < typename  _lreceiver_t >
void
ldndc::oproc_receiver_container_t< _lreceiver_t >::lock(
                char const *  _name)
{
    cbm::omp::set_lock( &this->m_lock, ( _name) ? _name : "receiver-container");
}
template < typename  _lreceiver_t >
void
ldndc::oproc_receiver_container_t< _lreceiver_t >::unlock(
                char const *  _name)
{
    cbm::omp::unset_lock( &this->m_lock, ( _name) ? _name : "receiver-container");
}

template < typename  _lreceiver_t >
typename ldndc::oproc_receiver_container_t< _lreceiver_t >::receiver_t *
ldndc::oproc_receiver_container_t< _lreceiver_t >::acquire_receiver(
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    this->lock( "receiver container: acquire receiver");
    _receiver_t *  a_receiver = this->m_findreceiver( _receiver_descriptor);
    if ( !a_receiver || ( a_receiver && !a_receiver->receiver))
    {
        receiver_t *  new_receiver = receiver_t::new_instance();
        if ( new_receiver)
        {
            new_receiver->set_descriptor( _receiver_descriptor);
            int  no_gap = 1;
            for ( size_t  j = 0;  j < this->m_receivers.size();  ++j)
            {
                if ( this->m_receivers[j].receiver == NULL)
                {
                    no_gap = 0;
                    this->m_receivers[j].receiver = new_receiver;
                    this->m_receivers[j].ref_count = 0;
                    a_receiver = &this->m_receivers[j];
                }
            }
            if ( no_gap)
            {
                _receiver_t  r;
                r.receiver = new_receiver;
                r.ref_count = 0;
                this->m_receivers.push_back( r);
                a_receiver = &this->m_receivers[this->m_receivers.size()-1];
            }
            this->m_nbreceivers += 1;
        }
        else
            { a_receiver = NULL; }
    }
    this->unlock( "receiver container: acquire receiver");

    /* receiver not found and failed to construct */
    if ( !a_receiver)
        { return  NULL; }

    /* increase reference count */
    a_receiver->ref_count += 1;
    return  a_receiver->receiver;
}


template < typename  _lreceiver_t >
void
ldndc::oproc_receiver_container_t< _lreceiver_t >::release_receiver(
        lreceiver_descriptor_t const &  _receiver_descriptor,
        typename landscape_receiver_t::release_op_t const *  _release_op)
{
    this->lock( "receiver container: release receiver");
    lreceiver_descriptor_t const  receiver_descriptor = _receiver_descriptor;
    size_t  receivers_cnt = this->m_receivers.size();
    for ( size_t  j = 0;  j < receivers_cnt;  ++j)
    {
        _receiver_t *  w = &this->m_receivers[j];
        if ( !w || !w->receiver)
        {
            /* skip */
            if ( w) { crabmeat_assert( w->ref_count == 0); }
        }
        else if (( receiver_descriptor == invalid_t< lreceiver_descriptor_t >::value)
                || ( w->receiver->descriptor() == receiver_descriptor))
        {
            if ( w->ref_count > 1)
            {
                w->ref_count -= 1;
            }
            else
            {
                crabmeat_assert( w->ref_count == 1);

                if ( _release_op)
                    { (*_release_op)( w->receiver); }
                w->receiver->delete_instance();

                this->m_receivers[j].receiver = NULL;
                this->m_receivers[j].ref_count = 0;
                this->m_nbreceivers -= 1;

// sk:TODO  move tail to free slot
// sk:??            this->m_receivers[j] = this->m_receivers[receivers_cnt-1];
// sk:??            this->m_receivers.erase( this->m_receivers.rbegin());

            }
            /* stop here unless we release all receivers */
            if ( receiver_descriptor != invalid_t< lreceiver_descriptor_t >::value)
                { break; }
        }
    }
    this->unlock( "receiver container: release receiver");
}


template < typename  _lreceiver_t >
typename ldndc::oproc_receiver_container_t< _lreceiver_t >::_receiver_t *
ldndc::oproc_receiver_container_t< _lreceiver_t >::m_findreceiver(
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    if ( _receiver_descriptor == invalid_t< lreceiver_descriptor_t >::value)
    {
        return  NULL;
    }

    _receiver_t *  the_receiver = NULL;
    for ( size_t  j = 0;  j < this->m_receivers.size();  ++j)
    {
        _receiver_t *  w = &this->m_receivers[j];
        if ( w && w->receiver && ( w->receiver->descriptor() == _receiver_descriptor))
        {
            the_receiver = w;
        }
    }

    return  the_receiver;
}

