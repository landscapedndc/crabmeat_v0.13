/*!
 * @brief
 *    output writers factory array
 *
 * @author
 *    steffen klatt (created on: nov 19, 2013)
 */

#ifndef  LDNDC_IO_OUTPUTWRITERS_H_
#define  LDNDC_IO_OUTPUTWRITERS_H_

#include  "io/data-receiver/owriters/owriter.h"

namespace ldndc {

    extern ldndc::ioproc_factory_base_t const *
        find_owriter_factory( char /*kind {'L','C'}*/,
               char const * /*format*/);
}
#endif  /*  !LDNDC_IO_OUTPUTWRITERS_H_ */

