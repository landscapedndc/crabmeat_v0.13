/*!
 * @brief
 *    virtual output writers
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013)
 */

#ifndef  LDNDC_OUTPUTWRITER_H_
#define  LDNDC_OUTPUTWRITER_H_

#include  "io/data-receiver/receiver.h"

namespace ldndc {

#define  LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON(__io_type__) \
public: \
    static char const *  format() \
        { return __io_type__; } \
    char const *  output_format_type() const \
        { return  this->format(); } \
    char  receiver_type() const \
        { return  'O'; } \
private:

class  CBM_API  oproc_owriter_interface_t
{
    public:
        /* discard unflushed data */
        virtual  lerr_t  discard_buffer_content() = 0;

        virtual  size_t  buffer_size() const = 0;
        virtual  size_t  buffer_size_limit() const = 0;

    protected:
        oproc_owriter_interface_t();
        virtual  ~oproc_owriter_interface_t() = 0;
};
struct  CBM_API  oproc_owriter_base_t
    :  public  oproc_owriter_interface_t, public  oproc_receiver_t
{
    oproc_owriter_base_t();
    virtual  ~oproc_owriter_base_t() = 0;
};


class  CBM_API  oproc_landscape_owriter_interface_t
{
     public:
         virtual  oproc_owriter_base_t *  acquire_owriter(
                 lreceiver_descriptor_t const & /*writer descriptor*/) = 0;
         virtual  void  release_owriter(
                 lreceiver_descriptor_t const & /*writer descriptor*/) = 0;

        virtual  lerr_t  set_layout_owriter(
                oproc_owriter_base_t * /*target block*/,
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*layout&entities*/) = 0;
 
        virtual  lerr_t  flush_owriter(
                oproc_owriter_base_t * /*target block*/) = 0;

    protected:
        oproc_landscape_owriter_interface_t();
        virtual  ~oproc_landscape_owriter_interface_t() = 0;
};
struct  CBM_API  oproc_landscape_owriter_base_t  :  public  oproc_landscape_owriter_interface_t, public  oproc_landscape_receiver_t
{
    oproc_receiver_t *  acquire_receiver(
                 lreceiver_descriptor_t const &);
    void  release_receiver(
                 lreceiver_descriptor_t const &);
 
    lerr_t  set_fixed_layout(
                 oproc_receiver_t *,
                sink_record_meta_srv_t const *,
                sink_entity_layout_t const *);

    lerr_t  flush();
    lerr_t  flush(
            oproc_receiver_t *);

    virtual  bool  flag_required_inputs(
            input_class_set_t *) const { return  false; }
    virtual  lerr_t  pull_required_inputs(
            input_class_set_t *, oproc_receiver_t *) { return  LDNDC_ERR_OK; }


    oproc_landscape_owriter_base_t();
    virtual  ~oproc_landscape_owriter_base_t() = 0;
};

#define  __LDNDC_IOPROC_FACTORY_TYPENAME_owriter(__format__)          \
        oproc_owriter_factory_##__format__##_t
#define  __LDNDC_IOPROC_FACTORY_TYPEDEF_owriter(__format__)           \
        typedef  ioproc_factory_t< oproc_owriter_##__format__##_t >  __LDNDC_IOPROC_FACTORY_TYPENAME_owriter(__format__)
#define  __LDNDC_IOPROC_FACTORY_owriter(__format__)                   \
        oproc_owriter_factory_##__format__

#define  __LDNDC_IOPROC_FACTORY_TYPENAME_olandscapewriter(__format__)          \
        oproc_olandscapewriter_factory_##__format__##_t
#define  __LDNDC_IOPROC_FACTORY_TYPEDEF_olandscapewriter(__format__)           \
        typedef  ioproc_factory_t< oproc_landscape_owriter_##__format__##_t >  __LDNDC_IOPROC_FACTORY_TYPENAME_olandscapewriter(__format__)
#define  __LDNDC_IOPROC_FACTORY_olandscapewriter(__format__)                   \
        oproc_olandscapewriter_factory_##__format__


}

#endif  /*  !LDNDC_OUTPUTWRITER_H_  */

