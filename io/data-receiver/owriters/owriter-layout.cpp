/*!
 * @brief
 *    output writer layout helper (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013)
 */

#include  "io/data-receiver/owriters/owriter-layout.h"
#include  "io/outputtypes.h"
#include  "math/cbm_math.h"
#include  "string/cbm_string.h"
#include  "memory/cbm_mem.h"

#include  "log/cbm_baselog.h"


namespace ldndc {

size_t
oproc_owriter_layouthelper_t::get_number_of_entities(
        sink_layout_t const *  _layout)
{
    if ( !_layout)
    {
        return  0;
    }

    ldndc_output_size_t  m = 0;
    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        m += _layout->sizes[r];
    }
    return  m;
}

lerr_t
oproc_owriter_layouthelper_t::set_layout(
        sink_entity_layout_t *  _l_dst,
        sink_entity_layout_t const *  _l_src)
{
    if ( _l_dst && _l_dst)
    {
        lerr_t  rc_ents = set_entities( &_l_dst->ents, &_l_src->ents);
        RETURN_IF_NOT_OK(rc_ents);

        lerr_t  rc_layout = set_record_layout( &_l_dst->layout, &_l_src->layout);
        RETURN_IF_NOT_OK(rc_layout);

        return  LDNDC_ERR_OK;
    }

    CBM_LogError( "oproc_owriter_layouthelper_t::set_layout(): missing source or destination layout buffer");
    return  LDNDC_ERR_REQUEST_MISMATCH;
}

lerr_t
oproc_owriter_layouthelper_t::set_entities(
        sink_entities_t *  _e_dst,
        sink_entities_t const *  _e_src)
{
    if ( !_e_dst || !_e_src)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }

    /* txt writer needs no entity names */
    _e_dst->names = NULL;

    if ( !_e_src->ids || ( _e_src->size == 0))
    {
        /* assume, that no entities are needed */
        _e_dst->ids = NULL;
        _e_dst->size = 0;
        if (( _e_src->size == 0)  && ( _e_src->ids))
        {
            CBM_LogWarn( "oproc_owriter_layouthelper_t::set_entities(): entity IDs are set but size is 0");
        }

        return  LDNDC_ERR_OK;
    }

    ldndc_string_t *  ent_names =
        CBM_DefaultAllocator->allocate_type< ldndc_string_t >( _e_src->size);
    if ( !ent_names)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::set_entities(): nomem");
        return  LDNDC_ERR_NOMEM;
    }
    _e_dst->size = _e_src->size;
    for ( ldndc_output_size_t  k = 0;  k < _e_src->size;  ++k)
    {
        char const *  eid = _e_src->ids[k];
        size_t const  eid_len = cbm::strlen( eid);
        if (( eid_len >= 4) 
            && ( eid[0]=='_'&&eid[1]=='_'&&eid[eid_len-2]=='_'&&eid[eid_len-1]=='_'))
        {
            cbm::as_strcpy( &ent_names[k], "$");
        }
        else
        {
            cbm::as_strcpy( ent_names[k], _e_src->ids[k]);
        }
    }
    _e_dst->ids = ent_names;

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_layouthelper_t::check_entities_match(
        sink_entities_t const *  _l0,
        sink_entities_t const *  _l1)
{
    if ( !_l0 || !_l1)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }

    /* receiving new client request .. checking validity */
    if ( _l0->size != _l1->size)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::check_entities_match(): ",
                "new client request for sink advertises different entity count  [",(int)_l0->size," vs ",(int)_l1->size,")]");
        return  LDNDC_ERR_FAIL;
    }

    /* sizes are identical, so unless they point at the same location check them */
    if ( _l0->names != _l1->names)
    {
// sk:no        if ( !_l0->names || !_l1->names)
// sk:no        {
// sk:no            return  LDNDC_ERR_FAIL;
// sk:no        }

        for ( ldndc_output_rank_t  r = 0;  r < _l0->size;  ++r)
        {
            if ( cbm::is_not_equal( _l0->names[r], _l1->names[r]))
            {
                CBM_LogWarn( "oproc_owriter_layouthelper_t::check_entities_match(): ",
                        "new client request for sink advertises different entity names  [sink=","?"/*sink_id*/,"]");
// sk:no                return  LDNDC_ERR_FAIL;
                break;
            }
        }
    }

    if ( _l0->ids != _l1->ids)
    {
        if ( !_l0->ids || !_l1->ids)
        {
            return  LDNDC_ERR_FAIL;
        }

        for ( ldndc_output_rank_t  r = 0;  r < _l0->size;  ++r)
        {
            if ( cbm::is_not_equal( _l0->ids[r], _l1->ids[r]))
            {
                CBM_LogError( "oproc_owriter_layouthelper_t::check_entities_match(): ",
                        "new client request for sink advertises different entity IDs  [sink=","?"/*sink_id*/,"]");
                return  LDNDC_ERR_FAIL;
            }
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
oproc_owriter_layouthelper_t::set_record_layout(
        sink_layout_t *  _l_dst,
        sink_layout_t const *  _l_src)
{
    if ( !_l_dst || !_l_src)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }

    if ( ( _l_src->rank == 0) || !_l_src->types || !_l_src->sizes)
    {
        _l_dst->types = NULL;
        _l_dst->sizes = NULL;
        _l_dst->entsizes = NULL;
        _l_dst->rank = 0;

        return  LDNDC_ERR_OK;
    }

    atomic_datatype_t *  l_types = CBM_DefaultAllocator->allocate_type< atomic_datatype_t >( _l_src->rank);
    if ( !l_types)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::set_record_layout(): nomem");
        return  LDNDC_ERR_NOMEM;
    }
    ldndc_output_size_t *  l_sizes = CBM_DefaultAllocator->allocate_type< ldndc_output_size_t >( _l_src->rank);
    if ( !l_sizes)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::set_record_layout(): nomem");
        CBM_DefaultAllocator->deallocate( l_types);
        _l_dst->types = NULL;
        return  LDNDC_ERR_NOMEM;
    }
    ldndc_output_size_t const  e_size = cbm::sum( _l_src->sizes, _l_src->rank);
    CBM_LogDebug( "sum of sizes=",e_size);
    ldndc_output_size_t *  e_sizes = NULL;
    if ( _l_src->entsizes)
    {
        e_sizes = CBM_DefaultAllocator->allocate_type< ldndc_output_size_t >( e_size);
        if ( !e_sizes)
        {
            CBM_LogError( "oproc_owriter_layouthelper_t::set_record_layout(): nomem");
            CBM_DefaultAllocator->deallocate( l_types);
            _l_dst->types = NULL;
            CBM_DefaultAllocator->deallocate( l_sizes);
            _l_dst->sizes = NULL;
            return  LDNDC_ERR_NOMEM;
        }
    }

    _l_dst->rank = _l_src->rank;

    ldndc_output_size_t *  e_s = e_sizes;
    ldndc_output_size_t const *  e_S = _l_src->entsizes;
    for ( ldndc_output_rank_t  r = 0;  r < _l_src->rank;  ++r)
    {
        l_types[r] = _l_src->types[r];
        if ( l_types[r] == LDNDC_COMPOUND)
        {
            CBM_LogError( "oproc_owriter_layouthelper_t::set_record_layout(): ",
                    "compound entity structure for txt writer not supported. skipping entity.");
            l_types[r] = LDNDC_DTYPE_NONE;
        }
        l_sizes[r] = _l_src->sizes[r];
        if ( l_sizes[r] == 0)
        {
            l_types[r] = LDNDC_DTYPE_NONE;
        }

        if ( _l_src->entsizes)
        {
            for ( ldndc_output_size_t  l = 0;  l < l_sizes[r];  ++l, ++e_s, ++e_S)
            {
                *e_s = *e_S;
            }
        }
    }

    _l_dst->types = l_types;
    _l_dst->sizes = l_sizes;
    _l_dst->entsizes = e_sizes;

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_layouthelper_t::check_record_layout_match(
        sink_layout_t const *  _l0,
        sink_layout_t const *  _l1)
{
    if ( !_l0 || !_l1)
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }

    /* receiving new client request .. checking validity */
    if ( _l0->rank != _l1->rank)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                "new client request for sink advertises different rank  [",(int)_l0->rank," vs ",(int)_l1->rank,")]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    if ( !_l0->types || !_l1->types)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                "datatypes array is NULL");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    if ( !_l0->sizes || !_l1->sizes)
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                "sizes array is NULL");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    ldndc_output_size_t const *  es0 = _l0->entsizes;
    ldndc_output_size_t const *  es1 = _l1->entsizes;
    if (( es0 && !es1) || ( !es0 && es1))
    {
        CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                "entity sizes exist only once");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    for ( ldndc_output_rank_t  r = 0;  r < _l0->rank;  ++r)
    {
        if ( _l0->types[r] != _l1->types[r])
        {
            CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                    "new client request for sink advertises different datatypes  [sink=",/*sink_id,*/"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        if ( _l0->sizes[r] != _l1->sizes[r])
        {
            CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                    "new client request for sink advertises different layout  [sink=",/*_sink_id,*/"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        if ( es0 && es1)
        {
            if ( *es0 != *es1)
            {
                CBM_LogError( "oproc_owriter_layouthelper_t::check_record_layout_match(): ",
                        "entity sizes differ  [size1=",*es0,",size2=",*es1,"]");
            }
            ++es0;
            ++es1;
        }
    }
    return  LDNDC_ERR_OK;
}

void
oproc_owriter_layouthelper_t::free_entity_layout(
        sink_entity_layout_t *  _layout)
{
    if ( !_layout)
    {
        return;
    }

    if ( _layout->ents.ids)
    {
        CBM_DefaultAllocator->deallocate( _layout->ents.ids);
        _layout->ents.ids = NULL;
    }
    if ( _layout->ents.names)
    {
        CBM_DefaultAllocator->deallocate( _layout->ents.names);
        _layout->ents.names = NULL;
    }
    if ( _layout->layout.sizes)
    {
        CBM_DefaultAllocator->deallocate( _layout->layout.sizes);
        _layout->layout.sizes = NULL;
    }
    if ( _layout->layout.types)
    {
        CBM_DefaultAllocator->deallocate( _layout->layout.types);
        _layout->layout.types = NULL;
    }
    if ( _layout->layout.entsizes)
    {
        CBM_DefaultAllocator->deallocate( _layout->layout.entsizes);
        _layout->layout.entsizes = NULL;
    }
}


}

