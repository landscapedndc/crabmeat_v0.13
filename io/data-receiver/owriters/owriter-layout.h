/*!
 * @brief
 *    output writer layout helper
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_LAYOUTHELPER_H_
#define  LDNDC_IO_OUTPUTWRITER_LAYOUTHELPER_H_

#include  "crabmeat-common.h"

namespace ldndc {

struct  sink_entities_t;
struct  sink_layout_t;
struct  sink_entity_layout_t;

struct  CBM_API  oproc_owriter_layouthelper_t
{
    static  size_t  get_number_of_entities(
            sink_layout_t const * /*layout*/);

    /* deep copy layout */
    static  lerr_t  set_layout(
            sink_entity_layout_t * /*buffer*/,
            sink_entity_layout_t const * /*layout&entities*/);


    static  lerr_t  set_entities(
            sink_entities_t * /*buffer*/,
            sink_entities_t const * /*entities*/);
    static  lerr_t  check_entities_match(
            sink_entities_t const * /*entities 1*/,
            sink_entities_t const * /*entities 2*/);

    static  lerr_t  set_record_layout(
            sink_layout_t * /*buffer*/,
            sink_layout_t const * /*layout*/);

    static  lerr_t  check_record_layout_match(
            sink_layout_t const * /*layout 1*/,
            sink_layout_t const * /*layout 2*/);

    static  void  free_entity_layout(
            sink_entity_layout_t *);
};

}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_LAYOUTHELPER_H_  */

