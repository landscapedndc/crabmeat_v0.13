/*!
 * @brief
 *    output writer factories (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 19, 2013)
 */

#include  "io/data-receiver/owriters/owriters.h"

#ifdef  LDNDC_IO_SQLITE3
#  include  "io/data-receiver/owriters/sqlite3/owriter_sqlite3.h"
static ldndc::ioproc_factory_t< ldndc::oproc_owriter_sqlite3_t >  owriter_sqlite3_factory;
static ldndc::ioproc_factory_t< ldndc::oproc_landscape_owriter_sqlite3_t >  owriter_landscape_sqlite3_factory;
#endif
#ifdef  LDNDC_IO_XML
#  include  "io/data-receiver/owriters/xml/owriter_xml.h"
static ldndc::ioproc_factory_t< ldndc::oproc_owriter_xml_t >  owriter_xml_factory;
static ldndc::ioproc_factory_t< ldndc::oproc_landscape_owriter_xml_t >  owriter_landscape_xml_factory;
#endif
#ifdef  LDNDC_IO_TXT
#  include  "io/data-receiver/owriters/txt/owriter_txt.h"
static ldndc::ioproc_factory_t< ldndc::oproc_owriter_txt_t >  owriter_txt_factory;
static ldndc::ioproc_factory_t< ldndc::oproc_landscape_owriter_txt_t >  owriter_landscape_txt_factory;
#endif


struct  owriter_factory_item_t
{
    ldndc::ioproc_factory_base_t const *  factory;
    char  kind; /* 'L':=landscape, 'C':=cell */
    char const *  format;
};

static owriter_factory_item_t const  __cbm_owriters_factories[] =
{
#ifdef  LDNDC_IO_SQLITE3
    { &owriter_sqlite3_factory, 'C', "sqlite3" },
    { &owriter_landscape_sqlite3_factory, 'L', "sqlite3" },
#endif
#ifdef  LDNDC_IO_TXT
    { &owriter_txt_factory, 'C', "txt" },
    { &owriter_landscape_txt_factory, 'L', "txt" },
#endif
#ifdef  LDNDC_IO_XML
    { &owriter_xml_factory, 'C', "xml" },
    { &owriter_landscape_xml_factory, 'L', "xml" },
#endif

    { NULL, '-', NULL} /* sentinel */
};

ldndc::ioproc_factory_base_t const *  ldndc::find_owriter_factory(
                char  _kind, char const *  _format)
{
    if (( _kind!='C' && _kind!='L') || !_format)
        { return  NULL; }

    int  r = 0;
    while ( __cbm_owriters_factories[r].factory)
    {
        if (( __cbm_owriters_factories[r].kind == _kind)
                && cbm::is_equal( __cbm_owriters_factories[r].format, _format))
            { return  __cbm_owriters_factories[r].factory; }
        ++r;
    }
    return  NULL;
}

