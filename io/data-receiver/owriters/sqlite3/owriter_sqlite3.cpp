/*!
 * @brief
 *    sqlite3 output writer  (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 09, 2014)
 */

#include  "io/data-receiver/owriters/sqlite3/owriter_sqlite3.h"
#include  "io/data-receiver/owriters/owriter-layout.h"

#include  "string/lstring-basic.h"
#include  "io/marshal/marshal.h"

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"

#define db_filename sqlite3_db_filename( _db, "main")

namespace ldndc {

oproc_owriter_sqlite3_base_t::oproc_owriter_sqlite3_base_t()
        : oproc_owriter_base_t()
{
}


oproc_owriter_sqlite3_base_t::~oproc_owriter_sqlite3_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_owriter_sqlite3_t)
oproc_owriter_sqlite3_t::oproc_owriter_sqlite3_t()
        : oproc_owriter_sqlite3_base_t(),
          rcv_d_( invalid_t< lreceiver_descriptor_t >::value)
{
    this->buf.slots = NULL;
    this->buf.n_slots = 0;
    this->buf.p_slot = -1;
}
oproc_owriter_sqlite3_t::~oproc_owriter_sqlite3_t()
{
    for ( int  p = 0;  p < this->buf.n_slots;  ++p)
    {
        if ( this->buf.slots[p].mem)
        {
            free_from_layout( this->buf.slots[p].mem, this->buf.layout);
            this->buf.slots[p].mem = NULL;
        }
    }
    CBM_DefaultAllocator->destroy< buffer_item_t >( this->buf.slots, this->buf.n_slots);
    this->buf.slots = NULL;
    this->buf.n_slots = 0;
    this->buf.p_slot = -1;
}


lerr_t
oproc_owriter_sqlite3_t::set_attributes(
        sink_attributes_t const *)
{
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_sqlite3_t::set_layout_owriter(
        sink_record_meta_srv_t const *,
        sink_entity_layout_t const *  _format,
        char const *  _sink_identifier)
{
    if ( !_format)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    this->buf.layout = &_format->layout;

    size_t const  buf_sz = ldndc::sizeof_layout( this->buf.layout);
    this->buf.n_slots = ( LDNDC_OWRITER_SQLITE_BUFFER_SIZE <= buf_sz) ? 1 : ( LDNDC_OWRITER_SQLITE_BUFFER_SIZE/buf_sz);
    CBM_LogDebug( "buffer slots for table ",_sink_identifier," have size ",buf_sz, " bytes; using ",this->buf.n_slots, " slots");
    CRABMEAT_FIX_UNUSED(_sink_identifier);

    crabmeat_assert( this->buf.n_slots > 0);
    this->buf.slots = CBM_DefaultAllocator->construct< buffer_item_t >( this->buf.n_slots);
    if ( !this->buf.slots)
    {
        CBM_LogError( "nomem");
        return  LDNDC_ERR_NOMEM;
    }
    for ( int  p = 0;  p < this->buf.n_slots;  ++p)
    {
        this->buf.slots[p].mem = NULL;
        malloc_from_layout( &this->buf.slots[p].mem, this->buf.layout);
        if ( !this->buf.slots[p].mem)
        {
            CBM_LogError( "nomem");
            return  LDNDC_ERR_NOMEM;
        }
    }

    this->buf.p_slot = 0;

    return  LDNDC_ERR_OK;
}

bool
oproc_owriter_sqlite3_t::need_flush()
const
{
    return  this->buf.p_slot == this->buf.n_slots;
}
lerr_t
oproc_owriter_sqlite3_t::commit_transactions(
        sqlite3 *  _db, sqlite3_stmt *  _stmt)
{
    if ( this->buf.p_slot == -1)
    {
        return  LDNDC_ERR_OK;
    }

    int  rc_trans = sqlite3_exec( _db, "BEGIN TRANSACTION", NULL, NULL, NULL);
    if ( rc_trans)
    {
        CBM_LogError( "oproc_owriter_sqlite3_t::commit_transactions(): ",
                "launching transaction failed:  ", sqlite3_errmsg(_db), "  [file=",db_filename,"]");
        return  LDNDC_ERR_FAIL;
    }

    for ( int  p = 0;  p < this->buf.p_slot;  ++p)
    {
        int  idx_offs = 1;

        sqlite3_bind_int( _stmt, idx_offs, this->buf.slots[p].t);
        ++idx_offs;

        this->commit_record_meta( _stmt, &idx_offs, &this->buf.client, &this->buf.slots[p].meta);

        this->commit_records( _stmt, &idx_offs, this->buf.slots[p].mem);

        if ( sqlite3_step( _stmt) != SQLITE_DONE)
        {
            CBM_LogError( "oproc_owriter_sqlite3_t::commit_transactions(): ",
                    "transaction failed; ",
                    "SQL error: ", sqlite3_errmsg( _db),
                    "  [client=",this->buf.client.target_id,",source=",this->buf.client.target_sid,
                    ",timestep=",this->buf.slots[p].t,",file=",db_filename,"]");
            sqlite3_reset( _stmt);
            sqlite3_exec( _db, "COMMIT TRANSACTION", NULL, NULL, NULL);
            return  LDNDC_ERR_FAIL;
        }
        sqlite3_reset( _stmt);
    }

    rc_trans = sqlite3_exec( _db, "COMMIT TRANSACTION", NULL, NULL, NULL);
    if ( rc_trans)
    {
        CBM_LogError( "oproc_owriter_sqlite3_t::commit_transactions(): ",
                "committing transaction failed:  ", sqlite3_errmsg(_db), "  [file=",db_filename,"]");
        return  LDNDC_ERR_FAIL;
    }

    this->buf.p_slot = 0;
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_sqlite3_t::write_record(
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( _record);
    if ( _client)
    {
        this->buf.client = *_client;
    }

    if ( _record->gap == ldndc::SINKGAP_NONE)
    {
        lerr_t  rc_write = this->write_record_( _client, _record);
        RETURN_IF_NOT_OK(rc_write);

        return  LDNDC_ERR_OK;
    }
    else if ( _record->gap == ldndc::SINKGAP_ZERO)
    {
        lerr_t  rc_writezero = this->write_zero_record_( _client, _record);
        RETURN_IF_NOT_OK(rc_writezero);

        return  LDNDC_ERR_OK;
    }

    /* do not know how to handle request */
    return  LDNDC_ERR_REQUEST_MISMATCH;

}

lerr_t
oproc_owriter_sqlite3_t::write_record_(
        sink_client_t const *,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( _record);
    if ( !_record->data || this->buf.p_slot == -1)
    {
        return  LDNDC_ERR_OK;
    }

    if ( this->buf.p_slot == this->buf.n_slots)
    {
        this->buf.p_slot = 0;
    }
    lerr_t  rc_cpy = ldndc::memcpy_with_layout(
            this->buf.slots[this->buf.p_slot].mem, (void const **)_record->data, this->buf.layout);
    if ( rc_cpy)
    {
        CBM_LogError( "failed to memcpy data record");
        return  LDNDC_ERR_FAIL;
    }
    this->buf.slots[this->buf.p_slot].meta = _record->meta;
    if ( _record->seq)
    {
        this->buf.slots[this->buf.p_slot].t = _record->seq->timestep;
    }
    else
    {
        this->buf.slots[this->buf.p_slot].t = -1;
    }

    this->buf.p_slot += 1;

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_sqlite3_t::write_zero_record_(
        sink_client_t const *,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( _record);
    if ( this->buf.p_slot == -1)
    {
        return  LDNDC_ERR_OK;
    }

    if ( this->buf.p_slot == this->buf.n_slots)
    {
        this->buf.p_slot = 0;
    }
    lerr_t  rc_clr = ldndc::memclr_with_layout(
            this->buf.slots[this->buf.p_slot].mem, this->buf.layout);
    if ( rc_clr)
    {
        CBM_LogError( "failed to clear data record");
        return  LDNDC_ERR_FAIL;
    }
    this->buf.slots[this->buf.p_slot].meta = _record->meta;
    if ( _record->seq)
    {
        this->buf.slots[this->buf.p_slot].t = _record->seq->timestep;
    }
    else
    {
        this->buf.slots[this->buf.p_slot].t = -1;
    }

    this->buf.p_slot += 1;

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_sqlite3_t::commit_records(
        sqlite3_stmt *  _stmt, int *  _index_offset,
        void **  _record)
{
    int const  r_end = static_cast< int >( this->buf.layout->rank);
    int  j = 0;
    int  idx( _index_offset ? *_index_offset : 1);
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = this->buf.layout->types[r];
        int const  this_size = this->buf.layout->sizes[r];

        if ( this_datatype == LDNDC_FLOAT64)
        {
            ldndc_flt64_t const *  d = (ldndc_flt64_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_double( _stmt, idx, *d);
            }
        }
        else if ( this_datatype == LDNDC_UINT32)
        {
            ldndc_uint32_t const *  d = (ldndc_uint32_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_int( _stmt, idx, *d);
            }
        }
        else if ( this_datatype == LDNDC_INT32)
        {
            ldndc_int32_t const *  d = (ldndc_int32_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_int( _stmt, idx, *d);
            }
        }
        else if ( this_datatype == LDNDC_FLOAT32)
        {
            ldndc_flt32_t const *  d = (ldndc_flt32_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_double( _stmt, idx, *d);
            }
        }
        else if ( this_datatype == LDNDC_STRING)
        {
            ldndc_string_t const *  d = (ldndc_string_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_text( _stmt, idx, *d, cbm::strlen( *d), NULL);
            }
        }
        else if ( this_datatype == LDNDC_UINT64)
        {
            ldndc_uint64_t const *  d = (ldndc_uint64_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_int64( _stmt, idx, *d);
            }
        }
        else if ( this_datatype == LDNDC_INT64)
        {
            ldndc_int64_t const *  d = (ldndc_int64_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_int64( _stmt, idx, *d);
            }
        }
        else if ( this_datatype == LDNDC_BOOL)
        {
            ldndc_bool_t const *  d = (ldndc_bool_t const *)_record[r];
            for ( j = 0;  j < this_size;  ++j, ++d, ++idx)
            {
                sqlite3_bind_int( _stmt, idx, (( *d) ? 1 : 0));
            }
        }
        else if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
            idx += this_size;
        }
        else
        {
            CBM_LogError( "[ERROR]  oproc_owriter_txt_t::commit_records(): ",
                    "datatype not supported  [datatype=",LDNDC_ATOMIC_DATATYPE_NAMES[this_datatype],"]");
            return  LDNDC_ERR_FAIL;
        }
    }

    if ( _index_offset)
    {
        *_index_offset = idx;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_sqlite3_t::commit_record_meta(
        sqlite3_stmt *  _stmt, int *  _index_offset,
        sink_client_t const *  _client, sink_record_meta_srv_t const *  _record_meta)
{
    if ( _record_meta->useregs == 0)
    {
        return  LDNDC_ERR_OK;
    }

    int  idx( _index_offset ? *_index_offset : 1);

    if ( _record_meta->useregs & io::CLIENT_SOURCE)
    {
        static char const *  unknown_clientsource = "?";
        char const *  cs = unknown_clientsource;
        if ( _client)
        {
            cs = _client->target_sid; // NOTE: remains alive after function return
        }
        sqlite3_bind_text( _stmt, idx, cs, cbm::strlen( cs), NULL);
        ++idx;
    }
    if ( _record_meta->useregs & io::CLIENT_ID)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >(( _client) ? _client->target_id : -1));
        ++idx;
    }
    if ( _record_meta->useregs & io::CLIENT_X)
    {
        sqlite3_bind_double( _stmt, idx, 
                static_cast< double >(( _client) ? _client->x : invalid_t< ldndc_flt64_t >::value));
        ++idx;
    }
    if ( _record_meta->useregs & io::CLIENT_Y)
    {
        sqlite3_bind_double( _stmt, idx, 
                static_cast< double >(( _client) ? _client->y : invalid_t< ldndc_flt64_t >::value));
        ++idx;
    }
    if ( _record_meta->useregs & io::CLIENT_Z)
    {
        sqlite3_bind_double( _stmt, idx, 
                static_cast< double >(( _client) ? _client->z : invalid_t< ldndc_flt64_t >::value));
        ++idx;
    }
    if ( _record_meta->useregs & io::CLIENT_AREA)
    {
        sqlite3_bind_double( _stmt, idx, 
                static_cast< double >(( _client) ? _client->area : invalid_t< ldndc_flt64_t >::value));
        ++idx;
    }

    sink_record_meta_t  m;
    m.timestamp_from_scalar( _record_meta->ts);

    if ( _record_meta->useregs & io::DATETIME)
    {
        cbm::tm_t  t;
        t.tm_year = m.t.reg.year;
        t.tm_month = m.t.reg.month-1;
        t.tm_day = m.t.reg.day-1;
        t.tm_hour = m.t.reg.hour;
        t.tm_min = m.t.reg.minute;
        t.tm_sec = m.t.reg.second;
        int const  isodate_sz = cbm::to_iso8601(
                t, this->isodate, CBM_ISO8601_MAXLEN);
        if ( isodate_sz >= CBM_ISO8601_MAXLEN)
            { CBM_LogWarn( "[BUG?]  datetime was truncated",
                "  [datetime=",this->isodate,"]"); }

        sqlite3_bind_text( _stmt, idx, this->isodate, isodate_sz, NULL);
        ++idx;
    }

    if ( _record_meta->useregs & io::YEAR)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.year));
        ++idx;
    }
    if ( _record_meta->useregs & io::MONTH)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.month));
        ++idx;
    }
    if ( _record_meta->useregs & io::DAY)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.day));
        ++idx;
    }
    if ( _record_meta->useregs & io::HOUR)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.hour));
        ++idx;
    }
    if ( _record_meta->useregs & io::MINUTE)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.minute));
        ++idx;
    }
    if ( _record_meta->useregs & io::SECOND)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.second));
        ++idx;
    }
    if ( _record_meta->useregs & io::JULIANDAY)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.julianday));
        ++idx;
    }
    if ( _record_meta->useregs & io::SUBDAY)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( m.t.reg.subday));
        ++idx;
    }
    if ( _record_meta->useregs & io::SEQNB)
    {
        sqlite3_bind_int( _stmt, idx, 
                static_cast< int >( _record_meta->record_seqnb));
        ++idx;
    }

    if ( _index_offset)
    {
        *_index_offset = idx;
    }

    return  LDNDC_ERR_OK;
}


oproc_landscape_owriter_sqlite3_base_t::oproc_landscape_owriter_sqlite3_base_t()
        : oproc_landscape_owriter_base_t()
{
}


oproc_landscape_owriter_sqlite3_base_t::~oproc_landscape_owriter_sqlite3_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_landscape_owriter_sqlite3_t)
oproc_landscape_owriter_sqlite3_t::oproc_landscape_owriter_sqlite3_t()
        : oproc_landscape_owriter_sqlite3_base_t(),
          fmt( sink_entity_layout_defaults),

          db_handle( NULL), db_insert_stmt( NULL),
          commit_count( 0),

          is_clean_( 1), is_layouted_( 0), is_open_( 0)
{
    cbm::omp::init_lock( &this->owriter_sqlite3_lock);
}
oproc_landscape_owriter_sqlite3_t::~oproc_landscape_owriter_sqlite3_t()
{
    (void)this->close();
    this->release_owriter( invalid_t< lreceiver_descriptor_t >::value /*==all*/);

    oproc_owriter_layouthelper_t::free_entity_layout( &this->fmt);

    cbm::omp::destroy_lock( &this->owriter_sqlite3_lock);
}

lerr_t
oproc_landscape_owriter_sqlite3_t::open(
        sink_info_t const *  _sink_info)
{
    crabmeat_assert( _sink_info);
    CBM_LogInfo( "oproc_landscape_owriter_sqlite3_t::open(): ",
            "opened sink  [sink=",_sink_info->identifier,",dev=",_sink_info->full_name(),"]");
#ifdef  CRABMEAT_OPENMP
    if ( !sqlite3_threadsafe())
    {
        CBM_LogError( "attempt to use SQLite3 in multi-threaded application (LDNDC) without threadsafe SQLite3 library.",
                   "  sorry for the paranoia :|");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
#endif

    lerr_t  rc_connect = this->connect( _sink_info);
    if ( rc_connect)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::open(): ",
                "failed to open database  [error=",sqlite3_errmsg(this->db_handle),"]");
        this->is_open_ = 0;
        return  LDNDC_ERR_FAIL;
    }
    int  rc_async = sqlite3_exec( this->db_handle, "PRAGMA synchronous=off", NULL, NULL, NULL);
    int  rc_journalmode = sqlite3_exec( this->db_handle, "PRAGMA journal_mode=MEMORY", NULL, NULL, NULL);
    int  rc_tempstore = sqlite3_exec( this->db_handle, "PRAGMA temp_store=MEMORY", NULL, NULL, NULL);
    if ( rc_async || rc_journalmode || rc_tempstore)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::open(): ",
                "failed to set pragma 'synchronous=OFF' or 'journal_mode=MEMORY' or 'temp_store=MEMORY'");
        return  LDNDC_ERR_FAIL;
    }

    this->sink_info = *_sink_info;
    this->is_open_ = 1;

        return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_sqlite3_t::connect(
        sink_info_t const *  _sink_info)
{
    if ( this->db_handle)
    {
        return  LDNDC_ERR_OK;
    }
    int  rc_connect = sqlite3_open_v2( _sink_info->full_name().c_str(), &this->db_handle,
            SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE|SQLITE_OPEN_PRIVATECACHE|SQLITE_OPEN_NOMUTEX, NULL);
    if ( rc_connect)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::connect(): ",
                "failed to connect to database  [error=",sqlite3_errmsg(this->db_handle),"]");
        this->is_open_ = 0;
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_sqlite3_t::reopen()
{
    return  LDNDC_ERR_FAIL;
}


lerr_t
oproc_landscape_owriter_sqlite3_t::close()
{
        if ( this->is_open_)
    {
        this->flush_all();
        if ( this->db_insert_stmt)
        {
            sqlite3_finalize( this->db_insert_stmt);
            this->db_insert_stmt = NULL;
        }
        if ( this->db_handle)
        {
            sqlite3_close( this->db_handle);
            this->db_handle = NULL;
        }

        this->is_open_ = 0;
// sk:dbg        CBM_LogDebug( "oproc_landscape_owriter_sqlite3_t::close(): ",
// sk:dbg                "successfully closed sink  [sink=",this->sink_info.identifier,",file=",this->sink_info.full_name(),"]");
        }

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_landscape_owriter_sqlite3_t::flush_owriter(
        oproc_owriter_base_t *  _owriter)
{
    if ( !_owriter)
    {
        return  this->flush_all();
    }
    return  static_cast< oproc_owriter_sqlite3_t * >( _owriter)->commit_transactions(
            this->db_handle, this->db_insert_stmt);
}
lerr_t
oproc_landscape_owriter_sqlite3_t::flush_all()
{

    this->owriters_.lock();
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(is_clean_)
#endif
    if ( this->is_clean_)
    {
        this->owriters_.unlock();
        return  LDNDC_ERR_OK;
    }

    for ( size_t  j = 0;  j < this->owriters_.size();  ++j)
    {
        oproc_owriter_sqlite3_t *  w = this->owriters_[j];
        if ( w)
            { this->flush_owriter( w); }
    }

    this->is_clean_ = 1;
    this->owriters_.unlock();

    return  LDNDC_ERR_OK;
}

oproc_owriter_base_t *
oproc_landscape_owriter_sqlite3_t::acquire_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    this->connect( &this->sink_info);

    oproc_owriter_base_t *  new_owriter =
        this->owriters_.acquire_receiver( _owriter_descriptor);
    if ( !new_owriter)
        { return  NULL; }

    lerr_t  rc_set_attrs =
        static_cast< oproc_owriter_sqlite3_t * >( new_owriter)->set_attributes( &this->sink_info.attrs);
    if ( rc_set_attrs)
        { return  NULL; }

    return  new_owriter;
}
void
oproc_landscape_owriter_sqlite3_t::release_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    if ( _owriter_descriptor == invalid_t< lreceiver_descriptor_t >::value)
        { return; }

    release_op_t  r_op;
    r_op.db = this->db_handle;
    r_op.stmt = this->db_insert_stmt;
    this->owriters_.release_receiver( _owriter_descriptor, &r_op);
}

lerr_t
oproc_landscape_owriter_sqlite3_t::set_layout_owriter(
        oproc_owriter_base_t *  _target,
        sink_record_meta_srv_t const *  _record_meta,
        sink_entity_layout_t const *  _layout)
{
    crabmeat_assert( _target);
    oproc_owriter_sqlite3_t *  this_target = static_cast< oproc_owriter_sqlite3_t *>( _target);

#ifdef  CRABMEAT_SANITY_CHECKS
    if ( !this->is_open_)
    {
        CBM_LogError( "[BUG]  oproc_landscape_owriter_sqlite3_t::set_layout_owriter(): ",
                "attempt to define entities for closed stream");
        return  LDNDC_ERR_FAIL;
    }
#endif  /*  CRABMEAT_SANITY_CHECKS  */

    lerr_t  rc_defent = LDNDC_ERR_OK;
    cbm::omp::set_lock( &this->owriter_sqlite3_lock, "layout");
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(is_layouted_)
#endif
    if ( !this->is_layouted_ && _layout)
    {
        rc_defent = oproc_owriter_layouthelper_t::set_layout( &this->fmt, _layout);
        if ( rc_defent == LDNDC_ERR_OK)
        {
            rc_defent = this->create_table(
                    _record_meta, _layout, this->sink_info.identifier.c_str());
            if ( rc_defent == LDNDC_ERR_OK)
            {
                this->is_layouted_ = 1;
            }
        }
        else
        {
            CBM_LogError( "oproc_landscape_owriter_sqlite3_t::set_layout_owriter(): ",
                    "setting layout failed");
        }
    }
    if ( rc_defent == LDNDC_ERR_OK)
    {
        rc_defent = this_target->set_layout_owriter(
                _record_meta, &this->fmt, this->sink_info.identifier.c_str());
    }
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(is_layouted_)
#endif
    cbm::omp::unset_lock( &this->owriter_sqlite3_lock, "layout");

    return  rc_defent;
}

lerr_t
oproc_landscape_owriter_sqlite3_t::create_table(
        sink_record_meta_srv_t const *  _record_meta,
        sink_entity_layout_t const *  _layout,
        char const *  _sink_identifier)
{
    if ( !_layout)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                "no layout :(");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( _layout->ents.size == 0)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                "no entities given");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( this->db_insert_stmt)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                "prepared insert statement should not exist yet..");
        return  LDNDC_ERR_FAIL;
    }

    char  sql_ctbl_buf[LDNDC_SQLITE3_STATEMENT_MAXSIZE+3];

    /* if overwriting is specified, drop and recreate table */
    if ( this->sink_info.attrs.overwrite)
    {
        if ( this->sink_info.attrs.noheader)
        {
            CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                    "you requested to drop a table (overwrite=1) and not to recreate it (noheader=1); ",
                    "impossible situation  [table=",_sink_identifier,",sink=",this->sink_info,"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        cbm::snprintf( sql_ctbl_buf, LDNDC_SQLITE3_STATEMENT_MAXSIZE,
            "DROP TABLE IF EXISTS %s", _sink_identifier);
        int  rc_ctbl = sqlite3_exec( this->db_handle, sql_ctbl_buf, NULL, NULL, NULL);
        if ( rc_ctbl != SQLITE_OK)
        {
            CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                    "failed to drop existing table:\nSQL error: ", sqlite3_errmsg( this->db_handle));
            return  LDNDC_ERR_FAIL;
        }
    }

    std::string  record_meta_str;
    int  n_meta = 0;
    if ( _record_meta && _record_meta->useregs > 0)
    {
        n_meta = this->generate_record_meta_sqlstmt( &record_meta_str, _record_meta);
        if ( n_meta < 1)
        {
            CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                    "generating record meta statement failed");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
// sk:dbg        CBM_LogDebug( "table meta entries=\"",record_meta_str,"\"");
    }

    char  sql_ins_buf[LDNDC_SQLITE3_STATEMENT_MAXSIZE+3];
    sql_ins_buf[LDNDC_SQLITE3_STATEMENT_MAXSIZE+1]='\0';
    char *  sql_ins = sql_ins_buf;
    int  n_prnt_ins = cbm::snprintf( sql_ins, LDNDC_SQLITE3_STATEMENT_MAXSIZE,
            "INSERT INTO %s VALUES(?1", _sink_identifier);
    sql_ins += n_prnt_ins;
    n_meta += 2; /*sql parameter index starts at 1 and timestep is already added*/
    for ( int  i = 2;  i < n_meta;  ++i)
    {
        n_prnt_ins = sprintf( sql_ins, ",?%d", i);
        sql_ins += n_prnt_ins;
    }

    int  n_prnt_max = LDNDC_SQLITE3_STATEMENT_MAXSIZE;
    char *  sql_ctbl = sql_ctbl_buf;
    int  n_prnt_ctbl = cbm::snprintf(
            sql_ctbl, n_prnt_max, "%s %s(timestep INT NOT NULL%s%s", "CREATE TABLE IF NOT EXISTS",
            _sink_identifier, ( record_meta_str.empty() ? "" : ","), record_meta_str.c_str());
    if ( n_prnt_ctbl < 0)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                "failed to set sql create table statement");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    sql_ctbl += n_prnt_ctbl;
    n_prnt_max -= n_prnt_ctbl;
    int  e = 0;
    for ( int  r = 0;  r < static_cast< int >( _layout->layout.rank);  ++r)
    {
        atomic_datatype_t const  this_datatype = _layout->layout.types[r];
        int const  this_size = _layout->layout.sizes[r];

        for ( int  j = 0;  j < this_size;  ++j, ++e)
        {
            if ( !_layout->ents.ids || cbm::is_empty(_layout->ents.ids[e]))
            {
                CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                        "invalid table column name");
                return  LDNDC_ERR_FAIL;
            }
            if ( this_datatype == LDNDC_INT8 || this_datatype == LDNDC_UINT8 || this_datatype == LDNDC_INT16 || this_datatype == LDNDC_UINT16
              || this_datatype == LDNDC_INT32 || this_datatype == LDNDC_UINT32 || this_datatype == LDNDC_INT64 || this_datatype == LDNDC_UINT64)
            {
                n_prnt_ctbl = cbm::snprintf( sql_ctbl, n_prnt_max, ",%s INT", _layout->ents.ids[e]);
                if ( n_prnt_ctbl < 0 || ( n_prnt_ctbl >= n_prnt_max))
                {
                    CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                            "failed to set sql create table statement");
                    return  LDNDC_ERR_RUNTIME_ERROR;
                }
                n_prnt_ins = sprintf( sql_ins, ",?%d", n_meta);
            }
            else if ( this_datatype == LDNDC_FLOAT32 || this_datatype == LDNDC_FLOAT64
#ifdef  LDNDC_HAVE_FLOAT128
                    || this_datatype == LDNDC_FLOAT128
#endif
            )
            {
                n_prnt_ctbl = cbm::snprintf( sql_ctbl, n_prnt_max, ",%s REAL", _layout->ents.ids[e]);
                if ( n_prnt_ctbl < 0 || ( n_prnt_ctbl >= n_prnt_max))
                {
                    CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                            "failed to set sql create table statement");
                    return  LDNDC_ERR_RUNTIME_ERROR;
                }
                n_prnt_ins = sprintf( sql_ins, ",?%d", n_meta);
            }
            else if ( this_datatype == LDNDC_CHAR)
            {
                n_prnt_ctbl = cbm::snprintf( sql_ctbl, n_prnt_max, ",%s CHAR(1)", _layout->ents.ids[e]);
                if ( n_prnt_ctbl < 0 || ( n_prnt_ctbl >= n_prnt_max))
                {
                    CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                            "failed to set sql create table statement");
                    return  LDNDC_ERR_RUNTIME_ERROR;
                }
                n_prnt_ins = sprintf( sql_ins, ",?%d", n_meta);
            }
            else if ( this_datatype == LDNDC_STRING)
            {
                n_prnt_ctbl = cbm::snprintf( sql_ctbl, n_prnt_max, ",%s CHAR(" STRINGIFY(CBM_STRING_SIZE) ")", _layout->ents.ids[e]);
                if ( n_prnt_ctbl < 0 || ( n_prnt_ctbl >= n_prnt_max))
                {
                    CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                            "failed to set sql create table statement");
                    return  LDNDC_ERR_RUNTIME_ERROR;
                }
                n_prnt_ins = sprintf( sql_ins, ",?%d", n_meta);
            }
            else
            {
                CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                        "unsupported datatype  [type=",this_datatype,"]");
                return  LDNDC_ERR_RUNTIME_ERROR;
            }

            sql_ctbl += n_prnt_ctbl;
            n_prnt_max -= n_prnt_ctbl;

            sql_ins += n_prnt_ins;
            n_meta += 1;
        }
    }
    sql_ctbl[0] = ')';
    sql_ctbl[1] = ';';
    sql_ctbl[2] = '\0';
// sk:dbg    CBM_LogDebug( "create table=",sql_ctbl_buf);
    sql_ins[0] = ')';
    sql_ins[1] = ';';
    sql_ins[2] = '\0';
// sk:dbg   CBM_LogDebug( "insert statement=",sql_ins_buf);

    /* if noheader option was given, assume table is already created */
    if ( !this->sink_info.attrs.noheader)
    {
        int  rc_ctbl = sqlite3_exec( this->db_handle, sql_ctbl_buf, NULL, NULL, NULL);
        if ( rc_ctbl != SQLITE_OK)
        {
            CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                    "SQL error: ", sqlite3_errmsg( this->db_handle));
            CBM_LogError( "SQL command: ", "\"",sql_ctbl_buf, "\"");
            return  LDNDC_ERR_FAIL;
        }
    }
    int  rc_prep = sqlite3_prepare_v2(
            this->db_handle, sql_ins_buf, cbm::strlen( sql_ins_buf), &this->db_insert_stmt, NULL);
    if ( rc_prep != SQLITE_OK)
    {
        CBM_LogError( "oproc_landscape_owriter_sqlite3_t::create_table(): ",
                "SQL error: ", sqlite3_errmsg( this->db_handle));
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

int
oproc_landscape_owriter_sqlite3_t::generate_record_meta_sqlstmt(
        std::string *  _record_meta_str,
        sink_record_meta_srv_t const *  _record_meta,
        bool  _with_dtype)
{
    if ( !_record_meta_str || !_record_meta || _record_meta->useregs == 0)
    {
        return  -1;
    }

    int  n_meta = 0;
    std::stringstream  sqlstmt;

    static char const * _D = ",";
    char const *  D = ldndc_empty_cstring;

    if ( _record_meta->useregs & io::CLIENT_SOURCE)
    {
        sqlstmt << D << io::record_meta_ids[io::_CLIENT_SOURCE];
        if ( _with_dtype)
        {
            sqlstmt << " TEXT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::CLIENT_ID)
    {
        sqlstmt << D << io::record_meta_ids[io::_CLIENT_ID];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::CLIENT_X)
    {
        sqlstmt << D << io::record_meta_ids[io::_CLIENT_X];
        if ( _with_dtype)
        {
            sqlstmt << " REAL";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::CLIENT_Y)
    {
        sqlstmt << D << io::record_meta_ids[io::_CLIENT_Y];
        if ( _with_dtype)
        {
            sqlstmt << " REAL";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::CLIENT_Z)
    {
        sqlstmt << D << io::record_meta_ids[io::_CLIENT_Z];
        if ( _with_dtype)
        {
            sqlstmt << " REAL";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::CLIENT_AREA)
    {
        sqlstmt << D << io::record_meta_ids[io::_CLIENT_AREA];
        if ( _with_dtype)
        {
            sqlstmt << " REAL";
        }
        D = _D;
        ++n_meta;
    }

    if ( _record_meta->useregs & io::DATETIME)
    {
        sqlstmt << D << io::record_meta_ids[io::_DATETIME];
        if ( _with_dtype)
        {
            sqlstmt << " TEXT";
        }
        D = _D;
        ++n_meta;
    }

    if ( _record_meta->useregs & io::YEAR)
    {
        sqlstmt << D << io::record_meta_ids[io::_YEAR];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::MONTH)
    {
        sqlstmt << D << io::record_meta_ids[io::_MONTH];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::DAY)
    {
        sqlstmt << D << io::record_meta_ids[io::_DAY];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::HOUR)
    {
        sqlstmt << D << io::record_meta_ids[io::_HOUR];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::MINUTE)
    {
        sqlstmt << D << io::record_meta_ids[io::_MINUTE];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::SECOND)
    {
        sqlstmt << D << io::record_meta_ids[io::_SECOND];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::JULIANDAY)
    {
        sqlstmt << D << io::record_meta_ids[io::_JULIANDAY];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::SUBDAY)
    {
        sqlstmt << D << io::record_meta_ids[io::_SUBDAY];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }
    if ( _record_meta->useregs & io::SEQNB)
    {
        sqlstmt << D << io::record_meta_ids[io::_SEQNB];
        if ( _with_dtype)
        {
            sqlstmt << " INT";
        }
        D = _D;
        ++n_meta;
    }

    *_record_meta_str = sqlstmt.str();

    return  n_meta;
}

int
oproc_landscape_owriter_sqlite3_t::collective_commit_commence(
        cbm::sclock_t const *)
{
    return  0;
}
int
oproc_landscape_owriter_sqlite3_t::collective_commit_complete(
        cbm::sclock_t const *)
{
    ++this->commit_count;
    return  0;
}

lerr_t
oproc_landscape_owriter_sqlite3_t::dispatch_write(
        oproc_receiver_t *  _receiver,
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    this->mark_as_dirty();
    oproc_owriter_sqlite3_t *  this_owriter = static_cast< oproc_owriter_sqlite3_t * >( _receiver);

    lerr_t  rc_write = this_owriter->write_record( _client, _record);
    if (( rc_write == LDNDC_ERR_OK) && this_owriter->need_flush())
    {
        cbm::omp::set_lock( &this->owriter_sqlite3_lock);
        this->flush_owriter( this_owriter);
        cbm::omp::unset_lock( &this->owriter_sqlite3_lock);
    }

    return  rc_write;
}

}

