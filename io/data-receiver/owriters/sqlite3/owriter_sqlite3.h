/*!
 * @brief
 *    sqlite3 output writer
 *
 * @author
 *    steffen klatt (created on: apr 09, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_SQLITE3_H_
#define  LDNDC_IO_OUTPUTWRITER_SQLITE3_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/receiver-container.h"
#include  "io/sink-info.h"

#include  "time/cbm_time.h"
#include  "openmp/cbm_omp.h"
#include  <sqlite3.h>

#ifndef  LDNDC_OWRITER_SQLITE_BUFFER_SIZE
#  define  LDNDC_OWRITER_SQLITE_BUFFER_SIZE  4096
#endif
#define  LDNDC_SQLITE3_STATEMENT_MAXSIZE  4096 /* a minimum of N/CBM_STRING_SIZE columns*/
namespace ldndc {

class  CBM_API  oproc_owriter_sqlite3_base_t  :  public  oproc_owriter_base_t
{
    protected:
        oproc_owriter_sqlite3_base_t();
        virtual  ~oproc_owriter_sqlite3_base_t() = 0;
};

class  CBM_API  oproc_owriter_sqlite3_t  :  public  oproc_owriter_sqlite3_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("sqlite3")
    LDNDC_SERVER_OBJECT(oproc_owriter_sqlite3_t)
    public:
        lreceiver_descriptor_t  descriptor() const { return  this->rcv_d_; }
        void  set_descriptor(
                lreceiver_descriptor_t const &  _rcv_d)
        {
            this->rcv_d_ = _rcv_d;
        }
        lerr_t  status() const { return  ( this->state.ok) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

    public:
        lerr_t  set_attributes(
                sink_attributes_t const *);

        /* only used for fixed layouts */
        lerr_t  set_layout_owriter(
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const *,
                char const * /*sink identifier*/);
        sink_layout_t const *  layout(
                sink_layout_hint_t const *) const { return  NULL; }
        sink_entities_t const *  entities(
                sink_layout_hint_t const *) const { return  NULL; }

    public:
        lerr_t  write_record(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);

        lerr_t  discard_buffer_content() { return  LDNDC_ERR_OK; }
        size_t  buffer_size() const { return  this->buf.p_slot; }
        size_t  buffer_size_limit() const { return  this->buf.n_slots; }

        bool  need_flush() const;
        lerr_t  commit_transactions(
                sqlite3 *, sqlite3_stmt *);

    private:
        char  isodate[CBM_ISO8601_MAXLEN];
        lerr_t  commit_record_meta(
                sqlite3_stmt *, int * /*index offset*/,
                sink_client_t const * /*client identifier*/,
                sink_record_meta_srv_t const * /*record meta info*/);
        lerr_t  commit_records(
                sqlite3_stmt *, int * /*index offset*/, void ** /*data*/);
        lerr_t  write_record_(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);
        lerr_t  write_zero_record_(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);

    protected:
        struct  buffer_item_t
        {
            void **  mem;
            sink_record_meta_srv_t  meta;
            int  t;
        };
        struct  buffer_t
        {
            buffer_item_t *  slots;
            int  n_slots;
            int  p_slot;

            sink_layout_t const *  layout;
            sink_client_t  client;
        };
        buffer_t  buf;

    private:
        lreceiver_descriptor_t  rcv_d_;

    public:
        oproc_owriter_sqlite3_t();
        virtual  ~oproc_owriter_sqlite3_t();
};


class  CBM_API  oproc_landscape_owriter_sqlite3_base_t  :  public  oproc_landscape_owriter_base_t
{
    protected:
        oproc_landscape_owriter_sqlite3_base_t();
        virtual  ~oproc_landscape_owriter_sqlite3_base_t() = 0;
};


class  CBM_API  oproc_landscape_owriter_sqlite3_t  :  public  oproc_landscape_owriter_sqlite3_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("sqlite3")
    LDNDC_SERVER_OBJECT(oproc_landscape_owriter_sqlite3_t)
    public:
        typedef  oproc_owriter_sqlite3_t  receiver_t;

    public:
        lerr_t  open(
                sink_info_t const *);
        lerr_t  reopen();

        lerr_t  close();

        void  mark_as_dirty() { this->is_clean_ = 0; }

        lerr_t  status() const { return  this->state.ok ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

        lerr_t  flush_all();
        lerr_t  flush_owriter(
                oproc_owriter_base_t *);
        oproc_owriter_base_t *  acquire_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);
        void  release_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);

    public:
        lerr_t  set_layout_owriter(
                oproc_owriter_base_t * /*target block*/,
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*layout&entities*/);
        static  int  generate_record_meta_sqlstmt(
                std::string * /*result buffer*/,
                sink_record_meta_srv_t const * /*record meta info*/,
                bool = true /*with datatypes*/);

    public:
        int  collective_commit_commence(
                cbm::sclock_t const *);
        int  collective_commit_complete(
                cbm::sclock_t const *);

        lerr_t  dispatch_write(
                oproc_receiver_t * /*target receiver*/,
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);


    protected:
        lerr_t  create_table(
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*layout&entities*/,
                char const * /*sink identifier*/);

        lerr_t  connect(
                sink_info_t const * /**/);

    protected:
        sink_entity_layout_t  fmt;
        sink_info_t  sink_info;

        sqlite3 *  db_handle;
        sqlite3_stmt *  db_insert_stmt;
        int  commit_count;

    private:
        int  is_clean_;
        int  is_layouted_;
        int  is_open_;

    private:
        cbm::omp::omp_lock_t  owriter_sqlite3_lock;

    public:
        oproc_landscape_owriter_sqlite3_t();
        virtual  ~oproc_landscape_owriter_sqlite3_t();

        struct  release_op_t
        {
            void    operator()(
                    oproc_owriter_sqlite3_t *  _owriter) const
            {
                _owriter->commit_transactions( this->db, this->stmt);
            }
            sqlite3 *  db;
            sqlite3_stmt *  stmt;
        };
    private:
        oproc_receiver_container_t< oproc_landscape_owriter_sqlite3_t >  owriters_;
};

}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_SQLITE3_H_  */

