/*!
 * @brief
 *    XML output writer  (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 30, 2014)
 */

#include  "io/data-receiver/owriters/xml/owriter_xml.h"
#include  "io/data-receiver/owriters/owriter-layout.h"
#include  "io/default-streams.h"

#include  "string/cbm_string.h"
#include  "time/cbm_time.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

oproc_owriter_xml_base_t::oproc_owriter_xml_base_t()
        : oproc_owriter_base_t()
{
}


oproc_owriter_xml_base_t::~oproc_owriter_xml_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_owriter_xml_t)
oproc_owriter_xml_t::oproc_owriter_xml_t()
        : oproc_owriter_xml_base_t(),
          fmt( sink_entity_layout_defaults),
          attrs( NULL),
          rcv_d_( invalid_t< lreceiver_descriptor_t >::value),
          xml_node( NULL)
{
}
oproc_owriter_xml_t::~oproc_owriter_xml_t()
{
    crabmeat_assert( this->attrs);
    if ( !this->attrs->uniform)
    {
        oproc_owriter_layouthelper_t::free_entity_layout( &this->fmt);
    }
}


lerr_t
oproc_owriter_xml_t::set_attributes(
        sink_attributes_t const *  _attrs)
{
    this->attrs = _attrs;
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_xml_t::set_layout_owriter(
        sink_entity_layout_t const *  _layout)
{
    if ( !this->attrs)
    {
        CBM_LogDebug( "oproc_owriter_xml_t::set_layout_owriter(): attributes not set");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( this->attrs->uniform)
    {
        this->fmt = ( _layout) ? *_layout : sink_entity_layout_defaults;
        return  LDNDC_ERR_OK;
    }

    if ( this->fmt.ents.ids || this->fmt.layout.types)
    {
        CBM_LogDebug( "oproc_owriter_xml_t::set_layout_owriter(): redefining layout not supported");
        /* already done (error?, redefine?) */
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( !_layout || _layout->layout.rank == 0 || !_layout->layout.types || !_layout->layout.sizes)
    {
        CBM_LogDebug( "oproc_owriter_xml_t::set_layout_owriter(): missing or incorrect layout information");
        if ( _layout)
        {
            CBM_LogDebug( "oproc_owriter_xml_t::set_layout_owriter(): rank=",(int)_layout->layout.rank, "  types=",(void*)_layout->layout.types, "  sizes=",(void*)_layout->layout.sizes);
        }
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    lerr_t  rc_setlayout = oproc_owriter_layouthelper_t::set_layout( &this->fmt, _layout);
    RETURN_IF_NOT_OK(rc_setlayout);

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_xml_t::write_block_meta(
        sink_client_t const *  _client,
        sink_record_meta_srv_t const *  _record_meta)
{
    crabmeat_assert( _client && _record_meta);

    if ( _record_meta->useregs & io::CLIENT_SOURCE)
    {
        static_cast< tinyxml2::XMLElement * >( this->xml_node)->SetAttribute(
                io::record_meta_ids[io::_CLIENT_SOURCE], _client->target_sid);
    }
    if ( _record_meta->useregs & io::CLIENT_ID)
    {
        static_cast< tinyxml2::XMLElement * >( this->xml_node)->SetAttribute(
                io::record_meta_ids[io::_CLIENT_ID], _client->target_id);
    }
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_xml_t::write_record(
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( _record);
    sink_layout_t const *  s_layout = ( _record->layout.rank == 0) ? &this->fmt.layout : &_record->layout;

    if ( _client)
    {
        if ( !this->xml_node->ToElement()->Attribute( io::record_meta_ids[io::_CLIENT_ID]))
        {
            this->write_block_meta( _client, &_record->meta);
        }
    }

    if ( _record->gap == ldndc::SINKGAP_NONE)
    {
        lerr_t  rc_write = this->write_record_payload( _record, s_layout);
        RETURN_IF_NOT_OK(rc_write);

        return  LDNDC_ERR_OK;
    }
    else if ( _record->gap == ldndc::SINKGAP_ZERO)
    {
        return  LDNDC_ERR_OK;
    }

    /* do not know how to handle request */
    return  LDNDC_ERR_REQUEST_MISMATCH;

}

lerr_t
oproc_owriter_xml_t::write_record_meta(
        tinyxml2::XMLElement *  _node,
        sink_record_meta_srv_t const *  _record_meta)
{
    if ( _record_meta)
    {
        sink_record_meta_t  m;
        m.timestamp_from_scalar( _record_meta->ts);

        int  have_time = 0;
        if ((( _record_meta->useregs & io::YEAR) && 
            ((( _record_meta->useregs & io::MONTH) && ( _record_meta->useregs & io::DAY))
             || (_record_meta->useregs & io::JULIANDAY))))
        {
            have_time = 1;
        }

        ldndc_uint32_t  year=1, month=1, day=1,
                hour=0, minute=0, second=0, subday=1;

        if ( _record_meta->useregs & io::YEAR)
        {
            year = static_cast< ldndc_uint32_t >( m.t.reg.year);
            if ( !have_time)
                { _node->SetAttribute( "year", year); }
        }
        if ( _record_meta->useregs & io::MONTH)
        {
            month = static_cast< ldndc_uint32_t >( m.t.reg.month);
            if ( !have_time)
                { _node->SetAttribute( "month", month); }
        }
        if ( _record_meta->useregs & io::DAY)
        {
            day = static_cast< ldndc_uint32_t >( m.t.reg.day);
            if ( !have_time)
                { _node->SetAttribute( "day", day); }
        }
        if ( _record_meta->useregs & io::HOUR)
        {
            hour = static_cast< ldndc_uint32_t >( m.t.reg.hour);
            if ( !have_time)
                { _node->SetAttribute( "hour", hour); }
        }
        if ( _record_meta->useregs & io::MINUTE)
        {
            minute = static_cast< ldndc_uint32_t >( m.t.reg.minute);
            if ( !have_time)
                { _node->SetAttribute( "minute", minute); }
        }
        if ( _record_meta->useregs & io::SECOND)
        {
            second = static_cast< ldndc_uint32_t >( m.t.reg.second);
            if ( !have_time)
                { _node->SetAttribute( "second", second); }
        }
        if ( _record_meta->useregs & io::JULIANDAY)
        {
            int const  julianday =
                static_cast< ldndc_uint32_t >( m.t.reg.julianday);
            if ( !have_time)
                { _node->SetAttribute( "julianday", julianday); }
        }
        if ( _record_meta->useregs & io::SUBDAY)
        {
            subday = static_cast< ldndc_uint32_t >( m.t.reg.subday);
            if ( !have_time)
                { _node->SetAttribute( "subday", subday); }
        }
        if ( _record_meta->useregs & io::SEQNB)
        {
            _node->SetAttribute( "seqnb",
                static_cast< ldndc_int32_t >( _record_meta->record_seqnb));
        }

        if ( have_time)
        {
            char  rectime[16];
            cbm::snprintf( rectime, 15, "%4u-%02u-%02u-%02u", year, month, day, subday);
            _node->SetAttribute( "time", rectime);
        }
    }

    return  LDNDC_ERR_OK;
}

static
tinyxml2::XMLNode *
oproc_owriter_xml_add_record_entity_node(
        tinyxml2::XMLNode *  _node,
        char const *  _tag, char const *  _value)
{
    tinyxml2::XMLNode * newnode =
        _node->GetDocument()->NewElement( _tag);
    if ( !newnode)
    {
        CBM_LogError( "nomem");
        return  NULL;
    }
    _node->InsertEndChild( newnode);
    tinyxml2::XMLNode *  newtext =
        _node->GetDocument()->NewText( _value);
    if ( !newtext)
    {
        CBM_LogError( "nomem");
        return  NULL;
    }
    newnode->InsertEndChild( newtext);
    return  newnode;
}

lerr_t
oproc_owriter_xml_t::write_record_payload(
        sink_record_srv_t const *  _record,
        sink_layout_t const *  _layout)
{
    if ( !_record->data)
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_write = LDNDC_ERR_OK;

    tinyxml2::XMLNode *  node = this->xml_node;
    tinyxml2::XMLNode *  newnode = NULL;

    static const size_t  n_value_str = 127;
    char  value_str[n_value_str+1];

    int const  r_end = static_cast< int >( _layout->rank);
    int  h = 0;
    int  j = 0;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = _layout->types[r];
        int const  this_size = _layout->sizes[r];

        if ( this_datatype == LDNDC_FLOAT64)
        {
            ldndc_flt64_t const *  d = (ldndc_flt64_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_FLT64, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_UINT32)
        {
            ldndc_uint32_t const *  d = (ldndc_uint32_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_UINT32, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_INT32)
        {
            ldndc_int32_t const *  d = (ldndc_int32_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_INT32, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_FLOAT32)
        {
            ldndc_flt32_t const *  d = (ldndc_flt32_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_FLT32, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_STRING)
        {
            ldndc_string_t const *  d = (ldndc_string_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_STRING, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_UINT64)
        {
            ldndc_uint64_t const *  d = (ldndc_uint64_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_UINT64, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_INT64)
        {
            ldndc_int64_t const *  d = (ldndc_int64_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                cbm::snprintf( value_str, n_value_str, LDNDC_PRINTF_SEQ_INT64, *d);
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], value_str);
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_BOOL)
        {
            ldndc_bool_t const *  d = (ldndc_bool_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                newnode = oproc_owriter_xml_add_record_entity_node(
                    node, this->fmt.ents.ids[h+j], ( *d ? "1" : "0"));
                if ( newnode)
                {
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
                else { rc_write = LDNDC_ERR_FAIL; }
            }
        }
        else if ( this_datatype == LDNDC_RAW)
        {
            ldndc_raw_t const *  d = (ldndc_raw_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                char const *  tag = this->fmt.ents.ids[h+j];

                if ( *tag == '$')
                {
                    tag = *d;
                    if ( node->FirstChildElement( tag))
                    {
                        node = node->FirstChildElement( tag);
                        continue;
                    }
                }

                newnode = this->xml_node->GetDocument()->NewElement( tag);
                if ( !newnode)
                {
                    CBM_LogError( "nomem");
                    return  LDNDC_ERR_NOMEM;
                }
                node->InsertEndChild( newnode);
                if ( tag == *d)
                {
                    node = newnode;
                }
                else
                {
                    tinyxml2::XMLNode *  newtext =
                        this->xml_node->GetDocument()->NewText( *d);
                    newnode->InsertEndChild( newtext);
                    lerr_t  rc_meta =
                        this->write_record_meta( newnode->ToElement(), &_record->meta);
                    if ( rc_meta) { rc_write = LDNDC_ERR_FAIL; }
                }
            }
        }
        else if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
        }
        else
        {
            CBM_LogError( "[ERROR]  oproc_owriter_xml_t::write_record(): ",
                    "datatype not supported  [datatype=",LDNDC_ATOMIC_DATATYPE_NAMES[this_datatype],"]");
            return  LDNDC_ERR_FAIL;
        }

        h += this_size;
    }

    return  rc_write;
}

oproc_landscape_owriter_xml_base_t::oproc_landscape_owriter_xml_base_t()
        : oproc_landscape_owriter_base_t()
{
}


oproc_landscape_owriter_xml_base_t::~oproc_landscape_owriter_xml_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_landscape_owriter_xml_t)
oproc_landscape_owriter_xml_t::oproc_landscape_owriter_xml_t()
        : oproc_landscape_owriter_xml_base_t(),
          fmt( sink_entity_layout_defaults),

          stdstream( NULL),

          is_layouted_( 0),
          is_open_( 0)
{
    cbm::omp::init_lock( &this->owriter_xml_lock);
}
oproc_landscape_owriter_xml_t::~oproc_landscape_owriter_xml_t()
{
    (void)this->close();
    this->release_owriter( invalid_t< lreceiver_descriptor_t >::value /*==all*/);

    if ( this->sink_info.attrs.uniform)
    {
        oproc_owriter_layouthelper_t::free_entity_layout( &this->fmt);
    }

    cbm::omp::destroy_lock( &this->owriter_xml_lock);
}

lerr_t
oproc_landscape_owriter_xml_t::open(
        sink_info_t const *  _sink_info)
{
    crabmeat_assert( _sink_info);
    if ( this->is_open_)
    {
        /* attempt to open sink again? close first. */
        return  LDNDC_ERR_OK;
    }

    this->sink_info = *_sink_info;

    if ( ldndc::sink_is_black_hole( _sink_info))
    {
        CBM_LogFatal( "[BUG]  oproc_landscape_owriter_xml_t::open(): ",
                "i think we inofficially agreed on not to instantiate receiver for black holes (..well, ok, i was the only voter)");
        return  LDNDC_ERR_FAIL;
    }

    cbm::string_t const  root_tag = cbm::string_t( "ldndc")+ this->sink_info.identifier;
    tinyxml2::XMLElement *  root_element = this->xml_document.NewElement( root_tag.c_str());
    if ( !root_element)
    {
        CBM_LogError( "Failed to create root element \"",root_tag, "\" for sink  [sink=",_sink_info->identifier,"]");
        return  LDNDC_ERR_FAIL;
    }
    static_cast< tinyxml2::XMLNode * >( this->xml_document.ToDocument())->InsertEndChild( root_element);

    this->is_open_ = 1;

    if ( cbm::is_equal_i( _sink_info->name, STANDARD_SINK_NAMES[STANDARD_SINK_OUT]))
    {
        this->stdstream = stdout;
    }
    else if ( cbm::is_equal_i( _sink_info->name, STANDARD_SINK_NAMES[STANDARD_SINK_ERR])
        || cbm::is_equal_i( _sink_info->name, STANDARD_SINK_NAMES[STANDARD_SINK_LOG]))
    {
        this->stdstream = stderr;
    }
    else
    {
        this->stdstream = NULL;
    }
        return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_xml_t::reopen()
{
    if ( this->is_open_)
    {
        if ( this->sink_info.attrs.overwrite == 0)
        {
            CBM_LogError( "oproc_landscape_owriter_xml_t::reopen(): ",
                    "attempting to reopen an overwrite protected sink  [sink=",this->sink_info.identifier,"]");
            return  LDNDC_ERR_INVALID_OPERATION;
        }
        this->close();
    }
    return  this->open( &this->sink_info);
}


lerr_t
oproc_landscape_owriter_xml_t::close()
{
        if ( this->is_open_)
    {
        if ( this->stdstream)
        {
            this->xml_document.SaveFile( this->stdstream);
            this->stdstream = NULL;
        }
        else
        {
            this->xml_document.SaveFile(
                    this->sink_info.full_name().c_str());
        }
        this->xml_document.DeleteChildren();

        this->is_open_ = 0;
// sk:dbg        CBM_LogDebug( "oproc_landscape_owriter_xml_t::close(): successfully closed sink  ",
// sk:dbg                "[sink=",this->sink_info.identifier,",file=",this->sink_info.full_name(),"]");
        }

    return  LDNDC_ERR_OK;
}


lerr_t
oproc_landscape_owriter_xml_t::flush_owriter(
        oproc_owriter_base_t *)
{
    return  LDNDC_ERR_OK;
}    

oproc_owriter_base_t *
oproc_landscape_owriter_xml_t::acquire_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    oproc_owriter_base_t *  new_owriter =
               this->owriters_.acquire_receiver( _owriter_descriptor);
    if ( !new_owriter)
    {
        return  NULL;
    }

    lerr_t  rc_set_attrs =
               static_cast< oproc_owriter_xml_t * >( new_owriter)->set_attributes( &this->sink_info.attrs);
    if ( rc_set_attrs)
    {
        return  NULL;
    }

    /* is anyone already working on this block? */
    if ( !static_cast< oproc_owriter_xml_t * >( new_owriter)->get_node())
    {
        tinyxml2::XMLElement *  root_node =
            this->xml_document.RootElement();
        if ( !root_node)
        {
            CBM_LogError( "no root node");
            return  NULL;
        }

        /* check if block already exists */
        std::stringstream  owriter_desc_ss;
        owriter_desc_ss << _owriter_descriptor;
        std::string const  owriter_desc_str( owriter_desc_ss.str());
        tinyxml2::XMLElement *  node =
            root_node->FirstChildElement( this->sink_info.identifier.c_str());
        for ( ; node; node = node->NextSiblingElement( this->sink_info.identifier.c_str()))
        {
            if ( cbm::is_equal( node->Attribute( "receiver_descriptor"), owriter_desc_str.c_str()))
            {
                static_cast< oproc_owriter_xml_t * >( new_owriter)->set_node( node);
            }
        }
        /* create if block was not found */
        if ( !static_cast< oproc_owriter_xml_t * >( new_owriter)->get_node())
        {
            tinyxml2::XMLNode *  new_block =
                this->xml_document.NewElement( this->sink_info.identifier.c_str());
            new_block->ToElement()->SetAttribute( "receiver_descriptor", owriter_desc_str.c_str());
            root_node->InsertEndChild( new_block);
            static_cast< oproc_owriter_xml_t * >( new_owriter)->set_node( new_block);
        }
    }

    return  new_owriter;
}
void
oproc_landscape_owriter_xml_t::release_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    this->owriters_.release_receiver( _owriter_descriptor, NULL);
}

lerr_t
oproc_landscape_owriter_xml_t::set_layout_owriter(
        oproc_owriter_base_t *  _target,
        sink_record_meta_srv_t const *  /*_record_meta*/,
        sink_entity_layout_t const *  _layout)
{
    crabmeat_assert( _target);
    oproc_owriter_xml_t *  this_target = static_cast< oproc_owriter_xml_t *>( _target);

#ifdef  CRABMEAT_SANITY_CHECKS
    if ( !this->is_open_)
    {
        CBM_LogError( "[BUG]  oproc_landscape_owriter_xml_t::set_layout_owriter(): attempt to define entities for closed stream");
        return  LDNDC_ERR_FAIL;
    }
#endif  /*  CRABMEAT_SANITY_CHECKS  */

    lerr_t  rc_defent = LDNDC_ERR_OK;
    if ( this->sink_info.attrs.uniform)
    {
        cbm::omp::set_lock( &this->owriter_xml_lock, "layout");
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (is_layouted_)
#endif
        if ( !this->is_layouted_ && _layout)
        {
            rc_defent = oproc_owriter_layouthelper_t::set_layout( &this->fmt, _layout);
            if ( _layout->layout.entsizes)
            {
                CBM_LogError( "oproc_landscape_owriter_xml_t::set_layout_owriter(): ",
                        "no support for vector-valued entities");
                rc_defent = LDNDC_ERR_INVALID_OPERATION;
            }
        }
        if ( rc_defent == LDNDC_ERR_OK)
        {
            rc_defent = this_target->set_layout_owriter( &this->fmt);
        }
        if ( rc_defent == LDNDC_ERR_OK)
        {
            this->is_layouted_ = 1;
        }
        else
        {
            CBM_LogError( "oproc_landscape_owriter_xml_t::set_layout_owriter(): setting layout failed");
        }
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(is_layouted_)
#endif
        cbm::omp::unset_lock( &this->owriter_xml_lock, "layout");
    }
    else
    {
        rc_defent = this_target->set_layout_owriter( _layout);
    }

    return  rc_defent;
}

int
oproc_landscape_owriter_xml_t::collective_commit_commence(
        cbm::sclock_t const *)
{
    return  0;
}
int
oproc_landscape_owriter_xml_t::collective_commit_complete(
        cbm::sclock_t const *)
{
    return  0;
}


}

