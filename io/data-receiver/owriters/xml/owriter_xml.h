/*!
 * @brief
 *    XML output writer
 *
 * @author
 *    steffen klatt (created on: nov 30, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_XML_H_
#define  LDNDC_IO_OUTPUTWRITER_XML_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/receiver-container.h"
#include  "io/sink-info.h"

#include  "time/cbm_time.h"

#include  <tinyxml2.h>

namespace ldndc {

class  CBM_API  oproc_owriter_xml_base_t  :  public  oproc_owriter_base_t
{
    public:
        static ldndc_string_t const  no_entname_name;

    protected:
        oproc_owriter_xml_base_t();
        virtual  ~oproc_owriter_xml_base_t() = 0;
};

class  CBM_API  oproc_owriter_xml_t  :  public  oproc_owriter_xml_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("xml")
    LDNDC_SERVER_OBJECT(oproc_owriter_xml_t)
    public:
        lreceiver_descriptor_t  descriptor() const { return  this->rcv_d_; }
        void  set_descriptor(
                lreceiver_descriptor_t const &  _rcv_d)
        {
            this->rcv_d_ = _rcv_d;
        }
        lerr_t  status() const { return  ( this->state.ok) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

    public:
        lerr_t  set_attributes(
                sink_attributes_t const *);

        /* only used for fixed layouts */
        lerr_t  set_layout_owriter(
                sink_entity_layout_t const *);
        sink_layout_t const *  layout(
                sink_layout_hint_t const *) const { return  &this->fmt.layout; }
        sink_entities_t const *  entities(
                sink_layout_hint_t const *) const { return  &this->fmt.ents; }

        tinyxml2::XMLNode const *  get_node() const { return  this->xml_node; }
        void  set_node(
                tinyxml2::XMLNode *  _node) { this->xml_node = _node; }

    public:
        lerr_t  write_record(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);

        lerr_t  discard_buffer_content() { return  LDNDC_ERR_OK; }
        size_t  buffer_size() const { return  0; }
        size_t  buffer_size_limit() const { return  0; }

    private:
        lerr_t  write_block_meta(
                sink_client_t const *  /*_client*/,
                sink_record_meta_srv_t const * /*record meta info*/);
        lerr_t  write_record_meta(
                tinyxml2::XMLElement *,
                sink_record_meta_srv_t const * /*record meta info*/);
        lerr_t  write_record_payload(
                sink_record_srv_t const * /*record*/,
                sink_layout_t const * /*use this layout*/);


    protected:
        sink_entity_layout_t  fmt;
        sink_attributes_t const *  attrs;
    private:
        lreceiver_descriptor_t  rcv_d_;
        tinyxml2::XMLNode *  xml_node;

    public:
        oproc_owriter_xml_t();
        virtual  ~oproc_owriter_xml_t();
};


class  CBM_API  oproc_landscape_owriter_xml_base_t  :  public  oproc_landscape_owriter_base_t
{
    protected:
        oproc_landscape_owriter_xml_base_t();
        virtual  ~oproc_landscape_owriter_xml_base_t() = 0;
};


class  CBM_API  oproc_landscape_owriter_xml_t  :  public  oproc_landscape_owriter_xml_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("xml")
    LDNDC_SERVER_OBJECT(oproc_landscape_owriter_xml_t)

    public:
        typedef  oproc_owriter_xml_t  receiver_t;

    public:
        lerr_t  open(
                sink_info_t const *);
        lerr_t  reopen();

        lerr_t  close();

        lerr_t  flush_owriter(
                oproc_owriter_base_t *);
        void  mark_as_dirty() { }

        lerr_t  status() const { return  this->state.ok ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

        oproc_owriter_base_t *  acquire_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);
        void  release_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);

    public:
        lerr_t  set_layout_owriter(
                oproc_owriter_base_t * /*target block*/,
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*layout&entities*/);

    public:
        int  collective_commit_commence(
                cbm::sclock_t const *);
        int  collective_commit_complete(
                cbm::sclock_t const *);

    protected:
        sink_entity_layout_t  fmt;
        sink_info_t  sink_info;

    private:
        tinyxml2::XMLDocument  xml_document;

        FILE *  stdstream;
        int  is_layouted_;
        int  is_open_;

    private:
        cbm::omp::omp_lock_t  owriter_xml_lock;

    public:
        oproc_landscape_owriter_xml_t();
        virtual  ~oproc_landscape_owriter_xml_t();

        struct  release_op_t
        {
            void  operator()(
                    oproc_owriter_xml_t *) const {};
        };
    private:
        oproc_receiver_container_t< oproc_landscape_owriter_xml_t >  owriters_;
};

}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_XML_H_  */

