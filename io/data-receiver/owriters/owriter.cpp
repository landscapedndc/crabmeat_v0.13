/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013)
 */

#include  "io/data-receiver/owriters/owriter.h"

namespace ldndc {

oproc_owriter_interface_t::oproc_owriter_interface_t()
{
}
oproc_owriter_interface_t::~oproc_owriter_interface_t()
{
}

oproc_owriter_base_t::oproc_owriter_base_t()
        : oproc_owriter_interface_t(),
          oproc_receiver_t()
{
}
oproc_owriter_base_t::~oproc_owriter_base_t()
{
}

oproc_landscape_owriter_interface_t::oproc_landscape_owriter_interface_t()
{
}
oproc_landscape_owriter_interface_t::~oproc_landscape_owriter_interface_t()
{
}

} /* namespace ldndc */

#include  "time/cbm_time.h"
#include  "cbm_rtcfg.h"
static int  s_commit( void * _obj, cbm::sclock_t const *  _clk)
{
    ldndc::oproc_landscape_owriter_base_t *  owriter =
        static_cast< ldndc::oproc_landscape_owriter_base_t * >( _obj);
    if ( owriter)
    {
        int rc_complete = owriter->collective_commit_complete( _clk);
        if ( rc_complete)
            { return -1; }
        int rc_commence = owriter->collective_commit_commence( _clk);
        if ( rc_commence)
            { return -1; }
    }
    return 0;
}

namespace ldndc {

oproc_landscape_owriter_base_t::oproc_landscape_owriter_base_t()
        : oproc_landscape_owriter_interface_t(),
          oproc_landscape_receiver_t()
{
    CBM_LibRuntimeConfig.clk->register_event(
            "on-leave-timestep", &s_commit, this, "output-writer");
}
oproc_landscape_owriter_base_t::~oproc_landscape_owriter_base_t()
{
    CBM_LibRuntimeConfig.clk->unregister_event(
            "on-leave-timestep", &s_commit, this, "output-writer");
}

oproc_receiver_t *
oproc_landscape_owriter_base_t::acquire_receiver(
        lreceiver_descriptor_t const &  _writer_descriptor)
{
    return  static_cast< oproc_receiver_t * >( this->acquire_owriter( _writer_descriptor));
}
void
oproc_landscape_owriter_base_t::release_receiver(
        lreceiver_descriptor_t const &  _writer_descriptor)
{
    this->release_owriter( _writer_descriptor);
}
 
lerr_t
oproc_landscape_owriter_base_t::set_fixed_layout(
        oproc_receiver_t *  _target,
        sink_record_meta_srv_t const *  _record_meta,
        sink_entity_layout_t const *  _layout)
{
    return  this->set_layout_owriter( static_cast< oproc_owriter_base_t * >( _target), _record_meta, _layout);
}

lerr_t
oproc_landscape_owriter_base_t::flush()
{
    return  this->flush_owriter( static_cast< oproc_owriter_base_t * >( NULL));
}
lerr_t
oproc_landscape_owriter_base_t::flush(
        oproc_receiver_t *  _receiver)
{
    return  this->flush_owriter( static_cast< oproc_owriter_base_t * >( _receiver));
}


}

