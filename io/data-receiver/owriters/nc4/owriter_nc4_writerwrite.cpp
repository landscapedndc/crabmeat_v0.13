/*!
 * @brief
 *    netCDF4 output writer: write functions  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_writer.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

lerr_t
oproc_owriter_nc4_t::write_record(
        sink_client_t const *  /*_client*/,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( _record);
    sink_layout_t const *  s_layout = ( _record->layout.rank == 0) ? &this->fmt->layout : &_record->layout;

// sk:TODO  write record meta information

    if ( _record->gap == ldndc::SINKGAP_NONE)
    {
        lerr_t  rc_write = this->write_record_( _record, s_layout);
        RETURN_IF_NOT_OK(rc_write);

        return  LDNDC_ERR_OK;
    }
    else if ( _record->gap == ldndc::SINKGAP_ZERO)
    {
        lerr_t  rc_writezero = this->write_zero_record_( s_layout);
        RETURN_IF_NOT_OK(rc_writezero);

        return  LDNDC_ERR_OK;
    }

    /* do not know how to handle request */
    return  LDNDC_ERR_REQUEST_MISMATCH;
}

lerr_t
oproc_owriter_nc4_t::write_record_(
        sink_record_srv_t const *  _record,
        sink_layout_t const *  _layout)
{
    crabmeat_assert( _record);
    crabmeat_assert( _layout);
    if ( !_record->data)
    {
        return  LDNDC_ERR_OK;
    }

    crabmeat_assert( _record->seq);
    crabmeat_assert( _record->data);
    crabmeat_assert( _layout->types);
    crabmeat_assert( _layout->sizes);

    int  rc_write = 0;
    int  var_nb = 0;

    int const  r_end = static_cast< int >( _layout->rank);
    int const  R = r_end - 1;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        crabmeat_assert( _record->data[r]);

        atomic_datatype_t const  this_datatype = _layout->types[r];
        int const  this_size = _layout->sizes[r];

//        CBM_LogDebug( "writing #var=",var_nb,"\tt=",_record->seq->timestep, " map=",_record->seq->records_in_timestep, "  size=",this_size, "   seq=",_record->seq->timestep, ":",_record->seq->records_in_timestep);

        if ( this_datatype == LDNDC_FLOAT64)
        {
            rc_write = this->write_record_to_cache< ldndc_flt64_t >( &var_nb, _record->seq, _record->data[r], this_size);
        }
        else if ( this_datatype == LDNDC_INT32)
        {
            rc_write = this->write_record_to_cache< ldndc_int32_t >( &var_nb, _record->seq, _record->data[r], this_size);
        }
// sk:off        else if ( this_datatype == LDNDC_UINT32)
// sk:off        {
// sk:off            this->write_record_to_databroker_< >( this->buf_->_data, _record->data[r], this_size);
// sk:off            ldndc_uint32_t const *  d = (ldndc_uint32_t const *)_record->data[r];
// sk:off        }
// sk:off        else if ( this_datatype == LDNDC_FLOAT32)
// sk:off        {
// sk:off            ldndc_flt32_t const *  d = (ldndc_flt32_t const *)_record->data[r];
// sk:off            this->write_record_to_databroker_< >( this->buf_->_data, _record->data[r], this_size);
// sk:off        }
// sk:off        else if ( this_datatype == LDNDC_STRING)
// sk:off        {
// sk:off            ldndc_string_t const *  d = (ldndc_string_t const *)_record->data[r];
// sk:off            this->write_record_to_databroker_< >( this->buf_->_data, _record->data[r], this_size);
// sk:off        }
// sk:off        else if ( this_datatype == LDNDC_UINT64)
// sk:off        {
// sk:off            ldndc_uint64_t const *  d = (ldndc_uint64_t const *)_record->data[r];
// sk:off            this->write_record_to_databroker_< >( this->buf_->_data, _record->data[r], this_size);
// sk:off        }
// sk:off        else if ( this_datatype == LDNDC_INT64)
// sk:off        {
// sk:off            ldndc_int64_t const *  d = (ldndc_int64_t const *)_record->data[r];
// sk:off            this->write_record_to_databroker_< >( this->buf_->_data, _record->data[r], this_size);
// sk:off        }
// sk:off        else if ( this_datatype == LDNDC_BOOL)
// sk:off        {
// sk:off            ldndc_bool_t const *  d = (ldndc_bool_t const *)_record->data[r];
// sk:off            this->write_record_to_databroker_< >( this->buf_->_data, _record->data[r], this_size);
// sk:off        }
        else if ( this_datatype == LDNDC_COMPOUND)
        {
            CBM_LogWarn( "oproc_owriter_nc4_t::write_record_(): [LDNDC_COMPOUND] not implemented");
// sk:chk            marshal_compound_t const *  m_compound = (marshal_compound_t const *)_record->data[r];
// sk:chk            for ( j = 0;  j < this_size;  ++j, ++m_compound)
// sk:chk            {
// sk:chk                if ( !m_compound)
// sk:chk                {
// sk:chk                    continue;
// sk:chk                }
// sk:chk                sink_record_srv_t  nested_rec( sink_record_srv_defaults);
// sk:chk                nested_rec.layout = m_compound->mi->layout;
// sk:chk                nested_rec.ents = m_compound->mi->ents;
// sk:chk                nested_rec.data = m_compound->data;
// sk:chk
// sk:chk                this->write_record_( &nested_rec, &m_compound->mi->layout);
// sk:chk            }
        }
        else if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
        }
        else
        {
            CBM_LogError( "[ERROR]  oproc_owriter_nc4_t::write_record(): datatype not supported  [datatype=",this_datatype,"]");
// sk:off            rc_write = LDNDC_ERR_FAIL;
        }

        if ( rc_write)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
oproc_owriter_nc4_t::write_zero_record_(
        sink_layout_t const *  _layout)
{
    CBM_LogWarn( "not implemented..");

    int const  r_end = static_cast< int >( _layout->rank);
    int const  R = r_end-1;
    register int  j = 0;
    register char  D = this->attrs->delimiter;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = _layout->types[r];
        int const  this_size = _layout->sizes[r];

        if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
            break;
        }
    }

    return  LDNDC_ERR_OK;
}

}

