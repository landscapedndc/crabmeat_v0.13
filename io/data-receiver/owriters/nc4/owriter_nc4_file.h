/*!
 * @brief
 *     netCDF4 output writer: netCDF file abstraction
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_NC4_FILE_H_
#define  LDNDC_IO_OUTPUTWRITER_NC4_FILE_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "geom/geom.h"
#include  <netcdf.h>

namespace ldndc {

typedef  size_t  ldndc_ncsize_t;
struct  nc_types_t
{
    typedef  char  nc_char_t;
    typedef  signed char  nc_byte_t;
    typedef  unsigned char  nc_ubyte_t;
    typedef  short  nc_short_t;
    typedef  unsigned short  nc_ushort_t;
    typedef  int  nc_int_t;
    typedef  unsigned int  nc_uint_t;
    typedef  float  nc_float_t;
    typedef  double  nc_double_t;
};

extern  nc_type  resolve_nctype(
            atomic_datatype_t);

#define  ncNoType  NC_NAT
template < typename >
struct  nc_atomic_t
{
};
template < >
struct  nc_atomic_t< ldndc_int8_t >
{
    typedef  nc_types_t::nc_byte_t  type;
    static nc_type const  nc_type_id = NC_BYTE;
};
template < >
struct  nc_atomic_t< ldndc_uint8_t >
{
    typedef  nc_types_t::nc_ubyte_t  type;
    static nc_type const  nc_type_id = NC_UBYTE;
};
template < >
struct  nc_atomic_t< ldndc_char_t >
{
    typedef  nc_types_t::nc_char_t  type;
    static nc_type const  nc_type_id = NC_CHAR;
};
template < >
struct  nc_atomic_t< ldndc_int16_t >
{
    typedef  nc_types_t::nc_short_t  type;
    static nc_type const  nc_type_id = NC_SHORT;
};
template < >
struct  nc_atomic_t< ldndc_uint16_t >
{
    typedef  nc_types_t::nc_ushort_t  type;
    static nc_type const  nc_type_id = NC_USHORT;
};
template < >
struct  nc_atomic_t< ldndc_int32_t >
{
    typedef  nc_types_t::nc_int_t  type;
    static nc_type const  nc_type_id = NC_INT;
};
template < >
struct  nc_atomic_t< ldndc_uint32_t >
{
    typedef  nc_types_t::nc_uint_t  type;
    static nc_type const  nc_type_id = NC_UINT;
};
template < >
struct  nc_atomic_t< ldndc_flt64_t >
{
    typedef  nc_types_t::nc_double_t  type;
    static nc_type const  nc_type_id = NC_DOUBLE;
};
template < >
struct  nc_atomic_t< ldndc_flt32_t >
{
    typedef  nc_types_t::nc_float_t  type;
    static nc_type const  nc_type_id = NC_FLOAT;
};

typedef  int  nc_err_t;

typedef  int  nc_file_t;
#define  nc_badfile  nc_badid
typedef  int  nc_var_t;
#define  nc_badvar  nc_badid
typedef  int  nc_dim_t;
#define  nc_baddim  nc_badid


#ifndef  CRABMEAT_MPI
#  define  LDNDC_NETCDF4_FORMAT  0 /*classic*/
#else
#  define  LDNDC_NETCDF4_FORMAT  NC_MPIIO|NC_NETCDF4 /*use hdf5 parallel i/o*/
#endif

#define  nc_badid  -1

#define  TIME_VARNAME  "time"
#define  ELEVATION_VARNAME  "height"
#define  LATITUDE_VARNAME  "latitude"
#define  LONGITUDE_VARNAME  "longitude"

struct  CBM_API  netcdf_file_t
{
    enum  dimension_e
    {
        TIME, ELEV, LAT, LON,
        NCDIM_CNT
    };
    struct  map_window_t
    {
        ldndc_ncsize_t  offs_lat, offs_lon;
        ldndc_ncsize_t  count_lat, count_lon;
    };
    netcdf_file_t();
    ~netcdf_file_t();

    size_t  get_number_of_vars() const { return  this->n_vars; }
    geom_dim_t const *  get_dimension() const { return  &this->mapinfo.dim; }


    lerr_t  open(
            sink_info_t const *);
    bool  is_open() const { return  this->ncfile != nc_badid; }
    bool  is_valid() const { return  this->ncfile != nc_badid; }
    void  flush();
    void  close();

    bool  is_structured() const { return  this->n_vars != invalid_t< size_t >::value; }
    lerr_t  set_structure(
            sink_entity_layout_t const *,
            geom_mapinfo_t const * /*map info*/, int /*4th dimension size*/);
    lerr_t  define_mapwindow(
            map_window_t const *);
    map_window_t const *  map_window() const { return  &this->mwin; }

    ldndc_ncsize_t  dimension_size( dimension_e /*dimension index*/) const;

    nc_type  variable_type( int /*variable index*/) const;

    template < typename _NT >
    lerr_t  put_map(
            _NT const * /*map buffer*/,
            ldndc_ncsize_t /*index time*/, ldndc_ncsize_t /*cache size*/,
            int /*variable number*/);
    void  reset();

    ldndc_ncsize_t  n_lat() const { return  static_cast< ldndc_ncsize_t >( this->mapinfo.dim.n_y); }
    ldndc_ncsize_t  n_lon() const { return  static_cast< ldndc_ncsize_t >( this->mapinfo.dim.n_x); }
    ldndc_ncsize_t  n_elev() const { return  static_cast< ldndc_ncsize_t >( this->size_elev); }
    private:
        nc_file_t  ncfile;
        /* 1st to 4th dimensions */
        nc_dim_t  ncdim[NCDIM_CNT];
        int  n_ncdim;

        nc_var_t *  vars;
        size_t  n_vars;

        geom_mapinfo_t  mapinfo;
        int  size_elev;

        map_window_t  mwin;

        lerr_t  define_height(
                dimension_e /*which mesh dimension*/, char const * /*variable name*/,
                char const ** /*COARDS attributes*/);
        lerr_t  write_mesh(
                dimension_e /*which mesh dimension*/, char const * /*variable name*/,
                char const ** /*COARDS attributes*/,
                double /*extent*/, double /*offset*/);
        lerr_t  write_heights();

        lerr_t  define_variables(
                sink_entity_layout_t const *);
        lerr_t  write_fillvalue(
                nc_var_t, nc_type);

        size_t  find_heights_data() const;
        lerr_t  find_column_position_for_heights_data(
                ldndc_ncsize_t * /*column position*/, int /*column size*/, nc_var_t);
        lerr_t  read_heights_data(
                double * /*buffer*/, int /*buffer size*/,
                nc_var_t /*source variable*/, ldndc_ncsize_t const * /*source column*/);
        lerr_t  write_heights_data(
                char const * /*variable name*/,
                double const * /*buffer*/, int /*buffer size*/);

        lerr_t  write_coards_attributes(
                nc_var_t /*destination*/, char const ** /*key/value list with sentinel*/);

        lerr_t  define_dimensions();
};
}

#include  "log/cbm_baselog.h"
#include  "mpi/lmpi.h"
template < typename  _NT >
lerr_t
ldndc::netcdf_file_t::put_map(
        _NT const *  _mapbuf, ldndc_ncsize_t  _timeindex, ldndc_ncsize_t  _cachesize,
        int  _var_nb)
{
    ldndc_ncsize_t const  nc_start[NCDIM_CNT] = { _timeindex, 0,
        static_cast< ldndc_ncsize_t >( this->mwin.offs_lat), static_cast< ldndc_ncsize_t >( this->mwin.offs_lon)};
    ldndc_ncsize_t const  nc_size[NCDIM_CNT] = {
        _cachesize, static_cast< ldndc_ncsize_t >( this->size_elev),
               static_cast< ldndc_ncsize_t >( this->mwin.count_lat), static_cast< ldndc_ncsize_t >( this->mwin.count_lon)};

    int const  rc_put = nc_put_vara( this->ncfile, this->vars[_var_nb], nc_start, nc_size, static_cast< void const * >( _mapbuf));
    if ( rc_put == NC_NOERR)
    {
        return  LDNDC_ERR_OK;
    }
    CBM_LogError( "netcdf_file_t::put_map(): ",
            "failed to write datablock to file");
    return  LDNDC_ERR_FAIL;
}

namespace ldndc {
struct  CBM_API  grid_decomposition_t
{
    static  lerr_t  get_map_dimensions_according_to_my_noderank(
            netcdf_file_t::map_window_t *,
            int /*gridsize (latitude)*/, int /*gridsize (longitude)*/);
    static  int  get_noderank_that_contains_gridpoint(
            int /*point (latitude)*/, int /*point (longitude)*/,
            int /*gridsize (latitude)*/, int /*gridsize (longitude)*/);
};
}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_NC4_FILE_H_  */

