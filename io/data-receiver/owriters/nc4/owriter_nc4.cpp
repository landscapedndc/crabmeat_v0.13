/*!
 * @brief
 *    netCDF4 output writer  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4.h"

#include  "io/data-receiver/owriters/nc4/owriter_nc4_writer.cpp"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_writerwrite.cpp"

#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriter.cpp"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriterdispatch.cpp"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriterwrite.cpp"
#ifdef  CRABMEAT_MPI
#  include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewritermpi.cpp"
#endif

#include  "io/data-receiver/owriters/nc4/owriter_nc4_datastore.cpp"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_file.cpp"

