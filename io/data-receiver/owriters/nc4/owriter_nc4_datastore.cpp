/*!
 * @brief
 *    netCDF4 output writer: data store  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_datastore.h"


namespace ldndc {

netcdf_entitystack_t::netcdf_entitystack_t()
        : buf( NULL), buf_size( 0)
{
}
netcdf_entitystack_t::~netcdf_entitystack_t()
{
    CBM_DefaultAllocator->deallocate( this->buf);
    this->buf = NULL;
    this->buf_size = 0;
}


netcdf_entitycache_t::netcdf_entitycache_t()
        : nctype( ncNoType)
{
}
netcdf_entitycache_t::~netcdf_entitycache_t()
{
}


netcdf_storagebroker_t::netcdf_storagebroker_t()
        : entity_caches( NULL), n_entity_caches( 0),
          max_stacksize( 0)
{
}
netcdf_storagebroker_t::~netcdf_storagebroker_t()
{
    this->deallocate_entity_caches();
    this->max_stacksize = 0;
}
int
netcdf_storagebroker_t::initialize(
        nc_type const *  _entity_types, size_t  _n_entities)
{
    if ( this->entity_caches)
    {
        this->deallocate_entity_caches();
    }
    return  this->allocate_entity_caches( _entity_types, _n_entities);
}
int
netcdf_storagebroker_t::allocate_entity_caches(
        nc_type const *  _entity_types, size_t  _n_entities)
{
    this->entity_caches = CBM_DefaultAllocator->construct< netcdf_entitycache_t >( _n_entities);
    if ( !this->entity_caches)
    {
        this->n_entity_caches = 0;
        CBM_LogError( "netcdf_storagebroker_t::allocate_entity_caches(): ",
                "nomem  [requested size=",_n_entities*sizeof(netcdf_entitycache_t),"]");
        return  -1;
    }
    this->n_entity_caches = _n_entities;
    for ( size_t  e = 0;  e < _n_entities;  ++e)
    {
        this->entity_caches[e].set_nctype( _entity_types[e]);
    }
    CBM_LogDebug( "allocated entity caches  [n=",_n_entities,"]");

    return  0;
}
void
netcdf_storagebroker_t::deallocate_entity_caches()
{
    if ( this->entity_caches)
    {
        CBM_DefaultAllocator->destroy< netcdf_entitycache_t >( this->entity_caches, this->n_entity_caches);
        this->entity_caches = NULL;
    }
    this->n_entity_caches = 0;
}



netcdf_mapsbroker_t::netcdf_mapsbroker_t()
        : ssz( 0),
          mapoffs_lat( 0), mapoffs_lon( 0),
              map_dim( 0), flat_dim( 0), mapmem( NULL),
          entity_types( NULL), n_entity_types( 0)
{
}
netcdf_mapsbroker_t::~netcdf_mapsbroker_t()
{
    this->deallocate_entity_types();
    if ( this->mapmem)
    {
        CBM_DefaultAllocator->deallocate( this->mapmem);
        this->mapmem = NULL;
    }
}

void
netcdf_mapsbroker_t::set_maps_dimensions(
        size_t  _max_stacksize,
        netcdf_file_t::map_window_t const *  _map_window)
{
    this->ssz = _max_stacksize;

    crabmeat_assert( _map_window);
    if (( _map_window->count_lat == 0) != ( _map_window->count_lon == 0))
    {
        CBM_LogWarn( "netcdf_mapsbroker_t::set_mapsize(): ",
                "invalid dimension  [dim=",_map_window->count_lon,"x",_map_window->count_lat,"]");
    }
    /* we tolerate 0x0 as invalidating */
    this->mapoffs_lat = _map_window->offs_lat;
    this->mapoffs_lon = _map_window->offs_lon;
    this->map_dim = _map_window->count_lat * _map_window->count_lon;
    this->flat_dim = this->map_dim * this->ssz;

    CBM_LogDebug( "mapsize=",this->map_dim, "{",this->ssz,"}   offsets lat/lon=",this->mapoffs_lat,"/",this->mapoffs_lon);
}

size_t
netcdf_mapsbroker_t::initialize(
        sink_layout_t const *  _layout)
{
    if ( this->entity_types)
    {
        this->deallocate_entity_types();
    }

    size_t const  n_entities = oproc_owriter_layouthelper_t::get_number_of_entities( _layout);
    this->allocate_entity_types( n_entities);
    if ( !this->entity_types)
    {
        return  invalid_size;
    }

    this->assign_nctypes( _layout);

    return  this->n_entity_types;
}

void
netcdf_mapsbroker_t::assign_nctypes(
        sink_layout_t const *  _layout)
{
    size_t  v = 0;
    for ( ldndc_output_rank_t  r = 0;  r < _layout->rank;  ++r)
    {
        for ( ldndc_output_size_t  s = 0;  s < _layout->sizes[r];  ++s, ++v)
        {
            if ( v >= this->n_entity_types)
            {
                CBM_LogError( "netcdf_mapsbroker_t::assign_nctypes(): ",
                        "layout does not match previously advertised layout");
                break;
            }
            this->entity_types[v] = resolve_nctype( _layout->types[r]);
        }
    }

    if ( this->n_entity_types > v)
    {
        CBM_LogError( "netcdf_mapsbroker_t::assign_nctypes(): ",
                "layout mismatch, not all variables were assigned a type");
    }
}

void
netcdf_mapsbroker_t::allocate_entity_types(
        size_t  _n_entities)
{
    crabmeat_assert( !this->entity_types);
    if ( _n_entities == 0 || cbm::is_invalid( _n_entities))
    {
        CBM_LogError( "netcdf_mapsbroker_t::allocate_entity_types(): ",
                "invalid size  [#vars=",_n_entities,"]");
        return;
    }

    this->entity_types = CBM_DefaultAllocator->construct< nc_type >( _n_entities);
    if ( !this->entity_types)
    {
        CBM_LogError( "netcdf_mapsbroker_t::allocate_entity_types(): ",
                "nomem");
    }
    this->n_entity_types = _n_entities;
    CBM_LogInfo( "netcdf_mapsbroker_t::allocate_entity_types(): ",
            "successfully allocated variable type list  [size=",this->n_entity_types,"]");
}
void
netcdf_mapsbroker_t::deallocate_entity_types()
{
    if ( this->entity_types)
    {
        CBM_DefaultAllocator->destroy( this->entity_types, this->n_entity_types);
    }
    this->entity_types = NULL;
    this->n_entity_types = 0;
}

}

