/*!
 * @brief
 *    netCDF4 output writer: block writer dispatching  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriter.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_writer.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

oproc_owriter_base_t *
oproc_landscape_owriter_nc4_t::acquire_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    oproc_owriter_base_t *  new_owriter = this->owriters_.acquire_receiver( _owriter_descriptor);
    if ( !new_owriter)
    {
        return  NULL;
    }
    new_owriter->set_object_id( this->owriters_.size());

    lerr_t  rc_set_attrs = static_cast< oproc_owriter_nc4_t * >( new_owriter)->set_attributes( &this->sink_info.attrs);
    if ( rc_set_attrs)
    {
        return  NULL;
    }

    return  new_owriter;
}


void
oproc_landscape_owriter_nc4_t::release_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    if (( _owriter_descriptor != invalid_t< lreceiver_descriptor_t >::value) && this->sink_info.attrs.ordered)
    {
        /* only collectively delete and flush blocks */
        return;
    }

    release_op_t  r_op;
    this->owriters_.release_receiver( _owriter_descriptor, &r_op);
}


void
oproc_landscape_owriter_nc4_t::release_op_t::operator()(
        oproc_owriter_nc4_t *  /*_w*/)
const
{
// sk:obs    _w->set_layout_owriter( NULL);
}

}

