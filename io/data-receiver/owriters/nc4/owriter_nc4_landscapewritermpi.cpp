/*!
 * @brief
 *    netCDF4 output writer: data submission  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriter.h"
#include  "log/cbm_baselog.h"

#include  "containers/lbitset.h"

namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(oproc_owriter_shadow_nc4_t)
oproc_owriter_shadow_nc4_t::oproc_owriter_shadow_nc4_t()
        : oproc_owriter_nc4_t(),
          ssz( 0)
{
}
oproc_owriter_shadow_nc4_t::~oproc_owriter_shadow_nc4_t()
{
}
void
oproc_owriter_shadow_nc4_t::set_source_rank(
        int  _source_rank)
{
    this->grid.rank = _source_rank;
}
int
oproc_owriter_shadow_nc4_t::set_grid_points(
        int const *  _points, int  _n_points)
{
    int  rc_alloc = this->grid.reallocate( _n_points);
    if ( rc_alloc < 0)
    {
        return  -1;
    }
    crabmeat_assert( _n_points == static_cast< int >( this->grid.n_I));
// sk:dbg    CBM_LogDebug( "r=",this->grid.rank, "  #p=",_n_points);
    for ( int  p = 0;  p < _n_points;  ++p, ++_points)
    {
        this->grid.I[p] = *_points;
// sk:dbg        CBM_LogDebug( *_points,";");
    }
    return  0;
}
lerr_t
oproc_owriter_shadow_nc4_t::set_layout_owriter(
        sink_entity_layout_t const *  _layout, int  _count)
{
    oproc_owriter_nc4_t::set_layout_owriter( _layout);
    int  rc_cinit = this->pcomm.initialize( &_layout->layout, NULL, 0, _count);
    this->ssz = _count;

    return  ( rc_cinit == 0) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}
lerr_t
oproc_owriter_shadow_nc4_t::mpi_receive_records(
        int  _timestep)
{
    CBM_LogDebug( "{",mpi::comm_rank( mpi::comm_ldndc),"} receiving from (",this->grid.rank, ",",this->object_id(), ")  .",this->pcomm.n_pbuf,".");
    int  rc_recv = this->pcomm.recv( this->grid.rank, this->object_id());
    if ( rc_recv)
    {
        CBM_LogError( "oproc_owriter_shadow_nc4_t::mpi_receive_records(): ",
                "failed to receive records  [t=",_timestep,"]");
        return  LDNDC_ERR_FAIL;
    }
    void **  data = NULL;
    malloc_from_layout( &data, this->pcomm.layout);
    sink_record_srv_t  srec = sink_record_srv_defaults;
    srec.data = (void const **)data;
    sink_record_sequence_t  rseq;
    rseq.timestep = _timestep;
    srec.seq = &rseq;

    int  m = 0;
    int  rc_unpack = 0;
    while ( m < this->ssz)
    {
// sk:dbg        if ( mpi::comm_rank( mpi::comm_ldndc) == 0)
// sk:dbg        {
// sk:dbg            CBM_LogError( "unpacking message ",m,"  {",this->pcomm.pbufpos,"}");
// sk:dbg        }
        rc_unpack = this->pcomm.unpack( data);
        if ( rc_unpack)
        {
            CBM_LogError( "oproc_owriter_shadow_nc4_t::mpi_receive_records(): ",
                    "errors occured during message unpacking");
            break;
        }
        rseq.records_in_timestep = m;
        oproc_owriter_nc4_t::write_record( NULL, &srec);

        ++m;
    }
    free_from_layout( data, this->pcomm.layout);

    return  ( rc_unpack == 0) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;;
}

oproc_landscape_owriter_shadow_nc4_t::oproc_landscape_owriter_shadow_nc4_t()
        : oproc_landscape_base_t()
{
}
oproc_landscape_owriter_shadow_nc4_t::~oproc_landscape_owriter_shadow_nc4_t()
{
}


lerr_t
oproc_landscape_owriter_nc4_t::mpi_construct_shadowwriters_from_pushgrids(
        oproc_owriter_nc4_grid_container_t const *  _pushgrids)
{
    bitset  mark_contained_nodes( mpi::comm_size( mpi::comm_ldndc));
    int const  n_pushnode_ranks = this->mpi_flag_contained_nodes( &mark_contained_nodes, _pushgrids);

    int const  n_pullnode_ranks_max = this->mpi_find_most_pullnodes( n_pushnode_ranks);
    if ( n_pullnode_ranks_max < 0)
    {
        CBM_LogError( "oproc_landscape_owriter_nc4_t::mpi_construct_shadowwriters_from_pushgrids(): ",
                "unable to find maximum number of pull-nodes");
// sk:?? if MPI_Allreduce fails, does it fail on all nodes?
        return  LDNDC_ERR_FAIL;
    }
    else if ( n_pullnode_ranks_max == 0)
    {
        /* nothing needs to be transfered */
        crabmeat_assert(n_pushnode_ranks == 0);
        return  LDNDC_ERR_OK;
    }
    int *  pushnode_ranks = CBM_DefaultAllocator->allocate_init_type< int >( n_pushnode_ranks, -1);
    this->mpi_set_pushnode_ranks( pushnode_ranks, n_pushnode_ranks, &mark_contained_nodes);

    int *  pullnode_ranks = CBM_DefaultAllocator->allocate_init_type< int >( n_pullnode_ranks_max, -1);
    int  n_pullnode_ranks = this->mpi_find_communication_endpoints( pullnode_ranks, n_pullnode_ranks_max, &mark_contained_nodes);
    if ( n_pullnode_ranks < 0)
    {
        CBM_LogError( "oproc_landscape_owriter_nc4_t::mpi_construct_shadowwriters_from_pushgrids(): ",
                "failed to find communication endpoints");
    }

    lerr_t  rc_estcomm = this->mpi_establish_communication_endpoints(
            pullnode_ranks, n_pullnode_ranks, pushnode_ranks, n_pushnode_ranks, _pushgrids);
    if ( rc_estcomm)
    {
        CBM_LogError( "oproc_landscape_owriter_nc4_t::mpi_construct_shadowwriters_from_pushgrids(): ",
                "failed to successfully establish communication endpoints");
    }
    CBM_DefaultAllocator->deallocate( pullnode_ranks);

    CBM_LogDebug( "#shadowwriters=",this->oshadowwriters_.size());
    return  ( n_pullnode_ranks < 0 || rc_estcomm) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}
int
oproc_landscape_owriter_nc4_t::mpi_find_most_pullnodes(
        int  _n_pushnodes)
{
    int  n_pullnode_ranks_max = -1;
    int  n_pushnodes = _n_pushnodes;
    int  rc_allred = MPI_Allreduce( &n_pushnodes, &n_pullnode_ranks_max, 1, MPI_INT, MPI_MAX, mpi::comm_ldndc);
    if ( rc_allred)
    {
        n_pullnode_ranks_max = -1;
    }
// sk:dbg    CBM_LogDebug( "myrank=",mpi::comm_rank( mpi::comm_ldndc), "  push-nodes=",n_pushnodes, "  max-push-nodes=",n_pullnode_ranks_max);

    return  n_pullnode_ranks_max;
}
int
oproc_landscape_owriter_nc4_t::mpi_find_communication_endpoints(
        int *  _pullnode_ranks, int  _n_pullnode_ranks_max,
        bitset const *  _flag_set)
{
    if ( _n_pullnode_ranks_max == 0)
    {
        return  0;
    }
    /* NOTE  this might not scale very well, so keep in mind
     * this is a place we need tuning once the number of nodes
     * grows large
     */
    int *  comm_endpoints = CBM_DefaultAllocator->allocate_init_type< int >( mpi::comm_size( mpi::comm_ldndc)*_n_pullnode_ranks_max, -1);
    if ( !comm_endpoints || !_pullnode_ranks || !_flag_set)
    {
        CBM_LogError( "oproc_owriter_nc4_t::make_pushnode_grids(): ",
                "nomem");

        CBM_DefaultAllocator->deallocate( comm_endpoints);
        return  -1;
    }
    int const  myrank = mpi::comm_rank( mpi::comm_ldndc);

    /* collect nodes that have to pull from us */
    for ( size_t  c = 0, i = 0;  c < _flag_set->size();  ++c)
    {
        if ( _flag_set->test( c))
        {
            _pullnode_ranks[i++] = c;
            CBM_LogDebug( "me [",myrank,"] pushing to ",c);
        }
    }
    MPI_Allgather( _pullnode_ranks, _n_pullnode_ranks_max, MPI_INT, 
            comm_endpoints, _n_pullnode_ranks_max, MPI_INT,
            mpi::comm_ldndc);

    /* collect nodes that we have to push to */
    int  j = 0;
    int const  n_c = mpi::comm_size( mpi::comm_ldndc)*_n_pullnode_ranks_max;
    for ( int  c = 0;  c < n_c;  ++c)
    {
        if ( comm_endpoints[c] == myrank)
        {
            int const  C = ( c / _n_pullnode_ranks_max);
            if ( C == myrank)
            {
                CBM_LogWarn( "found myself in push nodes .. well well. skipping.");
                continue;
            }
            CBM_LogDebug( "me [",myrank,"] receiving from ", C, "  c=",c);

            _pullnode_ranks[j++] = C;

            if ( j >= _n_pullnode_ranks_max)
            {
                /* found them all (hoping that there are no more ;-) )*/
                break;
            }
        }
    }

    return  j;
}
lerr_t
oproc_landscape_owriter_nc4_t::mpi_establish_communication_endpoints(
        int *  _pullnode_ranks, int  _n_pullnode_ranks,
        int *  _pushnode_ranks, int  _n_pushnode_ranks,
        oproc_owriter_nc4_grid_container_t const *  _pushgrids)
{
    MPI_Request *  push_req = CBM_DefaultAllocator->allocate_type< MPI_Request >( _n_pushnode_ranks);
    if (( _n_pushnode_ranks > 0) && !push_req)
    {
        CBM_LogWarn( "oproc_landscape_owriter_nc4_t::mpi_establish_communication_endpoints(): ",
                "failed to allocate mpi request or status structures .. expect the worse :-|");
    }
    /* send number of grids to push */
    /* maximum number of grids to send to a single node */
    int  n_pushgrids_max = 0;
    int *  n_pushgrids = CBM_DefaultAllocator->allocate_type< int >( _n_pushnode_ranks);
    for ( int  r_push = 0;  r_push < _n_pushnode_ranks;  ++r_push)
    {
        n_pushgrids[r_push] = this->mpi_count_pushrank_occurences( _pushnode_ranks[r_push], _pushgrids);
        CBM_LogDebug( "informing node ",_pushnode_ranks[r_push], " about ", n_pushgrids[r_push], " grids to pull");
        MPI_Isend( n_pushgrids+r_push, 1, MPI_INT, _pushnode_ranks[r_push], 0, mpi::comm_ldndc, &push_req[r_push]);

        n_pushgrids_max = std::max( n_pushgrids_max, n_pushgrids[r_push]);
    }
    /* receive number of grids to pull */
    /* maximum number of grids to pull from a single node */
    int  n_pullgrids_max = 0;
    int *  n_pullgrids = CBM_DefaultAllocator->allocate_type< int >( _n_pullnode_ranks);
    for ( int  r_pull = 0;  n_pullgrids && ( r_pull < _n_pullnode_ranks);  ++r_pull)
    {
        n_pullgrids[r_pull] = 0;
        MPI_Recv( n_pullgrids+r_pull, 1, MPI_INT, _pullnode_ranks[r_pull], 0, mpi::comm_ldndc, MPI_STATUS_IGNORE);
        CBM_LogDebug( "being informed by node ",_pullnode_ranks[r_pull], " about ", n_pullgrids[r_pull], " grids to pull");

        n_pullgrids_max = std::max( n_pullgrids_max, n_pullgrids[r_pull]);
    }

    if ( _n_pushnode_ranks > 0)
    {
        MPI_Waitall( _n_pushnode_ranks, push_req, MPI_STATUSES_IGNORE);
    }

    int  n_ps_max = 0;
    /* send number of points for each grid to push */
    int **  gs = CBM_DefaultAllocator->allocate_init_type< int * >( _n_pushnode_ranks, NULL);
    for ( int  r_push = 0;  r_push < _n_pushnode_ranks;  ++r_push)
    {
        crabmeat_assert( gs[r_push] == NULL);
        gs[r_push] = CBM_DefaultAllocator->allocate_type< int >( n_pushgrids[r_push]);
        crabmeat_assert( gs[r_push] != NULL);
        int  n_max = this->mpi_set_number_of_points_per_grid_for_rank( gs[r_push], _pushnode_ranks[r_push], _pushgrids);
        n_ps_max = std::max( n_max, n_ps_max);

        MPI_Isend( gs[r_push], n_pushgrids[r_push], MPI_INT, _pushnode_ranks[r_push], 0, mpi::comm_ldndc, &push_req[r_push]);
    }
    /* receive number of points for each grid to pull */
    int  n_pr_max = 0;
    int **  gr = CBM_DefaultAllocator->allocate_init_type< int * >( _n_pullnode_ranks, NULL);
    for ( int  r_pull = 0;  n_pullgrids && ( r_pull < _n_pullnode_ranks);  ++r_pull)
    {
        CBM_LogDebug( "me[",mpi::comm_rank( mpi::comm_ldndc),"] pulling ",n_pullgrids[r_pull]," grids from ",_pullnode_ranks[r_pull]);
        crabmeat_assert( gr[r_pull] == NULL);
        gr[r_pull] = CBM_DefaultAllocator->allocate_init_type< int >( n_pullgrids[r_pull], 0);
        crabmeat_assert( gr[r_pull] != NULL);
        int  rc_recv = MPI_Recv( gr[r_pull], n_pullgrids[r_pull], MPI_INT, _pullnode_ranks[r_pull], 0, mpi::comm_ldndc, MPI_STATUS_IGNORE);
        if ( rc_recv)
        {
            CBM_LogWarn( "mpi receive failed: attempted to receive number of points for each grid");
        }
        n_pr_max = std::max( n_pr_max, cbm::sum( gr[r_pull], n_pullgrids[r_pull]));
    }

    if ( _n_pushnode_ranks > 0)
    {
        MPI_Waitall( _n_pushnode_ranks, push_req, MPI_STATUSES_IGNORE);
    }

    /* send points for each grid to push and piggy-back a message tag */
    int **  ps_l = CBM_DefaultAllocator->allocate_init_type< int * >( _n_pushnode_ranks, NULL);
    for ( int  r_push = 0;  r_push < _n_pushnode_ranks;  ++r_push)
    {
        int *  ps = CBM_DefaultAllocator->allocate_init_type< int >( n_ps_max+n_pushgrids_max/*piggyback shadow tag*/, 0);
        ps_l[r_push] = ps;
        int const  n_ps = this->mpi_set_points_and_tag_for_rank( ps, _pushnode_ranks[r_push], _pushgrids);
        crabmeat_assert( n_ps == cbm::sum( gs[r_push], n_pushgrids[r_push]));
        MPI_Isend( ps, n_ps+n_pushgrids[r_push], MPI_INT, _pushnode_ranks[r_push], 0, mpi::comm_ldndc, &push_req[r_push]);
    }
    /* receive points for each grid to pull */
    int *  pr = CBM_DefaultAllocator->allocate_init_type< int >( n_pr_max+n_pullgrids_max/*piggyback shadow tag*/, 0);
    for ( int  r_pull = 0;  r_pull < _n_pullnode_ranks;  ++r_pull)
    {
        int const  n_pr = cbm::sum( gr[r_pull], n_pullgrids[r_pull]);
        int  rc_recv = MPI_Recv( pr, n_pr+n_pullgrids[r_pull], MPI_INT, _pullnode_ranks[r_pull], 0, mpi::comm_ldndc, MPI_STATUS_IGNORE);
        if ( rc_recv)
        {
            CBM_LogError( "failed to receive grid points from node ",_pullnode_ranks[r_pull]);
            continue;
        }

        this->mpi_construct_shadowwriters_for_rank( _pullnode_ranks[r_pull], pr, gr[r_pull], n_pullgrids[r_pull]);
    }

    if ( _n_pushnode_ranks > 0)
    {
        MPI_Waitall( _n_pushnode_ranks, push_req, MPI_STATUSES_IGNORE);
    }

    CBM_DefaultAllocator->deallocate( push_req);
// FIXME  free stuff

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_nc4_t::mpi_construct_shadowwriters_for_rank(
        int  _rank,
        int *  _gridpoints, int *  _gridsizes,  int  _n_grids)
{
    CBM_LogDebug( "{",mpi::comm_rank( mpi::comm_ldndc),"} constructing ",_n_grids," shadowwriters..");
    for ( int  g = 0;  g < _n_grids;  ++g)
    {
        int const  shadow_tag = _gridpoints[_gridsizes[g]];
        oproc_owriter_shadow_nc4_t *  w = this->mpi_acquire_oshadowwriter( shadow_tag);
        if ( !w)
        {
            return  LDNDC_ERR_FAIL;
        }
        CBM_LogDebug( "\t{",mpi::comm_rank( mpi::comm_ldndc),"} constructed @",(void*)w," with tag ",shadow_tag, "  pos=",_gridsizes[g]);
        w->initialize_storagebroker( &this->mapsbrk);
        w->set_layout_owriter( &this->fmt, this->mapsbrk.stacksize());

        w->set_source_rank( _rank);
        w->set_grid_points( _gridpoints, _gridsizes[g]);

        _gridpoints += _gridsizes[g]+1/*piggybacked shadowtag*/;
    }
    return  LDNDC_ERR_OK;
}
int
oproc_landscape_owriter_nc4_t::mpi_flag_contained_nodes(
        bitset *  _flag_set, oproc_owriter_nc4_grid_container_t const *  _pushgrids)
{
    _flag_set->clear();

    oproc_owriter_nc4_grid_container_t::const_iterator  gi = _pushgrids->begin();
    while ( gi != _pushgrids->end())
    {
        if ( gi->rank < 0)
        {
            CBM_LogWarn( "oproc_landscape_owriter_nc4_t::mpi_flag_contained_nodes(): ",
                    "found invalid rank in push-node list");
            continue;
        }
        _flag_set->set( gi->rank);
        ++gi;
    }
    return  _flag_set->bits_set();
}
void
oproc_landscape_owriter_nc4_t::mpi_set_pushnode_ranks(
        int *  _pushnode_ranks, int  _n_pushnode_ranks,
               bitset const *  _flag_set)
{
    int  n = 0, c = 0;
    while ( n < _n_pushnode_ranks)
    {
        if ( _flag_set->test( c))
        {
            _pushnode_ranks[n++] = c;
        }
        ++c;
    }
}
int
oproc_landscape_owriter_nc4_t::mpi_count_pushrank_occurences(
        int  _rank, oproc_owriter_nc4_grid_container_t const *  _pushgrids)
{
    int  p_count = 0;
    oproc_owriter_nc4_grid_container_t::const_iterator  gi = _pushgrids->begin();
    while ( gi != _pushgrids->end())
    {
        if ( gi->rank == _rank)
        {
            ++p_count;
        }
        ++gi;
    }

    return  p_count;
}
int
oproc_landscape_owriter_nc4_t::mpi_set_number_of_points_per_grid_for_rank(
        int *  _n_gridpoints, int  _rank,
        oproc_owriter_nc4_grid_container_t const *  _pushgrids)
{
    int  n_gridpoints_sum = 0;
    int  c = 0;
    oproc_owriter_nc4_grid_container_t::const_iterator  gi = _pushgrids->begin();
    while ( gi != _pushgrids->end())
    {
        if ( gi->rank == _rank)
        {
            _n_gridpoints[c] = static_cast< int >( gi->n_I);
// sk:dbg            CBM_LogWarn( "#P=",_n_gridpoints[c]);
            n_gridpoints_sum += _n_gridpoints[c];

            ++c;
        }
        ++gi;
    }
    return  n_gridpoints_sum;
}
int
oproc_landscape_owriter_nc4_t::mpi_set_points_and_tag_for_rank(
           int *  _gridpoints, int  _rank,
           oproc_owriter_nc4_grid_container_t const *  _pushgrids)
{
    int  n_gridpoints_sum = 0;
    oproc_owriter_nc4_grid_container_t::const_iterator  gi = _pushgrids->begin();
    while ( gi != _pushgrids->end())
    {
        if ( gi->rank == _rank)
        {
            crabmeat_assert( gi->owriter->object_id() < (1<<10));
            int const  shadow_tag = (mpi::comm_rank( mpi::comm_ldndc) << 10) + gi->owriter->object_id();

            /* piggyback source writer tag */
            CBM_LogDebug( "targetrank=",_rank, "  shadow tag ",shadow_tag, "  pos=", gi->n_I, "  wrt=@",(void*)gi->owriter, "  wrt-id=",gi->owriter->object_id());

            for ( int  p = 0;  p < static_cast< int >( gi->n_I);  ++p, ++_gridpoints)
            {
                *_gridpoints = static_cast< int >( gi->I[p]);
            }
            n_gridpoints_sum += static_cast< int >( gi->n_I);

//            *_gridpoints = static_cast< int >( gi->owriter->object_id());
            *_gridpoints = shadow_tag;
            _gridpoints += 1;

//            gi->owriter->mpi_add_shadowwriter( _rank, gi->owriter->object_id());
            gi->owriter->mpi_add_shadowwriter( _rank, shadow_tag);
        }
        ++gi;
    }
// sk:dbg    CBM_LogWarn( "#P=",n_gridpoints_sum);
    return  n_gridpoints_sum;
}

oproc_owriter_shadow_nc4_t *
oproc_landscape_owriter_nc4_t::mpi_acquire_oshadowwriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    oproc_owriter_shadow_nc4_t *  new_oshadowwriter = this->oshadowwriters_.acquire_receiver( _owriter_descriptor);
    if ( !new_oshadowwriter)
    {
        return  NULL;
    }
    new_oshadowwriter->set_object_id( _owriter_descriptor); //this->oshadowwriters_.size());

    lerr_t  rc_set_attrs = new_oshadowwriter->set_attributes( &this->sink_info.attrs);
    if ( rc_set_attrs)
    {
        return  NULL;
    }

    return  new_oshadowwriter;
}

lerr_t
oproc_landscape_owriter_nc4_t::mpi_send_to_shadowwriters(
        int  _timestep)
{
    lerr_t  rc_send = LDNDC_ERR_OK;
    for ( size_t  j = 0;  j < this->owriters_.size();  ++j)
    {
        oproc_owriter_nc4_t *  w = this->owriters_[j];
        if ( w)
        {
            lerr_t  rc_send_recs = w->mpi_send_records( _timestep);
            if ( rc_send_recs)
            {
                rc_send = LDNDC_ERR_FAIL;
            }
        }
    }

    return  rc_send;
}
lerr_t
oproc_landscape_owriter_nc4_t::mpi_recv_from_writers(
           int  _timestep)
{
    lerr_t  rc_recv = LDNDC_ERR_OK;
    for ( size_t  j = 0;  j < this->oshadowwriters_.size();  ++j)
    {
        oproc_owriter_shadow_nc4_t *  w = this->oshadowwriters_[j];
        if ( w)
        {
            lerr_t  rc_recv_recs = w->mpi_receive_records( _timestep);
            if ( rc_recv_recs)
            {
                rc_recv = LDNDC_ERR_FAIL;
            }
        }
    }

    return  rc_recv;
}
}

