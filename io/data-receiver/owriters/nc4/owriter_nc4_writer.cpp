/*!
 * @brief
 *    netCDF4 output writer: writer initialization parts  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_writer.h"

#include  "log/cbm_baselog.h"
#include  <map>

namespace ldndc {

oproc_owriter_nc4_grid_t::oproc_owriter_nc4_grid_t()
        : I(NULL), n_I(0), n_I_push(0), rank( -1), owriter(NULL)
{
}
oproc_owriter_nc4_grid_t::oproc_owriter_nc4_grid_t(
        oproc_owriter_nc4_grid_t const &  _grid)
        : I(NULL), n_I(0), n_I_push(0), rank( -1), owriter(NULL)
{
    this->copy_from( _grid);
    this->n_I_push = _grid.n_I_push;
    this->rank = _grid.rank;
    this->owriter = _grid.owriter;
}
oproc_owriter_nc4_grid_t &
oproc_owriter_nc4_grid_t::operator=(
        oproc_owriter_nc4_grid_t const &  _grid)
{
    if ( this == &_grid)
    {
        return  *this;
    }

    this->copy_from( _grid);
    this->n_I_push = _grid.n_I_push;
    this->rank = _grid.rank;
    this->owriter = _grid.owriter;

    return  *this;
}
oproc_owriter_nc4_grid_t::~oproc_owriter_nc4_grid_t()
{
    this->deallocate();
    this->rank = -1;
    this->owriter = NULL;
}
int
oproc_owriter_nc4_grid_t::copy_from(
        oproc_owriter_nc4_grid_t const &  _grid)
{
    return  this->reallocate( _grid.n_I, _grid.I);
}
int
oproc_owriter_nc4_grid_t::allocate(
        size_t  _size)
{
    if ( this->I && ( _size == 0))
    {
        this->deallocate();
    }
    else if ( this->I && ( _size > 0))
    {
        return  this->reallocate( _size);
    }            
    else if ( _size > 0)
    {
        this->I = CBM_DefaultAllocator->allocate_init_type< size_t >( _size, 0);
        if ( !this->I)
        {
            this->n_I = 0;
            return  -1;
        }
        this->n_I = _size;
    }
    return  0;
}
int
oproc_owriter_nc4_grid_t::reallocate(
        size_t  _size, size_t *  _src)
{
    if ( _size == 0)
    {
        this->deallocate();
    }
    else if ( this->n_I != _size)
    {
        size_t *  new_I = CBM_DefaultAllocator->allocate_init_type< size_t >( _size, 0);
        if ( !new_I)
        {
            CBM_LogError( "oproc_owriter_nc4_grid_t::reallocate(): ",
                    "nomem");
            return  -2;
        }
        if ( _src)
        {
            cbm::mem_cpy( new_I, _src, _size);
        }
        else if ( this->I)
        {
            cbm::mem_cpy( new_I, this->I, std::min( _size, this->n_I));
        }
        this->deallocate();

        this->I = new_I;
        this->n_I = _size;
        this->n_I_push = 0;
    }
    else if ( _src != NULL)
    {
        crabmeat_assert( this->I);
        crabmeat_assert( this->n_I > 0);
        cbm::mem_cpy( this->I, _src, this->n_I);
    }

    return  0;
}
void
oproc_owriter_nc4_grid_t::deallocate()
{
    if ( this->I)
    {
        CBM_DefaultAllocator->deallocate( this->I);
        this->I = NULL;
        this->n_I = 0;
        this->n_I_push = 0;
    }
    crabmeat_assert(( this->n_I == 0) &&( this->n_I_push == 0) && ( !this->I));
}


oproc_owriter_nc4_base_t::oproc_owriter_nc4_base_t()
        : oproc_owriter_base_t()
{
}


oproc_owriter_nc4_base_t::~oproc_owriter_nc4_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_owriter_nc4_t)
oproc_owriter_nc4_t::oproc_owriter_nc4_t()
        : oproc_owriter_nc4_base_t(),
          fmt( NULL),
          attrs( NULL),
          rcv_d_( invalid_t< lreceiver_descriptor_t >::value)
{
}
oproc_owriter_nc4_t::~oproc_owriter_nc4_t()
{
    crabmeat_assert( this->attrs);
    this->fmt = NULL;
    this->attrs = NULL;
}


lerr_t
oproc_owriter_nc4_t::set_attributes(
        sink_attributes_t const *  _attrs)
{
    this->attrs = _attrs;
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_nc4_t::set_layout_owriter(
        sink_entity_layout_t const *  _layout)
{
    this->fmt = _layout;
#ifdef  CRABMEAT_MPI
    this->mpi_initialize_communicator( &_layout->layout);
#endif
    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_nc4_t::set_grid_points(
        geom_shape_t const *  _shape, geom_bounding_box_t const &  _submapbb,
        geom_dim_t const &  _griddim, geom_bounding_box_t const &  _mapbb,
        netcdf_file_t::map_window_t const *  _map_window,
        oproc_owriter_nc4_grid_container_t *  _pushgrids)
{
    if ( !_shape)
    {
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

// sk:dbg    CBM_LogInfo( "map-bb=",_mapbb.to_string(), "  part-bb",_submapbb.to_string(), "  n-x=",_griddim.n_x, "  n-y=",_griddim.n_y);

    geom_shape_t  shifted_shape = *_shape;
    geom_coord_t  shift_value( _mapbb.ul_x, _mapbb.lr_y);
    shifted_shape.shift( shift_value);
    shifted_shape.make_orientation_counterclockwise();

    set_gridpoints_info_t  g_info;

    g_info.map_window = _map_window;

    g_info.xn = _griddim.n_x;
    g_info.yn = _griddim.n_y;

    g_info.rx = ( _mapbb.lr_x - _mapbb.ul_x)/static_cast< double >( g_info.xn);
    g_info.ry = ( _mapbb.ul_y - _mapbb.lr_y)/static_cast< double >( g_info.yn);

    g_info.x0 = ( _submapbb.ul_x - _mapbb.ul_x) / g_info.rx;
    g_info.y0 = ( _submapbb.lr_y - _mapbb.lr_y) / g_info.ry;

    g_info.dx = ( _submapbb.lr_x - _submapbb.ul_x) / g_info.rx;
    g_info.dy = ( _submapbb.ul_y - _submapbb.lr_y) / g_info.ry;

    if (( g_info.dx <= 0) || ( g_info.dy <= 0) || ( g_info.rx <= 0.0) || ( g_info.ry <= 0.0))
    {
        CBM_LogError( "oproc_owriter_nc4_t::set_grid_points(): ",
                "bounding box has illegal size");
        CBM_LogError( "dim-x=", g_info.dx,"  dim-y=", g_info.dy, "  res-x=", g_info.rx,"  res-y=",g_info.ry);
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    lerr_t  rc_setpoints = this->set_grid_points_from_shape( &shifted_shape, &g_info, _pushgrids);
    if ( rc_setpoints)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_nc4_t::set_grid_points_from_shape(
        geom_shape_t const *  _shape,
               set_gridpoints_info_t const *  _g,
        oproc_owriter_nc4_grid_container_t *  _pushgrids)
{
    this->grid.rank = mpi::comm_rank( mpi::comm_ldndc);
    int  rc_gridalloc = this->grid.allocate( _g->dx * _g->dy);
    if ( rc_gridalloc)
    {
        return  LDNDC_ERR_NOMEM;
    }

    size_t const  n_points = this->make_grid( &this->grid, _shape, _g);
    lerr_t  rc_push = LDNDC_ERR_OK;
    if ( _pushgrids && ( this->grid.n_I_push > 0))
    {
        rc_push = this->append_pushgrids( _pushgrids, &this->grid, _g);
    }

    CBM_LogDebug( "myrank=",this->grid.rank, "  new #P=",n_points, "  #push-P=",this->grid.n_I_push, "  #push-nodes=",((_pushgrids)?_pushgrids->size():0));
    lerr_t  rc_crop = this->crop_my_grid( &this->grid, n_points);

    return  ( rc_push || rc_crop) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}
#define  oproc_nc4_map_point_to_window(__x__,__y__,__g_info__)  ((((__y__)+(__g_info__)->y0)%(__g_info__)->map_window->count_lat)*(__g_info__)->map_window->count_lon + (((__x__)+(__g_info__)->x0)%(__g_info__)->map_window->count_lon))
#define  oproc_nc4_encode_point(__x__,__y__,__g_info__)  ((__y__)*(__g_info__)->xn + (__x__))
#define  oproc_nc4_decode_point(__x__,__y__,__g_info__,__p__)            \
    __y__ = ((__p__) / (__g_info__)->xn);                    \
    __x__ = ((__p__) - (__g_info__)->xn * (__y__))/*;*/
size_t
oproc_owriter_nc4_t::make_grid(
        oproc_owriter_nc4_grid_t *  _p_grid,
        geom_shape_t const *  _shape,
        set_gridpoints_info_t const *  _g)
{
    size_t  n_points = 0;
    size_t  n_push_points = _p_grid->n_I;
    for ( int  x = 0;  x < _g->dx;  ++x)
    {
        geom_coord_t  p;
        p.x = static_cast< double >( x+_g->x0)*_g->rx /*+ 0.5*_g->rx*/;

        for ( int  y = 0;  y < _g->dy;  ++y)
        {
            p.y = static_cast< double >( y+_g->y0)*_g->ry /*+ 0.5*_g->ry*/;

            if ( _shape->contains_point( p))
            {
                int const  pr = this->get_noderank_that_contains_gridpoint( y, x, _g);
                if ( _p_grid->rank == pr)
                {
                    _p_grid->I[n_points++] = oproc_nc4_map_point_to_window(x,y,_g);
                }
                else
                {
                    /*inform appropriate node later*/
                    _p_grid->I[--n_push_points] = oproc_nc4_encode_point(x,y,_g);
                }
            }
        }
    }

    _p_grid->n_I_push = _p_grid->n_I - n_push_points;
    return  n_points;
}
int
oproc_owriter_nc4_t::get_noderank_that_contains_gridpoint(
        int  _y,  int  _x, set_gridpoints_info_t const *  _g)
{
    return  grid_decomposition_t::get_noderank_that_contains_gridpoint(
            _y+_g->y0, _x+_g->x0, _g->yn, _g->xn);
}
lerr_t
oproc_owriter_nc4_t::crop_my_grid(
        oproc_owriter_nc4_grid_t *  _p_grid, size_t  _n_points)
{
    crabmeat_assert( _p_grid);
    if ( _n_points == 0)
    {
        _p_grid->deallocate();
    }
    else if ( _n_points < _p_grid->n_I)
    {
        _p_grid->reallocate( _n_points);
    }
    else
    {
        _p_grid->deallocate();
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_nc4_t::append_pushgrids(
        oproc_owriter_nc4_grid_container_t *  _pushgrids,
        oproc_owriter_nc4_grid_t const *  _p_grid,
        set_gridpoints_info_t const *  _g)
{
#ifdef  CRABMEAT_MPI
    lerr_t  rc_alloc_and_set = this->allocate_and_set_pushgrids( _pushgrids, _p_grid, _g);
    if ( rc_alloc_and_set)
    {
        CBM_LogError( "oproc_owriter_nc4_t::append_pushgrids(): ",
                "failed to set up push-grids");
        return  LDNDC_ERR_FAIL;
    }
#else
    CRABMEAT_FIX_UNUSED(_pushgrids);CRABMEAT_FIX_UNUSED(_p_grid);CRABMEAT_FIX_UNUSED(_g);
#endif
    return  LDNDC_ERR_OK;
}
#ifdef  CRABMEAT_MPI
#  define  get_point_and_rank(__px__,__py__,__rank__)                    \
    size_t  px; size_t  py;                                \
    oproc_nc4_decode_point(px,py,_g,_p_grid->I[p]);                    \
    int const  __rank__ = this->get_noderank_that_contains_gridpoint( py, px, _g)/*;*/
lerr_t
oproc_owriter_nc4_t::allocate_and_set_pushgrids(
        oproc_owriter_nc4_grid_container_t *  _pushgrids,
        oproc_owriter_nc4_grid_t const *  _p_grid,
        set_gridpoints_info_t const *  _g)
{
    pushnodes_count_t  pushnodes;
    for ( size_t  p = _p_grid->n_I-_p_grid->n_I_push;  p < _p_grid->n_I;  ++p)
    {
        get_point_and_rank(px,py,pushrank);
        if ( pushrank == _p_grid->rank)
        {
            CBM_LogWarn( "oproc_owriter_nc4_t::allocate_and_set_pushgrids(): ",
                    "pushed point found to be inside my map :-|  [x=",px,",y=",py,"]");
            continue;
        }
        if ( pushnodes.find( pushrank) != pushnodes.end())
        {
            pushnodes[pushrank] += 1;
        }
        else
        {
            pushnodes[pushrank] = 1;
        }
    }

    if ( pushnodes.size() == 0)
    {
        return  LDNDC_ERR_OK;
    }

    oproc_owriter_nc4_grid_container_t  my_pushgrids;
    lerr_t  rc_alloc = this->allocate_pushgrids( &my_pushgrids, &pushnodes);
    if ( rc_alloc)
    {
        CBM_LogError( "oproc_owriter_nc4_t::allocate_and_set_pushgrids(): ",
                "failed to allocate push grid buffers");
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_set = this->set_pushgrids( &my_pushgrids, _p_grid, _g);
    if ( rc_set)
    {
        CBM_LogError( "oproc_owriter_nc4_t::allocate_and_set_pushgrids(): ",
                "failed to set push grids");
        return  LDNDC_ERR_FAIL;
    }
    this->move_pushgrids( _pushgrids, &my_pushgrids);

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_nc4_t::allocate_pushgrids(
        oproc_owriter_nc4_grid_container_t *  _pushgrids,
        pushnodes_count_t const *  _pn)
{
    size_t  i = 0;
    for ( pushnodes_count_t::const_iterator  node = _pn->begin();
            node != _pn->end();  ++node, ++i)
    {
        oproc_owriter_nc4_grid_t  pg;
        pg.rank = node->first;
        pg.owriter = this;
        int  rc_alloc = pg.allocate( node->second);
        if ( rc_alloc)
        {
            CBM_LogError( "oproc_owriter_nc4_t::allocate_pushgrids(): ",
                    "nomem");
            return  LDNDC_ERR_NOMEM;
        }
        _pushgrids->push_back( pg);

// sk:dbg        CBM_LogError( "allocating push node buffer for ",pg.rank," with ", pg.n_I, " entries @",mpi::comm_rank( mpi::comm_ldndc));
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_nc4_t::set_pushgrids(
        oproc_owriter_nc4_grid_container_t *  _pushgrids,
        oproc_owriter_nc4_grid_t const *  _p_grid,
        set_gridpoints_info_t const *  _g)
{
    oproc_owriter_nc4_grid_container_t::iterator  gi = _pushgrids->begin();
    while ( gi != _pushgrids->end())
    {
        gi->n_I_push = 0;
        ++gi;
    }
    for ( size_t  p = _p_grid->n_I-_p_grid->n_I_push;  p < _p_grid->n_I;  ++p)
    {
        get_point_and_rank(px,py,pushrank);
        gi = _pushgrids->begin();
        while ( gi != _pushgrids->end())
        {
            if ( gi->rank == pushrank)
            {
                break;
            }
            ++gi;
        }
        if ( gi == _pushgrids->end())
        {
            if ( pushrank == mpi::comm_rank( mpi::comm_ldndc))
            {
                CBM_LogWarn( "oproc_owriter_nc4_t::set_pushgrids(): ",
                        "found point to push to belong to myself .. damn.");
                continue;
            }
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        if ( gi->I && ( gi->n_I_push < gi->n_I))
        {
            gi->I[gi->n_I_push++] = oproc_nc4_map_point_to_window(px,py,_g);
        }
        else
        {
            CBM_LogError( "oproc_owriter_nc4_t::set_pushgrids(): ",
                    "[BUG]  receiving more points than expected.  [rank=",_p_grid->rank,",pushrank=",pushrank,",n=",gi->n_I,"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_nc4_t::move_pushgrids(
        oproc_owriter_nc4_grid_container_t *  _pushgrids,
        oproc_owriter_nc4_grid_container_t *  _my_pushgrids)
{
    oproc_owriter_nc4_grid_container_t::iterator  gi = _my_pushgrids->begin();
    while ( gi != _my_pushgrids->end())
    {
        _pushgrids->push_back( *gi);
        gi = _my_pushgrids->erase( gi);
    }
    return  LDNDC_ERR_OK;
}
void
oproc_owriter_nc4_t::mpi_add_shadowwriter(
        int  _targetrank, int  _targettag)
{
    shadowwriter_info_t  si;
    si.rank = _targetrank;
    si.tag = _targettag;

    this->shadowwriters_info.push_back( si);
}
lerr_t
oproc_owriter_nc4_t::mpi_append_record_to_message(
        sink_record_srv_t const *  _record)
{
// sk:TODO    if ( this->shadowwriters_info.size() == 0)
// sk:TODO    {
// sk:TODO        return  LDNDC_ERR_OK;
// sk:TODO    }

    void **  data = const_cast< void ** >( _record->data);
    int  rc_pack = this->pcomm.pack( data);
    if ( rc_pack)
    {
        CBM_LogError( "oproc_owriter_nc4_t::mpi_append_record_to_message(): ",
                "failed to append record to timestep message  [timestep=",_record->seq->timestep,",#record=",_record->seq->records_in_timestep,"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_nc4_t::mpi_send_records(
        int  /*_timestep*/)
{
    lerr_t  rc_send_w = LDNDC_ERR_OK;
// sk:dbg    CBM_LogDebug( "sending to ",this->shadowwriters_info.size()," shadowwriters");
    for ( size_t  s = 0;  s < this->shadowwriters_info.size();  ++s)
    {
        CBM_LogDebug( "{",mpi::comm_rank( mpi::comm_ldndc),"} sending to (",this->shadowwriters_info[s].rank,",",this->shadowwriters_info[s].tag,")  sum(points)=",this->pcomm.n_pbuf);
        int  rc_send = this->pcomm.isend( this->shadowwriters_info[s].rank, this->shadowwriters_info[s].tag);
        if ( rc_send)
        {
            rc_send_w = LDNDC_ERR_FAIL;
        }
    }
    return  rc_send_w;
}
lerr_t
oproc_owriter_nc4_t::mpi_initialize_communicator(
                sink_layout_t const *  _layout)
{
    if ( !_layout)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    this->pcomm.comm = mpi::comm_ldndc;
    this->pcomm.set_layout( _layout);

    return  LDNDC_ERR_OK;
}
#endif  /*  CRABMEAT_MPI  */

void
oproc_owriter_nc4_t::initialize_storagebroker(
        netcdf_mapsbroker_t *  _mapsbroker)
{
    crabmeat_assert( _mapsbroker);
    this->storagebrk.initialize( _mapsbroker->get_entity_types(), _mapsbroker->get_number_of_entities());
}

}

