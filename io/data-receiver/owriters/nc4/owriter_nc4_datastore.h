/*!
 * @brief
 *     netCDF4 output writer: data store abstraction
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_NC4_DATASTORE_H_
#define  LDNDC_IO_OUTPUTWRITER_NC4_DATASTORE_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_file.h"

#include  "utils/lutils.h"
#include  "log/cbm_baselog.h"

namespace ldndc {

struct  CBM_API  netcdf_entitystack_t
{
    netcdf_entitystack_t();
    ~netcdf_entitystack_t();

    template < typename  _LT >
    void  clear(
                   typename nc_atomic_t< _LT >::type  /*value*/);


    size_t  stacksize() const { return  this->buf_size; }

    template < typename  _LT >
    typename nc_atomic_t< _LT >::type *  request_stack();
    template < typename  _LT >
    typename nc_atomic_t< _LT >::type *  request_storage(
            size_t /*index*/, size_t * = NULL /*update max stacksize*/);

    template < typename  _LT >
    int  resize_storage(
            size_t /*new size*/, size_t * = NULL /*update max stacksize*/);
    template < typename  _LT >
    int  reallocate_storage(
            size_t /*new size*/);

    void *  buf;
    size_t  buf_size;
};

}

template < typename  _LT >
void
ldndc::netcdf_entitystack_t::clear(
        typename nc_atomic_t< _LT >::type  _value)
{
    cbm::mem_set< typename nc_atomic_t< _LT >::type >(
            static_cast< typename nc_atomic_t< _LT >::type * >( this->buf), this->buf_size, _value);
}

template < typename  _LT >
typename ldndc::nc_atomic_t< _LT >::type *
ldndc::netcdf_entitystack_t::request_storage(
        size_t  _index, size_t *  _max_stacksize)
{
    int  rc_resize = this->resize_storage < _LT >( _index+1, _max_stacksize);
    if ( rc_resize != 0)
    {
        return  NULL;
    }

    return  static_cast< typename nc_atomic_t< _LT >::type * >( this->buf) + _index;
}
template < typename  _LT >
typename ldndc::nc_atomic_t< _LT >::type *
ldndc::netcdf_entitystack_t::request_stack()
{
    return  this->request_storage< _LT >( 0);
}


template < typename  _LT >
int
ldndc::netcdf_entitystack_t::resize_storage(
        size_t  _new_buf_size, size_t *  _max_stacksize)
{
    if ( this->buf_size >= _new_buf_size)
    {
        if ( _max_stacksize && ( *_max_stacksize < this->buf_size))
        {
            *_max_stacksize = this->buf_size;
        }
        return  0;
    }

// sk:dbg    CBM_LogDebug( "netcdf_entitystack_t::resize_storage():  resizing storage from ",this->buf_size," to ",_new_buf_size);
    int  rc = this->reallocate_storage< _LT >( _new_buf_size);
    if ( rc == -1)
    {
        return  -1;
    }

    if ( _max_stacksize && ( *_max_stacksize < this->buf_size))
    {
        *_max_stacksize = _new_buf_size;
    }
    return  0;
}


template < typename  _LT >
int
ldndc::netcdf_entitystack_t::reallocate_storage(
        size_t  _new_buf_size)
{
    typedef  typename nc_atomic_t< _LT >::type  nc_T;
    nc_T *  new_buf = CBM_DefaultAllocator->allocate_init_type< nc_T >( _new_buf_size, invalid_t< nc_T >::value);
    if ( !new_buf)
    {
        CBM_LogError( "netcdf_entitystack_t::reallocate_storage(): ",
                "failed to reallocate storage");
        return  -1;
    }

    if ( this->buf_size > 0)
    {
        crabmeat_assert( this->buf);
        cbm::mem_cpy< nc_T >( new_buf, static_cast< nc_T * >( this->buf), this->buf_size);
        CBM_DefaultAllocator->deallocate( this->buf);
    }
    else
    {
        crabmeat_assert( !this->buf);
    }
    this->buf = new_buf;
    this->buf_size = _new_buf_size;

    return  0;
}

namespace ldndc {

/* memory store for a single entity (=nc variable) */
struct  CBM_API  netcdf_entitycache_t
{
    enum
    {
        /* because all kernels are syncronized with
         * respect to time, any value greater 1 does
         * not have any effect.
         */
        DATASTORE_MAX = 1 << 0,

        DATASTORE_MASK = DATASTORE_MAX-1
    };
    static size_t  max_cachesize() { return  DATASTORE_MAX; }

    netcdf_entitycache_t();
    ~netcdf_entitycache_t();

    netcdf_entitystack_t *  get_entitystack( int _timestep) { return  &this->entitystore[entitystore_index( _timestep)]; }
    nc_type  get_nctype() const { return  this->nctype; }
    void  set_nctype( nc_type  _nctype) { this->nctype = _nctype; }
    private:
        netcdf_entitystack_t  entitystore[DATASTORE_MAX];
        nc_type  nctype;

        static int  entitystore_index( int _timestep) { return  _timestep & static_cast< int >( DATASTORE_MASK); }
};

class  CBM_API  netcdf_storagebroker_t
{
    public:
        enum
        {
            NO_FLAG = 0,
            NO_RESIZE = 0x01
        };
    public:
        netcdf_storagebroker_t();
        ~netcdf_storagebroker_t();

        bool  var_exists(
                int  _var_nb)
        const
        {
            return  this->entity_caches[_var_nb].get_nctype() != ncNoType;
        }

        size_t  stacksize() const { return  this->max_stacksize; }

        template < typename  _LT >
        typename nc_atomic_t< _LT >::type *
              get_storage(
                int /*ncvar index*/,
                int /*timestep*/, int /*stackindex*/,
                lflags_t = NO_FLAG /*flags to control behavior*/);

        int  initialize(
                nc_type const * /*entity types*/, size_t /*number of entities*/);

    private:
        netcdf_entitycache_t *  entity_caches;
        /* number of nc variables in maps cache */
        size_t  n_entity_caches;
        /* number of entries per variable (note, that
         * all variable should by design have the same
         * stack size)
         */
        size_t  max_stacksize;

        int  allocate_entity_caches(
                nc_type const * /*entity types*/, size_t /*number of entities*/);
        void  deallocate_entity_caches();
};
}

#include  "log/cbm_baselog.h"
template < typename  _LT >
typename ldndc::nc_atomic_t< _LT >::type *
ldndc::netcdf_storagebroker_t::get_storage(
        int  _var_nb,
        int  _timestep, int  _stackindex,
        lflags_t  _flags)
{
    crabmeat_assert( _var_nb < static_cast< int >( this->n_entity_caches));
    crabmeat_assert( this->entity_caches);

    if ( !this->var_exists( _var_nb))
    {
        return  NULL;
    }

    netcdf_entitystack_t *  e_stack = this->entity_caches[_var_nb].get_entitystack( _timestep);
    if ( !e_stack || (( _flags & NO_RESIZE) && (_stackindex >= static_cast< int >( e_stack->stacksize()))))
    {
        return  NULL;
    }

    typename nc_atomic_t< _LT >::type *  e_buf = 
        e_stack->request_storage< _LT >( _stackindex, &this->max_stacksize);
    return  e_buf;
}

namespace ldndc {
class  CBM_API  netcdf_mapsbroker_t
{
    public:
        netcdf_mapsbroker_t();
        ~netcdf_mapsbroker_t();

        bool  var_exists(
                int  _var_nb)
        const
        {
            return  this->entity_types[_var_nb] != ncNoType;
        }
        nc_type  get_var_nctype(
                int  _var_nb) const { return  this->entity_types[_var_nb]; }

        template < typename  _LT >
        typename nc_atomic_t< _LT >::type *  get_cleared_map_storage(
                int /*time index*/, int /*cache size*/);

        size_t  get_number_of_entities() const { return  this->n_entity_types; }
        nc_type const *  get_entity_types() const { return  this->entity_types; }

        void  set_maps_dimensions(
                size_t /*stack size*/,
                netcdf_file_t::map_window_t const *);

        size_t  initialize(
                sink_layout_t const *);

        size_t  stacksize() const { return  this->ssz; }
        size_t  map_size() const { return  this->map_dim; }
    private:
        size_t  ssz;
        size_t  mapoffs_lat, mapoffs_lon;
    private:
        /* map dimensions */
        size_t  map_dim;
        size_t  flat_dim;
        void *  mapmem;

        nc_type *  entity_types;
        /* number of nc variables in maps cache */
        size_t  n_entity_types;

        void  assign_nctypes(
                sink_layout_t const *);

        void  allocate_entity_types(
                size_t /*number of entities*/);
        void  deallocate_entity_types();
};
}

#include  "mpi/lmpi.h"
template < typename  _LT >
typename ldndc::nc_atomic_t< _LT >::type *
ldndc::netcdf_mapsbroker_t::get_cleared_map_storage(
        int  _timeindex, int  _cachesize)
{
    crabmeat_assert(this->flat_dim > 0);
    crabmeat_assert(_cachesize > 0);
    crabmeat_assert(sizeof(double) >= sizeof(typename ldndc::nc_atomic_t< _LT >::type));
    if ( !this->mapmem)
    {
        this->mapmem = CBM_DefaultAllocator->allocate( _cachesize*this->flat_dim*sizeof(double /*largest expected datatype*/));
        if ( !this->mapmem)
        {
            CBM_LogError( "netcdf_mapsbroker_t::get_cleared_map_storage(): ",
                    "nomem");
            return  NULL;
        }
        CBM_LogDebug( "netcdf_mapsbroker_t::get_cleared_map_storage(): ",
                "allocated mapsbuffer of size ",this->flat_dim, " [element-size=",sizeof(double),"]");
    }
    size_t const  slice = static_cast< size_t >( _timeindex % _cachesize);
    cbm::mem_set( static_cast< typename nc_atomic_t< _LT >::type * >( this->mapmem), this->flat_dim,
            invalid_t< typename nc_atomic_t< _LT >::type >::value);
    return  static_cast< typename nc_atomic_t< _LT >::type * >( this->mapmem);
}


#endif  /*  !LDNDC_IO_OUTPUTWRITER_NC4_DATASTORE_H_  */

