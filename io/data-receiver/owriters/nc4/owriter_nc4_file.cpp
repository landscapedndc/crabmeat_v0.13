/*!
 * @brief
 *    netCDF4 output writer: netCDF file abstraction  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_file.h"
#include  "string/lstring-compare.h"
#ifdef  CRABMEAT_MPI
#  include  <netcdf_par.h>
#endif

namespace ldndc {

nc_type
resolve_nctype(
        atomic_datatype_t  _data_type)
{
    if ( _data_type == LDNDC_FLOAT64 /*|| _data_type == LDNDC_FLOAT128*/)
    {
        return  nc_atomic_t< ldndc_flt64_t >::nc_type_id;
    }
    else if ( _data_type == LDNDC_FLOAT32)
    {
        return  nc_atomic_t< ldndc_flt32_t >::nc_type_id;
    }
    else if ( _data_type == LDNDC_INT32)
    {
        return  nc_atomic_t< ldndc_int32_t >::nc_type_id;
    }
// sk:off    else if ( _data_type == LDNDC_UINT32)
// sk:off    {
// sk:off        return  nc_atomic_t< ldndc_uint32_t >::nc_type_id;
// sk:off    }
// sk:off    else if ( _data_type == LDNDC_INT64)
// sk:off    {
// sk:off        return  nc_atomic_t< ldndc_int64_t >::nc_type_id;
// sk:off    }
// sk:off    else if ( _data_type == LDNDC_UINT64)
// sk:off    {
// sk:off        return  nc_atomic_t< ldndc_uint64_t >::nc_type_id;
// sk:off    }
    else if ( _data_type == LDNDC_INT16)
    {
        return  nc_atomic_t< ldndc_int16_t >::nc_type_id;
    }
    else if ( _data_type == LDNDC_UINT16)
    {
        return  nc_atomic_t< ldndc_uint16_t >::nc_type_id;
    }
    else if ( _data_type == LDNDC_INT8 || _data_type == LDNDC_UINT8 || _data_type == LDNDC_CHAR)
    {
        return  nc_atomic_t< ldndc_char_t >::nc_type_id;
    }
    else
    {
        return  ncNoType;
    }
}



netcdf_file_t::netcdf_file_t()
        : ncfile( nc_badfile),
          n_ncdim( NCDIM_CNT),
          vars( NULL), n_vars( invalid_t< size_t >::value)
{
    this->reset();
}

netcdf_file_t::~netcdf_file_t()
{
    this->close();
}

lerr_t
netcdf_file_t::open(
        sink_info_t const *  _sink_info)
{
    if ( this->ncfile != nc_badfile)
    {
        CBM_LogError( "[BUG]  netcdf_file_t::open(): ",
                   "file seems to be already opened (close file first)");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    nc_err_t  rc_create =
#ifdef  CRABMEAT_MPI
    nc_create_par( _sink_info->full_name().c_str(), NC_CLOBBER|LDNDC_NETCDF4_FORMAT,
            mpi::comm_ldndc, MPI_INFO_NULL, &this->ncfile);
#else
    nc_create( _sink_info->full_name().c_str(), NC_CLOBBER|LDNDC_NETCDF4_FORMAT, &this->ncfile);
#endif
    if ( rc_create == NC_NOERR)
    {
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}

void
netcdf_file_t::flush()
{
    if ( this->ncfile != nc_badfile)
    {
        nc_sync( this->ncfile);
           }
}

void
netcdf_file_t::close()
{
    if ( this->ncfile != nc_badfile)
    {
        this->write_heights();

        nc_close( this->ncfile);
        this->ncfile = nc_badfile;
    }

    this->reset();
}


lerr_t
netcdf_file_t::define_mapwindow(
        map_window_t const *  _mwin)
{
    this->mwin = *_mwin;
    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::set_structure(
        sink_entity_layout_t const *  _ent_layout,
        geom_mapinfo_t const *  _mapinfo, int  _dim_4)
{
    if ( this->vars)
    {
        return  LDNDC_ERR_OK;
    }
    CBM_LogDebug( "netcdf_file_t::set_structure(): ",
            "setting structure");

    if (( this->ncfile == nc_badfile) || !_ent_layout)
    {
        CBM_LogError( "netcdf_file_t::set_structure(): invalid arguments");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( _ent_layout->ents.size == 0 /*implies rank>0 etc..*/)
    {
        this->n_vars = 0;
        return  LDNDC_ERR_OK;
    }

    this->mapinfo = *_mapinfo;
    this->size_elev = _dim_4;
    if (( this->n_lat() * this->n_lon() * this->n_elev()) == 0)
    {
        CBM_LogError( "netcdf_file_t::set_structure(): ",
                "invalid dimension, at least one dimension has size 0");
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_dim = this->define_dimensions();
    if ( rc_dim)
    {
        this->reset();
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_var = this->define_variables( _ent_layout);
    if ( rc_var)
    {
        this->reset();
        return  LDNDC_ERR_FAIL;
    }

    nc_err_t  rc_enddef = nc_enddef( this->ncfile);
    if ( rc_enddef != NC_NOERR)
    {
        return  LDNDC_ERR_FAIL;
    }

    crabmeat_assert( this->vars && (this->n_vars > 0));

    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::define_dimensions()
{
    CBM_LogDebug( "defining dimensions: ",this->n_lat(),"x",this->n_lon());
    this->n_ncdim = NCDIM_CNT;
    if (( this->n_lat() * this->n_lon()) == 1)
    {
        this->n_ncdim = NCDIM_CNT-1;
    }

    static char const *  ncdim_names[NCDIM_CNT] = { TIME_VARNAME, ELEVATION_VARNAME, LATITUDE_VARNAME, LONGITUDE_VARNAME};
    ldndc_ncsize_t const  ncdim_sizes[NCDIM_CNT] = { NC_UNLIMITED, this->n_elev(), this->n_lat(), this->n_lon()};

    for ( int  dim = 0;  dim < this->n_ncdim;  ++dim)
    {
        nc_err_t  rc_dim = nc_def_dim( this->ncfile, ncdim_names[dim], ncdim_sizes[dim], &this->ncdim[dim]);
        if ( rc_dim != NC_NOERR)
        {
            CBM_LogError( "netcdf_file_t::define_dimensions(): ",
                    "failed to define dimension \"",ncdim_names[dim],"\"");
            return  LDNDC_ERR_FAIL;
        }
    }

    static char const *  lat_attrs[] = { "units","degrees_north", "standard_name",LATITUDE_VARNAME,  NULL,NULL /*sentinel*/};
    lerr_t  rc_lat = this->write_mesh( LAT, LATITUDE_VARNAME, lat_attrs,
            this->mapinfo.bb.dy() / static_cast< double >( this->mapinfo.dim.n_y), static_cast< double >( this->mapinfo.bb.ul_y));
    static char const *  lon_attrs[] = { "units","degrees_east", "standard_name",LONGITUDE_VARNAME,  NULL,NULL /*sentinel*/};
    lerr_t  rc_lon = this->write_mesh( LON, LONGITUDE_VARNAME, lon_attrs,
            this->mapinfo.bb.dx() / static_cast< double >( this->mapinfo.dim.n_x), static_cast< double >( this->mapinfo.bb.ul_x));
    static char const *  elev_attrs[] = { "units","meters", "positive","down",  NULL,NULL /*sentinel*/};
    lerr_t  rc_elev = this->define_height( ELEV, ELEVATION_VARNAME, elev_attrs);
    if ( rc_lat || rc_lon || rc_elev)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::write_mesh(
        dimension_e  _dim, char const *  _dim_name,
        char const **  _attrs,
        double  _dp, double  _p0)
{
    crabmeat_assert((_dim < this->n_ncdim) && ncdim[_dim]);
    nc_var_t  v_d = nc_badvar;
    nc_err_t  rc_dvar = nc_def_var( this->ncfile, _dim_name, NC_DOUBLE, 1, &this->ncdim[_dim], &v_d);
    if (( rc_dvar != NC_NOERR) || ( v_d == nc_badvar))
    {
        CBM_LogError( "netcdf_file_t::write_mesh(): ",
                "failed to create mesh variable  [name=",_dim_name,"]");
        return  LDNDC_ERR_FAIL;
    }
    this->write_coards_attributes( v_d, _attrs);

    ldndc_ncsize_t  dim_size = this->dimension_size( _dim);
    double *  d = CBM_DefaultAllocator->allocate_type< double >( dim_size);
    if ( !d)
    {
        CBM_LogError( "netcdf_file_t::write_mesh(): ",
                "failed to create and write mesh information  [dimension=",_dim_name,"]");
        return  LDNDC_ERR_FAIL;
    }
    double const  dp = _dp;
    double  p0 = _p0;
    for ( ldndc_ncsize_t  k = 0;  k < dim_size;  ++k)
    {
        d[k] = p0;
        p0 += dp;
    }
    ldndc_ncsize_t const  nc_pos_d[1] = { 0};
    ldndc_ncsize_t const  nc_size_d[1] = { dim_size};
    nc_put_vara_double( this->ncfile, v_d, nc_pos_d, nc_size_d, d);
    CBM_DefaultAllocator->deallocate( d);

    CBM_LogVerbose( "netcdf_file_t::write_mesh(): ",
            "wrote mesh dimension  [name=",_dim_name,"]");
    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::define_height(
        dimension_e  _dim, char const *  _dim_name, char const **  _attrs)
{
    if ( this->dimension_size( _dim) < 1)
    {
        CBM_LogError( "netcdf_file_t::define_height(): ",
                "illegal configuration");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    nc_var_t  v_h = nc_badvar;
    nc_err_t  rc_vh = nc_def_var( this->ncfile, _dim_name, NC_DOUBLE, 1, &this->ncdim[_dim], &v_h);
    if ( rc_vh || ( v_h == nc_badvar))
    {
        return  LDNDC_ERR_FAIL;
    }
    if ( this->write_coards_attributes( v_h, _attrs))
    {
        CBM_LogWarn( "netcdf_file_t::write_heights_data(): ",
                "failed to add COARDS attributes to netCDF variable  [name=",_dim_name,"]");
    }

    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::write_heights()
{
    if (( this->ncfile == nc_badfile) || !this->vars)
    {
        return  LDNDC_ERR_FAIL;
    }

    ldndc_ncsize_t  column_pos[2] = { 0, 0};
    ldndc_ncsize_t const *  column_pos_ptr = column_pos;

    ldndc_ncsize_t const  elev_size = dimension_size( ELEV);

    size_t  h = this->find_heights_data();
    if ( cbm::is_invalid( h))
    {
        column_pos_ptr = NULL;
    }
    else if ( this->variable_type( h) != nc_atomic_t< ldndc_flt64_t >::nc_type_id)
    {
        CBM_LogWarn( "netcdf_file_t::write_heights(): ",
                "z-dimension discretization currently only supports 64bit floating point data  [source-variable=","<TODO>","]");
        return  LDNDC_ERR_OK;
    }
    else
    {
        lerr_t  rc_column = this->find_column_position_for_heights_data(
                column_pos, elev_size, this->vars[h]);
        if ( rc_column)
        {
            CBM_LogWarn( "netcdf_file_t::write_heights(): ",
                    "did not find column with height values which were valid in the highest entry; ",
                    "using fake values  [source-variable=","?"/*this->vars[h]->name()*/,"]");
            column_pos_ptr = NULL;
        }
    }

    double *  height_data = CBM_DefaultAllocator->allocate_type< double >( elev_size);
    lerr_t  rc_heights = this->read_heights_data(
            height_data, elev_size, this->vars[h], column_pos_ptr);
    if ( rc_heights == LDNDC_ERR_OK)
    {
        rc_heights = this->write_heights_data(
                ELEVATION_VARNAME, height_data, elev_size);
    }

    CBM_DefaultAllocator->deallocate( height_data);
    return  rc_heights;
}

size_t
netcdf_file_t::find_heights_data()
const
{
    char  varname[NC_MAX_NAME+1];
    for ( size_t  m = 0;  m < this->n_vars;  ++m)
    {
        nc_inq_varname( this->ncfile, this->vars[m], varname);
        if ( cbm::has_prefix( varname, "height[m]", 9))
        {
            return  m;
        }    
    }
    return  invalid_t< size_t >::value;
}
lerr_t
netcdf_file_t::find_column_position_for_heights_data(
        ldndc_ncsize_t *  _column_pos, int  _size,
        nc_var_t  _ncvar)
{
    if (( this->variable_type( _ncvar) != NC_DOUBLE) || ( _size < 1))
    {
        return  LDNDC_ERR_FAIL;
    }

    ldndc_ncsize_t const  lat_sz = this->dimension_size( LAT);
    ldndc_ncsize_t const  lon_sz = this->dimension_size( LON);

    for ( ldndc_ncsize_t  p_lat = 0;  p_lat < lat_sz;  ++p_lat)
    {
        for ( ldndc_ncsize_t  p_lon = 0;  p_lon < lon_sz;  ++p_lon)
        {
            ldndc_ncsize_t  nc_pos[NCDIM_CNT] = { 0, static_cast< ldndc_ncsize_t >( _size-1), p_lat, p_lon};
            double  val = invalid_t< double >::value;
            nc_err_t  rc_var1 = nc_get_var1_double( this->ncfile, _ncvar, nc_pos, &val);
            if ( rc_var1 != NC_NOERR)
            {
                return  LDNDC_ERR_FAIL;
            }
            if ( cbm::is_valid( val))
            {
                _column_pos[0] = p_lon;
                _column_pos[1] = p_lat;
                return  LDNDC_ERR_OK;
            }
        }
    }

    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}

lerr_t
netcdf_file_t::read_heights_data(
        double *  _height_data, int  _size,
        nc_var_t  _ncvar, ldndc_ncsize_t const *  _column_pos)
{
    if ( !_height_data)
    {
        CBM_LogError( "netcdf_file_t::read_heights_data(): ",
                "nomem");
        return  LDNDC_ERR_NOMEM;
    }

    if ( !_column_pos)
    {
        /* use fake values */

        for ( int  l = 0;  l < _size;  ++l)
        {
            _height_data[l] = 1.0 / static_cast< double >( _size);
        }
    }
    else
    {
        /* use data from height column */

        ldndc_ncsize_t const  nc_pos[NCDIM_CNT] = { 0, 0, _column_pos[1], _column_pos[0]};
        ldndc_ncsize_t const  nc_size[NCDIM_CNT] = { 1, static_cast< ldndc_ncsize_t >( _size), 1, 1};
        if ( nc_get_vara_double( this->ncfile, _ncvar, nc_pos, nc_size, _height_data) != NC_NOERR)
        {
            CBM_LogError( "netcdf_file_t::read_heights_data(): ",
                    "failed to read height data from variable  [variablename=","<TODO>","]");
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
netcdf_file_t::write_heights_data(
        char const *  _var_name,
        double const *  _height_data, int  /*_size*/)
{
    nc_var_t  h_var = nc_badvar;
    nc_err_t  rc_hid = nc_inq_varid( this->ncfile, _var_name, &h_var);
    if (( rc_hid == NC_NOERR) && ( h_var != nc_badvar))
    {
        CBM_LogDebug( "found height variable  id=",h_var);
        nc_err_t  rc_hput = nc_put_var_double( this->ncfile, h_var, _height_data);
        if ( rc_hput != NC_NOERR)
        {
            CBM_LogError( "netcdf_file_t::write_heights_data(): ",
                    "failed to write netCDF variable  [name=",_var_name,"]");
            return  LDNDC_ERR_FAIL;
        }
    }
    else
    {
        CBM_LogError( "netcdf_file_t::write_heights_data(): ",
                "failed to create netCDF variable  [name=",_var_name,"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::write_coards_attributes(
        nc_var_t  _ncvar, char const **  _attrs)
{
    if (( _ncvar == nc_badvar) || !_attrs)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    size_t  j = 0;
    while ( _attrs[j])
    {
        if ( nc_put_att_text( this->ncfile, _ncvar, _attrs[j], cbm::strlen( _attrs[j+1]), _attrs[j+1]) != NC_NOERR)
        {
            return  LDNDC_ERR_FAIL;
        }
        j += 2;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
netcdf_file_t::define_variables(
        sink_entity_layout_t const *  _ent_layout)
{
    sink_entities_t const *  ents = &_ent_layout->ents;
    sink_layout_t const *  layout = &_ent_layout->layout;

    this->vars = CBM_DefaultAllocator->allocate_init_type< nc_var_t >( ents->size, nc_badvar);
    if ( !this->vars)
    {
        this->n_vars = invalid_t< size_t >::value;
        CBM_LogError( "netcdf_file_t::define_variables(): ",
                "nomem");
        return  LDNDC_ERR_NOMEM;
    }

    this->n_vars = oproc_owriter_layouthelper_t::get_number_of_entities( layout);
    ldndc_output_size_t  m = 0;
    for ( ldndc_output_rank_t  r = 0;  r < layout->rank;  ++r)
    {
        nc_type  nctype = resolve_nctype( layout->types[r]);
        for ( ldndc_output_size_t  j = 0;  j < layout->sizes[r];  ++j)
        {
            CBM_LogDebug( "creating variable \"",ents->ids[m], "\""); 
            if ( nctype == NC_NAT)
            {
                CBM_LogWarn( "netcdf_file_t::define_variables(): ",
                        "unsupported data type encountered");
                this->vars[m] = nc_badvar;
            }
            else
            {
                nc_err_t  rc_var = nc_def_var( this->ncfile, ents->ids[m], nctype, 
                        this->n_ncdim, &this->ncdim[0], &this->vars[m]);
                if (( rc_var != NC_NOERR) || ( this->vars[m] == nc_badvar) || this->write_fillvalue( this->vars[m], nctype))
                {
                    CBM_LogError( "netcdf_file_t::define_variables(): ",
                            "failed to create netCDF variable or its fill value  [variable=",ents->ids[m],"]");
                    return  LDNDC_ERR_FAIL;
                }
#ifdef  CRABMEAT_MPI
                nc_var_par_access( this->ncfile, this->vars[m], NC_COLLECTIVE);
#endif
            }

            ++m;
        }
    }
    crabmeat_assert( static_cast< size_t >( m) == this->n_vars);

    return  LDNDC_ERR_OK;
}
lerr_t
netcdf_file_t::write_fillvalue(
        nc_var_t  _ncvar, nc_type  _nctype)
{
    if ( _ncvar == nc_badvar)
    {
        CBM_LogError( "netcdf_file_t::write_fillvalue(): ",
                "received bad variable");
        return  LDNDC_ERR_FAIL;
    }

    nc_err_t  rc_attr = 1;
    if ( _nctype == nc_atomic_t< ldndc_flt64_t >::nc_type_id)
    {
        rc_attr = nc_put_att_double( this->ncfile, _ncvar, "_FillValue", nc_atomic_t< ldndc_flt64_t >::nc_type_id, 1, &invalid_dbl);
    }
    else if ( _nctype == nc_atomic_t< ldndc_flt32_t >::nc_type_id)
    {
        rc_attr = nc_put_att_float( this->ncfile, _ncvar, "_FillValue", nc_atomic_t< ldndc_flt32_t >::nc_type_id, 1, &invalid_flt);
    }
    else if ( _nctype == nc_atomic_t< ldndc_int32_t >::nc_type_id)
    {
        rc_attr = nc_put_att_int( this->ncfile, _ncvar, "_FillValue", nc_atomic_t< ldndc_int32_t >::nc_type_id, 1, &invalid_int);
    }
    else if ( _nctype == nc_atomic_t< ldndc_int16_t >::nc_type_id)
    {
        rc_attr = nc_put_att_short( this->ncfile, _ncvar, "_FillValue", nc_atomic_t< ldndc_int16_t >::nc_type_id, 1, &invalid_sint);
    }
    else if ( _nctype == nc_atomic_t< ldndc_int8_t >::nc_type_id)
    {
        rc_attr = nc_put_att_schar( this->ncfile, _ncvar, "_FillValue", nc_atomic_t< ldndc_int8_t >::nc_type_id, 1, &invalid_schr);
    }
    else if ( _nctype == nc_atomic_t< ldndc_uint8_t >::nc_type_id)
    {
        rc_attr = nc_put_att_uchar( this->ncfile, _ncvar, "_FillValue", nc_atomic_t< ldndc_uint8_t >::nc_type_id, 1, &invalid_uchr);
    }
    else
    {
        /* no op */
    }

    return  ( rc_attr == 0) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}

ldndc_ncsize_t
netcdf_file_t::dimension_size(
        dimension_e  _dim)
const
{
    if ( this->ncfile == nc_badfile || !this->ncdim || this->ncdim[_dim] == nc_baddim)
    {
        return  invalid_t< ldndc_ncsize_t >::value;
    }

    ldndc_ncsize_t  dim_size = 0;
    nc_err_t  rc_dimsize = nc_inq_dimlen( this->ncfile, this->ncdim[_dim], &dim_size);
    if ( rc_dimsize != NC_NOERR)
    {
        return  invalid_t< ldndc_ncsize_t >::value;
    }

    return  dim_size;
}
nc_type
netcdf_file_t::variable_type(
        int  _ncvar)
const
{
    if ( this->ncfile == nc_badfile || !this->vars || this->vars[_ncvar] == nc_badvar)
    {
        return  NC_NAT;
    }

    nc_type  vartype = NC_NAT;
    nc_err_t  rc_vartype = nc_inq_vartype( this->ncfile, this->vars[_ncvar], &vartype);
    if ( rc_vartype != NC_NOERR)
    {
        return  NC_NAT;
    }

    return  vartype;
}


void
netcdf_file_t::reset()
{
    if ( this->vars)
    {
        CBM_DefaultAllocator->deallocate( this->vars);
        this->vars = NULL;
    }
    this->n_vars = invalid_t< size_t >::value;

    for ( int  dim = 0;  dim < NCDIM_CNT;  ++dim)
    {
        this->ncdim[dim] = nc_baddim;
    }
}



lerr_t
grid_decomposition_t::get_map_dimensions_according_to_my_noderank(
        netcdf_file_t::map_window_t *  _map_window,
        int  _n_lat, int  _n_lon)
{
    int  mpirank = mpi::comm_rank( mpi::comm_ldndc);
    int  mpisize = mpi::comm_size( mpi::comm_ldndc);
    int  chunksize_lat = _n_lat;
    int  chunksize_lon = _n_lon / mpisize;

    if ( chunksize_lat == 0 || chunksize_lon == 0)
    {
        CBM_LogError( "[TODO]  oproc_landscape_owriter_nc4_t::establish_mapwindow(): ",
                "currently writing to a grid with less than #nodes columns is not supported");
        return  LDNDC_ERR_FAIL;
    }

    _map_window->offs_lat = 0;
    _map_window->offs_lon = mpirank*chunksize_lon;
    _map_window->count_lat = chunksize_lat;
    _map_window->count_lon = chunksize_lon;

    if ( mpirank == ( mpisize - 1))
    {
        /*acquire remaining chunk*/
        _map_window->count_lon = _n_lon - mpirank*chunksize_lon;
    }
// sk:dbg    CBM_LogDebug( "offs_lat=",_map_window->offs_lat, "  offs_lon=",_map_window->offs_lon, "  count_lat=",_map_window->count_lat, "  count_lon=",_map_window->count_lon);
    return  LDNDC_ERR_OK;
}

int
grid_decomposition_t::get_noderank_that_contains_gridpoint(
        int  /*_p_lat*/, int  _p_lon,
        int  /*_n_lat*/, int  _n_lon)
{
    int const  mpisize = mpi::comm_size( mpi::comm_ldndc);
    return  ( _p_lon * mpisize) / _n_lon;
}

}

