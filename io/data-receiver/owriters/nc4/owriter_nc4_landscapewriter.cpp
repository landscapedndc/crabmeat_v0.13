/*!
 * @brief
 *    netCDF4 output writer: landscape writer
 *    initialization/deinitialization parts  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriter.h"
#include  "io/data-receiver/owriters/owriter-layout.h"
#include  "io/default-streams.h"

#include  "input/geometry/geometry-srv.h"
#include  "input/setup/setup-srv.h"

#include  "string/lstring-basic.h"
#include  "math/lmath-basic.h"
#include  "math/lmath-float.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

oproc_landscape_owriter_nc4_base_t::oproc_landscape_owriter_nc4_base_t()
        : oproc_landscape_owriter_base_t()
{
}


oproc_landscape_owriter_nc4_base_t::~oproc_landscape_owriter_nc4_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_landscape_owriter_nc4_t)
oproc_landscape_owriter_nc4_t::oproc_landscape_owriter_nc4_t()
        : oproc_landscape_owriter_nc4_base_t(),
          fmt( sink_entity_layout_defaults),

          is_layouted_( 0)
{
    omp::init_lock( &this->owriter_nc4_lock);
}
oproc_landscape_owriter_nc4_t::~oproc_landscape_owriter_nc4_t()
{
    (void)this->close();
    this->release_owriter( invalid_t< lreceiver_descriptor_t >::value /*==all*/);

    if ( this->sink_info.attrs.uniform)
    {
        CBM_DefaultAllocator->deallocate( this->fmt.ents.ids);
        CBM_DefaultAllocator->deallocate( this->fmt.layout.types);
        CBM_DefaultAllocator->deallocate( this->fmt.layout.sizes);
    }

    omp::destroy_lock( &this->owriter_nc4_lock);
}


lerr_t
oproc_landscape_owriter_nc4_t::open(
        sink_info_t const *  _sink_info)
{
    crabmeat_assert( _sink_info);
    if ( this->netcdf.is_open())
    {
        /* attempt to open sink again? close first. */
        return  LDNDC_ERR_OK;
    }

    this->sink_info = *_sink_info;

    if ( ldndc::sink_is_black_hole( _sink_info))
    {
        CBM_LogFatal( "[BUG]  oproc_landscape_owriter_nc4_t::open(): i think we inofficially agreed on not to instantiate receiver for black holes (..well, ok, i was the only voter)");
        return  LDNDC_ERR_FAIL;
    }

    this->netcdf.open( _sink_info);
    if ( this->netcdf.is_valid())
    {
        return  LDNDC_ERR_OK;
    }

    CBM_LogDebug( "oproc_landscape_owriter_nc4_t::open(): failed to open sink  [sink=",_sink_info->identifier,",file=",_sink_info->full_name(),"]");
        return  LDNDC_ERR_FAIL;
}
lerr_t
oproc_landscape_owriter_nc4_t::reopen()
{
// sk:dbg    CBM_LogDebug( "oproc_landscape_owriter_nc4_t::reopen(): sink \"",this->sink_info.identifier,"\"");
    if ( this->netcdf.is_open())
    {
        if ( this->sink_info.attrs.overwrite == 0)
        {
            CBM_LogError( "oproc_landscape_owriter_nc4_t::reopen(): attempting to reopen an overwrite protected sink  [sink=",this->sink_info.identifier,"]");
            return  LDNDC_ERR_INVALID_OPERATION;
        }
        this->close();
// sk:dbg    CBM_LogDebug( "oproc_landscape_owriter_nc4_t::reopen(): closed sink");
    }
    return  this->open( &this->sink_info);
}


lerr_t
oproc_landscape_owriter_nc4_t::close()
{
    this->netcdf.close();

    CBM_LogDebug( "oproc_landscape_owriter_nc4_t::close(): successfully closed sink  [sink=",this->sink_info.identifier,",file=",this->sink_info.full_name(),"]");
    return  LDNDC_ERR_OK;
}


lerr_t
oproc_landscape_owriter_nc4_t::set_layout_owriter(
        oproc_owriter_base_t *  _target,
        sink_record_meta_srv_t const *,
        sink_entity_layout_t const *  _layout)
{
    crabmeat_assert( _target);
    oproc_owriter_nc4_t *  this_target = static_cast< oproc_owriter_nc4_t *>( _target);

#ifdef  CRABMEAT_SANITY_CHECKS
    if ( !this->netcdf.is_open())
    {
        CBM_LogError( "[BUG]  oproc_landscape_owriter_nc4_t::set_layout_owriter(): ",
                "attempt to define entities for closed stream");
        return  LDNDC_ERR_FAIL;
    }
#endif  /*  CRABMEAT_SANITY_CHECKS  */

    lerr_t  rc_defent = LDNDC_ERR_OK;
    if ( this->sink_info.attrs.uniform)
    {
        omp::set_lock( &this->owriter_nc4_lock, "layout");
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (is_layouted_)
#endif
        if ( !this->is_layouted_ && _layout)
        {
            oproc_owriter_layouthelper_t::set_layout( &this->fmt, _layout);
            if ( _layout->layout.entsizes)
            {
                CBM_LogError( "oproc_landscape_owriter_nc4_t::set_layout_owriter(): ",
                        "no support for vector-valued entities");
                rc_defent = LDNDC_ERR_INVALID_OPERATION;
            }

            size_t  n_entities = this->mapsbrk.initialize( &_layout->layout);
            if ( n_entities == 0 || cbm::is_invalid( n_entities))
            {
                rc_defent = LDNDC_ERR_FAIL;
            }
        }
        if ( rc_defent == LDNDC_ERR_OK)
        {
            this_target->initialize_storagebroker( &this->mapsbrk);
            this_target->set_layout_owriter( &this->fmt);
        }
        if ( rc_defent == LDNDC_ERR_OK)
        {
            this->is_layouted_ = 1;
            this->mark_as_dirty();
        }
        else
        {
            CBM_LogError( "oproc_landscape_owriter_nc4_t::set_layout_owriter(): ",
                    "setting layout for netCDF writer failed");
        }
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(is_layouted_)
#endif
        omp::unset_lock( &this->owriter_nc4_lock, "layout");
    }
    else
    {
        CBM_LogError( "oproc_landscape_owriter_nc4_t::set_layout_owriter() :",
                "non-uniform layout for netCDF not supported");
        rc_defent = LDNDC_ERR_FAIL;
    }

    CBM_LogInfo( "oproc_landscape_owriter_nc4_t::set_layout_owriter(): ",
            "successfully set layout for netCDF writer");
    return  rc_defent;
}

bool
oproc_landscape_owriter_nc4_t::flag_required_inputs(
        input_class_set_t *  _input_set)
const
{
    if ( _input_set)
    {
        _input_set->set_request_flag( INPUT_GEOMETRY);
        _input_set->set_request_flag( INPUT_SETUP);
    }
    return  true;
}
lerr_t
oproc_landscape_owriter_nc4_t::pull_required_inputs(
        input_class_set_t *  _input_set,
        oproc_receiver_t *  _owriter)
{
    crabmeat_assert( _input_set);
// sk:dbg    CBM_LogDebug( "pulling required inputs");

    int  n_cells = this->refine_geometry_from_geometry_(
            static_cast< oproc_owriter_nc4_t * >( _owriter),
            static_cast< geometry::input_class_geometry_srv_t const * >( _input_set->get_input_reference( INPUT_GEOMETRY)));
    if ( n_cells < 0)
    {
        CBM_LogInfo( "oproc_landscape_owriter_nc4_t::pull_required_inputs(): ",
                "geometry input not available or incorrect; using location from setup");
        n_cells = this->refine_geometry_from_setup_(
                static_cast< oproc_owriter_nc4_t * >( _owriter),
                static_cast< setup::input_class_setup_srv_t const * >( _input_set->get_input_reference( INPUT_SETUP)));
        if ( n_cells < 0)
        {
            CBM_LogError( "oproc_landscape_owriter_nc4_t::pull_required_inputs(): ",
                "unable to set geometry from site's location information");
            return  LDNDC_ERR_FAIL;
        }
    }
    else if ( n_cells == 0)
    {
        CBM_LogError( "oproc_landscape_owriter_nc4_t::pull_required_inputs(): ",
                "geometry input without data  [id=",_input_set->get_input_reference( INPUT_GEOMETRY)->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }

    CBM_LogDebug( "successfully updated geometry");
    return  LDNDC_ERR_OK;
}

int
oproc_landscape_owriter_nc4_t::refine_geometry_from_geometry_(
        oproc_owriter_nc4_t *  _owriter,
        geometry::input_class_geometry_srv_t const *  _geom)
{
    if ( !_geom)
    {
        CBM_LogInfo( "oproc_landscape_owriter_nc4_t::refine_geometry_from_geometry_(): ",
                "no geometry information in netCDF output writer");
        return  -1;
    }
// sk:dbg    CBM_LogDebug( "refining geometry.. [id=",_geom->object_id(),"]");

    int  n_shapes = this->set_mapinfo_from_geometry_( _geom);
    if ( n_shapes <= 0)
    {
        return  -1;
    }

    int  rc_addshape = this->add_shape_to_gridlayouter( _owriter, _geom->get_shape());
    if ( rc_addshape)
    {
        return  -1;
    }
    return  n_shapes;
}
int
oproc_landscape_owriter_nc4_t::set_mapinfo_from_geometry_(
        geometry::input_class_geometry_srv_t const *  _geom)
{
    omp::set_lock( &this->owriter_nc4_lock, "mapinfo");
    int  n_parts = this->grid_layouter.mapinfo.parts;
    if ( this->owriters_.size() == 1)
    {
        _geom->get_mapinfo( &this->grid_layouter.mapinfo);
        n_parts = this->grid_layouter.mapinfo.parts;
// sk:dbg        CBM_LogDebug( "set map from geometry: parts=",n_parts, "  dim=",this->grid_layouter.mapinfo.dim.size());
    }
    omp::unset_lock( &this->owriter_nc4_lock, "mapinfo");

    return  n_parts;
}

int
oproc_landscape_owriter_nc4_t::refine_geometry_from_setup_(
        oproc_owriter_nc4_t *  _owriter,
        setup::input_class_setup_srv_t const *  _setup)
{
    if ( !_setup)
    {
        CBM_LogInfo( "oproc_landscape_owriter_nc4_t::refine_geometry_from_setup_(): ",
                "no location information in netCDF output writer");
        return  -1;
    }

    setup::setup_info_t const *  loc = _setup->info();
    geom_shape_t  shp = geom_shape_t( 1);
    shp.vertices[0].x = loc->longitude;
    shp.vertices[0].y = loc->latitude;
    shp.vertices[0].z = loc->elevation;

    int  rc_addshape = this->add_shape_to_gridlayouter( _owriter, &shp);
    if ( rc_addshape)
    {
        return  -1;
    }
    return  1;
}


int
oproc_landscape_owriter_nc4_t::add_shape_to_gridlayouter(
        oproc_owriter_nc4_t *  _owriter,
        geom_shape_t const *  _shape)
{
    crabmeat_assert( _shape);
// sk:dbg    CBM_LogDebug( "received shape with ", _shape->get_number_of_vertices()," vertices");

    nc4_gridlayouter_t::shapeinfo_t  shp_info;
    shp_info.owriter = _owriter;
    shp_info.shape = *_shape;

    omp::set_lock( &this->owriter_nc4_lock);
    this->grid_layouter.parts.push_back( shp_info);
    omp::unset_lock( &this->owriter_nc4_lock);

    return  0;
}

lerr_t
nc4_gridlayouter_t::construct_layout()
{
    this->update_bounding_box( &this->mapinfo.bb);
#ifdef  CRABMEAT_MPI
    lerr_t  rc_comm = this->construct_mpi_communicator();
    if ( rc_comm)
    {
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_bb = this->construct_complete_bounding_box( &this->mapinfo.bb);
    if ( rc_bb)
    {
        return  LDNDC_ERR_FAIL;
    }
#endif

    if ( this->mapinfo.dim.size() == 0)
    {
        lerr_t  rc_gridsize = this->determine_gridsize_from_shapes( &this->mapinfo);
        if ( rc_gridsize)
        {
            return  LDNDC_ERR_FAIL;
        }
    }

    this->mapinfo.extent_x = this->mapinfo.bb.dx() / this->mapinfo.dim.n_x;
    this->mapinfo.extent_y = this->mapinfo.bb.dy() / this->mapinfo.dim.n_y;

    this->mapinfo.parts = this->get_number_of_clients();

    return  LDNDC_ERR_OK;
}

void
nc4_gridlayouter_t::update_bounding_box(
        geom_bounding_box_t *  _bb)
{
    if ( this->parts.size() == 0)
    {
        CBM_LogDebug( "no parts..");
        return;
    }

    parts_container_t::iterator  p = this->parts.begin();
    _bb->init( p->shape);
// sk:dbg    CBM_LogDebug( "updated bb .. ",_bb->to_string());
    ++p;
    for ( ; p != this->parts.end();  ++p)
    {
        _bb->update( p->shape);
    }
}

lerr_t
nc4_gridlayouter_t::determine_gridsize_from_shapes(
        geom_mapinfo_t *  _mapinfo)
{
    geom_coord_t::coord_t const  Dx = _mapinfo->bb.dx();
    geom_coord_t::coord_t const  Dy = _mapinfo->bb.dy();
    geom_coord_t  d_min( Dx, Dy);
    if ( d_min.is_origin())
    {
        CBM_LogError( "nc4_gridlayouter_t::determine_gridsize_from_shapes(): ",
                "bounding box is to small");
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_dmin = this->find_minimum_distance( &d_min, &_mapinfo->bb);
    if ( rc_dmin || ( d_min.x <= 0.0) || ( d_min.y <= 0.0))
    {
        return  LDNDC_ERR_FAIL;
    }

    _mapinfo->dim.n_x = static_cast< int >(( Dx / d_min.x) + 0.5);
    _mapinfo->dim.n_y = static_cast< int >(( Dy / d_min.y) + 0.5);

    return  LDNDC_ERR_OK;
}

lerr_t
nc4_gridlayouter_t::find_minimum_distance(
        geom_coord_t *  _d_min,
        geom_bounding_box_t const *  _mapbb)
{
    /* points inside this range are considered on the same line */
    double const  hx_min = _mapbb->dx() / static_cast< double >( GRID_MAX_X);
    double const  hy_min = _mapbb->dy() / static_cast< double >( GRID_MAX_Y);

    _d_min->x = _mapbb->dx();
    _d_min->y = _mapbb->dy();

    parts_container_t::iterator  p1 = this->parts.begin();
    for ( ; p1 != this->parts.end();  ++p1)
    {
        parts_container_t::iterator  p2 = p1;
        ++p2;
        geom_shape_t const *  shp1 = &p1->shape;
        for ( size_t  v1 = 0;  v1 < shp1->get_number_of_vertices();  ++v1)
        {
            for( ; p2 != this->parts.end();  ++p2)
            {
                geom_shape_t const *  shp2 = &p2->shape;
                for ( size_t  v2 = 0;  v2 < shp2->get_number_of_vertices();  ++v2)
                {
                    geom_coord_t::coord_t const  hx = cbm::abs( shp1->vertices[v1].x - shp2->vertices[v2].x);
                    geom_coord_t::coord_t const  hy = cbm::abs( shp1->vertices[v1].y - shp2->vertices[v2].y);

                    if ( hx < hx_min)
                    {
                        if ( hy < hy_min)
                        {
                            CBM_LogError( "nc4_gridlayouter_t::find_minimum_distance(): ",
                                    "grid resolution too small; definite overlap of shapes");
                            return  LDNDC_ERR_FAIL;
                        }
                    }
                    else if ( hx < _d_min->x)
                    {
                        _d_min->x = hx;
                    }

                    if ( hy < hy_min)
                    {
                    }
                    else if ( hy < _d_min->y)
                    {
                        _d_min->y = hy;
                    }
                }
            }
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
nc4_gridlayouter_t::set_gridpoints_in_writers(
        netcdf_file_t::map_window_t const *  _map_window,
        oproc_owriter_nc4_grid_container_t *  _pushgrids)
{
    parts_container_t::iterator  p = this->parts.begin();
    while ( p != this->parts.end())
    {
        lerr_t  rc_setpoints = p->owriter->set_grid_points(
                &p->shape, p->shape.get_bounding_box(), this->mapinfo.dim, this->mapinfo.bb, _map_window, _pushgrids);
        if ( rc_setpoints == LDNDC_ERR_OK)
        {
            p = this->parts.erase( p);
        }
        else
        {
            ++p;
        }
    }
    if ( this->parts.size() > 0)
    {
        CBM_LogError( "nc4_gridlayouter_t::set_gridpoints_in_writers(): ",
                "not all parts were successfully assigned a subset of the grid  [#fails=",this->parts.size(),"]");
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
nc4_gridlayouter_t::construct_mapwindow(
        netcdf_file_t::map_window_t *  _map_window)
{
    return  grid_decomposition_t::get_map_dimensions_according_to_my_noderank( _map_window, this->n_lat(), this->n_lon());
}

#ifdef  CRABMEAT_MPI
lerr_t
nc4_gridlayouter_t::construct_mpi_communicator()
{
// sk:??    int MPI_Isend(void *buf, int count, MPI_Datatype datatype, int dest,
// sk:??            int tag, MPI_Comm comm, MPI_Request *request)
// sk:??
// sk:??    if ( this->get_number_of_clients() == 0)
// sk:??    {
// sk:??        return  
// sk:??    }
    return  LDNDC_ERR_OK;
}
lerr_t
nc4_gridlayouter_t::construct_complete_bounding_box(
        geom_bounding_box_t *  _bb)
{
    mpi::tiny_comm_t< double, 4 >  mpi_comm;
    if ( mpi_comm.comm_rank() == mpi::MASTER)
    {
// sk:dbg        CBM_LogError( "    r=0        ul_x=",_bb->ul_x,"  ul_y=",_bb->ul_y, "  lr_x=",_bb->lr_x,"  lr_y=",_bb->lr_y);
        for ( int  r = 0;  r < mpi_comm.comm_size();  ++r)
        {
            if ( r == mpi::MASTER)
            {
                continue;
            }
            mpi_comm.recv( r);

            geom_bounding_box_t  bb_r;
            bb_r.ul_x = mpi_comm.buf[0]; bb_r.ul_y = mpi_comm.buf[1]; bb_r.lr_x = mpi_comm.buf[2]; bb_r.lr_y = mpi_comm.buf[3];
            _bb->merge( bb_r);
// sk:dbg            CBM_LogError( "[M] r=", r, "  ul_x=",mpi_comm.buf[0],"  ul_y=",mpi_comm.buf[1], "  lr_x=",mpi_comm.buf[2],"  lr_y=",mpi_comm.buf[3]);
// sk:dbg            CBM_LogError( "[M] merged     ul_x=",_bb->ul_x,"  ul_y=",_bb->ul_y, "  lr_x=",_bb->lr_x,"  lr_y=",_bb->lr_y);
        }

        mpi_comm.buf[0] = _bb->ul_x; mpi_comm.buf[1] = _bb->ul_y; mpi_comm.buf[2] = _bb->lr_x; mpi_comm.buf[3] = _bb->lr_y;
        mpi_comm.bcast( mpi::MASTER);
    }
    else
    {
        mpi_comm.buf[0] = _bb->ul_x; mpi_comm.buf[1] = _bb->ul_y; mpi_comm.buf[2] = _bb->lr_x; mpi_comm.buf[3] = _bb->lr_y;
// sk:dbg        CBM_LogError( "    r=", mpi_comm.comm_rank(), "  ul_x=",mpi_comm.buf[0],"  ul_y=",mpi_comm.buf[1], "  lr_x=",mpi_comm.buf[2],"  lr_y=",mpi_comm.buf[3]);
        mpi_comm.send( mpi::MASTER);

        mpi_comm.bcast( mpi::MASTER);
        _bb->ul_x = mpi_comm.buf[0]; _bb->ul_y = mpi_comm.buf[1]; _bb->lr_x = mpi_comm.buf[2]; _bb->lr_y = mpi_comm.buf[3];
// sk:dbg        CBM_LogError( "[S] new        ul_x=",_bb->ul_x,"  ul_y=",_bb->ul_y, "  lr_x=",_bb->lr_x,"  lr_y=",_bb->lr_y);
    }

    /* wait until all nodes have received the complete bounding box */
    mpi::barrier( mpi::comm_ldndc);

    return  LDNDC_ERR_OK;
}
#endif

}

