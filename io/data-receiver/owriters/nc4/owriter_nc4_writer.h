/*!
 * @brief
 *     netCDF4 output writer: block writer
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_NC4_WRITER_H_
#define  LDNDC_IO_OUTPUTWRITER_NC4_WRITER_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_file.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_datastore.h"

#include  <list>

namespace ldndc {

class  oproc_owriter_nc4_t;
struct  CBM_API  oproc_owriter_nc4_grid_t
{
    oproc_owriter_nc4_grid_t();
    ~oproc_owriter_nc4_grid_t();
    oproc_owriter_nc4_grid_t(
            oproc_owriter_nc4_grid_t const &);
    oproc_owriter_nc4_grid_t &  operator=(
            oproc_owriter_nc4_grid_t const &);

    int  copy_from( oproc_owriter_nc4_grid_t const &);

    int  allocate( size_t);
    int  reallocate( size_t, size_t * = NULL);
    void  deallocate();

    size_t *  I /*sort to reflect order in memory?*/;
    size_t  n_I;
    size_t  n_I_push;

    int  rank;
    oproc_owriter_nc4_t *  owriter;
};
typedef  std::list< oproc_owriter_nc4_grid_t >  oproc_owriter_nc4_grid_container_t;


class  CBM_API  oproc_owriter_nc4_base_t  :  public  oproc_owriter_base_t
{
    protected:
        oproc_owriter_nc4_base_t();
        virtual  ~oproc_owriter_nc4_base_t() = 0;
};

class  CBM_API  oproc_owriter_nc4_t  :  public  oproc_owriter_nc4_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("nc4")
    LDNDC_SERVER_OBJECT(oproc_owriter_nc4_t)
    public:
        lreceiver_descriptor_t  descriptor() const { return  this->rcv_d_; }
        void  set_descriptor(
                lreceiver_descriptor_t const &  _rcv_d)
        {
            this->rcv_d_ = _rcv_d;
        }
        lerr_t  status() const { return  ( this->state.ok) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

    public:
        lerr_t  set_attributes(
                sink_attributes_t const *);

        /* only used for fixed layouts */
        lerr_t  set_layout_owriter(
                sink_entity_layout_t const *);
        lerr_t  set_grid_points(
                geom_shape_t const *, geom_bounding_box_t const & /*submap bounding box*/,
                geom_dim_t const &  /*grid dimension*/, geom_bounding_box_t const & /*map bounding box*/,
                netcdf_file_t::map_window_t const *,
                oproc_owriter_nc4_grid_container_t *);


        virtual  lerr_t  write_record(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);
        sink_layout_t const *  layout(
                sink_layout_hint_t const *) const { return  this->fmt ? &this->fmt->layout : NULL; }
        sink_entities_t const *  entities(
                sink_layout_hint_t const *) const { return  this->fmt ? &this->fmt->ents : NULL; }

    public:
        lerr_t  discard_buffer_content() { return  LDNDC_ERR_OK; }
        size_t  buffer_size() const { return  0; }
        size_t  buffer_size_limit() const { return  static_cast< size_t >( 0); }

        void  initialize_storagebroker(
                netcdf_mapsbroker_t *);
        size_t  get_current_stacksize() const { return  this->storagebrk.stacksize(); }

    private:
        lerr_t  write_record_(
                sink_record_srv_t const * /*record*/,
                sink_layout_t const * /*use this layout*/);
        lerr_t  write_zero_record_(
                sink_layout_t const * /*use this layout*/);

        struct  set_gridpoints_info_t
        {
            netcdf_file_t::map_window_t const *  map_window;

            double  rx, ry;

            int  dx, dy;
            int  x0, y0;
            int  xn, yn;
        };
        lerr_t  set_grid_points_from_shape(
                geom_shape_t const *, set_gridpoints_info_t const *,
                oproc_owriter_nc4_grid_container_t *);
        size_t  make_grid(
                oproc_owriter_nc4_grid_t *,
                geom_shape_t const *, set_gridpoints_info_t const *);
        lerr_t  append_pushgrids(
                oproc_owriter_nc4_grid_container_t *,
                oproc_owriter_nc4_grid_t const *, set_gridpoints_info_t const *);
        lerr_t  crop_my_grid(
                oproc_owriter_nc4_grid_t *, size_t /*remaining points*/);

        int  get_noderank_that_contains_gridpoint(
                int /*point (latitude)*/, int /*point (longitude)*/,
                set_gridpoints_info_t const *);

#ifdef  CRABMEAT_MPI
        typedef std::map< int, size_t >  pushnodes_count_t;
        struct  shadowwriter_info_t
        {
            int  rank;
            int  tag;
        };
        std::vector< shadowwriter_info_t >  shadowwriters_info;

        lerr_t  allocate_and_set_pushgrids(
                oproc_owriter_nc4_grid_container_t *, oproc_owriter_nc4_grid_t const *, set_gridpoints_info_t const *);
        lerr_t  allocate_pushgrids(
                oproc_owriter_nc4_grid_container_t *, pushnodes_count_t const *);
        lerr_t  set_pushgrids(
                oproc_owriter_nc4_grid_container_t *,
                oproc_owriter_nc4_grid_t const *, set_gridpoints_info_t const *);
        lerr_t  move_pushgrids(
                oproc_owriter_nc4_grid_container_t *, oproc_owriter_nc4_grid_container_t *);

    public:
        void  mpi_add_shadowwriter(
                int /*targetrank*/, int /*targettag*/);
        lerr_t  mpi_initialize_communicator(
                sink_layout_t const *);
        lerr_t  mpi_append_record_to_message(
                sink_record_srv_t const *);
        lerr_t  mpi_send_records(
                int /*timestep*/);

    protected:
        mpi::packed_comm_t  pcomm;
    private:
#endif  /*  CRABMEAT_MPI  */

    protected:
        sink_entity_layout_t const *  fmt;
        sink_attributes_t const *  attrs;

        lreceiver_descriptor_t  rcv_d_;
        netcdf_storagebroker_t  storagebrk;
        oproc_owriter_nc4_grid_t  grid;

    public:
        oproc_owriter_nc4_t();
        virtual  ~oproc_owriter_nc4_t();

    private:
        template < typename  _LT >
        int  write_record_to_cache(
                int *  _var_nb, sink_record_sequence_t const *  _rec_seq,
                void const *  _data, int  _size)
        {
            if ( !_data)
            {
                *_var_nb += _size;
                /* nothing to write :-) */
                return  0;
            }

            _LT const *  d = (_LT const *)_data;
            for ( int  j = 0;  j < _size;  ++j, ++d)
            {
                if ( !this->storagebrk.var_exists( *_var_nb))
                {
                    ++*_var_nb;

                    /* legally missing :-) */
                    continue;
                }

                typename nc_atomic_t< _LT >::type *  buf = 
                    this->storagebrk.get_storage< _LT >( (*_var_nb)++, _rec_seq->timestep, _rec_seq->records_in_timestep);
                if ( !buf)
                {
                    return  -1;
                }

                *buf = *d;
            }

            /* all written :-) */
            return  0;
        }
    public:
        template < typename  _LT >
        int  flush(
                typename nc_atomic_t< _LT >::type *  _map, int  _map_size,
                int  _timestep, int  _var_nb)
        {
            if ( !_map || !this->grid.I)
            {
                CBM_LogError( "oproc_owriter_nc4_t::flush(): ",
                        "no map or no grid  [map=",(void *)_map,",grid=",(void *)this->grid.I,"]");
                /* nothing to write to :-( */
                return  -1;
            }

            crabmeat_assert( sizeof(_LT) == sizeof(typename nc_atomic_t< _LT >::type));

            for ( size_t  m = 0;  m < this->storagebrk.stacksize();  ++m, _map += _map_size)
            {
                typename nc_atomic_t< _LT >::type const *  val_ptr = 
                    this->storagebrk.get_storage< _LT >( _var_nb, _timestep, m, netcdf_storagebroker_t::NO_RESIZE);
/* sk:we have cleared it before..
                typename nc_atomic_t< _LT >::type const  val = ( val_ptr) ? *val_ptr : invalid_t< typename nc_atomic_t< _LT >::type >::value;
*/
                if ( !val_ptr)
                {
                    continue;
                }

                for ( size_t  g = 0;  g < this->grid.n_I;  ++g)
                {
                    _map[this->grid.I[g]] = *val_ptr;
                }
            }
            /* all written :-) */
            return  0;
        }
    private:
};

}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_NC4_WRITER_H_  */

