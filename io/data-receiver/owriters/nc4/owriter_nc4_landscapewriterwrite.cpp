/*!
 * @brief
 *    netCDF4 output writer: data submission  (implementation)
 *
 * @author
 *    steffen klatt (created on: feb 04, 2014)
 */

#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriter.h"
#include  "log/cbm_baselog.h"

namespace ldndc {

lerr_t
oproc_landscape_owriter_nc4_t::flush_owriter(
        oproc_owriter_base_t *)
{
    this->netcdf.flush();
    return  LDNDC_ERR_OK;
}    


lerr_t
oproc_landscape_owriter_nc4_t::dispatch_write(
        oproc_receiver_t *  _owriter,
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    oproc_owriter_nc4_t *  nc_writer = static_cast< oproc_owriter_nc4_t * >( _owriter);
    if ( !nc_writer)
    {
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_write = nc_writer->write_record( _client, _record);
    RETURN_IF_NOT_OK(rc_write);
#ifdef  CRABMEAT_MPI
// sk:dbg    CBM_LogDebug( "packing record t=",_record->seq->timestep, "  r=",_record->seq->records_in_timestep);
    lerr_t  rc_append = nc_writer->mpi_append_record_to_message( _record);
    RETURN_IF_NOT_OK(rc_append);
#endif

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_landscape_owriter_nc4_t::flush_flushable_maps(
        int  _timestep)
{
    int const  n_vars = static_cast< int >( this->mapsbrk.get_number_of_entities());
#ifdef  CRABMEAT_MPI
    double const  t_flush_start = MPI_Wtime();

    lerr_t  rc_send = this->mpi_send_to_shadowwriters( _timestep);
    lerr_t  rc_recv = this->mpi_recv_from_writers( _timestep);
    if ( rc_send || rc_recv)
    {
        return  LDNDC_ERR_FAIL;
    }
#endif
    for ( int  var_nb = 0;  var_nb < n_vars;  ++var_nb)
    {
        nc_type  var_type = this->mapsbrk.get_var_nctype( var_nb);
        if ( var_type == ncNoType)
        {
            continue;
        }

        lerr_t  rc_collect = this->collect_from_writers_and_flush_to_file(
                var_type, var_nb, _timestep);
        if ( rc_collect)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
#ifdef  CRABMEAT_MPI
    CBM_LogInfo( "elapsed time during flushing maps: ",MPI_Wtime()-t_flush_start);
#endif

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_landscape_owriter_nc4_t::collect_from_writers_and_flush_to_file(
        nc_type  _var_nctype, int  _var_nb, int  _timestep)
{
    if ( _var_nctype == ncNoType)
    {
        CBM_LogError( "nc4_file_t::collect_from_writers_and_flush_to_file(): unvalid data type encountered");
        return  LDNDC_ERR_FAIL;
    }

// sk:dbg    CBM_LogDebug( "writing variable \"",this->fmt.ents.ids[_var_nb], "\""); 
    lerr_t  rc_write = LDNDC_ERR_OK;
    if ( _var_nctype == nc_atomic_t< ldndc_flt64_t >::nc_type_id)
    {
        rc_write = this->write_map_to_ncfile< ldndc_flt64_t >( _var_nb, _timestep);
    }
    else if ( _var_nctype == nc_atomic_t< ldndc_int32_t >::nc_type_id)
    {
        rc_write = this->write_map_to_ncfile< ldndc_int32_t >( _var_nb, _timestep);
    }
    else
    {
        CBM_LogWarn( "unsupported data type");
    }

    return  rc_write;
}


int
oproc_landscape_owriter_nc4_t::prepare_writer_for_action()
{
    if ( !this->netcdf.is_structured() && this->grid_layouter.has_clients())
    {
        size_t  max_ssz = this->get_max_stacksize_from_writer_caches();
        if ( max_ssz == 0)
        {
            CBM_LogError( "oproc_landscape_owriter_nc4_t::prepare_writer_for_action(): ",
                    "z dimension has size 0; we handle this as an error. sorry.");
            return  -1;
        }
        netcdf_file_t::map_window_t  map_window;
        lerr_t  rc_grid = this->construct_grid_layout( &map_window);
        if ( rc_grid)
        {
            return  -1;
        }
        lerr_t  rc_brk = this->set_mapsbroker_structure( max_ssz, &map_window);
        if ( rc_brk)
        {
            return  -1;
        }
        lerr_t  rc_file = this->set_file_structure(
                &this->grid_layouter.mapinfo, max_ssz, &map_window);
        if ( rc_file)
        {
            return  -1;
        }
        lerr_t  rc_writers = this->construct_writer_maps( &map_window);
        if ( rc_writers)
        {
            return  -1;
        }
        crabmeat_assert( !this->grid_layouter.has_clients());

    }
    else if ( this->grid_layouter.has_clients())
    {
        CBM_LogWarn( "oproc_landscape_owriter_nc4_t::validate_and_set_file_structure(): ",
                "clients joined after defining netCDF file structure: discarding their data.");
    }

    return  0;
}
lerr_t
oproc_landscape_owriter_nc4_t::construct_grid_layout(
        netcdf_file_t::map_window_t *  _map_window)
{
    CBM_LogDebug( "constructing grid layout..");
    lerr_t  rc_gridlayout = this->grid_layouter.construct_layout();
    if ( rc_gridlayout)
    {
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_mapwindow = this->grid_layouter.construct_mapwindow( _map_window);
    if ( rc_mapwindow)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_nc4_t::set_mapsbroker_structure(
        size_t  _max_stacksize, netcdf_file_t::map_window_t const *  _map_window)
{
    this->mapsbrk.set_maps_dimensions( _max_stacksize, _map_window);

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_nc4_t::set_file_structure(
        geom_mapinfo_t const *  _map_info,
        size_t  _max_stacksize, netcdf_file_t::map_window_t const *  _map_window)
{
// sk:dbg    CBM_LogDebug( "oproc_landscape_owriter_nc4_t::set_file_structure(): ",
// sk:dbg            "setting mapinfo layout..  ",
// sk:dbg            "[dimension=",_map_info->dim.n_y,"x",_map_info->dim.n_x,"x",_max_stacksize,
// sk:dbg            ",window=",_map_window->count_lat,"x",_map_window->count_lon,"+",_map_window->offs_lat,"+",_map_window->offs_lon,"]");

    lerr_t  rc_structure = this->netcdf.set_structure(
            &this->fmt, _map_info, _max_stacksize);
    if ( rc_structure)
    {
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_mapwindow = this->netcdf.define_mapwindow( _map_window);
    if ( rc_mapwindow)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_landscape_owriter_nc4_t::construct_writer_maps(
        netcdf_file_t::map_window_t const *  _map_window)
{
#ifdef  CRABMEAT_MPI
    oproc_owriter_nc4_grid_container_t  pushgrids;
    oproc_owriter_nc4_grid_container_t *  pushgrids_ptr = &pushgrids;
#else
    oproc_owriter_nc4_grid_container_t *  pushgrids_ptr = NULL;
#endif
    lerr_t  rc_gridpoints = this->grid_layouter.set_gridpoints_in_writers( _map_window, pushgrids_ptr);
    if ( rc_gridpoints)
    {
        return  LDNDC_ERR_FAIL;
    }
#ifdef  CRABMEAT_MPI
    lerr_t  rc_shad = this->mpi_construct_shadowwriters_from_pushgrids( pushgrids_ptr);
    if ( rc_shad)
    {
        CBM_LogError( "oproc_landscape_owriter_nc4_t::construct_writer_maps(): ",
                "failed to create shadow writers");
        return  LDNDC_ERR_FAIL;
    }
#endif

    return  LDNDC_ERR_OK;
}
size_t
oproc_landscape_owriter_nc4_t::get_max_stacksize_from_writer_caches()
const
{
    size_t  max_stacksize = 0;
    for ( size_t  j = 0;  j < this->owriters_.size();  ++j)
    {
        oproc_owriter_nc4_t const *  w = this->owriters_[j];
        if ( w)
        {
            max_stacksize =
                std::max( max_stacksize, w->get_current_stacksize());
        }
    }
    return  max_stacksize;
}

int
oproc_landscape_owriter_nc4_t::collective_commit_commence(
        lclock_t const *)
{
    return  0;
}


int
oproc_landscape_owriter_nc4_t::collective_commit_complete(
        lclock_t const *  _lclock)
{
    int  rc_prepare = this->prepare_writer_for_action();
    if ( rc_prepare)
    {
        return  -1;
    }

    CBM_LogDebug( "landscape_owriter_nc4_t::collective_commit_complete(): ",
            "flushing for t=",_lclock->time_step());
    lerr_t  rc = this->flush_flushable_maps( _lclock->time_step());
    return  rc ? -1 : 0;
}

}

