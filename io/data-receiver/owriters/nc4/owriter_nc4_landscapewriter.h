/*!
 * @brief
 *     netCDF4 output writer: landscape writer
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_NC4_LANDSCAPEWRITER_H_
#define  LDNDC_IO_OUTPUTWRITER_NC4_LANDSCAPEWRITER_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/receiver-container.h"
#include  "io/sink-info.h"

#include  "io/data-receiver/owriters/nc4/owriter_nc4_file.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_datastore.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_writer.h"

#include  "geom/geom.h"

#include  "openmp/lomp.h"
#include  <list>

namespace ldndc {

namespace  geometry{ class  input_class_geometry_srv_t; }
namespace  setup{ class  input_class_setup_srv_t; }

struct CBM_API nc4_gridlayouter_t
{
    static int const  GRID_MAX_X = 1000;
    static int const  GRID_MAX_Y = 1000;
    struct  shapeinfo_t
    {
        oproc_owriter_nc4_t *  owriter;
        geom_shape_t  shape;
    };
    typedef  std::list< shapeinfo_t >  parts_container_t;
    parts_container_t  parts;

    geom_mapinfo_t  mapinfo;
    ldndc_ncsize_t  n_lat() const { return  static_cast< ldndc_ncsize_t >( this->mapinfo.dim.n_y); }
    ldndc_ncsize_t  n_lon() const { return  static_cast< ldndc_ncsize_t >( this->mapinfo.dim.n_x); }

    size_t  get_number_of_clients() const { return  this->parts.size(); }
    bool  has_clients() const { return  this->get_number_of_clients() > 0; }

    lerr_t  construct_layout();
    lerr_t  construct_mapwindow(
            netcdf_file_t::map_window_t *);

    void  update_bounding_box(
            geom_bounding_box_t *);
    lerr_t  determine_gridsize_from_shapes(
            geom_mapinfo_t *);
    lerr_t  find_minimum_distance(
            geom_coord_t *,
            geom_bounding_box_t const *);
    lerr_t  set_gridpoints_in_writers(
            netcdf_file_t::map_window_t const *,
            oproc_owriter_nc4_grid_container_t *);
#ifdef  CRABMEAT_MPI
    lerr_t  construct_mpi_communicator();
    lerr_t  construct_complete_bounding_box(
            geom_bounding_box_t *);
#endif
};

class  CBM_API  oproc_landscape_owriter_nc4_base_t  :  public  oproc_landscape_owriter_base_t
{
    protected:
        oproc_landscape_owriter_nc4_base_t();
        virtual  ~oproc_landscape_owriter_nc4_base_t() = 0;
};
}

#ifdef  CRABMEAT_MPI
#  include  "containers/lbitset.h"
#  include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewritermpi.h"
#endif

namespace ldndc {
class  CBM_API  oproc_landscape_owriter_nc4_t  :  public  oproc_landscape_owriter_nc4_base_t
{
    __LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON(IO_FORMAT_NC4)
    LDNDC_SERVER_OBJECT(oproc_landscape_owriter_nc4_t)

    public:
        typedef  oproc_owriter_nc4_t  receiver_t;

    public:
        lerr_t  open(
                sink_info_t const *);
        lerr_t  reopen();

        lerr_t  close();

        lerr_t  flush_owriter(
                oproc_owriter_base_t *);
        void  mark_as_dirty() { }

        lerr_t  status() const { return  this->state.ok ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

        oproc_owriter_base_t *  acquire_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);
        void  release_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);

    public:
        lerr_t  set_layout_owriter(
                oproc_owriter_base_t * /*target block*/,
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*layout&entities*/);
    public:
        int  collective_commit_commence(
                lclock_t const *);
        int  collective_commit_complete(
                lclock_t const *);

    public:
        lerr_t  dispatch_write(
                oproc_receiver_t *,
                sink_client_t const *,
                sink_record_srv_t const *);

    private:
        lerr_t  flush_flushable_maps(
                int /*timestep*/);
    private:
        int  prepare_writer_for_action();
        lerr_t  construct_grid_layout(
                netcdf_file_t::map_window_t *);
        lerr_t  set_mapsbroker_structure(
                size_t /*maps stack size*/,
                netcdf_file_t::map_window_t const *);
        lerr_t  set_file_structure(
                geom_mapinfo_t const *, size_t /*maps stack size*/,
                netcdf_file_t::map_window_t const *);
        lerr_t  construct_writer_maps(
                netcdf_file_t::map_window_t const *);
#ifdef  CRABMEAT_MPI
        lerr_t  mpi_construct_shadowwriters_from_pushgrids(
                oproc_owriter_nc4_grid_container_t const *);
        int  mpi_find_most_pullnodes(
                int /*pull nodes max count*/);
        int  mpi_find_communication_endpoints(
                int * /*pull nodes*/, int /*pull nodes max count*/,
                bitset const *);
        lerr_t  mpi_establish_communication_endpoints(
                int * /*pull nodes*/, int /*pull nodes count*/,
                int * /*push nodes*/, int /*push nodes count*/,
                oproc_owriter_nc4_grid_container_t const *);

        int  mpi_flag_contained_nodes(
                bitset *,
                oproc_owriter_nc4_grid_container_t const *);
        void  mpi_set_pushnode_ranks(
                int * /*push nodes*/, int /*push nodes count*/,
                bitset const *);
        int  mpi_count_pushrank_occurences(
                int /*rank*/,
                oproc_owriter_nc4_grid_container_t const *);
        int  mpi_set_number_of_points_per_grid_for_rank(
                int *, int, oproc_owriter_nc4_grid_container_t const *);
        int  mpi_set_points_and_tag_for_rank(
                int *, int, oproc_owriter_nc4_grid_container_t const *);
        lerr_t  mpi_construct_shadowwriters_for_rank(
                int /*source rank*/,
                int * /*grid points*/, int * /*grid sizes*/,  int /*number of grids*/);
        oproc_owriter_shadow_nc4_t *  mpi_acquire_oshadowwriter(
                lreceiver_descriptor_t const &);

        lerr_t  mpi_send_to_shadowwriters(
                int /*timestep*/);
        lerr_t  mpi_recv_from_writers(
                int /*timestep*/);

        oproc_receiver_container_t< oproc_landscape_owriter_shadow_nc4_t >  oshadowwriters_;
#endif 
        size_t  get_max_stacksize_from_writer_caches() const;

    protected:
        sink_entity_layout_t  fmt;
        sink_info_t  sink_info;

    private:
        netcdf_file_t  netcdf;
        netcdf_mapsbroker_t  mapsbrk;

        int  is_layouted_;

    private:
        omp::omp_lock_t  owriter_nc4_lock;

    public:
        oproc_landscape_owriter_nc4_t();
        virtual  ~oproc_landscape_owriter_nc4_t();

        bool  flag_required_inputs(
                input_class_set_t *) const;
        lerr_t  pull_required_inputs(
                input_class_set_t *,
                oproc_receiver_t *);

        struct  release_op_t
        {
            void  operator()(
                    oproc_owriter_nc4_t *) const;
        };
    private:
        oproc_receiver_container_t< oproc_landscape_owriter_nc4_t >  owriters_;

    private:
        int  refine_geometry_from_geometry_(
                oproc_owriter_nc4_t *,
                geometry::input_class_geometry_srv_t const *);
        int  set_mapinfo_from_geometry_(
                geometry::input_class_geometry_srv_t const *);
        int  refine_geometry_from_setup_(
                oproc_owriter_nc4_t *,
                setup::input_class_setup_srv_t const *);
        int  update_mapinfo_from_point_(
                geom_coord_t const *);
        int  add_new_point_to_map_(
                geom_coord_t const *);

        nc4_gridlayouter_t  grid_layouter;
        int  add_shape_to_gridlayouter(
                oproc_owriter_nc4_t *,
                geom_shape_t const *);

    private:
        lerr_t  collect_from_writers_and_flush_to_file(
                nc_type /*variable type*/, int  /*variable number*/,
                int /*timestep*/);
#define  ldndc_nc4_cachesize  1
        template < typename  _LT >
        lerr_t  write_map_to_ncfile(
                int  _var_nb, int  _dim_t)
        {
            int const  m_buf_size = static_cast< int >( this->mapsbrk.map_size());
            typename nc_atomic_t< _LT >::type *  m_buf =
                this->mapsbrk.get_cleared_map_storage< _LT >( _dim_t, ldndc_nc4_cachesize);

            this->flush_writers_data_to_map< oproc_landscape_owriter_nc4_t, _LT >(
                    this->owriters_, m_buf, m_buf_size, _dim_t, _var_nb);
#ifdef  CRABMEAT_MPI
            this->flush_writers_data_to_map< oproc_landscape_owriter_shadow_nc4_t, _LT >(
                    this->oshadowwriters_, m_buf, m_buf_size, _dim_t, _var_nb);
#endif
            if (( _dim_t % ldndc_nc4_cachesize) == 0)
            {
                return  this->nc_put_rec< _LT >( m_buf, _dim_t, _var_nb);
            }
            return  LDNDC_ERR_OK;
        }

        template < typename  _OW, typename  _LT >
        void  flush_writers_data_to_map(
                oproc_receiver_container_t< _OW > &  _owriters,
                typename nc_atomic_t< _LT >::type *  _m_buf, int  _map_size,
                int  _dim_t, int  _var_nb)
        {
            for ( size_t  j = 0;  j < _owriters.size();  ++j)
            {
                typename _OW::receiver_t *  w = _owriters[j];
                if ( w)
                {
                    w->template flush< _LT >( _m_buf, _map_size, _dim_t, _var_nb);
                }
            }
        }

        template < typename  _LT >
        lerr_t  nc_put_rec(
                typename nc_atomic_t< _LT >::type *  _map,
                int  _dim_t, int  _var_nb)
        {
            if ( !_map)
            {
                return  LDNDC_ERR_FAIL;
            }
            return  this->netcdf.put_map( _map, _dim_t, ldndc_nc4_cachesize, _var_nb);
        }

};

}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_NC4_LANDSCAPEWRITER_H_  */

