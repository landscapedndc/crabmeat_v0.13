/*!
 * @brief
 *     netCDF4 output writer
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_NC4_H_
#define  LDNDC_IO_OUTPUTWRITER_NC4_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/receiver-container.h"
#include  "io/sink-info.h"

#include  "io/data-receiver/owriters/nc4/owriter_nc4_writer.h"
#include  "io/data-receiver/owriters/nc4/owriter_nc4_landscapewriter.h"

#include  "geom/geom.h"

#include  "openmp/lomp.h"

#endif  /*  !LDNDC_IO_OUTPUTWRITER_NC4_H_  */

