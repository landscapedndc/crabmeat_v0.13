/*!
 * @brief
 *     netCDF4 output writer: landscape writer MPI specific
 *     declarations
 *
 * @author
 *    steffen klatt (created on: mar 10, 2014)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_NC4_LANDSCAPEWRITERMPI_H_
#define  LDNDC_IO_OUTPUTWRITER_NC4_LANDSCAPEWRITERMPI_H_

namespace ldndc {

class  CBM_API  oproc_owriter_shadow_nc4_t  :  public  oproc_owriter_nc4_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("nc4")
    LDNDC_SERVER_OBJECT(oproc_owriter_shadow_nc4_t)
    int  ssz;
    public:
        void  set_source_rank(
                int /*source rank*/);
        int  set_grid_points(
                int const * /*points*/, int /*number of points*/);

        lerr_t  mpi_receive_records(
                int /*timestep*/);
        lerr_t  set_layout_owriter(
                sink_entity_layout_t const *, int /*stacksize*/);
    public:
        oproc_owriter_shadow_nc4_t();
        virtual  ~oproc_owriter_shadow_nc4_t();
};

class  CBM_API  oproc_landscape_owriter_shadow_nc4_t  :  public  oproc_landscape_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("nc4")
    LDNDC_SERVER_OBJECT(oproc_landscape_owriter_shadow_nc4_t)

    public:
        typedef  oproc_owriter_shadow_nc4_t  receiver_t;

        struct  release_op_t
        {
            void  operator()(
                    oproc_owriter_shadow_nc4_t *) const
            {
            }
        };

        oproc_landscape_owriter_shadow_nc4_t();
        virtual  ~oproc_landscape_owriter_shadow_nc4_t();
};
}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_NC4_LANDSCAPEWRITERMPI_H_  */

