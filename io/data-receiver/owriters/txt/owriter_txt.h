/*!
 * @brief
 *    text output writer
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013)
 */

#ifndef  LDNDC_IO_OUTPUTWRITER_TXT_H_
#define  LDNDC_IO_OUTPUTWRITER_TXT_H_

#include  "io/data-receiver/owriters/owriter.h"
#include  "io/data-receiver/receiver-container.h"
#include  "io/sink-info.h"

#include  "time/cbm_time.h"
#include  "openmp/cbm_omp.h"

#include  <ostream>
#include  <fstream>
#include  <sstream>

#ifndef  LDNDC_OWRITER_TXT_FLUSH_SIZE
#  define  LDNDC_OWRITER_TXT_FLUSH_SIZE  (32768)
#endif


namespace ldndc {

class  CBM_API  oproc_owriter_txt_base_t  :  public  oproc_owriter_base_t
{
    public:
        static ldndc_string_t const  no_entname_name;

    protected:
        oproc_owriter_txt_base_t();
        virtual  ~oproc_owriter_txt_base_t() = 0;
};

class  CBM_API  oproc_owriter_txt_t  :  public  oproc_owriter_txt_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("txt")
    LDNDC_SERVER_OBJECT(oproc_owriter_txt_t)
    public:
        lreceiver_descriptor_t  descriptor() const { return  this->rcv_d_; }
        void  set_descriptor(
                lreceiver_descriptor_t const &  _rcv_d)
        {
            this->rcv_d_ = _rcv_d;
        }
        lerr_t  status() const { return  ( this->state.ok) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

    public:
        lerr_t  set_attributes(
                sink_attributes_t const *);

        /* only used for fixed layouts */
        lerr_t  set_layout_owriter(
                sink_entity_layout_t const *);
        sink_layout_t const *  layout(
                sink_layout_hint_t const *) const { return  &this->fmt.layout; }
        sink_entities_t const *  entities(
                sink_layout_hint_t const *) const { return  &this->fmt.ents; }

    public:
        lerr_t  write_header(
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entities_t const * /*header entities*/);
        lerr_t  write_record(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t const * /*record*/);

        /* key/value pair list version */
        lerr_t  write_key_value(
                sink_client_t const *  /*client identifier*/,
                sink_entities_t const * /*keys*/,
                sink_record_srv_t const * /*values (with layout)*/,
                ldndc_string_t const * = NULL /*key prefix*/);
        /* key/value pair version */
        lerr_t  write_key_value(
                sink_client_t const *  /*client identifier*/,
                ldndc_string_t const * /*key*/,
                ldndc_string_t const * /*value*/,
                ldndc_string_t const * = NULL /*key prefix*/);

        lerr_t  write_tag(
                ldndc_string_t const * /*tag name*/);
        lerr_t  write_tag_c(
                char const * /*tag name*/);
        lerr_t  write_prefixed_tag(
                ldndc_string_t const * /*prefix*/, ldndc_string_t const * /*tag name*/);
        lerr_t  write_prefixed_tag_c(
                char const * /*prefix*/, char const * /*tag name*/);

        size_t  flush(
                std::ostream * /*flush target*/,
                size_t /*flush only if buffer size exceeds this number of bytes*/);

        lerr_t  discard_buffer_content();
        size_t  buffer_size() const;
        size_t  buffer_size_limit() const
        {
            if ( this->attrs)
            {
                return  static_cast< size_t >( this->attrs->cache_size);
            }
            return  static_cast< size_t >( LDNDC_OWRITER_TXT_FLUSH_SIZE);
        }

    private:
        lerr_t  write_header_meta_(
                sink_record_meta_srv_t const * /*record meta info*/);
        lerr_t  write_header_entities_(
                sink_entities_t const * /*header entities*/);
        lerr_t  write_record_meta_(
                sink_client_t const * /*client identifier*/,
                sink_record_meta_srv_t const * /*record meta info*/);
        lerr_t  write_record_(
                sink_record_srv_t const * /*record*/,
                sink_layout_t const * /*use this layout*/);
        lerr_t  write_zero_record_(
                sink_layout_t const * /*use this layout*/);


    protected:
        sink_entity_layout_t  fmt;
        sink_attributes_t const *  attrs;
    private:
        lreceiver_descriptor_t  rcv_d_;
        std::stringstream  data_cache;

    public:
        oproc_owriter_txt_t();
        virtual  ~oproc_owriter_txt_t();
};


class  CBM_API  oproc_landscape_owriter_txt_base_t  :  public  oproc_landscape_owriter_base_t
{
    protected:
        oproc_landscape_owriter_txt_base_t();
        virtual  ~oproc_landscape_owriter_txt_base_t() = 0;
};


class  CBM_API  oproc_landscape_owriter_txt_t  :  public  oproc_landscape_owriter_txt_base_t
{
    LDNDC_OUTPUTWRITER_INTERFACE_DECL_COMMON("txt")
    LDNDC_SERVER_OBJECT(oproc_landscape_owriter_txt_t)

    public:
        typedef  oproc_owriter_txt_t  receiver_t;

    public:
        lerr_t  open(
                sink_info_t const *);
        lerr_t  reopen();

        lerr_t  close();

        lerr_t  flush_owriter(
                oproc_owriter_base_t *);
        void  mark_as_dirty() { this->is_clean_ = 0; }

        lerr_t  status() const { return  this->state.ok ? LDNDC_ERR_OK : LDNDC_ERR_FAIL; }

        oproc_owriter_base_t *  acquire_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);
        void  release_owriter(
                lreceiver_descriptor_t const & /*writer descriptor*/);

    public:
        lerr_t  set_layout_owriter(
                oproc_owriter_base_t * /*target block*/,
                sink_record_meta_srv_t const * /*record meta info*/,
                sink_entity_layout_t const * /*layout&entities*/);

    public:
        int  collective_commit_commence(
                cbm::sclock_t const *);
        int  collective_commit_complete(
                cbm::sclock_t const *);

    private:
        lerr_t  flush_all(
                bool = true /*force flush*/);

    protected:
        sink_entity_layout_t  fmt;
        sink_info_t  sink_info;

    private:
        std::filebuf  file_cache;
        std::ostream  file_stream;

        int  is_layouted_;
        int  is_open_;
        int  is_clean_;

    private:
        cbm::omp::omp_lock_t  owriter_txt_lock;

    public:
        oproc_landscape_owriter_txt_t();
        virtual  ~oproc_landscape_owriter_txt_t();

        struct  release_op_t
        {
            void  operator()(
                    oproc_owriter_txt_t *) const;

            std::ostream *  tgt;
            int  uniform;
        };
    private:
        oproc_receiver_container_t< oproc_landscape_owriter_txt_t >  owriters_;
};

}

#endif  /*  !LDNDC_IO_OUTPUTWRITER_TXT_H_  */

