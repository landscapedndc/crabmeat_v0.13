/*!
 * @brief
 *    text output writer  (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013)
 */

#include  "io/data-receiver/owriters/txt/owriter_txt.h"
#include  "io/data-receiver/owriters/owriter-layout.h"
#include  "io/default-streams.h"

#include  "string/lstring-basic.h"
#include  "io/marshal/marshal.h"

#include  "log/cbm_baselog.h"

#include  <iostream>

namespace ldndc {

ldndc_string_t const  oproc_owriter_txt_base_t::no_entname_name = "*";
oproc_owriter_txt_base_t::oproc_owriter_txt_base_t()
        : oproc_owriter_base_t()
{
}


oproc_owriter_txt_base_t::~oproc_owriter_txt_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_owriter_txt_t)
oproc_owriter_txt_t::oproc_owriter_txt_t()
        : oproc_owriter_txt_base_t(),
          fmt( sink_entity_layout_defaults),
          attrs( NULL),
          rcv_d_( invalid_t< lreceiver_descriptor_t >::value)
{
}
oproc_owriter_txt_t::~oproc_owriter_txt_t()
{
    crabmeat_assert( this->attrs);
    if ( !this->attrs->uniform)
    {
        oproc_owriter_layouthelper_t::free_entity_layout( &this->fmt);
    }
}


lerr_t
oproc_owriter_txt_t::set_attributes(
        sink_attributes_t const *  _attrs)
{
    this->attrs = _attrs;
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_txt_t::set_layout_owriter(
        sink_entity_layout_t const *  _layout)
{
    if ( !this->attrs)
    {
        CBM_LogDebug( "oproc_owriter_txt_t::set_layout_owriter(): attributes not set");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( this->attrs->uniform)
    {
        this->fmt = ( _layout) ? *_layout : sink_entity_layout_defaults;
        return  LDNDC_ERR_OK;
    }

    if ( this->fmt.ents.ids || this->fmt.layout.types)
    {
        CBM_LogDebug( "oproc_owriter_txt_t::set_layout_owriter(): redefining layout not supported");
        /* already done (error?, redefine?) */
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if ( !_layout || _layout->layout.rank == 0 || !_layout->layout.types || !_layout->layout.sizes)
    {
        CBM_LogDebug( "oproc_owriter_txt_t::set_layout_owriter(): missing or incorrect layout information");
        if ( _layout)
        {
            CBM_LogDebug( "oproc_owriter_txt_t::set_layout_owriter(): rank=",(int)_layout->layout.rank, "  types=",(void*)_layout->layout.types, "  sizes=",(void*)_layout->layout.sizes);
        }
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    lerr_t  rc_setlayout = oproc_owriter_layouthelper_t::set_layout( &this->fmt, _layout);
    RETURN_IF_NOT_OK(rc_setlayout);

    return  LDNDC_ERR_OK;
}


size_t
oproc_owriter_txt_t::buffer_size()
const
{
    return  static_cast< size_t >( const_cast< oproc_owriter_txt_t * >( this)->data_cache.tellp());
}

size_t
oproc_owriter_txt_t::flush(
        std::ostream *  _buf,
        size_t  _flush_size)
{
    crabmeat_assert( _buf);
    size_t  bs = 0;
    if ( _flush_size < this->buffer_size())
    {
        *_buf << this->data_cache.str();

        bs = this->buffer_size();
        this->discard_buffer_content();
    }
    return  bs;
}


lerr_t
oproc_owriter_txt_t::discard_buffer_content()
{
    this->data_cache.str( "");
    return  LDNDC_ERR_OK;
}


lerr_t
oproc_owriter_txt_t::write_header(
        sink_record_meta_srv_t const *  _record_meta,
        sink_entities_t const *  _ents)
{
    if ( _record_meta)
    {
        this->write_header_meta_( _record_meta);
    }
    if ( _ents)
    {
        if ( _record_meta && _record_meta->useregs > 0)
        {
            this->data_cache << this->attrs->delimiter;
        }
        this->write_header_entities_( _ents);
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_txt_t::write_header_meta_(
        sink_record_meta_srv_t const *  _record_meta)
{
    crabmeat_assert( _record_meta);

    char const  _D[2] = { this->attrs->delimiter, '\0'};
    char const *  D = ldndc_empty_cstring;

    if ( _record_meta->useregs & io::CLIENT_SOURCE)
    {
        this->data_cache << D << io::record_meta_ids[io::_CLIENT_SOURCE];
        D = _D;
    }
    if ( _record_meta->useregs & io::CLIENT_ID)
    {
        this->data_cache << D << io::record_meta_ids[io::_CLIENT_ID];
        D = _D;
    }
    if ( _record_meta->useregs & io::CLIENT_X)
    {
        this->data_cache << D << io::record_meta_ids[io::_CLIENT_X];
        D = _D;
    }
    if ( _record_meta->useregs & io::CLIENT_Y)
    {
        this->data_cache << D << io::record_meta_ids[io::_CLIENT_Y];
        D = _D;
    }
    if ( _record_meta->useregs & io::CLIENT_Z)
    {
        this->data_cache << D << io::record_meta_ids[io::_CLIENT_Z];
        D = _D;
    }
    if ( _record_meta->useregs & io::CLIENT_AREA)
    {
        this->data_cache << D << io::record_meta_ids[io::_CLIENT_AREA];
        D = _D;
    }
    if ( _record_meta->useregs & io::DATETIME)
    {
        this->data_cache << D << io::record_meta_ids[io::_DATETIME];
        D = _D;
    }
    if ( _record_meta->useregs & io::YEAR)
    {
        this->data_cache << D << io::record_meta_ids[io::_YEAR];
        D = _D;
    }
    if ( _record_meta->useregs & io::MONTH)
    {
        this->data_cache << D << io::record_meta_ids[io::_MONTH];
        D = _D;
    }
    if ( _record_meta->useregs & io::DAY)
    {
        this->data_cache << D << io::record_meta_ids[io::_DAY];
        D = _D;
    }
    if ( _record_meta->useregs & io::HOUR)
    {
        this->data_cache << D << io::record_meta_ids[io::_HOUR];
        D = _D;
    }
    if ( _record_meta->useregs & io::MINUTE)
    {
        this->data_cache << D << io::record_meta_ids[io::_MINUTE];
        D = _D;
    }
    if ( _record_meta->useregs & io::SECOND)
    {
        this->data_cache << D << io::record_meta_ids[io::_SECOND];
        D = _D;
    }
    if ( _record_meta->useregs & io::JULIANDAY)
    {
        this->data_cache << D << io::record_meta_ids[io::_JULIANDAY];
        D = _D;
    }
    if ( _record_meta->useregs & io::SUBDAY)
    {
        this->data_cache << D << io::record_meta_ids[io::_SUBDAY];
        D = _D;
    }
    if ( _record_meta->useregs & io::SEQNB)
    {
        this->data_cache << D << io::record_meta_ids[io::_SEQNB];
        D = _D;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_txt_t::write_header_entities_(
        sink_entities_t const *  _ents)
{
    crabmeat_assert( _ents);
    for ( ldndc_output_size_t  k = 0;  k < _ents->size;  ++k)
    {
        this->data_cache << ( cbm::is_not_empty( _ents->names[k]) ? _ents->names[k] : oproc_owriter_txt_base_t::no_entname_name) << (( k == (_ents->size-1)) ? '\n' : this->attrs->delimiter);
    }

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_txt_t::write_record(
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( _record);
    sink_layout_t const *  s_layout = ( _record->layout.rank == 0) ? &this->fmt.layout : &_record->layout;

    if ( _record->meta.useregs > 0)
    {
        this->write_record_meta_( _client, &_record->meta);
    }

    if ( _record->gap == ldndc::SINKGAP_NONE)
    {
        if ( _record->meta.useregs > 0)
        {
            this->data_cache << this->attrs->delimiter;
        }
        lerr_t  rc_write = this->write_record_( _record, s_layout);
        RETURN_IF_NOT_OK(rc_write);
        this->data_cache << ldndc_txt_newline;

        return  LDNDC_ERR_OK;
    }
    else if ( _record->gap == ldndc::SINKGAP_ZERO)
    {
        if ( _record->meta.useregs > 0)
        {
            this->data_cache << this->attrs->delimiter;
        }
        lerr_t  rc_writezero = this->write_zero_record_( s_layout);
        RETURN_IF_NOT_OK(rc_writezero);
        this->data_cache << ldndc_txt_newline;

        return  LDNDC_ERR_OK;
    }

    /* do not know how to handle request */
    return  LDNDC_ERR_REQUEST_MISMATCH;

}

lerr_t
oproc_owriter_txt_t::write_record_meta_(
        sink_client_t const *  _client,
        sink_record_meta_srv_t const *  _record_meta)
{
    char const  _D[2] = { this->attrs->delimiter, '\0'};
    char const *  D = ldndc_empty_cstring;
    if ( _record_meta && _client)
    {
        if ( _record_meta->useregs & io::CLIENT_SOURCE)
        {
            this->data_cache << D << _client->target_sid;
            D = _D;
        }
        if ( _record_meta->useregs & io::CLIENT_ID)
        {
            this->data_cache << D << _client->target_id;
            D = _D;
        }
        if ( _record_meta->useregs & io::CLIENT_X)
        {
            this->data_cache << D << _client->x;
            D = _D;
        }
        if ( _record_meta->useregs & io::CLIENT_Y)
        {
            this->data_cache << D << _client->y;
            D = _D;
        }
        if ( _record_meta->useregs & io::CLIENT_Z)
        {
            this->data_cache << D << _client->z;
            D = _D;
        }
        if ( _record_meta->useregs & io::CLIENT_AREA)
        {
            this->data_cache << D << _client->area;
            D = _D;
        }
    }
    if ( _record_meta)
    {
        sink_record_meta_t  m;
        m.timestamp_from_scalar( _record_meta->ts);

        if ( _record_meta->useregs & io::DATETIME)
        {
            char  isodate[CBM_ISO8601_MAXLEN];
            cbm::tm_t  t;
            t.tm_year = m.t.reg.year;
            t.tm_month = m.t.reg.month-1;
            t.tm_day = m.t.reg.day-1;
            t.tm_hour = m.t.reg.hour;
            t.tm_min = m.t.reg.minute;
            t.tm_sec = m.t.reg.second;
            int const  isodate_sz = cbm::to_iso8601(
                        t, isodate, CBM_ISO8601_MAXLEN);
            if ( isodate_sz >= CBM_ISO8601_MAXLEN)
                { CBM_LogWarn( "[BUG?]  datetime was truncated",
                    "  [datetime=",isodate,"]"); }
            this->data_cache << D << isodate;
            D = _D;
        }
        if ( _record_meta->useregs & io::YEAR)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.year);
            D = _D;
        }
        if ( _record_meta->useregs & io::MONTH)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.month);
            D = _D;
        }
        if ( _record_meta->useregs & io::DAY)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.day);
            D = _D;
        }
        if ( _record_meta->useregs & io::HOUR)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.hour);
            D = _D;
        }
        if ( _record_meta->useregs & io::MINUTE)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.minute);
            D = _D;
        }
        if ( _record_meta->useregs & io::SECOND)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.second);
            D = _D;
        }
        if ( _record_meta->useregs & io::JULIANDAY)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.julianday);
            D = _D;
        }
        if ( _record_meta->useregs & io::SUBDAY)
        {
            this->data_cache << D << static_cast< ldndc_uint32_t >( m.t.reg.subday);
            D = _D;
        }
        if ( _record_meta->useregs & io::SEQNB)
        {
            this->data_cache << D << static_cast< ldndc_int32_t >( _record_meta->record_seqnb);
            D = _D;
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_txt_t::write_record_(
        sink_record_srv_t const *  _record,
        sink_layout_t const *  _layout)
{
    if ( !_record->data)
    {
        return  LDNDC_ERR_OK;
    }

    int const  r_end = static_cast< int >( _layout->rank);
    int const  R = r_end - 1;
    int  j = 0;
    char  D = this->attrs->delimiter;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = _layout->types[r];
        int const  this_size = _layout->sizes[r];

        if ( this_datatype == LDNDC_FLOAT64)
        {
            ldndc_flt64_t const *  d = (ldndc_flt64_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_UINT32)
        {
            ldndc_uint32_t const *  d = (ldndc_uint32_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_INT32)
        {
            ldndc_int32_t const *  d = (ldndc_int32_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_FLOAT32)
        {
            ldndc_flt32_t const *  d = (ldndc_flt32_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_STRING)
        {
            ldndc_string_t const *  d = (ldndc_string_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_UINT64)
        {
            ldndc_uint64_t const *  d = (ldndc_uint64_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_INT64)
        {
            ldndc_int64_t const *  d = (ldndc_int64_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_BOOL)
        {
            ldndc_bool_t const *  d = (ldndc_bool_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << (( *d) ? "1" : "0");
            }
        }
        else if ( this_datatype == LDNDC_RAW)
        {
            ldndc_raw_t const *  d = (ldndc_raw_t const *)_record->data[r];
            this->data_cache  << *d; ++d;
            for ( j = 1;  j < this_size;  ++j, ++d)
            {
                this->data_cache << D << *d;
            }
        }
        else if ( this_datatype == LDNDC_COMPOUND)
        {
            marshal_compound_t const *  m_compound = (marshal_compound_t const *)_record->data[r];
            for ( j = 0;  j < this_size;  ++j, ++m_compound)
            {
                if ( !m_compound)
                {
                    continue;
                }
                sink_record_srv_t  nested_rec;
                nested_rec.layout = m_compound->mi->layout;
                nested_rec.ents = m_compound->mi->ents;
                nested_rec.data = m_compound->data;

                this->write_record_( &nested_rec, &m_compound->mi->layout);
            }
        }
        else if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
        }
        else
        {
            CBM_LogError( "[ERROR]  oproc_owriter_txt_t::write_record(): ",
                    "datatype not supported  [datatype=",LDNDC_ATOMIC_DATATYPE_NAMES[this_datatype],"]");
            this->data_cache << D << "[error:incomplete record]" << '\n';
            return  LDNDC_ERR_FAIL;
        }

        if ( r < R)
        {
            this->data_cache << D;
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_txt_t::write_zero_record_(
        sink_layout_t const *  _layout)
{
    int const  r_end = static_cast< int >( _layout->rank);
    int const  R = r_end-1;
    int  j = 0;
    char  D = this->attrs->delimiter;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = _layout->types[r];
        int const  this_size = _layout->sizes[r];

        if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
        }
        else if ( this_datatype == LDNDC_STRING)
        {
            this->data_cache << invalid_string;
            for ( j = 1;  j < this_size;  ++j)
            {
                this->data_cache << D << invalid_string;
            }
        }
        else if ( this_datatype == LDNDC_COMPOUND)
        {
            /* silently ignoring */
            /* TODO */
        }
        else
        {
            this->data_cache << "0";
            for ( j = 1;  j < this_size;  ++j)
            {
                this->data_cache << D << "0";
            }
        }
        if ( r < R)
        {
            this->data_cache << D;
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
oproc_owriter_txt_t::write_key_value(
        sink_client_t const *  _client,
        sink_entities_t const *  _key,
        sink_record_srv_t const *  _value,
        ldndc_string_t const *  _key_prefix)
{
    crabmeat_assert( _key && _value);
    if ( !_key->ids || !_value->data)
    {
        return  LDNDC_ERR_OK;
    }

    sink_layout_t const *  s_layout = ( _value->layout.rank == 0) ? &this->fmt.layout : &_value->layout;
    if ( !s_layout)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    char const  D = ldndc_txt_space;
    char const  PFX = ldndc_txt_tab;
    char const  EQ = ldndc_txt_assign;
    char const  NL = ldndc_txt_newline;
    static char const *  EMPTY = "";
    char const *  KPFX = ( _key_prefix) ? (char const *)_key_prefix : EMPTY;

    int const  r_end = static_cast< int >( s_layout->rank);
    int  j = 0;
    int  n = 0;
    for ( int  r = 0;  r < r_end;  ++r)
    {
        atomic_datatype_t const  this_datatype = s_layout->types[r];
        int const  this_size = s_layout->sizes[r];

        if ( this_datatype == LDNDC_FLOAT64)
        {
            ldndc_flt64_t const *  d = (ldndc_flt64_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << *d << NL;
            }
        }
        else if ( this_datatype == LDNDC_UINT32)
        {
            ldndc_uint32_t const *  d = (ldndc_uint32_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << *d << NL;
            }
        }
        else if ( this_datatype == LDNDC_INT32)
        {
            ldndc_int32_t const *  d = (ldndc_int32_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << *d << NL;
            }
        }
        else if ( this_datatype == LDNDC_FLOAT32)
        {
            ldndc_flt32_t const *  d = (ldndc_flt32_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << *d << NL;
            }
        }
        else if ( this_datatype == LDNDC_STRING)
        {
            ldndc_string_t const *  d = (ldndc_string_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << '"' << *d << '"' << NL;
            }
        }
        else if ( this_datatype == LDNDC_UINT64)
        {
            ldndc_uint64_t const *  d = (ldndc_uint64_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << *d << NL;
            }
        }
        else if ( this_datatype == LDNDC_INT64)
        {
            ldndc_int64_t const *  d = (ldndc_int64_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << *d << NL;
            }
        }
        else if ( this_datatype == LDNDC_BOOL)
        {
            ldndc_bool_t const *  d = (ldndc_bool_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++d)
            {
                this->data_cache << PFX << KPFX << _key->ids[n++] << D << EQ << D << '"' << (( *d) ? "yes" : "no") << '"' << NL;
            }
        }
        else if ( this_datatype == LDNDC_COMPOUND)
        {
            marshal_compound_t const *  m_compound = (marshal_compound_t const *)_value->data[r];
            for ( j = 0;  j < this_size;  ++j, ++m_compound, ++n)
            {
                if ( !m_compound)
                {
                    continue;
                }

                sink_record_srv_t  nested_rec( sink_record_srv_defaults);
                nested_rec.layout = m_compound->mi->layout;
                nested_rec.data = m_compound->data;

                ldndc_string_t  prefix;
                cbm::as_strcpy( prefix, m_compound->name);
                size_t const  L = cbm::strlen( m_compound->name);
                crabmeat_assert( L < CBM_STRING_SIZE);
                prefix[L] = '@';
                prefix[L+1] = '\0';
// sk:dbg                CBM_LogDebug( "prefix=",prefix);

                this->write_key_value( _client, &m_compound->mi->ents, &nested_rec, &prefix);
            }
        }
        else if ( this_datatype == LDNDC_DTYPE_NONE)
        {
            /* silently ignoring */
            n += this_size;
        }
        else
        {
            CBM_LogError( "[ERROR]  oproc_owriter_txt_t::write_key_value(): datatype not supported  [datatype=",this_datatype,"]");
            this->data_cache << D << "[error:incomplete record]" << '\n';
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_txt_t::write_key_value(
        sink_client_t const * /*client*/,
        ldndc_string_t const *  _key,
        ldndc_string_t const *  _value,
        ldndc_string_t const *  _key_prefix)
{
    crabmeat_assert( _key && _value);

    this->data_cache << ldndc_txt_tab << (( _key_prefix) ? (char const *)_key_prefix : "") << *_key << ldndc_txt_space << ldndc_txt_assign << ldndc_txt_space << ldndc_txt_quote_open << *_value << ldndc_txt_quote_close << ldndc_txt_newline;
    return  LDNDC_ERR_OK;
}

lerr_t
oproc_owriter_txt_t::write_tag(
        ldndc_string_t const *  _tag_name)
{
    crabmeat_assert( _tag_name);

    this->data_cache << ldndc_txt_tag << *_tag_name << ldndc_txt_newline;
    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_txt_t::write_tag_c(
        char const *  _tag_name)
{
    crabmeat_assert( _tag_name);

    this->data_cache << ldndc_txt_tag << _tag_name << ldndc_txt_newline;
    return  LDNDC_ERR_OK;
}
lerr_t
oproc_owriter_txt_t::write_prefixed_tag_c(
        char const *  _prefix, char const *  _tag_name)
{
    this->data_cache << ldndc_txt_tag << _prefix << '@' << _tag_name << ldndc_txt_newline;
    return  LDNDC_ERR_OK;
}

oproc_landscape_owriter_txt_base_t::oproc_landscape_owriter_txt_base_t()
        : oproc_landscape_owriter_base_t()
{
}


oproc_landscape_owriter_txt_base_t::~oproc_landscape_owriter_txt_base_t()
{
}


LDNDC_SERVER_OBJECT_DEFN(oproc_landscape_owriter_txt_t)
oproc_landscape_owriter_txt_t::oproc_landscape_owriter_txt_t()
        : oproc_landscape_owriter_txt_base_t(),
          fmt( sink_entity_layout_defaults),
          file_stream( NULL),

          is_layouted_( 0),
          is_open_( 0),
          is_clean_( 0)
{
    this->file_stream.clear();
    cbm::omp::init_lock( &this->owriter_txt_lock);
}
oproc_landscape_owriter_txt_t::~oproc_landscape_owriter_txt_t()
{
    (void)this->close();
    this->release_owriter( invalid_t< lreceiver_descriptor_t >::value /*==all*/);

    if ( this->sink_info.attrs.uniform)
    {
        oproc_owriter_layouthelper_t::free_entity_layout( &this->fmt);
    }

    cbm::omp::destroy_lock( &this->owriter_txt_lock);
}

lerr_t
oproc_landscape_owriter_txt_t::open(
        sink_info_t const *  _sink_info)
{
    crabmeat_assert( _sink_info);
    if ( this->is_open_)
    {
        /* attempt to open sink again? close first. */
        return  LDNDC_ERR_OK;
    }

    this->is_clean_ = 1;

    this->sink_info = *_sink_info;

    if ( this->sink_info.attrs.cache_size < 0)
    {
        if ( this->sink_info.attrs.cache_size != -1)
        {
            CBM_LogWarn( "invalid cache size; falling back to internal default",
                    "  [cachesize=",LDNDC_OWRITER_TXT_FLUSH_SIZE,"]");
        }
        this->sink_info.attrs.cache_size = LDNDC_OWRITER_TXT_FLUSH_SIZE;
    }

    if ( ldndc::sink_is_black_hole( _sink_info))
    {
        CBM_LogFatal( "[BUG]  oproc_landscape_owriter_txt_t::open():",
               " i think we inofficially agreed on not to instantiate receiver for black holes (..well, ok, i was the only voter)");
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_rdbuf = standard_sink_buffer( &this->file_stream, _sink_info->name.c_str());
    if ( rc_rdbuf == LDNDC_ERR_OK)
    {
        this->is_open_ = 1;
        CBM_LogDebug( "attaching sink to default stream  [sink=",_sink_info->identifier,",stream=",_sink_info->name,"]");
        /* no op */
    }
    else if ( rc_rdbuf == LDNDC_ERR_OBJECT_DOES_NOT_EXIST)
    {
        std::string  sink_name = _sink_info->full_name();
        this->file_cache.open( sink_name.c_str(), std::ios::out|std::ios::trunc);
        if ( !this->file_cache.is_open())
        {
            CBM_LogError( "oproc_landscape_owriter_txt_t::open(): output sink (file) could not be opened  [sink=",_sink_info->identifier,",file=",_sink_info->full_name(),"]");
/* FIXME  rename error */    return  LDNDC_ERR_READER_OPEN_FAILED;
        }
        this->file_stream.rdbuf( &this->file_cache);
    }
    else
    {
        return  rc_rdbuf;
    }

    if ( this->file_stream.good() && !this->file_stream.bad())
    {
        CBM_LogDebug( "successfully opened sink  [sink=",_sink_info->identifier,",file=",_sink_info->full_name(),"]");
        this->is_open_ = 1;
        return  LDNDC_ERR_OK;
    }

    CBM_LogDebug( "oproc_landscape_owriter_txt_t::open(): failed to open sink  [sink=",_sink_info->identifier,",file=",_sink_info->full_name(),"]");
        return  LDNDC_ERR_FAIL;
}
lerr_t
oproc_landscape_owriter_txt_t::reopen()
{
    if ( this->is_open_)
    {
        if ( this->sink_info.attrs.overwrite == 0)
        {
            CBM_LogError( "oproc_landscape_owriter_txt_t::reopen(): ",
                    "attempting to reopen an overwrite protected sink  [sink=",this->sink_info.identifier,"]");
            return  LDNDC_ERR_INVALID_OPERATION;
        }
        this->close();
    }
    return  this->open( &this->sink_info);
}


lerr_t
oproc_landscape_owriter_txt_t::close()
{
        if ( this->is_open_)
    {
        this->flush_all();
        this->file_stream.flush();

        if ( this->file_cache.is_open())
        {
            this->file_cache.close();
        }
        this->is_open_ = 0;
        this->is_clean_ = 1;
// sk:dbg        CBM_LogDebug( "oproc_landscape_owriter_txt_t::close(): successfully closed sink  ",
// sk:dbg                "[sink=",this->sink_info.identifier,",file=",this->sink_info.full_name(),"]");
        }

    return  LDNDC_ERR_OK;
}


lerr_t
oproc_landscape_owriter_txt_t::flush_owriter(
        oproc_owriter_base_t *  _owriter)
{
    if ( this->is_open_)
    {
        if ( _owriter)
        {
            if ( this->sink_info.attrs.ordered)
            {
                /*only flush before closing*/
                return  LDNDC_ERR_OK;
            }

            oproc_owriter_txt_t *  this_owriter = static_cast< oproc_owriter_txt_t * >( _owriter);

            this->owriters_.lock();
            this_owriter->flush( &this->file_stream, this_owriter->buffer_size_limit());
            this->owriters_.unlock();
        }
        else
        {
            return  this->flush_all();
        }
    }
    return  LDNDC_ERR_OK;
}    

lerr_t
oproc_landscape_owriter_txt_t::flush_all(
        bool  _force)
{
    if ( !_force && this->sink_info.attrs.ordered)
    {
        /*only flush before closing*/
        return  LDNDC_ERR_OK;
    }

    this->owriters_.lock();
    if ( this->is_clean_)
    {
        this->owriters_.unlock();
        return  LDNDC_ERR_OK;
    }

    size_t const  n_owriters = this->owriters_.size();
    size_t  cached_datasize = 0;
    for ( size_t  j = 0;  j < n_owriters;  ++j)
    {
        oproc_owriter_txt_t *  w = this->owriters_[j];
        if ( w)
        {
            cached_datasize +=
                w->flush( &this->file_stream, ( _force ? 0 : w->buffer_size_limit()));
        }
    }
    if ( cached_datasize > ( n_owriters * this->sink_info.attrs.cache_size))
    {
        this->file_stream.flush();
    }

    this->is_clean_ = 1;
    this->owriters_.unlock();

    return  LDNDC_ERR_OK;
}


oproc_owriter_base_t *
oproc_landscape_owriter_txt_t::acquire_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    oproc_owriter_base_t *  new_owriter =
        this->owriters_.acquire_receiver( _owriter_descriptor);
    if ( !new_owriter)
    {
        return  NULL;
    }

    lerr_t  rc_set_attrs =
        static_cast< oproc_owriter_txt_t * >( new_owriter)->set_attributes( &this->sink_info.attrs);
    if ( rc_set_attrs)
    {
        return  NULL;
    }

    return  new_owriter;
}
void
oproc_landscape_owriter_txt_t::release_owriter(
        lreceiver_descriptor_t const &  _owriter_descriptor)
{
    if (( _owriter_descriptor != invalid_t< lreceiver_descriptor_t >::value) && this->sink_info.attrs.ordered)
    {
        /* only collectively delete and flush blocks */
        return;
    }

    release_op_t  r_op;
    r_op.tgt = &this->file_stream;
    r_op.uniform = this->sink_info.attrs.uniform;

    this->owriters_.release_receiver( _owriter_descriptor, &r_op);
}
void
oproc_landscape_owriter_txt_t::release_op_t::operator()(
        oproc_owriter_txt_t *  _w)
const
{
    _w->flush( this->tgt, 0);
    if ( this->uniform)
    {
        _w->set_layout_owriter( NULL);
    }
}


lerr_t
oproc_landscape_owriter_txt_t::set_layout_owriter(
        oproc_owriter_base_t *  _target,
        sink_record_meta_srv_t const *  _record_meta,
        sink_entity_layout_t const *  _layout)
{
    crabmeat_assert( _target);
    oproc_owriter_txt_t *  this_target = static_cast< oproc_owriter_txt_t *>( _target);

#ifdef  CRABMEAT_SANITY_CHECKS
    if ( !this->is_open_)
    {
        CBM_LogError( "[BUG]  oproc_landscape_owriter_txt_t::set_layout_owriter(): attempt to define entities for closed stream");
        return  LDNDC_ERR_FAIL;
    }
#endif  /*  CRABMEAT_SANITY_CHECKS  */

    lerr_t  rc_defent = LDNDC_ERR_OK;
    if ( this->sink_info.attrs.uniform)
    {
        cbm::omp::set_lock( &this->owriter_txt_lock, "layout");
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush (is_layouted_)
#endif
        if ( !this->is_layouted_ && _layout)
        {
            rc_defent = oproc_owriter_layouthelper_t::set_layout( &this->fmt, _layout);
            if ( _layout->layout.entsizes)
            {
                CBM_LogError( "oproc_landscape_owriter_txt_t::set_layout_owriter(): ",
                        "no support for vector-valued entities");
                rc_defent = LDNDC_ERR_INVALID_OPERATION;
            }
        }
        if ( rc_defent == LDNDC_ERR_OK)
        {
            rc_defent = this_target->set_layout_owriter( &this->fmt);
        }
        if ( !this->sink_info.attrs.noheader)
        {
            if ( this->sink_info.attrs.ordered)
            {
                /* everyone writes its header if required */
                this_target->write_header( _record_meta, &_layout->ents);
            }
            else if ( !this->is_layouted_)
            {
                /* the first one writes the header if required */
                this_target->write_header( _record_meta, &_layout->ents);
                this_target->flush( &this->file_stream, 0);
            }
        }
        if ( rc_defent == LDNDC_ERR_OK)
        {
            this->is_layouted_ = 1;
            this->mark_as_dirty();
        }
        else
        {
            CBM_LogError( "oproc_landscape_owriter_txt_t::set_layout_owriter(): setting layout failed");
        }
#ifdef  CRABMEAT_OPENMP
#  pragma omp flush(is_layouted_)
#endif
        cbm::omp::unset_lock( &this->owriter_txt_lock, "layout");
    }
    else
    {
// sk:dbg        CBM_LogDebug( "defining non-uniform sink layout");
        rc_defent = this_target->set_layout_owriter( _layout);
        if (( rc_defent == LDNDC_ERR_OK) && ( !this->sink_info.attrs.noheader))
        {
            this_target->write_header( _record_meta, &_layout->ents);
        }
    }

    return  rc_defent;
}

int
oproc_landscape_owriter_txt_t::collective_commit_commence(
        cbm::sclock_t const *)
{
    return  0;
}
int
oproc_landscape_owriter_txt_t::collective_commit_complete(
        cbm::sclock_t const *)
{
    lerr_t  rc_flush = this->flush_all( false);
    if ( rc_flush)
    {
        return  -1;
    }

    return  0;
}


}

