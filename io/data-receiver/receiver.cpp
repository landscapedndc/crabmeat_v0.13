/*!
 * @brief
 *    data receiver base (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 19, 2013)
 */

#include  "io/data-receiver/receiver.h"

ldndc::oproc_receiver_t::oproc_receiver_t()
        : oproc_base_t()
{
}
ldndc::oproc_receiver_t::~oproc_receiver_t()
{
}

ldndc::oproc_landscape_receiver_t::oproc_landscape_receiver_t()
        : oproc_landscape_base_t()
{
}
ldndc::oproc_landscape_receiver_t::~oproc_landscape_receiver_t()
{
}


lerr_t
ldndc::oproc_landscape_receiver_t::dispatch_write(
        oproc_receiver_t *  _receiver,
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    this->mark_as_dirty();
    return  _receiver->write_record( _client, _record);
}


