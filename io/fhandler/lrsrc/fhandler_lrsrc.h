/*!
 * @brief
 *    Lrsources i/o format handler
 *
 * @author
 *    steffen klatt (created on: mar 16, 2014)
 */

#ifndef  LDNDC_IO_FHANDLER_LRESOURCES_H_
#define  LDNDC_IO_FHANDLER_LRESOURCES_H_

#include  "io/fhandler/fhandler.h"
#include  "io/source-info.h"

#include  "resources/Lresources.h"

namespace ldndc {

class  CBM_API  iobject_handler_lrsrc_base_t
    : public  iobject_handler_base_t
{
    public:
        static char const *  format()
            { return  "lrsrc"; }
        char const *  io_format_type() const
            { return this->format(); }
    protected:
        iobject_handler_lrsrc_base_t();
        virtual ~iobject_handler_lrsrc_base_t() = 0;

};


class  CBM_API  iobject_handler_lrsrc_t
    :  public  iobject_handler_lrsrc_base_t
{
    LDNDC_SERVER_OBJECT(iobject_handler_lrsrc_t)
    public:
        static char const *  name()
            { return  "Lresources format handler"; }

    public:
        iobject_handler_lrsrc_t();
        virtual  ~iobject_handler_lrsrc_t();

        lerr_t  open(
                source_info_t const *);

        lerr_t  load();
        lerr_t  reload();

        lerr_t  close();

    public:
        source_info_t const *  info() const
            { return  this->m_resources ? &this->source_info : NULL; }

    private:
        /* pointer to Lrsources object (not owner) */
        cbm::Lresources_t const *  m_resources;
};
}

#endif  /*  !LDNDC_IO_FHANDLER_LRESOURCES_H_  */

