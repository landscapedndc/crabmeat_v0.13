/*!
 * @brief
 *    Lresources i/o format handler (implementation)
 *
 * @author
 *    steffen klatt (created on: mar 16, 2014)
 */

#include  "io/fhandler/lrsrc/fhandler_lrsrc.h"
#include  "log/cbm_baselog.h"

namespace ldndc {

/***  INPUT HANDLER LRESOURCES BASE  ***/
iobject_handler_lrsrc_base_t::iobject_handler_lrsrc_base_t()
        : iobject_handler_base_t()
{
}
iobject_handler_lrsrc_base_t::~iobject_handler_lrsrc_base_t()
{
}


/***  INPUT HANDLER LRESOURCES  ***/
LDNDC_SERVER_OBJECT_DEFN(ldndc::iobject_handler_lrsrc_t)
iobject_handler_lrsrc_t::iobject_handler_lrsrc_t()
        : iobject_handler_lrsrc_base_t(),
          m_resources( NULL)
{
}


iobject_handler_lrsrc_t::~iobject_handler_lrsrc_t()
{
    (void)this->close();
}


lerr_t
iobject_handler_lrsrc_t::open(
        source_info_t const *  _source_info)
{
    if ( this->m_resources)
    {
        return  LDNDC_ERR_OK;
    }

    if ( _source_info)
    {
        /* (re)set */
        this->source_info = *_source_info;
    }

    this->m_resources = &CBM_DefaultResources;
    if ( !this->m_resources)
    {
        CBM_LogFatal( "iobject_handler_lrsrc_t::open(): ",
                    "reference to Lresources broken  [requesting-source=",this->source_info.full_name(),"]");
        return  LDNDC_ERR_READER_OPEN_FAILED;
    }
    CBM_LogDebug( "iobject_handler_lrsrc_t::open(): ",
                "set reference to Lresources  [requesting-source=",this->source_info.full_name(),"]");
    return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_lrsrc_t::load()
{
        return  LDNDC_ERR_OK;
}    
lerr_t
iobject_handler_lrsrc_t::reload()
{
    return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_lrsrc_t::close()
{
    this->m_resources = NULL;
    return  LDNDC_ERR_OK;
}

}

