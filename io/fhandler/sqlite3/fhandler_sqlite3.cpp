/*!
 * @brief
 *    sqlite3 i/o format handler (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 18, 2014)
 */

#include  "io/fhandler/sqlite3/fhandler_sqlite3.h"
#include  "io/data-source/data-source.h"
#include  "log/cbm_baselog.h"

#include  "utils/lutils-fs.h"

#include  <limits>

namespace ldndc {

/***  INPUT HANDLER SQLITE3 BASE  ***/
iobject_handler_sqlite3_base_t::iobject_handler_sqlite3_base_t()
        : iobject_handler_base_t()
{
}
iobject_handler_sqlite3_base_t::~iobject_handler_sqlite3_base_t()
{
}


/***  INPUT HANDLER SQLITE3  ***/
LDNDC_SERVER_OBJECT_DEFN(ldndc::iobject_handler_sqlite3_t)
iobject_handler_sqlite3_t::iobject_handler_sqlite3_t()
                : iobject_handler_sqlite3_base_t()
{
    this->handle.db = NULL;
}


iobject_handler_sqlite3_t::~iobject_handler_sqlite3_t()
{
    (void)this->close();
}


lerr_t
iobject_handler_sqlite3_t::open(
        source_info_t const *  _source_info)
{
    if ( this->handle.db)
    {
        //CBM_LogWarn( "attempting to open data source twice!? (source=" + this->source_info.full_name() + ")");
        return  LDNDC_ERR_OK;
    }

    if ( _source_info)
    {
        /* (re)set */
        this->source_info = *_source_info;
    }

    if ( !cbm::is_file( this->source_info.full_name().c_str()) && this->source_info.is_mutable())
    {
        CBM_LogDebug( "postponing open... (handle=",(void*)this->handle.db,")");
        return  LDNDC_ERR_OK;
    }


    CBM_LogDebug( "iobject_handler_sqlite3_t::open(): ",
            "opened source  [source=",this->source_info.identifier,",dev=",this->source_info.full_name(),"]");

// sk:deprecated     sqlite3_enable_shared_cache( 0);
// sk:deprecated // sk:TODO    if ( _source_info->pragma( "shared"))
// sk:deprecated     {
// sk:deprecated         sqlite3_enable_shared_cache( 1);
// sk:deprecated     }

    int  rc_open = sqlite3_open_v2( this->source_info.full_name().c_str(), &this->handle.db,
            SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE|SQLITE_OPEN_PRIVATECACHE|SQLITE_OPEN_NOMUTEX, NULL);
    if ( rc_open)
    {
        CBM_LogError( "iobject_handler_sqlite3_t::open(): ",
                "failed to open database  [error=",sqlite3_errmsg(this->handle.db),"]");
        return  LDNDC_ERR_READER_OPEN_FAILED;
    }
    int  rc_async = sqlite3_exec( this->handle.db, "PRAGMA synchronous = off", NULL, NULL, NULL);
    int  rc_journalmode = sqlite3_exec( this->handle.db, "PRAGMA journal_mode = off", NULL, NULL, NULL);
    int  rc_queryonly = sqlite3_exec( this->handle.db, "PRAGMA query_only = on", NULL, NULL, NULL);
// sk:TODO    if ( _source_info->pragma( "shared"))
    {
        /*int  rc_readuncommited =*/ sqlite3_exec( this->handle.db, "PRAGMA read_uncommitted = on", NULL, NULL, NULL);
    }
    if ( rc_async || rc_journalmode || rc_queryonly)
    {
        CBM_LogError( "iobject_handler_sqlite3_t::open(): ",
                "failed to set pragma 'synchronous = OFF' or 'journal_mode = OFF' or 'query_only = 1'");
        return  LDNDC_ERR_READER_OPEN_FAILED;
    }
    CBM_LogDebug( "successfully opened database");

    if ( !this->handle.db)
    {
        CBM_LogError( "iobject_handler_sqlite3_t::open(): ",
                    "opening data source failed  [source=",this->source_info.full_name(),"]");
        return  LDNDC_ERR_READER_OPEN_FAILED;
    }
    this->handle.tbl = this->source_info.identifier;

    CBM_LogDebug( "iobject_handler_sqlite3_t::open(): ",
                "opened data source object  [source=",this->source_info.full_name(),"]");

    return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_sqlite3_t::load()
{
    if ( this->handle.db || this->source_info.is_mutable())
    {
        return  LDNDC_ERR_OK;
    }

    CBM_LogError( "iobject_handler_sqlite3_t::load(): ",
                "loading data source failed  [source=",this->source_info.full_name(),"]");
    return  LDNDC_ERR_READER_LOAD_FAILED;
}    
lerr_t
iobject_handler_sqlite3_t::reload()
{
    lerr_t  rc_close = this->close();
    RETURN_IF_NOT_OK(rc_close);

    lerr_t  rc_open = this->open( &this->source_info);
    RETURN_IF_NOT_OK(rc_open);

    return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_sqlite3_t::close()
{
        if ( this->handle.db)
    {
        sqlite3_close( this->handle.db);
        this->handle.db = NULL;
        this->handle.tbl = "";
        }

    return  LDNDC_ERR_OK;
}


ioproc_handle_sqlite3_t
iobject_handler_sqlite3_t::get_iobject_handle()
{
    return  this->handle;
}

}

