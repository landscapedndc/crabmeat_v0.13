/*!
 * @brief
 *    sqlite3 i/o format handler
 *
 * @author
 *    steffen klatt (created on: apr 18, 2014)
 */

#ifndef  LDNDC_IO_FHANDLER_SQLITE3_H_
#define  LDNDC_IO_FHANDLER_SQLITE3_H_

#include  "io/fhandler/fhandler.h"
#include  "io/source-info.h"
#include  "string/cbm_string.h"

#include  <sqlite3.h>

namespace ldndc {

struct  ioproc_handle_sqlite3_t
{
    std::string  tbl;
    sqlite3 *  db;
};

class  CBM_API  iobject_handler_sqlite3_base_t  :  public  iobject_handler_base_t
{
    public:
        static char const *  format()
            { return  "sqlite3"; }
        char const *  io_format_type() const
            { return this->format(); }
    protected:
        iobject_handler_sqlite3_base_t();
        virtual ~iobject_handler_sqlite3_base_t() = 0;

};


class  CBM_API  iobject_handler_sqlite3_t  :  public  iobject_handler_sqlite3_base_t
{
    LDNDC_SERVER_OBJECT(iobject_handler_sqlite3_t)
    public:
        static char const *  name()
            { return  "sqlite3 format handler"; }

    public:
        iobject_handler_sqlite3_t();
        virtual  ~iobject_handler_sqlite3_t();

        bool  is_open() const { return  this->handle.db != NULL; }
        lerr_t  open(
                source_info_t const *);

        lerr_t  load();
        lerr_t  reload();

        lerr_t  close();

    public:
        ioproc_handle_sqlite3_t  get_iobject_handle();
        source_info_t const *  info()
        const
               {
                   return  &this->source_info;
        }

    private:
        ioproc_handle_sqlite3_t  handle;
};
}

#endif  /*  !LDNDC_IO_FHANDLER_SQLITE3_H_  */

