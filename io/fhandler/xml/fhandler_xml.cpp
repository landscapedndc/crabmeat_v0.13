/*!
 * @brief
 *    xml i/o format handler (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#include  "io/fhandler/xml/fhandler_xml.h"
#include  "io/data-source/data-source.h"
#include  "string/lstring-compare.h"

#include  "utils/lutils-fs.h"
#include  "memory/cbm_mem.h"

#include  "log/cbm_baselog.h"

namespace ldndc {

iobject_handler_xml_base_t::iobject_handler_xml_base_t()
        : iobject_handler_base_t(),
          iproc_handle_xml_types_t()
{
}
iobject_handler_xml_base_t::~iobject_handler_xml_base_t()
{
}


iobject_handler_xml_t::iobject_handler_xml_t()
                : iobject_handler_xml_base_t(),
          input_handle_( NULL),
          xml_( NULL)
{
#if    defined(_USE_LIB_XML_TINYXML)
    TiXmlBase::SetCondenseWhiteSpace( true);
#endif
}


iobject_handler_xml_t::~iobject_handler_xml_t()
{
        (void)this->close();
}

iobject_handler_xml_t::iobject_handle_t *
iobject_handler_xml_t::get_iobject_handle()
{
           return  this->input_handle_;
}
lerr_t
iobject_handler_xml_t::set_iobject_handle_(
        iobject_handle_t *  _ih)
{
    this->input_handle_ = _ih;
    return  ( _ih) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}


lerr_t
iobject_handler_xml_t::open(
        source_info_t const *  _source_info)
{
        if ( this->xml_ || this->get_iobject_handle())
           {
                //CBM_LogWarn( "attempting to open data source twice!? (source=" + this->source_info.full_name() + ")");
                return  LDNDC_ERR_OK;
        }
    if ( _source_info)
    {
        /* (re)set */
        this->source_info = *_source_info;
    }

// TODO  find persistence type from source name
    persistence_type_e const  this_persistence_type = PERSISTENCE_TYPE_FILE;
    switch ( this_persistence_type)
    {
        case  PERSISTENCE_TYPE_FILE:
        {
            if ( !cbm::is_file( this->source_info.full_name().c_str()))
            {
                CBM_LogDebug( "iobject_handler_xml_t::open(): ",
                        "file does not exist or is not a (readable) file  [source=",this->source_info.full_name(),"]");
                break;
            }
            this->xml_ = CBM_DefaultAllocator->construct< iobject_type_t >();
            break;
        }
        default:
        {
            /* at this stage no object handle is set
             * causing to return with error later on
             */
            this->xml_ = NULL;
        }
    }

        if (( ! this->xml_ ) && ( ! this->get_iobject_handle()))
           {
                CBM_LogError( "iobject_handler_xml_t::open(): ",
                "opening data source failed  [source=",this->source_info.full_name(),"]");
                return  LDNDC_ERR_READER_OPEN_FAILED;
        }
        CBM_LogDebug( "iobject_handler_xml_t::open(): ",
            "opened data source  [source=",this->source_info.full_name(),"]");
        return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_xml_t::load()
{
    if ( ! this->get_iobject_handle())
    {
        std::string const  fname = this->source_info.full_name();
        if ( cbm::is_empty( fname))
        {
            CBM_LogError( "iobject_handler_xml_t::load(): input source descriptor invalid  [empty or null]");
            return  LDNDC_ERR_INVALID_ARGUMENT;
        }
        if ( this->xml_  &&  ( XML_NO_ERROR == this->xml_->LoadFile( fname.c_str())))
        {
            if ( this->set_iobject_handle_( this->xml_->RootElement()) == LDNDC_ERR_OK)
            {
                CBM_LogDebug( "successfully loaded data source  [source=",fname,"]");
                return  LDNDC_ERR_OK;
            }
        }
        else
        {
            if ( this->xml_)
            {
#if  defined(_USE_LIB_XML_TINYXML)
                CBM_LogError( "error msg: ", this->xml_->ErrorDesc(),
                        "  [row=", this->xml_->ErrorRow(),",col=", this->xml_->ErrorCol(),"]",
                        "\n\t- do opening and closing tags match?",
                        "\n\t- closing tags have slash?",
                        "\n\t- invalid attribute names?",
                        "\n\t- check for duplicate attributes in same element (if row/col are 0).");
#elif defined(_USE_LIB_XML_TINYXML2)
                this->xml_->PrintError();
#endif
            }
            else
            {
                CBM_LogError( "error msg: xml document was null. called open()?");
            }
            CBM_LogError( "reading data source failed. [source=", fname, "]");
        }
    }
    else
    {
        //CBM_LogWarn( "data source already loaded (source=" + this->get_iobject_source_name_() + ")");
        return  LDNDC_ERR_OK;
    }

    CBM_LogError( "iobject_handler_xml_t::load():",
        " loading data source failed [source=", this->source_info.full_name(), "]");
    return  LDNDC_ERR_FAIL;
}    
lerr_t
iobject_handler_xml_t::reload()
{
    lerr_t  rc_close = this->close();
    RETURN_IF_NOT_OK(rc_close);

    lerr_t  rc_open = this->open( &this->source_info);
    RETURN_IF_NOT_OK(rc_open);

    lerr_t  rc_load = this->load();
    RETURN_IF_NOT_OK(rc_load);

    return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_xml_t::close()
{
    if ( this->xml_)
           {
        CBM_DefaultAllocator->destroy( this->xml_);
        this->xml_ = NULL;
    }

    return  LDNDC_ERR_OK;
}



}

