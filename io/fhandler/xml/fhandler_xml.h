/*!
 * @brief
 *    xml i/o format handler
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#ifndef  LDNDC_IO_FHANDLER_XML_H_
#define  LDNDC_IO_FHANDLER_XML_H_

#include  "io/fhandler/fhandler.h"
#include  "io/source-info.h"

#if    defined(_USE_LIB_XML_TINYXML)
#  error  no support for tinyxml-v1.
#elif  defined(_USE_LIB_XML_TINYXML2)
#  include  "tinyxml2.h"
#elif  defined(_USE_LIB_XML_PUGIXML)
#  error  no support for pugixml.
#else
#  error  need to select a xml library to use
#endif


namespace ldndc {
struct  CBM_API  iproc_handle_xml_types_t
{
#if    defined(_USE_LIB_XML_TINYXML)
#  define  XML_NO_ERROR             true
#  define  XML_CLONE_NODE(__unused__)    Clone()
#  define  XML_LINK_END_CHILD(__node__)  LinkEndChild(__node__)
    typedef  TiXmlDocument       iobject_type_t;
    typedef  TiXmlNode           iobject_handle_t;
    typedef  TiXmlNode           core_iobject_handle_t;
#elif  defined(_USE_LIB_XML_TINYXML2)
#  define  XML_NO_ERROR             tinyxml2::XML_NO_ERROR
#  define  XML_CLONE_NODE(__xmldoc__)    ShallowClone(__xmldoc__)
#  define  XML_LINK_END_CHILD(__node__)  InsertEndChild(__node__)
    typedef  tinyxml2::XMLDocument       iobject_type_t;
    typedef  tinyxml2::XMLNode           iobject_handle_t;
    typedef  tinyxml2::XMLNode           core_iobject_handle_t;
#elif  defined(_USE_LIB_XML_PUGIXML)
    typedef  pugi::xml_document  iobject_type_t;
    typedef  pugi::xml_node      iobject_handle_t;
    typedef  pugi::xml_node      core_iobject_handle_t;
#else
#  error  need to select a xml library to use
#endif
};


class  CBM_API  iobject_handler_xml_base_t  :  public  iobject_handler_base_t,  public  iproc_handle_xml_types_t
{
    public:
        static char const *  format()
            { return  "xml"; }
        char const *  io_format_type() const
            { return this->format(); }
    protected:
        iobject_handler_xml_base_t();
        virtual ~iobject_handler_xml_base_t() = 0;

};


class  CBM_API  iobject_handler_xml_t  :  public  iobject_handler_xml_base_t
{
    LDNDC_SERVER_OBJECT(iobject_handler_xml_t)
    public:
        static char const *  name()
            { return  "xml format handler"; }

    public:
        iobject_handler_xml_t();
        virtual  ~iobject_handler_xml_t();

        lerr_t  open(
                source_info_t const *);

        lerr_t  load();
        lerr_t  reload();

        lerr_t  close();

    public:
        iobject_handle_t *  get_iobject_handle();
        source_info_t const *  info()
        const
        {
            return  this->xml_ ? &this->source_info : NULL;
               }

    private:
        lerr_t  set_iobject_handle_( iobject_handle_t *);

    private:
                /* root level handle, root node of xml structure */
        iobject_handle_t *  input_handle_;

        /* pointer to xml document object (owner) */
        iobject_type_t *  xml_;
};
}

#endif  /*  !LDNDC_IO_FHANDLER_XML_H_  */

