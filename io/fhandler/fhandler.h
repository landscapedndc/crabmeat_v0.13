/*!
 * @brief
 *    i/o format handler base
 *
 * @author
 *    steffen klatt (created on: nov 17, 2013)
 */

#ifndef  LDNDC_IO_FHANDLER_H_
#define  LDNDC_IO_FHANDLER_H_

#include  "crabmeat-common.h"
#include  "io/source-info.h"

namespace ldndc {
struct  source_info_t;
/* the input handler classes are used
 *    - open, close, ... the input objects (files, streams, ...)
 */
class  CBM_API  iobject_handler_interface_t
{
    public:
// TODO        virtual  bool  is_open() const = 0;
        virtual  lerr_t  open(
                source_info_t const *) = 0;

        virtual  lerr_t  load() = 0;
// TODO        virtual  bool  can_reload() const = 0;
        virtual  lerr_t  reload() = 0;

        virtual  lerr_t  close() = 0;

        virtual  source_info_t const *  info() const = 0;

//TODO         virtual  format_version() const = 0;

    protected:
        iobject_handler_interface_t();
        virtual  ~iobject_handler_interface_t() = 0;
};

class  CBM_API  iobject_handler_base_t
    : public  iobject_handler_interface_t, public  cbm::server_object_t
{
    public:
        virtual  char const *  io_format_type() const = 0;
    public:
        iobject_handler_base_t();
        virtual  ~iobject_handler_base_t() = 0;

    public:
        virtual  source_info_t const *  get_source_info();
        virtual  void  set_source_info( source_info_t const *);
    protected:
        source_info_t  source_info;
    
};
}

#endif  /*  !LDNDC_IO_FHANDLER_H_  */

