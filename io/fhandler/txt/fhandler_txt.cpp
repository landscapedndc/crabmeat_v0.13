/*!
 * @brief
 *    txt i/o format handler (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 17, 2013)
 */

#include  "io/fhandler/txt/fhandler_txt.h"
#include  "io/data-source/data-source.h"
#include  "log/cbm_baselog.h"

#include  "utils/lutils-fs.h"

#include  <limits>

namespace ldndc {

ioproc_handle_txt_t::streampos const  ioproc_handle_txt_t::INVALID_POS = (ioproc_handle_txt_t::streampos)invalid_t< ioproc_handle_txt_t::streampos >::value;
ioproc_handle_txt_t::streampos const  ioproc_handle_txt_t::MAX_POS = std::numeric_limits< std::streamsize >::max();


ioproc_handle_txt_t::ioproc_handle_txt_t()
        : pb( 0),
          pe( 0),
          pd( 0),
          ifs( NULL),
          lineoffs( 0)
{
}

ioproc_handle_txt_t::streambuf *
ioproc_handle_txt_t::to_buf()
{
    if ( !this->ifs)
    {
        CBM_LogWarn( "to_buf(): requesting buffer for null stream");
        return  NULL;
    }
    std::istream::sentry  sentry_for_handle( *this->ifs, true);
    return  this->ifs->rdbuf();
}

ioproc_handle_txt_t::ifstream *
ioproc_handle_txt_t::get_stream(
        streampos  _offs)
const
{
    if ( !this->ifs)
    {
        CBM_LogWarn( "get_stream(): requesting buffer for null stream");
        return  NULL;
    }
    streampos const  p(( _offs == INVALID_POS) ? this->pb : _offs); 
// sk:dbg    CBM_LogDebug( "get_stream(): p=",p,"  (offs=",_offs,"  begin=",this->pb,")");

    this->ifs->seekg( p);
    /* check for trespassing */
    if (( p < this->pe) && ( p >= this->pb) && this->ifs->good())
    {
        return  this->ifs;
    }
    else
    {
        if ( ! this->ifs->good())
        {
            CBM_LogError( "stream not good: state=",this->ifs->rdstate());
        }
        return  NULL;
    }
}


/***  INPUT HANDLER TXT BASE  ***/
iobject_handler_txt_base_t::iobject_handler_txt_base_t()
        : iobject_handler_base_t(),
          iproc_handle_txt_types_t()
{
}
iobject_handler_txt_base_t::~iobject_handler_txt_base_t()
{
}


/***  INPUT HANDLER TXT  ***/
LDNDC_SERVER_OBJECT_DEFN(ldndc::iobject_handler_txt_t)
iobject_handler_txt_t::iobject_handler_txt_t()
                : iobject_handler_txt_base_t(),
          txt_( NULL)
{
}


iobject_handler_txt_t::~iobject_handler_txt_t()
{
    (void)this->close();
}


lerr_t
iobject_handler_txt_t::open(
        source_info_t const *  _source_info)
{
        if ( this->txt_ || this->get_iobject_handle())
    {
                //CBM_LogWarn( "attempting to open data source twice!? (source=" + this->source_info.full_name() + ")");
                return  LDNDC_ERR_OK;
        }

    if ( _source_info)
    {
        /* (re)set */
        this->source_info = *_source_info;
    }

// TODO  find persistence type from source name
    persistence_type_e  this_persistence_type = PERSISTENCE_TYPE_FILE;
    switch ( this_persistence_type)
    {
        case  ldndc::PERSISTENCE_TYPE_FILE:
        {
            if ( !cbm::is_file( this->source_info.full_name().c_str()))
            {
                CBM_LogDebug( "iobject_handler_xml_t::open(): ",
                        "file does not exist or is not a (readable) file  [source=",this->source_info.full_name(),"]");
                break;
            }
            /* create file stream */
            this->txt_ = CBM_DefaultAllocator->construct< iobject_type_t >();
            if ( this->txt_)
            {
                /* open file read only */
                std::string const  fname = this->source_info.full_name();
                this->txt_->open( fname.c_str(), iobject_type_t::in|iobject_type_t::binary);
                if ( !this->txt_->is_open())
                {
                    CBM_LogError( "iobject_handler_txt_t::open(): ",
                            "opening file failed  [file=",this->source_info.full_name(),"]");
                    return  LDNDC_ERR_READER_OPEN_FAILED;
                }
            }
            else
            {
                CBM_LogError( "iobject_handler_txt_t::open(): ",
                        "creating in file stream failed");
                return  LDNDC_ERR_OBJECT_CREATE_FAILED;
            }
            break;
        }
        default:
        {
            /* at this stage no object handle is set
             * causing to return with error later on
             */
                        this->txt_ = NULL;
        }
        }

        if ( ! this->txt_)
    {
                CBM_LogError( "iobject_handler_txt_t::open(): ",
                "opening data source failed  [source=",this->source_info.full_name(),"]");
                return  LDNDC_ERR_READER_OPEN_FAILED;
        }
        CBM_LogDebug( "iobject_handler_txt_t::open(): ",
            "opened data source object  [source=",this->source_info.full_name(),"]");
        return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_txt_t::load()
{
        if ( this->get_iobject_handle())
    {
                //CBM_LogWarn( "data source already loaded (source=" + this->source_info.full_name() + ")");
                return  LDNDC_ERR_OK;
        }

    if ( this->txt_ && this->txt_->good())
    {
        if ( this->set_iobject_handle_( this->txt_) == LDNDC_ERR_OK)
        {
            return  LDNDC_ERR_OK;
        }
    }

        CBM_LogError( "iobject_handler_txt_t::load(): loading data source failed  [source=",this->source_info.full_name(),"]");
        return  LDNDC_ERR_READER_LOAD_FAILED;
}    
lerr_t
iobject_handler_txt_t::reload()
{
    lerr_t  rc_close = this->close();
    RETURN_IF_NOT_OK(rc_close);

    lerr_t  rc_open = this->open( &this->source_info);
    RETURN_IF_NOT_OK(rc_open);

    return  LDNDC_ERR_OK;
}


lerr_t
iobject_handler_txt_t::close()
{
        if ( this->txt_  &&  this->txt_->is_open())
    {
                this->txt_->close();
        }

        if ( this->txt_)
    {
                CBM_DefaultAllocator->destroy< iobject_type_t >( this->txt_);
        this->txt_ = NULL;
        }

    return  LDNDC_ERR_OK;
}


iobject_handler_txt_t::iobject_handle_t *
iobject_handler_txt_t::get_iobject_handle()
{
    return  this->txt_;
}
lerr_t
iobject_handler_txt_t::set_iobject_handle_(
        iobject_handle_t *  _ih)
{
    return  ( _ih) ? LDNDC_ERR_OK : LDNDC_ERR_FAIL;
}

}

