/*!
 * @brief
 *    txt "miner": helper that reads data from txt streams
 *
 * @author
 *    steffen klatt (created on: jan 24, 2014)
 */

#ifndef  LDNDC_IO_FHANDLER_MINER_TXT_H_
#define  LDNDC_IO_FHANDLER_MINER_TXT_H_

#include  "io/fhandler/txt/fhandler_txt.h"

#include  "containers/lstack.h"
#include  "string/lstring-basic.h"
#include  "string/lstring-compare.h"

namespace ldndc {

#define  TABLE_MINER_MAX  3
#define  txt_invalid_pos  ldndc::ioproc_handle_txt_t::INVALID_POS
#define  txt_streampos_t  ldndc::ioproc_handle_txt_t::streampos
#define  txt_streambuf_t  ldndc::ioproc_handle_txt_t::streambuf

struct  CBM_API  fhandler_txt_miner_helper_t
{
    fhandler_txt_miner_helper_t(
            ioproc_handle_txt_t * = NULL /*miner*/);
    ~fhandler_txt_miner_helper_t();

        txt_streambuf_t *  buf;
        txt_streampos_t  p_bot;

        /* each of the following function return the number
         * of lines it processed. a negative return value
         * indicates an error
         */
        int  eat_line(
                        txt_streampos_t * /*new position*/,
                        int = 1 /*line count*/);
        int  eat_characters(
                        txt_streampos_t * /*new position*/,
            int /*character count*/);
        int  eat_blanks(
                        txt_streampos_t * /*new position*/);
        int  eat_until_next_word(
                        txt_streampos_t * /*new position*/);
};

struct  CBM_API  fhandler_txt_miner_table_t
{
        fhandler_txt_miner_table_t();
        ~fhandler_txt_miner_table_t();

        size_t  columns_size;
        char const **  columns;

        size_t  column_map_size;
        size_t *  column_map;

        size_t  record_stride;

        int  offset;
        int  stride;

        int  items_read, records_read;
        int  items_seen, records_seen;

        txt_streampos_t  p_top;
        txt_streampos_t  p_cur;
        txt_streampos_t  p_bot;
};

struct  CBM_API  fhandler_txt_miner_t
{
        typedef  int  table_miner_handle_t;
    static table_miner_handle_t const  table_miner_invalid_handle;

    ioproc_handle_txt_t  handle;
        ldndc::stack< txt_streampos_t >  p_push;

        fhandler_txt_miner_table_t *  table_miner[TABLE_MINER_MAX];

    fhandler_txt_miner_t(
            ioproc_handle_txt_t const * = NULL /*handle*/);
    ~fhandler_txt_miner_t();

    /* push and pop file position (each action restores file position) */
        void  push_fp(
                        txt_streampos_t = txt_invalid_pos);
        void  pop_fp();

        int  reset();

    template < typename  _attribute_type >
    _attribute_type  read_attribute(
            char const *  _tag_name /*tag name*/,
            char const *  _attr_name /*attribute name*/,
            _attribute_type const &  _default /*default value*/,
            int *  _rc = NULL /*error code*/)
    {
                txt_streampos_t  tp = this->find_tag( _tag_name, NULL, this->handle.pb);
        if ( tp != txt_invalid_pos)
        {
            return  this->read_attribute_at( tp, _attr_name, _default, _rc);
        }
        return  _default;
    }
    template < typename  _attribute_type >
    _attribute_type  read_attribute_at(
            txt_streampos_t /*stream position*/,
            char const * /*attribute name*/,
            _attribute_type const & /*default value*/,
            int * = NULL /*error code*/);

        /*!
         * @brief
         *      find next tag with the given name after the
         *      current position.
         *      the search terminates when hitting a tag matching
         *      the terminate tag. if the terminate tag is NULL
         *      block boundaries apply.
     *
     *      the name of the tag found is stored in buffer
     *      if it is non-null
     *
     * @returns
     *    position of start of tag, i.e. of tag symbol. if
     *    tag was not found, "invalid position" is returned.
         */
    txt_streampos_t  find_tag(
            char const * /*tagname*/,
            char const * = NULL /*terminate tagname*/,
                        txt_streampos_t = txt_invalid_pos /*offset*/,
            std::string * = NULL /*tagname buffer*/);

        /*!
         * @brief
         *      find next tag after the current position. the
         *      search terminates when hitting a tag matching
         *      the terminate tag. if the terminate tag is NULL
         *      block boundaries apply.
     *
     *      the name of the tag found is stored in buffer
     *      if it is non-null
     *
     * @returns
     *    position of start of tag, i.e. of tag symbol. if
     *    tag was not found, "invalid position" is returned.
         */
    txt_streampos_t  find_next_tag(
            std::string * /*tagname buffer*/,
            txt_streampos_t = txt_invalid_pos /*offset*/,
            char const * = NULL /*terminate tagname*/);

        /*!
         * @brief
         *      creates a new table miner object and enqueues
         *      it to the list of table miners. the function
         *      returns a table handle which is used for
         *      later reference.
         *
         * @returns
         *      table handle
         *
         * @param
         *      sets given register to point to the newly
         *      created table miner.
         */
    int  new_table_miner(
                        fhandler_txt_miner_table_t ** /*new table miner*/,
                        char const * /*tagname*/,
                        char const ** /*record item names*/, size_t /*record item count*/);
    /*!
     * @brief
     *    deletes the associated table miner
     */
    int  delete_table_miner(
            table_miner_handle_t /*table miner handle*/);
    /*!
         * @brief
         *      read equi-typed data from regular table
         */
    template < typename  _data_type >
    int  read_table(
                        table_miner_handle_t /*table miner handle*/,
                        _data_type * /*buffer*/, size_t /*buffer size (in records)*/);
    template < typename  _data_type >
    int  read_table(
                        fhandler_txt_miner_table_t * /*table miner*/,
                        _data_type * /*buffer*/, size_t /*buffer size (in records)*/);

        int  rewind_table(
                        table_miner_handle_t /*table miner handle*/);

        int  find_table(
                        char const * /*tag name*/,
                        fhandler_txt_miner_table_t * /*table miner*/);
        int  read_table_header(
                        fhandler_txt_miner_table_t * /*table miner*/);


        /*!
         * @brief
         *      read data line starting at current file position
         *      until end-of-line.
         *
         * @returns
         *      number of lines processed, i.e. zero or one valid
         *      data lines and zero or more non-valid data lines.
         *
         * @param
         *      new position is set to final file position while
         *      reading data lines
         * @param
         *      buffer holds read characters
         * @param
         *      offset specifies how many valid data lines to skip
         */
        int  read_data_line(
                        txt_streampos_t * /*new position*/,
                        fhandler_txt_miner_helper_t * /*miner helper*/,
                        char * /*buffer*/, int /*offset*/);
};

}

#include  "log/cbm_baselog.h"
template < typename  _attribute_type >
_attribute_type
ldndc::fhandler_txt_miner_t::read_attribute_at(
                txt_streampos_t  _p,
                char const *  _attribute_name,
                _attribute_type const &  _default,
        int *  _rc)
{
        txt_streampos_t  p = _p;
    if ( p == txt_invalid_pos)
    {
        if ( _rc) *_rc = 0;
        return  _default;
    }
        this->push_fp( p);

    std::streambuf *  s_buf = this->handle.to_buf();
    if ( !s_buf)
    {
        if ( _rc) *_rc = -1;
                this->pop_fp();
        return  _default;
    }


    int  eol_r = 0;
    size_t  line_num = 0;

    /* file positions */
    int  tag_line = 0;
    int  quotes = 0;
    int  comment_line = 0;
    int  assignment = 0;

    std::string  attribute_name;
    std::string  attribute_value;

    int  chr = 0;

    /* forward by one, past '%' */
    while (( p < this->handle.pe) && (( chr = s_buf->snextc()) != EOF))
    {
        ++p;
        if ( chr == '\r')
        {
            continue;
        }
        if ( is_newline(chr))
        {
            eol_r = 1;
            comment_line = 0;
        }

        /* discard each character until end of comment line */
        if ( comment_line && !eol_r)
        {
            continue;
        }

        if ( eol_r)
        {
            eol_r = 0;

            if (( !comment_line) && !cbm::is_empty( attribute_name))
            {
                if ( cbm::is_equal( cbm::to_lower( attribute_name), _attribute_name))
                {
                    _attribute_type  attribute_value_T;
                    if ( ! cbm::is_whitespace( attribute_value))
                    {
// sk:dbg                        CBM_LogDebug( "read_attribute_at(): key=",_attribute_name,",  value=",attribute_value);
                        lerr_t  rc_convert = cbm::s2n< _attribute_type >( attribute_value, &attribute_value_T);
                        if ( rc_convert)
                        {
                            CBM_LogError( "fhandler_txt_miner_t::read_attribute_at(): ",
                                    "failed to convert attribute value  [attribute=",attribute_name,",value=",attribute_value,"]");
                            break;
                        }
                    }
                    else
                    {
                        attribute_value_T = _default;
                    }

                    if ( _rc) *_rc = 1;
                    this->pop_fp();
                    return  attribute_value_T;
                }
            }

            attribute_name.clear();
            attribute_value.clear();

            tag_line = 0;
            quotes = 0;
            comment_line = 0;
            assignment = 0;
        }
        else if ( is_comment(chr) && (!quotes) && (!tag_line))
        {
            comment_line = 1;
        }
        else if ( is_block(chr) && (!quotes) && (!assignment))
        {
            tag_line = 1;
                        break;
        }
        else if ( ( !quotes) && is_quotes_open(chr))
        {
            quotes = 1;
        }
        else if ((  quotes) && is_quotes_close(chr))
        {
            quotes = 0;
        }
        else if (( !quotes) && is_assignment(chr))
        {
            assignment = 1;
        }
        else if (( quotes || (!is_space( chr))) && assignment)
        {
            if ( attribute_name.empty())
            {
                CBM_LogError( "fhandler_txt_miner_t::read_attribute_at(): ",
                        "value without tag [line=",line_num,"]");
                break;
            }

            attribute_value += chr;
        }
        else if (( quotes || (!is_space( chr))) && !assignment)
        {
            attribute_name += chr;
        }
        else
        {
            /* no op */
        }
    }

    if ( _rc) *_rc = 0;
        this->pop_fp();
    return  _default;
}

template < typename _data_type >
int
ldndc::fhandler_txt_miner_t::read_table(
                int  _table_handle,
                _data_type *  _buf, size_t  _buf_size)
{
        fhandler_txt_miner_table_t *  tm = this->table_miner[_table_handle];
        return  this->read_table( tm, _buf, _buf_size);
}
#define  TXT_MAX_LINESIZE  256
template < typename _data_type >
int
ldndc::fhandler_txt_miner_t::read_table(
                fhandler_txt_miner_table_t *  _tm,
                _data_type *  _buf, size_t  _buf_size)
{
        _tm->records_read = 0;
        _tm->records_seen = 0;
        _tm->items_read = 0;
        _tm->items_seen = 0;

        this->push_fp( _tm->p_cur);

        fhandler_txt_miner_helper_t  mh( &this->handle);
    txt_streampos_t  p = _tm->p_cur;

    /* data line helper, maximum line size + null byte */
    char  linebuf[TXT_MAX_LINESIZE+1];

    /* offset into record array _rec (could be shift...) */
    _data_type *  this_buf = _buf;
    int  r = 0;
    /* read records from stream */
        for ( size_t  k = 0;  k < _buf_size;  ++k)
    {
        /* mark as empty line */
        linebuf[0] = '\0';
        /* mark eol */
        linebuf[TXT_MAX_LINESIZE] = '\0';

        /* skip empty and comment lines */
        r = this->read_data_line( &p, &mh, linebuf, 0);
                if ( r == -1)
        {
            CBM_LogError( "fhandler_txt_miner_t::read_table(): ",
                    "finding next data line failed");
            break;
        }

        /* in case we read from the final position */
        if ( linebuf[0] == '\0')
        {
            break;
        }

                _tm->records_read += 1;
                _tm->records_seen += 1;

        /* just skip lines */
        if ( !this_buf)
        {
            continue;
        }

        /* read item of record into array */
        size_t  j( 0), l( 0);
        _data_type  data_item;
        char *  nptr = linebuf;
        char *  endptr = linebuf;
        while ( endptr)
        {
            if ( j >= _tm->column_map_size)
            {
                CBM_LogError( "fhandler_txt_miner_t::read_table():");
                CBM_LogError( "number of columns (",j+1,") in line exceeds number of header elements (",_tm->column_map_size,")");
                CBM_LogError( "last seen data item: \"", data_item, "\" in column ", j+1);
                fprintf( stderr, "line='%s'  remainder='%s'\n", linebuf, endptr);
                break;
            }

            /* ignore column entry for '*' columns */
            if ( _tm->column_map[j] == ldndc::invalid_t< size_t >::value)
            {
                /* forward to beginning of past next data item */
                while ( *nptr != '\0' && is_space(*nptr)) { ++nptr; }
                while ( *nptr != '\0' && !is_space(*nptr)) { ++nptr; }
                while ( *nptr != '\0' && is_space(*nptr)) { ++nptr; }

                ++j;
                if ( *nptr == '\0')
                {
                    break;
                }
                continue;
            }

            data_item = strtod( nptr, &endptr);
            if ( nptr != endptr)
            {
                nptr = endptr;
                while ( *nptr != '\0' && is_space(*nptr)) { ++nptr; }
                if ( *nptr == '\0')
                {
                    endptr = NULL;
                }
            }
            else
            {
                break;
            }

            crabmeat_assert( _tm->column_map[j] != ldndc::invalid_t< size_t >::value);
            this_buf[_tm->column_map[j]] = data_item;

            ++j;
            if ( ++l == _tm->columns_size)
            {
                break;
            }
// sk:todo            /* we are finished after we read all non-'*' data columns */
// sk:todo            if ( l == valid_data_columns)
// sk:todo            {
// sk:todo                break;
// sk:todo            }
        }

        /* issue warning for missing columns (unless we found what we wanted..) */
        if      (( l == _tm->columns_size) && ( j < _tm->column_map_size))
        {
            CBM_LogWarn( "fhandler_txt_miner_t::read_table(): ",
                    "incomplete line. but no missing input  [record=",k,"column count=",j,"]");
        }
               else if (( l  < _tm->columns_size) && ( j < _tm->column_map_size))
        {
            CBM_LogInfo( "fhandler_txt_miner_t::read_table(): ",
                    "last read line (",p,"): \"",linebuf,"\"");
            CBM_LogError( "fhandler_txt_miner_t::read_table(): ",
                    "incomplete data line in input stream  [line=",r+1,"[approx. and relative to reload (TODO)]]");
            this->pop_fp();
            return  -1;
        }

        /* update counters */
        if ( j > 0)
        {
            _tm->items_read += static_cast< int >( j);
        }

        /* update offset */
                this_buf += _tm->record_stride;
        }        

    /* update current position in stream */
        _tm->p_cur = p;
        this->pop_fp();

    return  ( r < 0) ? -1 : _tm->records_read;
}

#define  GLOBAL_ATTRIBUTE_VALUE_AT(__miner__,__reg__,__attr__,__default__)    \
    __reg__ = __miner__.read_attribute(                        \
                        ldndc_txt_global, __attr__, __default__)/*;*/

#define  GLOBAL_ATTRIBUTE_VALUE(__reg__,__attr__,__default__)            \
    {                                        \
        fhandler_txt_miner_t  ___m( &this->shandle);                \
    GLOBAL_ATTRIBUTE_VALUE_AT(___m,__reg__,__attr__,__default__);        \
    }                                    \
    CRABMEAT_FIX_UNUSED(__reg__)/*;*/

#define  ATTRIBUTE_VALUE_(__miner__,__tagname__,__attr__,__default__)                \
    (__miner__)->read_attribute(__tagname__,__attr__,__default__)/*;*/
#define  ATTRIBUTE_VALUE(__tagname__,__attr__,__default__)                \
    ATTRIBUTE_VALUE_(&this->miner,__tagname__,__attr__,__default__)/*;*/

#define  ATTRIBUTE_VALUE_AT_(__miner__,__pos__,__attr__,__default__)        \
    (__miner__)->read_attribute_at(__pos__,__attr__,__default__)/*;*/
#define  ATTRIBUTE_VALUE_AT(__pos__,__attr__,__default__)                \
    ATTRIBUTE_VALUE_AT_(&this->miner,__pos__,__attr__,__default__)/*;*/


#endif  /*  !LDNDC_IO_FHANDLER_MINER_TXT_H_  */

