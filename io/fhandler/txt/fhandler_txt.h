/*!
 * @brief
 *    txt i/o format handler
 *
 * @author
 *    steffen klatt (created on: nov 17, 2013)
 */

#ifndef  LDNDC_IO_FHANDLER_TXT_H_
#define  LDNDC_IO_FHANDLER_TXT_H_

#include  "io/fhandler/fhandler.h"
#include  "io/source-info.h"

#include  <iostream>
#include  <fstream>
#include  <sstream>


#define  ldndc_txt_global  "global"
#define  ldndc_txt_time  "time"
#define  ldndc_txt_cuefile  "cuefile"
#define  ldndc_txt_cueline  "cueline"
#define  ldndc_txt_id  "id"
#define  ldndc_txt_attributes  "attributes"
#define  ldndc_txt_data  "data"
#define  ldndc_txt_block  this->core_tag()
#define  ldndc_txt_units  "units"

#define  TXT_HEADER_GLOBAL_TAG  ldndc_txt_global
#define  TXT_HEADER_COREBLOCK_TAG  this->core_tag()
#define  TXT_HEADER_DATA_TAG  ldndc_txt_data
#define  TXT_HEADER_ATTRIB_TAG  ldndc_txt_attributes
#define  TXT_HEADER_CUELINE_TAG  "cueline"

#define  TXT_HEADER_TERMINATE_TAG  TXT_HEADER_DATA_TAG

namespace ldndc {

struct  ioproc_handle_txt_t
{
    typedef  std::ifstream  ifstream;
    typedef  std::streambuf  streambuf;
    typedef  ldndc_int64_t  streampos;

    static streampos const  INVALID_POS;
    static streampos const  MAX_POS;

    ioproc_handle_txt_t();

    /* block first position */
    streampos  pb;
    /* block last position */
    streampos  pe;
    /* data position */
    streampos  pd;

    ifstream *  ifs;

    int  lineoffs;

    streambuf *  to_buf();

    streampos  get_position()
    {
        return  ( this->ifs) ? static_cast< streampos >( this->ifs->tellg()) : INVALID_POS;
    }
    streampos  set_position(
            streampos  _pos)
    {
        if ( _pos != INVALID_POS)
        {
            if ( !this->ifs)
            {
                return  INVALID_POS;
            }
            this->ifs->seekg( _pos);
        }
        return  this->get_position();
    }

    /* return stream pointer set to requested position */
    ifstream *  get_stream(
            streampos = INVALID_POS) const;
};

struct  iproc_handle_txt_types_t
{
        /* type of the data source object */
    typedef  std::ifstream  iobject_type_t;
        /* top level node type (e.g. data structure's root) */
        typedef  ioproc_handle_txt_t::ifstream  iobject_handle_t;
        /* core node handle type */
    typedef  ioproc_handle_txt_t  core_iobject_handle_t;
};

typedef  ioproc_handle_txt_t::streampos  stream_buffer_position_t;



class  CBM_API  iobject_handler_txt_base_t  :  public  iobject_handler_base_t,  public  iproc_handle_txt_types_t
{
    public:
        static char const *  format()
            { return  "txt"; }
        char const *  io_format_type() const
            { return this->format(); }
    protected:
        iobject_handler_txt_base_t();
        virtual ~iobject_handler_txt_base_t() = 0;

};


class  CBM_API  iobject_handler_txt_t  :  public  iobject_handler_txt_base_t
{
    LDNDC_SERVER_OBJECT(iobject_handler_txt_t)
    public:
        static char const *  name()
            { return  "txt format handler"; }

    public:
        iobject_handler_txt_t();
        virtual  ~iobject_handler_txt_t();

        lerr_t  open(
                source_info_t const *);

        lerr_t  load();
        lerr_t  reload();

        lerr_t  close();

    public:
        iobject_handle_t *  get_iobject_handle();
        source_info_t const *  info()
        const
               {
                   return  this->txt_ ? &this->source_info : NULL;
        }

    private:
        lerr_t  set_iobject_handle_(
                iobject_handle_t *);

        /* pointer to stream object (owner) */
        iobject_type_t *  txt_;
};
}

#endif  /*  !LDNDC_IO_FHANDLER_TXT_H_  */

