/*!
 * @brief
 *    txt "miner": helper that reads data from txt streams
 *    (implementation) (uses stl i/o interface)
 *
 * @author
 *    steffen klatt (created on: jan 24, 2014)
 */


#include  "io/fhandler/txt/fhandler_txt_miner.h"

#include  "string/lstring-basic.h"
#include  "string/lstring-compare.h"
#include  "string/lstring-convert.h"
#include  "string/lstring-transform.h"

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"


ldndc::fhandler_txt_miner_helper_t::fhandler_txt_miner_helper_t(
        ioproc_handle_txt_t *  _handle)
        : buf( _handle ? _handle->to_buf() : NULL),
          p_bot( _handle ? _handle->pe : txt_invalid_pos)
{
    crabmeat_assert( !_handle || buf);

    if ( _handle)
    {
        _handle->set_position( _handle->pb);
    }
}
ldndc::fhandler_txt_miner_helper_t::~fhandler_txt_miner_helper_t()
{
}

int
ldndc::fhandler_txt_miner_helper_t::eat_line(
        txt_streampos_t *  _buf_pos,
        int  _num_lines)
{
    crabmeat_assert( this->buf && _buf_pos);

    int  chr = '\0';
    int  r = 0;
    txt_streampos_t  p = *_buf_pos;
    for ( int  l = 0;  l < _num_lines;  ++l)
    {
        while (( p < this->p_bot) && (( chr = this->buf->sbumpc()) != EOF))
        {
            ++p;
            ++r;
            if ( chr == '\r')
            {
                continue;
            }
            else if ( is_newline(chr))
            {
                break;
            }
        }
    }

    *_buf_pos = p;
    return  r;
}

int
ldndc::fhandler_txt_miner_helper_t::eat_characters(
        txt_streampos_t *  _buf_pos,
        int  _num_chars)
{
    crabmeat_assert( this->buf && _buf_pos);
    if ( _num_chars == 0)
    {
        return  0;
    }

    int  chr = '\0';
    int  r = 0;
    txt_streampos_t  p = *_buf_pos;
    while (( p < this->p_bot) && (( chr = this->buf->sbumpc()) != EOF))
    {
        ++p;
        ++r;
        if ( r == _num_chars)
        {
            break;
        }
    }

    *_buf_pos = p;
    return  r;
}

int
ldndc::fhandler_txt_miner_helper_t::eat_blanks(
        ldndc::stream_buffer_position_t *  _buf_pos)
{
    crabmeat_assert( this->buf && _buf_pos);

    int  chr = '\0';
    int  comment_line = 0;
    int  r = 0;
    txt_streampos_t  p = *_buf_pos;
    while (( p < this->p_bot) && (( chr = this->buf->sbumpc()) != EOF))
    {
        if ( is_newline(chr))
        {
            comment_line = 0;
        }
        else if ( is_space(chr) || (chr == '\r') || comment_line)
        {
            /* eat */
        }
        else if ( is_comment(chr))
        {
            comment_line = 1;
        }
        else
        {
            this->buf->sungetc();
            break;
        }
        ++p;
        ++r;
    }

    *_buf_pos = p;
    return  r;
}

int
ldndc::fhandler_txt_miner_helper_t::eat_until_next_word(
        ldndc::stream_buffer_position_t *  _buf_pos)
{
    crabmeat_assert( this->buf && _buf_pos);

    int  chr = '\0';
    txt_streampos_t  p = *_buf_pos;
    int  r = this->eat_blanks( &p);
    while (( p < this->p_bot) && (( chr = this->buf->sbumpc()) != EOF))
    {
        ++p;
        if ( !is_space(chr) && !is_newline(chr))
        {
            ++r;
            continue;
        }
        break;
    }
    r += this->eat_blanks( &p);

    *_buf_pos = p;
    return  r;
}


ldndc::fhandler_txt_miner_table_t::fhandler_txt_miner_table_t()
        : columns_size( 0), columns( NULL),
          column_map_size( 0), column_map( NULL),
          record_stride( 1),
          offset( 0), stride( 1),

          items_read( 0), records_read( 0),
          items_seen( 0), records_seen( 0),

          p_top( txt_invalid_pos), p_cur( txt_invalid_pos), p_bot( txt_invalid_pos)
{
}
ldndc::fhandler_txt_miner_table_t::~fhandler_txt_miner_table_t()
{
    if ( this->column_map)
    {
        CBM_DefaultAllocator->deallocate( this->column_map);
    }
}

ldndc::fhandler_txt_miner_t::table_miner_handle_t const  ldndc::fhandler_txt_miner_t::table_miner_invalid_handle = 
                ldndc::invalid_t< ldndc::fhandler_txt_miner_t::table_miner_handle_t >::value;


ldndc::fhandler_txt_miner_t::fhandler_txt_miner_t(
        ioproc_handle_txt_t const *  _handle)
          : handle(( _handle) ? *_handle : ioproc_handle_txt_t())
{
    for ( int  m = 0;  m < TABLE_MINER_MAX;  ++m)
    {
        this->table_miner[m] = NULL;
    }
}
ldndc::fhandler_txt_miner_t::~fhandler_txt_miner_t()
{
    for ( int  m = 0;  m < TABLE_MINER_MAX;  ++m)
    {
        if ( this->table_miner[m])
        {
            CBM_DefaultAllocator->destroy( this->table_miner[m]);
        }
    }

    if ( !this->p_push.is_empty())
    {
        CBM_LogError( "fhandler_txt_miner_t::~fhandler_txt_miner_t(): ",
                "[BUG]  txt miner position stack not empty at destruct time  [size=",this->p_push.fillsize(),"]");
#ifdef  _DEBUG
        while ( !this->p_push.is_empty())
        {
            CBM_LogError( "\t",this->p_push.pop());
        }
#endif  /*  _DEBUG  */
    }
}


void
ldndc::fhandler_txt_miner_t::push_fp(
        txt_streampos_t  _pos)
{
    this->p_push.push( this->handle.pb);
// sk:dbg    CBM_LogDebug( "PUSH  ", this->handle.pb, "  size=",this->p_push.fillsize());
    if ( _pos != txt_invalid_pos)
    {
        this->handle.pb = _pos;
    }
    this->handle.set_position( this->handle.pb);
}
void
ldndc::fhandler_txt_miner_t::pop_fp()
{
    crabmeat_assert( !this->p_push.is_empty());
    this->handle.pb = this->handle.set_position( this->p_push.pop());
// sk:dbg    CBM_LogDebug( " POP  ",this->handle.pb, "  size=",this->p_push.fillsize());
}


int
ldndc::fhandler_txt_miner_t::reset()
{
    while ( !this->p_push.is_empty())
    {
        this->handle.pb = this->p_push.pop();
    }

    for ( int  m = 0;  m < TABLE_MINER_MAX;  ++m)
    {
        fhandler_txt_miner_table_t *  tm = this->table_miner[m];
        if ( tm)
        {
            this->rewind_table( m);
        }
    }

    return  0;
}

txt_streampos_t
ldndc::fhandler_txt_miner_t::find_tag(
        char const *  _tag_name,
        char const *  _term_tag,
        txt_streampos_t  _p_offs,
        std::string *  _buf)
{
// sk:dbg    CBM_LogDebug( "find_tag() IN:  ",(_tag_name?_tag_name:"?"),"/",_p_offs);
    ( _p_offs != txt_invalid_pos) ? this->push_fp( _p_offs) : this->push_fp();

    std::streambuf *  s_buf = this->handle.to_buf();
    if ( !s_buf)
    {
        CBM_LogError( "fhandler_txt_miner_t::find_tag(): ",
                "nobuf");
        this->pop_fp();
        return  txt_invalid_pos;
    }

    int  eol_r = 0;
    //size_t  line_num = 0;

    /* file positions */
    int  tag_line = 0;
    int  comment_line = 0;
    int  ignore_line = 0;

    std::string  tag_name;
    txt_streampos_t  tag_pos = txt_invalid_pos;
    txt_streampos_t  p_tag = txt_invalid_pos;
    txt_streampos_t  p = this->handle.pb;

    int  chr = 0;

    while (( p < this->handle.pe) && ( chr = s_buf->sbumpc()) != EOF)
    {
        ++p;
        if ( chr == '\r')
        {
            continue;
        }
        if ( is_newline(chr))
        {
            eol_r = 1;
            comment_line = 0;
            //++line_num;
        }

        if ( ignore_line && !eol_r)
        {
            continue;
        }
        /* discard each character until end of comment line */
        if ( comment_line && !eol_r)
        {
            continue;
        }

        if ( eol_r)
        {
            eol_r = 0;

            if ( tag_line)
            {
                std::string const  tag_name_lowercase = cbm::to_lower( tag_name);
                /* use first tag if none was explicitly given (_tag_name == NULL) */
                if ( !_tag_name || cbm::is_equal( tag_name_lowercase, _tag_name))
                {
                    p_tag = tag_pos;
                    if ( _buf)
                    {
                        *_buf = tag_name_lowercase;
                    }
                    break;
                }
                else if ( _term_tag && cbm::is_equal( tag_name_lowercase, _term_tag))
                {
                    p_tag = txt_invalid_pos;
                    break;
                }
            }

            tag_line = 0;
            comment_line = 0;
            ignore_line = 0;
        }
        else if ( is_comment(chr))
        {
            comment_line = 1;
        }
        else if ( tag_line && (is_space(chr) && !tag_name.empty()))
        {
            /* tag fully read */
            eol_r = 1;
        }
        else if ( tag_line && (!is_space(chr)))
        {
            tag_name += chr;
        }
        else if ( is_block(chr))
        {
            tag_line = 1;
            tag_name.clear();

            tag_pos = p-1;
        }
        else if ( !tag_line && !(is_space(chr)))
        {
            ignore_line = 1;
        }
    }

    this->pop_fp();

// sk:dbg    CBM_LogDebug( "find_tag() OUT:  ",(_tag_name?_tag_name:"?"),"/",p_tag);
    return  p_tag;
}
txt_streampos_t
ldndc::fhandler_txt_miner_t::find_next_tag(
        std::string *  _buf,
        txt_streampos_t  _offset,
        char const *  _term_tag)
{
    return  this->find_tag( NULL, _term_tag, _offset, _buf);
}


int
ldndc::fhandler_txt_miner_t::rewind_table(
                        table_miner_handle_t  _table_handle)
{
    crabmeat_assert( _table_handle >= 0 && _table_handle < TABLE_MINER_MAX);
    if ( !this->table_miner[_table_handle])
    {
        CBM_LogError( "fhandler_txt_miner_t::rewind_table(): ",
                "handle not assigned to any table miner  [handle=",_table_handle,"]");
        return  -1;
    }
           this->table_miner[_table_handle]->p_cur = this->table_miner[_table_handle]->p_top;
    return  0;
}


ldndc::fhandler_txt_miner_t::table_miner_handle_t
ldndc::fhandler_txt_miner_t::new_table_miner(
        fhandler_txt_miner_table_t **  _table_miner,
        char const *  _table_name,
        char const **  _columns, size_t  _columns_size)
{
    for ( int  m = 0;  m < TABLE_MINER_MAX;  ++m)
    {
        if ( this->table_miner[m] == NULL)
        {
            fhandler_txt_miner_table_t *  tm = CBM_DefaultAllocator->construct< fhandler_txt_miner_table_t >();
            if ( !tm)
            {
                CBM_LogError( "fhandler_txt_miner_t::new_table_miner(): ",
                        "nomem");
                return  -1;
            }
            this->table_miner[m] = tm;

            int  rc_find = this->find_table( _table_name, tm);
            if ( rc_find)
            {
                this->delete_table_miner( m);
                CBM_LogError( "fhandler_txt_miner_t::new_table_miner(): ",
                        "no such table in block  [table=",_table_name,"]");
                return  -1;
            }

            tm->p_cur = tm->p_top;
            tm->p_bot = this->handle.pe;

            tm->columns = _columns;
            tm->columns_size = _columns_size;

            tm->record_stride = tm->columns_size;

            int  rc_header = this->read_table_header( tm);
            if ( rc_header)
            {
                this->delete_table_miner( m);
                CBM_LogError( "fhandler_txt_miner_t::new_table_miner(): ",
                        "failed to read table header  [table=",_table_name,"]");
                return  -1;
            }

            if ( _table_miner)
            {
                *_table_miner = tm;
            }
            return  m;
        }
    }

    CBM_LogError( "fhandler_txt_miner_t::new_table_miner(): ",
            "no more table miner slots available  [max=",TABLE_MINER_MAX,"]");
    return  -1;
}
int
ldndc::fhandler_txt_miner_t::delete_table_miner(
        table_miner_handle_t  _table_handle)
{
    if ( _table_handle == table_miner_invalid_handle)
    {
        return  0;
    }
    crabmeat_assert( _table_handle < TABLE_MINER_MAX);
    if ( this->table_miner[_table_handle])
    {
        CBM_DefaultAllocator->destroy( this->table_miner[_table_handle]);
        this->table_miner[_table_handle] = NULL;
    }
    return  0;
}
int
ldndc::fhandler_txt_miner_t::find_table(
        char const *  _table_name,
        fhandler_txt_miner_table_t *  _table_miner)
{
    this->push_fp();

    fhandler_txt_miner_helper_t  mh( &this->handle);

    txt_streampos_t  p_tbl = this->find_tag( _table_name);
    if ( p_tbl == txt_invalid_pos)
    {
        p_tbl = this->handle.pb;
        /* if no "%<blanks><tagname>" block, assume first non-blank to be start of column headers */
        mh.eat_blanks( &p_tbl);
    }
    else
    {
        ++p_tbl;
        /* move across "%<blanks><tagname>" block name towards start of column headers */
        p_tbl = this->handle.set_position( p_tbl);
        if ( p_tbl == txt_invalid_pos)
        {
            this->pop_fp();
            return  -1;
        }
        mh.eat_blanks( &p_tbl);
        mh.eat_until_next_word( &p_tbl);
    }

    _table_miner->p_top = p_tbl;
    _table_miner->p_cur = p_tbl;

    this->pop_fp();
    return  0;
}
int
ldndc::fhandler_txt_miner_t::read_table_header(
        fhandler_txt_miner_table_t *  _table_miner)
{
    crabmeat_assert( _table_miner);
    this->push_fp( _table_miner->p_top);

    fhandler_txt_miner_table_t *  tm = _table_miner;

    fhandler_txt_miner_helper_t  mh( &this->handle);

    txt_streampos_t  p = tm->p_top;

    char  linebuf[TXT_MAX_LINESIZE+1];
    int  lines = this->read_data_line( &p, &mh, linebuf, 0);
    if ( lines < 1)
    {
        this->pop_fp();
        return  -1;
    }

    std::string  s( linebuf);
    /* find number of columns in data stream (more exact, number of header items) */
    tm->column_map_size = 0;
    std::istringstream  header_line( s);
    std::string  s_item;
    while ( header_line >> s_item)
    {
        ++tm->column_map_size;
    }

    /* actually, this cannot happen because we found a valid data line ;-) */
    if ( tm->column_map_size == 0)
    {
        this->pop_fp();
        return  0;
    }
        tm->column_map = CBM_DefaultAllocator->allocate_init_type< size_t >( tm->column_map_size, invalid_t< size_t >::value);

    /* reset stream */
    header_line.str( s);
    header_line.clear();
    
    //int  r( 0);
    /* generate mapping from file table order to internal order */
    for ( size_t  j = 0;  j < tm->column_map_size;  ++j)
    {
        /* read next column header item from stream */
        std::string  c_hdr;
        /* cannot fail because it was checked previously */
        /* sk:FIXME  should use the same strategy of splitting as read_table .. you never know */
        header_line >> c_hdr;
        
        /* find index of current column header item */
        tm->column_map[j] = ldndc::invalid_t< size_t >::value;
        
        /* by definition we skip columns having as column name an asterisk '*' */
        if ( strcmp( c_hdr.c_str(), "*") == 0)
        {
            continue;
        }
        
        unsigned int  k( 0);
        if ( cbm::find_index( c_hdr.c_str(), tm->columns, tm->columns_size, &k) != LDNDC_ERR_OK)
        {
            CBM_LogWarn( "fhandler_txt_miner_t::read_table_header(): ",
                        "unknown record item \"",c_hdr,"\". skipping.");
            continue;
        }
        else
        {
            tm->column_map[j] = k;
        }
        
        //++r;
    }

    /* check if indexes are unique. if they are not that means there are duplicate columns */
    for ( size_t  j = 0;  j < tm->column_map_size-1;  ++j)
    {
        if ( tm->column_map[j] == ldndc::invalid_t< size_t >::value)
        {
            continue;
        }
        for ( size_t  k = j+1;  k < tm->column_map_size;  ++k)
        {
            if ( tm->column_map[j] == tm->column_map[k])
            {
                CBM_LogError( "fhandler_txt_miner_t::read_table_header(): ",
                        "duplicate columns in header  [column=(",j+1,",",k+1,"),item=",tm->columns[tm->column_map[j]],"]");
                this->pop_fp();
                return  -1;
            }
        }
    }

    /* update top position of table (data) */
    tm->p_top = p;
    this->pop_fp();
    return  0;
}

int
ldndc::fhandler_txt_miner_t::read_data_line(
        txt_streampos_t *  _pos,
        fhandler_txt_miner_helper_t *  _mh,
        char *  _line,
        int  _offset)
{
    crabmeat_assert( _pos);
#ifdef  CRABMEAT_SANITY_CHECKS
    /* _offset must _not_ be less than 0 */
    if ( _offset < 0)
    {
        CBM_LogError( "fhandler_txt_miner_t::read_data_line(): ",
                "negative offset is invalid  [offset=",_offset,"]");
        return  -1;
    }
#endif

    /* skip lines in the beginning */
    int  offset( _offset);
    /* current character read */
    int  chr = '\0';
    txt_streampos_t  p = *_pos;

    /* number of rows processed */
    int  r = 0;

    /* position in line buffer */
    int  c = 0;
    /* indicator for consecutive whitespace */
    bool  consec_spaces = 0;

    /* scan stream within allowed boundaries until non-comment line (or eof) */
    r += _mh->eat_blanks( &p);

    while (( p < _mh->p_bot) && (( chr = _mh->buf->sbumpc()) != EOF))
    {
        ++p;
        if ( offset)
        {
            r += _mh->eat_line( &p);
            r += _mh->eat_blanks( &p);

            /* keep last seen beginning of line */
            *_pos = p;
            --offset;

            continue;
        }

        if ( chr == '\r')
        {
            continue;
        }
        if ( is_newline(chr))
        {
            _line[c] = '\0';
            ++r;
            break;
        }

        if ( consec_spaces && is_space(chr))
        {
            continue;
        }
        else if ( is_space(chr))
        {
            chr = ' ';
            consec_spaces = 1;
        }
        else if ( is_block(chr) && ( c <= 1))
        {
            /* assume we hit tag (position is set to beginning of line) */
            return  0;
        }
        else
        {
            consec_spaces = 0;
        }

        _line[c++] = chr;
        _line[c] = '\0';
        if ( c == TXT_MAX_LINESIZE)
        {
            CBM_LogError( "fhandler_txt_miner_t::read_table_header(): ",
                    "insufficient buffersize in txt reader  [size>",TXT_MAX_LINESIZE,",lineno=","?","]");
            return  -1;
        }
    }

    if ( offset)
    {
        CBM_LogError( "fhandler_txt_miner_t::read_table_header(): ",
                "offset too large. \"empty wrapping\" not supported  [offset=",offset,"]");
        return  -1;
    }


    *_pos = p;
    return  r;
}

