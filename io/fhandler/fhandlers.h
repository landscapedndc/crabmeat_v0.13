/*!
 * @brief
 *    i/o object handlers factory array (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 17, 2013)
 */

#ifndef  LDNDC_IO_FHANDLERS_H_
#define  LDNDC_IO_FHANDLERS_H_

#include  "io/fhandler/fhandler.h"

namespace ldndc {
/*!
 * @brief
 *    construct i/o handler object
 */
struct  CBM_API  io_fhandler_factory_base_t
{
    io_fhandler_factory_base_t();
    virtual  ~io_fhandler_factory_base_t() = 0;

    virtual  char const *  name() const = 0;
    virtual  iobject_handler_base_t *  construct() const = 0;
    virtual  void  destroy( iobject_handler_base_t *) const = 0;
};
template < typename  _fhandler >
struct  CBM_API  io_fhandler_factory_t  :  public  io_fhandler_factory_base_t
{
    typedef  _fhandler  io_fhandler_t;

    io_fhandler_factory_t();
    virtual  ~io_fhandler_factory_t();

    char const *  name() const
        { return  io_fhandler_t::name(); }

    iobject_handler_base_t *  construct() const
        { return  static_cast< iobject_handler_base_t * >(
                        io_fhandler_t::new_instance()); }
    void  destroy( iobject_handler_base_t *  _hdl) const
    {
        if ( !_hdl)
            { return; }
        static_cast< _fhandler * >( _hdl)->delete_instance();
    }
};
template < typename  _fhandler >
io_fhandler_factory_t< _fhandler >::io_fhandler_factory_t()
        : ldndc::io_fhandler_factory_base_t()
{
}
template < typename  _fhandler >
io_fhandler_factory_t< _fhandler >::~io_fhandler_factory_t()
{
}

}

namespace ldndc {
    extern CBM_API ldndc::io_fhandler_factory_base_t const *
            find_fhandler_factory( char const * /*format*/);
}


#endif  /*  !LDNDC_IO_FHANDLERS_H_  */

