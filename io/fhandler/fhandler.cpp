/*!
 * @brief
 *    i/o format handler base
 *
 * @author
 *    steffen klatt (created on: nov 17, 2013)
 */

#include  "io/fhandler/fhandler.h"

ldndc::iobject_handler_interface_t::iobject_handler_interface_t()
{
}
ldndc::iobject_handler_interface_t::~iobject_handler_interface_t()
{
}

ldndc::iobject_handler_base_t::iobject_handler_base_t()
        : iobject_handler_interface_t(), cbm::server_object_t()
{
}
ldndc::iobject_handler_base_t::~iobject_handler_base_t()
{
}

ldndc::source_info_t const *
ldndc::iobject_handler_base_t::get_source_info()
{
    return  &this->source_info;
}
void
ldndc::iobject_handler_base_t::set_source_info(
        ldndc::source_info_t const *  _source_info)
{
    if ( _source_info)
    {
        this->source_info = *_source_info;
    }
}

