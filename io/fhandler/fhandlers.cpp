/*!
 * @brief
 *    i/o object handlers factory array (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 17, 2013)
 */

#include  "io/fhandler/fhandlers.h"

ldndc::io_fhandler_factory_base_t::io_fhandler_factory_base_t()
    { }
ldndc::io_fhandler_factory_base_t::~io_fhandler_factory_base_t()
    { }

#ifdef  LDNDC_IO_RESOURCES
#  include  "io/fhandler/lrsrc/fhandler_lrsrc.h"
static ldndc::io_fhandler_factory_t< ldndc::iobject_handler_lrsrc_t >  fhandler_lrsrc_factory;
#endif
#ifdef  LDNDC_IO_SQLITE3
#  include  "io/fhandler/sqlite3/fhandler_sqlite3.h"
static ldndc::io_fhandler_factory_t< ldndc::iobject_handler_sqlite3_t >  fhandler_sqlite3_factory;
#endif
#ifdef  LDNDC_IO_XML
#  include  "io/fhandler/xml/fhandler_xml.h"
static ldndc::io_fhandler_factory_t< ldndc::iobject_handler_xml_t >  fhandler_xml_factory;
#endif
#ifdef  LDNDC_IO_TXT
#  include  "io/fhandler/txt/fhandler_txt.h"
static ldndc::io_fhandler_factory_t< ldndc::iobject_handler_txt_t >  fhandler_txt_factory;
#endif

struct  fhandler_factory_item_t
{
    ldndc::io_fhandler_factory_base_t const *  factory;
    char const *  format;
};

static fhandler_factory_item_t const  __cbm_fhandler_factories[] =
{
#ifdef  LDNDC_IO_RESOURCES
    { &fhandler_lrsrc_factory, "lrsrc" },
#endif
#ifdef  LDNDC_IO_SQLITE3
    { &fhandler_sqlite3_factory, "sqlite3" },
#endif
#ifdef  LDNDC_IO_TXT
    { &fhandler_txt_factory, "txt" },
#endif
#ifdef  LDNDC_IO_XML
    { &fhandler_xml_factory, "xml" },
#endif

    { NULL, NULL} /* sentinel */
};



ldndc::io_fhandler_factory_base_t const *
ldndc::find_fhandler_factory( char const *  _format)
{
    if ( !_format)
        { return  NULL; }

    int  r = 0;
    while ( __cbm_fhandler_factories[r].factory)
    {
        if ( cbm::is_equal( __cbm_fhandler_factories[r].format, _format))
            { return  __cbm_fhandler_factories[r].factory; }
        ++r;
    }
    return  NULL;
}

