/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011),
 *    edwin haas
 */

#include  "source-handle-srv.h"
#include  "data-provider/provider.h"

#include  "cbm_rtcfg.h"
#include  "log/cbm_baselog.h"

ldndc::source_handle_srv_base_t::source_handle_srv_base_t(
        lid_t const &  _id)
        : cbm::server_object_t( _id)
{
}
ldndc::source_handle_srv_base_t::~source_handle_srv_base_t()
{
    /* no op */
}


void
ldndc::source_handle_srv_base_t::set_input_class_broker(
        ldndc::input_class_broker_t  _brkr)
{
    this->ic_brkr = _brkr;
}

cbm::source_descriptor_t const *
ldndc::source_handle_srv_base_t::get_source_descriptor()
const
{
    return &this->ic_brkr.sd;
}

ldndc::input_class_global_srv_base_t const *
ldndc::source_handle_srv_base_t::get_input_class_global(
        char const *  _type)
const
{
    if ( this->stream_id() != invalid_lid)
    {
        return  this->ic_brkr.get_input_class_global( this->stream_id(), _type);
    }
    CBM_LogWarn( "invalid stream ID");
    return  static_cast< input_class_global_srv_base_t const * >( NULL);
}

#include  "prjfile/cbm_prjfile.h"
ldndc::project_file_t const *
ldndc::source_handle_srv_base_t::get_project_file()
const
{
    return  CBM_LibRuntimeConfig.prj;
}

ldndc::source_attributes_t const *
ldndc::source_handle_srv_base_t::get_stream_attributes(
        char const *  _type)
const
{
    if ( CBM_LibRuntimeConfig.prj)
    {
        return  CBM_LibRuntimeConfig.prj->stream_attributes( _type);
    }
    return  NULL;
}

ltime_t const &
ldndc::source_handle_srv_base_t::get_reference_time()
const
{
    return  CBM_LibRuntimeConfig.clk->schedule();
}


LDNDC_SERVER_OBJECT_DEFN(ldndc::source_handle_srv_t)

ldndc::source_handle_srv_t::source_handle_srv_t()
        : source_handle_srv_base_t( invalid_lid),
          m_dataprovider( NULL),
          m_landscapedataprovider( NULL),
          m_streamid( invalid_lid)
{
}
ldndc::source_handle_srv_t::source_handle_srv_t(
        lid_t const &  _id)
        : source_handle_srv_base_t( _id),
          m_dataprovider( NULL),
          m_landscapedataprovider( NULL),
          m_streamid( invalid_lid)
{
}

ldndc::source_handle_srv_t::~source_handle_srv_t()
{
    /* no op */
}

void
ldndc::source_handle_srv_t::release_provider()
{
    if ( !this->m_landscapedataprovider)
    {
        crabmeat_assert( !this->m_dataprovider);
        return;
    }
    lid_t const  p_id = this->m_dataprovider->object_id();
    this->m_landscapedataprovider->release_provider( p_id);
}
lerr_t
ldndc::source_handle_srv_t::set_provider(
        iproc_provider_t *  _provider,
        iproc_landscape_provider_t *  _lprovider)
{
    if ( !_provider || !_lprovider)
    {
        CBM_LogError( "source_handle_srv_t::set_provider(): ",
                "providers are null");
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    /* no reset */
    if ( !this->m_dataprovider)
    {
        if (( this->object_id() == invalid_lid)
                || ( _provider->object_id() != this->object_id()))
        {
            CBM_LogError( "source_handle_srv_t::set_provider(): object IDs not matching or invalid",
                "  [expected=",this->object_id(),",got=",_provider->object_id(),"]");
            return  LDNDC_ERR_FAIL;
        }
        this->m_dataprovider = _provider;
    }
    if ( !this->m_landscapedataprovider)
    {
        this->m_landscapedataprovider = _lprovider;
    }
    return  LDNDC_ERR_OK;
}

void
ldndc::source_handle_srv_t::set_stream_id(
        lid_t const &  _stream_id)
{
    this->m_streamid = _stream_id;
}




LDNDC_SERVER_OBJECT_DEFN(ldndc::source_handle_noreader_srv_t)

ldndc::source_handle_noreader_srv_t::source_handle_noreader_srv_t()
        : ldndc::source_handle_srv_base_t( invalid_lid)
{
}
ldndc::source_handle_noreader_srv_t::source_handle_noreader_srv_t(
        lid_t const &  _id)
        : ldndc::source_handle_srv_base_t( _id)
{
    crabmeat_assert( _id != invalid_lid);
}

ldndc::source_handle_noreader_srv_t::~source_handle_noreader_srv_t()
{
    /* no op */
}

ldndc::input_class_srv_base_t const *
ldndc::source_handle_noreader_srv_t::get_input_class(
        char const *  _type)
const
{
    return  this->ic_brkr.get_input_class_indirect( _type);
}

