/*!
 * @brief
 *    abstract base classes for input classes
 *    (those read only input stream containers)
 *
 * @author
 *    steffen klatt (created on: Jul 14, 2011)
 */


#include  "input/ic-srv.h"

/***  input class  ***/

ldndc::input_class_srv_base_t::input_class_srv_base_t( lid_t const &  _id)
    : input_class_base_t(), cbm::server_object_t( _id)
{ }

ldndc::input_class_srv_base_t::~input_class_srv_base_t()
{ }

