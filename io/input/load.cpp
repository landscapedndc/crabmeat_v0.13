/*!
 * @brief
 *    class that implements the (re)load logic used with
 *    stream input data.
 *
 * @author
 *    steffen klatt (created on: apr 13, 2012),
 *    edwin haas
 */

#include  "input/load.h"

#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"


ldndc::load_status_t::load_status_t(
        bool  _endless)
{
    this->reset( _endless);
}

ldndc::load_status_t::~load_status_t()
{
    /* no op */
}


bool
ldndc::load_status_t::endless()
const
{
    return  this->m.endless;
}

void
ldndc::load_status_t::set(
        update_type *  _reg)
const
{
    crabmeat_assert( _reg);

    _reg->retry = 0;
    _reg->replicate = 0;

    _reg->rounds = 0;
}

void
ldndc::load_status_t::set_state(
        load_state_e  _state)
{
    this->m.state = _state;
    if ( _state == LOAD_STATE_REWIND)
    {
        this->m.endless = 1;
    }
}


lerr_t
ldndc::load_status_t::update(
        update_type *  _reg,
        size_t  _read_size)
{
    crabmeat_assert( _reg);

    lerr_t  rc_update = LDNDC_ERR_OK;
    _reg->rounds += 1;

    switch ( this->m.state)
    {
        case LOAD_STATE_NONE:

            rc_update = LDNDC_ERR_RUNTIME_ERROR;

            CBM_LogFatal( "[BUG]  go away!");
            break;

        case LOAD_STATE_RESET:

            if ( this->m.fit)
            {
                _reg->replicate = 1;
            }
            else
            {
                /* no op */
            }

            this->m.state = LOAD_STATE_LOAD;
            _reg->retry = 1;
            break;

        case LOAD_STATE_LOAD:

            if ( this->m.endless)
            {
                if ( _read_size == 0)
                {
                    if ( _reg->rounds > 1)
                    {
                        CBM_LogError( "load status: second round no data :-(");
                        this->m.state = LOAD_STATE_ERROR;
                        rc_update = LDNDC_ERR_INPUT_EXHAUSTED;
                    }
                    else if ( this->m.cyc == 1)
                    {
                        this->m.state = LOAD_STATE_RESET;
                        this->m.fit = 1;
                    }
                    else
                    {
                        this->m.state = LOAD_STATE_RESET;
                    }
                    _reg->retry = 1;
                }
                else
                {
                    if ( this->m.cyc < 2)
                    {
                        this->m.cyc += 1;
                    }
                    if ( this->m.fit)
                    {
                        this->m.state = LOAD_STATE_REWIND;
                    }
                    /* stay here */
                }
            }
            else /* not endless */
            {
                if ( _read_size == 0)
                {
                    this->m.state = LOAD_STATE_ERROR;
                    rc_update = LDNDC_ERR_INPUT_EXHAUSTED;
                }
            }

            break;

        case LOAD_STATE_REWIND:

            crabmeat_assert( this->m.endless);

            this->m.fit = 0;
            break;

        case LOAD_STATE_ERROR:
            rc_update = LDNDC_ERR_FAIL;
            break;

        default:
            CBM_LogFatal( "[BUG]  did you add a state to the machine without handling it here?!");
            break;
    }

    return  rc_update;
}

void
ldndc::load_status_t::reset(
        bool  _endless)
{
    this->m.endless = _endless;
    this->m.fit = 0;
    this->m.cyc = 0;

    this->m.state = LOAD_STATE_RESET;
}

