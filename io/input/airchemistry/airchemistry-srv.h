/*!
 * @brief
 *    high level access structure to air chemistry
 *    input streams
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_AIRCHEMISTRY_SERVER_H_
#define  LDNDC_INPUT_AIRCHEMISTRY_SERVER_H_

#include  "crabmeat-common.h"

#include  "input/airchemistry/airchemistrytypes-srv.h"
#include  "input/icbase-streamdata.h"

namespace ldndc {
class  input_source_srv_base_t;
class  source_handle_srv_base_t;

class  iproc_interface_airchemistry_t;

namespace  airchemistry
{

#define  AIRCHEMISTRY_RECORD_ITEM_SRV(__rec_item__,__offs__)  __LDNDC_STREAM_INPUT_BUFFER_ITEM(__rec_item__,RECORD_ITEM_##__offs__)

class  CBM_API  input_class_airchemistry_srv_t  :  public  iclassbase_streamdata_t< airchemistry_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_AIRCHEM, record::RECORD_SIZE >, public  record
{
    LDNDC_INPUT_SRV_CLASS_DECL(airchemistry,ICLASS_FLAG_STATE)
    public:
        char const **  record_item_names()
        const
        {
            return  RECORD_ITEM_NAMES;
        }

        data_filter_t< record::item_type > const **  datafilters()
        const
        {
            return  AIRCHEMISTRY_DATAFILTER_LIST;
        }

    public:
        ltime_t const &  get_time() const;

        void  get_record(
                         record::item_type *  /*buffer*/,
                         size_t  /*timestep*/) const;

    public:
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_ch4,CH4)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_co2,CO2)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_nh3,NH3)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_nh4,NH4)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_nh4dry,NH4_DRY)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_no,NO)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_no2,NO2)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_no3,NO3)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_no3dry,NO3_DRY)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_o2,O2)
        AIRCHEMISTRY_RECORD_ITEM_SRV(conc_o3,O3)

    public:
        airchemistry_info_t const *  info() const;
    private:
        airchemistry_info_t  info_;
        lerr_t  read_info_(
                           ldndc::iproc_interface_airchemistry_t const *);
};
}}

#include  "time/cbm_time.h"
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  airchemistry
{
class  CBM_API  input_class_airchemistry_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(airchemistry,ICLASS_FLAG_NONE)
    public:
        virtual  bool  has_time()
            const { return  true; }

        ltime_t const &  get_time()
            const { return  otime_; }

    private:
        /* time offset for data file ( subday offset, day offset, year offset) */
        ltime_t  otime_;
};
}}

#endif  /*  !LDNDC_INPUT_AIRCHEMISTRY_SERVER_H_  */

