/*!
 * @brief
 *    declare server types related to air chemistry input
 *
 * @author
 *    steffen klatt (created on: aug 13, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_AIRCHEMISTRY_TYPES_SERVER_H_
#define  LDNDC_INPUT_AIRCHEMISTRY_TYPES_SERVER_H_

#include  "airchemistry/airchemistrytypes.h"

namespace  ldndc{ namespace  airchemistry
{
struct  airchemistry_streamdata_info_t  :  public  streamdata_info_t
{
    struct  header_data_t
    {
        /* buffer's data resolution */
        int  res;
    };
};
}}

#endif  /*  !LDNDC_INPUT_AIRCHEMISTRY_TYPES_SERVER_H_  */


