/*!
 * @brief
 *    holds driver input for airchemistry
 *
 * @author
 *    steffen klatt,
 *    edwin haas
 */

#include  "input/airchemistry/airchemistry-srv.h"

#include  "time/cbm_time.h"

#include  "io/iif/iif_airchemistry.h"
#include  "io/source-handle-srv.h"
#include  "io/source-attrs.h"

#include  "log/cbm_baselog.h"

namespace  ldndc{ namespace  airchemistry
{
LDNDC_INPUT_SRV_CLASS_DEFN(airchemistry)
input_class_airchemistry_srv_t::input_class_airchemistry_srv_t()
        : iclassbase_streamdata_t< airchemistry_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_AIRCHEM, record::RECORD_SIZE >( invalid_lid),
          record()
{
}
input_class_airchemistry_srv_t::input_class_airchemistry_srv_t(
        lid_t const &  _id)
        : iclassbase_streamdata_t< airchemistry_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_AIRCHEM, record::RECORD_SIZE >( _id),
          record()
{
}


input_class_airchemistry_srv_t::~input_class_airchemistry_srv_t()
{
}


lerr_t
input_class_airchemistry_srv_t::initialize(
        ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    set_input_class_reader(airchemistry);
    set_input_class_global(airchemistry);

    /* read data from data stream */
    this->read_info_( this_reader);

    /* get simulation time if available, use input stream time instead */
    ltime_t  sim_time( _source_handle->get_reference_time());
    /* input stream start date filled with simulation time where missing */
    ltime_t  data_time( this_airchemistry_global->get_time());

    /* do we wish the input to restart at top */
    bool  endless(( _source_handle->get_stream_attributes( this->input_class_type())) ? 
                stream_attribute_endless( _source_handle->get_stream_attributes( this->input_class_type())) : false);

    lerr_t  rc_init_base = this->initialize_base( sim_time, data_time, this_reader, endless, &airchemistry_info_defaults);
    if ( rc_init_base)
    {
        CBM_LogError( "initializing stream-data base failed");
        return  init_error_( rc_init_base);
    }


    set_initialized();
    return  LDNDC_ERR_OK;
}


airchemistry_info_t const *
input_class_airchemistry_srv_t::info()
const
{
    return  &this->info_;
}
lerr_t
input_class_airchemistry_srv_t::read_info_(
        ldndc::iproc_interface_airchemistry_t const *  _reader)
{
    this->info_ = airchemistry_info_defaults;

    this->info_.ch4 = _reader->get_average_ch4();
    this->info_.co2 = _reader->get_average_co2();
    this->info_.nh3 = _reader->get_average_nh3();
    this->info_.nh4 = _reader->get_average_nh4();
    this->info_.nh4dry = _reader->get_average_nh4dry();
    this->info_.no = _reader->get_average_no();
    this->info_.no2 = _reader->get_average_no2();
    this->info_.no3 = _reader->get_average_no3();
    this->info_.no3dry = _reader->get_average_no3dry();
    this->info_.o2 = _reader->get_average_o2();
    this->info_.o3 = _reader->get_average_o3();

    return  LDNDC_ERR_OK;
}

ltime_t const &
input_class_airchemistry_srv_t::get_time()
const
{
    return  this->streamdata_time;
}

/* TODO  make generic */
void
input_class_airchemistry_srv_t::get_record(
        ldndc::airchemistry::record::item_type *  /*_c_rec*/,
        size_t  /*_t*/)
const
{
// sk:off    crabmeat_assert( _c_rec);
    CBM_LogFatal( "todo");

// sk:off     _c_rec[RECORD_ITEM_N] = conc_n( _t);
// sk:off     _c_rec[RECORD_ITEM_NH4] = conc_nh4( _t);
// sk:off     _c_rec[RECORD_ITEM_CH4] = conc_ch4( _t);
// sk:off         _c_rec[RECORD_ITEM_CO2] = conc_co2( _t);
// sk:off         _c_rec[RECORD_ITEM_O3] = conc_o3( _t);
// sk:off         _c_rec[RECORD_ITEM_NH3] = conc_nh3( _t);
// sk:off         _c_rec[RECORD_ITEM_NO] = conc_no( _t);
// sk:off         _c_rec[RECORD_ITEM_NO2] = conc_no2( _t);
// sk:off     _c_rec[RECORD_ITEM_NO3] = conc_no3( _t);
}
}}



#include  "io/input-source.h"
#include  "string/lstring-compare.h"
namespace  ldndc{ namespace  airchemistry
{
LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(airchemistry)
lerr_t
input_class_airchemistry_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *  _input_source,
        ltime_t const *  _time)
{
    if ( is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    set_input_global_class_reader(airchemistry);

    /* read datas time offset and resolution */
    if ( cbm::is_empty( this_reader->time()))
    {
        /* dates are set to default values and considered "not given" */
        if ( _time)
        {
            otime_ = *_time;
        }
    }
    else
    {
        if ( otime_.from_string( this_reader->time().c_str(), _time) != LDNDC_ERR_OK)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
    CBM_LogDebug( "time-rdr=", this_reader->time(), ", time-ic=", otime_.to_string(), ", time-sim=", (_time) ? _time->to_string(): "-");


    set_initialized();

    return  LDNDC_ERR_OK;
}
}}

