/*!
 * @brief
 *    factory for server input classes (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 07, 2013)
 */

#include  "input/ic-factory-srv.h"


#include  "input/airchemistry/airchemistry-srv.h"
static ldndc::input_class_factory_t< ldndc::airchemistry::input_class_airchemistry_srv_t,
        ldndc::airchemistry::input_class_airchemistry_global_srv_t >  input_airchemistry_factory;
#include  "input/anyclass/anyclass-srv.h"
static ldndc::input_class_factory_t< ldndc::anyclass::input_class_anyclass_srv_t,
        ldndc::anyclass::input_class_anyclass_global_srv_t >  input_anyclass_factory;
#include  "input/checkpoint/checkpoint-srv.h"
static ldndc::input_class_factory_t< ldndc::checkpoint::input_class_checkpoint_srv_t,
        ldndc::checkpoint::input_class_checkpoint_global_srv_t >  input_checkpoint_factory;
#include  "input/climate/climate-srv.h"
static ldndc::input_class_factory_t< ldndc::climate::input_class_climate_srv_t,
        ldndc::climate::input_class_climate_global_srv_t >  input_climate_factory;
#include  "input/event/event-srv.h"
static ldndc::input_class_factory_t< ldndc::event::input_class_event_srv_t,
        ldndc::event::input_class_event_global_srv_t >  input_event_factory;
#include  "input/geometry/geometry-srv.h"
static ldndc::input_class_factory_t< ldndc::geometry::input_class_geometry_srv_t,
        ldndc::geometry::input_class_geometry_global_srv_t >  input_geometry_factory;
#include  "input/groundwater/groundwater-srv.h"
static ldndc::input_class_factory_t< ldndc::groundwater::input_class_groundwater_srv_t,
        ldndc::groundwater::input_class_groundwater_global_srv_t >  input_groundwater_factory;
#include  "input/setup/setup-srv.h"
static ldndc::input_class_factory_t< ldndc::setup::input_class_setup_srv_t,
        ldndc::setup::input_class_setup_global_srv_t >  input_setup_factory;
#include  "input/siteparameters/siteparameters-srv.h"
static ldndc::input_class_factory_t< ldndc::siteparameters::input_class_siteparameters_srv_t,
        ldndc::siteparameters::input_class_siteparameters_global_srv_t >  input_siteparameters_factory;
#include  "input/site/site-srv.h"
static ldndc::input_class_factory_t< ldndc::site::input_class_site_srv_t,
        ldndc::site::input_class_site_global_srv_t >  input_site_factory;
#include  "input/soillayers/soillayers-srv.h"
static ldndc::input_class_factory_t< ldndc::soillayers::input_class_soillayers_srv_t,
        ldndc::soillayers::input_class_soillayers_global_srv_t >  input_soillayers_factory;
#include  "input/soilparameters/soilparameters-srv.h"
static ldndc::input_class_factory_t< ldndc::soilparameters::input_class_soilparameters_srv_t,
        ldndc::soilparameters::input_class_soilparameters_global_srv_t >  input_soilparameters_factory;
#include  "input/speciesparameters/speciesparameters-srv.h"
static ldndc::input_class_factory_t< ldndc::speciesparameters::input_class_speciesparameters_srv_t,
        ldndc::speciesparameters::input_class_speciesparameters_global_srv_t >  input_speciesparameters_factory;
#include  "input/species/species-srv.h"
static ldndc::input_class_factory_t< ldndc::species::input_class_species_srv_t,
        ldndc::species::input_class_species_global_srv_t >  input_species_factory;

struct  __cbm_input_factory_item_t
{
    ldndc::input_class_factory_base_t< ldndc::input_class_srv_base_t,
        ldndc::input_class_global_srv_base_t > const *  factory;
    char const **  dependencies; /*AIC==NULL, DIC!=NULL */
    char const *  role;
};

static char const *  __cbm_dic_dependency_table_soillayers[3] =
    { /*soillayers*/ "site",  "soilparameters",    NULL };
static char const *  __cbm_dic_dependency_table_species[3] =
    { /*species*/    "event", "speciesparameters", NULL };

static  __cbm_input_factory_item_t  __cbm_input_factories[] =
{
    { &input_airchemistry_factory, NULL, "airchemistry" },
    { &input_anyclass_factory, NULL, "anyclass" },
    { &input_checkpoint_factory, NULL, "checkpoint" },
    { &input_climate_factory, NULL, "climate" },
    { &input_event_factory, NULL, "event" },
    { &input_geometry_factory, NULL, "geometry" },
    { &input_groundwater_factory, NULL, "groundwater" },
    { &input_setup_factory, NULL, "setup" },
    { &input_site_factory, NULL, "site" },
    { &input_siteparameters_factory, NULL, "siteparameters" },
    { &input_soilparameters_factory, NULL, "soilparameters" },
    { &input_speciesparameters_factory, NULL, "speciesparameters" },

    { &input_soillayers_factory, __cbm_dic_dependency_table_soillayers, "soillayers" },
    { &input_species_factory, __cbm_dic_dependency_table_species, "species" },

    { NULL, NULL, NULL } /* sentinel */
};

#include  "string/cbm_string.h"
ldndc::input_class_factory_base_t< ldndc::input_class_srv_base_t,
        ldndc::input_class_global_srv_base_t > const *
ldndc::find_input_class_srv_factory( char  _kind,
        char const *  _role, char const ***  _dependencies)
{
    if (( _kind!='C' && _kind!='L') || !_role)
        { return  NULL; }

    int  r = 0;
    while ( __cbm_input_factories[r].factory)
    {
        if ( cbm::is_equal( __cbm_input_factories[r].role, _role))
        {
            if ( _dependencies)
                { *_dependencies = __cbm_input_factories[r].dependencies; }
            return __cbm_input_factories[r].factory;
        }
        ++r;
    }
    return  NULL;
}

