/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 20, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_ANYCLASS_SERVER_H_
#define  LDNDC_INPUT_ANYCLASS_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"

#include  "io/source-handle-srv.h"

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;

    struct  sink_entity_layout_t;

    class  iproc_interface_anyclass_t;
namespace  anyclass
{

class  CBM_API  input_class_anyclass_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(anyclass,ICLASS_FLAG_STATE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update_internal_state(
                cbm::sclock_t const &);

    public:
        int  retrieve_data_record(
                void ** /*buffer*/,
                sink_entity_layout_t const * /*data meta description*/) const;
    private:
        ldndc::source_handle_srv_t  sh;

        lerr_t  update();
};
}}

/* nothing here */
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  anyclass
{
class  CBM_API  input_class_anyclass_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(anyclass,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_ANYCLASS_SERVER_H_  */

