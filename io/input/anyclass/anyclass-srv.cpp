/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 20, 2014),
 *    edwin haas
 */

#include  "input/anyclass/anyclass-srv.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_anyclass.h"

#include  "log/cbm_baselog.h"


namespace  ldndc{ namespace  anyclass
{
LDNDC_INPUT_SRV_CLASS_DEFN(anyclass)
input_class_anyclass_srv_t::input_class_anyclass_srv_t()
        : input_class_srv_base_t( invalid_lid)
{
}
input_class_anyclass_srv_t::input_class_anyclass_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id)
{
}


input_class_anyclass_srv_t::~input_class_anyclass_srv_t()
{
}

lerr_t
input_class_anyclass_srv_t::initialize(
                ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
           {
        return  LDNDC_ERR_OK;
    }
    CBM_Assert( _source_handle);
    set_input_class_reader(anyclass);

    this->sh = *static_cast< ldndc::source_handle_srv_t const * >( _source_handle);
    this->update();

    this->set_initialized();
    return  LDNDC_ERR_OK;
}

lerr_t
input_class_anyclass_srv_t::update_internal_state(
        cbm::sclock_t const &)
{
    return  this->update();
}
lerr_t
input_class_anyclass_srv_t::update()
{
    return  LDNDC_ERR_OK;
}

int
input_class_anyclass_srv_t::retrieve_data_record(
        void **  _data, sink_entity_layout_t const *  _meta)
const
{
    set_input_class_reader_from_handle(anyclass,&this->sh);
    if ( dynamic_cast< iproc_provider_t * >( this_reader)->data_available())
    {
        return  this_reader->read_by_keys( _data, _meta);
    }
    return  0;
}


LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(anyclass)
lerr_t
input_class_anyclass_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *,
        ltime_t const *)
{
    /* guard */
    if ( this->is_initialized())
        { return  LDNDC_ERR_OK; } 
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

