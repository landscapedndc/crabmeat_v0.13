/*!
 * @brief
 *    holds main controls for kernel
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas
 */

#include  "input/setup/setup-srv.h"

#include  "time/cbm_time.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_setup.h"

#include  "string/cbm_string.h"
#include  "utils/cbm_utils.h"
#include  "log/cbm_baselog.h"

#include  "geom/geom.h"

#include  "prjfile/cbm_prjfile.h"

namespace  ldndc{ namespace  setup
{


input_class_setup_blocks_srv_t::input_class_setup_blocks_srv_t()
{
    this->reset();
}
void
input_class_setup_blocks_srv_t::reset()
{
    this->blk.clear();
}
lerr_t
input_class_setup_blocks_srv_t::set(
        cbm::string_t const &  _cp, int  _sz)
{
    std::istringstream  is;
    std::stringbuf *  ib = is.rdbuf();
    ib->str( _cp.str());

    std::string  c_class;
    lid_t  c_id;
    std::string  c_source;

    int  n = 0;
    while ( n++ < _sz)
    {
        is >> c_class >> c_source >> c_id;
// sk:dbg        CBM_LogDebug( "class=",c_class, "   source=",c_source, "   id=",c_id);

        if ( c_source == invalid_string)
            { c_source = c_class; }

        cbm::source_descriptor_t  source_descriptor;
        cbm::set_source_descriptor(
                &source_descriptor, c_id, c_source.c_str());
        this->blk[c_class] = source_descriptor;
    }
    return  LDNDC_ERR_OK;
}

lid_t
input_class_setup_blocks_srv_t::retrieve(
        cbm::source_descriptor_t *  _source_info,
        char const *  _type, int *  _given)
const
{
    crabmeat_assert( _type);
    crabmeat_assert( cbm_is_aic(_type));

    lid_t  bid = invalid_lid;

    if ( this->blk.find( _type) != this->blk.end())
    {
        cbm::source_descriptor_t const  sd =
            this->blk.find( _type)->second;
        bid = this->resolve_aic_id( sd.block_id, _type);
        if ( _source_info)
        {
            cbm::set_source_descriptor( _source_info,
                bid, sd.source_identifier);
        }
        if ( _given)
            { *_given = 1; }
    }
    else
    {
        bid = this->resolve_aic_id( invalid_lid, _type);
        if ( _source_info)
        {
            cbm::set_source_descriptor(
                _source_info, bid, _type);
        }
        if ( _given)
           { *_given = 0; }
    }

    return  bid;
}

lid_t
input_class_setup_blocks_srv_t::resolve_aic_id(
        lid_t const &  _aic_id, char const *  _type) const
{
    if ( !cbm_is_aic(_type))
        { return  invalid_lid; }

    if ( _aic_id == invalid_lid)
    {
        lid_t  default_id = invalid_lid;
        if ( this->projectfile)
            { default_id = this->projectfile->default_id( _type); }
        if ( default_id != invalid_lid)
        {
            /* choose (global) default */
            return  default_id;
        }
        /* if no default was given either, fall back to kernel id */
        return  this->blk.find( "setup")->second.block_id;
    }
    /* if valid, use! */
    return  _aic_id;
}



LDNDC_INPUT_SRV_CLASS_DEFN(setup)
input_class_setup_srv_t::input_class_setup_srv_t()
        : input_class_srv_base_t( invalid_lid),
          m_options( 0),
          m_info( setup_info_defaults)
    { }

input_class_setup_srv_t::input_class_setup_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id),
          m_options( 0),
          m_info( setup_info_defaults)
    { }


input_class_setup_srv_t::~input_class_setup_srv_t()
    { }


lerr_t
input_class_setup_srv_t::initialize(
        ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    set_input_class_reader(setup);

    this->m_reader = this_reader;

    lerr_t  rc_consume = this->read_blocks_consume_( this_reader,
        _source_handle->get_project_file(), _source_handle->get_source_descriptor());
    RETURN_IF_NOT_OK(rc_consume);

    lerr_t  rc_produce = this->read_blocks_produce_( this_reader);
    RETURN_IF_NOT_OK(rc_produce);

    lerr_t  rc_kernel_settings = this->read_kernel_settings_( this_reader);
    RETURN_IF_NOT_OK(rc_kernel_settings);

    lerr_t  rc_options = this->read_options_( this_reader);
    RETURN_IF_NOT_OK(rc_options);

    lerr_t  rc_info = this->read_info_( this_reader);
    RETURN_IF_NOT_OK(rc_info);

    lerr_t  rc_neighbors = this->read_neighbors( this_reader);
    RETURN_IF_NOT_OK(rc_neighbors);

    lerr_t  rc_boundaries = this->read_mooreboundaries( this_reader);
    RETURN_IF_NOT_OK(rc_boundaries);

    /* mark as initialized */
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

lerr_t
input_class_setup_srv_t::section_data( char **  _buffer,
            size_t *  _buffer_sz, char const * _section) const
{
    if ( _buffer)
        { *_buffer = NULL; }
    if ( _buffer_sz)
        { *_buffer_sz = 0; }

    cbm::string_t  sectiondata;
    lerr_t  rc = this->m_reader->get_section( &sectiondata, _section);

    if ( rc && rc != LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
        { return  LDNDC_ERR_FAIL; }
    else if ( rc == LDNDC_ERR_ATTRIBUTE_NOT_FOUND)
        { /*section not found*/ }
    else /* rc == LDNDC_ERR_OK */
    {
        if ( _buffer_sz)
            { *_buffer_sz = sectiondata.length(); }
        if ( _buffer)
        {
            *_buffer = cbm::strdup( sectiondata.c_str()); 
            if ( !*_buffer)
                { return LDNDC_ERR_NOMEM; }
        }
    }
    return  LDNDC_ERR_OK;
}

lerr_t
input_class_setup_srv_t::read_blocks_produce_(
        iproc_interface_setup_t const *  _reader)
{
    this->m_produce.reset();

    if ( _reader)
    {
        cbm::string_t  produces;
        int  produces_sz;
        lerr_t const  rc_produce = _reader->blocks_produce(
                    produces, &produces_sz, NULL);
        if ( rc_produce)
            { return  LDNDC_ERR_FAIL; }
        lerr_t const  rc_set =
            this->m_produce.set( produces, produces_sz);
        if ( rc_set)
            { return  LDNDC_ERR_FAIL; }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
input_class_setup_srv_t::read_blocks_consume_(
        iproc_interface_setup_t const *  _reader,
               project_file_t const *  _projectfile,
        cbm::source_descriptor_t const *  _setup_descriptor)
{
    this->m_consume.reset();

    if ( _reader)
    {
        cbm::string_t  consumes;
        int  consumes_sz = 0;
        lerr_t const  rc_consume = _reader->blocks_consume(
                        consumes, &consumes_sz, NULL);
        if ( rc_consume)
            { return  LDNDC_ERR_FAIL; }
        lerr_t const  rc_set =
            this->m_consume.set( consumes, consumes_sz);
        if ( rc_set)
            { return  LDNDC_ERR_FAIL; }

        /* add own id, since it's not specified in this block (invalidate setups source entry) */
        crabmeat_assert( _setup_descriptor);
        this->m_consume.blk["setup"] = *_setup_descriptor;
        this->m_consume.projectfile = _projectfile;
    }
    return  LDNDC_ERR_OK;
}

lerr_t
input_class_setup_srv_t::read_options_(
        iproc_interface_setup_t const *  _reader)
{
    this->m_options = 0; /*clear options*/
    /* read active flag */
    {
        int const  c_opt( _reader->active_flag());
        if ( c_opt == 1)
            { this->m_options |= OPT_ACTIVE; }
        else if ( c_opt < 0)
            { return  LDNDC_ERR_FAIL; }
    }
    /* read dispatch-on-any flag */
    {
        int const  c_opt( _reader->dispatchonany_flag());
        if ( c_opt == 1)
            { this->m_options |= OPT_DISPATCH_ON_ANY; }
        else if ( c_opt < 0)
            { return  LDNDC_ERR_FAIL; }
    }
    /* read quiet flag */
    {
        int const  c_opt( _reader->quiet_flag());
        if ( c_opt == 1)
            { this->m_options |= OPT_QUIET; }
        else if ( c_opt < 0)
            { return  LDNDC_ERR_FAIL; }
    }

    /* read checkpoint-create flag */
    {
        int  c_opt( _reader->checkpointcreate_flag());
        if ( c_opt == 1)
            { this->m_options |= OPT_CHECKPOINT_CREATE; }
        else if ( c_opt < 0)
            { return  LDNDC_ERR_FAIL; }
    }

    /* read checkpoint-restore flag */
    {
        int  c_opt( _reader->checkpointrestore_flag());
        if ( c_opt == 1)
            { this->m_options |= OPT_CHECKPOINT_RESTORE; }
        else if ( c_opt < 0)
            { return  LDNDC_ERR_FAIL; }
    }

    return  LDNDC_ERR_OK;
}


setup_info_t const *
input_class_setup_srv_t::info()
const
{
    return  &this->m_info;
}
lerr_t
input_class_setup_srv_t::read_info_(
        iproc_interface_setup_t const *  _reader)
{
    this->m_info = setup_info_defaults;

    cbm::geom_coord_t  p;
    lerr_t  rc_p = _reader->get_centroid( &p);
    if ( rc_p)
        { return  LDNDC_ERR_FAIL; }
    this->m_info.x = p.x; this->m_info.y = p.y; this->m_info.z = p.z;
    if ( cbm::is_invalid( this->m_info.x))
        { this->m_info.x = setup_info_defaults.x; }
    if ( cbm::is_invalid( this->m_info.y))
        { this->m_info.y = setup_info_defaults.y; }
    if ( cbm::is_invalid( this->m_info.z))
        { this->m_info.z = setup_info_defaults.z; }

    cbm::geom_bounding_box_t  bbox;
    lerr_t  rc_bbox = _reader->get_boundingbox( &bbox);
    if ( rc_bbox)
    {
        CBM_LogError( "invalid bounding box", "  [bbox=",bbox.to_string(),"]");
        return  LDNDC_ERR_FAIL;
    }
    this->m_info.dx = bbox.lr.x - bbox.ul.x;
    this->m_info.dy = bbox.lr.y - bbox.ul.y;
    this->m_info.dz = bbox.ul.z - bbox.lr.z;

    this->m_info.canopylayers = _reader->get_canopylayers();
    if ( cbm::is_invalid( this->m_info.canopylayers))
        { this->m_info.canopylayers = setup_info_defaults.canopylayers; }
    this->m_info.soillayers = _reader->get_soillayers();
    if ( cbm::is_invalid( this->m_info.soillayers))
        { this->m_info.soillayers = setup_info_defaults.soillayers; }
    
    this->m_info.elevation = _reader->get_elevation();
    if ( cbm::is_invalid( this->m_info.elevation))
        { this->m_info.elevation = setup_info_defaults.elevation; }
    this->m_info.latitude = _reader->get_latitude();
    if ( cbm::is_invalid( this->m_info.latitude))
        { this->m_info.latitude = setup_info_defaults.latitude; }
    this->m_info.longitude = _reader->get_longitude();
    if ( cbm::is_invalid( this->m_info.longitude))
        { this->m_info.longitude = setup_info_defaults.longitude; }

    this->m_info.timezone = _reader->get_timezone();
    if ( cbm::is_invalid( this->m_info.timezone))
        { this->m_info.timezone = setup_info_defaults.timezone; }
    this->m_info.aspect = _reader->get_aspect();
    if ( cbm::is_invalid( this->m_info.aspect))
        { this->m_info.aspect = setup_info_defaults.aspect; }
    this->m_info.slope = _reader->get_slope();
    if ( cbm::is_invalid( this->m_info.slope))
        { this->m_info.slope = setup_info_defaults.slope; }

    this->m_info.area = _reader->get_area();
    if ( cbm::is_invalid( this->m_info.area))
        { this->m_info.area = setup_info_defaults.area; }
    this->m_info.volume = _reader->get_volume();
    if ( cbm::is_invalid( this->m_info.volume))
        { this->m_info.volume = setup_info_defaults.volume; }

    return  LDNDC_ERR_OK;
}
size_t
input_class_setup_srv_t::get_number_of_neighbors()
const
{
    return  this->neighbors.size();
}
cbm::neighbor_t const *
input_class_setup_srv_t::get_neighbors()
const
{
    return  ( this->neighbors.size() == 0) ? NULL : &this->neighbors[0];
}
lerr_t
input_class_setup_srv_t::read_neighbors(
        iproc_interface_setup_t const *  _reader)
{
    crabmeat_assert( _reader);
    size_t  n_neighbors = _reader->get_number_of_neighbors();
    lerr_t  rc_read_neighbors = LDNDC_ERR_OK;
    if ( cbm::is_valid( n_neighbors) && (n_neighbors > 0))
    {
        this->neighbors.resize( n_neighbors);
        rc_read_neighbors =
            _reader->get_neighbors( &this->neighbors[0], n_neighbors);
    }
    if ( rc_read_neighbors)
    {
        this->neighbors.resize( 0);
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

cbm::geo_orientations_t
input_class_setup_srv_t::get_mooreboundaries()
const
{
    return  this->mooreboundaries;
}
lerr_t
input_class_setup_srv_t::read_mooreboundaries(
        iproc_interface_setup_t const *  _reader)
{
    crabmeat_assert( _reader);
    this->mooreboundaries = _reader->get_mooreboundaries();
    if ( this->mooreboundaries == cbm::ORIENT_INVALID)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_setup_srv_t::read_kernel_settings_(
        iproc_interface_setup_t const *  _reader)
{
    this->m_sitename =
        cbm::format_expand( _reader->get_site_name());

    return  LDNDC_ERR_OK;
}


/***  SETUP GLOBAL  ***/
LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(setup)
lerr_t
input_class_setup_global_srv_t::initialize(
                ldndc::input_source_srv_base_t const *  /*_input_source*/,
        ltime_t const *  /*reference time*/)
{
    /* guard */
    if ( is_initialized())
        { return  LDNDC_ERR_OK; }
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

