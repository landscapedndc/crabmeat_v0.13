/*!
 * @brief
 *    ...
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_SETUP_SERVER_H_
#define  LDNDC_INPUT_SETUP_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"
#include  "setup/setuptypes.h"

#include  "time/cbm_time.h"
#include  "string/cbm_string.h"
#include  "geom/cbm_geom.h"

#include  <vector>
#include  <map>

#define OPT_ACTIVE             0x00000001 /*! mark kernel as active or inactive */
#define OPT_DISPATCH_ON_ANY    0x00000002 /*! dispatch kernel on all nodes */
#define OPT_QUIET              0x00000004 /*! suppress output */
#define OPT_CHECKPOINT_CREATE  0x00000008 /*! create checkpoint */
#define OPT_CHECKPOINT_RESTORE 0x00000010 /*! restore from checkpoint */

#include  <cstdlib>
#define CBM_FreeSectionData(kdata) free(kdata)

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;

    class  iproc_interface_setup_t;

    class  project_file_t;
namespace  setup
{

struct  CBM_API  input_class_setup_blocks_srv_t
{
    input_class_setup_blocks_srv_t();
    void  reset();
    lerr_t  set( cbm::string_t const & /*consume/produce*/, int /*size*/);

    lid_t  retrieve( cbm::source_descriptor_t * /*buffer*/,
            char const * /*type*/, int * /*given?*/) const;
    lid_t  resolve_aic_id( lid_t const &,
                    char const * /*type*/) const;

    std::map< std::string, cbm::source_descriptor_t >  blk;
    ldndc::project_file_t const *  projectfile;
};
class  CBM_API  input_class_setup_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(setup,ICLASS_FLAG_NONE)
    public:
        lerr_t  reset_internal_state()
            { return  LDNDC_ERR_OK; }

        lerr_t  update_internal_state( cbm::sclock_t const &)
            { return  LDNDC_ERR_OK; }

        /*!
         * @brief
         *    true if kernel is participating in simulation run,
         *    false otherwise.
         */
        bool  is_active() const
            { return  this->m_options & OPT_ACTIVE; }
        bool  is_quiet() const
            { return  this->m_options & OPT_QUIET; }
        bool  dispatch_on_any() const
            { return  this->m_options & OPT_DISPATCH_ON_ANY; }
        bool  checkpoint_create() const
            { return  this->m_options & OPT_CHECKPOINT_CREATE; }
        bool  checkpoint_restore() const
            { return  this->m_options & OPT_CHECKPOINT_RESTORE; }

        /*!
         * @brief
         *    return arbitrary site name
         */
        char const *  site_name() const
            { return  this->m_sitename.c_str(); }

        /* returns JSON formatted string containing
         * inputs in given section*/
        lerr_t  section_data( char ** /*buffer*/, size_t * /*buffer size*/,
                    char const * /*section*/) const;

        /*!
         * @brief
         *    writes source descriptor of input class
         *    of type @p type linked to from this site
         *    to given buffer. it returns input class's
         *    block id. if parameter @p _given is
         *    non-null, it is set to 1 if value was
         *    read form input and was valid, 0 otherwise
         */
        lid_t  block_consume( cbm::source_descriptor_t *  _buf,
                char const *  _type, int *  _given) const
            { return  this->m_consume.retrieve( _buf, _type, _given); }
        /*!
         * @brief
         *    writes source descriptor of block of type
         *    @p type written by this site to given
         *    buffer. it returns input class's block id.
         */
        lid_t  block_produce( cbm::source_descriptor_t *  _buf,
                char const *  _type, int *  _given) const
            { return  this->m_produce.retrieve( _buf, _type, _given); }

    private:
        /* this member holds all options */
        int  m_options;

        cbm::string_t  m_sitename;

        iproc_interface_setup_t const *  m_reader;

        /* holds descriptors of blocks it consumes */
        input_class_setup_blocks_srv_t  m_consume;
        lerr_t  read_blocks_consume_(
                iproc_interface_setup_t const *,
                project_file_t const *,
                cbm::source_descriptor_t const *);
        /* holds descriptors of blocks it produces */
        input_class_setup_blocks_srv_t  m_produce;
        lerr_t  read_blocks_produce_(
                iproc_interface_setup_t const *);

        lerr_t  read_options_(
                iproc_interface_setup_t const *);
        lerr_t  read_kernel_settings_(
                iproc_interface_setup_t const *);

    public:
        size_t  get_number_of_neighbors() const;
        cbm::neighbor_t const *  get_neighbors() const;
        cbm::geo_orientations_t  get_mooreboundaries() const;

        setup_info_t const *  info() const;
    private:
        std::vector< cbm::neighbor_t >  neighbors;
        lerr_t  read_neighbors(
                iproc_interface_setup_t const *);
        cbm::geo_orientations_t  mooreboundaries;
        lerr_t  read_mooreboundaries(
                iproc_interface_setup_t const *);
        setup_info_t  m_info;
        lerr_t  read_info_(
                iproc_interface_setup_t const *);
};
}}


#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  setup
{
class  CBM_API  input_class_setup_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(setup,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_SETUP_SERVER_H_  */

