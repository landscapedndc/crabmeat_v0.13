/*!
 * @brief
 *    class that implements the (re)load logic used with
 *    stream input data.
 *
 * @author
 *    steffen klatt (created on: apr 13, 2012),
 *    edwin haas
 *
 * @note
 *    somewhat semi-tightly coupled to stream data buffer
 */

#ifndef  LDNDC_LOAD_STREAM_H_
#define  LDNDC_LOAD_STREAM_H_

#include  "crabmeat-common.h"

#if  defined(_HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK) && defined(_HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK)
#  error  client and server side synthesizing are mutually exclusive: unset at least one!
#endif

/* convert to data resolution */
#define  convert_resolution_s2d(res) \
    if ( this->m_stride < 0) \
        { res /= -this->m_stride; } \
    else \
        { res *= this->m_stride; }

/* convert to simulation resolution */
#define  convert_resolution_d2s(res) \
    if ( this->m_stride < 0) \
        { res *= -this->m_stride; } \
    else \
        { res /= this->m_stride; }


namespace ldndc {
/*!
 * @brief
 *    controls reading stream input data. it supports
 *    wrapping around (endless mode) to restart from top
 *    of input data (honoring offset). note that there is
 *    no data buffer attached to this object. state depends
 *    on buffersize only (and endless mode).
 *
 * @note
 *    offset larger that input stream for endless mode
 *    is not supported.
 */
class  load_status_t
{
    public:
        /*!
         * the states of the DFA
         */
        enum  load_state_e
        {
            /*! dummy state, not part of state machine */
            LOAD_STATE_NONE        = 0u,

            /*! first load or load after end of stream in endless mode */
            LOAD_STATE_RESET,
            /*! first load or load in normal mode */
            LOAD_STATE_LOAD,
            /*! "rewind" input if stream fits into buffer (endless mode only) */
            LOAD_STATE_REWIND,

            /*! indicates error (e.g. end of stream) */
            LOAD_STATE_ERROR
        };

    public:
        struct  update_type
        {
            bool  replicate:1;
            bool  retry:1;

            int  rounds:6;
        };

    public:
        load_status_t(
                bool  /* endless mode */);

        ~load_status_t();


        /*!
         * @brief
         *    return if in endless mode
         */
        bool  endless() const;

        void  set(
                update_type *) const;

    public:
        /*!
         * @brief
         *    return current state
         */
        load_state_e  state() const { return  static_cast< load_state_e >( this->m.state); }
        /*!
         * @brief
         *    force current state (usually bad idea)
         */
        void  set_state( load_state_e);
        /*!
         * @brief
         *    update the internal state of the implemented DFA.
         */
        lerr_t  update(
                update_type * /*register to store update results*/,
                size_t  /*read records from stream*/);

        /*!
         * @brief
         *    reset internal logic to restart reading stream
         */
        void  reset( bool /*endless*/);

    private:
        struct
        {
            /* set, if in endless mode, false otherwise */
            bool  endless:1;

            /* set, if stream content fits buffer */
            bool  fit:1;
            /* indicating if more than one reload occured before reset */
            unsigned int  cyc:2;

            /* current state of reload logic */
            unsigned int  state:4;
        }  m;
};
}

#include  "containers/lmrstreamarray.h"
#include  "io/iif/iiftable.h"
#include  "time/cbm_time.h"

namespace ldndc {
template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
class  stream_data_buffer_t
{
    public:
        typedef  _CB  callback_type;

        typedef  typename _E::element_type  element_type;
        typedef  typename _E::element_type &  element_reference_type;
        typedef  typename _E::element_type const &  element_reference_const_type;
        typedef  typename _E::element_type *  element_pointer_type;
        typedef  typename _E::element_type const *  element_pointer_const_type;

        typedef  ldndc::multiresolution_stream_array_t< _E, callback_type, _REC_SIZE, _BUF_SIZE >  data_buffer_t;

        static size_t const  buffer_size_log2 = _BUF_SIZE;
        static size_t const  record_size = _REC_SIZE;
        
    public:
        stream_data_buffer_t(
                callback_type *,
                ldate_t  /*data time*/, ldate_t  /*simulation time*/,
                bool /*endless flag*/);

        ~stream_data_buffer_t();

        lerr_t  clear(
                element_type const & = ldndc::invalid_t< element_type >::value);

        bool  is_ok() const
            { return  ( this->m_isOk != 0); }

        int  sim_offset() const
            { return  this->m_simoffset; }
        int  data_offset() const
            { return  this->m_dataoffset; }
        int  data_stride() const
            { return  this->m_stride; }

        element_type const *  data_record(
                typename callback_type::argument_type const * /*callback argument*/) const;
        element_type *  data_record(
                typename callback_type::argument_type const * /*callback argument*/);

        element_type  data_item(
                typename callback_type::argument_type const * /*callback argument*/,
                size_t /*item offset*/);

        typename callback_type::return_type  reload(
                typename callback_type::argument_type * /*callback argument*/,
                iproc_interface_table_base_t< _E > * /*reader*/,
                char const * [] /*item names*/);

        lerr_t  try_trigger_refresh(
                typename callback_type::argument_type const * /*callback argument*/);

    private:
        data_buffer_t *  m_buf;
// sk:later        data_buffer_t  buf_lookahead_;
        ldndc::load_status_t  m_loadstatus;

        callback_type *  m_callback;

        /* time offsets (in units of corresponding time resolution) */
        int  m_dataoffset, m_simoffset;
        /* time resolutions */
        int  m_datares, m_simres;

        /* resolution stride ( effectively input_stream_resolution devided by ts_max)
         *
         * let s be time resolution in input data stream, t simulation run's resolution
         * e.g. s=24 (hourly data), t=6 runs in four hour increments
         *
         * three situations:
         *    1. s = k*t, k is (positive) integer
         *       -> good.
         *
         *    2. s > t but no (positive) integer k exists, such that  s = k*t
         *       -> input data does not match request, i.e. "invalid" stride k
         *       (interpolation?)
         *
         *    3. s < t
         *       -> not enough data
         *       (interpolation?)
         */
        int  m_stride;

        int  m_isOk;
    private:
        typename callback_type::return_type  m_reloadError();
};
}


template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::stream_data_buffer_t(
        callback_type *  _callback,
        ldate_t  _data_time, ldate_t  _sim_time,
        bool  _endless)
        : m_buf( NULL),
          m_loadstatus( _endless),
          m_callback( _callback),

          m_datares( _data_time.time_resolution()), m_simres( _sim_time.time_resolution()),
          m_stride( 0),
          m_isOk( 1)
{
    cbm::td_t  sd_diff = _sim_time - _data_time;
    this->m_dataoffset = sd_diff.td_sec / _data_time.dt();
    cbm::td_t  ds_diff = _data_time - _sim_time;
    this->m_simoffset = ds_diff.td_sec / _sim_time.dt();

    /* NOTE  currently, it is impossible to have arrays with resolutions between 1 and sim-res */
#ifdef  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK
    int  R_LO = 0;
    int  r_lo_d[2];
    int const *  r_lo = r_lo_d;
    int  R_HI = 0;
    int  r_hi_d[2];
    int const *  r_hi = r_hi_d;

    if ( this->m_simres == 1)
    {
        if ( this->m_simres == this->m_datares)
        {
            r_lo = NULL;
            r_hi = NULL;
        }
        else if ( this->m_simres > this->m_datares) /* ?? */
        {
            R_LO = 1;
            r_lo_d[0] = this->m_datares;
        }
        else if ( this->m_simres < this->m_datares)
        {
            R_HI = 1;
            r_hi_d[0] = this->m_datares;
        }
        else
        {
            /* what the .. */
            this->m_isOk = 0;
        }
    }
    else /* this->m_simres > 1 */
    {
        if ( this->m_simres == this->m_datares)
        {
            R_LO = 1;
            r_lo_d[0] = 1;
        }
        else if ( this->m_simres > this->m_datares)
        {
            R_LO = 1;
            if ( this->m_datares > 1)
            {
                R_LO = 2;
                r_lo_d[1] = 1;
            }
            r_lo_d[0] = this->m_datares;
        }
        else if ( this->m_simres < this->m_datares)
        {
            R_HI = 1;
            r_hi_d[0] = this->m_datares;
            R_LO = 1;
            r_lo_d[0] = 1;
        }
        else
        {
            /* what the .. */
            this->m_isOk = 0;
        }
    }
#else
    static int const  R_LO = 0;
    static int const *  r_lo = NULL;
    static int const  R_HI = 0;
    static int const *  r_hi = NULL;
#endif
    this->m_buf = new data_buffer_t( _callback, r_lo, R_LO, r_hi, R_HI,
            this->m_simres, ldndc::invalid_t< element_type >::value);

    if ( ! this->m_buf || ! this->m_buf->is_ok())
    {
        CBM_LogError( "failed to initialize stream data buffer");
        this->m_isOk = 0;
    }
    if ( this->m_simres != this->m_datares)
    {
        if (( _sim_time.subday() > 1) || ( _data_time.subday() > 1))
        {
            CBM_LogError( "due to technical reasons we require for different time resolutions",
                   " input data time and simulation time to start on day boundaries");
            CBM_LogError( "t(sim)=", _sim_time.to_string(), ", t(data)=", _data_time.to_string());
            this->m_isOk = 0;
        }
    }

    if ( this->m_simres > this->m_datares)    /* synthesize */
    {
#if  defined(_HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK) || defined(_HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK)
        CBM_LogVerbose( "data time resolution to small, running in synthesizer mode",
                "  [R(sim)=",this->m_simres, ",R(data)=",this->m_datares, "]");
// sk:TODO        if ( have_synth_)
        {
            this->m_stride = this->m_simres / this->m_datares;
            if (( this->m_stride * this->m_datares) != this->m_simres)
            {
                CBM_LogError( "simulation's time resolution must be",
                        " integer multiple of input data time resolution");
                this->m_isOk = 0;
            }
            this->m_stride = -this->m_stride;
        }
#else
        CBM_LogError( "data time resolution to small  [R(sim)=",this->m_simres, ",R(data)=",this->m_datares, "]");
        this->m_isOk = 0;
#endif
    }
    else    /* aggregate */
    {
        /* calculate stride for data stream */
        this->m_stride = this->m_datares / this->m_simres;
        if (( this->m_stride * this->m_simres) != this->m_datares)
        {
            CBM_LogError( "currently, input data time resolution must be",
                    " integer multiple of simulation's time resolution",
                    "  [R(sim)=",this->m_simres, ",R(data)=",this->m_datares, "]");
            this->m_isOk = 0;
        }
    }
    crabmeat_assert( this->m_stride != 0);

    if ( this->m_dataoffset < 0)
    {
#if  !defined(_HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK) && !defined(_HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK)
        CBM_LogError( "negative offset not supported (that means your input stream time starts later than simulation time)");
        this->m_isOk = 0;
#endif
        this->m_dataoffset = 0;
    }
    else if ( this->m_dataoffset > 0)
    {
        this->m_simoffset = 0;
    }
    else
    {
        /* no op */
    }

    CBM_LogDebug( "o(sim+data)=",sd_diff.td_sec,
        ", o(sim)=", this->m_simoffset, ", o(data)=", this->m_dataoffset,
        ", t(sim)=",_sim_time.to_string(), ", t(data)=",_data_time.to_string(),
            ", stride=",this->m_stride);
}


template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::~stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >()
{
    if ( this->m_buf)
    {
        delete  this->m_buf;
    }
}


template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
lerr_t
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::clear(
        element_type const &  _value)
{
    lerr_t  rc_buffer_clear = this->m_buf->clear( _value);
    if ( rc_buffer_clear)
    {
        return  LDNDC_ERR_FAIL;
    }
    this->m_loadstatus.reset( this->m_loadstatus.endless());

    return  LDNDC_ERR_OK;
}


template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
typename ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::element_type const *
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::data_record(
        typename callback_type::argument_type const *  _cb_arg)
const
{
    return  const_cast< stream_data_buffer_t<
        _CB, _E, _BUF_SIZE, _REC_SIZE > * >( this)->data_record( _cb_arg);
}

template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
typename ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::element_type *
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::data_record(
        typename callback_type::argument_type const *  _cb_arg)
{
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
    if ( static_cast< int >( _cb_arg->t) < this->m_simoffset)
    {
        return  NULL;
    }
#endif

    element_pointer_type  rec( (*this->m_buf)[_cb_arg]);
    return  rec;
}


template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
typename ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::element_type
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::data_item(
        typename callback_type::argument_type const *  _cb_arg,
        size_t  _record_offs)
{
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
    if ( static_cast< int >( _cb_arg->t) < this->m_simoffset)
    {
        return  ldndc::invalid_t< element_type >::value;
    }
#endif

    element_pointer_const_type  rec( (*this->m_buf)[_cb_arg]);
    if ( rec)
    {
        return  rec[_record_offs];
    }
    return  ldndc::invalid_t< element_type >::value;
}


template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
typename ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::callback_type::return_type
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::m_reloadError()
{
    return  -1;
}

template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
typename ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::callback_type::return_type
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::reload(
        typename callback_type::argument_type *  _cb_arg,
        iproc_interface_table_base_t< _E > *  _reader,
        char const *  _item_names[])
{
    if ( !_cb_arg)
        { return  -1; }

    int  r_read = 0;

    ldndc::load_status_t::update_type  update_reg;
    this->m_loadstatus.set( &update_reg);
    do
    {

        lerr_t  rc_load = LDNDC_ERR_OK;

        update_reg.retry = 0;
        switch ( this->m_loadstatus.state())
        {
            case ldndc::load_status_t::LOAD_STATE_ERROR:
            {
                rc_load = LDNDC_ERR_FAIL;
                break;
            }

            case ldndc::load_status_t::LOAD_STATE_RESET:
            {
                rc_load = _reader->reset();

                /* ok, starting from top */
                r_read = 0;
                break;
            }

            case ldndc::load_status_t::LOAD_STATE_LOAD:
            {
// sk:dbg                CBM_LogError( "0: n=",_cb_arg->n, "  t=",_cb_arg->t, "  o(sim)=",this->m_simoffset, " (?)", "  stride=",this->m_stride);
                /* select buffer to load to */
                convert_resolution_s2d(_cb_arg->n); /*n is required to be in data stream resolution */
                lerr_t  rc_target_buffer = this->m_buf->reload_constraints( _cb_arg, this->m_datares);
                if ( !_cb_arg->buf || _cb_arg->n == 0 || rc_target_buffer)
                {
                    CBM_LogError( "i asked for a buffer but did not get one :(");
                    return  this->m_reloadError();
                }

                int  offs = static_cast< int >( _cb_arg->t)-this->m_simoffset;
                convert_resolution_s2d(offs);

// sk:dbg                CBM_LogError( "1: n=",_cb_arg->n, "  t=",_cb_arg->t, "  o(sim)=",this->m_simoffset, " (",offs,")", "  stride=",this->m_stride);


                /* invalidate data records */
                this->m_buf->mem_set_all( ldndc::invalid_t< element_type >::value);
                /* read from stream */
                rc_load = _reader->read(
                        _cb_arg->buf, _cb_arg->n, _cb_arg->s,
                        _item_names, this->m_buf->record_size(),
                        offs /*offset*/, this->m_stride /*stride*/,
                        &r_read, NULL);

                if ( update_reg.replicate)
                {
                    update_reg.replicate = 0;
                    this->m_buf->request_replicate();
                }

                break;
            }

            case ldndc::load_status_t::LOAD_STATE_REWIND:
            {
                r_read = static_cast< int >( this->m_buf->current_fill_cnt());
// sk:dbg                CBM_LogDebug( "rewind:  read-size=",r_read);
                break;
            }

            default:
            {
                CBM_LogFatal( "[BUG]  it's likely we trashed your data.");
                break;
            }
        }

        if ( rc_load)
        {
            CBM_LogError( "errors occurred during stream data reload");
            return  this->m_reloadError();
        }

        /* update logic after reload */
        lerr_t  rc_status_update = this->m_loadstatus.update( &update_reg, r_read);
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
        if ( rc_status_update == LDNDC_ERR_INPUT_EXHAUSTED)
        {
            this->m_buf->mem_set_all( ldndc::invalid_t< element_type >::value);
            this->m_buf->reload_constraints( _cb_arg, this->m_datares, -1);

            this->m_loadstatus.set_state( ldndc::load_status_t::LOAD_STATE_REWIND);

            r_read = _cb_arg->n;
            rc_status_update = LDNDC_ERR_OK;
        }
        else
#elif  defined(_HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK)
        if ( rc_status_update == LDNDC_ERR_INPUT_EXHAUSTED)
        {
            this->m_loadstatus.set_state( ldndc::load_status_t::LOAD_STATE_LOAD);
            update_reg.retry = 1;
        }
        else
#endif
        if ( rc_status_update)
        {
            CBM_LogError( "errors occurred during stream data reload state update");
            return  this->m_reloadError();
        }
// sk:dbg        CBM_LogDebug( "rounds=",update_reg.rounds,"  retry=",update_reg.retry,"  replicate=",update_reg.replicate);

    } while ( update_reg.retry);

    return  r_read;
}

template < typename  _CB, typename  _E, size_t  _BUF_SIZE,  size_t  _REC_SIZE >
lerr_t
ldndc::stream_data_buffer_t< _CB, _E, _BUF_SIZE, _REC_SIZE >::try_trigger_refresh(
        typename callback_type::argument_type const *  _cb_arg)
{
#ifdef  _HAVE_INPUTS_CLIENT_SIDE_SYNTHESIZE_OK
    if ( static_cast< int >( _cb_arg->t) < this->m_simoffset)
    {
        return  LDNDC_ERR_OK;
    }
#endif
    return  this->m_buf->try_trigger_refresh( _cb_arg);
}


#endif  /*  !LDNDC_LOAD_STREAM_H_  */

