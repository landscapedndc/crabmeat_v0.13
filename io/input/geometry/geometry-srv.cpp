/*!
 * @author
 *    steffen klatt (created on: feb 03, 2014),
 *    edwin haas
 */

#include  "input/geometry/geometry-srv.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_geometry.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"


namespace  ldndc{ namespace  geometry
{

LDNDC_INPUT_SRV_CLASS_DEFN(geometry)
input_class_geometry_srv_t::input_class_geometry_srv_t()
        : input_class_srv_base_t( invalid_lid)

{
}
input_class_geometry_srv_t::input_class_geometry_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id)
{
}


input_class_geometry_srv_t::~input_class_geometry_srv_t()
{
    set_input_class_reader_from_handle(geometry,&this->src_handle_);
    this_reader->free_shape( &this->shape_);
}

lerr_t
input_class_geometry_srv_t::initialize(
                ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
           {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    this->src_handle_ = *static_cast< source_handle_srv_t const * >( _source_handle);

    lerr_t  rc_read_shape = this->read_shape_();
    if ( rc_read_shape)
    {
        return  LDNDC_ERR_FAIL;
    }

    this->set_initialized();
    return  LDNDC_ERR_OK;
}



lerr_t
input_class_geometry_srv_t::read_shape_()
{
    set_input_class_reader_from_handle(geometry,&this->src_handle_);
    lerr_t  rc_read = this_reader->read_shape( &this->shape_);
    if ( rc_read)
    {
        this_reader->free_shape( &this->shape_);
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}

size_t
input_class_geometry_srv_t::vertices_cnt()
const
{
    return  this->shape_.n_vertices;
}
lerr_t
input_class_geometry_srv_t::get_vertex(
        cbm::geom_coord_t *  _coords,
        size_t  _index)
const
{
    crabmeat_assert( _coords);
    if ( this->shape_.vertices && ( _index < this->shape_.n_vertices))
    {
        *_coords = this->shape_.vertices[_index];
    }
    return  LDNDC_ERR_FAIL;
}

lerr_t
input_class_geometry_srv_t::get_mapinfo(
        cbm::geom_mapinfo_t *  _mapinfo)
const
{
    crabmeat_assert(_mapinfo);
    set_input_class_reader_from_handle(geometry,&this->src_handle_);
    this_reader->get_mapinfo( _mapinfo);

    return  LDNDC_ERR_OK;
}





LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(geometry)
lerr_t
input_class_geometry_global_srv_t::initialize(
    ldndc::input_source_srv_base_t const *, ltime_t const *)
{
    /* guard */
    if ( this->is_initialized())
        { return  LDNDC_ERR_OK; }
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

