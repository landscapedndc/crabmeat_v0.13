/*!
 * @brief
 *    container for geometry specific input data
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_GEOMETRY_SERVER_H_
#define  LDNDC_INPUT_GEOMETRY_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"

#include  "geometry/geometrytypes.h"

#include  "io/source-handle-srv.h"

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;

    class  iproc_interface_geometry_t;
namespace  geometry
{

class  CBM_API  input_class_geometry_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(geometry,ICLASS_FLAG_NONE)
    public:
        lerr_t  reset_internal_state()
            { return  LDNDC_ERR_OK; }

        lerr_t  update_internal_state( cbm::sclock_t const &)
            { return  LDNDC_ERR_OK; }

        cbm::geom_shape_t const *  get_shape()
            const { return  &this->shape_; }

        size_t  vertices_cnt() const;
        lerr_t  get_vertex(
            cbm::geom_coord_t * /*buffer*/, size_t /*index*/) const;

        lerr_t  get_mapinfo( cbm::geom_mapinfo_t * /*buffer*/) const;

    private:
        cbm::geom_shape_t  shape_;
        ldndc::source_handle_srv_t  src_handle_;

        lerr_t  read_shape_();
};
}}

/* nothing here */
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  geometry
{
class  CBM_API  input_class_geometry_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(geometry,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_GEOMETRY_SERVER_H_  */

