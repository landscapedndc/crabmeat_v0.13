/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: apr 07, 2013)
 */


#ifndef  LDNDC_INPUT_CLASS_ENV_H_
#define  LDNDC_INPUT_CLASS_ENV_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

#define  cbm_is_ic(c) ((c!=NULL)&&(cbm::strnlen(c,1)>0))
#ifdef  CRABMEAT_DEBUG
#  define  cbm_is_aic(c) (cbm_is_ic(c)&&(ldndc::is_aic(c)==1))
#  define  cbm_is_dic(c) (cbm_is_ic(c)&&(ldndc::is_dic(c)==1))
#else
#  define  cbm_is_aic(c) (ldndc::is_aic(c)==1)
#  define  cbm_is_dic(c) (ldndc::is_dic(c)==1)
#endif


namespace ldndc {
enum
{
    /*! empty flag (dummy) */
    ICLASS_FLAG_NONE  = 0u,
    /*! indicates that input class has internal changing state */
    ICLASS_FLAG_STATE  = 1u << 0
};

/*  input class status flags  */
enum  iclass_status_flag_e
{
    /*! empty status (dummy) */
    ICLASS_STATUS_NONE  = 0u,
    /*! class has been successfully initialized */
    ICLASS_STATUS_INITIALIZED  = 1u << 0,
    /*! end-of-input reached */
    ICLASS_STATUS_EXHAUSTED  = 1u << 1,
    /*! read error */
    ICLASS_STATUS_ERROR  = 1u << 2
};

#define  LDNDC_INPUT_CLASS_DECL_(__class__,__iclassname__,__flags__) \
    LDNDC_OBJECT(__class__) \
    private: \
        static bool  has_internal_state_() \
            { return  0 != ( flags() & ICLASS_FLAG_STATE); } \
    public: \
        static char const *  iclassname() \
            { return #__iclassname__; } \
        char const *  input_class_type() const \
            { return  iclassname(); } \
        static  lflags_t  flags() \
            { return  (__flags__); } \
        bool  has_internal_state() const \
            { return  has_internal_state_(); } \
    private:

#define  LDNDC_INPUT_CLASS_DEFN_(__class__)  LDNDC_OBJECT_DEFN(__class__)


/*!
 * @brief
 *    (abstract) base class for input classes
 */
class  CBM_API  input_class_base_t
{
    public:
        virtual  ~input_class_base_t() = 0;

        virtual  char const *  input_class_type() const = 0;
        virtual  std::string  name() const;

        virtual  bool  is_initialized() const
            { return  ( status_ & (lflags_t)ICLASS_STATUS_INITIALIZED) != 0; }

        virtual  bool  is_exhausted() const
            { return  ( status_ & (lflags_t)ICLASS_STATUS_EXHAUSTED) != 0; }

        virtual  bool  is_ok() const
            { return  ( status_ & (lflags_t)ICLASS_STATUS_ERROR) == 0; }

    protected:
        input_class_base_t();

        /* set new status */
        lflags_t  set_status( lflags_t);
        /* clear status */
        void  unset_status();
        /* activate status flag */
        lflags_t  set_status_flag( iclass_status_flag_e);
        /* deactivate status flag */
        lflags_t  unset_status_flag( iclass_status_flag_e);

        /* shortcut */
        void  set_initialized();

    private:
        /* status indicator to catch double inits, init stages, etc... */
        lflags_t  status_;
};

typedef  ldndc_uint64_t  hash_ic_t;

/*!
 * @brief
 *    generate unique identifier for associated
 *    input class
 */
#define  HASH_SOURCE_BYTESIZE  ( CBM_STRING_SIZE + 2*sizeof(lid_t))
CBM_API
int  hash_ic( hash_ic_t * /*buffer*/,
        lid_t const & /*block id*/, char const * /*source identifier*/,
        char const * /*type*/, char * = NULL /*hashsource buffer*/);

/* quick'n dirty solution to find DIC's dependencies */
CBM_API
int  ic_dependencies( char const * /*type*/,
            char const *** /*type array*/);

CBM_API int  is_aic( char const * /*type*/);
CBM_API int  is_dic( char const * /*type*/);

template < typename  _I_base, typename  _G_base >
struct  input_class_factory_base_t
{
    input_class_factory_base_t();
    virtual  ~input_class_factory_base_t();

    virtual  char const *  class_type() const = 0;

    virtual  _I_base *  construct( lid_t const &) const = 0;
    virtual  void  destruct( _I_base const *) const = 0;

    virtual  _G_base *  construct_global( lid_t const &) const = 0;
    virtual  void  destruct_global( _G_base const *) const = 0;
};

template < typename  _I_base, typename  _G_base >
ldndc::input_class_factory_base_t< _I_base, _G_base >::input_class_factory_base_t()
    { }
template < typename  _I_base, typename  _G_base >
ldndc::input_class_factory_base_t< _I_base, _G_base >::~input_class_factory_base_t< _I_base, _G_base >()
    { }

template < typename _I, typename _G >
struct  input_class_factory_t
    :  input_class_factory_base_t< typename _I::base_t, typename _G::base_t >
{
    input_class_factory_t()
        : input_class_factory_base_t< typename _I::base_t, typename _G::base_t >()
        { }

    char const *  class_type() const
        { return  _I::iclassname(); }

    typename _I::base_t *  construct( lid_t const &  _id) const
        { return  static_cast< typename _I::base_t * >( _I::new_instance( _id)); }
    virtual  void  destruct( typename _I::base_t const *  _ic) const
    {
        if ( _ic)
            { static_cast< _I const * >( _ic)->delete_instance(); }
    }

    typename _G::base_t *  construct_global( lid_t const &  _id) const
        { return  static_cast< typename _G::base_t * >( _G::new_instance( _id)); }
    virtual  void  destruct_global( typename _G::base_t const *  _ic)
    const
    {
        if ( _ic)
            { static_cast< _G const * >( _ic)->delete_instance(); }
    }
};

} /* namespace ldndc */

#endif  /*  !LDNDC_INPUT_CLASS_ENV_H_  */

