/*!
 * @brief
 *    base class global input classes
 *
 * @author
 *    steffen klatt (created on: sep 13, 2011)
 */

#ifndef  LDNDC_INPUT_CLASS_GLOBAL_SRV_H_
#define  LDNDC_INPUT_CLASS_GLOBAL_SRV_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "input/ic-env.h"

namespace ldndc {
class  input_source_srv_base_t;

/* (abstract) base class for input class globals (representing global sections) */
class CBM_API  input_class_global_srv_base_t
        :  public  input_class_base_t,  public  cbm::server_object_t
{
    public:
        virtual  ~input_class_global_srv_base_t() = 0;

        /* do not keep reference to input object, it will be destroyed! */
        virtual  lerr_t  initialize(
            input_source_srv_base_t const *, ltime_t const *) = 0;

        virtual  bool  has_time() const
            { return  false; }

    protected:
        input_class_global_srv_base_t( lid_t const &);

    private:
        /* hide .. these need to be cleanly implemented */
        input_class_global_srv_base_t( input_class_global_srv_base_t const &);
        input_class_global_srv_base_t &  operator=( input_class_global_srv_base_t const &);
};
}

#define  LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(__class__,__flags__) \
    LDNDC_INPUT_CLASS_DECL_( TOKENPASTE3(input_class_,__class__,_global_srv_t), \
            __class__, __flags__) \
    public: \
        typedef input_class_global_srv_base_t base_t; \
        TOKENPASTE3(input_class_,__class__,_global_srv_t)(); \
        TOKENPASTE3(input_class_,__class__,_global_srv_t)( lid_t const &); \
        lerr_t  initialize( \
            ldndc::input_source_srv_base_t const *, ltime_t const *); \
    private:

#define  LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(__class__) \
    LDNDC_INPUT_CLASS_DEFN_(TOKENPASTE3(input_class_,__class__,_global_srv_t)) \
    /* currently no specialized constructors are needed .. so we add them here */ \
    TOKENPASTE3(input_class_,__class__,_global_srv_t)::TOKENPASTE3(input_class_,__class__,_global_srv_t)() \
            : input_class_global_srv_base_t( invalid_lid) { } \
    TOKENPASTE3(input_class_,__class__,_global_srv_t)::TOKENPASTE3(input_class_,__class__,_global_srv_t)( lid_t const &  _id) \
            : input_class_global_srv_base_t( _id) { }


#define  set_input_global_class_reader(__iclass__) \
    ldndc::__LDNDC_IPROC_LANDSCAPE_INTERFACE_CLASS(__iclass__) const *  this_reader = NULL; \
    { \
        crabmeat_assert( _input_source); \
        ldndc::input_source_srv_t const *  this_input_source = \
            static_cast< ldndc::input_source_srv_t const * >( _input_source); \
        this_reader = this_input_source->reader< ldndc::__LDNDC_IPROC_LANDSCAPE_INTERFACE_CLASS(__iclass__) >(); \
        crabmeat_assert( this_reader); \
    } \
    CRABMEAT_FIX_UNUSED(this_reader)/*;*/

#endif  /*  !LDNDC_INPUT_CLASS_GLOBAL_SRV_H_  */

