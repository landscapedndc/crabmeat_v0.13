/*!
 * @brief
 *    factories for server input classes
 *
 * @author
 *    steffen klatt (created on: apr 07, 2013)
 */
#ifndef  LDNDC_INPUT_CLASS_FACTORIES_SRV_H_
#define  LDNDC_INPUT_CLASS_FACTORIES_SRV_H_

#include  "crabmeat-common.h"

#include  "input/ic-srv.h"
#include  "input/ic-global-srv.h"
#include  "input/ic-env.h"

namespace ldndc {
    extern CBM_API ldndc::input_class_factory_base_t< input_class_srv_base_t, input_class_global_srv_base_t > const *
            find_input_class_srv_factory( char /*kind {'L', 'C'} */, char const * /*type*/,
                    char const *** = NULL /*dependencies*/);
}

#endif  /*  !LDNDC_INPUT_CLASS_FACTORIES_SRV_H_  */

