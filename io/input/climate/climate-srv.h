/*!
 * @brief
 *    high level access structure to climate
 *    input streams
 *
 * @author
 *    Steffen Klatt (created on: Jul 8, 2010),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_CLIMATE_SERVER_H_
#define  LDNDC_INPUT_CLIMATE_SERVER_H_

#include  "crabmeat-common.h"

#include  "input/climate/climatetypes-srv.h"
#include  "input/icbase-streamdata.h"

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;

    class  iproc_interface_climate_t;
namespace  climate
{

#define  CLIMATE_RECORD_ITEM_SRV(__rec_item__,__offs__)  __LDNDC_STREAM_INPUT_BUFFER_ITEM(__rec_item__,RECORD_ITEM_##__offs__)

class  CBM_API  input_class_climate_srv_t
    :  public  iclassbase_streamdata_t< climate_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_CLIMATE, record::RECORD_SIZE >,  public  record
{
    LDNDC_INPUT_SRV_CLASS_DECL(climate,ICLASS_FLAG_STATE)
    public:
        char const **  record_item_names() const
            { return  RECORD_ITEM_NAMES; }

    public:
        ltime_t const &  get_time() const;

        void  get_record(
                item_type *  /*buffer*/,
                cbm::sclock_t const &  /*clock*/) const;

    public:
        /*! air pressure */
        CLIMATE_RECORD_ITEM_SRV(air_pressure,AIR_PRESSURE)
        /*! extraterrestrial radiation */
        CLIMATE_RECORD_ITEM_SRV(ex_rad,EX_RAD)
        /*! longwave radiation */
        CLIMATE_RECORD_ITEM_SRV(long_rad,LONGWAVE_RAD)
        /*! precipitation */
        CLIMATE_RECORD_ITEM_SRV(precip,PRECIP)
        /*! average temperature */
        CLIMATE_RECORD_ITEM_SRV(temp_avg,TEMP_AVG)
        /*! maximum temperature */
        CLIMATE_RECORD_ITEM_SRV(temp_max,TEMP_MAX)
        /*! minimum temperature */
        CLIMATE_RECORD_ITEM_SRV(temp_min,TEMP_MIN)
        /*! relative humidity */
        CLIMATE_RECORD_ITEM_SRV(rel_humidity,REL_HUMUDITY)
        /*! vapor pressure deficit */
        CLIMATE_RECORD_ITEM_SRV(vpd,VPD)
        /*! wind speed */
        CLIMATE_RECORD_ITEM_SRV(wind_speed,WIND_SPEED)

    public:
        climate_info_t const *  info() const;
    private:
        climate_info_t  info_;
        lerr_t  read_info_(
                iproc_interface_climate_t const *);
};
}}


#include  "time/cbm_time.h"
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  climate
{
class  CBM_API  input_class_climate_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(climate,ICLASS_FLAG_NONE)
    public:
        virtual  bool  has_time() const
            { return  true; }

        ltime_t const &  get_time() const
            { return  otime_; }

    private:
        /* time offset for data file ( subday offset, day offset, year offset) */
        ltime_t  otime_;
};
}}

#endif  /*  !LDNDC_INPUT_CLIMATE_SERVER_H_  */

