/*!
 * @brief
 *    manages climate driver input
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas
 */

#include  "input/climate/climate-srv.h"

#include  "time/cbm_time.h"

#include  "io/iif/iif_climate.h"
#include  "io/source-handle-srv.h"
#include  "io/source-attrs.h"

#include  "log/cbm_baselog.h"

namespace  ldndc{ namespace  climate
{
LDNDC_INPUT_SRV_CLASS_DEFN(climate)
input_class_climate_srv_t::input_class_climate_srv_t()
        : ldndc::iclassbase_streamdata_t< climate_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_CLIMATE, ldndc::climate::record::RECORD_SIZE >( invalid_lid),
          ldndc::climate::record(),
          info_( climate_info_defaults)
{
}
input_class_climate_srv_t::input_class_climate_srv_t(
        lid_t const &  _id)
        : ldndc::iclassbase_streamdata_t< climate_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_CLIMATE, ldndc::climate::record::RECORD_SIZE >( _id),
          ldndc::climate::record(),
          info_( climate_info_defaults)
{
}


input_class_climate_srv_t::~input_class_climate_srv_t()
{ 
}


lerr_t
input_class_climate_srv_t::initialize(
        ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    set_input_class_reader(climate);
    set_input_class_global(climate);

    /* read data from data stream */
    this->read_info_( this_reader);

    /* get simulation time if available, use input stream time instead */
    ltime_t  sim_time( _source_handle->get_reference_time());
    /* input stream start date filled with simulation time where missing */
    ltime_t  data_time( this_climate_global->get_time());

    /* do we wish the input to restart at top */
    bool  endless((_source_handle->get_stream_attributes( this->input_class_type())) ?
                stream_attribute_endless( _source_handle->get_stream_attributes( this->input_class_type())) : false);

    lerr_t  rc_init_base = this->initialize_base( sim_time, data_time, this_reader, endless, &this->info_);
    if ( rc_init_base)
    {
        return  this->init_error_( rc_init_base);
    }


    this->set_initialized();
    return  LDNDC_ERR_OK;
}


climate_info_t const *
input_class_climate_srv_t::info()
const
{
    return  &this->info_;
}
lerr_t
input_class_climate_srv_t::read_info_(
        ldndc::iproc_interface_climate_t const *  _reader)
{
    this->info_ = climate_info_defaults;

    this->info_.latitude = _reader->get_latitude();
    this->info_.longitude = _reader->get_longitude();
    this->info_.elevation = _reader->get_elevation();

    this->info_.cloudiness = _reader->get_cloudiness();
    this->info_.rainfall_intensity = _reader->get_rainfall_intensity();
    this->info_.windspeed = _reader->get_wind_speed();

    this->info_.temp = _reader->get_temperature_average();
    this->info_.temp_amplitude = _reader->get_temperature_amplitude();
    this->info_.precip_sum = _reader->get_annual_precipitation();

    return  LDNDC_ERR_OK;
}

ltime_t const &
input_class_climate_srv_t::get_time()
const
{
    return  this->streamdata_time;
}

void
input_class_climate_srv_t::get_record(
        item_type *  _c_rec,
        cbm::sclock_t const &  _clk)
const
{
    int const  t = _clk.cycles();
    int const  R = _clk.time_resolution();

    element_type const *  c_rec = this->data_record( t, R);
    cbm::mem_cpy( _c_rec, c_rec, ::ldndc::climate::record::RECORD_SIZE);
}

}}



#include  "io/input-source.h"
#include  "string/lstring-compare.h"
namespace  ldndc{ namespace  climate
{
LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(climate)
lerr_t
input_class_climate_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *  _input_source,
        ltime_t const *  _time)
{
    if ( this->is_initialized())
        { return  LDNDC_ERR_OK; }

    set_input_global_class_reader(climate);

    /* read datas time offset and resolution */
    if ( cbm::is_empty( this_reader->time()))
    {
        CBM_LogDebug( "cl-time=",this_reader->time());
        /* dates are set to default values and considered "not given" */
        if ( _time)
        {
            this->otime_ = *_time;
        }
    }
    else
    {
        if ( this->otime_.from_string( this_reader->time().c_str(), _time) != LDNDC_ERR_OK)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
    CBM_LogDebug( "time-rdr=", this_reader->time(), ", time-ic=", otime_.to_string(), ", time-sim=", (_time) ? _time->to_string(): "-");


    this->set_initialized();

    return  LDNDC_ERR_OK;
}
}}

