
/*!
 * @brief
 *    declare server types related to climate input
 *
 * @author
 *    steffen klatt (created on: aug 13, 2012),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_CLIMATE_TYPES_SERVER_H_
#define  LDNDC_INPUT_CLIMATE_TYPES_SERVER_H_

#include  "climate/climatetypes.h"

namespace ldndc{ namespace  climate
{
struct  climate_streamdata_info_t  :  public  streamdata_info_t
{
    struct  header_data_t
    {
        /* buffer's data resolution */
        int  res;
    };
};
}}

#endif  /*  !LDNDC_INPUT_CLIMATE_TYPES_SERVER_H_  */

