/*!
 * @brief
 *    abstract base classes for input classes
 *    (those read only input stream containers)
 *
 * @author
 *    steffen klatt (created on: Jul 14, 2011)
 */

#ifndef  LDNDC_INPUT_CLASS_SRV_H_
#define  LDNDC_INPUT_CLASS_SRV_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "input/ic-env.h"

namespace ldndc {
class  source_handle_srv_base_t;
/* (abstract) base class for input classes */
class CBM_API  input_class_srv_base_t
        :  public  input_class_base_t,  public  cbm::server_object_t
{
        public:
        virtual  ~input_class_srv_base_t() = 0;

                /*!
         * @brief
         *    initializes internal state
         *
         * @note
         *    do not keep reference to core input object;
         *    it will be destroyed (after input_exhausted
         *    returned true)!
         */
        virtual  lerr_t  initialize(
            ldndc::source_handle_srv_base_t const *) = 0;

        /*!
         * @brief
         *    resets input class time dependent internal
         *    state.
         */
        virtual  lerr_t  reset_internal_state() = 0;

        /*!
         * @brief
         *    updates input class time dependent internal
         *    state.
         */
        virtual  lerr_t  update_internal_state( cbm::sclock_t const &) = 0;

        /*!
         * @brief
         *    return true if input class has internal
         *    changing state, false otherwise. not having
         *    an internal changing state effectively means
         *    that @fn update_internal_state is assumed to
         *    be a "no operation" method, i.e. it's empty
         */
        virtual  bool  has_internal_state() const = 0;

    protected:
        input_class_srv_base_t( lid_t const &);
        lflags_t  status_;

    private:
        /* hide .. these need to be cleanly implemented */
        input_class_srv_base_t( input_class_srv_base_t const &);
        input_class_srv_base_t &  operator=( input_class_srv_base_t const &);
};

}

#define  LDNDC_INPUT_SRV_CLASS_DECL(__class__,__flags__) \
    LDNDC_INPUT_CLASS_DECL_( TOKENPASTE3(input_class_,__class__,_srv_t), \
            __class__, __flags__) \
    public: \
        typedef input_class_srv_base_t base_t; \
        TOKENPASTE3(input_class_,__class__,_srv_t)(); \
        TOKENPASTE3(input_class_,__class__,_srv_t)( lid_t const &); \
        ~TOKENPASTE3(input_class_,__class__,_srv_t)(); \
        lerr_t  initialize( ldndc::source_handle_srv_base_t const *);

#define  LDNDC_INPUT_SRV_CLASS_DEFN(__class__) \
    LDNDC_INPUT_CLASS_DEFN_(TOKENPASTE3(input_class_,__class__,_srv_t))


#define  set_input_class_reader_from_handle(__iclass__,__handle__) \
    ldndc::__LDNDC_IPROC_INTERFACE_CLASS(__iclass__) *  this_reader = NULL; \
    { \
        crabmeat_assert((__handle__)); \
        crabmeat_assert((__handle__)->object_id() == this->object_id()); \
        ldndc::source_handle_srv_t const *  this_source_handle = \
                static_cast< ldndc::source_handle_srv_t const * >( __handle__); \
        this_reader = this_source_handle->provider< ldndc::__LDNDC_IPROC_INTERFACE_CLASS(__iclass__) >(); \
    } \
    CRABMEAT_FIX_UNUSED(this_reader)/*;*/

#define  set_input_class_reader(__iclass__) \
    set_input_class_reader_from_handle(__iclass__,_source_handle); \
    if ( !this_reader) \
    { \
        CBM_LogError( "oops .. my reader has disappeared!  [class="#__iclass__"]"); \
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST; /* get reader asap since there is no cleanup on failure */ \
    }

#define  set_input_class_dataprovider(__dataprovider_iclass__) \
    __dataprovider_iclass__::TOKENPASTE3(input_class_,__dataprovider_iclass__,_srv_t) const *  this_##__dataprovider_iclass__ = NULL; \
    { \
        crabmeat_assert( _source_handle); \
        ldndc::source_handle_noreader_srv_t const *  this_source_handle = \
                static_cast< ldndc::source_handle_noreader_srv_t const * >( _source_handle); \
        this_##__dataprovider_iclass__ = \
            dynamic_cast< __dataprovider_iclass__::TOKENPASTE3(input_class_,__dataprovider_iclass__,_srv_t) const * >( \
            this_source_handle->get_input_class( \
                __dataprovider_iclass__::TOKENPASTE3(input_class_,__dataprovider_iclass__,_srv_t)::iclassname())); \
        if ( !this_##__dataprovider_iclass__) \
        { \
            CBM_LogError( "oops .. my data-provider has disappeared!  [requested server class=",#__dataprovider_iclass__," for kernel id=",this->object_id()," not available]"); \
            return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST; /* get dataprovider asap since there is no cleanup on failure */ \
        } \
    } \
    CRABMEAT_FIX_UNUSED(this_##__dataprovider_iclass__)/*;*/

#define  set_input_class_global(__iclass__)                                    \
    TOKENPASTE3(input_class_,__iclass__,_global_srv_t) const *  this_##__iclass__##_global = NULL;            \
    { \
        crabmeat_assert( _source_handle); \
        crabmeat_assert( _source_handle->object_id() == this->object_id()); \
        ldndc::source_handle_srv_t const *  this_source_handle = \
                static_cast< ldndc::source_handle_srv_t const * >( _source_handle); \
        this_##__iclass__##_global = \
                dynamic_cast< TOKENPASTE3(input_class_,__iclass__,_global_srv_t) const * >( \
                        this_source_handle->get_input_class_global( \
                            TOKENPASTE3(input_class_,__iclass__,_global_srv_t)::iclassname())); \
        if ( !this_##__iclass__##_global) \
        { \
            CBM_LogError( "oops .. my global reader has disappeared!  [class="#__iclass__"]"); \
            return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST; /* get global ic asap since there is no cleanup on failure */ \
        } \
    } \
    CRABMEAT_FIX_UNUSED(this_##__iclass__##_global)/*;*/

#endif  /*  !LDNDC_INPUT_CLASS_SRV_H_  */

