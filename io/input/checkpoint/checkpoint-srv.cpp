/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: dec 01, 2014)
 */

#include  "input/checkpoint/checkpoint-srv.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_checkpoint.h"

#include  "log/cbm_baselog.h"

namespace  ldndc{ namespace  checkpoint
{
LDNDC_INPUT_SRV_CLASS_DEFN(checkpoint)
input_class_checkpoint_srv_t::input_class_checkpoint_srv_t()
        : input_class_srv_base_t( invalid_lid)
{
}
input_class_checkpoint_srv_t::input_class_checkpoint_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id)
{
}


input_class_checkpoint_srv_t::~input_class_checkpoint_srv_t()
{
}

lerr_t
input_class_checkpoint_srv_t::initialize(
                ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
           {
        return  LDNDC_ERR_OK;
    }
    CBM_Assert( _source_handle);
    set_input_class_reader(checkpoint);

    this->sh = *static_cast< ldndc::source_handle_srv_t const * >( _source_handle);

    this->set_initialized();
    return  LDNDC_ERR_OK;
}

lerr_t
input_class_checkpoint_srv_t::retrieve_record(
        checkpoint_buffer_t *  _buf)
const
{
    set_input_class_reader_from_handle(checkpoint,&this->sh);
    if ( this_reader)
    {
        return  this_reader->retrieve_record( _buf);
    }
    return  LDNDC_ERR_RUNTIME_ERROR;
}


LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(checkpoint)
lerr_t
input_class_checkpoint_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *,
        ltime_t const *)
{
    /* guard */
    if ( this->is_initialized())
        { return  LDNDC_ERR_OK; } 
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

