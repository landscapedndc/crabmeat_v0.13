/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: dec 01, 2014)
 */

#ifndef  LDNDC_INPUT_CHECKPOINT_SERVER_H_
#define  LDNDC_INPUT_CHECKPOINT_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"
#include  "checkpoint/checkpointtypes.h"

#include  "io/source-handle-srv.h"

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;
namespace  checkpoint
{

class  CBM_API  input_class_checkpoint_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(checkpoint,ICLASS_FLAG_STATE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update_internal_state(
                cbm::sclock_t const &) { return  LDNDC_ERR_OK; }

    public:
        lerr_t  retrieve_record(
                checkpoint_buffer_t * /*buffer*/) const;

    private:
        ldndc::source_handle_srv_t  sh;
};
}}

/* nothing here */
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  checkpoint
{
class  CBM_API  input_class_checkpoint_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(checkpoint,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_CHECKPOINT_SERVER_H_  */

