/*!
 * @brief
 *    Holds site specific variables (parameters)
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas
 */

#include  "input/site/site-srv.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_site.h"

#include  "string/lstring-compare.h"
#include  "string/lstring-transform.h"
#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"


namespace  ldndc{ namespace  site
{
iclass_site_stratum_t::iclass_site_stratum_t()
        : stratum_height( invalid_dbl),
          split( invalid_int),
          ph( invalid_dbl),
          stone_fraction( invalid_dbl),
          bulk_density( invalid_dbl),
          sks( invalid_dbl),
          c_org( invalid_dbl),
          n_org( invalid_dbl),
          clay( invalid_dbl),
          sand( invalid_dbl),
          wcmin( invalid_dbl),
          wcmax( invalid_dbl),
          wfps_max( invalid_dbl),
          wfps_min( invalid_dbl),
          porosity( invalid_dbl),
          macropores( invalid_dbl),
          iron( invalid_dbl),
          vangenuchten_n( invalid_dbl),
          vangenuchten_alpha( invalid_dbl),
          wc_init( invalid_dbl),
          temp_init( invalid_dbl),
          nh4_init( invalid_dbl),
          no3_init( invalid_dbl),
          don_init( invalid_dbl),
          doc_init( invalid_dbl),
          pom_init( invalid_dbl),
          aorg_init( invalid_dbl)
{
}


LDNDC_INPUT_SRV_CLASS_DEFN(site)
input_class_site_srv_t::input_class_site_srv_t()
        : input_class_srv_base_t( invalid_lid),
          strata_( NULL),
          strata_cnt_( 0),
          soil_use_history_( ECOSYS_NONE),
          litterheight_( invalid_dbl),
          c_org05_( invalid_dbl),
          c_org30_( invalid_dbl)

{
}
input_class_site_srv_t::input_class_site_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id),
          strata_( NULL),
          strata_cnt_( 0),
          soil_use_history_( ECOSYS_NONE),
          litterheight_( invalid_dbl),
          c_org05_( invalid_dbl),
          c_org30_( invalid_dbl)
{
}


input_class_site_srv_t::~input_class_site_srv_t()
{
    if ( this->strata_)
    {
        CBM_DefaultAllocator->destroy< iclass_site_stratum_t >( this->strata_);
    }
}

#define  LAYER_HEIGHT_MIN  (0.1) /* 100 um */
#define  LITTERLAYER_HEIGHT_MIN  LAYER_HEIGHT_MIN
#define  SOIL_PROFILE_HEIGHT_DEFAULT  (1000.0) /* 1 m */

lerr_t
input_class_site_srv_t::initialize(
                ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    set_input_class_reader(site);

    /* "meta" info */
    lerr_t  rc_read_info = this->read_info_( this_reader);
    if ( rc_read_info)
    {
        return  rc_read_info;
    }
    /* soil history */
    lerr_t  rc_soil_use_history = this->read_use_history( this_reader);
    RETURN_IF_NOT_OK(rc_soil_use_history);

    size_t  this_strata_cnt = this_reader->get_number_of_strata();
    this->strata_cnt_ = ( this_strata_cnt == invalid_size) ? 0 : this_strata_cnt;

    if ( this->strata_cnt_ > 0)
    {
        this->strata_ = CBM_DefaultAllocator->construct< iclass_site_stratum_t >( this->strata_cnt_);
        if ( !this->strata_)
        {
            return  LDNDC_ERR_NOMEM;
        }

        /* litter height, organic carbon (5cm, 30cm)*/
        this->litterheight_ = this_reader->get_soil_litter_height();
        this->c_org05_ = this_reader->get_soil_organic_carbon_content_05();
        this->c_org30_ = this_reader->get_soil_organic_carbon_content_30();

        /* read general soil parameters */
        lerr_t  rc_soil_types = this->read_soil_types( this_reader);
        RETURN_IF_NOT_OK(rc_soil_types);

        lerr_t  rc_soil_discret = this->read_soil_discretization( this_reader);
        if ( rc_soil_discret == LDNDC_ERR_OK)
        {
            double *  buf = CBM_DefaultAllocator->allocate_init_type< double >(
                    STRATUM_PROPERTIES_CNT*this->strata_cnt_, invalid_t< double >::value);
            if ( !buf)
            {
                return  LDNDC_ERR_NOMEM;
            }

            lerr_t  rc_props = 
                this_reader->get_strata_properties( buf, this->strata_cnt_, STRATUM_PROPERTIES_CNT, STRATUM_PROPERTY_NAMES);
            if ( rc_props == LDNDC_ERR_OK)
            {
                rc_props = this->assign_strata_properties( buf);
            }

            CBM_DefaultAllocator->deallocate( buf);
            if ( rc_props)
            {
                return  LDNDC_ERR_FAIL;
            }
        }
    }
    else
    {
        CBM_LogError( "at least one stratum in site input stream must be provided");
        return  LDNDC_ERR_FAIL;
    }

    set_initialized();
    return  LDNDC_ERR_OK;
}
#undef  READ_STRATA_ITEM



lerr_t
input_class_site_srv_t::read_soil_discretization(
        ldndc::iproc_interface_site_t const *  _reader)
{
    double *  buf = CBM_DefaultAllocator->allocate_init_type< double >( 2*this->strata_cnt_, ldndc::invalid_t< double >::value);
    int *  buf_split = CBM_DefaultAllocator->allocate_init_type< int >( 1*this->strata_cnt_, ldndc::invalid_t< int >::value);
    if ( !buf || !buf_split)
    {
        CBM_DefaultAllocator->deallocate( buf);
        CBM_DefaultAllocator->deallocate( buf_split);
        return  LDNDC_ERR_NOMEM;
    }

    double *  buf_height = buf;
    double *  buf_splitheight = buf+this->strata_cnt_;

    lerr_t  rc_height = _reader->get_strata_height( buf_height, this->strata_cnt_, NULL, NULL);
    if ( rc_height)
    {
        CBM_DefaultAllocator->deallocate( buf);
        return  rc_height;
    }
    lerr_t  rc_split = _reader->get_strata_split( buf_split, this->strata_cnt_, NULL, NULL);
    if ( rc_split)
    {
        CBM_DefaultAllocator->deallocate( buf);
        return  rc_split;
    }
    lerr_t  rc_layer_height = _reader->get_strata_splitheight( buf_splitheight, this->strata_cnt_, NULL, NULL);
    if ( rc_layer_height)
    {
        CBM_DefaultAllocator->deallocate( buf);
        return  rc_layer_height;
    }

    lerr_t  rc = LDNDC_ERR_OK;

    double cumulated_depth( 0.0);
    for ( size_t  l = 0;  l < this->strata_cnt_;  ++l)
    {
        this->strata_[l].stratum_height = buf_height[l];
                
        cumulated_depth += 0.5 * this->strata_[l].stratum_height;
        
        if ( cbm::is_invalid( this->strata_[l].stratum_height) || ( this->strata_[l].stratum_height < LAYER_HEIGHT_MIN))
        {
            CBM_LogError( "height of strata [mm] is mandatory: missing, invalid or to small value  ",
                    "[stratum=",l+1,",height=",this->strata_[l].stratum_height,"]");
            rc = LDNDC_ERR_FAIL;
        }
        else if ( cbm::is_invalid( buf_split[l]))
        {
            if ( cbm::is_invalid( buf_splitheight[l]))
            {
                double layer_height( -1.0);
                if ( cbm::flt_less_equal( cumulated_depth, 20.0))
                {
                    layer_height = 10.0;
                }
                else if ( cbm::flt_less_equal( cumulated_depth, 100.0))
                {
                    layer_height = 20.0;
                }
                else if ( cbm::flt_less_equal( cumulated_depth, 200.0))
                {
                    layer_height = 20.0;
                }
                else if ( cbm::flt_less_equal( cumulated_depth, 400.0))
                {
                    layer_height = 20.0;
                }
                else if ( cbm::flt_less_equal( cumulated_depth, 600.0))
                {
                    layer_height = 40.0;
                }
                else if ( cbm::flt_less_equal( cumulated_depth, 800.0))
                {
                    layer_height = 60.0;
                }
                else if ( cbm::flt_less_equal( cumulated_depth, 1000.0))
                {
                    layer_height = 80.0;
                }
                else
                {
                    layer_height = 100.0;
                }

                //ensure minimum split of 1
                buf_split[l] = std::max( (int)(this->strata_[l].stratum_height / layer_height), 1);
            }
            else if ( buf_splitheight[l] < LAYER_HEIGHT_MIN)
            {
                CBM_LogWarn( "height of layer [mm] to small  ",
                        "[stratum=",l+1,",height(given)=",buf_splitheight[l],",height(used)=",this->strata_[l].stratum_height,"]");
                buf_split[l] = 1;
            }
            else
            {
                buf_split[l] = this->strata_[l].stratum_height / buf_splitheight[l];
            }
        }
        else if ( cbm::is_valid( buf_split[l]) && ( buf_split[l] > 0))
        {
            buf_splitheight[l] = this->strata_[l].stratum_height / buf_split[l];
            if ( buf_splitheight[l] < LAYER_HEIGHT_MIN)
            {
                CBM_LogError( "resulting height of layer [mm] to small  ",
                        "[stratum=",l+1,",height=",this->strata_[l].stratum_height,",split=",this->strata_[l].split,"]");
                rc = LDNDC_ERR_FAIL;
            }
        }
        else
        {
            CBM_LogError( "invalid value for layer split  [split=",buf_split[l],"]");
            rc = LDNDC_ERR_FAIL;
        }

        this->strata_[l].split = buf_split[l];
        
        cumulated_depth += 0.5 * this->strata_[l].stratum_height;
        
        if ( rc)
        {
            break;
        }
    }

    if ( rc)
    {
        CBM_LogWrite( "minimum strata height: ",LAYER_HEIGHT_MIN,"  minimum soil layer height: ",LAYER_HEIGHT_MIN, "  strata split: integer value greater or equal 1");
    }

    CBM_DefaultAllocator->deallocate( buf);
    CBM_DefaultAllocator->deallocate( buf_split);

    return  rc;
}

#define  ASSIGN_STRATA_ITEM(__item__,__itemname__)                    \
{                                            \
    unsigned int  prop_at = 0;                            \
    lerr_t  rc_find = cbm::find_index(                        \
        __itemname__, STRATUM_PROPERTY_NAMES, STRATUM_PROPERTIES_CNT, &prop_at);    \
    if ( rc_find)                                    \
    {                                        \
        CBM_LogError( "[BUG]  cannot find matching strata property entry  ",    \
                "[property=",__itemname__,"]");                \
        return  LDNDC_ERR_FAIL;                            \
    }                                        \
    double const *  prop_buf = buf + prop_at*this->strata_cnt_;            \
    for ( size_t  k = 0;  k < this->strata_cnt_;  ++k)                \
    {                                        \
        this->strata_[k].__item__ = prop_buf[k];                \
    }                                        \
}                                            \
CRABMEAT_FIX_UNUSED(_buffer)/*;*/
lerr_t
input_class_site_srv_t::assign_strata_properties(
        double const *  _buffer)
{
    double const *  buf = _buffer;

    ASSIGN_STRATA_ITEM(ph,"ph");
    ASSIGN_STRATA_ITEM(stone_fraction,"soil_skeleton");
    ASSIGN_STRATA_ITEM(bulk_density,"bulk_density");
    ASSIGN_STRATA_ITEM(sks,"hydraulic_conductivity");
    ASSIGN_STRATA_ITEM(c_org,"organic_carbon");
    ASSIGN_STRATA_ITEM(n_org,"organic_nitrogen");
    ASSIGN_STRATA_ITEM(clay,"clay");
    ASSIGN_STRATA_ITEM(sand,"sand");
    ASSIGN_STRATA_ITEM(wcmin,"wilting_point");
    ASSIGN_STRATA_ITEM(wcmax,"field_capacity");
    ASSIGN_STRATA_ITEM(wfps_max,"maximum_water_filled_pore_space");
    ASSIGN_STRATA_ITEM(wfps_min,"minimum_water_filled_pore_space");
    ASSIGN_STRATA_ITEM(porosity,"total_pore_space");
    ASSIGN_STRATA_ITEM(macropores,"macropores");
    ASSIGN_STRATA_ITEM(iron,"iron_content");
    ASSIGN_STRATA_ITEM(vangenuchten_n,"vangenuchten_n");
    ASSIGN_STRATA_ITEM(vangenuchten_alpha,"vangenuchten_alpha");
    ASSIGN_STRATA_ITEM(wc_init,"wc_init");
    ASSIGN_STRATA_ITEM(temp_init,"temp_init");
    ASSIGN_STRATA_ITEM(nh4_init,"nh4_init");
    ASSIGN_STRATA_ITEM(no3_init,"no3_init");
    ASSIGN_STRATA_ITEM(don_init,"don_init");
    ASSIGN_STRATA_ITEM(doc_init,"doc_init");
    ASSIGN_STRATA_ITEM(pom_init,"pom_init");
    ASSIGN_STRATA_ITEM(aorg_init,"aorg_init");

    return  LDNDC_ERR_OK;
}

lerr_t
input_class_site_srv_t::read_use_history(
        ldndc::iproc_interface_site_t const *  _reader)
{
    std::string  soil_history_s;
    lerr_t  rc_soil_use_history = _reader->soil_use_history( &soil_history_s);

    this->soil_use_history_ = ECOSYS_NONE;
    if ( rc_soil_use_history == LDNDC_ERR_OK)
    {
        cbm::find_enum( soil_history_s.c_str(), ECOSYSTEM_NAMES, ECOSYS_CNT, &this->soil_use_history_);
    }

//    if ( this->soil_use_history_ == ECOSYS_NONE)
//    {
//        cbm::cat_list( &soil_history_s, ECOSYSTEM_NAMES+1, ECOSYS_CNT-1, ",");
//        CBM_LogError( "soil use history required. give one of:", "{", soil_history_s, "}");
//        return  LDNDC_ERR_FAIL;
//    }

    return  LDNDC_ERR_OK;
}


lerr_t
input_class_site_srv_t::read_soil_types(
        ldndc::iproc_interface_site_t const *  _reader)
{
    std::string  chr_l( "");
    /* soil parameters */
    /*    soil name -> soil type */
    chr_l = cbm::to_upper( _reader->get_soil_type());
    unsigned int  this_soil_type = soillayers::SOIL_NONE;
    cbm::find_index( chr_l.c_str(), soillayers::SOIL_MNEMONIC, soillayers::SOIL_CNT, &this_soil_type);
    this->soil_type_ = static_cast< soillayers::soil_type_e >( this_soil_type);
    if ( this->soil_type_ == soillayers::SOIL_NONE)
    {
        if ( cbm::is_equal( chr_l, soillayers::SOIL_MNEMONIC[soillayers::SOIL_NONE]))
        {
            CBM_LogWarn( "unset soil type, using defaults");
        }
        else
        {
            CBM_LogError( "unknown soil type mnemonic.  [soil=",chr_l,"]");
            return  LDNDC_ERR_FAIL;
        }
    }
    /*    humus name -> humus type */
    chr_l = cbm::to_upper( _reader->get_humus_type());
    unsigned int  this_humus_type = soillayers::HUMUS_NONE;
    cbm::find_index( chr_l.c_str(), soillayers::HUMUS_MNEMONIC, soillayers::HUMUS_CNT, &this_humus_type);
    this->humus_type_ = static_cast< soillayers::humus_type_e >( this_humus_type);
    if ( this->humus_type_ == soillayers::HUMUS_NONE)
    {
        if ( cbm::is_equal( chr_l, soillayers::HUMUS_MNEMONIC[soillayers::HUMUS_NONE]))
        {
            if ( this->litterheight_ > 0.0)
            {
                CBM_LogWarn( "unset humus type, using defaults");
            }
        }
        else
        {
            CBM_LogError( "unknown humus type mnemonic  [humus=",chr_l,"]");
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}

site_info_t const *
input_class_site_srv_t::info()
const
{
    return  &this->info_;
}
lerr_t
input_class_site_srv_t::read_info_(
        ldndc::iproc_interface_site_t const *  _reader)
{
    this->info_ = site_info_defaults;

    this->info_.sksbottom = _reader->get_saturated_hydraulic_conductivity_bottom();

    this->info_.watertable = _reader->get_water_table_depth();
    if ( cbm::is_invalid( this->info_.watertable))
    {
        this->info_.watertable = site_info_defaults.watertable;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
input_class_site_srv_t::stratum(
        size_t  _l,
        iclass_site_stratum_t *  _stratum)
const
{
    if ( !_stratum)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    if ( _l < this->strata_cnt_)
    {
        *_stratum = this->strata_[_l];
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_INVALID_ARGUMENT;
}



LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(site)
lerr_t
input_class_site_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *,
        ltime_t const *)
{
    /* guard */
    if ( is_initialized())
        { return  LDNDC_ERR_OK; }
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

