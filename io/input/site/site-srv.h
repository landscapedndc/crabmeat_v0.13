/*!
 * @brief
 *    container for site specific input data
 *
 * @author
 *    Steffen Klatt (created on: Jul 8, 2010),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_SITE_SERVER_H_
#define  LDNDC_INPUT_SITE_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"

#include  "site/sitetypes.h"
#include  "soillayers/soillayerstypes.h"
#include  "ecosystemtypes.h"


namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;

    class  iproc_interface_site_t;
namespace  site
{

class  CBM_API  input_class_site_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(site,ICLASS_FLAG_NONE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update_internal_state(
                cbm::sclock_t const &)
        {
            return  LDNDC_ERR_OK;
        }


    public:
        lerr_t  stratum( size_t, iclass_site_stratum_t *) const;
        size_t  strata_cnt() const { return  this->strata_cnt_; }
    private:
                iclass_site_stratum_t *  strata_;
        size_t  strata_cnt_;


    public:
        site_info_t const *  info() const;
    private:
        site_info_t  info_;
        lerr_t  read_info_(
                ldndc::iproc_interface_site_t const *);


    public:
        ecosystem_type_e  soil_use_history() const { return  this->soil_use_history_; }
    private:
        ecosystem_type_e  soil_use_history_;


    public:
        soillayers::soil_type_e  soil_type() const { return  this->soil_type_; }
        soillayers::humus_type_e  humus_type() const { return  this->humus_type_; }
    private:
        soillayers::humus_type_e  humus_type_;
        soillayers::soil_type_e  soil_type_;

    public:
        double  litterheight() const { return  this->litterheight_; }
        double  c_org05() const { return  this->c_org05_; }
        double  c_org30() const { return  this->c_org30_; }
    private:
        /*! height of litter layer  [mm] */
        double  litterheight_;
        /*! organic carbon content in 50mm depth  [%] */
        double  c_org05_;
        /*! organic carbon content in 300mm depth  [%] */
        double  c_org30_;

    private:
        lerr_t  read_soil_discretization(
                ldndc::iproc_interface_site_t const *);
        lerr_t  assign_strata_properties(
                double const * /*buffer*/);
        lerr_t  read_use_history(
                ldndc::iproc_interface_site_t const *);
        lerr_t  read_soil_types(
                ldndc::iproc_interface_site_t const *);
};
}}

/* nothing here */
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  site
{
class  CBM_API  input_class_site_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(site,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_SITE_SERVER_H_  */

