/*!
 * @brief
 *    creates, initializes and stores all input classes
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011)
 */

#ifndef INPUT_CLASS_POOL_H_
#define INPUT_CLASS_POOL_H_

#include  "crabmeat-common.h"
#include  "input/ic-env.h"

#include  "time/cbm_time.h"
#include  "string/cbm_string.h"

#include  <map>

namespace ldndc {

class  input_class_global_srv_base_t;
class  input_class_srv_base_t;

class  input_source_srv_base_t;
class  source_handle_srv_base_t;

class  project_file_t;

class CBM_API  input_class_pool_t  :  public  cbm::server_object_t
{
    LDNDC_SERVER_OBJECT(input_class_pool_t)
    private:
        /* input classes for blocks < block-id, class ptr > */
        struct  node_ic_t
        {
            ldndc::input_class_srv_base_t *  ic;
            cbm::string_t  hs;
        };
        typedef  std::pair< ldndc::hash_ic_t, node_ic_t >  ic_pair_type;
        typedef  std::map< ic_pair_type::first_type, ic_pair_type::second_type >  ic_container_type;
        typedef  ic_container_type::iterator  ic_iterator;
        typedef  ic_container_type::const_iterator  ic_const_iterator;

        /* input classes for global section < stream-id, class ptr > */
        struct  node_gc_t
        {
            ldndc::input_class_global_srv_base_t *  ic;
        };
        typedef  std::pair< std::string, node_gc_t >  gc_pair_type;
        typedef  std::map< gc_pair_type::first_type, gc_pair_type::second_type >  gc_container_type;
        typedef  gc_container_type::iterator  gc_iterator;
        typedef  gc_container_type::const_iterator  gc_const_iterator;

        /*!
         * @brief
         *    generate unique identifier for associated or
         *    dependent input class according to type
         */
        lerr_t  hash_ic( ldndc::hash_ic_t * /*buffer*/,
            cbm::source_descriptor_t const * /*kernel info*/,
            char const * /*type*/, node_ic_t * = NULL) const;
        /*!
         * @brief
         *    generate unique identifier for associated
         *    input class
         */
        lerr_t  hash_aic( ldndc::hash_ic_t * /*buffer*/,
            cbm::source_descriptor_t const * /*kernel info*/,
            char const * /*type*/, node_ic_t * = NULL) const;
        /*!
         * @brief
         *    generate unique identifier for dependent
         *    input class
         */
        lerr_t  hash_dic( ldndc::hash_ic_t * /*buffer*/,
            cbm::source_descriptor_t const * /*kernel info*/,
            char const * /*type*/, node_ic_t * = NULL) const;

    public:
        input_class_pool_t();
        ~input_class_pool_t();

        /*!
         * @brief
         *    creates and initializes new input class
         *    and adds it to the pool. the type is given
         *    by the core input object.
         *
         * @note
         *    if an input class exists that has the same
         *    ID as the core input object it will silently
         *    return without touching the existing one. to
         *    catch such situation query for id beforehand.
         *
         * @return
         *    LDNDC_ERR_OK, everything went fine or class
         *    already existed (i.e. identical ID)
         *
         *    LDNDC_ERR_OBJECT_CREATE_FAILED, failed to
         *    create input class object
         *
         *    LDNDC_ERR_OBJECT_INIT_FAILED, succeeded to
         *    create but failed to initialize input class
         */
        lerr_t  mount_input_class( ldndc::source_handle_srv_base_t const *,
                        char const * /*type*/);
    private:
        lerr_t  mount_input_class_( ldndc::source_handle_srv_base_t const *,
                        char const * /*type*/);

    public:
        /*!
         * @brief
         *    destroy all input and global input classes
         */
        lerr_t  unmount_all_input_classes();

        /*!
         * @brief
         *    retrieve input class from the pool. the
         *    class is uniquely identified by its ID
         *    and class type.
         *
         * @return
         *    pointer to input class if it exists, NULL
         *    if it does not exist (i.e. no such ID (pool
         *    empty) invalid ID or invalid input class
         *    type was given.
         */
        ldndc::input_class_srv_base_t const *  get_input_class(
                cbm::source_descriptor_t const * /*buffer*/,
                char const * /*type*/) const;

        ldndc::input_class_srv_base_t *  get_input_class(
                cbm::source_descriptor_t const * /*buffer*/,
                char const * /*type*/);

        lid_t  get_source_descriptor( cbm::source_descriptor_t * /*buffer*/,
            cbm::source_descriptor_t const * /*kernel info*/, char const * /*type*/) const;

        lerr_t  mount_input_class_global(
                ldndc::input_source_srv_base_t const * /*input object*/,
                char const * /*type*/,
                ltime_t const * /*stream reference time*/);

    private:
        lerr_t  mount_input_class_global_(
                ldndc::input_source_srv_base_t const *,
                char const * /*type*/, ltime_t const *);

    public:
        ldndc::input_class_global_srv_base_t const *  get_input_class_global(
                lid_t const & /*stream id*/, char const * /*type*/) const;
        ldndc::input_class_global_srv_base_t *  get_input_class_global(
                lid_t const & /*stream id*/, char const * /*type*/);

    public:
        /*!
         * @brief    
         *    reset input class state to initial conditions
         */
        lerr_t  reset_input( cbm::source_descriptor_t const *, char const * /*type*/);
        /*!
         * @brief    
         *    trigger full collective input internal
         *    state reset
         */
        lerr_t  reset_input();

        /*!
         * @brief
         *    input class might possess an internal
         *    state (e.g. stream data buffers that have
         *    to be refreshed). this function tests
         *    whether a class does have an internal
         *    changing state and triggers an update if
         *    applicable. during a simulation run this
         *    should (must) happen at each time step.
         *
         * @return
         *    LDNDC_ERR_OK, update went fine
         *
         *    LDNDC_ERR_OBJECT_DOES_NOT_EXIST, input
         *    object was not found in the pool
         *    
         *    LDNDC_ERR_RUNTIME_ERROR, object exists
         *    but has no input class attached to it
         *
         *    return code of internal state update routine,
         *    if an error occured during internal state
         *    update
         */
        lerr_t  update_input( cbm::source_descriptor_t const * /*source descriptor*/,
                char const * /*type*/, cbm::sclock_t const *  /*clock*/);
        /*!
         * @brief
         *    same functionality as above but triggers
         *    state update for all objects existing in
         *    in the input pool.
         *
         * @return
         *    LDNDC_ERR_OK, all updates went fine
         *
         *    LDNDC_ERR_OBJECT_INIT_FAILED, an error
         *    occured during internal state update
         */
        lerr_t  update_input( cbm::sclock_t const *  /*clock*/);

    private:
        /* container, one for each global input class */
        gc_container_type  m_gc;

        /* inputs container ( 0: AIC, 1: DIC ) */
        ic_container_type  m_ic[2];

        /***  helpers  ***/

        lerr_t  create_input_class_( lid_t const &  /*object id*/,
                char const *  /*type*/,
                ldndc::input_class_srv_base_t **  /*new instance pointer*/);

        lerr_t  create_input_class_global_( lid_t const &  /*stream id*/,
                char const *  /*type*/,
                ldndc::input_class_global_srv_base_t **  /*new instance pointer*/);

        lerr_t  m_clear();
};
}

#endif  /* INPUT_CLASS_POOL_H_ */

