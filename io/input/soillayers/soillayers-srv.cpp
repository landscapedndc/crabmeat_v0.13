/*!
 * @brief
 *    container for initial set up of soil layers
 *
 * @author
 *    Steffen Klatt (Created on: dec 01, 2011)
 *    Edwin Haas
 */

#include  "input/soillayers/soillayers-srv.h"

#include  "io/source-handle-srv.h"

#include  "input/site/site-srv.h"
#include  "input/setup/setup-srv.h"
#include  "input/soilparameters/soilparameters-srv.h"

#include  "scientific/soil/ld_soil.h"
#include  "scientific/hydrology/ld_vangenuchten.h"

#include  "math/lmath-poly.h"
#include  "math/lmath-float.h"
#include  "math/lmath-means.h"

#include  "constants/lconstants-phys.h"
#include  "constants/lconstants-conv.h"

#include  "utils/lutils.h"


namespace  ldndc{ namespace  soillayers
{
LDNDC_INPUT_SRV_CLASS_DEFN(soillayers)
input_class_soillayers_srv_t::input_class_soillayers_srv_t()
        : input_class_srv_base_t( invalid_lid),
          strata_( NULL),
          strata_cnt_( 0),

          soil_layer_cnt_( 0),
          soil_layers_in_litter_cnt_( 0)
{
}
input_class_soillayers_srv_t::input_class_soillayers_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id),
          strata_( NULL),
          strata_cnt_( 0),

          soil_layer_cnt_( 0),
          soil_layers_in_litter_cnt_( 0)
{
}

input_class_soillayers_srv_t::~input_class_soillayers_srv_t()
{
    if ( this->strata_) 
    {
        CBM_DefaultAllocator->deallocate( this->strata_);
    }
}


lerr_t
input_class_soillayers_srv_t::initialize(
        ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    set_input_class_dataprovider(site);
    if ( !this_site)
    {
        return  LDNDC_ERR_FAIL;
    }
    set_input_class_dataprovider(setup);
    if ( !this_setup)
    {
        return  LDNDC_ERR_FAIL;
    }
    
    /* initialize soil layer object */
    this->strata_cnt_ = this_site->strata_cnt();
    this->strata_ = CBM_DefaultAllocator->allocate_type< site::iclass_site_stratum_t >( this->strata_cnt_);
    if ( !this->strata_)
    {
        return  LDNDC_ERR_FAIL;
    }
    
    /* set missing input data to default */
    lerr_t  rc_set_strata = this->set_strata_defaults_( this_site);
    if ( rc_set_strata)
    {
        return  LDNDC_ERR_FAIL;
    }
    
    /* from stratum information determine number of internal soil layers */
    lerr_t  rc_layer_cnt = this->set_soil_layers_cnt_( this_setup);
    if ( rc_layer_cnt)
    {
        return  LDNDC_ERR_FAIL;
    }
    lerr_t  rc_litter_layer_cnt = this->set_soil_layers_in_litter_cnt_();
    if ( rc_litter_layer_cnt)
    {
        return  LDNDC_ERR_FAIL;
    }
    CBM_LogDebug( "#l=", soil_layer_cnt_, "  #l-floor=", soil_layers_in_litter_cnt_);
    
    set_initialized();
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_soillayers_srv_t::set_strata_defaults_(
        site::input_class_site_srv_t const *  _site)
{
    this->p.soil = _site->soil_type();
    this->p.humus = _site->humus_type();

    this->p.c_org05 = _site->c_org05();
    this->p.c_org30 = _site->c_org30();

    /* LITTER HEIGHT */
    if ( cbm::flt_greater_equal_zero( _site->litterheight()))
    {
        this->p.litterheight = _site->litterheight();
    }
    else
    {
        this->p.litterheight = 0.0;
        CBM_LogInfo( "Litter height has not been indicated. A value of ",this->p.litterheight,"mm has been estimated!");
    }

    // - stratum specific parameters used to define layer specific information
    for ( size_t  s = 0;  s < this->strata_cnt_;  ++s)
    {
        _site->stratum( s, &this->strata_[s]);

        /* DEPTH 1 */
        if ( !cbm::flt_greater_zero( this->strata_[s].stratum_height))
        {
            CBM_LogError( "Found invalid stratum height. you should not be able to get here..?!");
            return  LDNDC_ERR_FAIL;
        }

        /* SPLIT */
        if ( this->strata_[s].split < 1)
        {
            CBM_LogError( "Stratum discretization is invalid. this should be impossible..?!");
            return  LDNDC_ERR_FAIL;
        }
        
        /* CLAY */
        if ( cbm::flt_greater( this->strata_[s].clay, 1.0))
        {
            CBM_LogError( "Clay content of stratum ",s+1," out of range: ",this->strata_[s].clay);
            return  LDNDC_ERR_FAIL;
        }
                
        /* SAND */
        if ( cbm::flt_greater( this->strata_[s].sand, 1.0))
        {
            CBM_LogError( "Sand content of stratum ",s+1," out of range: ",this->strata_[s].sand);
            return  LDNDC_ERR_FAIL;
        }

        if ( cbm::flt_greater( this->strata_[s].stone_fraction, 1.0))
        {
            CBM_LogError( "Stone fraction of stratum ",s+1," out of range: ",this->strata_[s].stone_fraction);
            return  LDNDC_ERR_FAIL;
        }

        /* BULK DENSITY */
        if ( cbm::flt_greater( this->strata_[s].bulk_density, 10.0))
        {
            CBM_LogError( "Soil bulk density of stratum ",s+1," out of range: ",this->strata_[s].bulk_density);
            return  LDNDC_ERR_FAIL;
        }

        /* SKS */
        if ( cbm::flt_greater( this->strata_[s].sks, 10.0))
        {
            CBM_LogError( "Soil hydraulic conductivity of stratum ",s+1," out of range: ",this->strata_[s].sks);
            return  LDNDC_ERR_FAIL;
        }

        /* IRON */
        if ( cbm::flt_greater( this->strata_[s].iron, 1.0))
        {
            CBM_LogError( "Iron content of stratum ",s+1," out of range: ",this->strata_[s].iron);
            return  LDNDC_ERR_FAIL;
        }

        /* PH */
        if ( cbm::flt_greater( this->strata_[s].ph, 15.0))
        {
            CBM_LogError( "pH value of stratum ",s+1," out of range: ",this->strata_[s].ph);
            return  LDNDC_ERR_FAIL;
        }

        /* ORGANIC CARBON */
        if ( cbm::flt_greater( this->strata_[s].c_org, 1.0))
        {
            CBM_LogError( "Organic carbon content of stratum ",s+1," out of range: ",this->strata_[s].c_org);
            return  LDNDC_ERR_FAIL;
        }

        /* Total NITROGEN */
        if ( cbm::flt_greater( this->strata_[s].n_org, 1.0))
        {
            CBM_LogError( "Total nitrogen content of stratum ",s+1," out of range: ",this->strata_[s].n_org);
            return  LDNDC_ERR_FAIL;
        }

        /* VAN GENUCHTEN ALPHA */
        if ( cbm::flt_greater( this->strata_[s].vangenuchten_alpha, 50.0))
        {
            CBM_LogError( "Van Genuchten alpha of stratum ",s+1," out of range: ",this->strata_[s].vangenuchten_alpha);
            return  LDNDC_ERR_FAIL;
        }

        /* VAN GENUCHTEN N */
        if ( cbm::flt_greater( this->strata_[s].vangenuchten_n, 6.0))
        {
            CBM_LogError( "Van Genuchten n of stratum ",s+1," out of range: ",this->strata_[s].vangenuchten_n);
            return  LDNDC_ERR_FAIL;
        }
        
        /* MAXIMUM WATER FILLED PORE SPACE */
        if ( cbm::flt_greater( this->strata_[s].wfps_max, 1.0))
        {
            CBM_LogError( "Maximum water filled pore space of stratum ",s+1," out of range: ",this->strata_[s].wfps_max);
            return  LDNDC_ERR_FAIL;
        }

        /* MINIMUM WATER FILLED PORE SPACE */
        if ( cbm::flt_greater( this->strata_[s].wfps_min, 1.0))
        {
            CBM_LogError( "Minimum water filled pore space of stratum ",s+1," out of range: ",this->strata_[s].wfps_min);
            return  LDNDC_ERR_FAIL;
        }

        /* TOTAL PORE SPACE including stones [m/m] */
        if ( cbm::flt_greater( this->strata_[s].porosity, 1.0))
        {
            CBM_LogError( "Total porosity of stratum ",s+1," out of range: ",this->strata_[s].porosity);
            return  LDNDC_ERR_FAIL;
        }
        
        /* MACROPORES */
        if ( cbm::flt_greater( this->strata_[s].macropores, 1.0))
        {
            CBM_LogError( "Macropores of stratum ",s+1," out of range: ",this->strata_[s].macropores);
            return  LDNDC_ERR_FAIL;
        }
        
        /* WCMAX */
        if ( cbm::flt_greater( this->strata_[s].wcmax, 1000.0))
        {
            CBM_LogError( "Field capacity of stratum ",s+1," out of range: ",this->strata_[s].wcmax);
            return  LDNDC_ERR_FAIL;//~ CBM_LogInfo( "wcmax set to ", this->strata_[s].wcmax / 1000.0, " for fine soil");
        }

        /* WCMIN */
        if ( cbm::flt_greater( this->strata_[s].wcmin, 1000.0))
        {
            CBM_LogError( "Wilting point of stratum ",s+1," out of range: ",this->strata_[s].wcmin);
            return  LDNDC_ERR_FAIL;
        }
    }
    
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_soillayers_srv_t::set_soil_layers_in_litter_cnt_()
{
    this->soil_layers_in_litter_cnt_ = 0;
    
    /* consider that an error... */
    if ( this->p.litterheight < 0.0)
    {
        CBM_LogError( "Litter height was negative when attempting to determine number of litter layers");
        return  LDNDC_ERR_FAIL;
    }
    
    if ( cbm::flt_equal_zero( this->p.litterheight))
    {
        return  LDNDC_ERR_OK;
    }
    
    /* count number of soil layers in strata above litter height */
    double  d = 0.0;
    size_t  l = 0;
    size_t  s = 0;
    while ( s < this->strata_cnt_)
    {
        double  d_next = d + this->strata_[s].stratum_height;
        if ( d_next > this->p.litterheight)
        {
            break;
        }
        d = d_next;
        l += this->strata_[s].split;
        ++s;
    }
    
    /* add "stratum fraction" unless already at bottom */
    if ( s < this->strata_cnt_)
    {
        l += static_cast< size_t >( ceil( ( this->p.litterheight - d) * this->strata_[s].split / this->strata_[s].stratum_height));
    }
    
    this->soil_layers_in_litter_cnt_ = l;
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_soillayers_srv_t::set_soil_layers_cnt_(
           setup::input_class_setup_srv_t const *_setup)
{
    this->soil_layer_cnt_ = 0;
    if ( _setup)
    {
        if ( _setup->info()->soillayers > 0)
        {
            this->soil_layer_cnt_ = _setup->info()->soillayers;
            return  LDNDC_ERR_OK;
        }
    }

    for ( size_t  s = 0;  s < this->strata_cnt_;  ++s)
    {
        if ( this->strata_[s].split < 1)
        {
            CBM_LogError( "Invalid discretization");
            this->soil_layer_cnt_ = 0;
            return  LDNDC_ERR_FAIL;
        }
        this->soil_layer_cnt_ += this->strata_[s].split;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
input_class_soillayers_srv_t::stratum(
        size_t  _l,
        site::iclass_site_stratum_t *  _stratum)
const
{
    if ( !_stratum)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    if ( _l < this->strata_cnt_)
    {
        *_stratum = this->strata_[_l];
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_INVALID_ARGUMENT;
}

char const *
input_class_soillayers_srv_t::soil_mnemonic()
const
{
    crabmeat_assert( this->p.soil< soillayers::SOIL_CNT);
    return  soillayers::SOIL_MNEMONIC[this->p.soil];
}

char const *
input_class_soillayers_srv_t::humus_mnemonic()
const
{
    crabmeat_assert( this->p.humus< soillayers::HUMUS_CNT);
    return  soillayers::HUMUS_MNEMONIC[this->p.humus];
}




LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(soillayers)
lerr_t
input_class_soillayers_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *,
        ltime_t const *)
{
    this->set_initialized();
    return  LDNDC_ERR_OK;
}

}}

