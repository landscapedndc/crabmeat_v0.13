/*!
 * @brief
 *    container for initial set up of soil layers
 *
 * @author
 *      steffen klatt (Created on: dec 01, 2011)
 *      edwin haas
 */

#ifndef  LDNDC_INPUT_SOILLAYERS_SERVER_H_
#define  LDNDC_INPUT_SOILLAYERS_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"
#include  "soillayers/soillayerstypes.h"

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;
namespace  site
{
    struct  iclass_site_stratum_t;
    class  input_class_site_srv_t;
}
namespace  setup
{
    class  input_class_setup_srv_t;
}
namespace  soillayers
{
class  CBM_API  input_class_soillayers_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(soillayers,ICLASS_FLAG_NONE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update_internal_state(
                cbm::sclock_t const &)
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  stratum( size_t, site::iclass_site_stratum_t *) const;
        size_t  strata_cnt()
        const
        {
            return  this->strata_cnt_;
        }
        size_t  soil_layer_cnt()
        const
        {
            return  this->soil_layer_cnt_;
        }
        size_t  soil_layers_in_litter_cnt()
        const
        {
            return  this->soil_layers_in_litter_cnt_;
        }

    public:
        soillayers::soil_type_e  soil_type() const { return  this->p.soil; }
        char const *  soil_mnemonic() const;

        soillayers::humus_type_e  humus_type() const { return  this->p.humus; }
        char const *  humus_mnemonic() const;

    public:
        double  litterheight() const { return  this->p.litterheight; }
        double  c_org05() const { return  this->p.c_org05; }
        double  c_org30() const { return  this->p.c_org30; }

    private:
        site::iclass_site_stratum_t *  strata_;
        size_t  strata_cnt_;

                /* number of soil layers */
        size_t  soil_layer_cnt_;                // slMax
                /* number of soil layers in litter layer */
        size_t  soil_layers_in_litter_cnt_;     // slFloor

        struct
        {
            double  litterheight;
            double  c_org05;
            double  c_org30;
            soillayers::soil_type_e  soil;
            soillayers::humus_type_e  humus;
        } p;

        /*!
         * @brief
         *    gapfill missing entities
         */
        lerr_t  set_strata_defaults_(
                site::input_class_site_srv_t const *);
        /*!
         * @brief
         *    calculate number of soil layers in litter layer
         */
        lerr_t  set_soil_layers_in_litter_cnt_();
        /*!
         * @brief
         *    calculate number of soil layers in total soil profile (slMax)
         */
        lerr_t  set_soil_layers_cnt_(
                 setup::input_class_setup_srv_t const *);
};
}}

#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  soillayers
{
class  CBM_API  input_class_soillayers_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(soillayers,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_SOILLAYERS_SERVER_H_  */

