/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2010),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_BASE_STREAMDATA_H_
#define  LDNDC_INPUT_BASE_STREAMDATA_H_

#include  "crabmeat-common.h"

#include  "input/ic-srv.h"
#include  "input/load.h"
#include  "utils/callback.h"
#include  "memory/cbm_mem.h"

#include  "time/cbm_time.h"



#define  __LDNDC_STREAM_INPUT_BUFFER_ITEM(__rec_item__,__offs__) \
public: \
    inline \
    element_type  __rec_item__( \
        callback_argument_type::time_type  _timestep, \
        callback_argument_type::timeresolution_type  _resolution) \
    const \
    { \
        if ( ! this->is_ok()) \
            { return  ldndc::invalid_t< element_type >::value; } \
        return  this->data_item( _timestep, _resolution, size_t(__offs__)); \
    } \
private:


namespace ldndc {
/*!
 * @brief
 *    common logic to handle stream input data
 */
template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
class  iclassbase_streamdata_t  :  public  input_class_srv_base_t
{
    public:
        /* element type */
        typedef  typename _E::element_type  element_type;

        typedef struct callback_argument_type
        {
            typedef  size_t  time_type;
            /* requested time step */
            time_type  t;
            typedef  int  timeresolution_type;
            /* requested resolution */
            timeresolution_type  r;

            typedef  size_t  size_type;
            /* reload size */
            size_type  n;
            /* buffer stride */
            size_type  s;

            typedef  typename _E::element_type  element_type;
            /* buffer (_not_ owner) */
            element_type *  buf;

            /* internal "control structure" */
            void *  node;

        } callback_argument_type;
        typedef  int  callback_return_type;
        /* data record reload notifier type */
        typedef  ldndc::callback_1< iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >, callback_return_type, callback_argument_type >  callback_type;
        /* data buffer type */
        typedef  ldndc::stream_data_buffer_t< callback_type, _E, _BUF_SIZE, _REC_SIZE >  buffer_type;

    protected:
        iclassbase_streamdata_t(
                lid_t const &);
        virtual  ~iclassbase_streamdata_t();

        /*!
         * @brief
         *    deriving class' record item names
         */
        virtual  char const **  record_item_names() const = 0;

        /*!
         * @brief
         *    initialize stream data handler structures
         */
        virtual  lerr_t  initialize_base(
                ltime_t const &  /*simulation time*/, ltime_t const &  /*data time*/,
                iproc_interface_table_base_t< _E > *  /*reader*/,
                bool  /*endless*/,
                typename _E::boundary_data_type const * /*class specific boundary conditions*/);

        virtual  lerr_t  reset_internal_state();
        virtual  lerr_t  update_internal_state(
                cbm::sclock_t const &);


        element_type const *  data_record(
                typename callback_argument_type::time_type  _timestep,
                typename callback_argument_type::timeresolution_type  _resolution)
        const
        {
            callback_argument_type  cb_arg = this->callback_argument_default;
            cb_arg.t = _timestep;
            cb_arg.r = _resolution;
            return  this->m_buf->data_record( &cb_arg);
        }
        element_type *  data_record(
                typename callback_argument_type::time_type  _timestep,
                typename callback_argument_type::timeresolution_type  _resolution)
        {
            callback_argument_type  cb_arg = this->callback_argument_default;
            cb_arg.t = _timestep;
            cb_arg.r = _resolution;
            return  this->m_buf->data_record( &cb_arg);
        }

        element_type  data_item(
                typename callback_argument_type::time_type  _timestep,
                typename callback_argument_type::timeresolution_type  _resolution,
                       size_t  _item_offset)
        const
        {
            callback_argument_type  cb_arg = this->callback_argument_default;
            cb_arg.t = _timestep;
            cb_arg.r = _resolution;
            return  this->m_buf->data_item( &cb_arg, _item_offset);
        }


        lerr_t  init_error_(
                lerr_t = LDNDC_ERR_OBJECT_INIT_FAILED);
        
        /* triggers reload in buffer and provides reader */
        typename callback_type::return_type  m_reloadcallback(
                typename callback_type::argument_type *);

    protected:
        ltime_t  simulation_time;
        ltime_t  streamdata_time;

    private:
        /*  */
        callback_type *  m_reloadnotifier;

        /* interface to the data source (not owner) */
        iproc_interface_table_base_t< _E > *  m_reader;

        /* holds all items of record of read records and handles load logic */
        buffer_type *  m_buf;

    public:
        static callback_argument_type const  callback_argument_default;
};

template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
typename iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::callback_argument_type const  iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::callback_argument_default =
{
    0,        /* requested time step */
    0,        /* requested resolution */

    0,        /* reload size */
    0,        /* buffer stride */

    NULL,        /* buffer */
    NULL        /* internal "control structure" */
};

template < typename _E,  size_t _BUF_SIZE, size_t _REC_SIZE >
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::iclassbase_streamdata_t(
                lid_t const &  _id)
                : input_class_srv_base_t( _id),

          m_reloadnotifier( NULL), m_reader( NULL),
          m_buf( NULL)
{
}

template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::~iclassbase_streamdata_t()
{
    if ( this->m_reloadnotifier)
    {
        CBM_DefaultAllocator->destroy( this->m_reloadnotifier);
    }
    if ( this->m_buf)
    {
        CBM_DefaultAllocator->destroy( this->m_buf);
    }
}

template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
lerr_t
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::initialize_base(
        ltime_t const &  _sim_time, ltime_t const &  _data_time,
        iproc_interface_table_base_t< _E > *  _reader,
        bool  _endless,
        typename _E::boundary_data_type const *  _bd)
{
    this->simulation_time = _sim_time;
    this->streamdata_time = _data_time;
    this->m_reader = _reader;

    /* setup reload callback ... */
    this->m_reloadnotifier = CBM_DefaultAllocator->construct_args< callback_type >(
            static_cast< size_t >( 1), this, &iclassbase_streamdata_t::m_reloadcallback);
    if ( !this->m_reloadnotifier)
    {
        CBM_LogError( "unable to allocate reload notifier object.");
        return  this->init_error_();
    }

// sk:dbg    CBM_LogDebug( "endless=",_endless, "  [",this->name(),"]");
    this->m_buf = CBM_DefaultAllocator->construct_args< buffer_type >(
            static_cast< size_t >( 1), this->m_reloadnotifier, _data_time.from(), _sim_time.from(), _endless);
    if ( !this->m_buf)
    {
        CBM_LogError( "failed to allocate data buffer object");
        return  this->init_error_();
    }
    else
    {
        if ( ! this->m_buf->is_ok())
        {
            CBM_LogError( "errors occured during ",this->name()," buffer initialization");
            return  this->init_error_();
        }
    }

    crabmeat_assert( _bd);
    typename iproc_interface_table_base_t< _E >::config_type  r_conf;
    r_conf.simulation_time = this->simulation_time;
    r_conf.streamdata_time = this->streamdata_time;
    r_conf.offset = this->m_buf->data_offset();
    r_conf.stride = this->m_buf->data_stride();
    r_conf.boundary_data = _bd;
    lerr_t  rc_preset = this->m_reader->preset( this->record_item_names(), _REC_SIZE, &r_conf);
    if ( rc_preset)
    {
        CBM_LogError( "presetting reader failed");
        return  init_error_( rc_preset);
    }


    return  LDNDC_ERR_OK;
}
template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
lerr_t
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::init_error_(
        lerr_t  _err)
{
    this->set_status_flag( ICLASS_STATUS_ERROR);
    return  _err;
}


template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
lerr_t
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::reset_internal_state()
{
    crabmeat_assert( this->m_buf);
    crabmeat_assert( this->m_reader);
    lerr_t  rc_buffer_reset = this->m_buf->clear( ldndc::invalid_t< element_type >::value);
    if ( rc_buffer_reset)
    {
        return  rc_buffer_reset;
    }
    lerr_t  rc_reader_reset = this->m_reader->reset();
    if ( rc_reader_reset)
    {
        return  rc_reader_reset;
    }

    return  LDNDC_ERR_OK;
}
template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
lerr_t
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::update_internal_state(
               cbm::sclock_t const &  _clock)
{
    callback_argument_type  cb_arg = this->callback_argument_default;

    cb_arg.t = _clock.cycles();
    cb_arg.r = _clock.time_resolution();

    ltime_t  t( _clock.now(), _clock.fromto().to());
    cb_arg.n = static_cast< typename callback_argument_type::size_type >(
            std::max( (ldndc_uint64_t)t.period( t.from().dt()),
                        (ldndc_uint64_t)t.time_resolution()));

// sk:dbg   CBM_LogError( "t=",cb_arg.t, " r=",cb_arg.r, " n=",cb_arg.n);

    return  this->m_buf->try_trigger_refresh( &cb_arg);
}



template < typename _E, size_t _BUF_SIZE, size_t _REC_SIZE >
typename iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::callback_type::return_type
iclassbase_streamdata_t< _E, _BUF_SIZE, _REC_SIZE >::m_reloadcallback(
        typename callback_type::argument_type *  _cb_arg)
{
    if ( ! this->is_ok())
    {
        return  -1;
    }

    callback_return_type  rc_reload = this->m_buf->reload(
                _cb_arg, this->m_reader, this->record_item_names());

    if ( rc_reload == 0)
    {
        this->set_status_flag( ICLASS_STATUS_EXHAUSTED);
    }
    else if ( rc_reload < 0)
    {
// sk:TODO        this->set_status_flag( ICLASS_STATUS_ERROR);
    }
    else
    {
        this->unset_status_flag( ICLASS_STATUS_EXHAUSTED);
    }

    return  rc_reload;
}
}


#endif  /*  !LDNDC_INPUT_BASE_STREAMDATA_H_  */

