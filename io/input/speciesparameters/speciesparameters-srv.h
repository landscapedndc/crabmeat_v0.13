/*!
 * @brief
 *    container class for one complete species parameters set
 * 
 * @author
 *      Steffen Klatt,
 *      Edwin Haas
 */

#ifndef  LDNDC_INPUT_SPECIESPARAMS_SERVER_H_
#define  LDNDC_INPUT_SPECIESPARAMS_SERVER_H_


#include  "crabmeat-common.h"
#include  "input/ic-srv.h"
#include  "io/source-handle-srv.h"
#include  "constants/lconstants-plant.h"
#include  "string/cbm_string.h"

#include  "speciesparameters/speciesparameterstypes.h"

namespace  ldndc
{
namespace  speciesparameters
{
class  CBM_API  input_class_speciesparameters_srv_t  :  public input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(speciesparameters,ICLASS_FLAG_STATE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update_internal_state(
                cbm::sclock_t const &);

    private:
        lerr_t  update();

        size_t  mnemonic_to_index(
                   char const *) const;

        int  phylogenetic_merge(
                speciesparameters_set_t * /*buffer*/,
                       size_t /*index*/, int /*ancestor counter*/) const;
    public:
        int  copy_valid(
                speciesparameters_set_t const *);

        speciesparameters_set_t  get_default_parameters() const;
        speciesparameters_set_t  get_parameters_as_read(
                char const *  /*mnemonic*/) const;
        speciesparameters_set_t  operator[](
                std::string const & /*mnemonic*/) const;
        speciesparameters_set_t  operator[](
                char const * /*mnemonic*/) const;
        speciesparameters_set_t  at(
                size_t /*index*/) const;

        size_t  parameterized_species_cnt()
        const
        {
            return  p_db_size;
        }

        /*!
         * @brief
         *      tells if species type @p type inherits from
         *      other type @p family
         *
         * @returns
         *      true, if species @p type was found and inherits
         *      from @p family,
         *      false otherwise.
         */
        bool  is_family(
                char const * /*type*/, char const * /*family*/) const;

    private:
        /* holds only default properties that apply when no species specific value is known */
        parameterized_species_t  p_default;
        /* parameterized species database */
        parameterized_species_t *  p_db;
        /* number of parameterized species */
        size_t  p_db_size;

        ldndc::source_handle_srv_t  sh;

        void  free_species_properties();
        lerr_t  read_species_properties(
                source_handle_srv_t const *);
        lerr_t  check_default_properties(
                int /*properties read*/, int /*properties seen*/);
        lerr_t  precompute_parent_indexes();

        lerr_t  semantic_check_() const;
};
}}

#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  speciesparameters
{
class  CBM_API  input_class_speciesparameters_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(speciesparameters,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_SPECIESPARAMS_SERVER_H_  */

