/*!
 * @author
 *      Steffen Klatt,
 *      Edwin Haas,
 */

#include  "input/speciesparameters/speciesparameters-srv.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_speciesparameters.h"

#include  "string/lstring-compare.h"
#include  "string/lstring-convert.h"
#include  "string/lstring-transform.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"

namespace  ldndc{ namespace  speciesparameters
{
LDNDC_INPUT_SRV_CLASS_DEFN(speciesparameters)
input_class_speciesparameters_srv_t::input_class_speciesparameters_srv_t()
        : input_class_srv_base_t( invalid_lid),
          p_db( NULL), p_db_size( 0)
{
    this->p_default.m =
        speciesparameters_meta_t( NULL, NULL, species::SPECIES_GROUP_NONE);
}
input_class_speciesparameters_srv_t::input_class_speciesparameters_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id),
          p_db( NULL), p_db_size( 0)
{
}

input_class_speciesparameters_srv_t::~input_class_speciesparameters_srv_t()
{
    this->free_species_properties();
}
void
input_class_speciesparameters_srv_t::free_species_properties()
{
    CBM_DefaultAllocator->destroy( this->p_db, this->p_db_size);
    this->p_db = NULL;
    this->p_db_size = 0;
}



lerr_t
input_class_speciesparameters_srv_t::initialize(
        source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    this->sh = *static_cast< source_handle_srv_t const * >( _source_handle);

    lerr_t  rc_update = this->update();
    if ( rc_update)
    {
        return  LDNDC_ERR_FAIL;
    }

    this->set_initialized();
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_speciesparameters_srv_t::update_internal_state(
        cbm::sclock_t const &)
{
    return  this->update();
}
lerr_t
input_class_speciesparameters_srv_t::update()
{
    set_input_class_reader_from_handle(speciesparameters, &this->sh);

    if ( !dynamic_cast< iproc_provider_t * >( this_reader)->data_available())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_read = this->read_species_properties( &this->sh);
    if ( rc_read)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
input_class_speciesparameters_srv_t::read_species_properties(
        source_handle_srv_t const *  _source_handle)
{
    set_input_class_reader(speciesparameters);

    int  default_props_read( -1), default_props_seen( -1);
    lerr_t  rc_species_defaults = this_reader->get_species_default_properties(
            p_default.p, SPECIESPARAMETERS_NAMES, SPECIESPARAMETERS_TYPES, SPECIESPARAMETERS_CNT, &default_props_read, &default_props_seen);
    if ( rc_species_defaults)
    {
        CBM_LogError( "read_species_properties(): ",
                "failed to read species default properties");
        return  LDNDC_ERR_FAIL;
    }

    this->free_species_properties();

    lerr_t  rc_read = this_reader->get_all_species_properties( 
            &this->p_db, &this->p_db_size,
            SPECIESPARAMETERS_NAMES, SPECIESPARAMETERS_TYPES, SPECIESPARAMETERS_CNT,
            NULL, NULL, NULL, NULL);
    CBM_LogDebug( "read all species available  [#species=",this->p_db_size,"]");
    if ( rc_read)
    {
        CBM_LogError( "read_species_properties(): ",
                "failed to read species properties",
                "  [id=",this->object_id(),",#species=",this->p_db_size,"]");
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_parents = this->precompute_parent_indexes();
    if ( rc_parents)
    {
        CBM_LogError( "read_species_properties(): ",
                "failed to precompute parent indexes  [id=",this->object_id(),"]");
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_check = this->semantic_check_();
    if ( rc_check)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
input_class_speciesparameters_srv_t::check_default_properties(
        int  _default_props_read, int  /*_default_props_seen*/)
{
    if ( _default_props_read == 0)
    {
        CBM_LogWarn( "check_default_properties(): ",
                "default species parameters set found but has no valid parameter");
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc = LDNDC_ERR_OK;
    for ( size_t  k = 0;  k < static_cast< size_t >( SPECIESPARAMETERS_CNT);  ++k)
    {
        if ( !this->p_default.p[k].is_valid( SPECIESPARAMETERS_TYPES[k]))
        {
            CBM_LogWrite( "possibly missing: ", SPECIESPARAMETERS_NAMES[k]);
            rc = LDNDC_ERR_FAIL;
        }
    }
    unsigned int  diff( (unsigned int)SPECIESPARAMETERS_CNT - (unsigned int)_default_props_read);
    if ( diff || rc)
    {
        CBM_LogWarn( "check_default_properties(): ",
                "some parameter have not been initialized. check parameter file! [n=",diff,"]");
    }

    return  LDNDC_ERR_OK;
}

static
lerr_t
speciesparameters_check_if_values_within_minmax_boundaries_s(
    speciesparameters_set_t const *  _speciesparameters)
{
    CRABMEAT_FIX_UNUSED(_speciesparameters);
    int  err = 0;
#   include  "speciesparameters_check_minmax.cpp.inc"
    return  err ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}


#define  SPECIESPARAM_REPORT_ERROR(__p__,__errmsg__)                \
    CBM_LogError( "species parameters (\"",(__p__).TYPE(),"\"): ", __errmsg__);    \
    rc = LDNDC_ERR_FAIL/*;*/
lerr_t
input_class_speciesparameters_srv_t::semantic_check_()
const
{
    lerr_t  rc = LDNDC_ERR_OK;

    for ( size_t  k = 0;  k < this->p_db_size;  ++k)
    {
        speciesparameters_set_t const  p =
            this->operator[]( this->p_db[k].m.mnemonic);

        lerr_t  rc_species =
            speciesparameters_check_if_values_within_minmax_boundaries_s( &p);
        if ( rc_species)
        {
            CBM_LogError( "there were errors for species '",this->p_db[k].m.mnemonic,"'");
            rc = rc_species;
        }

        if ( cbm::flt_greater( p.FRACTION_ROOT() + p.FRACTION_FRUIT() + p.FRACTION_FOLIAGE(), 1.0))
        {
            SPECIESPARAM_REPORT_ERROR( p, "species compartent partitions greater one");
        }
        
        if ( cbm::flt_greater( p.NC_FRUIT_MIN(), p.NC_FRUIT_MAX()))
        {
            SPECIESPARAM_REPORT_ERROR( p, "inconsistent species nitrogen concentrations of fruit (NC_FRUIT_MIN > NC_FRUIT_MAX)");
        }
        
        if ( cbm::flt_greater( p.NC_FOLIAGE_MIN(), p.NC_FOLIAGE_MAX()))
        {
            SPECIESPARAM_REPORT_ERROR( p, "inconsistent species nitrogen concentrations of foliage (NC_FOLIAGE_MIN > NC_FOLIAGE_MAX)");
        }
        
        if ( cbm::flt_greater( p.NC_FINEROOTS_MIN(), p.NC_FINEROOTS_MAX()))
        {
            SPECIESPARAM_REPORT_ERROR( p, "inconsistent species nitrogen concentrations of fineroots (NC_FINEROOTS_MIN > NC_FINEROOTS_MAX)");
        }
        
        if ( cbm::flt_greater( p.NC_STRUCTURAL_TISSUE_MIN(), p.NC_STRUCTURAL_TISSUE_MAX()))
        {
            SPECIESPARAM_REPORT_ERROR( p, "inconsistent species nitrogen concentrations of structural tissue (NC_STRUCTURAL_TISSUE_MIN > NC_STRUCTURAL_TISSUE_MAX)");
        }
    }

    return  rc;
}


size_t
input_class_speciesparameters_srv_t::mnemonic_to_index(
        char const *  _mnemonic)
const
{
    crabmeat_assert( _mnemonic);

    for ( size_t  s = 0;  s < this->p_db_size;  ++s)
    {
        if ( cbm::is_equal_i( _mnemonic, this->p_db[s].m.mnemonic))
        {
            return  s;
        }
    }
    return  invalid_t< size_t >::value;
}


speciesparameters_set_t
input_class_speciesparameters_srv_t::operator[](
        std::string const &  _mnemonic)
const
{
    return  this->operator[]( _mnemonic.c_str());
}
int
input_class_speciesparameters_srv_t::phylogenetic_merge(
        speciesparameters_set_t *  _p, size_t  _k, int  _a)
const
{
    if ( _p && cbm::is_valid( _k))
    {
#ifdef  _DEBUG
        if ( this->p_db[_k].m.group == species::SPECIES_GROUP_NONE)
        {
            CBM_LogFatal( "[BUG]  species has group 'none'",
                        "  [species=",this->p_db[_k].m.mnemonic,"]");
        }
#endif
        if ( this->p_db[_k].m.group == species::SPECIES_GROUP_ANY)
        {
            return  this->phylogenetic_merge(
                NULL, invalid_t< size_t >::value, _a);
        }
        size_t const  k = this->p_db[_k].parent_index;
        this->phylogenetic_merge( _p, k, _a+1);

        _p->copy_valid( this->p_db[_k].p);
    }

    return  _a+1;
}
speciesparameters_set_t
input_class_speciesparameters_srv_t::operator[](
        char const *  _mnemonic)
const
{
    size_t const  k = this->mnemonic_to_index( _mnemonic);
    return  this->at( k);
}
speciesparameters_set_t
input_class_speciesparameters_srv_t::at(
        size_t  _k)
const
{
    if ( cbm::is_valid( _k))
    {
        speciesparameters_set_t  p( &this->p_db[_k].m);
        p.copy_valid( this->p_default.p);

        /* merge parameters of its ancestors */
        int const  a = this->phylogenetic_merge( &p, _k, 0);
        CRABMEAT_FIX_UNUSED(a);

        return  p;
    }
    else
    {
        speciesparameters_set_t  p;
        p.copy_valid( this->p_default.p);

        return  p;
    }
}

bool
input_class_speciesparameters_srv_t::is_family(
                char const *  _mnemonic, char const *  _family)
const
{
    size_t  k_uint = this->mnemonic_to_index( _mnemonic);
    if ( cbm::is_valid( k_uint))
    {
        int  k = static_cast< int >( k_uint);
        while ( k != -1)
        {
            if ( cbm::is_equal_i(
                    this->p_db[k].m.mnemonic, _family))
            {
                return  true;
            }
            k = this->p_db[k].parent_index;
        }
    }
    return  false;
}
lerr_t
input_class_speciesparameters_srv_t::precompute_parent_indexes()
{
    lerr_t  rc = LDNDC_ERR_OK;
    for ( size_t  s = 0;  s < this->p_db_size;  ++s)
    {
        this->p_db[s].parent_index = -1;

        if ( cbm::is_equal_i(
                this->p_db[s].m.mnemonic, species::SPECIES_NAME_NONE)
            || cbm::is_equal_i(
                this->p_db[s].m.parent, species::SPECIES_GROUP_NAMES[species::SPECIES_GROUP_NONE]))
        {
            /* no op */
        }
        else
        {
            for ( size_t  p = 0;  p < this->p_db_size;  ++p)
            {
                if ( cbm::is_equal_i(
                            this->p_db[s].m.parent, this->p_db[p].m.mnemonic))
                {
                    this->p_db[s].parent_index = p;
                    break;
                }
            }

            if ( this->p_db[s].parent_index == -1)
            {
                CBM_LogError( "precompute_parent_indexes(): ",
                        "failed to find parent for '",this->p_db[s].m.mnemonic,"'");
                rc = LDNDC_ERR_FAIL;
            }
        }
    }
    return  rc;
}

LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(speciesparameters)
lerr_t
input_class_speciesparameters_global_srv_t::initialize(
                input_source_srv_base_t const *,
        ltime_t const *)
{
    /* guard */
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    // nothing here

    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

