/*!
 * @brief
 *    creates, initializes and stores one set of input classes
 *
 * @author
 *    steffen klatt, created on: oct 09, 2011
 */

#include  "input/ic-pool.h"
#include  "input/ic-srv.h"
#include  "input/ic-factory-srv.h"
#include  "input/setup/setup-srv.h"

#include  "time/cbm_time.h"
#include  "utils/cbm_utils.h"

#include  "hash/cbm_hash.h"
#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"
#include  "cbm_rtcfg.h"

#define  islot(c) (cbm_is_aic(c) ? 0 : (cbm_is_dic(c) ? 1 : -1 /*ouch!*/))

LDNDC_SERVER_OBJECT_DEFN(ldndc::input_class_pool_t)
ldndc::input_class_pool_t::input_class_pool_t()
        : cbm::server_object_t()
{
    this->m_clear();
}


ldndc::input_class_pool_t::~input_class_pool_t()
{
    this->m_clear();
}

lerr_t
ldndc::input_class_pool_t::unmount_all_input_classes()
{
    this->m_clear();
    return  LDNDC_ERR_OK;
}


#include  "io/source-handle-srv.h"
lerr_t
ldndc::input_class_pool_t::mount_input_class_(
        ldndc::source_handle_srv_base_t const *  _handle,
        char const *  _type)
{
    cbm::source_descriptor_t const *  sd =
                _handle->get_source_descriptor();

    crabmeat_assert( sd->block_id != invalid_lid);
    crabmeat_assert( cbm_is_ic(_type));

    ldndc::hash_ic_t  ic_hash;
    node_ic_t  ic_node; 
    lerr_t  rc_hash = this->hash_ic( &ic_hash, sd, _type, &ic_node);
    if ( rc_hash)
        { return  LDNDC_ERR_FAIL; }

    if ( this->m_ic[islot(_type)].find( ic_hash) == this->m_ic[islot(_type)].end())
    {
        ic_node.ic = NULL;
        if ( this->create_input_class_( sd->block_id, _type, &ic_node.ic) == LDNDC_ERR_OK)
        {
            if ( ic_node.ic->initialize( _handle) == LDNDC_ERR_OK)
            {
                CBM_LogDebug( "insert: H=",ic_hash, "  \"",ic_node.hs,"\"  sz=",ic_node.hs.length());
                std::pair< ic_iterator, bool >  ins =
                        m_ic[islot(_type)].insert( ic_pair_type( ic_hash, ic_node));
                if ( ins.second == false)
                {
                    CBM_LogError( "failed to insert input class into pool container");
                    return  LDNDC_ERR_FAIL;
                }
                if ( CBM_LibRuntimeConfig.clk)
                {
                    lerr_t  rc_update = ic_node.ic->update_internal_state( *CBM_LibRuntimeConfig.clk);
                    if ( rc_update)
                    {
                        CBM_LogError( "initial state update failed  [id=",*sd,",class=",
                                _type,",clock=",CBM_LibRuntimeConfig.clk->to_string(),"]");
                        return  rc_update;
                    }
                }
            }
            else
            {
                CBM_LogError( "initializing core input class failed [id=",*sd,",class=",_type,"]");
                if ( find_input_class_srv_factory( 'C',
                            ic_node.ic->input_class_type()))
                {
                    find_input_class_srv_factory( 'C',
                        ic_node.ic->input_class_type())->destruct( ic_node.ic);
                }
                ic_node.ic = NULL;
                return  LDNDC_ERR_OBJECT_INIT_FAILED;
            }
        }
        else
        {
            CBM_LogError( "instantiating core input class failed [id=",*sd,",class=",_type,"]");
            return  LDNDC_ERR_OBJECT_CREATE_FAILED;
        }
    }
    else
    {
        /* this input class is already set up */
        ic_const_iterator  this_ic_node =
                this->m_ic[islot(_type)].find( ic_hash);
        if ( !this_ic_node->second.ic)
        {
            CBM_LogError( "input class in pool container was null  [id=",*sd,"]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        /* check for hash collision */
        if ( ic_node.hs != this_ic_node->second.hs)
        {
 /*TODO  collision detection and handling */
            CBM_LogFatal( "[BUG]  detected hash collision for input class pool container;",
                " please notify the LDNDC developers  [source=",*sd,",collides",
               " with=[",_type,":",this_ic_node->second.ic->object_id(),"]]");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::input_class_pool_t::mount_input_class(
                ldndc::source_handle_srv_base_t const *  _handle,
        char const *  _type)
{
    /*
     * only one thread may change the pool at any time. because
     * input classes are triggering mounts of their dependencies
     * the same thread may enter this function without having
     * released the lock. for this reason it is necessary to use
     * a nested lock
     */
    lerr_t  rc_mount = this->mount_input_class_( _handle, _type);
    return  rc_mount;
}
lerr_t
ldndc::input_class_pool_t::create_input_class_(
        lid_t const &  _id, char const *  _type,
        input_class_srv_base_t **  _input_class)
{
    if ( ! _input_class)
        { return  LDNDC_ERR_FAIL; }

    if ( !cbm_is_ic(_type))
    {
        CBM_LogError( "requesting invalid input type [type=",_type,"]");
        return  LDNDC_ERR_INVALID_ID;
    }

    if ( find_input_class_srv_factory( 'C', _type))
    {
        *_input_class =
            find_input_class_srv_factory( 'C', _type)->construct( _id);
        return  LDNDC_ERR_OK;
    }
    CBM_LogError( "no creator function found for input class \"",_type,"\"");
    return  LDNDC_ERR_FAIL;
}


ldndc::input_class_srv_base_t const *
ldndc::input_class_pool_t::get_input_class(
        cbm::source_descriptor_t const *  _source_info,
        char const *  _type)
const
{
    return  const_cast< ldndc::input_class_pool_t * >(
                this)->get_input_class( _source_info, _type);
}
ldndc::input_class_srv_base_t *
ldndc::input_class_pool_t::get_input_class(
        cbm::source_descriptor_t const *  _source_info,
        char const *  _type)
{
    crabmeat_assert( _source_info);
    CBM_LogDebug( "get ",*_source_info,
        " | container sizes  AIC=",this->m_ic[0].size(),",DIC=",this->m_ic[1].size());

    if ( !cbm_is_ic(_type))
    {
        CBM_LogError( "got invalid class type [type=",_type,"]");
        return  NULL;
    }

    if ( _source_info->block_id == invalid_lid)
    {
        CBM_LogError( "got invalid id [class=",_type,"]");
        return  NULL;
    }

    ldndc::hash_ic_t  ic_hash;
    node_ic_t  ic_node;
    lerr_t  rc_hash = this->hash_ic(
        &ic_hash, _source_info, _type, &ic_node);
    if ( rc_hash)
        { return  NULL; }

    ic_const_iterator  this_ic_node =
            this->m_ic[islot(_type)].find( ic_hash);
    if ( this_ic_node == this->m_ic[islot(_type)].end())
        { return  NULL; }

    crabmeat_assert( this_ic_node->second.ic->is_initialized());
    return  static_cast< input_class_srv_base_t * >( this_ic_node->second.ic);
}

lerr_t
ldndc::input_class_pool_t::hash_ic( ldndc::hash_ic_t *  _hash,
        cbm::source_descriptor_t const *  _info,
        char const *  _type, node_ic_t *  _ic_node)
const
{
    if ( cbm_is_aic(_type))
        { return  this->hash_aic( _hash, _info, _type, _ic_node); }
    else if ( cbm_is_dic(_type))
        { return  this->hash_dic( _hash, _info, _type, _ic_node); }

    return  LDNDC_ERR_RUNTIME_ERROR;
}

lerr_t
ldndc::input_class_pool_t::hash_aic( ldndc::hash_ic_t *  _hash,
        cbm::source_descriptor_t const *  _source_info,
        char const *  _type, node_ic_t *  _ic_node)
const
{
    if ( !_hash || !_source_info || cbm::is_invalid( _source_info->block_id))
        { return  LDNDC_ERR_FAIL; }

    /* generate key with pattern
     *   SOURCEID ID
     * (without spaces) which is than hashed. */

    char  hashsource[HASH_SOURCE_BYTESIZE];
    int  hashsource_len = ldndc::hash_ic( _hash, _source_info->block_id,
        _source_info->source_identifier, _type, hashsource);
    if ( hashsource_len <= 0)
        { return  LDNDC_ERR_FAIL; }
    if ( _ic_node)
        { _ic_node->hs.assign( hashsource, hashsource_len); }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::input_class_pool_t::hash_dic( ldndc::hash_ic_t *  _hash,
        cbm::source_descriptor_t const *  _kernel_info,
        char const *  _type, node_ic_t *  _ic_node)
const
{
    if ( !_hash || cbm::is_invalid( _kernel_info->block_id))
        { return  LDNDC_ERR_FAIL; }

    char const **  dependencies = NULL;
    int  n_dependencies =
        ic_dependencies( _type, &dependencies);
    if ( n_dependencies <= 0)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    /* preallocated hashsource buffer for 5 dependencies */
    static size_t const  hashsource_prealloc_sz =
        HASH_SOURCE_BYTESIZE /*type name*/+ 5*HASH_SOURCE_BYTESIZE;
    char  hashsource_prealloc[hashsource_prealloc_sz];

    size_t const  hashsource_sz =
        (1+n_dependencies) * HASH_SOURCE_BYTESIZE;
    char *  hashsource = hashsource_prealloc;
    if ( hashsource_sz > hashsource_prealloc_sz)
        { hashsource = (char *)CBM_Allocate( hashsource_sz); }
    if ( !hashsource)
        { return  LDNDC_ERR_NOMEM; }

    /* generate key with pattern
     *   TYPE DEP1 ID1 DEP2 ID2 .. DEPn IDn
     * for n dependencies (without spaces) which
     * is than hashed. */

    char *  h_ptr = hashsource;
    size_t const  type_len = cbm::strlen( _type);
    cbm::mem_cpy( h_ptr, _type, type_len);
    h_ptr += type_len;

    for ( int  aic = 0;  aic < n_dependencies;  ++aic)
    {
        char const *  aic_name = dependencies[aic];

        /* check if input class of type <aic> was given for kernel <_id> */
        cbm::source_descriptor_t  source_info;
        lid_t const  aic_id = this->get_source_descriptor(
                        &source_info, _kernel_info, /*_attr,*/ aic_name);
        if ( aic_id == invalid_lid)
        {
            /* insufficient information or data .. we give up */
            if ( hashsource_sz > hashsource_prealloc_sz)
                { CBM_Free( hashsource); }
            return  LDNDC_ERR_FAIL;
        }

        int const  len = ldndc::hash_ic( NULL,
            source_info.block_id, source_info.source_identifier, _type, h_ptr);
        if ( len < 0)
        {
            if ( hashsource_sz > hashsource_prealloc_sz)
                { CBM_Free( hashsource); }
            return  LDNDC_ERR_RUNTIME_ERROR;
        }

        h_ptr += len;
    }

    if ( _ic_node)
        { _ic_node->hs.assign( hashsource, h_ptr-hashsource); }
    *_hash = static_cast< ldndc::hash_ic_t >(
        cbm::hash::sha1_uint64( hashsource, h_ptr-hashsource));
    if ( hashsource_sz > hashsource_prealloc_sz)
        { CBM_Free( hashsource); }

#ifdef  _DEBUG
// sk:dbg    cbm::strclrnul( hash_source, HASH_SOURCE_BYTESIZE_DIC);
// sk:dbg    CBM_LogDebug( "dic-source=\"",hash_source, "\"  hash=",*_hash);
#endif

    if ( cbm::is_invalid( *_hash))
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}

lid_t
ldndc::input_class_pool_t::get_source_descriptor(
        cbm::source_descriptor_t *  _source_info,
        cbm::source_descriptor_t const *  _kernel_info,
        char const *  _type)
const
{
    if ( cbm_is_aic(_type))
    {
        crabmeat_assert( _kernel_info);
        setup::input_class_setup_srv_t const *  ic_setup =
                static_cast< setup::input_class_setup_srv_t const * >(
                        this->get_input_class( _kernel_info, "setup"));
        if ( !ic_setup)
        {
            CBM_LogError( "failed to get setup input..  ",*_kernel_info);
            /* hmmm.. we cannot help you, if you do not provide dependencies */
            return  invalid_lid;
        }
        lid_t const  aic_id = ic_setup->block_consume( _source_info, _type, NULL);
        crabmeat_assert( cbm::is_valid( aic_id));
// sk:dbg        CBM_LogDebug( "established aic [",_type,"] for ",*_kernel_info," -> ",*_source_info);

        return  aic_id;
    }
    else if ( cbm_is_dic(_type))
    {
        *_source_info = *_kernel_info;
// sk:dbg        CBM_LogDebug( "established dic [",_type,"] for ",*_kernel_info," -> ",*_source_info);

        return  _kernel_info->block_id;
    }
    else
        { CBM_LogFatal( "[BUG]  type=",_type, " -- ", *_source_info); }
    return  invalid_lid;
}


/* global input classes */

#include  "io/input-source.h"
lerr_t
ldndc::input_class_pool_t::mount_input_class_global(
        ldndc::input_source_srv_base_t const *  _iobj,
        char const *  _type,
        ltime_t const *  _stream_time)
{
    lerr_t  rc_mountglobal =
        this->mount_input_class_global_( _iobj, _type, _stream_time);
    return  rc_mountglobal;
}
lerr_t
ldndc::input_class_pool_t::mount_input_class_global_(
        ldndc::input_source_srv_base_t const *  _iobj,
        char const *  _type, ltime_t const *  _stream_time)
{
    lid_t const  this_stream_id = _iobj->object_id();

    crabmeat_assert( this_stream_id != invalid_lid);
    crabmeat_assert( cbm_is_aic(_type));

    input_class_global_srv_base_t *  input_class = NULL;

    cbm::string_t  gc_key = _type;
    gc_key << "." << this_stream_id;

    /* silently ignore if ID exists.. */
    if ( this->m_gc.find( gc_key.str()) == this->m_gc.end())
    {
        if ( this->create_input_class_global_( this_stream_id, _type, &input_class) == LDNDC_ERR_OK)
        {
            if ( input_class->initialize( _iobj, _stream_time) == LDNDC_ERR_OK)
            {
                node_gc_t  node_global_ic = { input_class};
                this->m_gc.insert( gc_pair_type( gc_key.str(), node_global_ic));
// sk:TODO  check return value of insert method
            }
            else
            {
                CBM_LogError( "initializing input class object failed",
                            "  [stream-id=",this_stream_id,",class=",_type,"]");
                return  LDNDC_ERR_OBJECT_INIT_FAILED;
            }
        }
        else
        {
            CBM_LogError( "instantiating input class object failed",
                        "  [stream-id=",this_stream_id,",class=",_type,"]");
            return  LDNDC_ERR_OBJECT_CREATE_FAILED;
        }
    }
    else
        { /* this input class is already set up */ }

    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::input_class_pool_t::create_input_class_global_(
        lid_t const &  _stream_id,
        char const *  _type,
        input_class_global_srv_base_t **  _input_class)
{
    if ( ! _input_class)
        { return  LDNDC_ERR_FAIL; }

    if ( !cbm_is_ic(_type))
    {
        CBM_LogError( "requesting invalid input type [type=",_type,"]");
        return  LDNDC_ERR_INVALID_ID;
    }

    if ( find_input_class_srv_factory( 'L', _type))
    {
        *_input_class = find_input_class_srv_factory(
                'L', _type)->construct_global( _stream_id);
        return  LDNDC_ERR_OK;
    }
    CBM_LogError( "no creator function found",
            " for input class \"",_type,"\"");
    return  LDNDC_ERR_FAIL;
}



ldndc::input_class_global_srv_base_t const *
ldndc::input_class_pool_t::get_input_class_global(
        lid_t const &  _stream_id,
        char const *  _type)
const
{
    return  const_cast< ldndc::input_class_pool_t * >( this)->get_input_class_global( _stream_id, _type);
}
ldndc::input_class_global_srv_base_t *
ldndc::input_class_pool_t::get_input_class_global(
        lid_t const &  _stream_id,
        char const *  _type)
{
    if ( !cbm_is_aic(_type))
    {
        CBM_LogError( "got invalid class type [type=",_type,"]");
        return  NULL;
    }

    if ( _stream_id == invalid_lid)
    {
        CBM_LogFatal( "backtrace...");
        CBM_LogError( "got invalid id [class=",_type,"]");
        return  NULL;
    }

    cbm::string_t  gc_key = _type;
    gc_key << "." << _stream_id;

    gc_const_iterator  ic = this->m_gc.find( gc_key.str());
    if ( ic == this->m_gc.end())
    {
// sk:off        CBM_LogError( "global input class",_type,"matching stream-ID",_stream_id,"not found");
        return  NULL;
    }
    crabmeat_assert( ic->second.ic->is_initialized());
    return  ic->second.ic;
}


lerr_t
ldndc::input_class_pool_t::m_clear()
{
    for ( int  t = 0; t < 2; ++t)
    {
        for( ic_iterator  i = this->m_ic[t].begin();
                i != this->m_ic[t].end();  i++)
        {
            if ( i->second.ic)
                { find_input_class_srv_factory( 'C',
                    i->second.ic->input_class_type())->destruct( i->second.ic); }
        }
        this->m_ic[t].clear();
    }

    for( gc_iterator  i = this->m_gc.begin();
                i != this->m_gc.end();  i++)
    {
        if ( i->second.ic)
            { find_input_class_srv_factory( 'L',
                i->second.ic->input_class_type())->destruct_global( i->second.ic); }
    }
    this->m_gc.clear();

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::input_class_pool_t::reset_input(
        cbm::source_descriptor_t const *  _sd,
        char const *  _input_type)
{
    return  this->update_input( _sd, _input_type, NULL);
}
lerr_t
ldndc::input_class_pool_t::reset_input()
{
    return  this->update_input( NULL);
}


lerr_t
ldndc::input_class_pool_t::update_input(
        cbm::source_descriptor_t const *  _sd, char const *  _type,
        cbm::sclock_t const *  _clock)
{
    ldndc::hash_ic_t  ic_hash;
    lerr_t const  rc_hash = this->hash_ic( &ic_hash, _sd, _type);
    if ( rc_hash)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    ic_iterator  it = this->m_ic[islot(_type)].find( ic_hash);
    if ( it != this->m_ic[islot(_type)].end())
    {
        input_class_srv_base_t *  ic = it->second.ic;
        if ( !ic)
            { return  LDNDC_ERR_RUNTIME_ERROR; }

        return  _clock ? ic->update_internal_state( *_clock) : ic->reset_internal_state();
    }

    return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
}
lerr_t
ldndc::input_class_pool_t::update_input(
        cbm::sclock_t const *  _clock)
{
    int  fails = 0;

    /* DICs need to be updated after AICs */
    for ( size_t  t = 0;  t < 2;  ++t)
    {
        /* execute sequentially in order to avoid thrashing */
        for ( ic_iterator  it = this->m_ic[t].begin();
                it != this->m_ic[t].end();  ++it)
        {
            input_class_srv_base_t *  ic = it->second.ic;
            if ( !ic)
                { continue; }

            lerr_t const  u_err = 
                _clock ? ic->update_internal_state( *_clock) : ic->reset_internal_state();
            if ( u_err)
                { ++fails; }
        }
    }

    return  fails == 0 ? LDNDC_ERR_OK : LDNDC_ERR_OBJECT_INIT_FAILED;
}

