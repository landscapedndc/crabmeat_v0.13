/*!
 * @brief
 *    implementation
 *
 * @author
 *    Steffen Klatt (created on: dec 04, 2011),
 *    Edwin Haas,
 */

#define  LDNDC_SPECIES_NO_UNDEF_T
#include  "input/species/species-srv.h"

#include  "io/source-handle-srv.h"

#include  "log/cbm_baselog.h"
#include  "memory/cbm_mem.h"

#include  "event/events/event-harvest.h"
#include  "event/events/event-plant.h"
#include  "event/events/event-reparameterizespecies.h"

/* if this macro is set, allow buffer to shrink */
//#define  LDNDC_SPECIESBUFFER_SHRINK  1

namespace  ldndc{ namespace  species
{
size_t const  species_buffer_t::invalid_slot = invalid_t< size_t >::value;
species_buffer_t::species_buffer_t()
            : capacity_( 0),
              size_( 0),
              s_buf_( NULL)
{
}


species_buffer_t::~species_buffer_t()
{
    (void)this->reset();
}


lerr_t
species_buffer_t::reset()
{
    if ( this->s_buf_)
    {
        crabmeat_assert( this->capacity_ > 0);
        CBM_DefaultAllocator->destroy( this->s_buf_, this->capacity_);
        this->s_buf_ = NULL;
    }

    this->size_ = 0;
    this->capacity_ = 0;

    return  LDNDC_ERR_OK;
}

size_t
species_buffer_t::find_unused_slot()
{
    for ( size_t  c = 0;  c < this->capacity_;  ++c)
    {
        int  mapped = 0;
        for ( size_t  r = 0;  r < this->size_;  ++r)
        {
            if ( this->s_buf_[r].slot == c)
            {
                mapped = 1;
                break;
            }
        }

        if ( mapped == 0)
        {
            return  c;
        }
    }
    return  invalid_slot;
}

size_t
species_buffer_t::slot_hi() const
{
    if ( this->size() == 0)
        { return  0; }

    size_t  S_HI = 0;
    for ( size_t  c = 0;  c < this->capacity();  ++c)
    {
        size_t const  s_hi = this->s_buf_[c].slot;
        if (( s_hi != this->invalid_slot) && ( s_hi > S_HI))
            { S_HI = s_hi; }
    }
    return  S_HI + 1;
}


lerr_t
species_buffer_t::resize(
                size_t  _new_capacity)
{
    crabmeat_assert( _new_capacity > 0);
    CBM_LogDebug( "resizing species buffer  [capacity:current=",this->capacity_,",new=",_new_capacity,"]");
#ifndef  LDNDC_SPECIESBUFFER_SHRINK
    if ( _new_capacity <= this->capacity_)
    {
        /* no resize required if size is sufficient */
        return  LDNDC_ERR_OK;
    }
#else
    CBM_LogDebug( "attempt to shrink species buffer  [capacity:current=",this->capacity_,",new=",_new_capacity,"]");
#endif  /*  !LDNDC_SPECIESBUFFER_SHRINK  */
    if ( _new_capacity < this->size_)
    {
        /* not enough space to hold existing species */
        CBM_LogError( "not enough space to hold existing species  [capacity:current=",this->size_,",new=",_new_capacity,"]");
        return  LDNDC_ERR_RUNTIME_ERROR;
    }

    if (( _new_capacity == 0) && ( this->capacity_ > 0))
    {
// sk:dbg        CBM_LogDebug( "resetting species buffer..");
        return  this->reset();
    }

// sk:dbg    CBM_LogDebug( "allocating new species buffer slots  [capacity=",_new_capacity,"]");
    species_buffer_slot_t *  new_slots = CBM_DefaultAllocator->construct< species_buffer_slot_t >( _new_capacity);
    if ( !new_slots)
    {
        CBM_LogError( "nomem");
        return  LDNDC_ERR_NOMEM;
    }

    /* copy left-aligned to new buffer */
    for ( size_t  r = 0;  r < this->size_;  ++r)
    {
// sk:dbg        CBM_LogDebug( "copying slot ",T(r)," to ",r);
        new_slots[r] = this->s_buf_[T(r)];
        new_slots[r].slot = r;
// sk:dbg        CBM_LogDebug( "left-aligning in new species buffer  [species=",new_slots[r].species.name(),"]");
    }
    /* initialize new slot(s) */
// sk:dbg    CBM_LogDebug( "initializing new slots ",this->size_," through ",_new_capacity-1);
    for ( size_t  r = this->size_;  r < _new_capacity;  ++r)
    {
        new_slots[r].state = SPECIES_STATE_NONE;
        new_slots[r].slot = invalid_slot;
    }

    if ( this->s_buf_)
    {
// sk:dbg        CBM_LogDebug( "deallocating old species buffer slots  [capacity=",this->capacity_,"]");
        CBM_DefaultAllocator->destroy( this->s_buf_, this->capacity_);
    }
    this->s_buf_ = new_slots;
    this->capacity_ = _new_capacity;


    return  LDNDC_ERR_OK;
}

size_t
species_buffer_t::species_mount(
        event::__event_class_name(plant) const &  _ev,
        cbm::sclock_t const &  _clock,
        speciesparameters::speciesparameters_set_t const &  _param_set)
{
    if ( this->size_ == this->capacity_)
    {
        lerr_t  rc_resize = this->resize( this->capacity_ + 1);
        CBM_LogDebug( "resized species buffer  [capacity=",this->capacity_,",status=",rc_resize,"]");
        if ( rc_resize)
        {
            CBM_LogError( "error occured while resizing species buffer");
            return  invalid_slot;
        }
    }

    /* by definition next available slot is at current size */
    size_t const  r = this->size_;
    /* sanity check state for species in acquired slot */
    if ( cbm::is_valid( this->s_buf_[r].slot))
    {
        /* complain if species in slot r is alive */
        if ( this->s_buf_[this->s_buf_[r].slot].state != SPECIES_STATE_NONE)
        {
            CBM_LogError( "species_buffer_t::species_mount(): ",
                    "attempt to mount species over living species  [slot=",this->s_buf_[r].slot,",virtual slot=",r,",species=",_ev.species_name(),"]");
            return  invalid_slot;
        }
    }

    /* invalidate for later check */
    size_t const  R = this->find_unused_slot();
    if (( R == invalid_slot) || ( R >= this->capacity_))
    {
        CBM_LogError( "species_buffer_t::species_mount(): ",
                "no available species slot found");
        return  invalid_slot;
    }
    CBM_LogDebug( "mount species  [slot=",R,",virtual slot=",r,",species=",_ev.species_name(),"]");


    /* set species parameters, i.e. merge defaults with refinements from plant event
     *
     * note
     *    @p _param_set always exists because it defaults to the
     *    default parameter set, if no specific set was found
     */
    speciesparameters::speciesparameters_set_t  p_db = _param_set;
    if ( _ev.speciesparameters_given())
    {
        speciesparameters::speciesparameters_set_t const  p_ev = _ev.params();
        p_db.copy_valid( &p_ev);
    }
    else
    {
        /* that's ok :-) */
    }
    this->s_buf_[R].species.set_parameters( p_db);
    if ( !this->s_buf_[R].species->is_valid())
    {
        /* failed to aquire species parameter set */
        CBM_LogError( "species_buffer_t::species_mount(): ",
                "failed to attach parameter set to species  [species=",_ev.species_name(),"]");
        return  invalid_slot;
    }

//     crabmeat_assert(( _ev.group() == SPECIES_GROUP_NONE) || ( _param_set.GROUP() == SPECIES_GROUP_NONE) || ( _ev.group() == _param_set.GROUP()));
    /* reset species properties */
    species_group_e  this_group(( _ev.group() == SPECIES_GROUP_NONE) ? _param_set.GROUP() : _ev.group());
    if (( this_group != SPECIES_GROUP_NONE) && ( this_group < SPECIES_GROUP_CNT))
    {
        if (( _param_set.GROUP() != SPECIES_GROUP_NONE) && ( _ev.group() != SPECIES_GROUP_NONE))
        {
            if ( _param_set.GROUP() != _ev.group())
            {
                CBM_LogError( "species_buffer_t::species_mount(): ",
                        "mismatching species groups  [species=",_ev.species_name(),",groups(speciesparameters/event)=",SPECIES_GROUP_NAMES[_param_set.GROUP()],"/",SPECIES_GROUP_NAMES[_ev.group()],"]");
                return  invalid_slot;
            }
        }
        this->s_buf_[R].species.set_properties( this_group, _ev.species_properties());
    }
    else
    {
        CBM_LogError( "species_buffer_t::species_mount(): ",
                "unknown species group. no idea how to handle this entity :(");
        return  invalid_slot;
    }
    /* set species plant day */
    this->s_buf_[R].species.plant_date( _clock.now());
    /* set species name */
    this->s_buf_[R].species.name( _ev.species_name());
    /* mark species as planted */
    this->s_buf_[R].state = SPECIES_STATE_PLANTED;
    /* set index as used */
    this->s_buf_[r].slot = R;

    /* update fill status of buffer */
    this->size_ += 1;

// sk:dbg #ifdef  _DEBUG
// sk:dbg     for ( size_t  k = 0;  k < this->capacity_;  ++k)
// sk:dbg     {
// sk:dbg         LOGWRITE_( "mount::s",k,"=",this->s_buf_[k].species.name(), "  state=",this->s_buf_[k].state,"   (r=",r,",R=",this->s_buf_[k].slot,")");
// sk:dbg     }
// sk:dbg #endif

    return  r;
}

lerr_t
species_buffer_t::species_prerelease(
        size_t  _r)
{
    if ( _r >= this->size_)
    {
        CBM_LogError( "attempt to prerelease species failed: index out-of-bounds");
        return  LDNDC_ERR_FAIL;
    }

    size_t const  R = T(_r);
    crabmeat_assert( R < this->capacity_);
    if ( this->s_buf_[R].state != SPECIES_STATE_PLANTED)
    {
        CBM_LogError( "attempt to prerelease species at incorrect stage");
        return  LDNDC_ERR_FAIL;
    }
    this->s_buf_[R].state = SPECIES_STATE_TO_BE_HARVESTED;

    return  LDNDC_ERR_OK;
}
size_t
species_buffer_t::species_release()
{
    if ( this->size_ == 0)
    {
        CBM_LogWarn( "attempt to release species from empty buffer");
        return  0;
    }

    size_t  this_size = this->size_;
    for ( size_t  r = 0;  r < this_size;  ++r)
    {
        size_t const  R = this->s_buf_[r].slot;
        crabmeat_assert( R != invalid_slot);
        if ( this->s_buf_[R].state == SPECIES_STATE_TO_BE_HARVESTED)
        {
            CBM_LogDebug( "releasing species  [slot=",R,"(",r,"),name=",this->s_buf_[R].species.name(),"]");

            this->s_buf_[R].state = SPECIES_STATE_NONE;
            this->s_buf_[r].slot = invalid_slot;

            crabmeat_assert( this->size_ > 0);
            /* update fill status of buffer */
            this->size_ -= 1;
        }
    }

    if (( this->size_ == this_size) || ( this->size_ == 0u))
    {
        /* no or all species released */
        return  0;
    }

    /* shift slots towards index 0 */
    for ( size_t  r = 0;  r < this->size_;  /*++r*/)
    {
        if ( this->s_buf_[r].slot != invalid_slot)
        {
            ++r;
            continue;
        }

        size_t  v = r + 1; /* v < capacity */
        while ( this->s_buf_[v].slot == invalid_slot)
        {
            ++v;
            if ( v == this->capacity_)
            {
                v = invalid_size;
                break;
            }
        }

        if ( v == invalid_size)
        {
            break;
        }
        /* "swap" */
        this->s_buf_[r].slot = this->s_buf_[v].slot;
        this->s_buf_[v].slot = invalid_slot;
    }
// sk:dbg #ifdef  _DEBUG
// sk:dbg     for ( size_t  k = 0;  k < this->capacity_;  ++k)
// sk:dbg     {
// sk:dbg         LOGWRITE_( "release::s",k,"=",this->s_buf_[k].species.name(), "  state=",this->s_buf_[k].state,"   (R=",this->s_buf_[k].slot,")");
// sk:dbg     }
// sk:dbg     for ( size_t  k = 0;  k < this->size_;  ++k)
// sk:dbg     {
// sk:dbg         LOGWRITE_( "RELEASE::s",k,"=",this->s_buf_[T(k)].species.name(), "  state=",this->s_buf_[T(k)].state,"   (R=",this->s_buf_[k].slot,")");
// sk:dbg     }
// sk:dbg #endif


    return  this_size - this->size_;
}

lerr_t
species_buffer_t::switch_parameters(
        size_t  _r,
        speciesparameters::speciesparameters_set_t const &  _param_set)
{
    if ( _param_set.is_valid())
    {
        this->operator[]( _r).set_parameters( _param_set);
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}

species_buffer_t::species_state_e
species_buffer_t::get_state(
        size_t  _r)
const
{
    return  this->s_buf_[T(_r)].state;
}

#undef  LDNDC_SPECIES_NO_UNDEF_T  
#undef  T


LDNDC_INPUT_SRV_CLASS_DEFN(species)
input_class_species_srv_t::input_class_species_srv_t()
        : input_class_srv_base_t( invalid_lid),
          event_( NULL),
          sppar_( NULL),
          t_update_( TD_unset)
{
}
input_class_species_srv_t::input_class_species_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id),
          event_( NULL),
          sppar_( NULL),
          t_update_( TD_unset)
{
}

input_class_species_srv_t::~input_class_species_srv_t()
{
}


lerr_t
input_class_species_srv_t::initialize(
        source_handle_srv_base_t const *  _source_handle)
{
    if ( is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    set_input_class_dataprovider(event);
    if ( !this_event)
    {
        return  LDNDC_ERR_FAIL;
    }
    this->event_ = this_event;

    set_input_class_dataprovider(speciesparameters);
    if ( !this_speciesparameters)
    {
        return  LDNDC_ERR_FAIL;
    }
    this->sppar_ = this_speciesparameters;

    set_initialized();
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_species_srv_t::reset_internal_state()
{
    this->t_update_ = TD_unset;

    lerr_t  rc_reset_species_buffer = this->buf_.reset();
    if ( rc_reset_species_buffer)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}
lerr_t
input_class_species_srv_t::update_internal_state(
        cbm::sclock_t const &  _clock)
{
    /* guard for reupdate */
    if ( this->t_update_ == _clock.seconds_since_epoch())
        { return  LDNDC_ERR_OK; }
    this->t_update_ = _clock.seconds_since_epoch();

    /* update if there are species in simulation */
    if ( this->update_species_buffer_( _clock) != LDNDC_ERR_OK)
        { return  LDNDC_ERR_FAIL; }
    return  LDNDC_ERR_OK;
}


size_t
input_class_species_srv_t::species_cnt(
        species_group_e  _group)
const
{
    if ( _group == SPECIES_GROUP_ANY)
    {
        return  this->species_cnt();
    }

    size_t  s( 0);
    for ( size_t  k = 0;  k < this->species_cnt();  ++k)
    {
        s += ( this->buf_[k]->group() == _group) ? 1 : 0;
    }
    return  s;
}

/* NOTE
 *
 * immediately deleting species at harvest event causes
 * trouble!
 *
 * because updating the species buffer happens at the
 * beginning of a timestep the species will have vanished
 * before harvesting actions can be performed.
 */
lerr_t
input_class_species_srv_t::update_species_buffer_(
        cbm::sclock_t const &  _clock)
{
    int  h_err( 0), p_err( 0);

    /* clear species initialization stack */
    if ( ! this->init_s_.is_empty())
    {
        this->init_s_.clear();
    }

    /* mark species as harvested if it was marked to be harvested in previous time step */
    if ( this->species_cnt() > 0)
    {
        this->buf_.species_release();
    }

    event::events_view_t< event::__event_class_name(reparameterizespecies) >  ev_repa_v( this->event_->events_view< event::__event_class_name(reparameterizespecies) >());
    event::events_view_t< event::__event_class_name(reparameterizespecies) >::const_iterator  ev_repa( ev_repa_v.begin());
    for ( ;  ev_repa != ev_repa_v.end();  ++ev_repa)
    {
        size_t  p = this->get_index( ev_repa->species_name());
        if ( cbm::is_invalid( p))
        {
            CBM_LogWarn( "request for species reparameterization does not match any living species  [name=",ev_repa->species_name(),",type=",ev_repa->species_type(),"]");
            continue;
        }

        crabmeat_assert( ev_repa->species_type());

        if ( cbm::is_equal_i( this->operator[]( p)->TYPE(), ev_repa->species_type()))
        {
            /* no switch needed */
            CBM_LogWarn( "skipping reparameterization request because old and new species parameter sets are identical  [type:old=",this->operator[]( p)->TYPE(),",new=",ev_repa->species_type(),"]");
            continue;
        }

        speciesparameters::speciesparameters_set_t  spar = (*this->sppar_)[(char const *)ev_repa->species_type()];
        if ( !spar.is_valid())
        {
            CBM_LogError( "requested species parameter set does not exist  [type=",ev_repa->species_type(),"]");
            return  LDNDC_ERR_FAIL;
        }

        if ( this->operator[]( p)->GROUP() != spar.GROUP())
        {
            CBM_LogError( "species parameterization may not change group  [name=",ev_repa->species_name(),",type=",ev_repa->species_type(),"]");
            return  LDNDC_ERR_FAIL;
        }

        lerr_t  rc_switch_params = this->buf_.switch_parameters( p, spar);
        if ( rc_switch_params)
        {
            CBM_LogError( "resetting species parameter set failed  [type=",ev_repa->species_type(),"]");
            return  LDNDC_ERR_FAIL;
        }
    }

    /* mark species for deletion */
    event::events_view_t< event::__event_class_name(harvest) >  ev_harv_v( this->event_->events_view< event::__event_class_name(harvest) >());
    event::events_view_t< event::__event_class_name(harvest) >::const_iterator  ev_harv( ev_harv_v.begin());
    for ( ;  ev_harv != ev_harv_v.end();  ++ev_harv)
    {
        crabmeat_assert( ! cbm::is_empty( ev_harv->species_name()));
        size_t  h = this->get_index( ev_harv->species_name());

        if ( cbm::is_invalid( h))
        {
            CBM_LogError( "unmatched harvest event  [name=",ev_harv->species_name(),",time=",_clock.now(),"]");
            h_err = 1;
            break;
        }

        CBM_LogDebug( "mark slot for deletion  [slot=",h, ",name=",ev_harv->species_name(),"]");
        this->buf_.species_prerelease( h);
    }
    if ( !h_err)
    {
        /* mount "new" species into buffer */
        event::events_view_t< event::__event_class_name(plant) >  ev_plant_v( this->event_->events_view< event::__event_class_name(plant) >());
        event::events_view_t< event::__event_class_name(plant) >::const_iterator  ev_plant( ev_plant_v.begin());
        for ( ;  ev_plant != ev_plant_v.end();  ++ev_plant)
        {
            size_t  p = this->get_index( ev_plant->species_name());
            if ( cbm::is_valid( p))
            {
                CBM_LogError( "non-unique species name: species already in simulation  [name=",ev_plant->species_name(),"]");
                p_err = 2;
                break;
            }

            p = this->buf_.species_mount( *ev_plant, _clock, (*this->sppar_)[ev_plant->species_type()]);
            if ( cbm::is_invalid( p))
            {
                p_err = 1;
                break;
            }
            this->init_s_.push( static_cast< replace_index_t::element_type >( p));
        }
    }

    return  ( p_err || h_err) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}


species_t const &
input_class_species_srv_t::operator[](
        size_t  _r)
const
{
    crabmeat_assert( this->buf_[_r]);
    return  *this->buf_[_r];
}
// sk:ambiguous overload species_t const &
// sk:ambiguous overload input_class_species_srv_t::operator[](
// sk:ambiguous overload         char const *  _name)
// sk:ambiguous overload const
// sk:ambiguous overload {
// sk:ambiguous overload     size_t  k( this->get_index( _name));
// sk:ambiguous overload     if ( k == invalid_size)
// sk:ambiguous overload     {
// sk:ambiguous overload         CBM_LogFatal( "requested species not in buffer.");
// sk:ambiguous overload         return  species_t();
// sk:ambiguous overload     }
// sk:ambiguous overload     return  this->buf_[k];
// sk:ambiguous overload }


size_t
input_class_species_srv_t::slot_cnt() const
    { return  this->buf_.slot_hi(); }


size_t
input_class_species_srv_t::get_index( char const *  _name) const
{
    for ( size_t  k = 0;  k < this->species_cnt();  ++k)
    {
        crabmeat_assert( this->buf_[k]);
        if ( IS_SPECIE( this->buf_[k]->name(), _name))
        {
            return  k;
        }
    }
    return  invalid_size;
}


LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(species)
lerr_t
input_class_species_global_srv_t::initialize(
        input_source_srv_base_t const *,
        ltime_t const *)
{
    set_initialized();
    return  LDNDC_ERR_OK;
}

}}

