/*!
 * @brief
 *    container for species living during simulation. here we
 *    handle mounting/ evicting species into/ from buffer. only
 *    currently mounted species are visible to the client.
 *
 * @author
 *    steffen klatt (created on: dec 04, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SPECIES_SERVER_H_
#define  LDNDC_INPUT_SPECIES_SERVER_H_

#include  "crabmeat-common.h"

#include <stdexcept>

#include  "input/ic-srv.h"
#include  "input/event/event-srv.h"
#include  "input/speciesparameters/speciesparameters-srv.h"

#include  "event/events/eventbase.h"
#include  "species/speciestypes.h"

#include  "time/cbm_time.h"

#include  "containers/lbstack.h"

namespace  ldndc
{
    class  input_source_srv_base_t;
    class  source_handle_srv_base_t;
namespace  event
{
    class  input_class_event_srv_t;
    class  __event_class_name(plant);
}
namespace  speciesparameters
{
    class  input_class_speciesparameters_srv_t;
}
namespace  species
{
#ifdef  _DEBUG
#  define  T(r)  this->m_s( r)
#else
#  define  T(r)  this->s_buf_[r].slot
#endif
class  CBM_API  species_buffer_t
{
    public:
        enum  species_state_e
        {
            SPECIES_STATE_NONE,

            /*! species was seeded */
            SPECIES_STATE_PLANTED,
            /*! species is about to be harvested */
            SPECIES_STATE_TO_BE_HARVESTED,

            /*! number of species states */
            SPECIES_STATE_CNT
        };

    public:
        species_buffer_t();
        ~species_buffer_t();

        /*! maximum number of species storable in the buffer */
        size_t  capacity() const { return  this->capacity_; }
        /*! current number of species stored in the buffer */
        size_t  size() const { return  this->size_; }
        /*! largest used slot */
        size_t  slot_hi() const;

        lerr_t  reset();

    public:
        /*! mount species into buffer and return its virtual index */
        size_t  species_mount(
                event::__event_class_name(plant) const &,
                cbm::sclock_t const &,
                speciesparameters::speciesparameters_set_t const &);
        lerr_t  species_prerelease(
                size_t  /*virtual index*/);
        /*!
         * @brief
         *    evict all species from buffer that where
         *    marked for deletion.
         *
         * @return
         *    number of species evicted
         */
        size_t  species_release();

        /*!
         * @brief
         *    switch parameter set for species in
         *    given virtual slot
         */
        lerr_t  switch_parameters(
                size_t  /*virtual index*/,
                speciesparameters::speciesparameters_set_t const &);


        /*!
         * @brief
         *    find unused slot in buffer, i.e. an
         *    array index where currently no species
         *    is mounted.
         *
         * @return
         *    return index of unused slot. result buffer
         *    can also be given as argument.
         */
        size_t  find_unused_slot();

    public:
        /*! access to species by index */
        species_t const *  operator[](
                size_t  _r  /*virtual index*/)
        const
        {
            return  &this->s_buf_[T(_r)].species;
        }

        /*! access to species by index */
        species_t &  operator[](
                size_t  _r  /*virtual index*/)
        {
            return  this->s_buf_[T(_r)].species;
        }

        /*!
         * @brief
         *    retrieve current state of n-th species
         */
        species_state_e  get_state(
                size_t  /*virtual index*/) const;

    protected:
        lerr_t  resize(
                size_t  /*new capacity*/);
    private:
        /* maximum number of species storable in the buffer */
        size_t  capacity_;
        /* current number of species stored in the buffer */
        size_t  size_;
        /* storage buffer */
        static size_t const  invalid_slot;
        struct  species_buffer_slot_t
        {
            species_t  species;
            species_state_e  state;

            /* look up table that maps buffer indexes
             *
             * + can i delete harvested species from buffer?
             * yes. but it is not required to actually do this. released
             * slots will be reused, i.e. overwritten.
             */
            size_t  slot;
        };
        species_buffer_slot_t *  s_buf_;


#ifdef  _DEBUG
        inline
        size_t  m_s(
                size_t  _r)
        const
        {
            if ( _r >= this->size_)
            {
                CBM_LogFatal( "species buffer: index out of bounds (bug!?)  [virtual-slot=",_r,",size=",this->size_,"]");
            }
            if ( this->s_buf_[_r].slot == invalid_slot)
            {
                CBM_LogFatal( "species buffer: invalid slot index (bug!?)  [virtual-slot=",_r,"]");
            }
            return  this->s_buf_[_r].slot;
        }
#endif  /*  _DEBUG  */
};

#ifndef  LDNDC_SPECIES_NO_UNDEF_T
#  undef  T
#endif


class  CBM_API  input_class_species_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(species,ICLASS_FLAG_STATE)
    public:
        /* iterator for living species (it is possible to limit to specific group)
         *
         * if species buffer is empty only "end" iterator is constructed
         */
        template < typename  _G1 = species::any, typename  _G2 = species::none, typename  _G3 = species::none, 
             typename  _G4 = species::none, typename  _G5 = species::none, typename  _G6 = species::none, typename  _G7 = species::none >
        class  const_iterator
              {
            enum
            {
                _G = species_groups_select_t< _G1, _G2, _G3, _G4, _G5, _G6, _G7 >::select
            };
            public:
                const_iterator(
                        species_buffer_t const *  _sbuf,
                        size_t  _k = invalid_size)
                        : sbuf( _sbuf),
                          species(( !_sbuf || (_sbuf->size() == 0)) ? NULL : (*_sbuf)[( _k == invalid_size) ? 0 : _k]),
                          slot(( invalid_size) ? 0 : _k)
                {
                    if ( this->sbuf && cbm::is_invalid( _k) && !this->get_species_if_it_matches())
                    {
                        (void)this->operator++();
                    }
                }
                const_iterator(
                        const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 > const &  _it)
                        : sbuf( _it.sbuf),
                          species( _it.species),
                          slot( _it.slot)
                {
                }

                speciesparameters::speciesparameters_set_t const *  operator->()
                const
                {
                    crabmeat_assert( this->species);
                    return  this->species->operator->();
                }
                
                species_t const &  operator*()
                const
                {
                    crabmeat_assert( this->species);
                    return  *this->species;
                }

                bool  operator==( const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 > const &  _it)
                const
                {
                    return  ( this->species == _it.species);
                }
                bool  operator!=( const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 > const &  _it)
                const
                {
                    return  ( this->species != _it.species);
                }

                const_iterator &  operator++()
                {
                    crabmeat_assert( this->sbuf);

                    species_t const *  s = NULL;
                    while ( ++this->slot < this->sbuf->size())
                    {
                        s = this->get_species_if_it_matches();
                        if ( s)
                        {
                            break;
                        }
                    }
                    this->species = s;
                    return  *this;
                }

            private:
                /* _not_ owner */
                species_buffer_t const *  sbuf;
                /* _not_ owner */
                species_t const *  species;
                /* slot refers to the position of a specific plant */
                size_t  slot;

                species_t const *  get_species_if_it_matches()
                {
                    species_t const *  s = this->sbuf->operator[]( this->slot);
                    crabmeat_assert( s);

                    /* skip if species is not matching this iterators species group */
                    if ( select_species_t< SPECIES_GROUP_ANY >::fselect( s->group()) & _G)
                    {
                        return  s;
                    }
                    return  NULL;
                }
        };
    public:
        lerr_t  reset_internal_state();
        lerr_t  update_internal_state(
                cbm::sclock_t const &);

        replace_index_t const &  replace_species()
        const
        {
            return  this->init_s_;
        }

        size_t  species_cnt( species_group_e) const;
        size_t  species_cnt()
        const
        {
            return  this->buf_.size();
        }

    public:
        species_t const &  operator[]( size_t) const;
// sk:ambiguous overload        species_t const &  operator[]( char const *) const;

        size_t  get_index( char const *) const;

        template < typename  _G >
        const_iterator< _G >  const_iterator_by_name(
                char const *  _name)
        const
        {
            if ( this->buf_.size() > 0)
            {
                size_t const  s_idx = cbm::is_valid( this->get_index( _name));
                if ( cbm::is_valid( s_idx))
                {
                    return  const_iterator< _G, species::none, species::none, species::none, species::none, species::none, species::none >( &this->buf_, s_idx);
                }
            }
            return  this->end< _G, species::none, species::none, species::none, species::none, species::none, species::none >();
        }
        template < typename  _G >
        const_iterator< _G >  const_iterator_by_name(
                std::string const &  _name)
        const
        {
            return  this->const_iterator_by_name< _G, species::none, species::none, species::none, species::none, species::none, species::none >( _name.c_str());
        }


        template < typename  _G1, typename  _G2, typename  _G3, typename  _G4,
            typename  _G5, typename  _G6, typename  _G7 >
        const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 >  begin()
        const
        {
            return  ( this->buf_.size() > 0) 
                    ? const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 >( &this->buf_)
                    : this->end< _G1, _G2, _G3, _G4, _G5, _G6, _G7 >();
        }
        template < typename  _G1, typename  _G2, typename  _G3, typename  _G4,
            typename  _G5, typename  _G6, typename  _G7 >
        const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 >  end()
        const
        {
            return   const_iterator< _G1, _G2, _G3, _G4, _G5, _G6, _G7 >( NULL);
        }

        const_iterator< species::any >  begin_any()
        const
        {
            return  this->begin< species::any, species::none, species::none, species::none, species::none, species::none, species::none >();
        }
        const_iterator< species::any >  end_any()
        const
        {
            return  this->end< species::any, species::none, species::none, species::none, species::none, species::none, species::none >();
        }


        bool  is_family( char const *  _type, char const *  _family)
        const
        {
            if ( this->sppar_)
            {
                return  this->sppar_->is_family( _type, _family);
            }
            throw  std::runtime_error( "no speciesparameters"); 
        }

    private:
        species_buffer_t  buf_;

        replace_index_t  init_s_;

        event::input_class_event_srv_t const *  event_;
        speciesparameters::input_class_speciesparameters_srv_t const *  sppar_;

        /*!
         * @brief
         *    reacts to plant and harvest events, i.e.
         *    adding, replacing species in the buffer.
         */
        lerr_t  update_species_buffer_(
                cbm::sclock_t const &);
        ltime_t::td_scalar_t  t_update_;
    public:
        size_t  slot_cnt() const;
};

}}

/* dummy global class */
namespace  ldndc{ namespace  species
{
class  CBM_API  input_class_species_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(species,ICLASS_FLAG_STATE)
};
}}

#endif  /*  !LDNDC_INPUT_SPECIES_SERVER_H_  */

