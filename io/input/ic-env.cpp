/*!
 * @brief
 *    abstract base classes for input classes
 *    (those read only input stream containers)
 *
 * @author
 *    steffen klatt (created on: Jul 14, 2011)
 */


#include  "input/ic-env.h"

namespace ldndc {

input_class_base_t::input_class_base_t()
        : status_( ICLASS_STATUS_NONE)
{ }

input_class_base_t::~input_class_base_t()
{ }

std::string
input_class_base_t::name()
const
{
    return  std::string( "input class ") + this->input_class_type();
}


lflags_t
input_class_base_t::set_status(
        lflags_t  _status)
{
    status_ = _status;
    return  status_;
}
void
input_class_base_t::unset_status()
{
    status_ = ICLASS_STATUS_NONE;
}

lflags_t
input_class_base_t::set_status_flag(
        iclass_status_flag_e  _status_flag)
{
    status_ |= (lflags_t)_status_flag;
    return  status_;
}
lflags_t
input_class_base_t::unset_status_flag(
        iclass_status_flag_e  _status_flag)
{
    status_ &= ~((lflags_t)_status_flag);
    return  status_;
}

void
input_class_base_t::set_initialized()
{
    status_ |= (lflags_t)ICLASS_STATUS_INITIALIZED;
}

}

#include  "utils/cbm_utils.h"
#include  "hash/cbm_hash.h"
#include  "math/cbm_math.h"
int
ldndc::hash_ic( hash_ic_t *  _hash,
        lid_t const &  _block_id,  char const *  _source_identifier,
        char const *, char *  _hashsource)
{
    char  hashsource[HASH_SOURCE_BYTESIZE];
    char *  hashsource_buffer = _hashsource ? _hashsource : hashsource;
    char const *  hashsource_buffer_offs = hashsource_buffer;

    char const *  sid = _source_identifier;
    int  len = cbm::strnlen( sid, CBM_STRING_SIZE);
    cbm::mem_cpy( hashsource_buffer, sid, len);
    hashsource_buffer += len;
    len += cbm::stringify_t< sizeof(lid_t) >::to_hex( hashsource_buffer, _block_id);

    if ( _hash)
        { *_hash = static_cast< hash_ic_t >(
            cbm::hash::sha1_uint64( hashsource_buffer_offs, len)); }

#ifdef  _DEBUG
// sk:dbg    cbm::strclrnul( hash_source, HASH_SOURCE_BYTESIZE);
// sk:dbg    CBM_LogDebug( "type=",_type,"  aic-source=\"",_source_identifier, ":",_block_id,"\"  hash=",*_hash, "  hash-source=\"",hash_source,"\"");
// sk:testing collision detection    if ( cbm::is_equal( _source_identifier, "site") && _block_id == 0)
// sk:testing collision detection    {
// sk:testing collision detection        *_hash = 14623071064645027531UL;
// sk:testing collision detection    }
#endif

    if ( _hash && cbm::is_invalid( *_hash))
        { return  -1; }

    return  len;
}

#include  "input/ic-factory-srv.h"
int
ldndc::ic_dependencies( char const *  _type,
            char const ***  _dependencies)
{
    char const **  dependencies = NULL;
    void const *  factory =
        find_input_class_srv_factory( 'C', _type, &dependencies);
    if ( !factory)
        { return -1; }

    if ( _dependencies)
        { *_dependencies = dependencies; }

    int  c = 0;
    while ( dependencies && *dependencies)
    {
        ++dependencies;
        ++c;
    }
    return  c;
}

int
ldndc::is_aic( char const *  _type)
{
    char const **  dependencies;
    void const *  factory =
        find_input_class_srv_factory( 'C', _type, &dependencies);
    if ( factory)
        { return dependencies==NULL ? 1 : 0; }
    return -1;
}
int
ldndc::is_dic( char const *  _type)
{
    char const **  dependencies;
    void const *  factory =
        find_input_class_srv_factory( 'C', _type, &dependencies);
    if ( factory)
        { return dependencies==NULL ? 0 : 1; }
    return -1;
}

