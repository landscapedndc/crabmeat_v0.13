/*!
 * @brief
 *    container class for one complete soil
 *    and humus parameters set
 * 
 * @author
 *      steffen klatt (created on oct 09, 2011)
 */


#ifndef  LDNDC_INPUT_SOILPARAMETERS_SERVER_H_
#define  LDNDC_INPUT_SOILPARAMETERS_SERVER_H_


#include  "crabmeat-common.h"
#include  "input/ic-srv.h"
#include  "io/source-handle-srv.h"
#include  "io/iif/iif.h"
#include  "string/lstring.h"

#include  "soilparameters/soilparameterstypes.h"
#include  "soillayers/soillayerstypes.h"

namespace  ldndc
{
    class  iproc_interface_soilparameters_t;
namespace  soilparameters
{
class  CBM_API  input_class_soilparameters_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(soilparameters,ICLASS_FLAG_STATE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update_internal_state(
                cbm::sclock_t const &);
        lerr_t  update();

    public:
        /* expands to getters for all soil parameters */
        SOILPARAMETERS_GETTERS
        /* expands to getters for all humus parameters */
        HUMUSPARAMETERS_GETTERS

    public:
        size_t  parameterized_soil_cnt() const
        {
            return   static_cast< size_t >( soillayers::SOIL_CNT);
        }
        std::string  soilparametervalue_as_string(
                char const * /*parameter name*/, char const * /*soilname*/) const;
        size_t  parameterized_humus_cnt() const
        {
            return   static_cast< size_t >( soillayers::HUMUS_CNT);
        }
        std::string  humusparametervalue_as_string(
                char const * /*parameter name*/, char const * /*humusname*/) const;

    private:
        /* holds only default properties that apply when no humus type specific value is known */
        humusparameters::humusparameter_t  humus_default_props_[1 * humusparameters::HUMUSPARAMETERS_CNT];
        /* list of properties structs of _all_ humus types (free after init?) */
        humusparameters::humusparameter_t  humus_props_[soillayers::HUMUS_CNT * humusparameters::HUMUSPARAMETERS_CNT];

        /* holds only default properties that apply when no soil type specific value is known */
        soilparameters::soilparameter_t  soil_default_props_[1 * soilparameters::SOILPARAMETERS_CNT];
        /* list of properties structs of _all_ soil types (free after init?) */
        soilparameters::soilparameter_t  soil_props_[soillayers::SOIL_CNT * soilparameters::SOILPARAMETERS_CNT];

        ldndc::source_handle_srv_t  sh;

        lerr_t  read_soils_(
                iproc_interface_soilparameters_t const *);
        lerr_t  read_humus_(
                iproc_interface_soilparameters_t const *);

        lerr_t  semantic_check_() const;
};
}}

#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  soilparameters
{
class  CBM_API  input_class_soilparameters_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(soilparameters,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_SOILPARAMETERS_SERVER_H_  */

