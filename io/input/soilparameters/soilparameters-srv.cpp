/*!
 * @author
 *      steffen klatt (created on oct 09, 2011)
 */

#include  "input/soilparameters/soilparameters-srv.h"
#include  "soillayers/soillayerstypes.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_soilparameters.h"

#include  "math/lmath-array.h"
#include  "string/lstring.h"
#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"

namespace  ldndc{ namespace  soilparameters
{
LDNDC_INPUT_SRV_CLASS_DEFN(soilparameters)
input_class_soilparameters_srv_t::input_class_soilparameters_srv_t()
        : input_class_srv_base_t( invalid_lid)
{
}
input_class_soilparameters_srv_t::input_class_soilparameters_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id)
{
}

input_class_soilparameters_srv_t::~input_class_soilparameters_srv_t()
{
}



lerr_t
input_class_soilparameters_srv_t::initialize(
        ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
           {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    this->sh = *static_cast< source_handle_srv_t const * >( _source_handle);

    lerr_t  rc_update = this->update();
    if ( rc_update)
    {
        return  LDNDC_ERR_FAIL;
    }

    this->set_initialized();
    return  LDNDC_ERR_OK;
}

lerr_t
input_class_soilparameters_srv_t::update_internal_state(
        cbm::sclock_t const &)
{
    return  this->update();
}
lerr_t
input_class_soilparameters_srv_t::update()
{
    set_input_class_reader_from_handle(soilparameters, &this->sh);

    if ( !dynamic_cast< iproc_provider_t * >( this_reader)->data_available())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_read_soil = this->read_soils_( this_reader);
    if ( rc_read_soil != LDNDC_ERR_OK)
    {
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_read_humus = this->read_humus_( this_reader);
    if ( rc_read_humus != LDNDC_ERR_OK)
    {
        return  LDNDC_ERR_FAIL;
    }

    lerr_t  rc_check = this->semantic_check_();
    if ( rc_check)
    {
        return  LDNDC_ERR_FAIL;
    }


    return  LDNDC_ERR_OK;
}


lerr_t
input_class_soilparameters_srv_t::read_soils_(
        ldndc::iproc_interface_soilparameters_t const *  _rdr)
{
    int  default_props_read( -1), default_props_seen( -1);
    lerr_t  rc_soil_defaults = _rdr->get_soil_default_properties(
            this->soil_default_props_,
            soilparameters::SOILPARAMETERS_NAMES,
            soilparameters::SOILPARAMETERS_TYPES,
            soilparameters::SOILPARAMETERS_CNT,
            &default_props_read, &default_props_seen);
    size_t  soils_cnt_total = 0;
    if ( !rc_soil_defaults)
    {
        if ( default_props_read > 0)
               {
            /* set defaults for all soil */
            for ( size_t  k = 0;  k < soillayers::SOIL_CNT;  ++k)
                   {
                memcpy( (void *)(&this->soil_props_[k*soilparameters::SOILPARAMETERS_CNT]), (void *)this->soil_default_props_, soilparameters::SOILPARAMETERS_CNT*sizeof( soilparameter_t));
            }
        }
               else
        {
            CBM_LogWarn( "default soil parameters set found but has no valid parameter");
        }
        int  soils_read( -1);
        (void)_rdr->get_soil_properties( 
                this->soil_props_,
                soilparameters::SOILPARAMETERS_NAMES,
                soilparameters::SOILPARAMETERS_TYPES,
                soilparameters::SOILPARAMETERS_CNT,
                soillayers::SOIL_MNEMONIC,
                soillayers::SOIL_CNT,
                       &soils_read, NULL, NULL, NULL);
        if ( soils_read < 0)
        {
            CBM_LogError( "error reading soil properties");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        /* total number of read soil */
        soils_cnt_total = static_cast< size_t >( soils_read);
    }
    else
    {
        CBM_LogError( "failed to read soil parameter defaults");
        return  rc_soil_defaults;
    }

    if ( soils_cnt_total < soillayers::SOIL_CNT)
    {
        CBM_LogWarn( "read ", soils_cnt_total, " soil type(s) of ", soillayers::SOIL_CNT, " known type(s)");
    }
           else if ( soils_cnt_total > soillayers::SOIL_CNT)
           {
        CBM_LogWarn( "data file contains multiple blocks for some soil");
    }

    return  LDNDC_ERR_OK;
}

lerr_t
input_class_soilparameters_srv_t::read_humus_(
        ldndc::iproc_interface_soilparameters_t const *  _rdr)
{
    int  default_props_read( -1), default_props_seen( -1);
    lerr_t  rc_humus_defaults = _rdr->get_humus_default_properties(
            this->humus_default_props_,
            humusparameters::HUMUSPARAMETERS_NAMES,
            humusparameters::HUMUSPARAMETERS_TYPES,
            humusparameters::HUMUSPARAMETERS_CNT,
            &default_props_read, &default_props_seen);

    size_t  humuses_cnt_total = 0;
    if ( !rc_humus_defaults)
           {
        if ( default_props_read > 0)
        {
            /* set defaults for all humus */
            for ( size_t  k = 0;  k < soillayers::HUMUS_CNT;  ++k)
                   {
                memcpy( (void *)(&this->humus_props_[k*humusparameters::HUMUSPARAMETERS_CNT]), (void *)this->humus_default_props_, humusparameters::HUMUSPARAMETERS_CNT*sizeof( humusparameters::humusparameter_t));
            }
        }
               else
               {
            CBM_LogWarn( "default humus parameters set found but has no valid parameter");
        }
        int  humus_read = -1;
        (void)_rdr->get_humus_properties( 
                this->humus_props_,
                humusparameters::HUMUSPARAMETERS_NAMES,
                humusparameters::HUMUSPARAMETERS_TYPES,
                humusparameters::HUMUSPARAMETERS_CNT,
                soillayers::HUMUS_MNEMONIC,
                soillayers::HUMUS_CNT,
                       &humus_read, NULL, NULL, NULL);
        if ( humus_read < 0)
        {
            CBM_LogError( "error reading humus properties");
            return  LDNDC_ERR_RUNTIME_ERROR;
        }
        /* total number of read humus */
        humuses_cnt_total = static_cast< size_t >( humus_read);
    }
    else
    {
        CBM_LogError( "failed to read humus parameter defaults");
        return  rc_humus_defaults;
    }

    if ( humuses_cnt_total < soillayers::HUMUS_CNT)
           {
        CBM_LogWarn( "read ",humuses_cnt_total," humus type(s) of ",soillayers::HUMUS_CNT," known type(s)");
    }
           else if ( humuses_cnt_total > soillayers::HUMUS_CNT)
           {
        CBM_LogWarn( "data file contains multiple blocks for some humus");
    }

    return  LDNDC_ERR_OK;
}

static
lerr_t
soilparameters_check_if_values_within_minmax_boundaries_s(
    input_class_soilparameters_srv_t const *  _soilparameters)
{
    CRABMEAT_FIX_UNUSED(_soilparameters);
    int  err = 0;
#   include  "soilparameters_check_minmax.cpp.inc"
    return  err ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}
static
lerr_t
humusparameters_check_if_values_within_minmax_boundaries_s(
    input_class_soilparameters_srv_t const *  _humusparameters)
{
    CRABMEAT_FIX_UNUSED(_humusparameters);
    int  err = 0;
#   include  "humusparameters_check_minmax.cpp.inc"
    return  err ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}


lerr_t
input_class_soilparameters_srv_t::semantic_check_()
const
{
    lerr_t  rc = LDNDC_ERR_OK;
    lerr_t  rc_soil =
        soilparameters_check_if_values_within_minmax_boundaries_s( this);
    if ( rc_soil)
    {
        rc = rc_soil;
    }

    lerr_t  rc_humus = 
        humusparameters_check_if_values_within_minmax_boundaries_s( this);
    if ( rc_humus)
    {
        rc = rc_humus;
    }

    return  rc;
}

std::string
input_class_soilparameters_srv_t::soilparametervalue_as_string(
        char const *  _parname, char const *  _soilname)
const
{
    unsigned int  s = invalid_uint;
    cbm::find_index(
            cbm::to_upper( _soilname).c_str(),
            soillayers::SOIL_MNEMONIC,
            soillayers::SOIL_CNT,
            &s);

    unsigned int  p = invalid_uint;
    cbm::find_index(
            cbm::to_upper( _parname).c_str(),
            soilparameters::SOILPARAMETERS_NAMES,
            soilparameters::SOILPARAMETERS_CNT,
            &p);

    if ( cbm::is_valid( s) && cbm::is_valid( p))
    {
        return  this->soil_props_[s*soilparameters::SOILPARAMETERS_CNT+p].get_value_as_string(
                soilparameters::SOILPARAMETERS_TYPES[p]);
    }
    return  "nan";
}
std::string
input_class_soilparameters_srv_t::humusparametervalue_as_string(
        char const *  _parname, char const *  _humusname)
const
{
    unsigned int  h = invalid_uint;
    cbm::find_index(
            cbm::to_upper( _humusname).c_str(),
            soillayers::HUMUS_MNEMONIC,
            soillayers::HUMUS_CNT,
            &h);

    unsigned int  p = invalid_uint;
    cbm::find_index(
            cbm::to_upper( _parname).c_str(),
            humusparameters::HUMUSPARAMETERS_NAMES,
            humusparameters::HUMUSPARAMETERS_CNT,
            &p);

    if ( cbm::is_valid( h) && cbm::is_valid( p))
    {
        return  this->humus_props_[h*humusparameters::HUMUSPARAMETERS_CNT+p].get_value_as_string(
                humusparameters::HUMUSPARAMETERS_TYPES[p]);
    }
    return  "nan";
}

LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(soilparameters)
lerr_t
input_class_soilparameters_global_srv_t::initialize(
                ldndc::input_source_srv_base_t const *,
        ltime_t const *)
{
    /* guard */
    if ( this->is_initialized())
        { return  LDNDC_ERR_OK; }
    this->set_initialized();
    return  LDNDC_ERR_OK;
}

}}

