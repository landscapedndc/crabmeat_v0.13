/*! 
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: dec 08, 2011),
 *    edwin haas
 */

#include  "input/event/event-srv.h"
#include  "event/events/eventfactory.h"

#include  "time/cbm_time.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_event.h"

#include  "memory/cbm_mem.h"

#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

/* parse time string */
static
lerr_t
parse_time_s(
        ltime_t &  _t, char const *  _t_exec_s, ltime_t const &  _t_ref)
{
    if ( _t_exec_s && !cbm::is_empty( _t_exec_s))
    {
                /* second, refine from given time specification */
        lerr_t  rc = _t.from_string( _t_exec_s, _t_ref);
        if ( rc != LDNDC_ERR_OK)
        {
            return  rc;
        }
    }
    /* otherwise, set full reference date */
    else if ( &_t != &_t_ref)
    {
        _t = ltime_t( _t_ref.from(), _t_ref.from());
    }
    else
    {
        /* huh ?!? */
    }

    return  LDNDC_ERR_OK;
}

namespace  ldndc{ namespace  event
{
LDNDC_INPUT_SRV_CLASS_DEFN(event)
input_class_event_srv_t::input_class_event_srv_t()
        : input_class_srv_base_t( invalid_lid)
{
    this->e_buf.l0 = NULL;
    this->e_buf.l1 = NULL;
}
input_class_event_srv_t::input_class_event_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id)
{
    this->e_buf.l0 = NULL;
    this->e_buf.l1 = NULL;
}

input_class_event_srv_t::~input_class_event_srv_t()
{
    while ( ! this->ev_owner_.empty())
    {
        abstract_event_factory_t::destroy( this->ev_owner_.back());
        this->ev_owner_.pop_back();
    }

    this->ev_owner_.clear();
}


lerr_t
input_class_event_srv_t::initialize(
        source_handle_srv_base_t const *  _source_handle)
{
// sk:dbg    CBM_LogDebug( "event::initialize()  IN:");
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }

    crabmeat_assert( _source_handle);
    this->sh = *static_cast< source_handle_srv_t const * >( _source_handle);

    set_input_class_global(event);

    this->ref_time = this_event_global->get_time();

    this->e_buf.l0 = &this->e_buf.b0;
    this->e_buf.l1 = &this->e_buf.b1;


    this->set_initialized();
// sk:dbg    CBM_LogDebug( "event::initialize()  OUT:");
    return  LDNDC_ERR_OK;
}


lerr_t
input_class_event_srv_t::load_events_(
        event_tree_t *  _event_tree,
        int  _number_of_events_to_load,
        source_handle_srv_t const *  _source_handle)
{
    set_input_class_reader(event);
    /* read event meta info and map structure of input stream into event tree */
    int  r( 0), s( 0);
    this_reader->get_event_tree( &_event_tree->root, _number_of_events_to_load, &r, &s);
    CBM_LogDebug( "read events  [read=", r, ",seen=",s,"]");

    /* traverse event tree, create management events and read event attributes */
    cbm::event_info_tree_t::iterator_t  ev_node( _event_tree->root.begin());
    cbm::event_info_tree_t::iterator_t  ev_node_end( _event_tree->root.end());
    size_t  k( 0);
    for ( ; ev_node != ev_node_end; ++ev_node)
    {
        if ( ev_node->type >= EVENT_CNT)
        {
            continue;
        }
        ++k;

        /* testing for correct date setting if given */
        ltime_t  t_exec;
        lerr_t  rc_t_exec =
                parse_time_s( t_exec, ev_node->t_exec_s.c_str(), this->ref_time);
        if ( rc_t_exec != LDNDC_ERR_OK)
        {
            CBM_LogError( "input_class_event_srv_t::load_events_(): invalid date for event",
                    "  [event=",ldndc::event::EVENT_NAMES[ev_node->type],",date=",ev_node->t_exec_s,"]");
            return  rc_t_exec;
        }
// sk:dbg        CBM_LogDebug( " ==== event=", ldndc::event::EVENT_NAMES[ev_node->type], " ====  t-exec-s=",ev_node->t_exec_s, "  t-exec=", t_exec.to_string());

        /* creating event and event attribute objects (attributes are internally attached to event) */
        event_attribute_t *  ev_attrs;
        Event *  ev = event_factory[ev_node->type]->construct( lid_t( k), &ev_attrs);
        if ( !ev)
        {
            CBM_LogError( "failed to construct event",
                    "  [event=",ldndc::event::EVENT_NAMES[ev_node->type],",date=",ev_node->t_exec_s,"]");
            return  LDNDC_ERR_NOMEM;
        }

        this->ev_owner_.push_back( ev);

        /* getting event attributes */
        r = s = 0;
        lerr_t const  rc_getattribs = this_reader->get_event_attributes(
                ev_attrs, event_attribute_defaults[ev_node->type], &(*ev_node), &r, &s);
        if ( rc_getattribs)
        {
            return  LDNDC_ERR_FAIL;
        }

        /* after the reader has done his job we can "safely" overwrite reader data handle (union!) */
        ev_node->event = ev;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
input_class_event_srv_t::enqueue_loaded_events_(
        event_tree_t *  _event_tree,
        ltime_t const *  _ltime,
        int *  _drop_past, int *  _drop_future)
{
    /* prepare event list for internal logic
     *    1. unroll tree, appends to list
     */                 
    size_t const  this_event_cnt_0 = this->e_buf.b0.size();
    _event_tree->unroll( &this->e_buf.b0, this->ref_time/*time_offs*/);
    size_t const  this_event_cnt_1 = this->e_buf.b0.size();

    size_t const  this_event_cnt = this_event_cnt_1 - this_event_cnt_0;

    if ( this_event_cnt > 0)
    {
        CBM_LogDebug( "unrolled ",this_event_cnt, " events,  total=",this->e_buf.b0.size());

        /* NOTE  dropping events here prevents an input
         *     data reset and running again with modified
         *     simulation schedule.
         */
        int  drop_cnt = this->e_buf.b0.drop_unreachable_events( *_ltime, _drop_past, _drop_future);
        if ( drop_cnt > 0)
        {
            CBM_LogVerbose( "input_class_event_srv_t::enqueue_loaded_events_():",
                   " dropped ",drop_cnt," unreachable event",((drop_cnt==1)?"":"s"),".",
                 "  [past=",((_drop_past)?*_drop_past:-1),",future=",((_drop_future)?*_drop_future:-1),"]");
        }

        this->e_buf.b0.sort_by_date();
    }

    return  LDNDC_ERR_OK;
}


#include  "utils/lutils-swap.h"
lerr_t
input_class_event_srv_t::reset_internal_state()
{
    /* NOTE
     * we collected all events that we read so far,
     * hence we decide *not* to reset our reader
     */
    if ( this->ev_sel_.size() > 0)
    {
        event_list_t::iterator_t  evnt = this->e_buf.l0->begin();
        /* shovel all what's left in l0 to l1 */
        while ( evnt != this->e_buf.l0->end())
        {
            evnt->r = evnt->R;
            this->e_buf.l1->push_back( *evnt);

            evnt = this->e_buf.l0->erase( evnt);
        }

        /* toggle lists */
        cbm::swap_values( this->e_buf.l0, this->e_buf.l1);
        /* sort .. is this strictly required? */
        this->e_buf.l0->sort_by_date();

        /* reset event selection */
        return  this->ev_sel_.reset();
    }
    return  LDNDC_ERR_OK;
}

lerr_t
input_class_event_srv_t::update_internal_state(
        cbm::sclock_t const &  _clock)
{
    if ( !this->is_initialized())
    {
        lerr_t  rc_init = this->initialize( &this->sh);
        RETURN_IF_NOT_OK(rc_init);
    }

    lerr_t  rc_update_evsel = this->ev_sel_.update(
        _clock.seconds_since_epoch(), this->e_buf.l0, this->e_buf.l1);
    RETURN_IF_NOT_OK(rc_update_evsel);

    set_input_class_reader_from_handle(event, &this->sh);

    lerr_t  rc_load = LDNDC_ERR_OK;

    if ((( this->max_readahead() == -1) || ( static_cast< int >( this->remaining_event_cnt()) < this->max_readahead()))
            && dynamic_cast< iproc_provider_t * >( this_reader)->data_available())
    {
        event_tree_t  this_event_tree;

        int const  read_ahead = ( this->max_readahead() == -1) ? -1 : (this->max_readahead() - static_cast< int >( this->remaining_event_cnt()));
        rc_load = this->load_events_( &this_event_tree, read_ahead, &this->sh);

        ltime_t const  ltime( _clock.time());
        int  miss_count = 0;
        this->enqueue_loaded_events_( &this_event_tree, &ltime, &miss_count, NULL);

        if (( _clock.cycles() > 0) && ( miss_count > 0))
        {
            CBM_LogWarn( "input_class_event_srv_t::update_internal_state(): dropped ",
                    miss_count," event",((miss_count==1)?"":"s"),".",
                    " is your look-ahead size too small? consider rearranging your event input.  [look-ahead=",this->max_readahead(),"]");
        }

        rc_update_evsel = this->ev_sel_.reupdate(
            _clock.seconds_since_epoch(), this->e_buf.l0, this->e_buf.l1);
    }

    return  ( rc_update_evsel || rc_load) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

int
input_class_event_srv_t::max_readahead()
const
{
    set_input_class_reader_from_handle(event,&this->sh);
    if ( !this_reader)
    {
        return  ldndc::invalid_t< int >::value;
    }
    int const  readahead_size = static_cast< int >( this_reader->readahead_size());
    if ( readahead_size == this_reader->invalid_readahead_size())
    {
        /* -1 means read until EOF */
        return  -1;
    }
    else if ( readahead_size < 0)
    {
        /* -1 means read until EOF */
        return  -1;
    }
    return  readahead_size;
}

size_t
input_class_event_srv_t::remaining_event_cnt()
const
{
    return  ( this->e_buf.l0) ? this->e_buf.l0->size() : 0;
}

size_t
input_class_event_srv_t::event_cnt()
const
{
    return  this->ev_sel_.size();
}


bool
input_class_event_srv_t::have_events()
const
{
    return  this->event_cnt() > 0;
}


Event const *
input_class_event_srv_t::operator[]( size_t  _k)
const
{
    return  this->ev_sel_[_k];
}

size_t
input_class_event_srv_t::get_event_handles_cnt()
const
{
    return  this->remaining_event_cnt();
}

lerr_t
input_class_event_srv_t::get_event_handles(
        event_handle_t *  _buf, size_t  _n)
const
{
    crabmeat_assert( _buf);
    size_t  j = 0;

    event_list_t::const_iterator_t  it = this->e_buf.l0->begin();
    for ( ; it != this->e_buf.l0->end();  ++it)
    {
        if ( j < _n)
        {
            CBM_LogVerbose( "input_class_event_srv_t::get_event_handles():",
                   " copying event handle #",j);
            _buf[j] = *it;
            ++j;
        }
        else
        {
            break;
        }
    }

    if ( j < _n)
    {
        return  LDNDC_ERR_INPUT_EXHAUSTED;
    }
    return  LDNDC_ERR_OK;
}


event_tree_t::event_tree_t()
{
    this->root.insert( this->root.begin(), event_info_t( MGNT_ROTATION));
}


size_t
event_tree_t::unroll(
        event_list_t *  _ev_list, ltime_t const &  _t_ref)
{
    return  this->unroll_( _ev_list, _t_ref, this->root.begin( this->root.begin()));
}
size_t
event_tree_t::unroll_(
        event_list_t *  _ev_list, ltime_t const &  _t_ref,
        cbm::event_info_tree_t::sibling_iterator_t  _sib_ev,
        size_t  _r)
{
    if ( !_ev_list || ( _r == 0))
    {
        return  0;
    }

    if ( this->root.begin() == this->root.end())
    {
        return  0;
    }

    //while ( _t_ref < t_rot_end)
    // TODO  _t_ref += rotations time span

    for ( size_t  k = 0;  k < _r;  ++k)
    {
        cbm::event_info_tree_t::sibling_iterator_t  sib_end = this->root.end( _sib_ev);
        while ( _sib_ev != sib_end)
        {
            ltime_t  t;
            parse_time_s( t, _sib_ev->t_exec_s.c_str(), _t_ref);

/*FIXME: CHECK*/
            event_handle_t  ev_handle( _sib_ev->event, t, t.period( t.from().dt()));

            switch ( _sib_ev->type)
            {
                case ldndc::event::EVENT_NONE:
                {
                    CBM_LogFatal( "event_tree_t::unroll(): oopsie, how did that event get in here?!");
                    break;
                }
                case ldndc::event::MGNT_ROTATION:
                {
                    if (( ev_handle.r > 0) && ( t.from() >= _t_ref.from()))
                    {
// sk:dbg                        CBM_LogDebug( "unroll subtree,  k", k, "  T=", t.to_string());

                        /* new reference time is parent rotation's time */
/*FIXME  correct this*/                (void)unroll_(
                            _ev_list, t/*.from()*/ /*rotations time*/,
                            this->root.begin( _sib_ev),
                            1 /*t.period() / t.time_resolution()*/ /*repeats*/);
                    }
                    else
                    {
                        /* this is a semantic error within the input stream
                         * e.g. rotation starts after its parent rotation
                         */
                        CBM_LogWarn( "event_tree_t::unroll(): skipping rotation (sorry for little helpful error message)");
// TODO  set error flag?
                    }
                    break;
                }
                /* the remaining cases are assumed to be valid events */
                default:
                {
// sk:dbg                    CBM_LogDebug( "r=", ev_handle.r, "  ref.time=", _t_ref.to_string(), "  time=", et_iter->t_exec_s.c_str(), "  t=", t.to_string());
                    if (( ev_handle.r > 0) && ( ev_handle.t_exec >= _t_ref.from().seconds_since_epoch()))
                    {
                        _ev_list->push_back( ev_handle);
// sk:dbg                        CBM_LogDebug( "adding handle,  ev=", ev_handle.event->name());
                    }
                    else
                    {
// sk:dbg                        CBM_LogDebug( "ref.time=", _t_ref.to_string(), "  time=", et_iter->t_exec_s.c_str(), "  t=", t.to_string());
                        CBM_LogWarn( "event_tree_t::unroll(): dropping event due to time settings");
                    }
                }
            }

            ++_sib_ev;
        }
// sk:dbg        CBM_LogDebug( "id=", ev_handle.event->id(), "  type=", ldndc::event::EVENT_NAMES[ev_handle.type()], "  t=", ev_handle.t_exec, "  r=", ev_handle.r);
    }
    return  _ev_list->size();
}


event_dispatch_list_t::event_dispatch_list_t()
        : ev_buf_( event_buffer_t()),
          ev_cnt_( 0),
          update_time_( TD_unset)
{
}


lerr_t
event_dispatch_list_t::reset()
{
    this->update_time_ = TD_unset;
    this->ev_cnt_ = 0;

    return  LDNDC_ERR_OK;
}
lerr_t
event_dispatch_list_t::update(
        ltime_t::td_scalar_t const &  _time_stamp,
        event_list_t *  _l0, event_list_t *  _l1)
{
    if ( this->update_time_ == _time_stamp)
    {
        /* no op */
    }
    else
    {
        this->update_time_ = _time_stamp;

        crabmeat_assert( _l0);
        crabmeat_assert( _l1);

        event_list_t::iterator_t  evnt( _l0->begin());
        size_t  l( 0);

        while (( evnt != _l0->end()) && ( evnt->t_exec <= _time_stamp))
        {
// sk:dbg            CBM_LogDebug( "event=",evnt->name(), "  r=",evnt->r,"  t=",evnt->t_exec, "  t-sim=",_time_stamp);
            if ( evnt->r == 0)
            {
                CBM_LogDebug( "push to replay buffer and drop from working buffer  [event=\"",evnt->name(),"\"]");

                /* reset repeat counter and append to "future l0" */
                evnt->r = evnt->R;
                _l1->push_back( *evnt);
                /* drop element ... */
                evnt = _l0->erase( evnt);

                continue;
            }
            this->ev_buf_[l] = *evnt;
            ++l;
            evnt->r -= 1;
            ++evnt;
        }
        this->ev_cnt_ = l;
    }

    return  LDNDC_ERR_OK;
}
lerr_t
event_dispatch_list_t::reupdate(
        ltime_t::td_scalar_t const &  _time_stamp,
        event_list_t *  _l0, event_list_t *  _l1)
{
    this->update_time_ = ( this->update_time_ == 0)
        ? TD_unset : ( this->update_time_-1);
    return  this->update( _time_stamp, _l0, _l1);
}


Event const *
event_dispatch_list_t::operator[](
        size_t  _k)
const
{
    return  ( _k < this->ev_cnt_) ? this->ev_buf_[_k].event : NULL;
}

}}


#include  "io/input-source.h"
namespace  ldndc{ namespace  event
{
LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(event)
lerr_t
input_class_event_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *  _input_source,
        ltime_t const *  _time)
{
    /* guard */
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    set_input_global_class_reader(event);

    /* read datas time offset and resolution */
    if ( cbm::is_empty( this_reader->time()))
    {
        /* dates are set to default values and considered "not given" */
        if ( _time)
        {
            this->otime_ = *_time;
        }
    }
    else
    {
        if ( this->otime_.from_string( this_reader->time().c_str(), _time) != LDNDC_ERR_OK)
        {
            return  LDNDC_ERR_FAIL;
        }
    }
// sk:dbg    CBM_LogDebug( "time-rdr=", this_reader->time(), ", time-ic=", otime_.to_string(), ", time-sim=", (_time) ? _time->to_string(): "-");


    this->set_initialized();
    return  LDNDC_ERR_OK;
}

}}

