/*!
 * @brief
 *    container for (management) event input data
 *
 * @author
 *    steffen klatt (created on: dec 08, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_SERVER_H_
#define  LDNDC_INPUT_EVENT_SERVER_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "io/source-handle-srv.h"

#include  "input/ic-srv.h"
#include  "input/event/event-tree.h"
#include  "event/eventtypes.h"
#include  "event/events/eventbase.h"
#include  "containers/lgrowarray.h"

#include  "openmp/cbm_omp.h"

#include  <list>

namespace  ldndc
{
    class  input_source_srv_base_t;

namespace  event
{
class  event_list_t
{
    struct  event_date_compare
    {
        bool operator()(
                event_handle_t const &  _h1, event_handle_t const &  _h2)
        const
        {
            return  _h1.t_exec < _h2.t_exec;
        }
    };
    public:
        typedef  std::list< event_handle_t >  event_buffer_t;
        typedef  event_buffer_t::iterator  iterator_t;
        typedef  event_buffer_t::const_iterator  const_iterator_t;

    public:
        size_t  size() const { return  this->ev_buf_.size(); }

        void  clear()
        {
            this->ev_buf_.clear();
        }

        void  push_back(
                event_handle_t  _ev_h)
        {
            this->ev_buf_.push_back( _ev_h);
        }

        /* iterators */
        iterator_t  begin() { return  this->ev_buf_.begin(); }
        iterator_t  end() { return  this->ev_buf_.end(); }

        const_iterator_t  begin() const { return  this->ev_buf_.begin(); }
        const_iterator_t  end() const { return  this->ev_buf_.end(); }

        iterator_t  erase(
                iterator_t  _it)
        {
            return  this->ev_buf_.erase( _it);
        }

        void  sort_by_date()
        {
            this->ev_buf_.sort( event_date_compare());
// sk:dbg            CBM_LogDebug( "sorted full event list, size =", this->ev_buf_.size());
        }

        int  drop_unreachable_events(
                ltime_t const &  _t_sim,
                int *  _drop_past, int *  _drop_future)
        {
            ltime_t::td_scalar_t  sim_beg( _t_sim.from().seconds_since_epoch());
            ltime_t::td_scalar_t  sim_end( _t_sim.to().seconds_since_epoch());

            int  drop_past = 0;
            int  drop_future = 0;

            iterator_t  e( this->ev_buf_.begin());
            while ( e != this->ev_buf_.end())
            {
                if ( e->t_exec < sim_beg)
                {
                    e = erase( e);
                    ++drop_past;
                }
                else if ( e->t_exec > sim_end)
                {
                    e = erase( e);
                    ++drop_future;
                }
                else
                {
                    ++e;
                }
            }

            if ( _drop_past) { *_drop_past = drop_past; }
            if ( _drop_future) { *_drop_future = drop_future; }

            return  drop_past + drop_future;
        }

        void  dump_events()
        const
        {
            const_iterator_t  e( this->ev_buf_.begin());
            while ( e != this->ev_buf_.end())
            {
                ++e;
            }
        }

    private:
        event_buffer_t  ev_buf_;
};


struct  event_tree_t
{
    event_tree_t();

    size_t  unroll( event_list_t *, ltime_t const &);

    cbm::event_info_tree_t  root;
private:
    size_t  unroll_(
        event_list_t *, ltime_t const &,
        cbm::event_info_tree_t::sibling_iterator_t,
        size_t = 1);
};


/*!
 * @brief
 *    holds one time step worth of events
 *
 * @todo
 *    add iterators?
 */
class  event_dispatch_list_t
{
    typedef  ldndc::growable_array< event_handle_t, 192 >  event_buffer_t;
    public:
        event_dispatch_list_t();

        size_t  max_size() const { return  event_buffer_t::MAX_SIZE; }
        inline  size_t  size() const { return  this->ev_cnt_; }

        Event const *  operator[]( size_t) const;

        lerr_t  reset();

        lerr_t  update(
                ltime_t::td_scalar_t const &,
                event_list_t *, event_list_t *);
        lerr_t  reupdate(
                ltime_t::td_scalar_t const &,
                event_list_t *, event_list_t *);

        event_handle_t const &  handle( size_t  _k)
        const
        {
            return  this->ev_buf_[_k];
        }

    private:
        event_buffer_t  ev_buf_;
        size_t  ev_cnt_;

        ltime_t::td_scalar_t  update_time_;
};


/*!
 * @brief
 *    this class is like a stencil to filter a single event
 *    type from all events for current time step. it provides
 *    an iterator mechanism.
 *
 * @note
 *    potentially dangerous because the underlying event list
 *    is going to be invalidated at the next time step.
 */
template < typename  _E >
class  events_view_t_
{
    public:
        class  const_iterator
        {
            public:
                /*!
                 * @brief
                 *    constructor
                 *
                 * if list holds no events (i.e. it's empty) handle and
                 * event pointers will be set to match end() iterator
                 */
                explicit  const_iterator(
                        events_view_t_< _E > const *  _ev_l)
                        : ev_l_( _ev_l),
                          ev_h_(( !_ev_l || (_ev_l->size() == 0)) ? NULL : &(_ev_l->handle_( 0))),
                          ev_(( !ev_h_) ? NULL : static_cast< _E const * >( ev_h_->event)),
                          k_( 0)
                {
                }
                /*!
                 * @brief
                 *    copy constructor for const iterator
                 */
                const_iterator(
                        const_iterator const &  _it)
                        : ev_l_( _it.ev_l_), ev_h_( _it.ev_h_),
                          ev_( _it.ev_), k_( _it.k_)
                {
                }

                /*!
                 * @brief
                 *    dereferences event, convenience shortcut to
                 *    event properties
                 */
                _E const *  operator->()
                const
                {
                    crabmeat_assert( this->ev_);
                    return  this->ev_;
                }
                /*!
                 * @brief
                 *    dereference operator to get reference to event
                 *    via iterator
                 */
                _E const &  operator*()
                const
                {
                    crabmeat_assert( this->ev_);
                    return  *this->ev_;
                }

                /*!
                 * @brief
                 *    query execution time step
                 */
                ldate_t::td_scalar_t  t_exec()
                const
                {
                    crabmeat_assert( this->ev_h_);
                    return  this->ev_h_->t_exec;
                }
                /*!
                 * @brief
                 *    query execution time span
                 *
                 * @return
                 *    number of time steps for event duration (always
                 *    1 for non-period events
                 */
                ltime_t::td_scalar_t  t_span()
                const
                {
                    crabmeat_assert( this->ev_h_);
                    return  this->ev_h_->R;
                }

                /*!
                 * @brief
                 *    check iterator for equality. two iterators are
                 *    considered equal if they point to the same 
                 *    event.
                 *
                 * @todo
                 *    compare handle IDs !?
                 */
                bool  operator==( const_iterator const &  _it)
                const
                {
                    return  ( this->ev_ == _it.ev_);
                }
                /*!
                 * @brief
                 *    check iterator for inequality. two iterators are
                 *    considered not equal if they do not point to the
                 *    same event.
                 *
                 * @todo
                 *    compare handle IDs !?
                 */
                bool  operator!=( const_iterator const &  _it)
                const
                {
                    return  ( this->ev_ != _it.ev_);
                }

                /*!
                 * @brief
                 *    advance iterator to next position in event view.
                 *    it sets pointer to event to null if it hit the
                 *    end of the buffer. note that this matches the
                 *    special end() iterator where matches means that
                 *    comparing them for equality will yield true.
                 */
                const_iterator &  operator++()
                {
                    if ( this->ev_l_ && ( ++this->k_ < this->ev_l_->size()))
                    {
                        this->ev_h_ = &this->ev_l_->handle_( this->k_);
                        this->ev_ = static_cast< _E const * >( this->ev_h_->event);
                    }
                    else
                    {
                        --this->k_;
                        this->ev_h_ = NULL;
                        /* match iterator end() */
                        this->ev_ = NULL;
                    }
                    return  *this;
                }

            private:
                /* _not_ owner */
                events_view_t_< _E > const *  ev_l_;
                /* _not_ owner */
                event_handle_t const *  ev_h_;
                /* _not_ owner */
                _E const *  ev_;
                size_t  k_;
        };
    /*!
     * @brief
     *    iterator class is friend of event view because we 
     *    need access to its private member handle_()
     */
//    friend  class events_view_t_< _E >::const_iterator;
    public:
        explicit  events_view_t_( event_dispatch_list_t const *);
        virtual  ~events_view_t_();

        /*!
         * @brief
         *    return the number of events held by this
         *    objects event list.
         */
        size_t  size() const { return  this->ev_cnt_; }

        /*!
         * @brief
         *    access element of this objects event list by
         *    index. if index is greater than number of
         *    elements available the program will exit.
         */
        _E const *  operator[]( size_t) const;

        /*!
         * @brief
         *    return iterator to first element in event
         *    list. if event list is empty it returns the
         *    special iterator end() to signal end of
         *    list.
         */
        const_iterator  begin()
        const
        {
            if ( this->ev_cnt_ > 0)
            {
                return  const_iterator( this);
            }
            return  this->end();
        }
        /*!
         * @brief
         *    the special iterator signaling end of
         *    event list.
         */
        const_iterator  end()
        const
        {
            return  const_iterator( NULL);
        }

    protected:
        /*! event list (_not_ owner) */
        event_dispatch_list_t const *  ev_list_;
        /*! number of selected events */
        size_t  ev_cnt_;

        /*! helper function to get k-th element from event list */
        virtual  event_handle_t const &  handle_( size_t  _k) const = 0;
};

template < class  _E >
events_view_t_< _E >::events_view_t_(
        event_dispatch_list_t const *  _ev_list)
        : ev_list_( _ev_list),
          ev_cnt_( 0)
{
}

template < class  _E >
events_view_t_< _E >::~events_view_t_()
{
}


template < class  _E >
_E const *
events_view_t_< _E >::operator[](
        size_t  _k)
const
{
    if ( _k < this->ev_cnt_)
    {
        return  static_cast< _E const * >( this->handle_( _k).event);
    }

    CBM_LogError( "events_view_t_< _E >::operator[](): access violation in event view");
    return  NULL;
}


/*!
 * @brief
 *    event view specialization that selects event type
 *    specified by the template parameter _E
 */
template < class  _E >
class  events_view_t  :  public  events_view_t_< _E >
{
    enum
          {
        event_type_ = _E::event_type_
    };
    public:
        explicit  events_view_t( event_dispatch_list_t const *);

        events_view_t( events_view_t< _E > const &);

        events_view_t< _E > &  operator=( events_view_t< _E > const &);

        ~events_view_t();

    protected:
        /*! helpers to speed up consecutive look ups (size == 2*#threads ???) */
        mutable size_t  k0_, k1_;
        /*! openmp parallel version helper */
        cbm::omp::omp_lock_t *  mxlock_;

        /*! helper function to get k-th element from event list */
        event_handle_t const &  handle_( size_t) const;
};

template < class  _E >
events_view_t< _E >::events_view_t(
        event_dispatch_list_t const *  _ev_list)
        : events_view_t_< _E >( _ev_list),
          k0_( invalid_size),
          k1_( invalid_size),
          mxlock_( NULL)
{
    this->mxlock_ = cbm::omp::new_lock();
    cbm::omp::init_lock( this->mxlock_);

    size_t  ev_cnt( 0);
    for ( size_t  k = 0;  _ev_list && ( k < _ev_list->size());  ++k)
    {
        ev_cnt += ( _ev_list->handle(k).type() == (event_type_e)this->event_type_) ? 1 : 0;
    }
    events_view_t_< _E >::ev_cnt_ = ev_cnt;
}

template < class  _E >
events_view_t< _E >::events_view_t(
        events_view_t< _E > const &  _rhs)
        : events_view_t_< _E >( _rhs),
          k0_( _rhs.k0_),
          k1_( _rhs.k1_)
{
    this->mxlock_ = cbm::omp::new_lock();
}

template < class  _E >
events_view_t< _E > &
events_view_t< _E >::operator=(
        events_view_t< _E > const &  _rhs)
{
    if ( this == &_rhs)
    {
        return  *this;
    }

    events_view_t_< _E >::operator=( _rhs);

    this->k0_ = _rhs.k0_;
    this->k1_ = _rhs.k1_;

    this->mxlock_ = cbm::omp::new_lock();

    return  *this;
}


template < class  _E >
events_view_t< _E >::~events_view_t()
{
    cbm::omp::destroy_lock( this->mxlock_);
    cbm::omp::free_lock( this->mxlock_);
}

template < class  _E >
event_handle_t const &
events_view_t< _E >::handle_(
        size_t  _k)
const
{
    cbm::omp::set_lock( this->mxlock_);

    size_t  j( invalid_size);

    /* in threaded mode the caching of the final
     * position might be pretty useless
     */
    if ( this->k0_ == _k)
    {
        j = this->k1_;
    }
           else
    {
        size_t  L( this->ev_list_->size());
        size_t  k( 0);

        for ( size_t  l = 0;  l < L;  ++l)
        {
            if ( this->ev_list_->handle(l).type() == (event_type_e)this->event_type_)
            {
                if ( k == _k)
                {
                    this->k0_ = _k;
                    this->k1_ = l;
                    j = l;
                    break;
                }
                ++k;
            }
        }
        if ( j == invalid_size)
        {
            CBM_LogFatal( "[BUG]  events_view_t< _E >::handle_(): access violation in event view [size=",L,",k=",_k,"]");
        }
    }

    cbm::omp::unset_lock( this->mxlock_);
    return  this->ev_list_->handle( j);
}


/*!
 * @brief
 *    event view specialization that selects all event types
 */
template < >
class  events_view_t< Event >  :  public  events_view_t_< Event >
{
    public:
        explicit  events_view_t(
                event_dispatch_list_t const *  _ev_list)
                : events_view_t_< Event >( _ev_list)
        {
            events_view_t_< Event >::ev_cnt_ = _ev_list->size();
        }

    protected:
        /*! helper function to get k-th element from event list */
        event_handle_t const &  handle_(
                size_t  _k)
        const
        {
            if ( _k < this->ev_list_->size())
            {
                return  this->ev_list_->handle( _k);
            }
            CBM_LogFatal( "[BUG]  events_view_t< Event >::handle_(): access violation in event view [size=",this->ev_list_->size(),",k=",_k,"]");
            return  this->ev_list_->handle( 0);
        }
};


/*!
 * @brief
 *    event (aka management) input stream high level interface
 */
class  CBM_API  input_class_event_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(event,ICLASS_FLAG_STATE)
    public:
        /*!
         * @brief
         *    reset to first timesteps events
         */
        lerr_t  reset_internal_state();
        /*!
         * @brief
         *    select current time step's events
         */
        lerr_t  update_internal_state(
                cbm::sclock_t const &);

        /*!
         * @brief
         *    number of events to be scheduled for
         *    remaining simulation time
         */
        size_t  remaining_event_cnt() const;


        /*!
         * @brief
         *    this time step's event count
         */
        size_t  event_cnt() const;
        /*!
         * @brief
         *    convenience function to check if there are
         *    events pending for this time step
         */
        bool  have_events() const;

        /*!
         * @brief
         *    return k-th event from list of events
         *    for current time step
         */
        Event const *  operator[]( size_t) const;

        /*!
         * @brief
         *    returns the subset of events limited
         *    to specified management event type
         *
         * @n
         *    the event type is determined by the
         *    events class name (e.g. EventPlant
         *    for planting events). it is also
         *    possible to use the base class (i.e.
         *    Event) to get all events for current
         *    time step. the user will then have to
         *    determine the type and cast it appropriatly
         *    herself.
         */
        template < typename  _E >
        events_view_t< _E >  events_view()
        const
        {
            return  events_view_t< _E >( &this->ev_sel_);
        }
        /*!
         * @brief
         *    return number of events of type given
         *    by template parameter for current time
         *    step.
         */
        template < typename  _E >
        size_t  size()
        const
        {
            return  this->events_view< _E >().size();
        }

        int  max_readahead() const;

    public:
        size_t  get_event_handles_cnt() const;
        lerr_t  get_event_handles(
                event_handle_t * /*buffer*/, size_t /*maximum to get*/) const;

    private:
        /* unrolled full event list
         *
         * l0 (pointing to b0) holds initial list of events. at each
         * state update, the list is searched for events (if such exist)
         * to dispatch. if an event's repeat counter has reached zero
         * the event is dropped from l0 and added to l1. during
         * a state reset l0 and l1 are toggled such that l0 again points
         * to the "full" list.
         */
        struct
        {
            event_list_t  b0;
            event_list_t  b1;

            event_list_t *  l0;
            event_list_t *  l1;
        }  e_buf;

        /* subset of events that are to be executed at current time step */
                event_dispatch_list_t  ev_sel_;
        /* holds pointers to event (owner of event objects) */
        std::list< Event * >  ev_owner_;

        ldndc::source_handle_srv_t  sh;
        ltime_t  ref_time;

        lerr_t  load_events_(
                event_tree_t * /*event buffer*/,
                int  /*number of events to load*/,
                source_handle_srv_t const *);
        lerr_t  enqueue_loaded_events_(
                event_tree_t * /*event buffer*/,
                ltime_t const * /*drop events outside time boundaries*/,
                int * /*#events in past*/, int * /*#events in future*/);
};
}}


#include  "time/cbm_time.h"
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  event
{
class  CBM_API  input_class_event_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(event,ICLASS_FLAG_NONE)
    public:
        virtual  bool  has_time()
            const { return  true; }

        ltime_t const &  get_time()
            const { return  this->otime_; }

    private:
        /* time offset for data file ( subday offset, day offset, year offset) */
        ltime_t  otime_;
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_SERVER_H_  */

