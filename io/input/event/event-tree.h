/*!
 * @brief
 *    minimal event tree data structure
 *
 * @author
 *    steffen klatt  (created on: june 10, 2021)
 */

#ifndef  CBM_EVENTTREE_H_
#define  CBM_EVENTTREE_H_

#include <stdio.h>

#include  "crabmeat-common.h"
namespace cbm {

template< typename _T >
class tree_node_t {
    public:
        tree_node_t();
        tree_node_t( _T const & );

        tree_node_t<_T> *parent;
        tree_node_t<_T> *sibling;
        tree_node_t<_T> *child;

        _T data;
};

template< typename _T >
tree_node_t<_T>::tree_node_t()
    : parent( NULL ), sibling( NULL ), child( NULL )
{ }

template< typename  _T >
tree_node_t<_T>::tree_node_t( _T const & _data )
    : parent( NULL ), sibling( NULL ), child( NULL ), data( _data )
{ }

template< typename _T >
class _event_tree_t {
public:
    _event_tree_t();
    ~_event_tree_t();

    class iterator_base_t {
    public:
        iterator_base_t() : node( NULL )
            { }
        iterator_base_t( tree_node_t<_T> * _node )
            : node( _node )
            { }

        _T& operator*() const
            { return this->node->data; }
        _T* operator->() const
            { return &this->node->data; }

        tree_node_t<_T> *node;
    };

    class iterator_t : public iterator_base_t {
    public:
        iterator_t() : iterator_base_t()
            { }
        iterator_t( tree_node_t<_T> * _node )
            : iterator_base_t( _node )
            { }

        bool operator==( iterator_t const & _rhs )
            { return this->node==_rhs.node; }
        bool operator!=( iterator_t const & _rhs )
            { return this->node!=_rhs.node; }
        iterator_t & operator++()
        {
            if ( this->node ) {
                if ( this->node->child ) {
                    this->node = this->node->child;
                } else if ( this->node->sibling ) {
                    this->node = this->node->sibling;
                } else {
                    this->node = this->node->parent;
                    while ( this->node && this->node->sibling==NULL ) {
                        this->node = this->node->parent;
                    }
                    if ( this->node ) {
                        this->node = this->node->sibling;
                    }
                }
            }
            return *this;
        }
    };

    class sibling_iterator_t : public iterator_base_t {
    public:
        sibling_iterator_t() : iterator_base_t()
            { }
        sibling_iterator_t( tree_node_t<_T> * _node )
            : iterator_base_t( _node )
            { }

        bool operator==( sibling_iterator_t const & _rhs ) const
            { return this->node==_rhs.node; }
        bool operator!=( sibling_iterator_t const & _rhs ) const
            { return this->node!=_rhs.node; }
        sibling_iterator_t & operator++()
        {
            if ( this->node ) {
                this->node = this->node->sibling;
            }
            return *this;
        }
    };

    /* attach root node, root child or new sibling node before <node> */
    iterator_t insert( iterator_t /*node*/, _T const & /*data*/ );
    /* attach new sibling node to last child of <node> */
    iterator_t append_child( iterator_t /*node*/, _T const & /*data*/ );

    iterator_t begin() const;
    iterator_t end() const;

    sibling_iterator_t begin( iterator_base_t const & ) const;
    sibling_iterator_t end( iterator_base_t const & ) const;

private:
    tree_node_t<_T> *root;
    void delete_children( tree_node_t<_T> * );
};

template< typename _T >
_event_tree_t<_T>::_event_tree_t()
    : root( NULL )
{
}
template< typename _T >
_event_tree_t<_T>::~_event_tree_t()
{
    if ( this->root ) {
        this->delete_children( this->root );
        delete this->root;
        this->root = NULL;
    }
}

template< typename _T >
typename _event_tree_t<_T>::iterator_t _event_tree_t<_T>::insert(
            _event_tree_t<_T>::iterator_t _at,
                _T const & _data )
{
    tree_node_t<_T> *node = new tree_node_t<_T>( _data );

    if ( _at.node==NULL ) {
        if ( this->root!=NULL ) {
            delete node;
            return this->end();
        }
        this->root = node;
    } else if ( _at.node->parent==NULL ) {
        _at.node->child = node;
        node->parent = _at.node;
    } else {
        node->parent = _at.node->parent;
        if ( _at.node==node->parent->child ) {
            node->sibling = _at.node;
            node->parent->child = node;
        } else {
            tree_node_t<_T> *n = node->parent->child;
            while ( n->sibling != _at.node ) {
                n = n->sibling;
            }
            n->sibling = node;
            node->sibling = _at.node;
        }
    }

    return _event_tree_t<_T>::iterator_t( node );
}

template< typename _T >
typename _event_tree_t<_T>::iterator_t _event_tree_t<_T>::append_child(
        _event_tree_t<_T>::iterator_t _at,
            _T const & _data )
{
    if ( _at.node==NULL ) {
        return this->end();
    }

    tree_node_t<_T> *node = new tree_node_t<_T>( _data );
    node->parent = _at.node;

    if ( _at.node->child==NULL ) {
        _at.node->child = node;
    } else {
        tree_node_t<_T> *n = _at.node->child;
        while ( n->sibling!=NULL ) {
            n = n->sibling;
        }
        n->sibling = node;
    }

    return _event_tree_t<_T>::iterator_t( node );
}

template< typename _T >
typename _event_tree_t<_T>::iterator_t _event_tree_t<_T>::begin() const
{
    return _event_tree_t<_T>::iterator_t( this->root );
}

template< typename _T >
typename _event_tree_t<_T>::iterator_t _event_tree_t<_T>::end() const
{
    return _event_tree_t<_T>::iterator_t( NULL );
}

template< typename _T >
typename _event_tree_t<_T>::sibling_iterator_t _event_tree_t<_T>::begin(
		typename _event_tree_t<_T>::iterator_base_t const & _at ) const
{
    if ( _at.node->child==NULL ) {
        return this->end( _at );
    }
    return _event_tree_t<_T>::sibling_iterator_t( _at.node->child );
}

template< typename _T >
typename _event_tree_t<_T>::sibling_iterator_t _event_tree_t<_T>::end(
		typename _event_tree_t<_T>::iterator_base_t const & /*_at*/ ) const
{
    return _event_tree_t<_T>::sibling_iterator_t( NULL );
}


template< typename _T >
void _event_tree_t<_T>::delete_children( tree_node_t<_T> *node )
{
    tree_node_t<_T> *c = node->child;
    while ( c ) {
        tree_node_t<_T> *n = c;
        c = c->sibling;
        delete_children( n );

        delete n;
    }

    node->child = NULL;
}

} /* namespace cbm */

#include <event/eventtypes.h>

namespace cbm {
    typedef cbm::_event_tree_t<ldndc::event::event_info_t> event_info_tree_t;
} /* namespace cbm */

#endif  /* !CBM_EVENTTREE_H_ */

