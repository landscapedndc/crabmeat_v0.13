/*!
 * @brief
 *    container class for site parameters
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 */

#ifndef  LDNDC_INPUT_SITEPARAMETERS_SERVER_H_
#define  LDNDC_INPUT_SITEPARAMETERS_SERVER_H_

#include  "crabmeat-common.h"
#include  "input/ic-srv.h"
#include  "io/source-handle-srv.h"
#include  "string/cbm_string.h"

#include  "siteparameters/siteparameterstypes.h"

namespace  ldndc
{
namespace  siteparameters
{
class  CBM_API  input_class_siteparameters_srv_t  :  public  input_class_srv_base_t
{
    LDNDC_INPUT_SRV_CLASS_DECL(siteparameters,ICLASS_FLAG_STATE)
    public:
        lerr_t  reset_internal_state()
        {
            return  LDNDC_ERR_OK;
        }

        lerr_t  update();
        lerr_t  update_internal_state(
                cbm::sclock_t const &);

        /* expands to getters for all site parameters */
        SITEPARAMETERS_GETTERS

    public:
        siteparameter_t::float_type  RCNRL()
        const
        {
            return  ( 2.0 * RCNRVL() * RCNRR()) / (RCNRVL() + RCNRR());
        }
        siteparameter_t::float_type  CNAORG()
        const
        {
            return  ( 1.0 / ( RBO() / RCNB() + (1.0 - RBO()) / RCNH()));
        }

    public:
        std::string  parametervalue_as_string(
                char const *  _name) const;

    public:
        lerr_t  premarshal(
                void ** /*marshalling buffer*/) const;

    private:
        siteparameter_t  site_params_[siteparameters::SITEPARAMETERS_CNT];

        lerr_t  semantic_check_(
                int /*number of read parameters*/);

        ldndc::source_handle_srv_t  sh;
        lerr_t  read_siteparameters(
                source_handle_srv_t const *);
};
}}


#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  siteparameters
{
class  CBM_API  input_class_siteparameters_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(siteparameters,ICLASS_FLAG_NONE)
};
}}

#endif  /*  !LDNDC_INPUT_SITEPARAMETERS_SERVER_H_  */

