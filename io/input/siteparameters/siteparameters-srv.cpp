/*!
 * @brief
 *    reads site parameter from file
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 */

#include  "input/siteparameters/siteparameters-srv.h"

#include  "io/source-handle-srv.h"
#include  "io/iif/iif_siteparameters.h"

#include  "string/lstring-transform.h"
#include  "log/cbm_baselog.h"
#include  "utils/lutils.h"

#include  "math/lmath-float.h"


namespace  ldndc{ namespace  siteparameters
{
LDNDC_INPUT_SRV_CLASS_DEFN(siteparameters)
input_class_siteparameters_srv_t::input_class_siteparameters_srv_t()
        : input_class_srv_base_t( invalid_lid)
{
}
input_class_siteparameters_srv_t::input_class_siteparameters_srv_t(
        lid_t const &  _id)
        : input_class_srv_base_t( _id)
{
}

input_class_siteparameters_srv_t::~input_class_siteparameters_srv_t()
{
    // noop
}


lerr_t
input_class_siteparameters_srv_t::initialize(
        ldndc::source_handle_srv_base_t const *  _source_handle)
{
    if ( this->is_initialized())
    {
        return  LDNDC_ERR_OK;
    }
    crabmeat_assert( _source_handle);
    this->sh = *static_cast< source_handle_srv_t const * >( _source_handle);

    lerr_t  rc_read = this->update();
    if ( rc_read)
    {
        return  LDNDC_ERR_FAIL;
    }

    /* if successful ... */
    this->set_initialized();

    return  LDNDC_ERR_OK;
}
lerr_t
input_class_siteparameters_srv_t::update_internal_state(
        cbm::sclock_t const &)
{
    return  this->update();
}


lerr_t
input_class_siteparameters_srv_t::update()
{
    set_input_class_reader_from_handle(siteparameters, &this->sh);

    if ( !dynamic_cast< iproc_provider_t * >( this_reader)->data_available())
    {
        return  LDNDC_ERR_OK;
    }

    lerr_t  rc_read = this->read_siteparameters( &this->sh);
    if ( rc_read)
    {
        return  LDNDC_ERR_FAIL;
    }
    return  LDNDC_ERR_OK;
}
lerr_t
input_class_siteparameters_srv_t::read_siteparameters(
        source_handle_srv_t const *  _source_handle)
{
    set_input_class_reader(siteparameters);

    int  params_read = -1;
    int  params_seen =  0;
    lerr_t  rc_siteparameters = this_reader->get_siteparameters_set(
            site_params_,
            siteparameters::SITEPARAMETERS_NAMES,
            siteparameters::SITEPARAMETERS_TYPES,
            siteparameters::SITEPARAMETERS_CNT,
            &params_read, &params_seen);
    if ( rc_siteparameters)
    {
        CBM_LogError( "input_class_siteparameters_t::read_siteparameters(): ",
                "failed to read site parameters");
    }

    lerr_t  rc_check = this->semantic_check_( params_read);
    if ( rc_check)
    {
        return  LDNDC_ERR_FAIL;
    }
    CBM_LogDebug( "site parameters: read=", params_read, "  found=", params_seen);

    return  LDNDC_ERR_OK;
}

std::string
input_class_siteparameters_srv_t::parametervalue_as_string(
        char const *  _name)
const
{
    unsigned int  p_index( invalid_uint);

    cbm::find_index(
            /* NOTE  cannot uppercase if lower case params exist! */
            cbm::to_upper( _name).c_str(),
            siteparameters::SITEPARAMETERS_NAMES,
            siteparameters::SITEPARAMETERS_CNT,
            &p_index);

    if ( p_index != invalid_uint)
    {
        return  site_params_[p_index].get_value_as_string( siteparameters::SITEPARAMETERS_TYPES[p_index]);
    }
    return  "nan";
}

static
lerr_t
siteparameters_check_if_values_within_minmax_boundaries_s(
    input_class_siteparameters_srv_t const *  _siteparameters)
{
    CRABMEAT_FIX_UNUSED(_siteparameters);
    int  err = 0;
#   include  "siteparameters_check_minmax.cpp.inc"
    return  err ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

lerr_t
input_class_siteparameters_srv_t::semantic_check_(
        int  _params_read)
{
    lerr_t  rc = LDNDC_ERR_OK;

    for ( size_t  k = 0;  k < static_cast< size_t >( siteparameters::SITEPARAMETERS_CNT);  ++k)
    {
        if ( !this->site_params_[k].is_valid( siteparameters::SITEPARAMETERS_TYPES[k]))
        {
            CBM_LogWrite( "possibly missing: ", siteparameters::SITEPARAMETERS_NAMES[k]);
            rc = LDNDC_ERR_FAIL;
        }
    }
    if (( _params_read != (signed int)siteparameters::SITEPARAMETERS_CNT) || rc)
    {
        CBM_LogError( "input_class_siteparameters_t::read_siteparameters(): ",
                "currently we require the site parameter set to be complete  ",
                "[exspected=", siteparameters::SITEPARAMETERS_CNT,",found=", _params_read,"]");
        return  LDNDC_ERR_FAIL;
    }

    rc = siteparameters_check_if_values_within_minmax_boundaries_s( this);


    if (( this->KNO() + this->KN2O()) > 1.0)
    {
        CBM_LogError( "site parameters: sum of KNO and KN2O is larger than 1.0",
                " (suggestion: KNO=",  this->KNO()  / (this->KNO() + this->KN2O()),
                              "KN2O=", this->KN2O() / (this->KNO() + this->KN2O()),")");
        rc = LDNDC_ERR_FAIL;
    }

    if ( ! cbm::flt_equal( 1.0, this->LIQNH4_SLURRY() + this->LIQNO3_SLURRY() + this->LIQUREA_SLURRY() + this->LIQDON_SLURRY()))
    {
        CBM_LogError( "site parameters 'LIQNH4_SLURRY', 'LIQNO3_SLURRY', 'LIQUREA_SLURRY', 'LIQDON_SLURRY' must add up to 1.0");
        rc = LDNDC_ERR_FAIL;
    }
    if ( ! cbm::flt_equal( 1.0, this->LIQNH4_FARMYARD() + this->LIQNO3_FARMYARD() + this->LIQUREA_FARMYARD() + this->LIQDON_FARMYARD()))
    {
        CBM_LogError( "site parameters 'LIQNH4_FARMYARD', 'LIQNO3_FARMYARD', 'LIQUREA_FARMYARD', 'LIQDON_FARMYARD' must add up to 1.0");
        rc = LDNDC_ERR_FAIL;
    }
    if ( ! cbm::flt_equal( 1.0, this->LIQNH4_COMPOST() + this->LIQNO3_COMPOST() + this->LIQUREA_COMPOST() + this->LIQDON_COMPOST()))
    {
        CBM_LogError( "site parameters 'LIQNH4_COMPOST', 'LIQNO3_COMPOST', 'LIQUREA_COMPOST', 'LIQDON_COMPOST' must add up to 1.0");
        rc = LDNDC_ERR_FAIL;
    }
#ifdef  _DEBUG
    if ( rc == LDNDC_ERR_OK)
    {
        CBM_LogDebug( "passed site parameter semantic check");
    }
#endif

    return  rc;
}


LDNDC_INPUT_GLOBAL_SRV_CLASS_DEFN(siteparameters)
lerr_t
input_class_siteparameters_global_srv_t::initialize(
        ldndc::input_source_srv_base_t const *,
        ltime_t const *)
{
    /* guard */
    if ( is_initialized())
        { return  LDNDC_ERR_OK; }
    this->set_initialized();

    return  LDNDC_ERR_OK;
}

}}

