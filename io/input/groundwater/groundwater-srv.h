/*!
 * @brief
 *    high level access structure to groundwater
 *    input streams
 *
 * @author
 *    steffen klatt (created on: may 29, 2015),
 *    david kraus
 */

#ifndef  LDNDC_INPUT_GROUNDWATER_SERVER_H_
#define  LDNDC_INPUT_GROUNDWATER_SERVER_H_

#include  "crabmeat-common.h"

#include  "input/groundwater/groundwatertypes-srv.h"
#include  "input/icbase-streamdata.h"

namespace ldndc {
class  input_source_srv_base_t;
class  source_handle_srv_base_t;

class  iproc_interface_groundwater_t;

namespace  groundwater
{

#define  GROUNDWATER_RECORD_ITEM_SRV(__rec_item__,__offs__)  __LDNDC_STREAM_INPUT_BUFFER_ITEM(__rec_item__,RECORD_ITEM_##__offs__)

class  CBM_API  input_class_groundwater_srv_t  :  public  iclassbase_streamdata_t< groundwater_streamdata_info_t, _CONFIG_BUFFERSIZE_LOG_GROUNDWATER, record::RECORD_SIZE >, public  record
{
    LDNDC_INPUT_SRV_CLASS_DECL(groundwater,ICLASS_FLAG_STATE)
    public:
        char const **  record_item_names()
        const
        {
            return  RECORD_ITEM_NAMES;
        }

        data_filter_t< record::item_type > const **  datafilters()
        const
        {
            return  GROUNDWATER_DATAFILTER_LIST;
        }

    public:
        ltime_t const &  get_time() const;

        void  get_record(
                         record::item_type *  /*buffer*/,
                         size_t  /*timestep*/) const;

    public:
        GROUNDWATER_RECORD_ITEM_SRV(watertable,WATERTABLE)
        GROUNDWATER_RECORD_ITEM_SRV(no3,NO3)

    public:
        groundwater_info_t const *  info() const;
    private:
        groundwater_info_t  info_;
        lerr_t  read_info_(
                           ldndc::iproc_interface_groundwater_t const *);
};
}}

#include  "time/cbm_time.h"
#include  "input/ic-global-srv.h"
namespace  ldndc{ namespace  groundwater
{
class  CBM_API  input_class_groundwater_global_srv_t  :  public  input_class_global_srv_base_t
{
    LDNDC_INPUT_GLOBAL_SRV_CLASS_DECL(groundwater,ICLASS_FLAG_NONE)
    public:
        virtual  bool  has_time()
            const { return  true; }

        ltime_t const &  get_time()
            const { return  otime_; }

    private:
        /* time offset for data file ( subday offset, day offset, year offset) */
        ltime_t  otime_;
};
}}

#endif  /*  !LDNDC_INPUT_GROUNDWATER_SERVER_H_  */

