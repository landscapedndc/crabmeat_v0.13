/*!
 * @brief
 *    declare server types related to groundwater input
 *
 * @author
 *    steffen klatt (created on: may 29, 2015),
 *    david kraus
 */

#ifndef  LDNDC_INPUT_GROUNDWATERTYPES_SERVER_H_
#define  LDNDC_INPUT_GROUNDWATERTYPES_SERVER_H_

#include  "groundwater/groundwatertypes.h"

namespace  ldndc{ namespace  groundwater
{
struct  groundwater_streamdata_info_t  :  public  streamdata_info_t
{
    struct  header_data_t
    {
        /* buffer's data resolution */
        int  res;
    };
};
}}

#endif  /*  !LDNDC_INPUT_GROUNDWATERTYPES_SERVER_H_  */


