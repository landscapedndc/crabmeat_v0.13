/*!
 * @brief
 *    creates, initializes and stores the global input classes
 *
 * @author
 *    steffen klatt (created on: sep 13, 2011),
 */

#include  "input/ic-global-srv.h"

ldndc::input_class_global_srv_base_t::input_class_global_srv_base_t(
        lid_t const &  _id)
    : input_class_base_t(), cbm::server_object_t( _id)
{ }

ldndc::input_class_global_srv_base_t::~input_class_global_srv_base_t()
{ }

