/*!
 * @brief
 *    simulation project meta information (e.g. project name,
 *    project creator)
 *
 * @author
 *    steffen klatt (created on: apr 25, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_PROJECTFILE_PROJECTMETA_H_
#define  LDNDC_PROJECTFILE_PROJECTMETA_H_

#include  "crabmeat-common.h"

namespace  ldndc
{
    struct  CBM_API  package_require_t
    {
        std::string  version_required;
        std::string  minimum_version_required;
    };

    struct  CBM_API  project_info_t
    {
        std::string  project_name;
        std::string  project_creator;
        std::string  creation_date;
        std::string  creator_email;
    };

}  /*  namespace ldndc  */


#endif  /*  !LDNDC_PROJECTFILE_PROJECTMETA_H_  */

