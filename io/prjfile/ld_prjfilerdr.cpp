/*!
 * @brief
 *    reader for the LandscapeDNDC top-level project file (implementation)
 *
 *    holds both project file reader and deprecated setup global
 *    block reader.
 *
 * @author
 *    steffen klatt (created on: apr 25, 2013),
 *    edwin haas
 */

#include  "prjfile/ld_prjfilerdr.h"

#include  "io/source-attrs.h"
#include  "io/source-info.h"
#include  "io/sink-attrs.h"
#include  "io/sink-info.h"

#include  "log/cbm_baselog.h"
#include  "string/cbm_string.h"

#define  LDNDC_PROJECT_FILE_ROOT_TAG  "ldndcproject"
char const *  ldndc::project_file_reader_t::ROOT_TAG = LDNDC_PROJECT_FILE_ROOT_TAG;

LDNDC_OBJECT_DEFN(ldndc::project_file_reader_t)
ldndc::project_file_reader_t::project_file_reader_t()
                : ldndc::object_t()
{
    this->m_reset();
}
ldndc::project_file_reader_t::project_file_reader_t(
                lid_t const &  _id  /*project id*/)
                : ldndc::object_t( _id)
{
    this->m_reset();
}

ldndc::project_file_reader_t::~project_file_reader_t()
{
    this->m_cleanup( LDNDC_ERR_OK);
    this->close();
}


lerr_t
ldndc::project_file_reader_t::open(
                char const *  _project_file_name)
{
    this->m_projectfile = new tinyxml2::XMLDocument();

    if ( !this->m_projectfile)
    {
        CBM_LogError( "allocating project-files xml structures failed");
        return  LDNDC_ERR_NOMEM;
    }

    this->m_projectfile->LoadFile( _project_file_name);
    if ( this->m_projectfile->Error())
    {
        CBM_LogError( "error occured during load,  error=",this->m_projectfile->ErrorID());
        if ( this->m_projectfile->GetErrorStr1())
        {
            CBM_LogError( "\terror diagnostic-1:",this->m_projectfile->GetErrorStr1());
        }
        if ( this->m_projectfile->GetErrorStr2())
        {
            CBM_LogError( "\terror diagnostic-2: ",this->m_projectfile->GetErrorStr2());
        }

        this->m_projectfile->PrintError();
        delete  this->m_projectfile;
        this->m_projectfile = NULL;
        this->m_reset();

        return  LDNDC_ERR_FAIL;
    }

    /* attempt to determine mode */
    {
        tinyxml2::XMLNode *  xml_node = this->m_projectfile->RootElement();
        if ( !xml_node)
        {
            return  this->m_cleanup( LDNDC_ERR_READER_OPEN_FAILED);
        }
        if ( cbm::is_equal( xml_node->Value(), ldndc::project_file_reader_t::ROOT_TAG))
        {
            this->m_mode = project_file_reader_t::FORMAT_PROJECT_FILE;
        }
        else if ( cbm::is_equal( xml_node->Value(), "ldndcsetup"))
        {
            this->m_mode = project_file_reader_t::FORMAT_PROJECT_SETUP_MULTI;
        }
        else if ( cbm::is_equal( xml_node->Value(), "setup"))
        {
            this->m_mode = project_file_reader_t::FORMAT_PROJECT_SETUP_SINGLE;
        }
        else
        {
            CBM_LogError( "project file format not understood");
            return  this->m_cleanup( LDNDC_ERR_READER_OPEN_FAILED);
        }
    }

    /* initialize handles */
    {
        /* attribute handle (assumed non-null after successfully reading root-tag) */
        tinyxml2::XMLNode *  xml_node = this->m_projectfile->RootElement();
        if ( !xml_node)
        {
            return  this->m_cleanup( LDNDC_ERR_READER_OPEN_FAILED);
        }

        if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_FILE)
        {
            xml_node = xml_node->FirstChildElement( "input");
            if ( xml_node)
            {
                this->sattr_handle_ = xml_node->FirstChildElement( "attributes");
            }
        }
        else if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_SETUP_MULTI)
        {
            xml_node = xml_node->FirstChildElement( "global");
            if ( xml_node)
            {
                this->sattr_handle_ = xml_node->FirstChildElement( "files");
            }
        }
        else if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_SETUP_SINGLE)
        {
            this->sattr_handle_ = xml_node->FirstChildElement( "files");
        }
        else
        {
            CBM_LogFatal( "[BUG]  no no no");
        }

        /* not finding attributes is not a problem: they are optional */
    }

    {
        char const *  searchpath_input = NULL;
        static char const *  searchpath_project_file_i = LDNDC_PROJECT_FILE_ROOT_TAG "/input/sources";
        static char const *  searchpath_setup_multi_i = "ldndcsetup/global/files";
        static char const *  searchpath_setup_single_i = "setup/files";

        char const *  searchpath_output = NULL;
        static char const *  searchpath_project_file_o = LDNDC_PROJECT_FILE_ROOT_TAG "/output/sinks";

        tinyxml2::XMLNode *  xml_node = this->m_projectfile->RootElement();
        if ( !xml_node)
        {
            return  this->m_cleanup( LDNDC_ERR_READER_OPEN_FAILED);
        }
        /* sources and sink handles */
        if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_FILE)
        {
            tinyxml2::XMLNode *  xml_node_input = xml_node->FirstChildElement( "input");
            if ( xml_node_input)
            {
                this->source_handle_ = xml_node_input->FirstChildElement( "sources");
            }
            searchpath_input = searchpath_project_file_i;

            tinyxml2::XMLNode *  xml_node_output = xml_node->FirstChildElement( "output");
            if ( xml_node_output)
            {
                this->sink_handle_ = xml_node_output->FirstChildElement( "sinks");
            }
            searchpath_output = searchpath_project_file_o;

        }
        else if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_SETUP_MULTI)
        {
            this->source_handle_ = this->sattr_handle_;
            searchpath_input = searchpath_setup_multi_i;

            /* no output sink definitions here */
            this->sink_handle_ = NULL;
        }
        else if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_SETUP_SINGLE)
        {
            this->source_handle_ = this->sattr_handle_;
            searchpath_input = searchpath_setup_single_i;

            /* no output sink definitions here */
            this->sink_handle_ = NULL;
        }
        else
        {
            CBM_LogFatal( "[BUG]  no no no");
        }

        /* just informational .. not finding inputs is not critical */
        if ( searchpath_input && !this->source_handle_)
        {
            CBM_LogVerbose( "found no inputs in project file. falling back to defaults.  [search path=\"",searchpath_input,"\"]");
        }
        /* just informational .. not finding outputs is not critical */
        if ( searchpath_output && !this->sink_handle_)
        {
            CBM_LogVerbose( "found no outputs in project file  [search path=\"",searchpath_output,"\"]");
        }
    }

    /* store source and attribute base handles */
    this->sattr_base_handle_ = this->sattr_handle_;
    this->source_base_handle_ = this->source_handle_;

    this->sink_base_handle_ = this->sink_handle_;


    return  LDNDC_ERR_OK;
}
void
ldndc::project_file_reader_t::close()
{
    if ( this->m_projectfile)
        { delete  this->m_projectfile; }
}


#include  "prjfile/cbm_prjinfo.h"
lerr_t
ldndc::project_file_reader_t::read_package_require(
        ldndc::package_require_t *  _pkg_require)
{
    crabmeat_assert( this->m_projectfile);
    crabmeat_assert( _pkg_require);

    tinyxml2::XMLElement *  xml_node = NULL;
    xml_node = this->m_projectfile->RootElement();
    if ( xml_node)
    {
        char const *  pkg_version_required =
            xml_node->Attribute( "PackageVersionRequired");
        if ( pkg_version_required)
        {
            _pkg_require->version_required =
                    pkg_version_required;
        }
        char const *  pkg_minimum_version_required =
            xml_node->Attribute( "PackageMinimumVersionRequired");
        if ( pkg_minimum_version_required)
        {
            _pkg_require->minimum_version_required =
                    pkg_minimum_version_required;
        }
    }
    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::project_file_reader_t::read_projectinfo(
        ldndc::project_info_t *  _info)
{
    crabmeat_assert( this->m_projectfile);
    crabmeat_assert( _info);

    tinyxml2::XMLElement *  xml_node = NULL;
    xml_node = this->m_projectfile->RootElement()->FirstChildElement( "description");
    if ( xml_node)
    {
        tinyxml2::XMLElement *  xml_node_author = xml_node->FirstChildElement( "author");
        if ( xml_node_author)
        {
            _info->project_creator = ( xml_node_author->GetText()) ? xml_node_author->GetText() : "";
        }
        tinyxml2::XMLElement *  xml_node_email = xml_node->FirstChildElement( "email");
        if ( xml_node_email)
        {
            _info->creator_email = ( xml_node_email->GetText()) ? xml_node_email->GetText() : "";
        }
        tinyxml2::XMLElement *  xml_node_name = xml_node->FirstChildElement( "name");
        if ( xml_node_name)
        {
            _info->project_name = ( xml_node_name->GetText()) ? xml_node_name->GetText() : "";
        }
        tinyxml2::XMLElement *  xml_node_date = xml_node->FirstChildElement( "date");
        if ( xml_node_date)
        {
            _info->creation_date = ( xml_node_date->GetText()) ? xml_node_date->GetText() : "";
        }
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::project_file_reader_t::read_schedule(
        cbm::string_t *  _schedule)
{
    crabmeat_assert( this->m_projectfile);
    crabmeat_assert( _schedule);

    tinyxml2::XMLElement *  xml_node = NULL;
    if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_FILE)
    {
        xml_node = this->m_projectfile->RootElement()->FirstChildElement( "schedule");
    }
    else if ( this->m_mode != project_file_reader_t::FORMAT_PROJECT_UNKNOWN)
    {
        xml_node = this->m_projectfile->RootElement()->FirstChildElement( "global");
    }
    else
    {
        CBM_LogFatal( "[BUG]  did you call open()?");
    }

    if ( xml_node && xml_node->Attribute( "time"))
    {
        *_schedule = xml_node->Attribute( "time");
        return  LDNDC_ERR_OK;
    }

    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}


#include  "utils/cbm_utils.h"
lerr_t
ldndc::project_file_reader_t::read_sources(
        ldndc::source_info_t *  _ssrc)
{
    if ( !this->source_handle_)
    {
        return  LDNDC_ERR_STOP_ITERATION;
    }

    if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_FILE)
    {
        if ( this->source_handle_ == this->source_base_handle_)
        {
            this->source_handle_ = this->source_handle_->FirstChildElement();
        }
        else
        {
            this->source_handle_ = this->source_handle_->NextSiblingElement();
        }
        if ( this->source_handle_)
        {
            lerr_t  rc_source = this->set_source_info_( _ssrc, this->source_handle_);
            RETURN_IF_NOT_OK(rc_source);
        }
    }
    else if ( this->m_mode != FORMAT_PROJECT_UNKNOWN)
    {
        if ( this->source_handle_ == this->source_base_handle_)
        {
            this->source_handle_ = this->source_handle_->FirstChildElement( "file");
        }
        else
        {
            this->source_handle_ = this->source_handle_->NextSiblingElement( "file");
        }
        if ( this->source_handle_)
        {
            lerr_t  rc_source = this->set_source_info_( _ssrc, this->source_handle_);
            RETURN_IF_NOT_OK(rc_source);
        }
    }
    else
    {
        CBM_LogFatal( "what?!");
    }

    return  ( this->source_handle_) ? LDNDC_ERR_OK : LDNDC_ERR_STOP_ITERATION;
}
lerr_t
ldndc::project_file_reader_t::set_source_info_(
        ldndc::source_info_t *  _ssrc,
        tinyxml2::XMLElement const *  _node)
{
    crabmeat_assert( _ssrc);
    crabmeat_assert( _node);

    _ssrc->identifier = _node->Value();

    /* input class type */
    char const *  type_c = _node->Attribute( "class");
    if ( !type_c)
    {
        if ( this->m_mode != project_file_reader_t::FORMAT_PROJECT_FILE)
            { return  LDNDC_ERR_FAIL; }
        else
            { type_c = _node->Value(); }
    }

    /* set class specific defaults */
    lerr_t  rc_si = source_info_default( _ssrc, type_c);
    if ( rc_si)
    {
        CBM_LogError( "errors occured when trying to resolve input type \"",type_c,"\"");
        return LDNDC_ERR_FAIL;
    }

    _ssrc->type = type_c;
    _ssrc->identifier = _node->Value();

    this->read_global_source_prefix( NULL);

    /* io source description */
    char const *  io_source_name_c = _node->Attribute( "source");
    if ( io_source_name_c)
        { _ssrc->name = io_source_name_c; }
    /* io source description prefix, use global one if no stream value was found */
    char const *  io_source_name_prefix_c = _node->Attribute( "sourceprefix");
    if ( io_source_name_prefix_c)
        { _ssrc->name_prefix = io_source_name_prefix_c; }
    else
        { _ssrc->name_prefix = this->m_sourceprefix; }
    /* io format */
    lerr_t  rc_io_format = this->m_resolve_io_format(
        &_ssrc->format, _node, _ssrc->name_prefix, _ssrc->name);
    RETURN_IF_NOT_OK(rc_io_format);

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::project_file_reader_t::read_global_source_prefix(
        cbm::string_t *  _stream_source_prefix)
{
    /* initialize to empty */
    this->m_sourceprefix.clear();
    if ( this->source_base_handle_)
    {
        /* try to obtain from project file */
        char const *  stream_source_prefix_c = this->source_base_handle_->Attribute( "sourceprefix");
        if ( stream_source_prefix_c)
        {
            this->m_sourceprefix = stream_source_prefix_c;
            if ( _stream_source_prefix)
            {
                *_stream_source_prefix = this->m_sourceprefix;
            }
            return  LDNDC_ERR_OK;
        }
    }
    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}



lerr_t
ldndc::project_file_reader_t::read_source_attributes(
        ldndc::source_attributes_t *  _sattr)
{
    if ( !this->sattr_handle_)
        { return  LDNDC_ERR_STOP_ITERATION; }

    if ( this->sattr_handle_ == this->sattr_base_handle_)
    {
        this->read_default_id();
        this->read_default_flags();
    }

    if ( this->m_mode == project_file_reader_t::FORMAT_PROJECT_FILE)
    {
        if ( this->sattr_handle_ == this->sattr_base_handle_)
        {
            this->sattr_handle_ = this->sattr_handle_->FirstChildElement();
        }
        else
        {
            this->sattr_handle_ = this->sattr_handle_->NextSiblingElement();
        }
        while ( this->sattr_handle_ && ( this->set_attribute_info_( _sattr, this->sattr_handle_) != LDNDC_ERR_OK))
        {
            this->sattr_handle_ = this->sattr_handle_->NextSiblingElement();
        }
    }
    else if ( this->m_mode != FORMAT_PROJECT_UNKNOWN)
    {
        if ( this->sattr_handle_ == this->sattr_base_handle_)
        {
            this->sattr_handle_ = this->sattr_handle_->FirstChildElement( "file");
        }
        else
        {
            this->sattr_handle_ = this->sattr_handle_->NextSiblingElement( "file");
        }
        if ( this->sattr_handle_)
        {
            lerr_t  rc_attr = this->set_attribute_info_( _sattr, this->sattr_handle_);
            RETURN_IF_NOT_OK(rc_attr);
        }
    }
    else
        { CBM_LogFatal( "what?!"); }

    return  ( this->sattr_handle_) ? LDNDC_ERR_OK : LDNDC_ERR_STOP_ITERATION;
}
lerr_t
ldndc::project_file_reader_t::set_attribute_info_(
        ldndc::source_attributes_t *  _sattr,
        tinyxml2::XMLElement const *  _node)
{
    crabmeat_assert( _sattr);
    crabmeat_assert( _node);

    /* input class type */
    char const *  type_c = _node->Attribute( "class");
    if ( !type_c)
    {
        if ( this->m_mode != project_file_reader_t::FORMAT_PROJECT_FILE)
            { return  LDNDC_ERR_FAIL; }
        else
            { type_c = _node->Value(); }
    }
    _sattr->type = type_c;


    /* default source id */
    char const *  default_id_c = _node->Attribute( "use");
    if ( default_id_c)
    {
        lerr_t  rc_id_convert = cbm::s2n( default_id_c, &_sattr->default_id);
        if ( rc_id_convert)
        {
            CBM_LogError( "non-numeric id:", default_id_c);
            return  LDNDC_ERR_FAIL;
        }
    }
    else
        { _sattr->default_id = this->m_defaultid; }

    /* stream flags */
    _sattr->flags = this->m_defaultflags;
    (void)this->check_stream_attribute_flag_( _node,
        source_attributes_t::IFLAG_NAMES[source_attributes_t::IFLAG_ENDLESS], &_sattr->flags);

    return  LDNDC_ERR_OK;
}


lid_t const &
ldndc::project_file_reader_t::read_default_id()
{
    this->m_defaultid = invalid_lid;
    if ( this->sattr_base_handle_)
    {
        char const *  default_id_c = this->sattr_base_handle_->Attribute( "use");
        if ( default_id_c)
        {
            lerr_t  rc_id_convert = cbm::s2n( default_id_c, &this->m_defaultid);
            if ( rc_id_convert)
            {
                CBM_LogError( "non-numeric id:", default_id_c);
            }
        }
    }

    if (( this->m_defaultid == invalid_lid) && ( this->m_mode == FORMAT_PROJECT_SETUP_SINGLE))
    {
        return  this->m_idzero;
    }
    return  this->m_defaultid;
}

lflags_t const &
ldndc::project_file_reader_t::read_default_flags()
{
    this->m_defaultflags = lflag_noflag;
    if ( this->sattr_base_handle_)
    {
        /* endless */
        (void)this->check_stream_attribute_flag_(
                this->sattr_base_handle_, 
                source_attributes_t::IFLAG_NAMES[source_attributes_t::IFLAG_ENDLESS],
                &this->m_defaultflags);

        /* add more here .. */
    }

    return  this->m_defaultflags;
}


lerr_t
ldndc::project_file_reader_t::check_stream_attribute_flag_(
        tinyxml2::XMLElement const *  _node,
        char const *  _flag_name,
        lflags_t *  _flags)
{
    crabmeat_assert( _node);
    char const *  flag_c = _node->Attribute( _flag_name);
    if ( flag_c)
    {
        bool  flag_status;
        lerr_t  rc_bool_convert = cbm::s2n< bool >( flag_c, &flag_status);
        if ( rc_bool_convert)
        {
            CBM_LogError( "invalid boolean expression:",flag_c);
            return  LDNDC_ERR_FAIL;
        }

        source_attributes_t::stream_attribute_flag_e  ssattr( source_attributes_t::IFLAG_NONE);
        lerr_t  rc_flag_type = cbm::find_enum( _flag_name, source_attributes_t::IFLAG_NAMES, source_attributes_t::IFLAG_CNT, &ssattr);
        if ( rc_flag_type)
        {
            CBM_LogError( "unknown stream attribute flag");
            return  LDNDC_ERR_FAIL;
        }
        if ( _flags)
        {
            if ( flag_status)
            {
                *_flags |= ssattr;
            }
            else
            {
                *_flags &= ~static_cast< lflags_t >( ssattr);
            }
        }
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}



/** outputs **/
lerr_t
ldndc::project_file_reader_t::read_global_sink_prefix(
        cbm::string_t *  _stream_sink_prefix)
{
    /* initialize to empty */
    this->m_sinkprefix.clear();

    /* only available in project file, i.e. mode == FORMAT_PROJECT_FILE */
    if ( !this->sink_base_handle_)
    {
        if ( _stream_sink_prefix)
        {
            *_stream_sink_prefix = "";
            if ( this->m_mode != FORMAT_PROJECT_FILE)
            {
                return  LDNDC_ERR_OK;
            }
        }
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }

    /* try to obtain from project file */
    char const *  stream_sink_prefix_c = this->sink_base_handle_->Attribute( "sinkprefix");
    if ( stream_sink_prefix_c)
    {
        this->m_sinkprefix = stream_sink_prefix_c;
        if ( _stream_sink_prefix)
        {
            *_stream_sink_prefix = this->m_sinkprefix;
        }
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
}

lerr_t
ldndc::project_file_reader_t::read_sinks(
        ldndc::sink_info_t *  _ssnk)
{
    if ( !this->sink_handle_)
    {
        return  LDNDC_ERR_STOP_ITERATION;
    }

    if ( this->sink_handle_ == this->sink_base_handle_)
    {
        this->sink_handle_ = this->sink_handle_->FirstChildElement();
    }
    else
    {
        this->sink_handle_ = this->sink_handle_->NextSiblingElement();
    }

    if ( this->sink_handle_)
    {
        return  this->set_sink_info_( _ssnk, this->sink_handle_);
    }

    return  LDNDC_ERR_STOP_ITERATION;
}

lerr_t
ldndc::project_file_reader_t::set_sink_info_(
        ldndc::sink_info_t *  _ssnk,
        tinyxml2::XMLElement const *  _node)
{
    crabmeat_assert( _ssnk);
    crabmeat_assert( _node);

    _ssnk->identifier = _node->Name();
    if ( _node->Attribute( "index"))
    {
        int  rc_index = _node->QueryIntAttribute( "index", &_ssnk->index);
        if (( rc_index != tinyxml2::XML_NO_ERROR) || ( _ssnk->index < 0))
        {
            CBM_LogWarn( "invalid index attribute for sink, using 0  [sink=",_ssnk->identifier,",index=",_node->Attribute( "index"),"]");
            _ssnk->index = 0;
        }
    }

    sink_info_t  snk_info;
    lerr_t  rc_sinkinfo = sink_info_default(
        STANDARD_SINKS, &snk_info, _ssnk->identifier.c_str(), NULL);
    RETURN_IF_NOT_OK(rc_sinkinfo);

    _ssnk->name = "";
    _ssnk->name_forced = 0;
    if ( _node->Attribute( "sink"))
    {
        _ssnk->name = _node->Attribute( "sink");
        if (( _ssnk->name.length() > 0) && ( _ssnk->name[0] == '!'))
        {
            _ssnk->name_forced = 1;
            std::string  name_without_exclamation_mark( _ssnk->name.c_str()+1);
            _ssnk->name = name_without_exclamation_mark;
        }
    }
    else
    {
        _ssnk->name = snk_info.name;
    }
    if ( cbm::is_empty( _ssnk->name) || cbm::is_whitespace( _ssnk->name))
    {
        CBM_LogError( "invalid sink name (empty) for sink  [sink=",_ssnk->identifier,",index=",_ssnk->index,"]");
        return  LDNDC_ERR_INVALID_ARGUMENT;
    }

    if ( _node->Attribute( "sinkprefix"))
    {
        _ssnk->name_prefix = _node->Attribute( "sinkprefix");
    }
    else
    {
        _ssnk->name_prefix = this->m_sinkprefix;
    }

    _ssnk->attrs = sink_attributes_defaults;

    if ( _node->Attribute( "uniform"))
    {
        char const *  p = _node->Attribute( "uniform");
        if ( !cbm::is_empty( p))
        {
            _ssnk->attrs.uniform = ( cbm::is_bool( p) && cbm::is_bool_true( p)) ? 1 : 0;
        }
    }
    if ( _node->Attribute( "ordered"))
    {
        char const *  p = _node->Attribute( "ordered");
        if ( !cbm::is_empty( p))
        {
            _ssnk->attrs.ordered = ( cbm::is_bool( p) && cbm::is_bool_true( p)) ? 1 : 0;
        }
    }
    if ( _node->Attribute( "overwrite"))
    {
        char const *  p = _node->Attribute( "overwrite");
        if ( !cbm::is_empty( p))
        {
            _ssnk->attrs.overwrite = ( cbm::is_bool( p) && cbm::is_bool_true( p)) ? 1 : 0;
        }
    }
    if ( _node->Attribute( "header"))
    {
        char const *  p = _node->Attribute( "header");
        if ( !cbm::is_empty( p))
        {
            _ssnk->attrs.noheader = ( cbm::is_bool( p) && cbm::is_bool_true( p)) ? 0 : 1;
        }
    }
    if ( _node->Attribute( "delimiter"))
    {
        char const *  d = _node->Attribute( "delimiter");
        if ( cbm::is_empty( d))
        {
            _ssnk->attrs.delimiter = sink_attributes_defaults.delimiter;
        }
        else if ( cbm::is_equal_i( d, "\\t"))
        {
            _ssnk->attrs.delimiter = '\t';
        }
        else if ( cbm::is_equal_i( d, "\\n"))
        {
            _ssnk->attrs.delimiter = '\n';
        }
        else if ( cbm::is_equal_i( d, "\\\\"))
        {
            _ssnk->attrs.delimiter = '\\';
        }
        else
        {
            _ssnk->attrs.delimiter = d[0];
        }
    }
    if ( _node->Attribute( "sizelimit"))
    {
        char const *  p = _node->Attribute( "sizelimit");
        if ( !cbm::is_empty( p))
        {
            lerr_t  rc_convert = cbm::s2n( p, &_ssnk->attrs.size_limit);
            RETURN_IF_NOT_OK(rc_convert);
        }
    }
    if ( _node->Attribute( "cachesize"))
    {
        char const *  p = _node->Attribute( "cachesize");
        if ( !cbm::is_empty( p))
        {
            lerr_t  rc_convert = cbm::s2n( p, &_ssnk->attrs.cache_size);
            RETURN_IF_NOT_OK(rc_convert);
        }
    }

    lerr_t  rc_rcvtype = this->resolve_receiver_type_(
            &_ssnk->rcv_type, _node, 'O');
    RETURN_IF_NOT_OK(rc_rcvtype);

    lerr_t  rc_format = this->m_resolve_io_format(
        &_ssnk->format, _node, _ssnk->name_prefix, _ssnk->name);
    RETURN_IF_NOT_OK(rc_format);

    return  LDNDC_ERR_OK;
}


/** helpers **/

/* receiver type */
lerr_t
ldndc::project_file_reader_t::resolve_receiver_type_(
    char *  _rt, tinyxml2::XMLElement const *  _hdl, char  _default)
{
    crabmeat_assert( _rt);
    crabmeat_assert( _hdl);

    *_rt = _default;

    size_t  j = 0;
    while ( STANDARD_SINKS[j].identifier)
    {
        if ( cbm::is_equal( STANDARD_SINKS[j].identifier, _hdl->Name()))
        {
            *_rt = STANDARD_SINKS[j].rcv_type;
            break;
        }
        ++j;
    }

    return  LDNDC_ERR_OK;
}

/* io format */
lerr_t
ldndc::project_file_reader_t::m_resolve_io_format(
        cbm::string_t *  _io_format, tinyxml2::XMLElement const *  _hdl,
        cbm::string_t const &  _nameprefix, cbm::string_t const &  _name)
{
    crabmeat_assert( _io_format);
    crabmeat_assert( _hdl);

    char const *  io_format_c = _hdl->Attribute( "format");
    if ( io_format_c)
        { *_io_format = io_format_c; }
    else
    {
        cbm::string_t  source_name = _nameprefix;
        source_name << _name;
        lerr_t const  rc_fexpand = source_name.format_expand();
        if ( rc_fexpand)
            { return LDNDC_ERR_FAIL; }
        /* find suffix and use as format */
        cbm::string_t const  fformat =
            cbm::string_t( strrchr( source_name.c_str(), '.')
                ? strrchr( source_name.c_str(), '.') + 1 : "");
        CBM_LogDebug( "fformat=",fformat);
        *_io_format = fformat;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::project_file_reader_t::m_cleanup(
        lerr_t  _err)
{
    if ( this->m_projectfile)
        { delete  this->m_projectfile; }
    this->m_reset();

    return  _err;
}
void
ldndc::project_file_reader_t::m_reset()
{
    this->m_projectfile = NULL;
    this->m_mode = FORMAT_PROJECT_UNKNOWN;

    /* inputs */
    this->sattr_base_handle_ = NULL;
    this->sattr_handle_ = NULL;
    this->source_base_handle_ = NULL;
    this->source_handle_ = NULL;

    this->m_sourceprefix.clear();
    this->m_defaultid = invalid_lid;
    this->m_idzero = 0;
    this->m_defaultflags = lflag_noflag;

    /* outputs */
    this->sink_base_handle_ = NULL;
    this->sink_handle_ = NULL;

    this->m_sinkprefix.clear();
}

