/*!
 * @brief
 *    interface to the LandscapeDNDC top-level project file
 *
 * @author
 *    steffen klatt (created on: apr 21, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_PROJECTFILE_H_
#define  LDNDC_PROJECTFILE_H_

#include  "crabmeat-common.h"
#include  "prjfile/cbm_prjinfo.h"

#include  "io/source-info.h"
#include  "io/source-attrs.h"
#include  "io/sink-info.h"
#include  "io/sink-attrs.h"

#include  "string/cbm_string.h"

#include  <list>
#include  <map>

namespace  ldndc
{
    class  project_file_reader_t;
    class  CBM_API  project_file_t  :  public  ldndc::object_t
    {
        LDNDC_OBJECT(project_file_t)
        public:
            project_file_t();
            project_file_t( lid_t const &  /*project id*/);
            ~project_file_t();

    
            /*!
             * @brief
             *    read project file. sets global simulation properties
             *    and reads input stream source descriptions
             */
            lerr_t  read( char const *);

            /*!
             * @brief
             *    project file name
             */
            char const *  name() const;

            /*!
             * @brief
             *    internal state of this object (read error, etc..)
             */
            lerr_t  status() const;

            /*!
             * @brief
             *    dump content of project file
             */
            void  dump() const;

            /*!
             * @brief
             *    get package requirements information
             *    (e.g., minimum package version)
             */
            lerr_t  package_require(
                ldndc::package_require_t * /*buffer*/) const;
            /*!
             * @brief
             *    get simulation meta information
             */
            lerr_t  project_info(
                ldndc::project_info_t * /*buffer*/) const;

            /*!
             * @brief
             *    get simulation run time
             */
            char const *  schedule() const;

            /*!
             * @brief
             *    return pointer to stream attributes of given
             *    stream type
             */
            ldndc::source_attributes_t const *  stream_attributes(
                    cbm::string_t const & /*type*/) const;
            lid_t  default_id( cbm::string_t const & /*type*/) const;

            char const *  global_source_prefix() const;
            char const *  global_sink_prefix() const;

        private:
            typedef  std::list< ldndc::source_info_t >  source_container_t;
            typedef  std::map< std::string, ldndc::source_attributes_t >  sourceattributes_container_t;
        public:
            typedef  source_container_t::const_iterator  source_const_iterator;
            source_const_iterator  sources_begin()
            const
            {
                return  source_const_iterator( this->m_sources.begin());
            }

            source_const_iterator  sources_end()
            const
            {
                return  source_const_iterator( this->m_sources.end());
            }

        private:
            typedef  std::list< ldndc::sink_info_t >  sink_container_t;
        public:
            typedef  sink_container_t::const_iterator  sink_const_iterator;
            sink_const_iterator  sinks_begin()
            const
            {
                return  sink_const_iterator( this->m_sinks.begin());
            }

            sink_const_iterator  sinks_end()
            const
            {
                return  sink_const_iterator( this->m_sinks.end());
            }


        private:
            /* status indicator */
            lerr_t  m_read;

            /* store file name for informational purposes */
            cbm::string_t  m_filename;

            /* package requirements */
            ldndc::package_require_t  m_packagerequire;
            /* simulation project meta information */
            ldndc::project_info_t  m_projectinfo;
            /* simulation run time */
            cbm::string_t  m_schedule;

            /** INPUT **/

            /* stream atttributes */
            sourceattributes_container_t  m_sourceattr;
            /* stream sources */
            source_container_t  m_sources;
            /* global sources prefix */
            cbm::string_t  m_sourceprefix;

            /** OUTPUT **/

            /* contains sinks listed in project file and system-wide defaults */
            sink_container_t  m_sinks;
            /* global sink prefix */
            cbm::string_t  m_sinkprefix;

            /* helpers retrieving inputs and outputs respectively */
            lerr_t  read_sources_( ldndc::project_file_reader_t *);
            lerr_t  read_source_infos_( ldndc::project_file_reader_t *);
            lerr_t  resolve_source_persistence_types_();
            lerr_t  resolve_source_names_();
            lerr_t  read_sinks_( ldndc::project_file_reader_t *);
            lerr_t  read_sink_infos_( ldndc::project_file_reader_t *);
            lerr_t  resolve_sink_persistence_types_();
            lerr_t  resolve_sink_names_();


            /* helper to reset internal object state */
            void  m_reset();
    };
}  /*  namespace ldndc  */

namespace cbm {
    typedef  ldndc::project_file_t  project_file_t; }

#endif  /*  !LDNDC_PROJECTFILE_H_  */

