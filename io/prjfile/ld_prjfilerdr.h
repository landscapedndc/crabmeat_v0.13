/*!
 * @brief
 *    reader for the LandscapeDNDC top-level project file
 *
 * @author
 *    steffen klatt (created on: apr 25, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_PROJECTFILE_READER_H_
#define  LDNDC_PROJECTFILE_READER_H_

#include  "crabmeat-common.h"
#include  "string/cbm_string.h"

#include  <tinyxml2.h>

namespace  ldndc
{
struct  source_info_t;
struct  source_attributes_t;
struct  sink_info_t;
struct  package_require_t;
struct  project_info_t;
class  project_file_reader_t  :  public  ldndc::object_t
{
    LDNDC_OBJECT(project_file_reader_t)
    enum   project_file_format_e
    {
        FORMAT_PROJECT_FILE,
        FORMAT_PROJECT_SETUP_SINGLE,
        FORMAT_PROJECT_SETUP_MULTI,

        FORMAT_PROJECT_UNKNOWN
    };
    static char const *  ROOT_TAG;
    public:
        project_file_reader_t();
        project_file_reader_t( lid_t const &  /*project id*/);
        ~project_file_reader_t();

        lerr_t  open( char const *  /*file name*/);
        void  close();

        /*!
         * @brief
         *      read package requirements for simulation
         *      project
         */
        lerr_t  read_package_require( package_require_t *);

        /*!
         * @brief
         *      read simulation meta information
         */
        lerr_t  read_projectinfo( project_info_t *);

        /*!
         * @brief
         *      read schedule
         */
        lerr_t  read_schedule( cbm::string_t *);


        /** inputs **/

        /*!
         * @brief
         *    read input stream source descriptions. this
         *    function needs to be called until it returns
         *    no longer LDNDC_ERR_OK which indicates that
         *    all stream definitions have been read or an
         *    error occured.
         *
         *    it return LDNDC_ERR_OK if item was successfully
         *    read, LDNDC_ERR_FAIL, LDNDC_ERR_STOP_ITERATION
         *    otherwise.
         *
         *    to reset close (i.e. call @fn close()) and
         *    reopen. note that this resets both source and
         *    attribute handler.
         */
        lerr_t  read_sources( source_info_t *  /*buffer*/);

        /*!
         * @brief
         *    retrieve stream source prefix for inputs. this
         *    prefix is prepended to source value for each
         *    stream and appended to the input base path given
         *    in configuration file.
         */
        lerr_t  read_global_source_prefix( cbm::string_t *);

        /*!
         * @brief
         *    read input stream attributes. use as function @fn
         *    read_sources() above.
         */
        lerr_t  read_source_attributes(
            source_attributes_t *  /*buffer*/);
        /*!
         * @brief
         *    global attribute value: object ID
         */
        lid_t const &  read_default_id();
        /*!
         * @brief
         *    global attribute value: flags
         */
        lflags_t const &  read_default_flags();


        /** outputs **/

        /*!
         * @brief
         *    retrieve stream sink prefix for outputs. this
         *    prefix is prepended to sink value for each
         *    stream and appended to the output base path given
         *    in configuration file.
         */
        lerr_t  read_global_sink_prefix( cbm::string_t *);

        lerr_t  read_sinks( sink_info_t *  /*buffer*/);

    private:
        /* xml handle to project file */
        tinyxml2::XMLDocument *  m_projectfile;
        /* mode */
        project_file_format_e  m_mode;

        /** inputs **/

        /* stream attributes handle */
        tinyxml2::XMLElement *  sattr_base_handle_;
        tinyxml2::XMLElement *  sattr_handle_;

        /* stream sources handle */
        tinyxml2::XMLElement *  source_base_handle_;
        tinyxml2::XMLElement *  source_handle_;

        /* copy stream source information */
        lerr_t  set_source_info_( source_info_t *,
                tinyxml2::XMLElement const * /*xml node*/);
        lerr_t  set_attribute_info_( source_attributes_t *,
                tinyxml2::XMLElement const * /*xml node*/);

        std::string  m_sourceprefix;
        lid_t  m_defaultid, m_idzero;
        lflags_t  m_defaultflags;

        lerr_t  check_stream_attribute_flag_(
                tinyxml2::XMLElement const * /*xml node*/,
                char const * /*flag name*/,
                lflags_t * = NULL /*merge into flags*/);

        /** outputs **/

        /* stream sink handle */
        tinyxml2::XMLElement *  sink_base_handle_;
        tinyxml2::XMLElement *  sink_handle_;

        lerr_t  set_sink_info_( sink_info_t *,
                tinyxml2::XMLElement const * /*xml node*/);

        std::string  m_sinkprefix;

        /** helpers **/
        lerr_t  resolve_receiver_type_(
                char * /*result*/,
                tinyxml2::XMLElement const * /*xml node*/,
                char /*default*/);
        lerr_t  m_resolve_io_format( cbm::string_t * /*result*/,
                tinyxml2::XMLElement const * /*xml node*/,
                cbm::string_t const & /*name prefix*/, cbm::string_t const & /*name*/);

        lerr_t  m_cleanup( lerr_t);
        void  m_reset();
    };
}  /*  namespace ldndc  */


#endif  /*  !LDNDC_PROJECTFILE_READER_H_  */

