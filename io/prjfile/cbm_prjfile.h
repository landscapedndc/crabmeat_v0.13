/*!
 * @brief
 *    interface to the LandscapeDNDC top-level project file
 *
 * @author
 *    steffen klatt (created on: apr 21, 2013),
 *    edwin haas
 */

#ifndef  CBM_PRJFILE_H_
#define  CBM_PRJFILE_H_

#include  "crabmeat-common.h"
#include  "prjfile/ld_prjfile.h"

#endif  /*  !CBM_PRJFILE_H_  */

