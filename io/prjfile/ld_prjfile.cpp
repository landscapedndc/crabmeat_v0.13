/*!
 * @brief
 *    interface to the LandscapeDNDC top-level project file (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 21, 2013),
 *    edwin haas
 */

#include  "prjfile/ld_prjfile.h"
#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

#define  default_srcattr std::string(":DEFAULT:")

LDNDC_OBJECT_DEFN(ldndc::project_file_t)
ldndc::project_file_t::project_file_t()
        : ldndc::object_t()
{
    this->m_reset();
}
ldndc::project_file_t::project_file_t(
        lid_t const &  _id)
        : ldndc::object_t( _id)
{
    this->m_reset();
}

ldndc::project_file_t::~project_file_t()
{
}

lerr_t
ldndc::project_file_t::package_require(
        ldndc::package_require_t *  _pkg_require)
const
{
    crabmeat_assert( _pkg_require);

    if ( this->m_read == LDNDC_ERR_OK)
    {
        *_pkg_require = this->m_packagerequire;
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}

lerr_t
ldndc::project_file_t::project_info(
        ldndc::project_info_t *  _info)
const
{
    crabmeat_assert( _info);

    if ( this->m_read == LDNDC_ERR_OK)
    {
        *_info = this->m_projectinfo;
        return  LDNDC_ERR_OK;
    }
    return  LDNDC_ERR_FAIL;
}


#include  "prjfile/ld_prjfilerdr.h"
lerr_t
ldndc::project_file_t::read(
        char const *  _project_filename)
{
    if ( !_project_filename || cbm::is_empty( _project_filename))
    {
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }

    /* allow rereading file ... ?! */
    if ( cbm::is_equal( this->m_filename.c_str(), _project_filename))
    {
        return  this->m_read;
    }

    this->m_reset();
    this->m_filename = _project_filename;
    this->m_read = LDNDC_ERR_FAIL;

    ldndc::project_file_reader_t *  rdr =
            project_file_reader_t::new_instance( this->object_id());
    if ( !rdr)
        { return  LDNDC_ERR_OBJECT_CREATE_FAILED; }

    lerr_t  rc_open = rdr->open( _project_filename);
    if ( rc_open)
    {
        rdr->delete_instance();
        return  rc_open;
    }

    /* obtain package requirements information */
    lerr_t  rc_read_pkgrequire =
        rdr->read_package_require( &this->m_packagerequire);
    if ( rc_read_pkgrequire)
    {
        rdr->delete_instance();
        return  rc_read_pkgrequire;
    }

    /* obtain project meta information */
    lerr_t  rc_read_info
        = rdr->read_projectinfo( &this->m_projectinfo);
    if ( rc_read_info)
    {
        rdr->delete_instance();
        return  rc_read_info;
    }

    /* obtain schedule (tolerate missing schedule) */
    lerr_t  rc_read_schedule = rdr->read_schedule( &this->m_schedule);
    if (( rc_read_schedule != LDNDC_ERR_OK)
            && ( rc_read_schedule != LDNDC_ERR_ATTRIBUTE_NOT_FOUND))
    {
        rdr->delete_instance();
        return  rc_read_schedule;
    }

    /* obtain sinks */
    lerr_t  rc_read_sinks = this->read_sinks_( rdr);
    if ( rc_read_sinks)
    {
        rdr->delete_instance();
        return  rc_read_sinks;
    }

    /* obtain sources (requires sink declarations to be read) */
    lerr_t  rc_read_sources = this->read_sources_( rdr);
    if ( rc_read_sources)
    {
        rdr->delete_instance();
        return  rc_read_sources;
    }

    rdr->delete_instance();
    this->m_read = LDNDC_ERR_OK;
    return  this->m_read;
}

lerr_t
ldndc::project_file_t::read_sources_(
        ldndc::project_file_reader_t *  _rdr)
{
        /* obtain sources */
    lerr_t  rc_read_info = this->read_source_infos_( _rdr);
    RETURN_IF_NOT_OK(rc_read_info);

    lerr_t  rc_persis = this->resolve_source_persistence_types_();
    RETURN_IF_NOT_OK(rc_persis);

    lerr_t  rc_resolve_names = this->resolve_source_names_();
    RETURN_IF_NOT_OK(rc_resolve_names);

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::project_file_t::read_sinks_(
        ldndc::project_file_reader_t *  _rdr)
{
    lerr_t  rc_read_info = this->read_sink_infos_( _rdr);
    RETURN_IF_NOT_OK(rc_read_info);

    lerr_t  rc_persis = this->resolve_sink_persistence_types_();
    RETURN_IF_NOT_OK(rc_persis);

    lerr_t  rc_resolve_names = this->resolve_sink_names_();
    RETURN_IF_NOT_OK(rc_resolve_names);

    return  LDNDC_ERR_OK;
}

lerr_t
ldndc::project_file_t::read_source_infos_(
        ldndc::project_file_reader_t *  _rdr)
{
    /* obtain stream prefix */
    _rdr->read_global_source_prefix( &this->m_sourceprefix);

    /* obtain stream sources */
    ldndc::source_info_t  ssrc;
    while ( _rdr->read_sources( &ssrc) != LDNDC_ERR_STOP_ITERATION)
    {
        int  source_valid = 1;
        if ( ssrc.type == "")
        {
            source_valid = 0;
            CBM_LogError( "unable to resolve input class of source entity. ignoring entry",
                  "  [identifier=",ssrc.identifier,"]");
        }
        /* check if source identifier already exists */
        source_const_iterator  s = this->sources_begin();
        for ( ; source_valid && ( s != this->sources_end());  ++s)
        {
            if ( cbm::is_equal( ssrc.identifier, s->identifier))
            {
                source_valid = 0;
                CBM_LogError( "duplicate source definition. ignoring entry",
                      "  [identifier=",ssrc.identifier,",class=",s->type,"]");
                break;
            }
        }

        if ( source_valid)
        {
            this->m_sources.push_back( ssrc);
        }
        /* reset */
        ssrc = ldndc::source_info_t();
    }
    /* provide minimal default input: a region definition */
    if ( this->m_sources.size() == 0)
    {
        /* attach default setup input stream (minimal input: kick-starts initialization) */
        ldndc::source_info_default( &ssrc, "setup");
        ssrc.name_prefix = this->m_sourceprefix.str();
        this->m_sources.push_back( ssrc);
    }

        /* obtain stream attributes */
    ldndc::source_attributes_t  sattr;
    while ( _rdr->read_source_attributes( &sattr) == LDNDC_ERR_OK)
    {
        if ( sattr.type != "")
            { this->m_sourceattr[sattr.type.str()] = sattr; }
        else
            { CBM_LogFatal( "[BUG] should not get here"); }
    }
    /* fill information where not provided */
    this->m_sourceattr[default_srcattr] = source_attributes_t();
    this->m_sourceattr[default_srcattr].default_id = _rdr->read_default_id();
    this->m_sourceattr[default_srcattr].flags = _rdr->read_default_flags();

    return  LDNDC_ERR_OK;
}

#include  "string/lstring-compare.h"
lerr_t
ldndc::project_file_t::read_sink_infos_(
        ldndc::project_file_reader_t *  _rdr)
{
    /* obtain stream sink prefix */
    _rdr->read_global_sink_prefix( &this->m_sinkprefix);

    /* obtain sink definitions */
    ldndc::sink_info_t  ssnk;
    lerr_t  rc_read = LDNDC_ERR_OK;
    while (( rc_read = _rdr->read_sinks( &ssnk)) != LDNDC_ERR_STOP_ITERATION)
    {
        if ( rc_read)
        {
            return  rc_read;
        }

        int  sink_exists = 0;
        for ( sink_const_iterator  s = this->sinks_begin();  s != this->sinks_end();  ++s)
        {
            sink_info_t const &  snk = *s;
            if ( cbm::is_equal( snk.identifier, ssnk.identifier) && ( snk.index == ssnk.index))
            {
                sink_exists = 1;

                CBM_LogWarn( "ignoring duplicate sink definition in project file  [sink=",ssnk.identifier,"]");
                break;
            }
        }
        if ( !sink_exists)
        {
            this->m_sinks.push_back( ssnk);
        }
        ssnk = sink_info_t();
    }

    /* add default sinks here to make further handling more convenient */
    size_t  j = 0;
    while ( STANDARD_SINKS[j].identifier)
    {
        int  sink_exists = 0;
        int  i = static_cast< int >( this->m_sinks.size());
        for ( sink_const_iterator  s = this->sinks_begin();  s != this->sinks_end();  ++s, --i)
        {
            /* do not consider newly added default sinks */
            if ( i == 0)
                { break; }

            sink_info_t const &  snk = *s;
            if ( cbm::is_equal( snk.identifier, STANDARD_SINKS[j].identifier) && ( snk.index == 0))
            {
                sink_exists = 1;
                break;
            }
        }
        if ( !sink_exists)
        {
            sink_info_t  snk;
            sink_info_default( STANDARD_SINKS, &snk, NULL, NULL, j);
            if ( !sink_is_black_hole( &snk))
            {
                snk.name_prefix = this->global_sink_prefix();
            }

            this->m_sinks.push_back( snk);
        }
        ++j;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::project_file_t::resolve_source_persistence_types_()
{
    source_container_t::iterator  s = this->m_sources.begin();
    for ( ; s != this->m_sources.end();  ++s)
    {
        if ( cbm::is_empty( s->name))
        {
            continue;
        }

        cbm::string_t  s_name = s->name;

        s->persis = data_source_t::persistence_type_from_source_name( s_name.c_str(), &s->name);
        if ( s->persis == PERSISTENCE_TYPE_INVALID)
        {
            CBM_LogError( "unknown persistence type for source  [identifier=",s->identifier,"]");
            return  LDNDC_ERR_FAIL;
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::project_file_t::resolve_sink_persistence_types_()
{
    /* nothing here */
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::project_file_t::resolve_source_names_()
{
    source_container_t::iterator  src = this->m_sources.begin();
    for ( ; src != this->m_sources.end();  ++src)
    {
        if ( cbm::is_empty( src->name))
            { continue; }

        if ( src->persis == PERSISTENCE_TYPE_FILE)
        {
            /* no op */
        }
        else if ( src->persis == PERSISTENCE_TYPE_MUTABLE_FILE)
        {
            /* check if there exists a sink identifier matching source's name */
            int  have_sink = 0;
            sink_container_t::iterator  snk = this->m_sinks.begin();
            for ( ; snk != this->m_sinks.end();  ++snk)
            {
                if ( cbm::is_equal( src->name, snk->identifier))
                {
                    src->name = snk->name;
                    src->name_prefix = snk->name_prefix;
                    src->format = snk->format;
                    have_sink = 1;
                    break;
                }
            }

            /* so far there are no other data producers */

            if ( !have_sink)
            {
                CBM_LogError( "unable to resolve source name  [identifier=",src->identifier,"]");
                return  LDNDC_ERR_FAIL;
            }
        }
    }

    return  LDNDC_ERR_OK;
}
lerr_t
ldndc::project_file_t::resolve_sink_names_()
{
    /* nothing here */
    return  LDNDC_ERR_OK;
}


char const *
ldndc::project_file_t::name()
const
{
    return  this->m_filename.c_str();
}


lerr_t
ldndc::project_file_t::status()
const
{
    return  this->m_read;
}


void
ldndc::project_file_t::dump()
const
{
}


char const *
ldndc::project_file_t::schedule()
const
{
    if ( this->m_read == LDNDC_ERR_OK)
    { 
        return  this->m_schedule.c_str();
    }
    return  NULL;
}


char const *
ldndc::project_file_t::global_source_prefix()
const
{
    return  this->m_sourceprefix.c_str();
}
char const *
ldndc::project_file_t::global_sink_prefix()
const
{
    return  this->m_sinkprefix.c_str();
}


ldndc::source_attributes_t const *
ldndc::project_file_t::stream_attributes(
        cbm::string_t const &  _ic_type)
const
{
    if ( this->m_sourceattr.find( _ic_type.str())
            == this->m_sourceattr.end())
        { return  &this->m_sourceattr.find( default_srcattr)->second; }
    return  &this->m_sourceattr.find( _ic_type.str())->second;
}
lid_t
ldndc::project_file_t::default_id(
        cbm::string_t const &  _ic_type)
const
{
    if ( this->m_sourceattr.find( _ic_type.str())
                == this->m_sourceattr.end())
        { return  this->m_sourceattr.find( default_srcattr)->second.default_id; }
    return  this->m_sourceattr.find( _ic_type.str())->second.default_id;
}


void
ldndc::project_file_t::m_reset()
{
    this->m_read = LDNDC_ERR_NONE;

    this->m_filename.clear();
    this->m_schedule.clear();

    this->m_sourceattr.clear();
    this->m_sources.clear();
    this->m_sourceprefix.clear();


    /* OUTPUT */
    this->m_sinks.clear();
    this->m_sinkprefix.clear();
}

