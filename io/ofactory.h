/*!
 * @brief
 *    output processors factory
 *
 * @author
 *    steffen klatt (created on: nov 19, 2013)
 */

#ifndef  LDNDC_IO_OPROCFACTORY_H_
#define  LDNDC_IO_OPROCFACTORY_H_

#include  "crabmeat-common.h"

namespace ldndc {
struct  oproc_landscape_receiver_t;
struct  oproc_receiver_t;

struct  sink_info_t;

/*!
 * @brief
 *    output processors object factory
 */
struct  CBM_API  oproc_factories_t
{
    /*!
     * @brief
     *    check if appropriate landscape output writer
     *    is available.
     */
    bool  landscape_receiver_available(
            sink_info_t const *) const;
    /*!
     * @brief
     *    construct landscape output writer for given 
     *    i/o format
     */
    oproc_landscape_receiver_t *  construct_landscape_receiver(
            sink_info_t const *) const;
    /*!
     * @brief
     *    destroy landscape reader
     */
    void  destroy_landscape_receiver(
            oproc_landscape_receiver_t *) const;
};
}


#endif  /*  !LDNDC_IO_OPROCFACTORY_H_  */

