/*!
 * @brief
 *    data modifier base (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#include  "io/data-modifier/modifier.h"

ldndc::iproc_modifier_t::iproc_modifier_t()
        : iproc_provider_t()
{
}
ldndc::iproc_modifier_t::~iproc_modifier_t()
{
}

