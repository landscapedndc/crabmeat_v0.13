/*!
 * @brief
 *    data modifier base
 *
 * @author
 *    steffen klatt (created on: nov 16, 2013)
 *
 * FIXME
 *    make modifiers data provider
 */

#ifndef  LDNDC_IO_DATAMODIFIER_H_
#define  LDNDC_IO_DATAMODIFIER_H_

#include  "crabmeat-common.h"
#include  "io/data-provider/provider.h"

namespace ldndc {
struct  CBM_API  iproc_modifier_t  :  iproc_provider_t
{
    iproc_modifier_t();
    virtual  ~iproc_modifier_t() = 0;
};
}

#endif  /*  !LDNDC_IO_DATAMODIFIER_H_  */

