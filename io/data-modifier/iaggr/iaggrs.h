/*!
 * @brief
 *    data aggregating factory array
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#ifndef  LDNDC_IO_IAGGREGATORS_H_
#define  LDNDC_IO_IAGGREGATORS_H_

#include  "io/data-modifier/iaggr/iaggr.h"

namespace ldndc {
    extern CBM_API ldndc::ioproc_factory_base_t const *
        find_iaggregator_factory( char const * /*role/class*/);
}


#endif  /*  !LDNDC_IO_IAGGREGATORS_H_ */

