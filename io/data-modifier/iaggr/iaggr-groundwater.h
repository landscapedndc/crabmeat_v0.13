/*!
 * @brief
 *    data aggregation layer for core readers (groundwater)
 *
 * @author
 *    steffen klatt (created on: oct 19, 2015)
 */

#ifndef  LDNDC_IO_IAGGREGATOR_GROUNDWATER_H_
#define  LDNDC_IO_IAGGREGATOR_GROUNDWATER_H_

#include  "io/data-modifier/iaggr/iaggr.h"
#include  "io/iif/iif_groundwater.h"

#include  "input/groundwater/groundwatertypes-srv.h"
#include  "datafilters/aggregator.h"

namespace ldndc {
class  CBM_API  iproc_iaggregator_groundwater_t  :  public  iproc_iaggregator_base_t,  public  iproc_interface_groundwater_t
{
    LDNDC_SERVER_OBJECT(iproc_iaggregator_groundwater_t)
    typedef  groundwater::record::item_type  element_type;
    public:
        char const *  input_class_type() const
            { return  iproc_interface_groundwater_t::input_class_type(); }
        enum
            { IS_IMPLEMENTED = 1 };
    public:
        iproc_iaggregator_groundwater_t();
        virtual  ~iproc_iaggregator_groundwater_t();

        iproc_reader_base_t *  to_reader();
        void  set_provider(
                iproc_provider_t *);

        int  data_available() const;

    public:
        /* info getters here */
        double  get_average_no3() const;
        double  get_average_watertable() const;

    public:
        lerr_t  preset( char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read( element_type [], size_t, size_t,
                char const * [], size_t,
                int, int, int *, int *);

    private:
        iproc_interface_groundwater_t *  rdr_;
    private:
        data_aggregate_t< groundwater::record::item_type > *  aggr_;
        int  data_res_;
};
}


#endif  /*  !LDNDC_IO_IAGGREGATOR_GROUNDWATER_H_  */

