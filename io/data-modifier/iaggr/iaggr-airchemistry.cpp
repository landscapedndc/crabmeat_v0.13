
#ifndef  LDNDC_IO_IAGGREGATOR_AIRCHEMISTRY_CPP_
#define  LDNDC_IO_IAGGREGATOR_AIRCHEMISTRY_CPP_

/*  *****  AIR CHEMISTRY CORE DATA AGGREGATOR  *****  */
#include  "io/data-modifier/iaggr/iaggr-airchemistry.h"
#include  "memory/cbm_mem.h"

namespace ldndc {
    LDNDC_SERVER_OBJECT_DEFN(iproc_iaggregator_airchemistry_t)
    iproc_iaggregator_airchemistry_t::iproc_iaggregator_airchemistry_t()
    : iproc_iaggregator_base_t(),
    iproc_interface_airchemistry_t(),
    rdr_( NULL),
    aggr_( NULL),
    data_res_( 0)
    {
    }
    iproc_iaggregator_airchemistry_t::~iproc_iaggregator_airchemistry_t()
    {
        if ( this->rdr_)
        {
            dynamic_cast< iproc_base_t * >( this->rdr_)->delete_instance();
            this->rdr_ = NULL;
        }
        if ( this->aggr_)
        {
            CBM_DefaultAllocator->destroy( this->aggr_);
            this->aggr_ = NULL;
        }
    }

    iproc_reader_base_t *
    iproc_iaggregator_airchemistry_t::to_reader()
    {
        return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
    }
    void
    iproc_iaggregator_airchemistry_t::set_provider(
                                                   iproc_provider_t *  _rdr)
    {
        crabmeat_assert( _rdr);
        this->rdr_ = dynamic_cast< iproc_interface_airchemistry_t * >( _rdr);
    }

    int
    iproc_iaggregator_airchemistry_t::data_available()
    const
    {
        return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->data_available() : 0;
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_ch4()
    const
    {
        return  this->rdr_->get_average_ch4();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_co2()
    const
    {
        return  this->rdr_->get_average_co2();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_nh3()
    const
    {
        return  this->rdr_->get_average_nh3();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_nh4()
    const
    {
        return  this->rdr_->get_average_nh4();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_nh4dry()
    const
    {
        return  this->rdr_->get_average_nh4dry();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_no()
    const
    {
        return  this->rdr_->get_average_no();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_no2()
    const
    {
        return  this->rdr_->get_average_no2();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_no3()
    const
    {
        return  this->rdr_->get_average_no3();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_no3dry()
    const
    {
        return  this->rdr_->get_average_no3dry();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_o2()
    const
    {
        return  this->rdr_->get_average_o2();
    }

    double
    iproc_iaggregator_airchemistry_t::get_average_o3()
    const
    {
        return  this->rdr_->get_average_o3();
    }

    lerr_t
    iproc_iaggregator_airchemistry_t::preset(
                                             char const *  _rec_item[], size_t  _rec_item_cnt, config_type *  _config)
    {
        lerr_t  rc_preset = this->rdr_->preset( _rec_item, _rec_item_cnt, _config);
        if ( rc_preset)
        {
            return  rc_preset;
        }

        crabmeat_assert( _config);
        this->data_res_ = _config->streamdata_time.time_resolution();
        this->aggr_ = CBM_DefaultAllocator->construct_args< data_aggregate_t< element_type > >(
                                                                                                 static_cast< size_t >( 1), airchemistry::AIRCHEMISTRY_DATAFILTER_LIST, airchemistry::record::RECORD_SIZE);
        if ( !this->aggr_)
        {
            CBM_LogError( "failed to allocate data aggregator object");
            return  LDNDC_ERR_FAIL;
        }
        
        return  LDNDC_ERR_OK;
    }
    
    lerr_t
    iproc_iaggregator_airchemistry_t::reset()
    {
        return  this->rdr_->reset();
    }
    
}

#define  __class__  airchemistry
#define  __modifier__  iproc_iaggregator_airchemistry_t
#define  __streamdata_info__  airchemistry::airchemistry_streamdata_info_t
#include  "io/data-modifier/iaggr/shared/iaggr-streamdata-read.cpp"
#undef  __streamdata_info__
#undef  __modifier__
#undef  __class__


#endif  /*  !LDNDC_IO_IAGGREGATOR_AIRCHEMISTRY_CPP_  */

