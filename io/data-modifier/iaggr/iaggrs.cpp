/*!
 * @brief
 *    data aggregator factory (implementation)
 *
 * @author
 *    steffen klatt (created on: nov 18, 2013)
 */

#include  "io/data-modifier/iaggr/iaggrs.h"

#include  "io/data-modifier/iaggr/iaggr-airchemistry.h"
static ldndc::ioproc_factory_t< ldndc::iproc_iaggregator_airchemistry_t >  iaggregator_airchemistry_factory;
#include  "io/data-modifier/iaggr/iaggr-climate.h"
static ldndc::ioproc_factory_t< ldndc::iproc_iaggregator_climate_t >  iaggregator_climate_factory;
#include  "io/data-modifier/iaggr/iaggr-groundwater.h"
static ldndc::ioproc_factory_t< ldndc::iproc_iaggregator_groundwater_t >  iaggregator_groundwater_factory;

struct  iaggregator_factory_item_t
{
    ldndc::ioproc_factory_base_t const *  factory;
    char const *  role;
};

static iaggregator_factory_item_t const  __cbm_iaggregators_factories[] =
{
    { &iaggregator_airchemistry_factory, "airchemistry" },
    { &iaggregator_climate_factory, "climate" },
    { &iaggregator_groundwater_factory, "groundwater" },

    { NULL, NULL} /* sentinel */
};

ldndc::ioproc_factory_base_t const *  ldndc::find_iaggregator_factory(
                char const *  _role)
{
    if ( !_role)
        { return  NULL; }

    int  r = 0;
    while ( __cbm_iaggregators_factories[r].factory)
    {
        if ( cbm::is_equal( __cbm_iaggregators_factories[r].role, _role))
            { return  __cbm_iaggregators_factories[r].factory; }
        ++r;
    }
    return  NULL;
}

