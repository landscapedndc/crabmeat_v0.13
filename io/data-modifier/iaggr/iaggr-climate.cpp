
#ifndef  LDNDC_IO_IAGGREGATOR_CLIMATE_CPP_
#define  LDNDC_IO_IAGGREGATOR_CLIMATE_CPP_

/***  CLIMATE CORE DATA AGGREGATOR ***/
#include  "io/data-modifier/iaggr/iaggr-climate.h"
#include  "memory/cbm_mem.h"

namespace ldndc {
LDNDC_SERVER_OBJECT_DEFN(iproc_iaggregator_climate_t)
iproc_iaggregator_climate_t::iproc_iaggregator_climate_t()
        : iproc_iaggregator_base_t(),
          iproc_interface_climate_t(),
          rdr_( NULL),
          aggr_( NULL),
          data_res_( 0)
{
}
iproc_iaggregator_climate_t::~iproc_iaggregator_climate_t()
{
    if ( this->rdr_)
    {
        dynamic_cast< iproc_base_t * >( this->rdr_)->delete_instance();
        this->rdr_ = NULL;
    }
    if ( this->aggr_)
    {
        CBM_DefaultAllocator->destroy( this->aggr_);
        this->aggr_ = NULL;
    }
}

iproc_reader_base_t *
iproc_iaggregator_climate_t::to_reader()
{
    return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
}
void
iproc_iaggregator_climate_t::set_provider(
        iproc_provider_t *  _rdr)
{
    crabmeat_assert( _rdr);
    this->rdr_ = dynamic_cast< iproc_interface_climate_t * >( _rdr);
}

int
iproc_iaggregator_climate_t::data_available()
const
{
    return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->data_available() : 0;
}

double
iproc_iaggregator_climate_t::get_latitude()
const
{
    return  this->rdr_->get_latitude();
}

double
iproc_iaggregator_climate_t::get_longitude()
const
{
    return  this->rdr_->get_longitude();
}

double
iproc_iaggregator_climate_t::get_elevation()
const
{
    return  this->rdr_->get_elevation();
}

double
iproc_iaggregator_climate_t::get_cloudiness()
const
{
    return  this->rdr_->get_cloudiness();

}
double
iproc_iaggregator_climate_t::get_rainfall_intensity()
const
{
    return  this->rdr_->get_rainfall_intensity();
}
double
iproc_iaggregator_climate_t::get_wind_speed()
const
{
    return  this->rdr_->get_wind_speed();
}

double
iproc_iaggregator_climate_t::get_annual_precipitation()
const
{
    return  this->rdr_->get_annual_precipitation();
}
double
iproc_iaggregator_climate_t::get_temperature_average()
const
{
    return  this->rdr_->get_temperature_average();
}
double
iproc_iaggregator_climate_t::get_temperature_amplitude()
const
{
    return  this->rdr_->get_temperature_amplitude();
}


lerr_t
iproc_iaggregator_climate_t::preset(
        char const *  _rec_item[], size_t  _rec_item_cnt, config_type *  _config)
{
    lerr_t  rc_preset = this->rdr_->preset( _rec_item, _rec_item_cnt, _config);
    if ( rc_preset)
    {
        return  rc_preset;
    }

    crabmeat_assert( _config);
    this->data_res_ = _config->streamdata_time.time_resolution();
    this->aggr_ = CBM_DefaultAllocator->construct_args< data_aggregate_t< element_type > >(
                   static_cast< size_t >( 1), climate::CLIMATE_DATAFILTER_LIST, climate::record::RECORD_SIZE);
    if ( !this->aggr_)
    {
        CBM_LogError( "failed to allocate data aggregator object");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}

lerr_t
iproc_iaggregator_climate_t::reset()
{
    return  this->rdr_->reset();
}

}

#define  __class__  climate
#define  __modifier__  iproc_iaggregator_climate_t
#define  __streamdata_info__  climate::climate_streamdata_info_t
#include  "io/data-modifier/iaggr/shared/iaggr-streamdata-read.cpp"
#undef  __streamdata_info__
#undef  __modifier__
#undef  __class__


#endif  /*  !LDNDC_IO_IAGGREGATOR_CLIMATE_CPP_  */

