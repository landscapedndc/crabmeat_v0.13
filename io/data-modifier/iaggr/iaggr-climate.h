/*!
 * @brief
 *    data aggregation layer for core readers (climate)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#ifndef  LDNDC_IO_IAGGREGATOR_CLIMATE_H_
#define  LDNDC_IO_IAGGREGATOR_CLIMATE_H_

#include  "io/data-modifier/iaggr/iaggr.h"
#include  "io/iif/iif_climate.h"

#include  "input/climate/climatetypes-srv.h"
#include  "datafilters/aggregator.h"

namespace ldndc {
class  CBM_API  iproc_iaggregator_climate_t  :  public  iproc_iaggregator_base_t,  public  iproc_interface_climate_t
{
    LDNDC_SERVER_OBJECT(iproc_iaggregator_climate_t)
    typedef  climate::record::item_type  element_type;
    public:
        char const *  input_class_type() const
            { return  iproc_interface_climate_t::input_class_type(); }
        enum
            { IS_IMPLEMENTED = 1 };
    public:
        iproc_iaggregator_climate_t();
        virtual  ~iproc_iaggregator_climate_t();

        iproc_reader_base_t *  to_reader();
        void  set_provider(
                iproc_provider_t *);

        int  data_available() const;

    public:
        double  get_latitude() const;
        double  get_longitude() const;
        double  get_elevation() const;

        double  get_cloudiness() const;
        double  get_rainfall_intensity() const;
        double  get_wind_speed() const;

        double  get_annual_precipitation() const;
        double  get_temperature_average() const;
        double  get_temperature_amplitude() const;

    public:
        lerr_t  preset( char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read( element_type [], size_t, size_t,
                char const * [], size_t,
                int, int, int *, int *);

    private:
        iproc_interface_climate_t *  rdr_;
    private:
        data_aggregate_t< climate::record::item_type > *  aggr_;
        int  data_res_;
};
}


#endif  /*  !LDNDC_IO_IAGGREGATOR_CLIMATE_H_  */

