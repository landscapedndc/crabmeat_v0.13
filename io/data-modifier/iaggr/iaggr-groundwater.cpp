
#ifndef  LDNDC_IO_IAGGREGATOR_GROUNDWATER_CPP_
#define  LDNDC_IO_IAGGREGATOR_GROUNDWATER_CPP_

/*  *****  AIR CHEMISTRY CORE DATA AGGREGATOR  *****  */
#include  "io/data-modifier/iaggr/iaggr-groundwater.h"
#include  "memory/cbm_mem.h"

namespace ldndc {
    LDNDC_SERVER_OBJECT_DEFN(iproc_iaggregator_groundwater_t)
    iproc_iaggregator_groundwater_t::iproc_iaggregator_groundwater_t()
    : iproc_iaggregator_base_t(),
    iproc_interface_groundwater_t(),
    rdr_( NULL),
    aggr_( NULL),
    data_res_( 0)
    {
    }
    iproc_iaggregator_groundwater_t::~iproc_iaggregator_groundwater_t()
    {
        if ( this->rdr_)
        {
            dynamic_cast< iproc_base_t * >( this->rdr_)->delete_instance();
            this->rdr_ = NULL;
        }
        if ( this->aggr_)
        {
            CBM_DefaultAllocator->destroy( this->aggr_);
            this->aggr_ = NULL;
        }
    }

    iproc_reader_base_t *
    iproc_iaggregator_groundwater_t::to_reader()
    {
        return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->to_reader() : NULL;
    }
    void
    iproc_iaggregator_groundwater_t::set_provider(
            iproc_provider_t *  _rdr)
    {
        crabmeat_assert( _rdr);
        this->rdr_ = dynamic_cast< iproc_interface_groundwater_t * >( _rdr);
    }

    int
    iproc_iaggregator_groundwater_t::data_available()
    const
    {
        return  ( this->rdr_) ? dynamic_cast< iproc_provider_t * >( this->rdr_)->data_available() : 0;
    }

    double
    iproc_iaggregator_groundwater_t::get_average_no3()
    const
    {
        return  this->rdr_->get_average_no3();
    }

    double
    iproc_iaggregator_groundwater_t::get_average_watertable()
    const
    {
        return  this->rdr_->get_average_watertable();
    }

    lerr_t
    iproc_iaggregator_groundwater_t::preset(
                                             char const *  _rec_item[], size_t  _rec_item_cnt, config_type *  _config)
    {
        lerr_t  rc_preset = this->rdr_->preset( _rec_item, _rec_item_cnt, _config);
        if ( rc_preset)
        {
            return  rc_preset;
        }

        crabmeat_assert( _config);
        this->data_res_ = _config->streamdata_time.time_resolution();
        this->aggr_ = CBM_DefaultAllocator->construct_args< data_aggregate_t< element_type > >(
                static_cast< size_t >( 1), groundwater::GROUNDWATER_DATAFILTER_LIST, groundwater::record::RECORD_SIZE);
        if ( !this->aggr_)
        {
            CBM_LogError( "failed to allocate data aggregator object");
            return  LDNDC_ERR_FAIL;
        }
        
        return  LDNDC_ERR_OK;
    }
    
    lerr_t
    iproc_iaggregator_groundwater_t::reset()
    {
        return  this->rdr_->reset();
    }
    
}

#define  __class__  groundwater
#define  __modifier__  iproc_iaggregator_groundwater_t
#define  __streamdata_info__  groundwater::groundwater_streamdata_info_t
#include  "io/data-modifier/iaggr/shared/iaggr-streamdata-read.cpp"
#undef  __streamdata_info__
#undef  __modifier__
#undef  __class__


#endif  /*  !LDNDC_IO_IAGGREGATOR_GROUNDWATER_CPP_  */

