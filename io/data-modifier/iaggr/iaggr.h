/*!
 * @brief
 *    data aggregating layer for readers (base type)
 *
 * @author
 *    steffen klatt (created on: aug 14, 2013)
 */

#ifndef  LDNDC_IO_IAGGREGATOR_H_
#define  LDNDC_IO_IAGGREGATOR_H_

#include  "io/data-modifier/modifier.h"

namespace ldndc {
class  CBM_API  iproc_iaggregator_base_t  :  public  iproc_modifier_t
{
    public:
        virtual  char const *  input_class_type() const = 0;
    public:
        virtual  void  set_provider( iproc_provider_t *) = 0;

    protected:
        iproc_iaggregator_base_t();
        virtual  ~iproc_iaggregator_base_t() = 0;
};
}

#endif  /*  !LDNDC_IO_IAGGREGATOR_H_ */

