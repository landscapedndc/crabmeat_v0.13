
#include  "utils/lutils.h"
#include  "memory/cbm_mem.h"

#include  "containers/linkarray.h"
namespace ldndc {
lerr_t
__modifier__::read(
        element_type  _rec[], size_t  _rec_cnt, size_t  _rec_size,
                char const *  _rec_item[], size_t  _rec_item_cnt,
        int  _offset, int  _stride,
                int *  _r, int * _s)
{
    ldndc::linkarray_operations_t< __streamdata_info__ >  rec_ops( _rec_size);
    ldndc::linkarray_operations_t< __streamdata_info__ >::buffer_header_t  r_hdr;
    rec_ops.read_buffer_header( &r_hdr, _rec);

    crabmeat_assert( r_hdr.hx.res);
    if ( r_hdr.hx.res < -1)
    {
        CBM_LogError( "sorry, we are currently not able to handle ", STRINGIFY(__class__), " input data with supdaily resolution .. ?!?");
        return  LDNDC_ERR_REQUEST_MISMATCH;
    }
    int  stride = this->data_res_ / (( r_hdr.hx.res < 0) ? -r_hdr.hx.res : r_hdr.hx.res);
// sk:dbg    CBM_LogDebug( "stride=",stride, "  data-resolution=",data_res_, "  buffer-resolution=",r_hdr.hx.res);

    if ( stride == 1)
    {
        /* pass through */
        return  this->rdr_->read( _rec, _rec_cnt, _rec_size, _rec_item, _rec_item_cnt, _offset, _stride, _r, _s);
    }
    else if ( stride > 1)
    {
        /* aggregate */

        ldndc::linkarray_operations_t< __streamdata_info__ >  buf_ops( _rec_item_cnt);
        size_t  buf_capacity( stride * _rec_cnt);
        element_type *  buf = buf_ops.allocate( buf_capacity);
        if ( !buf)
        {
            return  LDNDC_ERR_FAIL;
        }
        buf_ops.mem_set( buf, buf_capacity, ldndc::invalid_t< element_type >::value);

        int  r = 0;
        lerr_t  rc_read = this->rdr_->read( buf, stride*_rec_cnt, _rec_item_cnt, _rec_item, _rec_item_cnt, _offset, _stride, &r, _s);
        if ( rc_read)
        {
        }
        else if ( r >= stride)
        {
// sk:dbg            CBM_LogDebug( "filter ",r, " items");
            lerr_t  rc_aggr = this->aggr_->filter( r/stride, _rec, _rec_size, buf, _rec_item_cnt, stride);
            if ( rc_aggr)
            {
                CBM_LogError( "aggregating values failed");
                rc_read = rc_aggr;
            }
        }
        buf_ops.deallocate( buf);
        if ( _r)
        {
            /* ignore trailing incomplete set of data records */
            *_r = r/stride;
        }
        return  rc_read;
    }
    else if ( stride < 0)
    {
        int  r = 0;
        int  s = -stride;
        lerr_t  rc_read = this->rdr_->read( _rec, _rec_cnt/s, s*_rec_size, _rec_item, _rec_item_cnt, _offset, _stride, &r, _s);
        if ( rc_read)
        {
        }
        else if ( r > 0)
        {
        }
        if ( _r)
        {
            *_r = r * s;
        }
        return  rc_read;
    }
    else
    {
        CBM_LogFatal( "[BUG]  you must be kidding me ...");
    }

    return  LDNDC_ERR_FAIL;
}
}

