/*!
 * @brief
 *    data aggregation layer for core readers (air chemistry)
 *
 * @author
 *    steffen klatt (created on: jul 10, 2013)
 */

#ifndef  LDNDC_IO_IAGGREGATOR_AIRCHEMISTRY_H_
#define  LDNDC_IO_IAGGREGATOR_AIRCHEMISTRY_H_

#include  "io/data-modifier/iaggr/iaggr.h"
#include  "io/iif/iif_airchemistry.h"

#include  "input/airchemistry/airchemistrytypes-srv.h"
#include  "datafilters/aggregator.h"

namespace ldndc {
class  CBM_API  iproc_iaggregator_airchemistry_t  :  public  iproc_iaggregator_base_t,  public  iproc_interface_airchemistry_t
{
    LDNDC_SERVER_OBJECT(iproc_iaggregator_airchemistry_t)
    typedef  airchemistry::record::item_type  element_type;
    public:
        char const *  input_class_type() const
            { return  iproc_interface_airchemistry_t::input_class_type(); }
        enum
            { IS_IMPLEMENTED = 1 };
    public:
        iproc_iaggregator_airchemistry_t();
        virtual  ~iproc_iaggregator_airchemistry_t();

        iproc_reader_base_t *  to_reader();
        void  set_provider(
                iproc_provider_t *);

        int  data_available() const;

    public:
        /* info getters here */
        double  get_average_ch4() const;
        double  get_average_co2() const;
        double  get_average_nh3() const;
        double  get_average_nh4() const;
        double  get_average_nh4dry() const;
        double  get_average_no() const;
        double  get_average_no2() const;
        double  get_average_no3() const;
        double  get_average_no3dry() const;
        double  get_average_o2() const;
        double  get_average_o3() const;

    public:
        lerr_t  preset( char const * [], size_t, config_type *);

        lerr_t  reset();
        lerr_t  read( element_type [], size_t, size_t,
                char const * [], size_t,
                int, int, int *, int *);

    private:
        iproc_interface_airchemistry_t *  rdr_;
    private:
        data_aggregate_t< airchemistry::record::item_type > *  aggr_;
        int  data_res_;
};
}


#endif  /*  !LDNDC_IO_IAGGREGATOR_AIRCHEMISTRY_H_  */

