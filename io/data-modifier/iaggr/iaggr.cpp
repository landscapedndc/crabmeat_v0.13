/*!
 * @brief
 *    data aggregating layer for readers
 *    (implementation)
 *
 * @author
 *    steffen klatt (created on: aug 14, 2013)
 */

#include  "io/data-modifier/iaggr/iaggr.h"

namespace ldndc {
iproc_iaggregator_base_t::iproc_iaggregator_base_t()
        : iproc_modifier_t()
{
}
iproc_iaggregator_base_t::~iproc_iaggregator_base_t()
{
}
}

