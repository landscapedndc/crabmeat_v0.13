/*!
 * @brief
 *    holds stuff that deals with reading input data
 *
 * @author
 *    Steffen Klatt (rewritten on: oct 09, 2011, rewritten on: apr 22, 2013)
 *    Edwin Haas
 */

#include  "io/input.h"
#include  "io/input-source.h"
#include  "io/source-handle-srv.h"
#include  "io/source-info.h"

#include  "input/ic-env.h"

#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

LDNDC_SERVER_OBJECT_DEFN(ldndc::raw_inputs_t)

ldndc::raw_inputs_t::raw_inputs_t()
        : cbm::server_object_t()
{
    this->m_clear();
}

ldndc::raw_inputs_t::~raw_inputs_t()
{
    /* free all input objects held by this object */
    this->unmount_all_sources();
}


lid_t
ldndc::raw_inputs_t::mount_and_open_source(
        source_info_t const *  _src_info)
{
    if ( !_src_info)
        { return  invalid_lid; }
    if ( !cbm_is_aic(_src_info->type.c_str()))
        { return  invalid_lid; }

    input_source_srv_t *  iobj =
        this->mount_and_open_source_( _src_info);
    if ( iobj)
        { return  iobj->object_id(); }
    return  invalid_lid;
}
ldndc::input_source_srv_t *
ldndc::raw_inputs_t::mount_and_open_source_(
        source_info_t const *  _src_info)
{
    CBM_LogInfo( "attaching input source \"",_src_info->full_name(),"\" ...");

    lid_t const  this_source_id = this->available_source_id();
    std::string const  source_key = this->make_source_key(
            _src_info->type.c_str(), this_source_id);

    if ( this->m_sources.find( source_key)
                != this->m_sources.end())
    {
        CBM_LogError( "raw_inputs_t::mount_and_open_source():",
                    " source ID already exists .. unmount source first  [id=",this_source_id,"]");
        return  NULL;
    }

    /* instantiate new input object */
    input_source_srv_t *  iobj =
        input_source_srv_t::new_instance( this_source_id);
    if ( !iobj)
    {
        return  NULL;
    }
    crabmeat_assert( this_source_id == iobj->object_id());

    /* open input source */
    lerr_t  rc_open_source = iobj->attempt_to_open_stream( _src_info);
    if ( rc_open_source)
    {
        iobj->delete_instance();
        return  NULL;
    }

    /* if all is well, add input object to container */
    sources_container_insert_return_type  rc_insert =
        this->m_sources.insert( sources_pair_type( source_key, iobj));
    if ( !rc_insert.second)
    {
        CBM_LogError( "raw_inputs_t::mount_and_open_source():",
                    " failed to add source  [source=",*_src_info,"]");
        iobj->delete_instance();
        return  NULL;
    }

    return  iobj;
}

lerr_t
ldndc::raw_inputs_t::open_mounted_source(
    char const *  _identifier, char const *  _type)
{
    lid_t  s_id = this->find_by_source_identifier( _identifier);
    if ( s_id == invalid_lid)
        { return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST; }

    input_source_srv_t *  iobj = NULL;
    lerr_t  rc_get = this->get_input_source_( &iobj, s_id, _type);
    if ( !iobj || rc_get)
        { return  LDNDC_ERR_FAIL; }

    return  this->open_mounted_source( iobj);
}
lerr_t
ldndc::raw_inputs_t::open_mounted_source(
        input_source_srv_t *  _iobj)
{
    if ( !_iobj)
    {
        return  LDNDC_ERR_FAIL;
    }

    /* open input source */
    lerr_t  rc_open_source = _iobj->open_stream();
    if ( rc_open_source)
    {
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::raw_inputs_t::unmount_all_sources()
{
    this->m_clear();
    return  LDNDC_ERR_OK;
}


lerr_t
ldndc::raw_inputs_t::check_source_exists(
    lid_t const &  _id, char const *  _type)
const
{
    if ( !cbm_is_aic(_type))
    {
        CBM_LogError( "[BUG]  no such input class type");
        return  LDNDC_ERR_FAIL;
    }

    if ( this->number_of_open_sources( _type) == 0)
        { return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST; }

    std::string const  source_key =
        this->make_source_key( _type, _id);
    if ( this->m_sources.find( source_key)
            != this->m_sources.end())
    {
        if ( this->m_sources.find( source_key)->second)
            { return  LDNDC_ERR_OK; }
        CBM_LogWarn( "input object existed but is NULL");
    }
    return  LDNDC_ERR_INVALID_ID;
}
lerr_t
ldndc::raw_inputs_t::get_input_source_(
        input_source_srv_t **  _input_source,
    lid_t const &  _id, char const *  _type)
{
    crabmeat_assert( _input_source);
    crabmeat_assert( cbm_is_aic(_type));

    std::string const  source_key =
        this->make_source_key( _type, _id);
    if ( this->m_sources.find( source_key)
            != this->m_sources.end())
    {
        *_input_source = this->m_sources.find( source_key)->second;
        return  LDNDC_ERR_OK;
    }
    CBM_LogError( "input object request failed: given source ID was not found");
    return  LDNDC_ERR_INVALID_ID;
}
lerr_t
ldndc::raw_inputs_t::get_input_source(
        input_source_srv_t **  _input_source,
    lid_t const &  _id, char const *  _type)
{
    return  this->get_input_source_( _input_source, _id, _type);
}

lerr_t
ldndc::raw_inputs_t::get_input_source(
        input_source_srv_t const **  _input_source,
    lid_t const &  _id, char const *  _type)
const
{
    return  const_cast< ldndc::raw_inputs_t * >( this)->get_input_source_(
        const_cast< input_source_srv_t ** >( _input_source), _id, _type);
}
lerr_t
ldndc::raw_inputs_t::get_input_source(
        input_source_srv_t **  _input_source,
    cbm::source_descriptor_t const *  _sd, char const *  _type)
{
    lid_t const  source_id = this->find_by_source_identifier( _sd);
    if ( source_id == invalid_lid)
    {
        return  LDNDC_ERR_INVALID_ID;
    }
    return  this->get_input_source_( _input_source, source_id, _type);
}
lerr_t
ldndc::raw_inputs_t::get_input_source(
        input_source_srv_t const **  _input_source,
    cbm::source_descriptor_t const *  _sd, char const *  _type)
const
{
    return  const_cast< ldndc::raw_inputs_t * >( this)->get_input_source(
        const_cast< input_source_srv_t ** >( _input_source), _sd, _type);
}





lerr_t
ldndc::raw_inputs_t::check_input_exists(
        cbm::source_descriptor_t const *  _sd,
            char const *  _type)
const
{
    crabmeat_assert( cbm_is_aic(_type));

    input_source_srv_t const *  iobj = NULL;
    this->get_input_source( &iobj, _sd, _type);
    if ( iobj)
    {
        bool  rc_exists =
            iobj->check_input_exists( _sd->block_id);
        if ( rc_exists)
            { return  LDNDC_ERR_OK; }

    }
    return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
}

lerr_t
ldndc::raw_inputs_t::source_handle_acquire(
                source_handle_srv_t *  _sh,
        cbm::source_descriptor_t const *  _sd,
                char const *  _type)
{
    crabmeat_assert( _sh);
    crabmeat_assert( _sd);
    crabmeat_assert( cbm_is_aic(_type));

    input_source_srv_t *  iobj = NULL;
    this->get_input_source( &iobj, _sd, _type);
    if ( !iobj)
    {
        CBM_LogError( "core input object request failed:",
            " no such core ID  [id=",_sd,",type=",_type,"]");
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }
    return  iobj->source_handle_acquire( _sh, _sd->block_id);
}

size_t
ldndc::raw_inputs_t::number_of_open_sources()
const
{
    return  this->number_of_open_sources( NULL, NULL);
}
size_t
ldndc::raw_inputs_t::number_of_open_sources(
        char const *  _type, size_t *  _block_cnt)
const
{
    size_t  n = 0;
    if ( _block_cnt)
        { *_block_cnt = 0; }

    if ( !cbm_is_aic(_type))
        { return  ldndc::invalid_t< size_t >::value; }

    cbm::string_t  type;
    if ( _type)
        { type = cbm::string_t( _type) + "."; }
    for ( sources_const_iterator_type  source = this->m_sources.begin();
            source != this->m_sources.end();  ++source)
    {
        cbm::string_t const  source_key = source->first;
        if ( source->second
            && ( !_type || source_key.startswith( type)))
        {
            n += 1;
            if ( _block_cnt)
                { *_block_cnt += source->second->number_of_blocks(); }
        }
    }
    return  n;
}

size_t
ldndc::raw_inputs_t::number_of_core_blocks()
const
{
    size_t  n = 0;
    this->number_of_open_sources( NULL, &n);
    return  n;
}
size_t
ldndc::raw_inputs_t::number_of_core_blocks(
        char const *  _type) const
{
    size_t  n = 0;
    this->number_of_open_sources( _type, &n);
    return  n;
}

int
ldndc::raw_inputs_t::get_available_blocks(
        source_block_mapping_t  _list[], size_t  _max_size,
        char const *  _type)
const
{
    if ( !_type)
        { return -1; }

    cbm::string_t const  source_key_prefix = cbm::string_t( _type) + ".";
    size_t  s = 0;
    sources_const_iterator_type  src = this->m_sources.begin();
    for ( ;  src != this->m_sources.end();  ++src)
    {
        if ( s == _max_size)
            { break; }

        if ( !src->second)
            { continue; }

        cbm::string_t const  source_key = src->first;
        if ( !source_key.startswith( source_key_prefix))
            { continue; }

        int const  cs = static_cast< int >( src->second->number_of_blocks());
        if ( cs < 0)
            { return  -1; }
        else if ( cs == 0) /* empty source */
            { continue; }


        _list[s].bl = CBM_DefaultAllocator->construct< lid_t >( cs);
        if ( !_list[s].bl)
            { return  -1; }

        int const  rcs = static_cast< int >( src->second->get_available_blocks( _list[s].bl, cs));
        if ( cs != rcs)
        {
            CBM_LogError( "got less blocks than i expected",
                  "  [received=",rcs,",expected=",cs,"]");
            return  -1;
        }
        _list[s].bl_size = rcs;
        cbm::as_strcpy( &_list[s].sd.source_identifier, src->second->source_identifier());
    
        ++s;
    }

    return  static_cast< int >( s);
}


void
ldndc::raw_inputs_t::m_clear()
{
    sources_iterator_type  iobj = this->m_sources.begin();
    for ( ; iobj != this->m_sources.end();  ++iobj)
    {
        if ( iobj->second)
        {
            iobj->second->delete_instance();
            iobj->second = NULL;
        }
    }
    this->m_sources.clear();
}

lid_t
ldndc::raw_inputs_t::find_by_source_identifier(
        char const *  _identifier) const
{
    crabmeat_assert( _identifier);
    sources_const_iterator_type  source = this->m_sources.begin();
    for ( ; source != this->m_sources.end();  ++source)
    {
        if ( !source->second)
            { continue; }
        if ( cbm::is_equal(
                source->second->source_identifier(), _identifier))
            { return  source->second->object_id(); }
    }
    return  invalid_lid;
}
lid_t
ldndc::raw_inputs_t::find_by_source_identifier(
    cbm::source_descriptor_t const *  _sd) const
{
    return  this->find_by_source_identifier( _sd->source_identifier);
}

lid_t
ldndc::raw_inputs_t::available_source_id()
const
{
    for ( size_t  k = 0;  k < this->m_sources.size();  ++k)
    {
        lid_t  I = static_cast< lid_t >( k);

        sources_const_iterator_type  source = this->m_sources.begin();
        for ( ; source != this->m_sources.end();  ++source)
        { 
            if ( source->second && ( source->second->object_id()==I))
            {
                I = invalid_lid; /*ID is used*/
                break;
            }
        }
        if ( I != invalid_lid)
        {
            /* reuse ID */
            return  static_cast< lid_t >( k);
        }
    }

    /* new one otherwise */
    return  static_cast< lid_t >( this->m_sources.size());
}

std::string
ldndc::raw_inputs_t::make_source_key(
    char const *  _type, lid_t const &  _lid) const
{
    cbm::string_t  source_key = _type;
    source_key << "." << _lid;
    return  source_key.str();
}


lerr_t
ldndc::raw_inputs_t::update(
        cbm::sclock_t const *  _lclock)
{
    lerr_t  rc_update_output = LDNDC_ERR_OK;

    sources_const_iterator_type  source = this->m_sources.begin();
    sources_const_iterator_type  source_end = this->m_sources.end();
    for ( ;  source != source_end;  ++source)
    {
        if ( !source->second)
            { continue; }
        lerr_t  rc_update = source->second->update( _lclock);
        if ( rc_update)
            { rc_update_output = rc_update; }
    }

    return  rc_update_output;
}
lerr_t
ldndc::raw_inputs_t::commit(
        cbm::sclock_t const *  _lclock)
{
    lerr_t  rc_commit_output = LDNDC_ERR_OK;

    sources_const_iterator_type  source = this->m_sources.begin();
    sources_const_iterator_type  source_end = this->m_sources.end();
    for ( ;  source != source_end;  ++source)
    {
        if ( !source->second)
            { continue; }
        lerr_t  rc_commit = source->second->commit( _lclock);
        if ( rc_commit)
            { rc_commit_output = rc_commit; }
    }

    return  rc_commit_output;
}
lerr_t
ldndc::raw_inputs_t::reset()
{
    lerr_t  rc_reset_output = LDNDC_ERR_OK;

    sources_const_iterator_type  source = this->m_sources.begin();
    sources_const_iterator_type  source_end = this->m_sources.end();
    for ( ;  source != source_end;  ++source)
    {
        if ( !source->second)
            { continue; }
        lerr_t  rc_reset = source->second->reset();
        if ( rc_reset)
            { rc_reset_output = rc_reset; }
    }

    return  rc_reset_output;
}

