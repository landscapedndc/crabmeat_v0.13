/*!
 * @brief
 *    serves as a getter for input classes
 *    available within the given project
 *    (implementation)
 *
 * @author
 *    steffen klatt (created on: jan 20, 2014)
 */

#include  "cbm_inputbroker.h"
#include  "cbm_project.h"

ldndc::input_class_broker_t::input_class_broker_t()
                : p( NULL)
{ }
ldndc::input_class_broker_t::input_class_broker_t(
                cbm::project_t const *  _project,
                cbm::source_descriptor_t const *  _sd)
                : p( _project), sd( *_sd)
{ }

ldndc::input_class_global_srv_base_t const *
ldndc::input_class_broker_t::get_input_class_global(
        lid_t const &  _stream_id, char const *  _type)
const
{
    crabmeat_assert(p);
    return  this->p->get_input_class_global( _stream_id, _type);
}

ldndc::input_class_srv_base_t const *
ldndc::input_class_broker_t::get_input_class_indirect(
        char const *  _type)
const
{
    crabmeat_assert(p);
    return  this->p->get_input_class_indirect( &this->sd, _type);
}

