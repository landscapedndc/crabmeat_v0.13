/*!
 * @brief
 *
 * @authors
 *    steffen klatt (created on: nov 02, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_OUTPUT_H_
#define  LDNDC_OUTPUT_H_

#include  "crabmeat-common.h"
#include  "time/cbm_time.h"

#include  "io/sink-handle-srv.h"
#include  "io/data-receiver/receiver.h"

#include  "openmp/cbm_omp.h"

#include  <vector>

namespace ldndc {

class  output_sink_srv_t;
struct  sink_info_t;

class CBM_API  raw_outputs_t  :  public  cbm::server_object_t
{
    LDNDC_SERVER_OBJECT(raw_outputs_t)

    /* output objects container related types */
    typedef  std::vector< output_sink_srv_t * >  output_objects_container_type;
    typedef  output_objects_container_type::iterator  output_objects_iterator_type;
    public:
        raw_outputs_t();

        ~raw_outputs_t();

        lid_t  mount_sink(
                sink_info_t const *);

        lerr_t  unmount_sink(
                lid_t const & /*sink descriptor*/);
        lerr_t  unmount_all_sinks();

        lid_t  check_output_object_exists(
                char const * /*sink identifier*/, int /*index*/) const;
        /*!
         * @brief
         *    retrieving output handle
         */
        sink_handle_srv_t  sink_handle_acquire(
                lid_t const & /*sink descriptor*/,
                lreceiver_descriptor_t const & /*receiver descriptor*/);
        lerr_t  sink_handle_release(
                lid_t const & /*sink descriptor*/,
                lreceiver_descriptor_t const & /*receiver descriptor*/);

    public:
        lerr_t  update( cbm::sclock_t const *);
        lerr_t  commit( cbm::sclock_t const *);
        lerr_t  reset();

// sk:later    public:
// sk:later        /*!
// sk:later         * @brief
// sk:later         *    return number of opened streams.
// sk:later         */
// sk:later        size_t  number_of_open_streams() const;

    private:
        /* holds pointers to output objects */ 
        output_objects_container_type  sinks_;
        cbm::omp::omp_nest_lock_t  sink_mount_lock;
};

}


#endif  /*  !LDNDC_OUTPUT_H_  */

