/*!
 * @brief
 *    server-side interface to sink objects (implementation)
 *
 * @authors
 *    steffen klatt (created on: nov 03, 2013),
 *    edwin haas
 */

#include  "io/sink-handle-srv.h"
#include  "log/cbm_baselog.h"

LDNDC_SERVER_OBJECT_DEFN(ldndc::sink_handle_srv_t)
ldndc::sink_handle_srv_t::sink_handle_srv_t()
                : cbm::server_object_t(),
                  rcv_( NULL),
                  lrcv_( NULL)
{
}
ldndc::sink_handle_srv_t::sink_handle_srv_t(
                lid_t const &  _object_id)
                : cbm::server_object_t( _object_id),
                  rcv_( NULL),
                  lrcv_( NULL)
{
}

ldndc::sink_handle_srv_t::~sink_handle_srv_t()
{
}

lerr_t
ldndc::sink_handle_srv_t::status()
const
{
    if ( !this->state.ok)
    {
        return  LDNDC_ERR_FAIL;
    }


    if ( !this->lrcv_ && this->rcv_)
    {
        CBM_LogFatal( "[BUG]  wow :o");
    }
    else if ( this->lrcv_ && !this->rcv_)
    {
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }
    else if ( !this->lrcv_ && !this->rcv_)
    {
        /* null sink */
        return  ( this->object_id() == invalid_lid) ? LDNDC_ERR_INVALID_ID : LDNDC_ERR_OK;
    }
    else /* ( this->lrcv_ && this->rcv_) */
    {
        if (( this->lrcv_->status() == LDNDC_ERR_OK) && ( this->rcv_->status() == LDNDC_ERR_OK))
        {
            return  LDNDC_ERR_OK;
        }
    }
    return  LDNDC_ERR_FAIL;
}

lid_t
ldndc::sink_handle_srv_t::sink_descriptor()
const
{
    return  ( this->lrcv_) ? this->lrcv_->descriptor() : this->object_id();
}
lreceiver_descriptor_t
ldndc::sink_handle_srv_t::receiver_descriptor()
const
{
    return  ( this->rcv_) ? this->rcv_->descriptor() : invalid_t< lreceiver_descriptor_t >::value;
}


lerr_t
ldndc::sink_handle_srv_t::flush_stream()
{
    if ( !this->lrcv_)
    {
        return  LDNDC_ERR_OBJECT_DOES_NOT_EXIST;
    }
    return  this->lrcv_->flush();
}


lerr_t
ldndc::sink_handle_srv_t::set_fixed_layout(
        sink_record_meta_srv_t const *  _record_meta,
        sink_entity_layout_t const *  _layout)
{
    crabmeat_assert( this->lrcv_ && this->rcv_);
    return  this->lrcv_->set_fixed_layout( this->rcv_, _record_meta, _layout);
}
lerr_t
ldndc::sink_handle_srv_t::write_record(
        sink_client_t const *  _client,
        sink_record_srv_t const *  _record)
{
    crabmeat_assert( this->lrcv_);
    crabmeat_assert( this->rcv_);
    crabmeat_assert( _record);

    crabmeat_assert( _record->seq);
// sk:dbg    CBM_LogDebug( "sink_handle_srv_t:  T=",_record->seq->timestep, "  record-sequence-number=",_record->seq->records_in_timestep);

    sink_record_srv_t  this_record = *_record;
    if (( _record->layout.rank == 0) && _record->data)
    {
        if ( this->rcv_->layout( &_record->layout_hint))
        {
            this_record.layout =
                *this->rcv_->layout( &_record->layout_hint);
        }
    }
    if (( _record->ents.size == 0) && _record->data)
    {
        if ( this->rcv_->entities( &_record->layout_hint))
        {
            this_record.ents =
                *this->rcv_->entities( &_record->layout_hint);
        }
    }

// sk:later    lerr_t  rc_unmarshal = unmarshal_from_layout( data_record.data, &this_record.layout);

    lerr_t  rc_write = this->lrcv_->dispatch_write( this->rcv_, _client, &this_record);
    if ( rc_write)
    {
        CBM_LogError( "errors during write dispatch  [id=",(_client?_client->target_id:-1),"]");
        return  LDNDC_ERR_FAIL;
    }

    return  LDNDC_ERR_OK;
}


void
ldndc::sink_handle_srv_t::set_receiver(
        oproc_receiver_t *  _rcv,
        oproc_landscape_receiver_t *  _lrcv)
{
    if ( !this->rcv_)
    {
        this->rcv_ = dynamic_cast< oproc_receiver_t * >( _rcv);
    }
    if ( !this->lrcv_)
    {
        this->lrcv_ = dynamic_cast< oproc_landscape_receiver_t * >( _lrcv);
    }
}

bool
ldndc::sink_handle_srv_t::receiver_requires_input(
        input_class_set_t *  _input_set)
{
    if ( !this->lrcv_)
    {
        return  false;
    }
    return  this->lrcv_->flag_required_inputs( _input_set);
}
lerr_t
ldndc::sink_handle_srv_t::receiver_pass_required_input(
        input_class_set_t *  _input_set)
{
    if ( !this->lrcv_)
    {
        return  LDNDC_ERR_RUNTIME_ERROR;
    }
    return  this->lrcv_->pull_required_inputs( _input_set, this->rcv_);
}

