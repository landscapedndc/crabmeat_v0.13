/*!
 * @brief
 *    holds input for single simulation (implementation)
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011)
 */

#include  "cbm_project.h"
#include  "cbm_rtcfg.h"

#include  "io/source-info.h"
#include  "io/input-source.h"
#include  "io/sink-info.h"

#include  "input/ic-pool.h"

#include  "input/setup/setup-srv.h"

#include  "log/cbm_baselog.h"
#include  "utils/cbm_utils.h"
#include  "string/cbm_string.h"

LDNDC_SERVER_OBJECT_DEFN(cbm::project_t)
cbm::project_t::project_t(
        lid_t const &  _id)
        : cbm::server_object_t( _id)
{
    this->m_inputs.set_object_id( _id);
    this->m_icpool.set_object_id( _id);

    this->m_outputs.set_object_id( _id);
}

cbm::project_t::~project_t()
{
    this->detach_all_sources();
    this->detach_all_sinks();
}

lerr_t
cbm::project_t::open_project()
{
    if ( !CBM_LibRuntimeConfig.prj)
        { return  LDNDC_ERR_RUNTIME_ERROR; }

    ldndc::project_file_t::source_const_iterator  source = CBM_LibRuntimeConfig.prj->sources_begin();
    for ( ; source != CBM_LibRuntimeConfig.prj->sources_end();  ++source)
    {
        std::string const  i_name = source->full_name();
        lid_t  iobj_id = this->attach_source( &(*source));
        if ( iobj_id == invalid_lid)
        {
            CBM_LogError( "failed to attach source  [source=",i_name,"]");
            return  LDNDC_ERR_OBJECT_CREATE_FAILED;
        }
    }

    return  LDNDC_ERR_OK;
}


void
cbm::project_t::detach_all_sources()
{
    /* free input class pool */
    lerr_t  rc_unmount_classes = this->m_icpool.unmount_all_input_classes();
    if ( rc_unmount_classes)
    {
        CBM_LogWarn( "errors occurred while unmounting input classes");
    }

    /* free input subsystem */
    lerr_t  rc_unmount_sources = this->m_inputs.unmount_all_sources();
    if ( rc_unmount_sources)
    {
        CBM_LogWarn( "errors occurred while unmounting sources");
    }
}


lid_t
cbm::project_t::attach_source(
        ldndc::source_info_t const *  _source_info)
{
    if ( !_source_info)
        { return  invalid_lid; }

    /* open and scan source */
    return  this->m_inputs.mount_and_open_source( _source_info);
}

size_t
cbm::project_t::number_of_open_sources() const
    { return  this->m_inputs.number_of_open_sources(); }
size_t
cbm::project_t::number_of_open_sources(
        char const *  _type) const
    { return  this->m_inputs.number_of_open_sources( _type); }

size_t
cbm::project_t::number_of_core_blocks() const
    { return  this->m_inputs.number_of_core_blocks(); }
size_t
cbm::project_t::number_of_core_blocks(
        char const *  _type) const
    { return  this->m_inputs.number_of_core_blocks( _type); }


lid_t
cbm::project_t::attach_sink(
        char const *  _sink_identifier, int  _sink_index)
{
    /* check if sink stream is already mounted */
    lid_t  sid = this->m_outputs.check_output_object_exists( _sink_identifier, _sink_index);
    if ( cbm::is_valid( sid))
    {
// sk:dbg        CBM_LogDebug( "sink already mounted. returning it's descriptor  [sink=",_sink_identifier,",index=",_sink_index,"]");
        return  sid;
    }

    CBM_LogDebug( "sink not yet mounted .. attempting to find sink information",
          "  [sink=",_sink_identifier,",index=",_sink_index,"]");

    /* try to find exact match (i.e. identifier and index) or
     * use partial match (i.e. only match identifier for smallest given index)
     */
    ldndc::project_file_t::sink_const_iterator  ssnk_it;
    ssnk_it = CBM_LibRuntimeConfig.prj->sinks_begin();

    ldndc::sink_info_t  sink_info;
    sink_info.index = _sink_index;

    int  have_partial_match = 0;
    
    for ( ; ssnk_it != CBM_LibRuntimeConfig.prj->sinks_end();  ++ssnk_it)
    {
        if ( cbm::is_equal( ssnk_it->identifier, _sink_identifier))
        {
// sk:dbg            CBM_LogDebug( "match identifier  [sink=",_sink_identifier,"]");
            if ( ssnk_it->index == _sink_index)
            {
                CBM_LogDebug( "match found for sink information",
                      "  [sink=",_sink_identifier,",index=",_sink_index,"]");
                sink_info = *ssnk_it;
                sid = this->m_outputs.mount_sink( &sink_info);
                return  sid;
            }

            if ( ssnk_it->index < sink_info.index)
            {
                CBM_LogDebug( "better partial match  [sink=",_sink_identifier,
                    ",index=",_sink_index,"(",sink_info.index,")]");
                have_partial_match = 1;
                sink_info = *ssnk_it;
            }
        }
    }
    if ( have_partial_match)
    {
        CBM_LogDebug( "partial match found for sink information",
            "  [sink=",_sink_identifier,",index=",_sink_index,"(",sink_info.index,")]");
        sink_info.index = _sink_index;
        sid = this->m_outputs.mount_sink( &sink_info);
        return  sid;
    }

    CBM_LogInfo( "did not find sink information. sink not defined.",
        "  [sink=",_sink_identifier,",index=",_sink_index,"]");
    return  invalid_lid;
}
void
cbm::project_t::detach_all_sinks()
{
    lerr_t  rc_unmount_sinks = this->m_outputs.unmount_all_sinks();
    if ( rc_unmount_sinks)
    {
        CBM_LogWarn( "errors occurred while unmounting sinks");
    }
}

ldndc::sink_handle_srv_t
cbm::project_t::sink_handle_acquire(
        cbm::source_descriptor_t const *  _source_descriptor,
        char const *  _type, lid_t const &  _sink_descriptor)
{
    crabmeat_assert( _type);

    ldndc::hash_ic_t  source_descriptor_hash;
    int  hash_len = ldndc::hash_ic( &source_descriptor_hash,
        _source_descriptor->block_id, _source_descriptor->source_identifier, _type);
    lreceiver_descriptor_t  receiver_descriptor = static_cast< lreceiver_descriptor_t >(
            (hash_len<0) ? ldndc::invalid_t< lreceiver_descriptor_t >::value : source_descriptor_hash);

    ldndc::sink_handle_srv_t  sink_handle = this->m_outputs.sink_handle_acquire( _sink_descriptor, receiver_descriptor);
    if ( sink_handle.receiver_requires_input( NULL))
    {
        sink_handle.state.ok = 0;
        CBM_LogError( "injecting inputs in receiver no longer available");
    }

    return  sink_handle;
}
lerr_t
cbm::project_t::sink_handle_release( lid_t const &  _sink_id,
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    return  this->m_outputs.sink_handle_release( _sink_id, _receiver_descriptor);
}



lerr_t
cbm::project_t::reset_io( cbm::source_descriptor_t const *  _sink_descriptor,
                char const *  _type)
{
    return  this->m_icpool.reset_input( _sink_descriptor, _type);
}
lerr_t
cbm::project_t::update_io( cbm::source_descriptor_t const *  _sink_descriptor,
                char const *  _type, cbm::sclock_t const &  _clock)
{
    return  this->m_icpool.update_input( _sink_descriptor, _type, &_clock);
}


lerr_t
cbm::project_t::reset_io()
{
    lerr_t  rc_output = this->m_outputs.reset();
    RETURN_IF_NOT_OK(rc_output);
    lerr_t  rc_input = this->m_inputs.reset();
    RETURN_IF_NOT_OK(rc_input);
    lerr_t  rc_pool = this->m_icpool.reset_input();
    RETURN_IF_NOT_OK(rc_pool);

    return  LDNDC_ERR_OK;
}

lerr_t
cbm::project_t::update_io(
        cbm::sclock_t const &  _clock)
{
    lerr_t  rc_input = this->m_inputs.update( &_clock);
    RETURN_IF_NOT_OK(rc_input);
    lerr_t  rc_output = this->m_outputs.update( &_clock);
    RETURN_IF_NOT_OK(rc_output);

    lerr_t  rc_pool = this->m_icpool.update_input( &_clock);
    RETURN_IF_NOT_OK(rc_pool);

    return  LDNDC_ERR_OK;
}
lerr_t
cbm::project_t::commit_io(
        cbm::sclock_t const &  _clock)
{
    lerr_t  rc_output = this->m_outputs.commit( &_clock);
    RETURN_IF_NOT_OK(rc_output);
    lerr_t  rc_input = this->m_inputs.commit( &_clock);
    RETURN_IF_NOT_OK(rc_input);

    return  LDNDC_ERR_OK;
}


static
lid_t
try_attach_default_source_for_type(
        cbm::project_t *  _project, char const *  _type)
{
    /* try attaching input type's default source */
    ldndc::source_info_t  source_info( _type);
    if ( source_info.is_fully_servable_by_resources())
    {
        source_info.format = "lrsrc";
        source_info.set_nodev();
        CBM_LogInfo( "checking if we can use Lresources  [type=",_type,"]  ... yes!");
        return  _project->attach_source( &source_info);
    }
    CBM_LogInfo( "checking if we can use Lresources  [type=",_type,"]  ... no!");
    return  invalid_lid;
}
ldndc::input_class_srv_base_t const *
cbm::project_t::get_input_class(
        cbm::source_descriptor_t const *  _source_info,
        char const *  _type)
const
{
    return  const_cast< cbm::project_t * >(
                this)->get_input_class( _source_info, _type);
}
ldndc::input_class_srv_base_t *
cbm::project_t::get_input_class(
        cbm::source_descriptor_t const *  _sd,
        char const *  _type)
{
    if ( !_sd || _sd->block_id == invalid_lid)
    {
        return  NULL;
    }

    ldndc::input_class_srv_base_t *  ic = this->m_icpool.get_input_class( _sd, _type);
    if ( !ic)
    {
        if ( cbm_is_aic(_type))
        {
            lerr_t  rc_chk = this->try_find_input_( _sd, _type);
            if ( rc_chk)
                { return  NULL; }

            ldndc::source_handle_srv_t  s_aic;
            lerr_t  rc_source_handle = this->m_inputs.source_handle_acquire( &s_aic, _sd, _type);
            if ( rc_source_handle)
                { return  NULL; }

            this->m_preparesourcehandle( &s_aic, _sd);
            lerr_t  rc_mount_ic = this->try_mount_input_class_( &s_aic, _type);
            if ( rc_mount_ic)
                { return  NULL; }
        }
        else if ( cbm_is_dic(_type))
        {
            ldndc::source_handle_noreader_srv_t  s_dic;
            this->m_preparesourcehandle( &s_dic, _sd);

            lerr_t  rc_mount_ic = this->try_mount_input_class_( &s_dic, _type);
            if ( rc_mount_ic)
                { return  NULL; }
        }
        else
            { CBM_LogFatal( "[BUG]"); }

        /* return newly created input class .. */
        ic = this->m_icpool.get_input_class( _sd, _type);
    }

    return  ic;
}

lerr_t
cbm::project_t::m_preparesourcehandle(
        ldndc::source_handle_srv_base_t *  _sh,
        cbm::source_descriptor_t const *  _sd)
{
    /* allow access to global input classes for inputs */
    _sh->set_input_class_broker( ldndc::input_class_broker_t( this, _sd));

    return  LDNDC_ERR_OK;
}



lerr_t
cbm::project_t::try_find_input_(
        cbm::source_descriptor_t const *  _source_info,
        char const *  _type)
{
    if ( cbm_is_aic(_type))
    {
// sk:dbg        CBM_LogDebug( "input class ",_type," with id ",_source_info->block_id," from stream ",_source_info->source_identifier," not yet constructed");
        /* check if we've got the core input data */
        lerr_t  rc_get_core_iobj = this->m_inputs.check_input_exists( _source_info, _type);
        if ( rc_get_core_iobj == LDNDC_ERR_OBJECT_DOES_NOT_EXIST)
        {
            /* fall back to default only if no stream of requested type is already open */
            if ( this->m_inputs.number_of_open_sources( _type) == 0)
            {
                lid_t  default_source_id =
                    try_attach_default_source_for_type( this, _type);
                if ( default_source_id == invalid_lid)
                {
                    return  LDNDC_ERR_FAIL;
                }
                /* if opening default worked, try again.. */
                return  this->m_inputs.check_input_exists( _source_info, _type);
            }
            else
            {
                lerr_t  rc_open = this->m_inputs.open_mounted_source(
                                _source_info->source_identifier, _type);
                if ( rc_open)
                {
                    return  LDNDC_ERR_FAIL;
                }
                return  this->m_inputs.check_input_exists( _source_info, _type);
            }


            /* well .. we tried */
            return  LDNDC_ERR_FAIL;
        }
        return  LDNDC_ERR_OK;
    }
    else if ( cbm_is_dic(_type))
    {
        return  LDNDC_ERR_OK;
    }
    else
    {
        CBM_LogFatal( "[BUG]");
    }

    return  LDNDC_ERR_FAIL;
}

lerr_t
cbm::project_t::try_mount_input_class_(
        ldndc::source_handle_srv_base_t *  _sh,
        char const *  _type)
{
    /* create input class in pool */
    lerr_t  rc_mount_ic = this->m_icpool.mount_input_class( _sh, _type);
    if ( rc_mount_ic)
    {
        /* that did not work... */
        CBM_LogError( "mounting input class failed",
            "  [type=",_type,",id=",*_sh->get_source_descriptor(),"]");
        return  rc_mount_ic;
    }

// sk:dbg    CBM_LogDebug( "mounted input class  [type=",_type,",id=",*_sd,"]");
    return  rc_mount_ic;
}


lid_t
cbm::project_t::get_source_descriptor(
        cbm::source_descriptor_t *  _source_info,
        cbm::source_descriptor_t const *  _kernel_info,
        char const *  _type)
const
{
    return  this->m_icpool.get_source_descriptor(
                _source_info, _kernel_info, _type);
}

ldndc::input_class_srv_base_t const *
cbm::project_t::get_input_class_indirect(
        cbm::source_descriptor_t const *  _kernel_info,
        char const *  _type)
const
{
    crabmeat_assert( _kernel_info);
    if ( _kernel_info->block_id == invalid_lid)
    {
        return  NULL;
    }

    cbm::source_descriptor_t  source_info;
    /* find core id from "sources" information in setup */
    lid_t  core_block_id = this->m_icpool.get_source_descriptor( &source_info, _kernel_info, _type);
// sk:dbg    CBM_LogDebug( "kernel",*_kernel_info,"<",_type,">=",source_info);

    if ( core_block_id == invalid_lid)
    {
        return  NULL;
    }
    return  this->get_input_class( &source_info, _type);
}


ldndc::input_class_global_srv_base_t const *
cbm::project_t::get_input_class_global(
        lid_t const &  _source_id,
        char const *  _type)
const
{
    return  const_cast< cbm::project_t * >( this)->get_input_class_global( _source_id, _type);
}
ldndc::input_class_global_srv_base_t *
cbm::project_t::get_input_class_global(
        lid_t const &  _source_id,
        char const *  _type)
{
    ldndc::input_class_global_srv_base_t *  ic = this->m_icpool.get_input_class_global( _source_id, _type);
    if ( !ic)
    {
// sk:dbg        CBM_LogDebug( "global input class ",_type," not yet constructed");
        /* check if we've got the input data */
        lerr_t  input_missing = this->m_inputs.check_source_exists( _source_id, _type);
        if ( input_missing)
        {
            /* no such stream id (or out of memory...) */
            CBM_LogVerbose( "no such input object  [stream-id=",_source_id,",class=",_type,"]");
            return  NULL;
        }

        /* retrieve input object */
        ldndc::input_source_srv_t *  iobj = NULL;
        /*lerr_t  rc_get_iobj = */(void)this->m_inputs.get_input_source( &iobj, _source_id, _type);
        crabmeat_assert( /*!rc_get_iobj &&*/ iobj);

        /* create global input class in pool */
        lerr_t  rc_mount_ic =  this->m_icpool.mount_input_class_global( iobj, _type, &CBM_LibRuntimeConfig.clk->schedule());
        if ( rc_mount_ic)
        {
            /* that did not work... */
            return  NULL;
        }

        /* return newly created global input class */
        ic = this->m_icpool.get_input_class_global( _source_id, _type);
    }

    return  ic;
}


#include  "io/source-handle-srv.h"
std::vector< cbm::source_descriptor_t >
cbm::project_t::object_id_list( char const *  _type)
const
{
    std::vector< cbm::source_descriptor_t >  source_v;

    int  source_cnt =
        static_cast< int >( this->m_inputs.number_of_open_sources( _type));
    if ( source_cnt == 0)
    {
        return  source_v;
    }
    int  block_cnt =
        static_cast< int >( this->m_inputs.number_of_core_blocks( _type));
    if ( block_cnt == 0)
    {
        return  source_v;
    }

    ldndc::raw_inputs_t::source_block_mapping_t *  block_list =
        CBM_DefaultAllocator->construct< ldndc::raw_inputs_t::source_block_mapping_t >( source_cnt);
    for ( size_t  s = 0;  s < static_cast< size_t >( source_cnt);  ++s)
    {
        cbm::set_source_descriptor_defaults( &block_list[s].sd, _type);
        block_list[s].bl = NULL;
        block_list[s].bl_size = 0;
    }

    this->m_inputs.get_available_blocks( block_list, static_cast< size_t >( source_cnt), _type);
    for ( size_t  s = 0;  s < static_cast< size_t >( source_cnt);  ++s)
    {
        if ( block_list[s].bl == NULL)
        {
            continue;
        }

        cbm::source_descriptor_t  sd;
        cbm::as_strcpy( sd.source_identifier, block_list[s].sd.source_identifier);
        for ( size_t  c = 0;  c < block_list[s].bl_size;  ++c)
        {
            sd.block_id = block_list[s].bl[c];
            CBM_LogDebug( "adding block ",sd.block_id, "  source='",sd.source_identifier,"'");
            source_v.push_back( sd);
        }
    }

    if ( block_list)
    {
        for ( size_t  s = 0;  s < static_cast< size_t >( source_cnt);  ++s)
        {
            if ( block_list[s].bl)
            {
                CBM_DefaultAllocator->destroy( block_list[s].bl, block_list[s].bl_size);
            }
        }
        CBM_DefaultAllocator->destroy( block_list, source_cnt);
    }

    return  source_v;
}


