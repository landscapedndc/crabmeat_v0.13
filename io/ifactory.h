/*!
 * @brief
 *    virtual input reader
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IPROCFACTORY_H_
#define  LDNDC_IO_IPROCFACTORY_H_

#include  "crabmeat-common.h"

namespace ldndc {
class  iobject_handler_base_t;
class  iproc_landscape_reader_base_t;
class  iproc_reader_base_t;
struct  iproc_provider_t;
#ifdef  _HAVE_UNITS_CONVERSION
class  iproc_iuconverter_base_t;
#endif
#ifdef  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK
class  iproc_iaggregator_base_t;
#endif
#ifdef  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK
class  iproc_isynthesizer_base_t;
#endif

/*!
 * @brief
 *    input processors object factory
 */
struct  CBM_API  iproc_factories_t
{
    iproc_factories_t();
    ~iproc_factories_t();

    /*!
     * @brief
     *    check if i/o format handler is available for
     *    given format.
     */
    bool  ihandler_available( char const * /*format*/) const;
    /*!
     * @brief
     *    construct i/o format handler for given i/o format
     */
    iobject_handler_base_t *  construct_ihandler( char const * /*format*/) const;
    /*!
     * @brief
     *    destroy i/o format handler
     */
    void  destroy_ihandler( iobject_handler_base_t *) const;

    /*!
     * @brief
     *    check if appropriate landscape reader is available.
     *    if no i/o format is specified first existing
     *    reader for type yields true, false if none
     *    is found.
     */
    bool  landscape_ireader_available( char const * /*type*/,
                        char const * = NULL /*format*/) const;
    /*!
     * @brief
     *    construct landscape reader for given type and i/o format
     */
    iproc_landscape_reader_base_t *  construct_landscape_ireader(
            char const * /*type*/, char const * /*format*/) const;
    /*!
     * @brief
     *    destroy landscape reader
     */
    void  destroy_landscape_ireader(
            iproc_landscape_reader_base_t *) const;
    /*!
     * @brief
     *    check if appropriate reader is available. if
     *    no i/o format is specified first existing
     *    reader for type yields true, false if none
     *    is found.
     */
    bool  ireader_available( char const * /*type*/,
                    char const * = NULL /*format*/) const;
    /*!
     * @brief
     *    construct reader for given type and i/o format
     */
    iproc_reader_base_t *  construct_ireader( char const * /*type*/,
                    char const * /*format*/) const;
    /*!
     * @brief
     *    destroy reader
     */
    void  destroy_ireader( iproc_reader_base_t *) const;

#ifdef  _HAVE_UNITS_CONVERSION
    /*!
     * @brief
     *    check if unit converter is available for given
     *    input type
     */
    bool  iuconverter_available( char const * /*type*/) const;
    /*!
     * @brief
     *    costruct input unit converter
     */
    iproc_iuconverter_base_t *  construct_iuconverter(
            char const * /*type*/) const;
    void  destroy_iuconverter( iproc_iuconverter_base_t *) const;
#endif  /*  _HAVE_UNITS_CONVERSION  */

#ifdef  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK
    bool  iaggregator_available( char const * /*type*/) const;
    iproc_iaggregator_base_t *  construct_iaggregator(
            char const * /*type*/) const;
    void  destroy_iaggregator( iproc_iaggregator_base_t *) const;
#endif  /*  _HAVE_INPUTS_SERVER_SIDE_AGGREGATE_OK  */

#ifdef  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK
    bool  isynthesizer_available( char const * /*type*/) const;
    iproc_isynthesizer_base_t *  construct_isynthesizer(
            char const * /*type*/) const;
    void  destroy_isynthesizer( iproc_isynthesizer_base_t *) const;
#endif  /*  _HAVE_INPUTS_SERVER_SIDE_SYNTHESIZE_OK  */

    iproc_provider_t *  construct_iproc_stack(
            iproc_provider_t *, char const * /*type*/) const;
    void  destroy_iproc_stack( iproc_provider_t *) const;
};
}


#endif  /*  !LDNDC_IO_IPROCFACTORY_H_  */

