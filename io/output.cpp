/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: nov 02, 2013),
 *    edwin haas
 */

#include  "io/output.h"
#include  "io/output-sink.h"

#include  "log/cbm_baselog.h"

LDNDC_SERVER_OBJECT_DEFN(ldndc::raw_outputs_t)

ldndc::raw_outputs_t::raw_outputs_t()
        : cbm::server_object_t()
{
    cbm::omp::init_nest_lock( &this->sink_mount_lock);
}

ldndc::raw_outputs_t::~raw_outputs_t()
{
    /* free all input objects held by this object */
    (void)this->unmount_all_sinks();
    cbm::omp::destroy_nest_lock( &this->sink_mount_lock);
}

#include  "string/lstring-compare.h"
lid_t
ldndc::raw_outputs_t::mount_sink(
        sink_info_t const *  _sink_info)
{
    crabmeat_assert( _sink_info);

    cbm::omp::set_nest_lock( &this->sink_mount_lock, "output:mount");

    /* check if stream conflicts with mounted streams */
    lid_t  this_sd = this->check_output_object_exists( _sink_info->identifier.c_str(), _sink_info->index);
    if ( this_sd != invalid_lid)
    {
        cbm::omp::unset_nest_lock( &this->sink_mount_lock, "output:mount");
        return  this_sd;
    }

    output_sink_srv_t *  this_oobj = output_sink_srv_t::new_instance();
    if ( !this_oobj)
    {
        cbm::omp::unset_nest_lock( &this->sink_mount_lock, "output:mount");
        return  invalid_lid;
    }

    /* find available sink descriptor */
    for ( size_t  sd = 0;  sd < this->sinks_.size();  ++sd)
    {
        if ( !this->sinks_[sd])
        {
            this_sd = static_cast< lid_t >( sd);
            break;
        }
    }

    if ( this_sd == invalid_lid)
    {
        this_sd = static_cast< lid_t >( this->sinks_.size());
        this_oobj->set_object_id( this_sd);
        this->sinks_.push_back( this_oobj);
    }
    else
    {
        this_oobj->set_object_id( this_sd);
        this->sinks_[this_sd] = this_oobj;
    }

    lerr_t  rc_open = this_oobj->open_stream( _sink_info);
    if ( rc_open)
    {
        CBM_LogError( "failed to open output sink");
        lerr_t  rc_umount = this->unmount_sink( this_oobj->object_id());
        if ( rc_umount)
        {
            CBM_LogWarn( "failed to unmount sink after unsuccessful attempt to open stream  [identifier=",_sink_info->identifier,",index=",_sink_info->index,"]");
        }

        this_sd = invalid_lid;
    }


    cbm::omp::unset_nest_lock( &this->sink_mount_lock, "output:mount");
    return  this_sd;
}

lerr_t
ldndc::raw_outputs_t::unmount_sink(
        lid_t const &  _sink_descriptor)
{
    crabmeat_assert( _sink_descriptor != invalid_lid);

    cbm::omp::set_nest_lock( &this->sink_mount_lock, "output:mount");
    lerr_t  rc_close = LDNDC_ERR_OK;
    lid_t const  sink_descriptor = _sink_descriptor;

    output_sink_srv_t *  oobj = this->sinks_[sink_descriptor];
    if ( oobj)
    {
        (void)oobj->flush_stream();
        rc_close = oobj->close_stream();

        oobj->delete_instance();
        this->sinks_[sink_descriptor] = NULL;
    }

    cbm::omp::unset_nest_lock( &this->sink_mount_lock, "output:mount");
    return  rc_close;
}
lerr_t
ldndc::raw_outputs_t::unmount_all_sinks()
{
    cbm::omp::set_nest_lock( &this->sink_mount_lock, "output:mount");
    lerr_t  rc_unmout = LDNDC_ERR_OK;

    for ( size_t  sd = 0;  sd < this->sinks_.size();  ++sd)
    {
        if ( !this->sinks_[sd])
        {
            continue;
        }

        lerr_t  rc_unmout_sink = this->unmount_sink( static_cast< lid_t >( sd));
        if ( rc_unmout_sink)
        {
            rc_unmout = rc_unmout_sink;
        }
    }
    this->sinks_.clear();

    cbm::omp::unset_nest_lock( &this->sink_mount_lock, "output:mount");
    return  rc_unmout;
}

lid_t
ldndc::raw_outputs_t::check_output_object_exists(
        char const *  _identifier, int  _index)
const
{
    lid_t  this_sink_descriptor = invalid_lid;

    for ( size_t  sd = 0;  sd < this->sinks_.size();  ++sd)
    {
        if ( this->sinks_[sd])
        {
            sink_info_t const *  sink_info = this->sinks_[sd]->operator->();
            if ( cbm::is_equal( sink_info->identifier, _identifier) && ( sink_info->index == _index))
            {
                this_sink_descriptor = static_cast< lid_t >( sd);
                break;
            }
        }
    }

    return  this_sink_descriptor;
}

ldndc::sink_handle_srv_t
ldndc::raw_outputs_t::sink_handle_acquire(
        lid_t const &  _sink_descriptor,
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    crabmeat_assert( _sink_descriptor < this->sinks_.size());
    return  this->sinks_[_sink_descriptor]->sink_handle_acquire( _receiver_descriptor);
}
lerr_t
ldndc::raw_outputs_t::sink_handle_release(
        lid_t const &  _sink_descriptor,
        lreceiver_descriptor_t const &  _receiver_descriptor)
{
    crabmeat_assert( _sink_descriptor < this->sinks_.size());
    return  this->sinks_[_sink_descriptor]->sink_handle_release( _receiver_descriptor);
}


lerr_t
ldndc::raw_outputs_t::update(
        cbm::sclock_t const *  _lclock)
{
    lerr_t  rc_update_output = LDNDC_ERR_OK;
    for ( size_t  s = 0;  s < this->sinks_.size();  ++s)
    {
        if ( !this->sinks_[s])
        {
            continue;
        }

        lerr_t  rc_update = this->sinks_[s]->update( _lclock);
        if ( rc_update)
        {
            rc_update_output = rc_update;
        }
    }

    return  rc_update_output;
}
lerr_t
ldndc::raw_outputs_t::commit(
        cbm::sclock_t const *  _lclock)
{
    lerr_t  rc_commit_output = LDNDC_ERR_OK;
    for ( size_t  s = 0;  s < this->sinks_.size();  ++s)
    {
        if ( !this->sinks_[s])
        {
            continue;
        }

        lerr_t  rc_commit = this->sinks_[s]->commit( _lclock);
        if ( rc_commit)
        {
            rc_commit_output = rc_commit;
        }
    }

    return  rc_commit_output;
}
lerr_t
ldndc::raw_outputs_t::reset()
{
    lerr_t  rc_reset_output = LDNDC_ERR_OK;
    for ( size_t  s = 0;  s < this->sinks_.size();  ++s)
    {
        if ( !this->sinks_[s])
        {
            continue;
        }

        lerr_t  rc_reset = this->sinks_[s]->reset();
        if ( rc_reset)
        {
            rc_reset_output = rc_reset;
        }
    }

    return  rc_reset_output;
}

