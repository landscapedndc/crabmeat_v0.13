/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#include  "io/pmr.h"

ldndc::ioproc_base_t::ioproc_base_t()
        : cbm::server_object_t()
{
}
ldndc::ioproc_base_t::~ioproc_base_t()
{
}

ldndc::iproc_landscape_base_t::iproc_landscape_base_t()
        : ldndc::ioproc_base_t()
{
}
ldndc::iproc_landscape_base_t::~iproc_landscape_base_t()
{
}
ldndc::iproc_base_t::iproc_base_t()
        : ldndc::ioproc_base_t()
{
}
ldndc::iproc_base_t::~iproc_base_t()
{
}

ldndc::oproc_landscape_base_t::oproc_landscape_base_t()
        : ldndc::ioproc_base_t()
{
}
ldndc::oproc_landscape_base_t::~oproc_landscape_base_t()
{
}
ldndc::oproc_base_t::oproc_base_t()
        : ldndc::ioproc_base_t()
{
}
ldndc::oproc_base_t::~oproc_base_t()
{
}


ldndc::iproc_collective_operations_interface_t::iproc_collective_operations_interface_t()
{
}
ldndc::iproc_collective_operations_interface_t::~iproc_collective_operations_interface_t()
{
}

ldndc::oproc_collective_operations_interface_t::oproc_collective_operations_interface_t()
{
}
ldndc::oproc_collective_operations_interface_t::~oproc_collective_operations_interface_t()
{
}


/* factory base */
ldndc::ioproc_factory_base_t::ioproc_factory_base_t() { }
ldndc::ioproc_factory_base_t::~ioproc_factory_base_t() { }

