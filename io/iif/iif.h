/*!
 * @brief
 *    reader base interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IPROC_IIF_H_
#define  LDNDC_IO_IPROC_IIF_H_

#include  "string/cbm_string.h"

/* common parts of all abstract concrete reader declarations and definitions */
#define  __LDNDC_IPROC_INTERFACE_CLASS(__class__)  TOKENPASTE3(iproc_interface_,__class__,_t)
#define  __LDNDC_IPROC_LANDSCAPE_INTERFACE_CLASS(__class__)  TOKENPASTE3(iproc_landscape_interface_,__class__,_t)
#define  __LDNDC_INTERFACE_DECL_COMMON_(__type__,__class__,__name__) \
public: \
    static char const *  input_class_type() \
        { return  #__class__; } \
protected: \
    __type__(); \
    virtual  ~__type__() = 0; \
private:
#define  __LDNDC_INTERFACE_DECL_COMMON(__class__)                    \
    __LDNDC_INTERFACE_DECL_COMMON_(__LDNDC_IPROC_INTERFACE_CLASS(__class__),__class__,"")
#define  __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(__class__)                \
    __LDNDC_INTERFACE_DECL_COMMON_(__LDNDC_IPROC_LANDSCAPE_INTERFACE_CLASS(__class__),__class__,"landscape")


#endif  /*  !LDNDC_IO_IPROC_IIF_H_  */

