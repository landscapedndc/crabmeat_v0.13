/*!
 * @brief
 *    base class for input usually given as (multidimensional) table
 *
 * @author
 *    steffen klatt (created on: nov 16, 2013)
 */

#ifndef  LDNDC_IO_IIFTABLE_H_
#define  LDNDC_IO_IIFTABLE_H_

#include  "io/iif/iif.h"
#include  "time/cbm_time.h"

namespace ldndc {
/* table data processor */
template < typename  _E >
class  iproc_interface_table_base_t
{
    public:
        typedef  typename _E::element_type  element_type;
        struct  config_type
        {
            ltime_t  simulation_time;
            ltime_t  streamdata_time;

            int  offset;
            int  stride;

            typename _E::boundary_data_type const *  boundary_data;
        };
    public:
        /*!
         * @brief
         *    set offset and various format specific structures
         *
         * @param
         *    item name array
         * @param
         *    item name array size
         * @param
         *    record offset
         */
        virtual  lerr_t  preset( char const **, size_t,  config_type *) = 0;

        /*!
         * @brief
         *    reset internal position handle
         */
        virtual  lerr_t  reset() = 0;
        /*!
         * @brief
         *    read data from data source into buffer
         *
         * @param
         *    data buffer
         * @param
         *    ...
         */
        virtual  lerr_t  read(
                element_type [] /*buffer*/, size_t /*record count*/, size_t /*record size*/,
                char const * [] /*record item names*/, size_t /*record items count*/,
                int /*offset*/, int /*stride*/,
                       int * /*items read*/, int * /*items seen*/) = 0;

    protected:
        iproc_interface_table_base_t();
        virtual  ~iproc_interface_table_base_t() = 0;
};
template < typename  _E >
iproc_interface_table_base_t< _E >::iproc_interface_table_base_t() { }
template < typename  _E >
iproc_interface_table_base_t< _E >::~iproc_interface_table_base_t() { }
}


#endif  /*  !LDNDC_IO_IIFTABLE_H_  */

