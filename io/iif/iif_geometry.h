/*!
 * @brief
 *    geometry reader interface declarations
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#ifndef  LDNDC_IO_IIF_GEOMETRY_H_
#define  LDNDC_IO_IIF_GEOMETRY_H_

#include  "io/iif/iif.h"
#include  "geometry/geometrytypes.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'geometry'
 */
class  CBM_API  iproc_landscape_interface_geometry_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(geometry)
};


/*!
 * @brief
 *    interface declaration for core input 'geometry'
 */
class  CBM_API  iproc_interface_geometry_t
{
    __LDNDC_INTERFACE_DECL_COMMON(geometry)
    public:
        virtual  lerr_t  read_shape( cbm::geom_shape_t *) = 0;
        virtual  void  free_shape( cbm::geom_shape_t *) = 0;

        virtual  void  get_mapinfo( cbm::geom_mapinfo_t *) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_GEOMETRY_H_  */

