/*!
 * @brief
 *    providing constructor and destructor
 *
 * @author
 *    steffen klatt (created on: mar 20, 2014),
 *    edwin haas
 */

#include  "io/iif/iif_anyclass.h"

namespace ldndc {
iproc_landscape_interface_anyclass_t::iproc_landscape_interface_anyclass_t() {}
iproc_landscape_interface_anyclass_t::~iproc_landscape_interface_anyclass_t() {}

iproc_interface_anyclass_t::iproc_interface_anyclass_t() {}
iproc_interface_anyclass_t::~iproc_interface_anyclass_t() {}
}

