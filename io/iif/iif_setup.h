/*!
 * @brief
 *    setup reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_SETUP_H_
#define  LDNDC_IO_IIF_SETUP_H_

#include  "io/iif/iif.h"
#include  "geom/cbm_geom.h"
#include  "string/cbm_string.h"

#define  cbm_default_kernelname  "mobile"
namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'setup'
 */
class  CBM_API  iproc_landscape_interface_setup_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(setup)
};


/*!
 * @brief
 *    interface declaration for kernel input 'setup'
 */
class  CBM_API  iproc_interface_setup_t
{
    __LDNDC_INTERFACE_DECL_COMMON(setup)
    public:
        /*!
         * @brief
         *    arbitrary name for this site
         *
         * @return
         *    name of the site
         */
        virtual  std::string  get_site_name() const = 0;

        /* @brief
         *    reads configuration data in section @section
         *    as JSON formatted string */
        virtual  lerr_t  get_section( cbm::string_t * /*buffer*/,
                    char const * /*section*/) const = 0;

        /*!
         * @brief
         *    the list of input class IDs requested
         *    by kernel associated to this setup block.
         *    blocks not provided in input stream will
         *    not be modified.
         *
         * @param
         *    result buffer
         * @param
         *    size of buffer
         * @param
         *    number of input class IDs read
         * @param
         *    number of input class IDs seen
         *
         * @return
         *    LDNDC_ERR_OK, if read was successful
         *
         *    LDNDC_ERR_INVALID_ARGUMENT, if invalid ID specification
         */
        virtual  lerr_t  blocks_consume(
                cbm::string_t &, int *, int *) const = 0;
        /*!
         * @brief
         *    the list of input class IDs created during
         *    simulation by kernel associated to this setup
         *    block. blocks not provided in input stream
         *    will not be modified.
         *
         * @param
         *    result buffer
         * @param
         *    size of buffer
         * @param
         *    number of input class IDs read
         * @param
         *    number of input class IDs seen
         *
         * @return
         *    LDNDC_ERR_OK, if read was successful
         *
         *    LDNDC_ERR_INVALID_ARGUMENT, if invalid ID specification
         */
        virtual  lerr_t  blocks_produce(
                cbm::string_t &, int *, int *) const = 0;

        /* general section */
        /*    general location section */
        virtual  lerr_t  get_centroid( cbm::geom_coord_t *) const = 0;
        virtual  lerr_t  get_boundingbox( cbm::geom_bounding_box_t *) const = 0;

        virtual  int  get_canopylayers() const = 0;
        virtual  int  get_soillayers() const = 0;
    
        virtual  double  get_elevation() const = 0;
        virtual  double  get_latitude() const = 0;
        virtual  double  get_longitude() const = 0;

        virtual  double  get_area() const = 0;
        virtual  double  get_volume() const = 0;

        virtual  int  get_timezone() const = 0;
        /*    general terrain section */
        virtual  double  get_slope() const = 0;
        virtual  double  get_aspect() const = 0;

        virtual  size_t  get_number_of_neighbors() const = 0;
        virtual  lerr_t  get_neighbors(
                cbm::neighbor_t [], size_t) const = 0;
        virtual  cbm::geo_orientations_t  get_mooreboundaries() const = 0;


        /* kernel options */
        /*!
         * @brief
         *    specifies if kernel is excluded from simulation
         *
         * @return
         *    1, if attribute evaluated to true or attribute
         *    was not given.
         *
         *    0, if attribute evaluated to false
         *
         *    <0, otherwise (e.g. invalid argument)
         */
        virtual  int  active_flag() const = 0;
        /*!
         * @brief
         *    specifies if kernel is prevented from
         *    creating any outputs
         *
         * @return
         *    same as @fn active_flag()
         */
        virtual  int  quiet_flag() const = 0;
        /*!
         * @brief
         *    specifies if kernel is to be dispatched
         *    on all nodes
         *
         * @return
         *    same as @fn active_flag()
         *
         * @note
         *    this causes duplicates of the kernel
         *    identification across computing nodes,
         *    which may cause errors in transport
         *    protocols and output identification!
         */
        virtual  int  dispatchonany_flag() const = 0;
        /*!
         * @brief
         *    specifies if kernel is included in dump
         *    process
         *
         * @return
         *    same as @fn active_flag()
         */
        virtual  int  checkpointcreate_flag() const = 0;
        /*!
         * @brief
         *    specifies if kernel is included in restore
         *    process
         *
         * @return
         *    same as @fn active_flag()
         */
        virtual  int  checkpointrestore_flag() const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_SETUP_H_  */

