/*!
 * @brief
 *    soil parameter reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_SOILPARAMETERS_H_
#define  LDNDC_IO_IIF_SOILPARAMETERS_H_

#include  "io/iif/iif.h"
#include  "soilparameters/soilparameterstypes.h"

namespace ldndc {
/*!
 * @brief    
 *    interface declaration for input 'soil parameter'
 */
class  CBM_API  iproc_landscape_interface_soilparameters_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(soilparameters)
};


/*!
 * @brief
 *    interface declaration for core input 'soil parameters'
 */
class  CBM_API  iproc_interface_soilparameters_t
{
    __LDNDC_INTERFACE_DECL_COMMON(soilparameters)
    public:
        virtual  lerr_t  get_humus_default_properties(
                humusparameters::humusparameter_t [],  ldndc_string_t const [], char const  [], size_t,
                       int *, int *) const = 0;

        virtual  lerr_t  get_humus_properties(
                humusparameters::humusparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                char const * [], size_t,
                int *, int *,
                int [], int []) const = 0;

        virtual  lerr_t  get_soil_default_properties(
                soilparameters::soilparameter_t [],  ldndc_string_t const [], char const  [], size_t,
                       int *, int *) const = 0;

        virtual  lerr_t  get_soil_properties(
                soilparameters::soilparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                char const * [], size_t,
                int *, int *,
                int [], int []) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_SOILPARAMETERS_H_  */

