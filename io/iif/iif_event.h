/*!
 * @brief
 *    event reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_EVENT_H_
#define  LDNDC_IO_IIF_EVENT_H_

#include  "io/iif/iif.h"
#include  "io/data-provider/provider.h"
#include  "io/input/event/event-tree.h"

#include  "event/eventtypes.h"

#include  "cbm_nan.h"

namespace ldndc {

/*!
 * @brief
 *    interface declaration for input 'event'
 */
class  CBM_API  iproc_landscape_interface_event_t
{
        __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(event)
    public:
        /*!
         * @brief
         *    read input time as a character list (means
         *    time offset of input)
         */
        virtual  std::string  time() const = 0;

};


/*!
 * @brief
 *    interface declaration for core input 'event'
 */
class  CBM_API  iproc_interface_event_t  :  public  iproc_provider_interface_t
{
        __LDNDC_INTERFACE_DECL_COMMON(event)
    public:
        /*!
         * @brief
         *    number of consecutive events that need to read 
         *    during one timestep to not miss any event. this
         *    number is user supplied and needs to be interpreted
         *    as a hint rather than the correct value. a value
         *    of -1 means all event shall be read.
         */
        virtual  int  readahead_size() const = 0;
        int  invalid_readahead_size() const { return  invalid_t< int >::value; }

        /*!
         * @brief
         *    event tree including type and event execution time. leaf nodes
         *    are either events or empty rotations. non-leaf nodes are always
         *    rotations the reader may add a pointer to an identification
         *    object later used for querying attributes for each event.
         */
        virtual  lerr_t  get_event_tree(
                cbm::event_info_tree_t *,
                int /*load limit*/,
                int *, int *) = 0;

        /*!
         * @brief
         *    get specific event attributes according to previously
         *    retrieved meta data
         *
         * @p structure for event attributes
         * @p structure holding event information (see @f get_event_tree)
         * @p attributes read
         * @p attributes seen (counting unknown entries)
         */
        virtual  lerr_t  get_event_attributes(
                event::event_attribute_t *, event::event_attribute_t const *,
                       event::event_info_t *,
                       int *, int *) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_EVENT_H_  */

