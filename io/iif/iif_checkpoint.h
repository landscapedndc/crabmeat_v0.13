/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: may 23, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_IO_IIF_CHECKPOINT_H_
#define  LDNDC_IO_IIF_CHECKPOINT_H_

#include  "io/iif/iif.h"
#include  "checkpoint/checkpointtypes.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'checkpoint'
 */
class  CBM_API  iproc_landscape_interface_checkpoint_t
{
        __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(checkpoint)
};


/*!
 * @brief
 *    interface declaration for core input 'checkpoint'
 */
class  CBM_API  iproc_interface_checkpoint_t
{
        __LDNDC_INTERFACE_DECL_COMMON(checkpoint)
    public:
        virtual  lerr_t  retrieve_record(
                ldndc::checkpoint::checkpoint_buffer_t * /*buffer*/) = 0;
// sk:off                virtual  size_t  number_of_checkpoints() const = 0;
// sk:off                virtual  size_t  checkpoint_size( ldate_t /*timestep*/) = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_CHECKPOINT_H_  */

