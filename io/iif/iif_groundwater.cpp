/*!
 * @brief
 *    providing constructor and destructor
 *
 * @author
 *    steffen klatt (created on: may 29, 2015)
 */

#include  "io/iif/iif_groundwater.h"

namespace ldndc {
iproc_landscape_interface_groundwater_t::iproc_landscape_interface_groundwater_t() {}
iproc_landscape_interface_groundwater_t::~iproc_landscape_interface_groundwater_t() {}

iproc_interface_groundwater_t::iproc_interface_groundwater_t() {}
iproc_interface_groundwater_t::~iproc_interface_groundwater_t() {}
}

