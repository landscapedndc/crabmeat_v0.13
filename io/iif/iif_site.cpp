/*!
 * @brief
 *    providing constructor and destructor
 *
 * @author
 *    steffen klatt (created on: aug 12, 2012)
 */

#include  "io/iif/iif_site.h"

namespace ldndc {
iproc_landscape_interface_site_t::iproc_landscape_interface_site_t() {}
iproc_landscape_interface_site_t::~iproc_landscape_interface_site_t() {}

iproc_interface_site_t::iproc_interface_site_t() {}
iproc_interface_site_t::~iproc_interface_site_t() {}
}

