/*!
 * @brief
 *    site reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_SITE_H_
#define  LDNDC_IO_IIF_SITE_H_

#include  "io/iif/iif.h"
#include  "site/sitetypes.h"
namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'site'
 */
class  CBM_API  iproc_landscape_interface_site_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(site)
};


/*!
 * @brief
 *    interface declaration for core input 'site'
 */
class  CBM_API  iproc_interface_site_t
{
    __LDNDC_INTERFACE_DECL_COMMON(site)
    public:
        virtual  double  get_saturated_hydraulic_conductivity_bottom() const = 0;
        virtual  double  get_water_table_depth() const = 0;

                /* soil general section */
        virtual  double  get_soil_organic_carbon_content_05() const = 0;
        virtual  double  get_soil_organic_carbon_content_30() const = 0;
        virtual  double  get_soil_litter_height() const = 0;
        virtual  std::string  get_soil_type() const = 0;
        virtual  std::string  get_humus_type() const = 0;

        virtual  lerr_t  soil_use_history( std::string *) const = 0;

        /* soil strata section */
        virtual  size_t  get_number_of_strata() const = 0;

        virtual  lerr_t  get_strata_height( double [], size_t, int *, int *) const = 0;
        virtual  lerr_t  get_strata_splitheight( double [], size_t, int *, int *) const = 0;
        virtual  lerr_t  get_strata_split( int [], size_t, int *, int *) const = 0;

        virtual  lerr_t  get_strata_properties( double [] /*buffer*/,
                                                size_t /*number of strata*/,
                                                size_t /*number of properties*/,
                                                ldndc_string_t const [] /*property names*/) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_SITE_H_  */

