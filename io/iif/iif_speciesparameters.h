/*!
 * @brief
 *    species parameter reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_SPECIESPARAMETERS_H_
#define  LDNDC_IO_IIF_SPECIESPARAMETERS_H_

#include  "io/iif/iif.h"
#include  "speciesparameters/speciesparameterstypes.h"
#include  "containers/cbm_bitset.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'species parameter'
 */
class  CBM_API  iproc_landscape_interface_speciesparameters_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(speciesparameters)
};


/*!
 * @brief
 *    interface declaration for core input 'species parameters'
 */
class  CBM_API  iproc_interface_speciesparameters_t
{
    __LDNDC_INTERFACE_DECL_COMMON(speciesparameters)
    public:
        enum  species_filing_e
        {
            /* none type */
            F_NONE = 0,
            /* regular valid species */
            F_REGULAR,
            /* invalid (e.g. missing or too long mnemonic) */
            F_INVALID
        };

        /*!
         * @brief
         *    read default set from core block
         *
         * @param
         *    buffer holding read values
         * @param
         *    parameter name array
         * @param
         *    parameter type array
         * @param
         *    number of parameters
         *
         * @param
         *    number of paramaters read
         * @param
         *    number of parameters seen
         *
         * @returns
         *    LDNDC_ERR_OK if all went well @n
         */
        virtual  lerr_t  get_species_default_properties(
                speciesparameters::speciesparameter_t [],
                ldndc_string_t const [], char const [], size_t, int *, int *) const = 0;

        /*!
         * @brief
         *    report the number of species listed in input stream
         *    including duplicates
         */
        virtual  size_t  get_species_cnt() const = 0;

        virtual  size_t  check_species_properties_given(
                char const *, ldndc::bitset * = NULL, size_t = 0) const = 0;

        virtual  lerr_t  get_species_properties(
                speciesparameters::parameterized_species_t [], size_t,
                ldndc_string_t const [], char const [], size_t,
                int *, int *, int [], int []) const = 0;
        virtual  lerr_t  get_all_species_properties(
                speciesparameters::parameterized_species_t **, size_t *,
                ldndc_string_t const [], char const [], size_t,
                int *, int *, int [], int []) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_SPECIESPARAMETERS_H_  */

