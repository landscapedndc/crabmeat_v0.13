/*!
 * @brief
 *    providing constructor and destructor
 *
 * @author
 *    steffen klatt (created on: aug 12, 2012)
 */

#include  "io/iif/iif_setup.h"

namespace ldndc {
iproc_landscape_interface_setup_t::iproc_landscape_interface_setup_t() {}
iproc_landscape_interface_setup_t::~iproc_landscape_interface_setup_t() {}

iproc_interface_setup_t::iproc_interface_setup_t() {}
iproc_interface_setup_t::~iproc_interface_setup_t() {}
}

