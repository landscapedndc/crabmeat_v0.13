/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 20, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_IO_IIF_ANYCLASS_H_
#define  LDNDC_IO_IIF_ANYCLASS_H_

#include  "io/iif/iif.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'anyclass'
 */
class  CBM_API  iproc_landscape_interface_anyclass_t
{
        __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(anyclass)
};


/*!
 * @brief
 *    interface declaration for core input 'anyclass'
 */
struct  sink_entity_layout_t;
class  CBM_API  iproc_interface_anyclass_t
{
        __LDNDC_INTERFACE_DECL_COMMON(anyclass)
    public:
                virtual  int  read_by_keys(
                void ** /*data*/,
                sink_entity_layout_t const * /*data meta data*/) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_ANYCLASS_H_  */

