/*!
 * @brief
 *    site parameter reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_SITEPARAMETERS_H_
#define  LDNDC_IO_IIF_SITEPARAMETERS_H_

#include  "io/iif/iif.h"
#include  "siteparameters/siteparameterstypes.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'site parameter'
 */
class  CBM_API  iproc_landscape_interface_siteparameters_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(siteparameters)
};


/*!
 * @brief
 *    interface declaration for core input 'site parameters'
 */
class  CBM_API  iproc_interface_siteparameters_t
{
    __LDNDC_INTERFACE_DECL_COMMON(siteparameters)
    public:
        /*!
         * @brief
         *    read core blocks site parameters
         *
         * @param
         *    buffer holding read values
         * @param
         *    parameter name array
         * @param
         *    parameter type array
         * @param
         *    size of parameter name and type array
         *
         * @param
         *    number of parameters read
         * @param
         *    number of parameters seen
         *
         * @returns
         *    LDNDC_ERR_OK if all went well @n
         */
        virtual  lerr_t  get_siteparameters_set(
                siteparameters::siteparameter_t [],
                ldndc_string_t const [], char const [], size_t,
                int *, int *) const = 0;
};
}


#endif  /*  !LDNDC_IO_IIF_SITEPARAMETERS_H_  */

