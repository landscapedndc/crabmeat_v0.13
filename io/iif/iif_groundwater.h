/*!
 * @brief
 *    groundwater reader interface declarations
 *
 * @author
 *    steffen klatt (created on: may 29, 2015)
 */

#ifndef  LDNDC_IO_IIF_GROUNDWATER_H_
#define  LDNDC_IO_IIF_GROUNDWATER_H_

#include  "io/iif/iiftable.h"
#include  "input/groundwater/groundwatertypes-srv.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'groundwater'
 */
class  CBM_API  iproc_landscape_interface_groundwater_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(groundwater)
    public:
        /*!
         * @brief
         *    read input time as a character list (means
         *    time offset of input)
         */
        virtual  std::string  time() const = 0;
};


/*!
 * @brief
 *    interface declaration for core input 'groundwater'
 */
class  CBM_API  iproc_interface_groundwater_t  :  public  ldndc::iproc_interface_table_base_t< groundwater::groundwater_streamdata_info_t >
{
        __LDNDC_INTERFACE_DECL_COMMON(groundwater)
    public:
        virtual  double  get_average_watertable() const = 0;
        virtual  double  get_average_no3() const = 0;
};
}


#endif  /*  !DNDC_IO_IIF_GROUNDWATER_H_  */

