/*!
 * @brief
 *    climate reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_CLIMATE_H_
#define  LDNDC_IO_IIF_CLIMATE_H_

#include  "io/iif/iiftable.h"
#include  "input/climate/climatetypes-srv.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'climate'
 */
class  CBM_API  iproc_landscape_interface_climate_t
{
        __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(climate)
    public:
        /*!
         * @brief
         *    read input time as a character list (means
         *    time offset of input)
         */
        virtual  std::string  time() const = 0;
};


/*!
 * @brief
 *    interface declaration for core input 'climate'
 */
class  CBM_API  iproc_interface_climate_t  :  public  ldndc::iproc_interface_table_base_t< climate::climate_streamdata_info_t >
{
        __LDNDC_INTERFACE_DECL_COMMON(climate)
    public:
        virtual  double  get_latitude() const = 0;
        virtual  double  get_longitude() const = 0;
        virtual  double  get_elevation() const = 0;

        virtual  double  get_cloudiness() const = 0;
        virtual  double  get_rainfall_intensity() const = 0;
        virtual  double  get_wind_speed() const = 0;

        virtual  double  get_annual_precipitation() const = 0;
        virtual  double  get_temperature_average() const = 0;
        virtual  double  get_temperature_amplitude() const = 0;
};
}


#endif  /*  !DNDC_IO_IIF_CLIMATE_H_  */

