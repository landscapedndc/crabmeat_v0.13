
/*!
 * @brief
 *    providing constructor and destructor
 *
 * @author
 *    steffen klatt (created on: may 23, 2014),
 *    edwin haas
 */

#include  "io/iif/iif_checkpoint.h"

namespace ldndc {
iproc_landscape_interface_checkpoint_t::iproc_landscape_interface_checkpoint_t() {}
iproc_landscape_interface_checkpoint_t::~iproc_landscape_interface_checkpoint_t() {}

iproc_interface_checkpoint_t::iproc_interface_checkpoint_t() {}
iproc_interface_checkpoint_t::~iproc_interface_checkpoint_t() {}
}

