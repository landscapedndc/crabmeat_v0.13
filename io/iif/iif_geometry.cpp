/*!
 * @brief
 *    providing constructor and destructor
 *
 * @author
 *    steffen klatt (created on: feb 03, 2014)
 */

#include  "io/iif/iif_geometry.h"

namespace ldndc {
iproc_landscape_interface_geometry_t::iproc_landscape_interface_geometry_t() {}
iproc_landscape_interface_geometry_t::~iproc_landscape_interface_geometry_t() {}

iproc_interface_geometry_t::iproc_interface_geometry_t() {}
iproc_interface_geometry_t::~iproc_interface_geometry_t() {}
}

