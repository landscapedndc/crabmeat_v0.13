/*!
 * @brief
 *    air chemistry reader interface declarations
 *
 * @author
 *    steffen klatt (created on: Jul 8, 2011)
 */

#ifndef  LDNDC_IO_IIF_AIRCHEMISTRY_H_
#define  LDNDC_IO_IIF_AIRCHEMISTRY_H_

#include  "io/iif/iiftable.h"
#include  "input/airchemistry/airchemistrytypes-srv.h"

namespace ldndc {
/*!
 * @brief
 *    interface declaration for input 'air chemistry'
 */
class  CBM_API  iproc_landscape_interface_airchemistry_t
{
    __LDNDC_LANDSCAPE_INTERFACE_DECL_COMMON(airchemistry)
    public:
        /*!
         * @brief
         *    read input time as a character list (means
         *    time offset of input)
         */
        virtual  std::string  time() const = 0;
};


/*!
 * @brief
 *    interface declaration for core input 'air chemistry'
 */
class  CBM_API  iproc_interface_airchemistry_t  :  public  ldndc::iproc_interface_table_base_t< airchemistry::airchemistry_streamdata_info_t >
{
        __LDNDC_INTERFACE_DECL_COMMON(airchemistry)
    public:
        virtual  double  get_average_ch4() const = 0;
        virtual  double  get_average_co2() const = 0;
        virtual  double  get_average_nh3() const = 0;
        virtual  double  get_average_nh4() const = 0;
        virtual  double  get_average_nh4dry() const = 0;
        virtual  double  get_average_no() const = 0;
        virtual  double  get_average_no2() const = 0;
        virtual  double  get_average_no3() const = 0;
        virtual  double  get_average_no3dry() const = 0;
        virtual  double  get_average_o2() const = 0;
        virtual  double  get_average_o3() const = 0;
};
}


#endif  /*  !DNDC_IO_IIF_AIRCHEMISTRY_H_  */

