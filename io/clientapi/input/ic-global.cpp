/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: Jul 14, 2011)
 */

#include  "input/ic-global.h"

ldndc::input_class_global_client_base_t::input_class_global_client_base_t()
        : input_class_base_t(), cbm::client_object_t()
{ }
ldndc::input_class_global_client_base_t::input_class_global_client_base_t(
        lid_t const &  _id)
    : input_class_base_t(), cbm::client_object_t( _id)
{ }

ldndc::input_class_global_client_base_t::~input_class_global_client_base_t()
{ }

