/*!
 * @brief
 *    container for initial set up of soil layers
 *
 * @author
 *      steffen klatt (created on: dec 01, 2011)
 *      edwin haas
 */

#ifndef  LDNDC_INPUT_SOILLAYERS_CLIENT_H_
#define  LDNDC_INPUT_SOILLAYERS_CLIENT_H_

#include  "input/ic.h"
#include  "input/soillayers/soillayers-srv.h"


namespace  ldndc{ namespace  soillayers
{
class  CBM_API  input_class_soillayers_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(soillayers,ICLASS_FLAG_NONE)
    public:
        input_class_soillayers_t()
                : input_class_client_base_t( invalid_lid), soillayers_( NULL)
        { }
        input_class_soillayers_t( lid_t const &  _id)
                : input_class_client_base_t( _id), soillayers_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(soillayers);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }

        lerr_t  stratum( size_t  _s, site::iclass_site_stratum_t *  _stratum)
        const
        {
            return  this->soillayers_->stratum( _s, _stratum);
        }
        inline
        size_t  strata_cnt()
        const
        {
            return  this->soillayers_->strata_cnt();
        }
        inline
        size_t  soil_layer_cnt()
        const
        {
            return  this->soillayers_->soil_layer_cnt();
        }
        inline
        size_t  soil_layers_in_litter_cnt()
        const
        {
            return  this->soillayers_->soil_layers_in_litter_cnt();
        }

    public:
        soillayers::soil_type_e  soil_type() const
        {
                   return  this->soillayers_->soil_type();
        }
        char const *  soil_mnemonic()
        const
        {
            return  this->soillayers_->soil_mnemonic();
        }

        soillayers::humus_type_e  humus_type() const
        {
                   return  this->soillayers_->humus_type();
               }
        char const *  humus_mnemonic()
        const
        {
            return  this->soillayers_->humus_mnemonic();
        }

        double  litterheight() const { return  this->soillayers_->litterheight(); }

        double  c_org05() const { return  this->soillayers_->c_org05(); }
        double  c_org30() const { return  this->soillayers_->c_org30(); }


    private:
        input_class_soillayers_srv_t const *  soillayers_;
};
}}

#include  "input/ic-global.h"
namespace  ldndc{ namespace  soillayers
{
class  CBM_API  input_class_soillayers_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(soillayers,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            set_initialized();
            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SOILLAYERS_CLIENT_H_  */

