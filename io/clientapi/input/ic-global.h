/*!
 * @brief
 *    container for all global input classes (pointers)
 *
 * @author
 *    steffen klatt (created on: sep 13, 2011)
 */

#ifndef  LDNDC_INPUT_CLASS_GLOBAL_CLIENT_H_
#define  LDNDC_INPUT_CLASS_GLOBAL_CLIENT_H_

#include  "crabmeat-common.h"
#include  "input/ic-env.h"

namespace ldndc {
class  input_source_t;

/* (abstract) base class for input class globals (representing global sections) */
class  CBM_API  input_class_global_client_base_t
        :  public  input_class_base_t,  public  cbm::client_object_t
{
    public:
        virtual  ~input_class_global_client_base_t() = 0;

        /* do not keep reference to input object, it will be destroyed! */
        virtual  lerr_t  initialize( input_source_t const *) = 0;

        virtual  bool  has_time()
            const { return  false; }

    protected:
        input_class_global_client_base_t();
        input_class_global_client_base_t( lid_t const &);
};
}

#define  LDNDC_INPUT_GLOBAL_CLASS_DECL(__class__,__flags__) \
    LDNDC_INPUT_CLASS_DECL_(TOKENPASTE3(input_class_,__class__,_global_t),__class__,__flags__) \
    public: \
        typedef input_class_global_client_base_t base_t;
#define  LDNDC_INPUT_GLOBAL_CLASS_DEFN(__class__) \
    LDNDC_INPUT_CLASS_DEFN_(TOKENPASTE3(input_class_,__class__,_global_t))


#endif  /*  !LDNDC_INPUT_CLASS_GLOBAL_CLIENT_H_  */

