/*!
 * @brief
 *    factories for client input classes
 *
 * @author
 *    steffen klatt (created on: apr 07, 2013)
 */
#ifndef  LDNDC_INPUT_CLASS_FACTORIES_CLIENT_H_
#define  LDNDC_INPUT_CLASS_FACTORIES_CLIENT_H_

#include  "crabmeat-common.h"

#include  "input/ic.h"
#include  "input/ic-global.h"

#include  "input/ic-env.h"

namespace ldndc {
    extern CBM_API ldndc::input_class_factory_base_t< input_class_client_base_t, input_class_global_client_base_t > const *
            find_input_class_factory( char /*kind {'L', 'C'} */, char const * /*type*/);
}

#endif  /*  !LDNDC_INPUT_CLASS_FACTORIES_CLIENT_H_  */

