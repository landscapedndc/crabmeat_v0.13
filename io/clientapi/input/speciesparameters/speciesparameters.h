/*!
 * @brief
 *    container class for one complete species parameters set
 * 
 * @author
 *      steffen klatt,
 *      edwin haas
 */

#ifndef  LDNDC_INPUT_SPECIESPARAMS_CLIENT_H_
#define  LDNDC_INPUT_SPECIESPARAMS_CLIENT_H_

#include  "input/ic.h"
#include  "input/speciesparameters/speciesparameters-srv.h"
#ifdef  __SPECIESPARAMETER_VALUE__
#  undef  __SPECIESPARAMETER_VALUE__
#endif
#define  __SPECIESPARAMETER_VALUE__(__name__,__type__)  this->speciesparameters_->__name__()

namespace  ldndc{ namespace  speciesparameters
{
class  CBM_API  input_class_speciesparameters_t  :  public input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(speciesparameters,ICLASS_FLAG_NONE)
    public:
        input_class_speciesparameters_t()
                : input_class_client_base_t( invalid_lid),
                  speciesparameters_( NULL)
        { }
        input_class_speciesparameters_t( lid_t const &  _id)
                : input_class_client_base_t( _id),
                  speciesparameters_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(speciesparameters);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }


    public:
        speciesparameters_set_t  operator[]( std::string const &  _name)
        const
        {
            return  this->speciesparameters_->operator[]( _name);
        }
        speciesparameters_set_t  operator[]( char const *  _name)
        const
        {
            return  this->speciesparameters_->operator[]( _name);
        }
        speciesparameters_set_t  at( size_t  _index)
        const
        {
            return  this->speciesparameters_->at( _index);
        }

        size_t  parameterized_species_cnt()
        const
        {
            return  this->speciesparameters_->parameterized_species_cnt();
        }

        bool  is_family( char const * _type, char const * _family) const
            { return this->speciesparameters_->is_family( _type, _family); }

    private:
        input_class_speciesparameters_srv_t const *  speciesparameters_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  speciesparameters
{
class  CBM_API  input_class_speciesparameters_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(speciesparameters,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            this->set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SPECIESPARAMS_CLIENT_H_  */

