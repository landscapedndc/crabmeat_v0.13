/*!
 * @brief
 *    container for site specific input data
 *
 * @author
 *    Steffen Klatt (created on: mar 31, 2013)
 *    Edwin Haas
 *    David Kraus
 */

#ifndef  LDNDC_INPUT_SITE_CLIENT_H_
#define  LDNDC_INPUT_SITE_CLIENT_H_

#include  "input/ic.h"
#include  "input/site/site-srv.h"

namespace  ldndc{ namespace  site
{
class  CBM_API  input_class_site_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(site,ICLASS_FLAG_NONE)
    public:
        input_class_site_t()
            : input_class_client_base_t( invalid_lid), site_( NULL)
        { }

        input_class_site_t( lid_t const &  _id)
            : input_class_client_base_t( _id), site_( NULL)
        { }


        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            crabmeat_assert( _source_handle);
            this->site_ = _source_handle->data_provider< input_class_site_srv_t >();
            crabmeat_assert( this->site_ && ( this->object_id() == this->site_->object_id()));

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }


    public:
        size_t  strata_cnt() const { return  this->site_->strata_cnt(); }
        lerr_t  stratum( size_t  _s, iclass_site_stratum_t *  _stratum)
        const
        {
            return  this->site_->stratum( _s, _stratum);
        }
        iclass_site_stratum_t  stratum( size_t  _s)
        const
        {
            iclass_site_stratum_t  s;
            lerr_t const  rc_stratum = this->stratum( _s, &s);
            if ( rc_stratum)
            {
                return  iclass_site_stratum_t();
            }
            return  s;
        }

        /*! saturated hydraulic conductivity below soil [cm:min-1] */
        double  sksbottom() const { return  this->site_->info()->sksbottom; }

        /*! depth of water Table beneath soil surface [m] */
        double  watertable() const { return  this->site_->info()->watertable; }

        ecosystem_type_e  soil_use_history() const
        {
                   return  this->site_->soil_use_history();
               }

        double  litterheight() const { return  this->site_->litterheight(); }

        double  c_org05() const { return  this->site_->c_org05(); }
        double  c_org30() const { return  this->site_->c_org30(); }

        soillayers::soil_type_e  soil_type() const { return  this->site_->soil_type(); }
        soillayers::humus_type_e  humus_type() const { return  this->site_->humus_type(); }

    private:
        input_class_site_srv_t const *  site_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  site
{
class  CBM_API  input_class_site_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(site,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SITE_CLIENT_H_  */

