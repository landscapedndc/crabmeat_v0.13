/*!
 * @brief
 *    factory for client input classes (implementation)
 *
 * @author
 *    steffen klatt (created on: apr 07, 2013)
 */

#include  "input/ic-factory.h"


#include  "input/airchemistry/airchemistry.h"
static ldndc::input_class_factory_t< ldndc::airchemistry::input_class_airchemistry_t,
        ldndc::airchemistry::input_class_airchemistry_global_t >  input_airchemistry_factory;
#include  "input/anyclass/anyclass.h"
static ldndc::input_class_factory_t< ldndc::anyclass::input_class_anyclass_t,
        ldndc::anyclass::input_class_anyclass_global_t >  input_anyclass_factory;
#include  "input/checkpoint/checkpoint.h"
static ldndc::input_class_factory_t< ldndc::checkpoint::input_class_checkpoint_t,
        ldndc::checkpoint::input_class_checkpoint_global_t >  input_checkpoint_factory;
#include  "input/climate/climate.h"
static ldndc::input_class_factory_t< ldndc::climate::input_class_climate_t,
        ldndc::climate::input_class_climate_global_t >  input_climate_factory;
#include  "input/event/event.h"
static ldndc::input_class_factory_t< ldndc::event::input_class_event_t,
        ldndc::event::input_class_event_global_t >  input_event_factory;
#include  "input/geometry/geometry.h"
static ldndc::input_class_factory_t< ldndc::geometry::input_class_geometry_t,
        ldndc::geometry::input_class_geometry_global_t >  input_geometry_factory;
#include  "input/groundwater/groundwater.h"
static ldndc::input_class_factory_t< ldndc::groundwater::input_class_groundwater_t,
        ldndc::groundwater::input_class_groundwater_global_t >  input_groundwater_factory;
#include  "input/setup/setup.h"
static ldndc::input_class_factory_t< ldndc::setup::input_class_setup_t,
        ldndc::setup::input_class_setup_global_t >  input_setup_factory;
#include  "input/siteparameters/siteparameters.h"
static ldndc::input_class_factory_t< ldndc::siteparameters::input_class_siteparameters_t,
        ldndc::siteparameters::input_class_siteparameters_global_t >  input_siteparameters_factory;
#include  "input/site/site.h"
static ldndc::input_class_factory_t< ldndc::site::input_class_site_t,
        ldndc::site::input_class_site_global_t >  input_site_factory;
#include  "input/soillayers/soillayers.h"
static ldndc::input_class_factory_t< ldndc::soillayers::input_class_soillayers_t,
        ldndc::soillayers::input_class_soillayers_global_t >  input_soillayers_factory;
#include  "input/soilparameters/soilparameters.h"
static ldndc::input_class_factory_t< ldndc::soilparameters::input_class_soilparameters_t,
        ldndc::soilparameters::input_class_soilparameters_global_t >  input_soilparameters_factory;
#include  "input/speciesparameters/speciesparameters.h"
static ldndc::input_class_factory_t< ldndc::speciesparameters::input_class_speciesparameters_t,
        ldndc::speciesparameters::input_class_speciesparameters_global_t >  input_speciesparameters_factory;
#include  "input/species/species.h"
static ldndc::input_class_factory_t< ldndc::species::input_class_species_t,
        ldndc::species::input_class_species_global_t >  input_species_factory;


struct  __cbm_input_factory_item_t
{
    ldndc::input_class_factory_base_t< ldndc::input_class_client_base_t,
        ldndc::input_class_global_client_base_t > const *  factory;
    char  kind; /* { 'L', 'C' } */
    char const *  role;
};

static  __cbm_input_factory_item_t  __cbm_input_factories[] =
{
    { &input_airchemistry_factory, 'C', "airchemistry" },
    { &input_airchemistry_factory, 'L', "airchemistry" },
    { &input_anyclass_factory, 'C', "anyclass" },
    { &input_anyclass_factory, 'L', "anyclass" },
    { &input_checkpoint_factory, 'C', "checkpoint" },
    { &input_checkpoint_factory, 'L', "checkpoint" },
    { &input_climate_factory, 'C', "climate" },
    { &input_climate_factory, 'L', "climate" },
    { &input_event_factory, 'C', "event" },
    { &input_event_factory, 'L', "event" },
    { &input_geometry_factory, 'C', "geometry" },
    { &input_geometry_factory, 'L', "geometry" },
    { &input_groundwater_factory, 'C', "groundwater" },
    { &input_groundwater_factory, 'L', "groundwater" },
    { &input_setup_factory, 'C', "setup" },
    { &input_setup_factory, 'L', "setup" },
    { &input_site_factory, 'C', "site" },
    { &input_site_factory, 'L', "site" },
    { &input_siteparameters_factory, 'C', "siteparameters" },
    { &input_siteparameters_factory, 'L', "siteparameters" },
    { &input_soilparameters_factory, 'C', "soilparameters" },
    { &input_soilparameters_factory, 'L', "soilparameters" },
    { &input_speciesparameters_factory, 'C', "speciesparameters" },
    { &input_speciesparameters_factory, 'L', "speciesparameters" },

    { &input_soillayers_factory, 'C', "soillayers" },
    { &input_soillayers_factory, 'L', "soillayers" },
    { &input_species_factory, 'C', "species" },
    { &input_species_factory, 'L', "species" },

    { NULL, '-', NULL } /* sentinel */
};

#include  "string/cbm_string.h"
ldndc::input_class_factory_base_t< ldndc::input_class_client_base_t,
    ldndc::input_class_global_client_base_t > const *
ldndc::find_input_class_factory( char  _kind, char const *  _role)
{
    if (( _kind!='C' && _kind!='L') || !_role)
        { return  NULL; }

    int  r = 0;
    while ( __cbm_input_factories[r].factory)
    {
        if (( __cbm_input_factories[r].kind == _kind)
                && cbm::is_equal( __cbm_input_factories[r].role, _role))
            { return __cbm_input_factories[r].factory; }
        ++r;
    }
    return  NULL;
}

