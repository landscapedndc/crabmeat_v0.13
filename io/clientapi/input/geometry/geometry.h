/*!
 * @author
 *    steffen klatt (created on: feb 03, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_GEOMETRY_CLIENT_H_
#define  LDNDC_INPUT_GEOMETRY_CLIENT_H_

#include  "input/ic.h"
#include  "input/geometry/geometry-srv.h"

namespace  ldndc{ namespace  geometry
{
class  CBM_API  input_class_geometry_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(geometry,ICLASS_FLAG_NONE)
    public:
        input_class_geometry_t()
            : input_class_client_base_t( invalid_lid), geometry_( NULL)
        { }
        input_class_geometry_t( lid_t const &  _id)
            : input_class_client_base_t( _id), geometry_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(geometry);

            /* mark as initialized */
            this->set_initialized();

            return  LDNDC_ERR_OK;
        }

        size_t  vertices_cnt() const
        {
            return  this->geometry_->vertices_cnt();
        }
        lerr_t  get_vertex(
                cbm::geom_coord_t *  _vertex,
                size_t  _index)
        {
            return  this->geometry_->get_vertex( _vertex, _index);
        }

    private:
        input_class_geometry_srv_t const *  geometry_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  geometry
{
class  CBM_API  input_class_geometry_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(geometry,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_GEOMETRY_CLIENT_H_  */

