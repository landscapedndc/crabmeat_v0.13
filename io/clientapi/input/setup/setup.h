/*!
 * @brief
 *    relay object for main controls for kernel (client side)
 *
 * @author
 *    steffen klatt (created on: mar 30, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SETUP_CLIENT_H_
#define  LDNDC_INPUT_SETUP_CLIENT_H_

#include  "input/ic.h"
#include  "input/setup/setup-srv.h"

namespace  ldndc{ namespace  setup
{
class  CBM_API  input_class_setup_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(setup,ICLASS_FLAG_NONE)
    public:
        input_class_setup_t()
            : input_class_client_base_t( invalid_lid), setup_( NULL)
        { }
        input_class_setup_t( lid_t const &  _id)
            : input_class_client_base_t( _id), setup_( NULL)
        { }


        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
            {
                crabmeat_assert( this->setup_);
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(setup);

            /* mark as initialized */
            this->set_initialized();

            return  LDNDC_ERR_OK;
        }


        /*!
         * @brief
         *    return arbitrary site name
         */
        char const *  site_name() const
            { return  this->setup_->site_name(); }

        lerr_t  section_data( char **  _buffer,
                    char const *  _section) const
            { return  this->setup_->section_data( _buffer, NULL, _section); }

        /* "meta" info */
        double  x() const { return  this->setup_->info()->x; }
        double  dx() const { return  this->setup_->info()->dx; }
        double  y() const { return  this->setup_->info()->y; }
        double  dy() const { return  this->setup_->info()->dy; }
        double  z() const { return  this->setup_->info()->z; }
        double  dz() const { return  this->setup_->info()->dz; }

        double  canopylayers() const { return  this->setup_->info()->canopylayers; }
        double  soillayers() const { return  this->setup_->info()->soillayers; }

        double  elevation() const { return  this->setup_->info()->elevation; }
        double  latitude() const { return  this->setup_->info()->latitude; }
        double  longitude() const { return  this->setup_->info()->longitude; }

        double  area() const { return  this->setup_->info()->area; }
        double  volume() const { return  this->setup_->info()->volume; }

        double  timezone() const { return  this->setup_->info()->timezone; }
        double  aspect() const { return  this->setup_->info()->aspect; }
        double  slope() const { return  this->setup_->info()->slope; }

        size_t  get_number_of_neighbors() const { return  this->setup_->get_number_of_neighbors(); }
        cbm::neighbor_t const *  get_neighbors() const { return  this->setup_->get_neighbors(); }
        cbm::geo_orientations_t  get_mooreboundaries() const { return  this->setup_->get_mooreboundaries(); }

        /*!
         * @brief
         *      writes block descriptor to given buffer
         *      and type
         */        
        lid_t  block_consume( cbm::source_descriptor_t *  _source_info,
                char const *  _type, int *  _given = NULL)
        const
        {
            return  this->setup_->block_consume( _source_info, _type, _given);
        }
        /*!
         * @brief
         *      writes block descriptor to given buffer
         *      and type
         */        
        lid_t  block_produce( cbm::source_descriptor_t *  _source_info,
                char const *  _type, int *  _given = NULL)
        const
        {
            return  this->setup_->block_produce( _source_info, _type, _given);
        }

        std::string  source_id() const
        {
            cbm::source_descriptor_t  ksource;
            this->block_consume( &ksource, "setup");
            return  ksource.source_identifier;
        }

        bool  is_active() const
            { return  this->setup_->is_active(); }
        bool  is_quiet() const
            { return  this->setup_->is_quiet(); }
        bool  dispatch_on_any() const
            { return  this->setup_->dispatch_on_any(); }
        bool  checkpoint_create() const
            { return  this->setup_->checkpoint_create(); }
        bool  checkpoint_restore() const
            { return  this->setup_->checkpoint_restore(); }

    private:
        input_class_setup_srv_t const *  setup_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  setup
{
class  CBM_API  input_class_setup_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(setup,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SETUP_CLIENT_H_  */

