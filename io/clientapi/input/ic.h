/*!
 * @brief
 *    abstract base classes for input classes
 *    (those read only input stream containers)
 *
 * @author
 *    steffen klatt (created on: jul 14, 2011)
 */

#ifndef  LDNDC_INPUT_CLASS_CLIENT_H_
#define  LDNDC_INPUT_CLASS_CLIENT_H_

#include  "crabmeat-common.h"
#include  "input/ic-env.h"

namespace ldndc {
class  source_handle_t;

/* (abstract) base class for client input classes */
class  CBM_API  input_class_client_base_t
        :  public  input_class_base_t,  public  cbm::client_object_t
{
    public:
        virtual  ~input_class_client_base_t() = 0;

        /*!
         * @brief
         *    initializes internal state
         *
         * @note
         *    do not keep reference to core input object;
         *    it will be destroyed (after input_exhausted
         *    returned true)!
         */
        virtual  lerr_t  initialize( source_handle_t const *) = 0;

    protected:
        input_class_client_base_t( lid_t const &);
};
}


#define  LDNDC_INPUT_CLASS_DECL(__class__,__flags__) \
    LDNDC_INPUT_CLASS_DECL_( \
        TOKENPASTE3(input_class_,__class__,_t), __class__, __flags__) \
    public: \
        typedef input_class_client_base_t base_t;

#define  LDNDC_INPUT_CLASS_DEFN(__class__) \
    LDNDC_INPUT_CLASS_DEFN_(TOKENPASTE3(input_class_,__class__,_t))


#include  "io/source-handle.h"
#define  set_input_class_client_dataprovider(__dataprovider_iclass__) \
{ \
    crabmeat_assert( _source_handle); \
    this->__dataprovider_iclass__##_ = \
        _source_handle->data_provider< TOKENPASTE3(input_class_,__dataprovider_iclass__,_srv_t) >(); \
    if ( !this->__dataprovider_iclass__##_) \
        { return  LDNDC_ERR_INVALID_ARGUMENT; } \
    crabmeat_assert(cbm_is_dic(this->__dataprovider_iclass__##_->input_class_type()) || \
            ( this->object_id() == this->__dataprovider_iclass__##_->object_id())); \
} \
CRABMEAT_FIX_UNUSED(_source_handle)/*;*/


#define  set_input_global_class_client_dataprovider(__dataprovider_iclass__) \
{ \
    crabmeat_assert( _input_source); \
    this->__dataprovider_iclass__##_ = \
        _input_source->data_provider< TOKENPASTE3(input_class_,__dataprovider_iclass__,_global_srv_t) >(); \
    crabmeat_assert( this->__dataprovider_iclass__##_); \
    crabmeat_assert( this->input_class_type() == this->__dataprovider_iclass__##_->input_class_type()); \
} \
CRABMEAT_FIX_UNUSED(_input_source)/*;*/



#endif  /*  !LDNDC_INPUT_CLASS_CLIENT_H_  */

