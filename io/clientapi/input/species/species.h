/*!
 * @brief
 *    container for species living during simulation
 *
 * @author
 *    steffen klatt (created on: dec 04, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_SPECIES_CLIENT_H_
#define  LDNDC_INPUT_SPECIES_CLIENT_H_

#include  "input/ic.h"
#include  "input/species/species-srv.h"

namespace  ldndc{ namespace  species
{
class  CBM_API  input_class_species_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(species,ICLASS_FLAG_STATE)
    public:
        input_class_species_t()
                : input_class_client_base_t( invalid_lid), species_( NULL)
        { }
        input_class_species_t( lid_t const &  _id)
                : input_class_client_base_t( _id), species_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
                { return  LDNDC_ERR_OK; }
            set_input_class_client_dataprovider(species);

            /* mark as initialized */
            this->set_initialized();

            return  LDNDC_ERR_OK;
        }

        replace_index_t const &  replace_species() const
            { return  this->species_->replace_species(); }

    public:
        /* DEPRECATED */
        species_t const &  operator[]( size_t  _k) const
            { return  this->species_->operator[]( _k); }
        species_t const *  get_species( char const *  _name) const
        {
            size_t const  slot = this->species_->get_index( _name);
            if ( slot == invalid_size)
                { return  NULL; }
            return  &this->species_->operator[]( slot);
        }

        bool  is_family( char const *  _type, char const *  _family) const
            { return  this->species_->is_family( _type, _family); }

    private:
        input_class_species_srv_t const *  species_;
};

}}

#include  "input/ic-global.h"
namespace  ldndc{ namespace  species
{
class  CBM_API  input_class_species_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(species,ICLASS_FLAG_STATE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            set_initialized();
            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SPECIES_CLIENT_H_  */

