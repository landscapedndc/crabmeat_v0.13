/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: dec 01, 2014)
 */

#ifndef  LDNDC_INPUT_CHECKPOINT_CLIENT_H_
#define  LDNDC_INPUT_CHECKPOINT_CLIENT_H_

#include  "input/ic.h"
#include  "input/checkpoint/checkpoint-srv.h"


namespace  ldndc{ namespace  checkpoint
{
class  CBM_API  input_class_checkpoint_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(checkpoint,ICLASS_FLAG_STATE)
    public:
        input_class_checkpoint_t()
            : input_class_client_base_t( invalid_lid), checkpoint_( NULL)
        { }

        input_class_checkpoint_t( lid_t const &  _id)
            : input_class_client_base_t( _id), checkpoint_( NULL)
        { }


        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            crabmeat_assert( _source_handle);
            this->checkpoint_ = _source_handle->data_provider< input_class_checkpoint_srv_t >();
            crabmeat_assert( this->checkpoint_ && ( this->object_id() == this->checkpoint_->object_id()));

            /* mark as initialized */
            this->set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        lerr_t  retrieve_record(
                checkpoint_buffer_t *  _buf)
        const
        {
            if ( !_buf)
            {
                return  LDNDC_ERR_RUNTIME_ERROR;
            }
            return  this->checkpoint_->retrieve_record( _buf);
        }

    private:
        input_class_checkpoint_srv_t const *  checkpoint_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  checkpoint
{
class  CBM_API  input_class_checkpoint_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(checkpoint,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            this->set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_CHECKPOINT_CLIENT_H_  */

