/*!
 * @brief
 *    container class for one complete soil
 *    and humus parameters set
 * 
 * @author
 *      steffen klatt (created on oct 09, 2011)
 */


#ifndef  LDNDC_INPUT_SOILPARAMETERS_CLIENT_H_
#define  LDNDC_INPUT_SOILPARAMETERS_CLIENT_H_

#include  "input/ic.h"
#include  "input/soilparameters/soilparameters-srv.h"
#ifdef  __HUMUS_SOIL_PARAMETER_VALUE__
#  undef  __HUMUS_SOIL_PARAMETER_VALUE__
#endif
#define  __HUMUS_SOIL_PARAMETER_VALUE__(__p_idx__,__type__,__TYPE__,__name__,__dtype__,__buf__)  this->soilparameters_->__TYPE__ ##_##__name__( __p_idx__)

namespace  ldndc{ namespace  soilparameters
{
class  CBM_API  input_class_soilparameters_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(soilparameters,ICLASS_FLAG_NONE)
    public:
        input_class_soilparameters_t()
            : input_class_client_base_t( invalid_lid),
              soilparameters_( NULL)
        { }
        input_class_soilparameters_t( lid_t const &  _id)
            : input_class_client_base_t( _id),
              soilparameters_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(soilparameters);

            /* mark as initialized */
            this->set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        /* expands to getters for all soil parameters */
        SOILPARAMETERS_GETTERS
        /* expands to getters for all humus parameters */
        HUMUSPARAMETERS_GETTERS

    public:
        size_t  parameterized_soil_cnt() const
        {
            return  this->soilparameters_->parameterized_soil_cnt();
        }
        std::string  soilparametervalue_as_string(
                char const *  _parname, char const *  _soilname)
        const
        {
            return  this->soilparameters_->soilparametervalue_as_string( _parname, _soilname);
        }
        size_t  parameterized_humus_cnt() const
        {
            return  this->soilparameters_->parameterized_humus_cnt();
        }
        std::string  humusparametervalue_as_string(
                char const *  _parname, char const *  _humusname)
        const
        {
            return  this->soilparameters_->humusparametervalue_as_string( _parname, _humusname);
        }
    private:
        input_class_soilparameters_srv_t const *  soilparameters_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  soilparameters
{
class  CBM_API  input_class_soilparameters_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(soilparameters,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SOILPARAMETERS_CLIENT_H_  */

