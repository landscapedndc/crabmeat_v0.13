/*!
 * @brief
 *    container class for site parameters
 *
 * @author
 *    Steffen Klatt,
 *    Edwin Haas,
 */

#ifndef  LDNDC_INPUT_SITEPARAMS_CLIENT_H_
#define  LDNDC_INPUT_SITEPARAMS_CLIENT_H_

#include  "input/ic.h"
#include  "input/siteparameters/siteparameters-srv.h"
#ifdef  __SITEPARAMETER_VALUE__
#  undef  __SITEPARAMETER_VALUE__
#endif
#define  __SITEPARAMETER_VALUE__(__name__,__type__)  this->siteparameters_->__name__()

namespace  ldndc{ namespace  siteparameters
{
class  CBM_API  input_class_siteparameters_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(siteparameters,ICLASS_FLAG_NONE)
    public:
        input_class_siteparameters_t()
            : input_class_client_base_t( invalid_lid), siteparameters_( NULL)
        { }
        input_class_siteparameters_t( lid_t const &  _id)
            : input_class_client_base_t( _id), siteparameters_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(siteparameters);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }


        /* expands to getters for all site parameters */
        SITEPARAMETERS_GETTERS

    public:
        siteparameter_t::float_type  RCNRL()
        const
        {
            return  this->siteparameters_->RCNRL();
        }
        siteparameter_t::float_type  CNAORG()
        const
        {
            return  this->siteparameters_->CNAORG();
        }

    public:
        std::string  parametervalue_as_string(
                char const *  _name) const
        {
            return  this->siteparameters_->parametervalue_as_string( _name);
        }

    private:
        input_class_siteparameters_srv_t const *  siteparameters_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  siteparameters
{
class  CBM_API  input_class_siteparameters_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(siteparameters,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_SITEPARAMS_CLIENT_H_  */

