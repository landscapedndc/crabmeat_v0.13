/*!
 * @brief
 *    high level access structure to air chemistry
 *    input streams (client interface)
 *
 * @author
 *    Steffen Klatt (created on: jul 8, 2010),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_AIRCHEMISTRY_CLIENT_H_
#define  LDNDC_INPUT_AIRCHEMISTRY_CLIENT_H_

#include  "input/ic.h"
#include  "input/airchemistry/airchemistry-srv.h"

#define  AIRCHEMISTRY_RECORD_ITEM_CLIENT(__rec_item__) \
    inline \
    ldndc::airchemistry::record::item_type  __rec_item__##_subday( \
            cbm::sclock_t const &  _clock) \
    const \
    { \
        return  this->airchemistry_->__rec_item__( \
                _clock.cycles(), \
                (int)_clock.time_resolution()); \
    } \
    inline \
    ldndc::airchemistry::record::item_type  __rec_item__##_day( \
            cbm::sclock_t const &  _clock) \
    const \
    { \
        return  this->airchemistry_->__rec_item__( \
                _clock.cycles(), 1); \
    }


namespace  ldndc{ namespace airchemistry
{

class  CBM_API  input_class_airchemistry_t  :  public  input_class_client_base_t, public  record
{
    LDNDC_INPUT_CLASS_DECL(airchemistry,ICLASS_FLAG_STATE)
    public:
        char const **  record_item_names()
        const
        {
            return  RECORD_ITEM_NAMES;
        }

    public:
        input_class_airchemistry_t()
                : input_class_client_base_t( invalid_lid),
                record(), airchemistry_( NULL)
        { }
        input_class_airchemistry_t( lid_t const &  _id)
                : input_class_client_base_t( _id),
                record(), airchemistry_( NULL)
        { }

        lerr_t  initialize( source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(airchemistry);

            /* mark as initialized */
            set_initialized();
            return  LDNDC_ERR_OK;
        }

        ltime_t const &  get_time()
        const
        {
            return  this->airchemistry_->get_time();
        }

        void  get_record(
            record::item_type *  _buffer, size_t  _timestep)
        const
        {
            this->airchemistry_->get_record( _buffer, _timestep);
        }



    public:
        airchemistry_info_t const *  info()
        const
        {
            return  this->airchemistry_->info();
        }
        double  average_ch4() const { return  this->info()->ch4; }
        double  average_co2() const { return  this->info()->co2; }
        double  average_nh3() const { return  this->info()->nh3; }
        double  average_nh4() const { return  this->info()->nh4; }
        double  average_nh4dry() const { return  this->info()->nh4dry; }
        double  average_no() const { return  this->info()->no; }
        double  average_no2() const { return  this->info()->no2; }
        double  average_no3() const { return  this->info()->no3; }
        double  average_no3dry() const { return  this->info()->no3dry; }
        double  average_o2() const { return  this->info()->o2; }
        double  average_o3() const { return  this->info()->o3; }


    public:

    /* concentrations above the canopy (ppm) */
    /*! methane concentration above the canopy (ppm). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_ch4)
    /*! carbon dioxide concentration above the canopy (ppm). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_co2)
    /*! oxygen concentration above the canopy (m^3:m^-3). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_o2)
    /*! ozone concentration above the canopy (ppm). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_o3)

    /*! ammoniak concentration above the canopy (ppm). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_nh3)
    /*! ammonium concentration in rain (g m-3) */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_nh4)
    /*!  */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_nh4dry)
    /*! nitrous oxide concentration above the canopy (ppm). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_no)
    /*! nitrous dioxide concentration above the canopy (ppm). */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_no2)
    /*! nitrate concentration in rain (g m-3) */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_no3)
    /*!  */
    AIRCHEMISTRY_RECORD_ITEM_CLIENT(conc_no3dry)

    private:
        input_class_airchemistry_srv_t const *  airchemistry_;
};
#undef  AIRCHEMISTRY_RECORD_ITEM_CLIENT
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  airchemistry
{
class  CBM_API  input_class_airchemistry_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(airchemistry,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                input_source_t const *  _input_source)
        {
            if ( is_initialized())
                { return  LDNDC_ERR_OK; }
            set_input_global_class_client_dataprovider(airchemistry);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        bool  has_time()
            const { return  true; }

        ltime_t const &  get_time()
            const { return  this->airchemistry_->get_time(); }

    private:
        input_class_airchemistry_global_srv_t const *  airchemistry_;
};
}}

#endif  /*  !LDNDC_INPUT_AIRCHEMISTRY_CLIENT_H_  */

