/*!
 * @brief
 *    high level access structure to climate
 *    input streams
 *
 * @author
 *    Steffen Klatt (created on: jul 8, 2010),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_CLIMATE_CLIENT_H_
#define  LDNDC_INPUT_CLIMATE_CLIENT_H_

#include  "input/ic.h"
#include  "input/climate/climate-srv.h"

#define  CLIMATE_RECORD_ITEM_CLIENT(__rec_item__) \
    inline \
    ldndc::climate::record::item_type  __rec_item__##_subday( \
            cbm::sclock_t const &  _clock) \
    const \
    { \
        return  this->climate_->__rec_item__( \
                _clock.cycles(), \
                (int)_clock.time_resolution()); \
    } \
    inline \
    ldndc::climate::record::item_type  __rec_item__##_day( \
            cbm::sclock_t const &  _clock) \
    const \
    { \
        return  this->climate_->__rec_item__( \
                _clock.cycles(), 1); \
    }


namespace  ldndc{ namespace  climate
{
/*!
 * @brief
 *    provides consistent access to climate input data
 */
class  CBM_API  input_class_climate_t  :  public  input_class_client_base_t, public  ldndc::climate::record
{
    LDNDC_INPUT_CLASS_DECL(climate,ICLASS_FLAG_STATE)
    public:
        char const **  record_item_names()
        const
        {
            return  ldndc::climate::RECORD_ITEM_NAMES;
        }

    public:
        input_class_climate_t()
                : input_class_client_base_t( invalid_lid),
                  ldndc::climate::record(), climate_( NULL)
        { }
        input_class_climate_t( lid_t const &  _id)
                : input_class_client_base_t( _id),
                  ldndc::climate::record(), climate_( NULL)
        { }
        
        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(climate);

            /* mark as initialized */
            this->set_initialized();
            
            return  LDNDC_ERR_OK;
        }

        ltime_t const &  get_time()
        const
        {
            return  this->climate_->get_time();
        }

        void  get_record(
                item_type *  _buffer,
                cbm::sclock_t const &  _clk)
        const
        {
            this->climate_->get_record( _buffer, _clk);
        }

        climate_info_t const *  station_info()
        const
        {
            return  this->climate_->info();
        }
        /*! climate station's elevation  [m] */
        double  elevation() const { return  this->climate_->info()->elevation; }
        /*! climate station's latitude  [degree] */
        double  latitude() const { return  this->climate_->info()->latitude; }
        /*! climate station's longitude  [degree] */
        double  longitude() const { return  this->climate_->info()->longitude; }

        /*! average cloudiness  [%] */
        double  cloudiness() const { return  this->climate_->info()->cloudiness; }
        /*! average rainfall intensity  [%] */
        double  rainfall_intensity() const { return  this->climate_->info()->rainfall_intensity; }

        /*! average wind speed  [m/s] */
        double  windspeed() const { return  this->climate_->info()->windspeed; }
        /*! annual average temperature  [oC] */
        double  annual_temperature_average() const { return  this->climate_->info()->temp; }
        /*! annual average temperature amplitude  [oC] */
        double  annual_temperature_amplitude() const { return  this->climate_->info()->temp_amplitude; }
        /*! annual average precipitation  [mm] */
        double  annual_precipitation() const { return  this->climate_->info()->precip_sum; }

    public:
                /*! air pressure */
        CLIMATE_RECORD_ITEM_CLIENT(air_pressure)
                /*! extraterrestrial radiation */
        CLIMATE_RECORD_ITEM_CLIENT(ex_rad)
                /*! longwave radiation */
        CLIMATE_RECORD_ITEM_CLIENT(long_rad)
                /*! precipitation */
        CLIMATE_RECORD_ITEM_CLIENT(precip)
                /*! average temperature */
        CLIMATE_RECORD_ITEM_CLIENT(temp_avg)
                /*! maximum temperature */
        CLIMATE_RECORD_ITEM_CLIENT(temp_max)
                /*! minimum temperature */
        CLIMATE_RECORD_ITEM_CLIENT(temp_min)
                /*! relative humidity */
        CLIMATE_RECORD_ITEM_CLIENT(rel_humidity)
                /*! vapor pressure deficit */
        CLIMATE_RECORD_ITEM_CLIENT(vpd)
                /*! wind speed */
        CLIMATE_RECORD_ITEM_CLIENT(wind_speed)

    private:
        input_class_climate_srv_t const *  climate_;
};
}}



#include  "input/ic-global.h"
namespace  ldndc{ namespace  climate
{
class  CBM_API  input_class_climate_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(climate,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  _input_source)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_global_class_client_dataprovider(climate);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        bool  has_time()
                const
                {
                        return  true;
                }

        ltime_t const &  get_time()
        const
        {
                   return  this->climate_->get_time();
        }

    private:
        input_class_climate_global_srv_t const *  climate_;
};
}}

#endif  /*  !LDNDC_INPUT_CLIMATE_CLIENT_H_  */

