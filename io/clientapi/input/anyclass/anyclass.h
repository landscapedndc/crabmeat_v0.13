/*!
 * @brief
 *
 * @author
 *    steffen klatt (created on: mar 20, 2014),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_ANYCLASS_CLIENT_H_
#define  LDNDC_INPUT_ANYCLASS_CLIENT_H_

#include  "input/ic.h"
#include  "input/anyclass/anyclass-srv.h"
#include  "io/outputtypes.h"


namespace  ldndc{ namespace  anyclass
{
class  CBM_API  input_class_anyclass_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(anyclass,ICLASS_FLAG_STATE)
    public:
        input_class_anyclass_t()
            : input_class_client_base_t( invalid_lid), anyclass_( NULL)
        { }

        input_class_anyclass_t( lid_t const &  _id)
            : input_class_client_base_t( _id), anyclass_( NULL)
        { }


        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            crabmeat_assert( _source_handle);
            this->anyclass_ = _source_handle->data_provider< input_class_anyclass_srv_t >();
            crabmeat_assert( this->anyclass_ && ( this->object_id() == this->anyclass_->object_id()));

            /* mark as initialized */
            this->set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        int  retrieve_data_record(
                void ***  _buffer, sink_entity_layout_t const *  _meta)
        const
        {
            if ( !_buffer)
            {
                return  -1;
            }

            if ( !*_buffer)
            {
                void **  buf = NULL;
                ldndc::malloc_from_layout( &buf, &_meta->layout);
                if ( !buf)
                {
                    return  -1;
                }
                *_buffer = buf;
            }
            return  this->anyclass_->retrieve_data_record( *_buffer, _meta);
        }
        void  free_data_record(
                void **  _buffer, sink_layout_t const *  _layout)
        const
        {
            ldndc::free_from_layout( _buffer, _layout);
        }

    private:
        input_class_anyclass_srv_t const *  anyclass_;
};
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  anyclass
{
class  CBM_API  input_class_anyclass_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(anyclass,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  /*input object*/)
        {
            /* guard */
            if ( this->is_initialized())
            {
                return  LDNDC_ERR_OK;
            }

            // nothing here

            this->set_initialized();

            return  LDNDC_ERR_OK;
        }
};
}}

#endif  /*  !LDNDC_INPUT_ANYCLASS_CLIENT_H_  */

