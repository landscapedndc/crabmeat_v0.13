/*!
 * @brief
 *    container for (management) event input data
 *
 * @author
 *    steffen klatt (created on: apr 01, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_INPUT_EVENT_CLIENT_H_
#define  LDNDC_INPUT_EVENT_CLIENT_H_

#include  "input/ic.h"
#include  "input/event/event-srv.h"

namespace  ldndc{ namespace  event
{

typedef events_view_t< event::Event > EventsView;
class  CBM_API  input_class_event_t  :  public  input_class_client_base_t
{
    LDNDC_INPUT_CLASS_DECL(event,ICLASS_FLAG_STATE)
    public:
        input_class_event_t()
                : input_class_client_base_t( invalid_lid), event_( NULL)
        { }
        input_class_event_t( lid_t const &  _id)
                : input_class_client_base_t( _id), event_( NULL)
        { }

        lerr_t  initialize(
                ldndc::source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
                { return  LDNDC_ERR_OK; }
            set_input_class_client_dataprovider(event);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }

        EventsView  events_view() const
            { return  this->event_->events_view< event::Event >(); }

    public:
        size_t  get_event_handles_cnt() const
            { return  this->event_->get_event_handles_cnt(); }
        lerr_t  get_event_handles(
                event_handle_t *  _buf, size_t  _n) const
            { return  this->event_->get_event_handles( _buf, _n); }

    private:
        input_class_event_srv_t const *  event_;
};
}}



#include  "input/ic-global.h"
namespace  ldndc{ namespace  event
{
class  CBM_API  input_class_event_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(event,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                ldndc::input_source_t const *  _input_source)
        {
            /* guard */
            if ( is_initialized())
                { return  LDNDC_ERR_OK; }
            set_input_global_class_client_dataprovider(event);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        bool  has_time() const
            { return  true; }

        ltime_t const &  get_time() const
            { return  this->event_->get_time(); }

    private:
        input_class_event_global_srv_t const *  event_;
};
}}

#endif  /*  !LDNDC_INPUT_EVENT_CLIENT_H_  */

