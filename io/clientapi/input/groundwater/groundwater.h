/*!
 * @brief
 *    high level access structure to groundwater
 *    input streams (client interface)
 *
 * @author
 *    Steffen Klatt (created on: may 29, 2015),
 *    Edwin Haas
 */

#ifndef  LDNDC_INPUT_GROUNDWATER_CLIENT_H_
#define  LDNDC_INPUT_GROUNDWATER_CLIENT_H_

#include  "input/ic.h"
#include  "input/groundwater/groundwater-srv.h"

#define  GROUNDWATER_RECORD_ITEM_CLIENT(__rec_item__) \
    inline \
    ldndc::groundwater::record::item_type  __rec_item__##_subday( \
            cbm::sclock_t const &  _clock) \
    const \
    { \
        return  this->groundwater_->__rec_item__( \
                _clock.cycles(), \
                (int)_clock.time_resolution()); \
    } \
    inline \
    ldndc::groundwater::record::item_type  __rec_item__##_day(      \
            cbm::sclock_t const &  _clock) \
    const \
    { \
        return  this->groundwater_->__rec_item__( \
                _clock.cycles(), 1); \
    }


namespace  ldndc{ namespace groundwater
{

class  CBM_API  input_class_groundwater_t  :  public  input_class_client_base_t, public  record
{
    LDNDC_INPUT_CLASS_DECL(groundwater,ICLASS_FLAG_STATE)
    public:
        char const **  record_item_names()
        const
        {
            return  RECORD_ITEM_NAMES;
        }

    public:
        input_class_groundwater_t()
            : input_class_client_base_t( invalid_lid),
              record(), groundwater_( NULL)
        { }
        input_class_groundwater_t( lid_t const &  _id)
            : input_class_client_base_t( _id),
              record(), groundwater_( NULL)
        { }

        lerr_t  initialize(
                source_handle_t const *  _source_handle)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_class_client_dataprovider(groundwater);

            /* mark as initialized */
            set_initialized();
            return  LDNDC_ERR_OK;
        }

        ltime_t const &  get_time()
        const
        {
            return  this->groundwater_->get_time();
        }

        void  get_record(
                record::item_type *  _buffer, size_t  _timestep)
        const
        {
            this->groundwater_->get_record( _buffer, _timestep);
        }

    public:
        groundwater_info_t const *  info()
        const
        {
            return  this->groundwater_->info();
        }
        double  average_watertable() const { return  this->info()->watertable; }
        double  average_no3() const { return  this->info()->no3; }


    public:

        /* concentrations above the canopy (ppm) */
        GROUNDWATER_RECORD_ITEM_CLIENT(watertable)
        GROUNDWATER_RECORD_ITEM_CLIENT(no3)

    private:
            input_class_groundwater_srv_t const *  groundwater_;
};
#undef  GROUNDWATER_RECORD_ITEM_CLIENT
}}


#include  "input/ic-global.h"
namespace  ldndc{ namespace  groundwater
{
class  CBM_API  input_class_groundwater_global_t  :  public  input_class_global_client_base_t
{
    LDNDC_INPUT_GLOBAL_CLASS_DECL(groundwater,ICLASS_FLAG_NONE)
    public:
        lerr_t  initialize(
                input_source_t const *  _input_source)
        {
            if ( is_initialized())
            {
                return  LDNDC_ERR_OK;
            }
            set_input_global_class_client_dataprovider(groundwater);

            /* mark as initialized */
            set_initialized();

            return  LDNDC_ERR_OK;
        }

    public:
        bool  has_time()
        const
        {
            return  true;
        }

        ltime_t const &  get_time()
        const
        {
            return  this->groundwater_->get_time();
        }

    private:
        input_class_groundwater_global_srv_t const *  groundwater_;
};
}}

#endif  /*  !LDNDC_INPUT_GROUNDWATER_CLIENT_H_  */

