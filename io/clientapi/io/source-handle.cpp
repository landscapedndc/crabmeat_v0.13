/*!
 * @brief
 *    ...
 *
 * @author
 *    steffen klatt (created on: oct 09, 2011),
 *    edwin haas
 */

#include  "io/source-handle.h"

ldndc::source_handle_t::source_handle_t(
        input_class_srv_base_t const *  _ic_srv)
        : ic_srv_( _ic_srv)
{
}

ldndc::source_handle_t::~source_handle_t()
{
}


ldndc::input_source_t::input_source_t(
        input_class_global_srv_base_t const *  _ic_srv)
        : ic_srv_( _ic_srv)
{
}

ldndc::input_source_t::~input_source_t()
{
}

