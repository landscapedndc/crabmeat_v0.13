/*!
 * @brief
 *    a container class for core data providers
 *
 * @authors
 *    steffen klatt (created on: oct 09, 2011),
 *    edwin haas
 */

#ifndef  LDNDC_SOURCEHANDLE_MONOLITHIC_H_
#define  LDNDC_SOURCEHANDLE_MONOLITHIC_H_

#include  "crabmeat-common.h"

namespace ldndc {
class  input_class_srv_base_t;

class CBM_API source_handle_t
{
    public:
        source_handle_t(
                input_class_srv_base_t const *);

        ~source_handle_t();

        template < typename  _D >
        _D const *  data_provider()
        const
        {
            return  dynamic_cast< _D const * >( this->ic_srv_);
        }

    private:
        /* data provider (_not_ owner) */
        input_class_srv_base_t const *  ic_srv_;
};

}


namespace ldndc {
class  input_class_global_srv_base_t;

class  CBM_API  input_source_t
{
    public:
        input_source_t(
                input_class_global_srv_base_t const *);

        ~input_source_t();

        template < typename  _D >
        _D const *  data_provider()
        const
        {
            return  static_cast< _D const * >( ic_srv_);
        }

    private:
        /* data provider (_not_ owner) */
        input_class_global_srv_base_t const *  ic_srv_;
};

}


#endif  /*  !LDNDC_SOURCEHANDLE_MONOLITHIC_H_  */

