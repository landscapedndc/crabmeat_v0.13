/*!
 * @brief
 *    client-side interface to sink objects
 *
 * @authors
 *    steffen klatt (created on: nov 03, 2013),
 *    edwin haas
 */

#ifndef  LDNDC_IO_SINKHANDLE_MONOLITHIC_H_
#define  LDNDC_IO_SINKHANDLE_MONOLITHIC_H_

#include  "crabmeat-common.h"

#include  "io/outputtypes.h"
#include  "io/sink-handle-srv.h"

namespace ldndc {
class  CBM_API  sink_handle_t  :  public  cbm::client_object_t
{
    LDNDC_CLIENT_OBJECT(sink_handle_t)
    public:
        sink_handle_t();
        sink_handle_t(
                lid_t const & /*object id*/);

        ~sink_handle_t();

        /* aliases */
        lid_t  sink_descriptor() const;
        lreceiver_descriptor_t  receiver_descriptor() const;

    public:
        lerr_t  status() const;

        lerr_t  flush_stream();


        /* fixed predefined layout */
        lerr_t  set_fixed_layout(
                sink_record_meta_t const * /*select record meta info*/,
                sink_entity_layout_t const * /*entities&layout*/);
        lerr_t  write_fixed_record(
                sink_client_t const * /*client identifier*/,
                sink_fixed_record_t const * /*record*/);
        lerr_t  write_fixed_zero_record(
                sink_client_t const * /*client identifier*/,
                sink_fixed_record_t const * /*record*/);

        /* on-the-fly layout */
        lerr_t  write_nonfixed_record(
                sink_client_t const * /*client identifier*/,
                sink_nonfixed_record_t const * /*record*/);

        /* public layout, i.e. layout known by receiver */
        lerr_t  write_public_record(
                sink_client_t const * /*client identifier*/,
                sink_public_record_t const * /*record*/);

    private:
        void  update_sequence_number();

    public:
        bool  is_acquired() const;
        void  set_acquired(
                sink_handle_srv_t & /*server sink handle*/);

        bool  is_released() const;
        void  set_released();

        void  set_lclock(
                cbm::sclock_t const *);

    private:
        /* sink handle */
        sink_handle_srv_t  h_;

        struct
        {
            bool  released:1;
            bool  devnull:1;
        }  m;


    private:
        sink_record_sequence_t  rec_seq;
        lflags_t  useregs;
        lerr_t  write_record_(
                sink_client_t const * /*client identifier*/,
                sink_record_srv_t * /*server sink handle*/);
};
}


#endif  /*  !LDNDC_IO_SINKHANDLE_MONOLITHIC_H_  */

