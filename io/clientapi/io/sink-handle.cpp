/*!
 * @brief
 *    client-side interface to sink objects (implementation)
 *
 * @authors
 *    steffen klatt (created on: jan 15, 2014),
 *    edwin haas
 */

#include  "io/sink-handle.h"
#include  "time/cbm_time.h"
#include  "cbm_rtcfg.h"

namespace ldndc {
sink_handle_t::sink_handle_t()
    : cbm::client_object_t( invalid_lid),
          useregs( 0)
{
    this->m.released = 1;
    this->m.devnull = 0;
}
sink_handle_t::sink_handle_t(
        lid_t const &  _object_id)
    : cbm::client_object_t( _object_id),
          useregs( 0)
{
    this->m.released = 1;
    this->m.devnull = 0;
}

sink_handle_t::~sink_handle_t()
{
}

/* aliases */
lid_t
sink_handle_t::sink_descriptor()
const
{
    return  this->h_.sink_descriptor();
}
lreceiver_descriptor_t
sink_handle_t::receiver_descriptor()
const
{
    return  this->h_.receiver_descriptor();
}

lerr_t
sink_handle_t::status()
const
{
    lerr_t const  handle_status = this->h_.status();
    if ( handle_status == LDNDC_ERR_INVALID_ID)
    {
        return  LDNDC_ERR_ATTRIBUTE_NOT_FOUND;
    }
    return  ((this->state.ok == 0) || handle_status) ? LDNDC_ERR_FAIL : LDNDC_ERR_OK;
}

lerr_t
sink_handle_t::flush_stream()
{
    if ( this->m.devnull)
    {
        return  LDNDC_ERR_OK;
    }
    return  this->h_.flush_stream();
}

/* fixed predefined layout */
lerr_t
sink_handle_t::set_fixed_layout(
        sink_record_meta_t const *  _record_meta,
        sink_entity_layout_t const *  _layout)
{
    if ( this->m.devnull)
    {
        return  LDNDC_ERR_OK;
    }

    if ( _record_meta)
    {
        this->useregs = _record_meta->useregs;

        sink_record_meta_srv_t  record_meta( *_record_meta);
        return  this->h_.set_fixed_layout( &record_meta, _layout);
    }
    return  this->h_.set_fixed_layout( NULL, _layout);
}
lerr_t
sink_handle_t::write_fixed_record(
        sink_client_t const *  _client,
        sink_fixed_record_t const *  _record)
{
    sink_record_srv_t  r( *_record);

    /* inject use meta registers if not overwritten */
    if ( this->useregs)
    {
        r.meta.useregs = this->useregs;
    }

    return  this->write_record_( _client, &r);
}
lerr_t
sink_handle_t::write_fixed_zero_record(
        sink_client_t const *  _client,
        sink_fixed_record_t const *  _record)
{
    sink_record_srv_t  r( *_record);
    r.gap = ldndc::SINKGAP_ZERO;

    if ( this->useregs)
    {
        r.meta.useregs = this->useregs;
    }

    return  this->write_record_( _client, &r);
}

/* on-the-fly layout */
lerr_t
sink_handle_t::write_nonfixed_record(
        sink_client_t const *  _client,
        sink_nonfixed_record_t const *  _record)
{
    sink_record_srv_t  r( *_record);
    return  this->write_record_( _client, &r);
}

/* known layout, i.e. layout known by receiver */
lerr_t
sink_handle_t::write_public_record(
        sink_client_t const *  _client,
        sink_public_record_t const *  _record)
{
    sink_record_srv_t  r( *_record);
    return  this->write_record_( _client, &r);
}

lerr_t
sink_handle_t::write_record_(
        sink_client_t const *  _client,
        sink_record_srv_t *  _record)
{
    if ( this->m.devnull)
    {
        return  LDNDC_ERR_OK;
    }

    this->update_sequence_number();

    /* inject sequence number */
    _record->seq = &this->rec_seq;

    return  this->h_.write_record( _client, _record);
}


void
sink_handle_t::update_sequence_number()
{
    if ( this->m.devnull)
    {
        return;
    }

    if ( this->rec_seq.timestep != CBM_LibRuntimeConfig.clk->cycles())
    {
        this->rec_seq.timestep = CBM_LibRuntimeConfig.clk->cycles();
        this->rec_seq.records_in_timestep = 0;
        return;
    }
    this->rec_seq.records_in_timestep += 1;
}


bool
sink_handle_t::is_acquired()
const
{
    return  this->m.released == 0;
}
void
sink_handle_t::set_acquired(
        sink_handle_srv_t &  _h)
{
    this->set_object_id( _h.object_id());
    this->h_ = _h;

    this->state.ok = ( _h.status() == LDNDC_ERR_OK) ? 1 : 0;
    this->m.released = 0;
    this->m.devnull = _h.is_devnull();
}

bool
sink_handle_t::is_released()
const
{
    return  this->m.released == 1;
}
void
sink_handle_t::set_released()
{
    this->m.released = 1;
}

void
sink_handle_t::set_lclock(
        cbm::sclock_t const *  _clock)
{
    this->rec_seq.timestep = _clock->cycles()+1 /*plus one forces reset*/;
    this->rec_seq.records_in_timestep = 0;
}

}

