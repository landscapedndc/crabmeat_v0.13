/*!
 * @brief
 *    a container class for core readers
 *
 * @authors
 *    steffen klatt (created on: oct 09, 2011),
 *    edwin haas
 */

#ifndef  CBM_SOURCE_HANDLE_H_
#define  CBM_SOURCE_HANDLE_H_

#include  "crabmeat-common.h"

#include  "cbm_inputbroker.h"
#include  "time/cbm_time.h"

#include  "data-provider/provider.h"

namespace ldndc {

struct  source_attributes_t;
class  project_file_t;
class  input_class_global_srv_base_t;
class  CBM_API  source_handle_srv_base_t  :  public  cbm::server_object_t
{
    protected:
        source_handle_srv_base_t( lid_t const &);
    public:
        virtual  ~source_handle_srv_base_t() = 0;
        virtual  bool  need_reader() const = 0;

        virtual  lid_t const &  stream_id() const = 0;
        cbm::source_descriptor_t const *  get_source_descriptor() const;


    public:
        void  set_input_class_broker(
                ldndc::input_class_broker_t);

        input_class_global_srv_base_t const *  get_input_class_global(
                char const * /*type*/) const;

    public:
        ldndc::project_file_t const *  get_project_file() const;
        ldndc::source_attributes_t const *  get_stream_attributes(
                char const * /*type*/) const;

    public:
        ltime_t const &  get_reference_time() const;

    protected:
        ldndc::input_class_broker_t  ic_brkr;
};

class  CBM_API  source_handle_srv_t  :  public  source_handle_srv_base_t
{
    LDNDC_SERVER_OBJECT(source_handle_srv_t)
    public:
        source_handle_srv_t();
        source_handle_srv_t( lid_t const & /*id*/);
        ~source_handle_srv_t();

        bool  need_reader() const
            { return  true; }

        template < typename  _R >
            _R *  provider() const
                { return  dynamic_cast< _R * >( this->m_dataprovider); }

        template < typename  _R >
            _R *  landscape_provider() const
                { return  dynamic_cast< _R * >( this->m_landscapedataprovider); }

    public:
        void  release_provider();
        lerr_t  set_provider( iproc_provider_t * /*data provider*/,
                iproc_landscape_provider_t * /*landscape data provider*/);

    private:
        /* pointer to data provider */
        iproc_provider_t *  m_dataprovider;
        /* pointer to landscape data provider */
        iproc_landscape_provider_t *  m_landscapedataprovider;
        /* stream id (i.e. id of input stream this core belongs to) */
        lid_t  m_streamid;
    public:
        void  set_stream_id( lid_t const &  /*stream id*/);
        lid_t const &  stream_id() const
            { return  this->m_streamid; }
};


class  input_class_srv_base_t;
class  CBM_API  source_handle_noreader_srv_t  :  public  source_handle_srv_base_t
{
    LDNDC_SERVER_OBJECT(source_handle_noreader_srv_t)
    public:
        source_handle_noreader_srv_t();
        source_handle_noreader_srv_t( lid_t const &  /*object id*/);
        ~source_handle_noreader_srv_t();

        bool  need_reader() const
            { return  false; }

        input_class_srv_base_t const *  get_input_class(
                char const * /*type*/) const;

        lid_t const &  stream_id() const
            { return  invalid_lid; }
};

}

#endif  /*  !CBM_SOURCE_HANDLE_H_  */

