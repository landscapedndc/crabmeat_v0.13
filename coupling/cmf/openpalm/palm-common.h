/*!
 * @file
 *    coupling/cmf/openpalm/palm-common.h
 *
 * @brief
 *    foo
 */

#ifndef  LDNDC_CMF_COMMON_H_
#define  LDNDC_CMF_COMMON_H_

#define  MPI_MASTER_NODE  (0)

#define  opalm_name(__chrlist__,__name__)    sprintf( __chrlist__,"%s",__name__)/*;*/

extern "C"
{
    enum  meteo_record_e
    {
        M_T, M_TMIN, M_TMAX, M_VPD, M_WIND, M_RAD, M_PRECIP, /*M_DAYLENGTH,*/
        METEO_RECORD_SIZE
    };

    static int const  SOIL_LAYER_CNT = 8;
    enum  water_record_e
    {
        W_TRANSP = SOIL_LAYER_CNT, W_EVACEP, W_EVASOIL, W_EVAPOT, W_WTABLE,
        WATER_RECORD_SIZE
    };

    enum  __soil_record_e
    {
        __S_HEIGHT, __S_BULKD, __S_PORO, /*__S_WCMIN,*/ __S_WCMAX, __S_SKS, __S_CLAY, __S_WATER,
        SOIL_PROPERTIES_SIZE
    };
    enum  soil_record_e
    {
        S_HEIGHT = SOIL_LAYER_CNT * __S_HEIGHT,
        S_BULKD = SOIL_LAYER_CNT * __S_BULKD,
        S_PORO = SOIL_LAYER_CNT * __S_PORO,
// sk:unused        S_WCMIN = SOIL_LAYER_CNT * __S_WCMIN,
        S_WCMAX = SOIL_LAYER_CNT * __S_WCMAX,
        S_SKS = SOIL_LAYER_CNT * __S_SKS,
        S_CLAY = SOIL_LAYER_CNT * __S_CLAY,
        S_WATER = SOIL_LAYER_CNT * __S_WATER,

        SOIL_PROPERTIES_RECORD_SIZE = SOIL_LAYER_CNT * SOIL_PROPERTIES_SIZE
    };


    static char const *  CMF_SOLUTES[] = { "NO3"};
    static int const  CMF_SOLUTES_CNT = sizeof( CMF_SOLUTES) / sizeof( char *);
    static char const  CMF_SOLUTES_SEP = ' ';

    /* number of soil layers multiplied by number of transported solutes */
    static int const  SOIL_LAYER_FULL_CNT = SOIL_LAYER_CNT * CMF_SOLUTES_CNT;

}  /* extern "C" */

#endif  /*  !LDNDC_CMF_COMMON_H_  */

