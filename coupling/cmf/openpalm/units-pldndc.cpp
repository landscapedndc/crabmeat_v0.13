/*!
 * @file
 *    coupling/cmf/openpalm/units-pldndc.cpp
 *
 * @brief
 *    main program file of regional parallelized simulation system
 *
 * @author
 *    edwin haas,
 *    steffen klatt,
 */

#define  _I_AM_PLDNDC  1

#include  "ldndc-config.h"
#include  "ldndc.h"
#include  "io/work-dispatcher/work-dispatcher-roundrobin.h"
#include  "io/ioserver.h"
#include  "palm-landscape.h"

#include  "logging/llog.h"
#include  "mpi/lmpi.h"

#ifdef  _HAVE_CMDLINE_PARSING
#  include  "kernels/lkernels.h"
#  include  "kernels/mobile/modules/modules.h"

#  include  "app/ldndcopt.h"
#endif

#include  "palmlibc.h"
#include  "palm_user_paramc.h"
#include  "palm-common.h"

#include  <stdlib.h>
#include  <stdio.h>

int rc_init = LDNDC_ERR_FAIL;

ldndc::config_file_t * ldndc_glob_cf = NULL;

lio_server_t * ldndc_glob_iosrv = NULL;
work_dispatcher_round_robin_t *  ldndc_glob_wd = NULL;

cmf_palm_llandscape_t * ldndc_glob_region = NULL;


int  my_rank;   // MPI proc rank
int  comm_size; // MPI comm size

extern "C"
std::string
log_file_names(
        char const *  _lf)
{
    std::string  lf( _lf);
    lstring::format_expand( &lf, PL_COMM_EXEC);

    return  lf;
}

extern "C"
int
region_init(
        char const *  _pf_name,
                char const *  _cf_name,
                bool  _cf_given,
                ltime_t const *  _schedule,
                int  _my_rank, int  _comm_size)
{
    ldndc_glob_cf = new ldndc::config_file_t( _cf_name);
    if ( ldndc_glob_cf->read() != LDNDC_ERR_OK)
    {
        if ( _cf_given)
        {
            fprintf( stderr, "failed to load configuration file  [file=%s]\n", _cf_name);
            return  LDNDC_ERR_FAIL;
        }
        else
        {
            /* use default */
        }
    }

    lerr_t  rc( LDNDC_ERR_OK);

    /* open log file, set log level */
    std::string  my_logfile_name = log_file_names( ldndc_glob_cf->log_file());
    llog::init( my_logfile_name.c_str(), ldndc_glob_cf->log_level(), lstring::n2s( _my_rank).c_str());
    lflags_t  tgt_mask( ldndc_glob_cf->log_targets_mask());
    llog::log_targets_mask( &tgt_mask);

    char const *  pf_name(( _pf_name) ? _pf_name : ldndc_glob_cf->project_file());
    if ( ! pf_name  ||  lstring::is_empty( pf_name))
    {
        LOGERROR( "no valid project file found");
        return  LDNDC_ERR_FAIL;
    }

    // TODO  alternatively read project file from config
    LOGINFO( "setup file = \"",pf_name,"\"");
    LOGINFO( "config file = \"",_cf_name,"\"");

    /* prepare model inputs */
    ldndc_glob_iosrv = new lio_server_t( ldndc_glob_cf, _my_rank, _comm_size);
    ldndc_glob_wd = new work_dispatcher_round_robin_t( _my_rank, _comm_size);

    lid_t  lpid = ldndc_glob_iosrv->open_project( pf_name, ldndc_glob_wd, _schedule);
    if ( ! lutil::is_valid( lpid))
    {
        LOGERROR( "failed to read input data");
        llog::deinit();
        return LDNDC_ERR_FAIL;
    }

    /* instantiate and intialize simulation system */
    ldndc_glob_region = new cmf_palm_llandscape_t( ldndc_glob_cf);

    rc = ldndc_glob_region->initialize( ldndc_glob_iosrv->service_agent( lpid));

    if ( rc == LDNDC_ERR_OK)
    {
#ifdef  _HAVE_SERIALIZE
        /* restore cores from dump that are tagged accordingly (i.e. setup's 'outinit'
         * option is set to "load" or "both")
         */
        if ( ldndc_glob_region->restore_core_states() > 0)
        {
            llog::deinit();
            return  LDNDC_ERR_OBJECT_INIT_FAILED;
        }
#endif

        if ( ldndc_glob_region->get_active_cores_cnt() == 0u)
        {
            llog::deinit();
            return  LDNDC_ERR_INPUT_EXHAUSTED;
        }
    }
    else
    {
        LOGERROR( "failed to correctly initialize simulation system. check log.");
    }

    return  0;
}


extern "C"
int
pldndc_init(
        int  argc,
               char **  argv)
{
#ifdef  _HAVE_CMDLINE_PARSING
    struct  ldndc_args_info  args_info;
    struct  ldndc_cmdline_parser_params *  parser_params = ldndc_cmdline_parser_params_create();

    ldndc_cmdline_parser_params_init( parser_params);
    ldndc_cmdline_parser_init( &args_info);

    if ( ldndc_cmdline_parser_ext( argc, argv, &args_info, parser_params) != 0)
    {
        fprintf( stderr, "error parsing command line options\n");
        ldndc_cmdline_parser_free( &args_info);
        free( parser_params);
        exit( EXIT_FAILURE);
    }
    if ( args_info.help_given)
    {
        ldndc_cmdline_parser_print_help();
        ldndc_cmdline_parser_free( &args_info);
        free( parser_params);
        exit( EXIT_SUCCESS);
    }

    if ( args_info.list_modules_flag)
    {
        mmodule::list_available_modules( args_info.verbose_given);
        ldndc_cmdline_parser_free( &args_info);
        free( parser_params);
        exit( EXIT_SUCCESS);
    }

    if ( args_info.list_kernels_flag)
    {
        lkernel::list_available_kernels( args_info.verbose_given);
        ldndc_cmdline_parser_free( &args_info);
        free( parser_params);
        exit( EXIT_SUCCESS);
    }
#endif


#ifdef  _HAVE_CMDLINE_PARSING
    unsigned int const  setups_num( args_info.inputs_num);
    char **  setup_files = args_info.inputs;

    bool const  cf_given( args_info.config_given);
    char const *  config_file_name(( cf_given) ? args_info.config_arg : lconf::CONFIG_FILE);
    char const *  schedule(( args_info.schedule_given) ? args_info.schedule_arg : NULL);
#else
    unsigned int const  setups_num( argc-1);
    char **  setup_files = argv+1;

    bool const  cf_given( false);
    char const *  config_file_name( lconf::CONFIG_FILE);
    char const *  schedule( NULL);
#endif
    srand( 20111210);

    ltime_t  schedule_ldate;
    ltime_t *  schedule_ldate_p( NULL);
    if ( schedule && ( schedule_ldate.from_string( schedule) != LDNDC_ERR_OK))
    {
        LOGERROR( "invalid schedule specification.");
        rc_init = LDNDC_ERR_FAIL;
    }
    else
    {
        if ( schedule)
        {
            schedule_ldate_p = &schedule_ldate;
        }

        /* start simulation with given setup file parameter */
        for ( unsigned int  k = 0;  k < ((setups_num) ? setups_num : 1);  ++k)
        {
            //LOGDEBUG( "ldndc: initializing region", k);
            rc_init = region_init(((setups_num) ? setup_files[k] : NULL),
                    config_file_name, cf_given, schedule_ldate_p,
                    my_rank, comm_size);
            if ( rc_init)
            {
                PALM_Write( PL_OUT, "ldndc: initialization failed. notify cmf...  [rc=",rc_init,"]");
            }
            LOGDEBUG( "node",my_rank,"has",ldndc_glob_region->get_active_cores_cnt(),"kernels");
        }
    }

#ifdef  _HAVE_CMDLINE_PARSING
    ldndc_cmdline_parser_free( &args_info);
    free( parser_params);
#endif

    PALM_Write( PL_OUT, "ldndc says hello\n");
    PALM_Flush( PL_OUT);

    return (( rc_init == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE);
}


extern "C"
int
pldndc_main(
        int  argc,
        char **  argv)
{
    // get MPI proc rank and comm size;
    MPI_Comm_rank(PL_COMM_EXEC, &my_rank);
    MPI_Comm_size(PL_COMM_EXEC, &comm_size);
    //std::cout << "PLDNDC proc = " <<  my_rank << " , pid = " << pid << std::endl;
    /* initialize all regions */
    int rc = pldndc_init( argc, argv);

    LOGDEBUG( "ldndc: init: me",((rc)?"failed":"succeeded"),"[c=",my_rank,"]");
    int  ldndc_init_ok( 0);
    /* collect initialization status from all processes */
    MPI_Reduce( &rc, &ldndc_init_ok, 1, MPI_INT, MPI_SUM, MPI_MASTER_NODE, PL_COMM_EXEC);
    if ( my_rank == MPI_MASTER_NODE)
    {
        if ( ldndc_init_ok != 0)
        {
            fprintf( stderr, "ldndc: initialization failed  (rc=%d)\n", ldndc_init_ok);
            PALM_Abort();
        }
        else
        {
            fprintf( stderr, "ldndc: initialization succeeded\n");
        }
    }

    ldndc_glob_region->palm_initialize();

    ldndc_glob_region->launch();

#ifdef  _HAVE_SERIALIZE
    /* dump cores that are tagged accordingly (i.e. setup's 'outinit' is set) */
    ldndc_glob_region->dump_core_states();
#endif

    delete  ldndc_glob_region;
    delete  ldndc_glob_iosrv;
    delete  ldndc_glob_wd;
    delete  ldndc_glob_cf;

    PALM_Flush( PL_OUT);

    if ( llog::is_inited())
    {
        /* flush all logs commands to file and close logfile */
        llog::deinit();
    }

    return  0;
}

