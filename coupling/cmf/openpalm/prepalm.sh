
## path to openpalm 
prepalm_dir="$1"
prepalm_bin=${PREPALMBIN:-${prepalm_dir}/PrePALM_MP/prepalm_MP.tcl}
## the prepalm file
ppl="$2"
## the resulting makefile
mf="$3"

LDNDC_LIBS_PATTERN=__LDNDC_LIBS__
CMF_LIBS_PATTERN=__CMF_LIBS__

export  PREPALMMPDIR="${prepalm_dir}/PrePALM_MP"
$prepalm_bin --mpi2 --compile "$ppl"

echo  $mf
if [ -r "$mf" ]
then
	echo "injecting library locations/names into openpalm-generated makefile"
	sed --in-place "s/__LDNDC_LIBS__/\$(LDNDC_LIBS)/g" "$mf"
	sed --in-place "s/__CMF_LIBS__/\$(CMF_LIBS)/g" "$mf"
else
	echo "openpalm-generated makefile missing :-("
	exit 1
fi

exit 0


