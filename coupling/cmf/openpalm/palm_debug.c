/*
    This file is part of O-PALM.

    O-PALM is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    O-PALM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with O-PALM.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 1998-2011 (c) CERFACS 

  O-PALM tag 4.0.0

*/
#include "palmlibc.h" 
#include "lib_dvpvar.h" 
#include "gen_usrcsts.h"

void pl_debug(int* il_space_len,char* cda_space,int *il_obj_len,char* cda_obj,int* id_time,int* id_tag,int *il_nbelts,int *il_obj_sz,void* pvd_obj) {
/** TITLE - pl_debug : User defined debug procedures

     Purpose:
     --------
     To apply user defined debug procedures to objects handled by PALM

     Interface:
     ----------
     There is no direct call in the user's programs. This routine is 
     activated by the PrePALM interface through the DEBUG parameter of
     communications. If PL_DEBUG_ON_SEND is selected, the debug procedure
     is applied during the execution of PALM_Put. If PL_DEBUG_ON_SEND 
     is selected, the debug procedure is applied during the execution of 
     PALM_Get. If PL_DEBUG_ON_BOTH is selected, the debug procedure is 
     applied both during the execution on PALM_Put and the execution
     of PALM_Get.
     The user has to define his own debug procedures in the internal SELECT
     CASE.
     The variable il_nbelts contains the number of elements of the 
     object to debug.
     The variable il_obj_sz contains the size (in number of integer) of the 
     object.

***  Method:
     -------
     Select an appropriate debug procedure following the space name.
     "Cast" the PALM object into the mould of the type of the concerned
     space.
     Apply the debug procedure.
     Return a user defined error code.

     Externals:
     ----------
     None

     Files:
     ------
     None

     References:
     -----------
     The PALM Software User's Guide

     History:
     --------
      Version    Programmer      Date         Description
      -------    ----------      ----         -----------
       1.0       N.B.          2004/03/22     Created
--------------------------------------------------------------------------
**/


/** Local user defined variables
    int il_op;
**/


/**   1. DEBUG PROCEDURES 
         ----------------  **/

/**   1.1 Initialization : DO NOT CHANGE THIS LINES   **/

  char cla_time[10],cla_tag[PL_LNAME];

  /** Cast the integer time and tag in string **/
  if (*id_time==PL_NO_TIME) strcpy(cla_time,"none"); 
  else sprintf(cla_time,"%d",*id_time);
  if (*id_tag==PL_NO_TAG)  strcpy(cla_tag,"none"); 
  else sprintf(cla_tag,"%d",*id_tag);


  PALM_Write(PL_OUT,"DEBUG: Applying debug to object %s",cda_obj);
  PALM_Write(PL_OUT,"       at time %s for space %s",cla_time,cda_space);


/**   1.2 User defined cases  **/

  /* FOR EXAMPLE

     switch on space type.
     please remember that C switch instruction does not work for string

     Remember than PALM identifies spaces by a string composed by
     the space name suffixed by the name of the unit defining the
     the space. For instance, if the space "dspace" is defined in
     the identity card of the unit "the_unit" the corresponding
     internal PALM identifier is "dspace.the_unit" */

     if (!strcmp(cda_space,"dspace.the_unit")) {


  /* To print some statistics about an object, the simplest way is to use the
     PALM primitive PALM_Dump as follows */ 


    /*  il_op must be the sum of the different desired operation (PL_DUMP_ALL,
         PL_DUMP_MIN, PL_DUMP_MAX, PL_DUMP_SUM) */
    /*
       il_op = PL_DUMP_MIN+PL_DUMP_MAX+PL_DUMP_SUM;
       PALM_Dump(il_op, cda_space, cda_obj, id_time, id_tag, pvd_obj); */


  /* If the user wants to compute his/her own statistics, he/she must cast 
     ida_obj (the representation of the object values in an integer array) 
     into the write type of the object. */


  /* to treat other spaces use else if as follows */
 
    /*
       } else if (!strcmp(cda_space,"dspace2.the_unit")) {
       ... */


  /* Uncomment these lines to treat each object that have a pre-defined 
    space */

    /* } else if (!strcmp(cda_space,"one_integer") ||
               !strcmp(cda_space,"one_real") ||
           !strcmp(cda_space,"one_double") ||
           !strcmp(cda_space,"one_complex") ||
           !strcmp(cda_space,"one_logical") ||
           !strcmp(cda_space,"one_string") ||
           !strcmp(cda_space,"one_character")){

    PALM_Dump(PL_DUMP_ALL,cda_space,cda_obj,PL_NO_TIME,PL_NO_TAG, pvd_obj);

    */
 

  /**   1.3 End of the user part. DO NOT CHANGE THESE LINES. **/

     } else {
       PALM_Write(PL_OUT,"DEBUG: No debug action associated ");
       PALM_Write(PL_OUT,"       to space %s",cda_space);
  }

  return;
}

