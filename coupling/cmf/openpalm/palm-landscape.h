/*!
 * @file
 *    coupling/cmf/openpalm/landscape.h
 *
 * @brief
 *    cmf/openpalm wrapper class
 *
 * @author
 *    steffen klatt (created: 2009),
 *    edwin haas
 */

#ifndef COUPLING_CMF_PALM_LANDSCAPE_H_
#define COUPLING_CMF_PALM_LANDSCAPE_H_

#include  "ldndc-config.h"
#include  "landscape/landscape.h"

#include  "input/ic-names.h"
#include  "state/substate-names.h"

#include  "containers/lvector.h"

#include  "palmlibc.h"
#include  "palm-common.h"
#include  "palm_user_paramc.h"

#define  GET_SUBSTATE(__kernel__,__substate__) this->kernels_[__kernel__]->state().get_substate< __LDNDC_SUBSTATE_CLASS(__substate__) >()

class  __LDNDC_INPUT_CLASS(climate);
class  __LDNDC_SUBSTATE_CLASS(microclimate);
class  __LDNDC_SUBSTATE_CLASS(watercycle);

typedef  struct { char  solutes[PL_LNAME]; char  meteo[PL_LNAME]; char  water[PL_LNAME]; }  palm_spaceobjectname_t;
typedef  palm_spaceobjectname_t  palm_spacename_t;
typedef  palm_spaceobjectname_t  palm_objectname_t;

struct  buf_size_t
{
    size_t  solutes;
    size_t  meteo;
    size_t  water;
};

class  ldndc_cmf_transfer_t
{
    public:
        /*!
         * @brief
         *    create empty transfer structure
         */
        ldndc_cmf_transfer_t();

        /*!
         * @brief
         *
         * @param
         *      number of kernels
         */
        ldndc_cmf_transfer_t(
                size_t);

        ldndc_cmf_transfer_t &  operator=(
                ldndc_cmf_transfer_t const &);


        ~ldndc_cmf_transfer_t();

        buf_size_t  alloc_buffers();
        buf_size_t  buffer_sizes()
        const
        {
            buf_size_t  bs;
            bs.solutes = s_buf_size_;
            bs.meteo = m_buf_size_;
            bs.water = w_buf_size_;

            return  bs;
        }

        void  set_meteo(
                size_t /*timestep*/,
                __LDNDC_INPUT_CLASS(climate) const * /*climate stream interface*/);
        int  send_meteo_data();

        void  set_solutes(
                size_t /*kernel index*/,
                lvector_t< double > const * [] /*solute vectors*/);
        int  send_solutes_data();

        void  get_solutes(
                size_t /*kernel index*/,
                lvector_t< double > * [] /*solute vectors*/);
        int  receive_solutes_data();

        void  store_solutes(
                size_t /*kernel index*/,
                lvector_t< double > const * [] /*solute vectors*/);

        void compute_solutes_update();

        void update_solutes();

        void  get_water(
                size_t /*kernel index*/,
                __LDNDC_SUBSTATE_CLASS(watercycle) * /*watercycle substate*/);
        int  receive_water_data();

    private:
        /* number of entries in each solute array */
        size_t  kernel_cnt_;

        /* buffer holding all solute arrays */
        double *  s_buf_;
        double *  s_buf_store_;
        /* size of solute buffer */
        int  s_buf_size_;

        /* buffer holding meteorological boundary conditions */
        double *  m_buf_;
        /* size of meteo. buffer (for symmetry reasons) */
        int  m_buf_size_;

        /* buffer holding all water related data
         *
         * layout (L is number of soil layers):
         * 0...L-1    water content (nd_wc_sl)
         * L...L+3    transpiration, evaporation, evasoil?, evapotranspiration
         * L+4...L+5    water table
         */
        double *  w_buf_;
        /* size of water buffer */
        int  w_buf_size_;

        int  pl_no_time_;
        int  pl_no_tag_;
        palm_spacename_t  space_name_;
        palm_objectname_t  object_name_;
};


class  cmf_palm_llandscape_t  :  public  llandscape_t
{
    public:
        /*!
         * @brief
         *    instantiate empty regional simulation
         */
        cmf_palm_llandscape_t(
                ldndc::config_file_t const * = NULL);
        ~cmf_palm_llandscape_t();

        lerr_t  palm_initialize();

        lerr_t  launch(
                unsigned int = 0u);

        lerr_t  palm_send_meteo_data(
                size_t /*timestep*/);

        lerr_t  palm_send_solutes_data();
        lerr_t  palm_receive_solutes_data();

        lerr_t  palm_store_solutes();
        lerr_t  palm_compute_solutes_update();
        lerr_t  palm_update_solutes();

        lerr_t  palm_receive_water_data();

        ldndc_cmf_transfer_t const *  palm_transfers()
        const
        {
            return  &palm_transfers_;
        }

    private:
        ldndc_cmf_transfer_t  palm_transfers_;

        int  run_time_;

        int  region_size_;
};

#endif  /*  !COUPLING_CMF_PALM_LANDSCAPE_H_  */

