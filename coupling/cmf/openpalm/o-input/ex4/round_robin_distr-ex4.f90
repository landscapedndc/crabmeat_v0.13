!    This file is part of O-PALM.
!
!    O-PALM is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    O-PALM is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with O-PALM.  If not, see <http://www.gnu.org/licenses/>.
!
!    Copyright 1998-2011 (c) CERFACS
!
! O-PALM tag 4.0.0

!
!=====> User section:  Give the subroutine a proper name
!
SUBROUTINE round_robin_distr(id_action, id_nbproc, id_rank, ida_shape, &
   id_usrinfo_sz, ida_usrinfo, id_size, ida_descr)
!
!-----> End of user section
!
!**** Generic_reg_descr
!
!     Purpose: provides a regular distributor description
!     --------
!
!     Interface:
!     ----------
!         Inputs :
!         --------
!             id_action : code determining what the routine must return
!                         can be PL_GIVE_SIZE : the routine must return id_size
!                                  the size of the distributor description vect
!                                PL_GIVE_DESCR : the routine must return
!                                  ida_descr, the vector containing the
!                                  distributor description
!             id_nbproc : number of processes of the distributor
!             id_rank : rank of the global object (info for the user)
!             ida_shape : shape of the global object (info for the user)
!             ida_usrinfo : user informations usefull for computing the
!                           distributor can be passed through this vector for
!                           dynamic distributions
!             id_usrinfo_sz : size of ida_usrinfo
!
!
!         Outputs :
!         ---------
!             id_size : size of ida_descr, the vector containing the
!                       distributor description
!             ida_descr : the vector containing the distributor description
!
!
!***  References: The PALM and PrePALM PROTO Quick StartGuide
!     ----------- Tutorial I, session VI.
!
!     History:
!     --------
!      Version    Programmer      Date         Description
!      -------    ----------      ----         -----------
!       1.0       Samuel          2001.09.03   Template
!*--------------------------------------------------------------------------
!
!**   0. DECLARATIONS
!        ------------
!
!**   0.1 Include files and modules
!
  USE palmlib     !*I The PALM interface
  USE palm_user_param !*I The PALM constants
 !
  IMPLICIT NONE
!
!**   0.2 Dummy variables
!
  INTEGER, INTENT(IN) :: id_action, id_nbproc, id_rank, id_usrinfo_sz
  INTEGER, INTENT(INOUT) :: id_size
  INTEGER, DIMENSION(id_rank), INTENT(IN) :: ida_shape
  INTEGER, DIMENSION(id_usrinfo_sz), INTENT(IN) :: ida_usrinfo
  INTEGER, DIMENSION(id_size), INTENT(OUT) :: ida_descr
!
!**   0.3 Local variables
!
  INTEGER :: il_start  !*V il_start : array index
!
!*--------------------------------------------------------------------------
!
  SELECT CASE(id_action)
!
  CASE(PL_GIVE_SIZE)
      id_size =  1 + (2 * id_nbproc + 3) * id_rank
!
  CASE(PL_GIVE_DESCR)
!
      il_start = 1
      ida_descr(il_start) = id_rank
      il_start = il_start + 1
!!
!!=====> User section:  Fill the description accordingly to instructions
!!
!!     we have a 1-d processor grid (since the array of kernels
!!     is also 1-d) of 5 nodes
      ida_descr(il_start : il_start + id_rank - 1) = (/ 2 /)
      il_start = il_start + id_rank
!!
!!     the round robin distribution assigns one kernel after the other
!!     to the nodes, so the block size is 1
      ida_descr(il_start : il_start + id_rank - 1) = (/ 21 /)
      il_start = il_start + id_rank
!!
!!     proc 0 has the first kernel
      ida_descr(il_start : il_start + id_rank - 1) = (/ 0 /)!=> Grid coords of
      il_start = il_start + id_rank                       !   the proc storing
!!                                                         !   the first block
!!
!!     Fill the rest of ida_descr accordingly to the following instructions
!!          *) informations are entered one after the others in ida_descr
!!             as vector elements
!!          *) for each process of the distributor enter
!!               +) the SHAPE of the local array
!!               +) the coords of the first element of the first block
!!                  in the local array
!!
      ida_descr(il_start : id_size) = (/ 42, & ! 20 kernels on proc 0
                                          0, & ! no offset in kernel array on proc 0
                                         42, & ! 20 kernels on proc 1
                                          0  & ! no offset in kernel array on proc 1
                                       /)
!
!-----> End of user section
!
  END SELECT
!
END SUBROUTINE

