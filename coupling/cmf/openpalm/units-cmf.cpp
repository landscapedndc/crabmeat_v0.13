/*!
 * @file
 *    coupling/cmf/openpalm/units-cmf.cpp
 *
 * @brief
 *    C++ interface for CMF when running in coupled mode
 *
 * @author
 *    edwin haas,
 *    steffen klatt (created on: sep. 21, 2012),
 */

#define  _I_AM_CMF  1

/* CMF includes */
#include  "project.h"
#include  "upslope/vegetation/ET.h"
#include  "math/integrators/cvodeintegrator.h"
#include  "math/integrators/implicit_euler.h"
#include  "math/integrators/WaterSoluteIntegrator.h"
#include  "upslope/connections/subsurfacefluxes.h"
#include  "upslope/connections/Percolation.h"


#include  "palmlibc.h"
#include  "palm_user_paramc.h"
#include  "palm-common.h"

#include  <iostream>
#include  <fstream>
#include  <stdio.h>

#define  CMF_ERR_OK  0
#define  CMF_ERR_FAIL  1

#define  CMF_ERR_BUFFER_INVALID  20
#define  CMF_ERR_NOMEM  21

#define  CMF_ERR_LDNDC_INIT_FAILED  50

#include <omp.h>

extern "C"
std::string
solutes_to_string(
        char const * _solutes[],
        size_t _solute_cnt)
{
    if ( _solute_cnt == 0u)
    {
        return  std::string("");
    }

    std::string  solutes( _solutes[0]);
    for ( size_t  k = 1;  k < _solute_cnt;  ++k)
    {
        solutes += CMF_SOLUTES_SEP + std::string(_solutes[k]);
    }

    return  solutes;
}

extern "C"
void
cmf_set_weather(
        cmf::atmosphere::Weather *  _w,
        double const *  _m)
{
    fprintf( stderr, "cmf[b]:\t\ttemp=%f, tMin=%f, tMax=%f, vpd=%f, win=%f, rad=%f, prec=%f\n",
                _m[M_T], _m[M_TMIN], _m[M_TMAX], _m[M_VPD], _m[M_WIND], _m[M_RAD], _m[M_PRECIP]);

    _w->T = _m[M_T];
    _w->Tmin = _m[M_TMIN];
    _w->Tmax = _m[M_TMAX];
    _w->e_s = cmf::atmosphere::vapour_pressure( _m[M_T]);
    _w->e_a = _w->e_s - _m[M_VPD];
    _w->Windspeed = _m[M_WIND];
    _w->Rs = _m[M_RAD] * ( 24.0 * 60.0 * 60.0 * 1.0e-06);

    _w->instrument_height = 2.0;
}


extern "C"
int
cmf_build_region(
        cmf::project *  _cmf_project,
        char const *  _cells_file_name,
        char const *  _topo_file_name)
{
    {
        // load cells from file
        std::ifstream  cells_file;
        cells_file.open( _cells_file_name);
        if ( ! cells_file.is_open())
        {
            PALM_Write( PL_OUT, "failed to open \"cells\" file: %s", _cells_file_name);
            PALM_Flush( PL_OUT);
            return  31;
        }
        int  id;
        double  x, y, z, area;
        while ( ! cells_file.eof())
        {
            cells_file >> id >> x >> y >> z >> area;
            if ( ! cells_file.good())
            {
                break;
            }
// sk:dbg            fprintf( stderr, "seeing cmf-id %4d\n", id);
            cmf::upslope::Cell *  cell = _cmf_project->NewCell( x, y, z, area);
            cell->Id = id;
        }
        cells_file.close();
    }

    {
        // load topology from file
        std::ifstream  topo_file;
        topo_file.open( _topo_file_name);
        if ( ! topo_file.is_open())
        {
            PALM_Write( PL_OUT, "failed to open \"topology\" file: %s", _topo_file_name);
            PALM_Flush( PL_OUT);
            return  32;
        }
        int  cmf_id_0, cmf_id_1;
        double  layer_width;
        while ( ! topo_file.eof())
        {
            topo_file >> cmf_id_0 >> cmf_id_1 >> layer_width;
            if ( ! topo_file.good())
            {
                break;
            }
            _cmf_project->get_cell( cmf_id_0).get_topology().AddNeighbor( _cmf_project->get_cell( cmf_id_1), layer_width);
        }
        topo_file.close();
    }


    return  0;
}


extern "C"
int
cmf_add_layers_to_cells(
        cmf::project *  _cmf_project)
{
    int  pl_no_time( PL_NO_TIME);
    int  pl_no_tag( PL_NO_TAG);

    char  soil_prop_space[PL_LNAME];
    opalm_name(soil_prop_space,"soilprops_space");
    char  soil_prop_object[PL_LNAME];
    opalm_name(soil_prop_object,"soilprops");

    double *  soilprops_buffer = new double[_cmf_project->size()*SOIL_PROPERTIES_RECORD_SIZE];
    PALM_Get( soil_prop_space, soil_prop_object, &pl_no_time, &pl_no_tag, soilprops_buffer);

    for ( int  i = 0;  i < _cmf_project->size();  ++i)
    {
        // design cells
        cmf::upslope::Cell &  cell = _cmf_project->get_cell( i);

        double  d = 0.0;
        double *  s_props = soilprops_buffer + i*SOIL_PROPERTIES_RECORD_SIZE;
        for ( int  l = 0;  l < SOIL_LAYER_CNT;  ++l, ++s_props)
        {
//#ifdef  _DEBUG
            if ( i == 3)
            {
                fprintf( stderr, "l:%02d h=%.6f, bd=%.6f, poro=%.6f, wc-max=%.6f, sks=%.6f, clay=%.6f, wc=%.6f\n",
                        l,
                        s_props[S_HEIGHT], s_props[S_BULKD], s_props[S_PORO],
                        s_props[S_WCMAX], s_props[S_SKS], s_props[S_CLAY], s_props[S_WATER]);
            }
//#endif
            double const  b = 12.958 / ( 1.0 + exp( -( s_props[S_CLAY] - 0.32) / 0.1124)) + 1.99;
            double const  sks = s_props[S_SKS] * 24.0 * 60.0 / 100.0;
            double const  porosity = s_props[S_PORO];
            double const  fcap = s_props[S_WCMAX] * 1.0e-03;
// sk:unused            double  wilt = s_props[S_WCMIN] * 1.0e-03;

// sk:chgd            cmf::upslope::VanGenuchtenMualem  retcurve_vgm( 1.0, 0.5, 0.04, 2.0);
            cmf::upslope::BrooksCoreyRetentionCurve  retcurve_bc( sks, porosity, b, fcap);

            d += s_props[S_HEIGHT];
            cell.add_layer( d, retcurve_bc);
            cell.get_layer( l)->set_theta( s_props[S_WATER] * 1.0e-03);
        }

        // Richards equation
        cmf::upslope::connections::Richards::use_for_cell( cell);
        // evapotranspiration
        cmf::upslope::ET::PenmanMonteithET::use_for_cell( cell);
        // initial water content
        cell.set_saturated_depth( 0.5);
    }

    delete[]  soilprops_buffer;

    return  0;
}


extern "C"
int
cmf_main(
        int  _argc,
        char **  _argv)
{
    omp_set_num_threads(5);
    /* TODO  pass via arguments */
    char const *  cells_file_name = "./c-input/cells.asc";
    char const *  topo_file_name = "./c-input/topology.asc";

    int  pl_no_time = PL_NO_TIME;
    int  pl_no_tag = PL_NO_TAG;

    char  space_name_solutes[PL_LNAME];
    sprintf( space_name_solutes, "%s", "solutes_space");
    char  object_name_solutes[PL_LNAME];
    sprintf( object_name_solutes, "%s", "solutes");

    char  space_name_meteo[PL_LNAME];
    sprintf( space_name_meteo, "%s", "meteo_space");
    char  object_name_meteo[PL_LNAME];
    sprintf( object_name_meteo, "%s", "meteo");

    char  space_name_water[PL_LNAME];
    sprintf( space_name_water, "%s", "water_space");
    char  object_name_water[PL_LNAME];
    sprintf( object_name_water, "%s", "water");


    // get nb. of time steps
    int  run_time = 0;
    {
        char  space_name[PL_LNAME];
        char  object_name[PL_LNAME];
        sprintf( space_name, "%s", "one_integer");
        sprintf( object_name, "%s", "run_time");
        PALM_Get( space_name, object_name, &pl_no_time, &pl_no_tag, &run_time);
    }


    // get the shape = size (1-d array!) for the solutes buffer
    int  solutes_buffer_size[1] = { 0};
    PALM_Space_get_shape( space_name_solutes, 1, solutes_buffer_size);
    if ( solutes_buffer_size[0] == 0)
    {
        PALM_Abort();
    }
    fprintf( stderr, "%s-buffer-size=%d\n", object_name_solutes, solutes_buffer_size[0]);

    // allocate the solutes buffer
    double *  solutes_buffer = new double[solutes_buffer_size[0]];
    if ( !solutes_buffer)
    {
        PALM_Abort();
    }


    // get the shape = size (1-d array!) for the meteo buffer
    int  meteo_buffer_size[1] = { 0};
    PALM_Space_get_shape( space_name_meteo, 1, meteo_buffer_size);
    if ( meteo_buffer_size[0] == 0)
    {
        PALM_Abort();
    }
    fprintf( stderr, "%s-buffer-size=%d\n", object_name_meteo, meteo_buffer_size[0]);
    // allocate the meteo buffer
    double *  meteo_buffer = new double[meteo_buffer_size[0]];
    if ( !meteo_buffer)
    {
        PALM_Abort();
    }


    // get the shape = size (1-d array!) for the water buffer
    int  water_buffer_size[1] = { 0};
    PALM_Space_get_shape( space_name_water, 1, water_buffer_size);
    if ( water_buffer_size[0] == 0)
    {
        PALM_Abort();
    }
    fprintf( stderr, "%s-buffer-size=%d\n", object_name_water, water_buffer_size[0]);
    // allocate the water buffer
    double *  water_buffer = new double[water_buffer_size[0]];
    if ( !water_buffer)
    {
        PALM_Abort();
    }


    // create a CMF project with solutes
    cmf::project *  cmf_project = new cmf::project( solutes_to_string( CMF_SOLUTES, CMF_SOLUTES_CNT));
    {
        int  rc = cmf_build_region( cmf_project, cells_file_name, topo_file_name);
        if ( rc)
        {
            return  EXIT_FAILURE;
        }
    }
    {
        int  rc = cmf_add_layers_to_cells( cmf_project);
        if ( rc)
        {
            return  EXIT_FAILURE;
        }
    }

    cmf::upslope::cell_vector  cmf_project_cells_copy = cmf_project->get_cells();
    cmf::upslope::connect_cells_with_flux(
            cmf_project_cells_copy,
            cmf::upslope::connections::Richards_lateral::cell_connector);

    // integrator
    cmf::math::CVodeIntegrator *  integrator = new cmf::math::CVodeIntegrator(*cmf_project,
                                                                                                                                                        1.0e-6);

    fprintf( stderr, "CMF integrator size = %d\n", integrator->size());

    // solute integrator
    //cmf::math::ImplicitEuler *  solute_solver = new cmf::math::ImplicitEuler( 1.0e-6);
    // combined integrator
    /*cmf::math::SoluteWaterIntegrator *  cmf_solute_water_integrator;
    cmf_solute_water_integrator = new cmf::math::SoluteWaterIntegrator(
                                *water_solver,
                                *solute_solver,
                                *cmf_project);
    delete  water_solver;
    water_solver = static_cast< cmf::math::CVodeIntegrator * >( cmf_solute_water_integrator->get_water_integrator());
    delete  solute_solver;
    solute_solver = static_cast< cmf::math::ImplicitEuler * >( cmf_solute_water_integrator->get_solute_integrator());*/

    // run time loop
    int  cmf_run_time = 0;
    while ( cmf_run_time++ < run_time)
    {
        fprintf( stderr, "cmf-run-time=%d/%d\n", cmf_run_time, (int)run_time);
        // the current time
        cmf::math::Time  t_now = integrator->get_t();

        // get meteo data
        PALM_Get( space_name_meteo, object_name_meteo, &pl_no_time, &pl_no_tag, meteo_buffer);
        cmf::atmosphere::Weather  w_meteo;
        cmf_set_weather( &w_meteo, meteo_buffer);

        // prepare to put water
#pragma omp parallel for
        for ( int  i = 0;  i < cmf_project->size();  i++)
        {
            cmf::upslope::Cell &  cell = cmf_project->get_cell( i);

            /* weather */
            cell.set_weather( w_meteo);
            cell.set_rainfall( meteo_buffer[M_PRECIP]);

            /* vegetation */
// sk:todo            cell.vegetation.LAI = TODO;
            cell.vegetation.Height = 1.0;
            cell.vegetation.StomatalResistance = 200.0;

            /* water */
            size_t const  W( i * WATER_RECORD_SIZE);
            double *  w_buf = water_buffer + W;
            for ( int  k = 0;  k < SOIL_LAYER_CNT;  ++k)
            {
                cmf::upslope::SoilLayer::ptr  s_layer = cell.get_layer( k);
                w_buf[k] = s_layer->get_wetness() * s_layer->get_porosity() * 1.0e03;
            }
            /* transpiration (nd_transp) */
            w_buf[W_TRANSP] = cell.get_transpiration()->waterbalance( t_now);
            /* evaporation from interception (nd_evacep) */
            w_buf[W_EVACEP] = ( cell.get_canopy()) ? cell.get_canopy()->flux_to( *cell.get_evaporation(), t_now) : 0.0;
            /* evaporation from soil (nd_evasoil) */
            w_buf[W_EVASOIL] = cell.get_layer( 0)->flux_to( *cell.get_evaporation(), t_now);
            /* potential evaporation (nd_evapot) */
            w_buf[W_EVAPOT] = cmf::upslope::ET::PenmanMonteith( cell.get_weather( t_now), cell.vegetation, t_now);
            /* water table */
            w_buf[W_WTABLE] = 0.0; // sk:unused
            /* surface water */
            // TODO w_buf[W_SURFACE] = cell.get_surfacewater()->waterbalance( t_now);
        }

        // put water
        PALM_Put( space_name_water, object_name_water, &pl_no_time, &pl_no_tag, water_buffer);

        // the next cmf::math::Time
        cmf::math::Time  t_next = t_now + cmf::math::day;

        // do one time step
        integrator->integrate_until( t_next);

        // set next time and time step size in the combined integrator
        //cmf_solute_water_integrator->set_t( t_next);

        // get the solutes from the CMF project
// sk:dbg        fprintf( stderr, "%u  %u  %u\n", (unsigned)cmf_project->size(), (unsigned)CMF_SOLUTES_CNT, (unsigned)SOIL_LAYER_CNT);
#pragma omp parallel for
        for ( int  i = 0;  i < cmf_project->size();  i++)
        {
            cmf::upslope::Cell &  cell = cmf_project->get_cell( i);

            double *  s_buf = solutes_buffer + ( i * SOIL_LAYER_FULL_CNT);
            // loop over the solutes
            for ( int  j = 0;  j < CMF_SOLUTES_CNT;  j++)
            {
                cmf::water::solute const &  sol = cmf_project->solutes[j];

                s_buf += j * SOIL_LAYER_CNT;
                // loop over the soil layers
                for( int  k = 0;  k < SOIL_LAYER_CNT;  k++, ++s_buf)
                {
                    *s_buf = cell.get_layer( k)->Solute( sol).get_state() * 1.0e-03 * ( 1.0 / cell.get_area());
                }
            }
        }

        // put the solutes
        PALM_Put( space_name_solutes, object_name_solutes, &pl_no_time, &pl_no_tag, solutes_buffer);

        // get the updated solutes
        PALM_Get( space_name_solutes, object_name_solutes, &pl_no_time, &pl_no_tag, solutes_buffer);

        // set the solutes in the CMF project
#pragma omp parallel for
        for ( int  i = 0;  i < cmf_project->size();  i++)
        {
            cmf::upslope::Cell &  cell = cmf_project->get_cell( i);

            size_t const  I( i * SOIL_LAYER_FULL_CNT);
            // loop over the solutes
            for (int  j = 0;  j < CMF_SOLUTES_CNT;  j++)
            {
                cmf::water::solute const &  sol = cmf_project->solutes[j];

                size_t const  J( I + j * SOIL_LAYER_CNT);
                // loop over the soil layers
                for( int  k = 0;  k < SOIL_LAYER_CNT;  k++)
                {
                    size_t const  K( J + k);
                    double const  sol_state = solutes_buffer[K] * 1.0e03 * cell.get_area();
                    cell.get_layer( k)->Solute( sol).set_state( sol_state);
                }
            }
        } // END set solutes

    } // while ( cmf_run_time < run_time)

    // free memory
    delete[]  solutes_buffer;
    delete[]  meteo_buffer;
    delete[]  water_buffer;
    delete  cmf_project;
    delete  integrator;

    PALM_Flush( PL_OUT);

    return  CMF_ERR_OK;
}

