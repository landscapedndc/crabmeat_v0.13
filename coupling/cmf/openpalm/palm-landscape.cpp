/*!
 * @file
 *    coupling/cmf/openpalm/landscape.cpp
 *
 * @brief
 *    cmf/openpalm wrapper class
 *
 * @author
 *    steffen klatt (created: 2009),
 *    edwin haas
 */

#include  "palm-landscape.h"
#include  "kernels/lkernels.h"

#include  "time/clock.h"
#include  "math/lmath.h"

#include  "state/state.h"
#include  "state/substates.h"
#include  "input/ic.h"
#include  "input/soillayers/soillayers.h"

#include  "containers/lvector.h"

#include  "logging/llog.h"
#include  "openmp/lomp.h"
#include  "mpi/lmpi.h"


#include  "palm-common.h"
#include  "palmlibc.h"


ldndc_cmf_transfer_t::ldndc_cmf_transfer_t()
        : kernel_cnt_( 0),
          s_buf_( NULL), s_buf_store_( NULL), s_buf_size_( 0),
          m_buf_( NULL), m_buf_size_( 0),
          w_buf_( NULL), w_buf_size_( 0),

          pl_no_time_( PL_NO_TIME), pl_no_tag_( PL_NO_TAG)
{
    sprintf( this->space_name_.solutes, "%s", "solutes_space");
    sprintf( this->object_name_.solutes, "%s", "solutes");
    sprintf( this->space_name_.meteo, "%s", "meteo_space");
    sprintf( this->object_name_.meteo, "%s", "meteo");
    sprintf( this->space_name_.water, "%s", "water_space");
    sprintf( this->object_name_.water, "%s", "water");
}

ldndc_cmf_transfer_t::ldndc_cmf_transfer_t(
        size_t  _kcnt)
        : kernel_cnt_( _kcnt),
          s_buf_( NULL), s_buf_store_( NULL), s_buf_size_( 0),
          m_buf_( NULL), m_buf_size_( 0),
          w_buf_( NULL), w_buf_size_( 0),

          pl_no_time_( PL_NO_TIME), pl_no_tag_( PL_NO_TAG)
{
    sprintf( this->space_name_.solutes, "%s", "solutes_space");
    sprintf( this->object_name_.solutes, "%s", "solutes");
    sprintf( this->space_name_.meteo, "%s", "meteo_space");
    sprintf( this->object_name_.meteo, "%s", "meteo");
    sprintf( this->space_name_.water, "%s", "water_space");
    sprintf( this->object_name_.water, "%s", "water");
}

ldndc_cmf_transfer_t &
ldndc_cmf_transfer_t::operator=(
        ldndc_cmf_transfer_t const &  _rhs)
{
    if ( this == &_rhs)
    {
        return  *this;
    }

    /* we simply do not support this currently */
    ldndc_assert( !this->s_buf_);
    ldndc_assert( !this->s_buf_store_);
    ldndc_assert( this->s_buf_size_ == 0u);
    ldndc_assert( !this->m_buf_);
    ldndc_assert( this->m_buf_size_ == 0u);
    ldndc_assert( !this->w_buf_);
    ldndc_assert( this->w_buf_size_ == 0u);

    this->kernel_cnt_ = _rhs.kernel_cnt_;

    sprintf( this->space_name_.solutes, "%s", _rhs.space_name_.solutes);
    sprintf( this->object_name_.solutes, "%s", _rhs.object_name_.solutes);
    sprintf( this->space_name_.meteo, "%s", _rhs.space_name_.meteo);
    sprintf( this->object_name_.meteo, "%s", _rhs.object_name_.meteo);
    sprintf( this->space_name_.water, "%s", _rhs.space_name_.water);
    sprintf( this->object_name_.water, "%s", _rhs.object_name_.water);

    this->pl_no_time_ = _rhs.pl_no_time_;
    this->pl_no_tag_ = _rhs.pl_no_tag_;

    /* copy meteorological boundary conditions */
    //memcpy( this->m_buf_, _rhs.m_buf_, _rhs.m_buf_size_*sizeof( double));


    return  *this;
}


ldndc_cmf_transfer_t::~ldndc_cmf_transfer_t()
{
        delete[]  this->s_buf_;
                delete[]  this->s_buf_store_;
        delete[]  this->m_buf_;
        delete[]  this->w_buf_;
}


buf_size_t
ldndc_cmf_transfer_t::alloc_buffers()
{
    ldndc_assert( this->kernel_cnt_);
    ldndc_assert( !this->s_buf_);
    ldndc_assert( !this->s_buf_store_);
    ldndc_assert( !this->m_buf_);
    ldndc_assert( !this->w_buf_);

    buf_size_t  buf_sizes;
    buf_sizes.solutes = 0;
    buf_sizes.meteo = 0;
    buf_sizes.water = 0;

    this->s_buf_size_ = SOIL_LAYER_FULL_CNT * this->kernel_cnt_;
    ldndc_assert( this->s_buf_size_);
    this->s_buf_ = new  double[this->s_buf_size_];
    this->s_buf_store_ = new  double[this->s_buf_size_];
    if ( this->s_buf_ && this->s_buf_store_)
    {
        buf_sizes.solutes = this->s_buf_size_;
    }
    else
    {
        LOGFATAL( "failed to allocate memory buffer [buffer=", this->object_name_.solutes,"]");
        buf_sizes.solutes = 0;
    }


    PALM_Space_get_shape( this->space_name_.meteo,  1, &this->m_buf_size_);
    ldndc_assert( this->m_buf_size_);
    this->m_buf_ = new  double[this->m_buf_size_];
    if ( this->m_buf_)
    {
        buf_sizes.meteo = this->m_buf_size_;
    }
    else
    {
        LOGFATAL( "failed to allocate memory buffer [buffer=", this->object_name_.meteo,"]");
        buf_sizes.meteo = 0;
    }

    this->w_buf_size_ = WATER_RECORD_SIZE * this->kernel_cnt_;
    ldndc_assert( this->w_buf_size_);
    this->w_buf_ = new  double[this->w_buf_size_];
    if ( this->w_buf_)
    {
        buf_sizes.water = this->w_buf_size_;
    }
    else
    {
        LOGFATAL( "failed to allocate memory buffer [buffer=", this->object_name_.water,"]");
        buf_sizes.water = 0;
    }


    fprintf( stderr, "(|k|=%d,|k*l|=%d) %s-buffer-size=%d %s-buffer-size=%d %s-buffer-size=%d\n",
                (int)this->kernel_cnt_, SOIL_LAYER_FULL_CNT,
                this->object_name_.solutes, this->s_buf_size_,
                this->object_name_.meteo, this->m_buf_size_,
                this->object_name_.water, this->w_buf_size_);
    return  buf_sizes;
}


void
ldndc_cmf_transfer_t::set_meteo(
        size_t  _timestep,
                __LDNDC_INPUT_CLASS(climate) const *  _m)
{
    double *  buf = this->m_buf_;
    buf[M_T] = _m->temp_avg( _timestep);
    buf[M_TMIN] = _m->temp_min( _timestep);
    buf[M_TMAX] = _m->temp_max( _timestep);
    buf[M_VPD] = _m->vpd( _timestep);
    buf[M_WIND] = _m->wind_speed( _timestep);
    buf[M_RAD] = _m->ex_rad( _timestep);
    buf[M_PRECIP] = _m->precip( _timestep);

//    fprintf( stderr, "ldndc[i]:\ttemp=%f, tMin=%f, tMax=%f, vpd=%f, win=%f, rad=%f, prec=%f\n",
//            _m->nd_airtemperature, _m->nd_minimumairtemperature, _m->nd_maximumairtemperature, _m->nd_watervaporsaturationdeficit, _m->nd_windspeed, _m->nd_shortwaveradiance, _m->nd_precipitation);
    fprintf( stderr, "ldndc[b]:\ttemp=%f, tMin=%f, tMax=%f, vpd=%f, win=%f, rad=%f, prec=%f\n",
            this->m_buf_[M_T], this->m_buf_[M_TMIN], this->m_buf_[M_TMAX], this->m_buf_[M_VPD], this->m_buf_[M_WIND], this->m_buf_[M_RAD], this->m_buf_[M_PRECIP]);
}
int
ldndc_cmf_transfer_t::send_meteo_data()
{
// sk:dbg    fprintf( stderr, "s:ldndc-run-time=%d/?, meteo-space-name=%s, meteo-object-name=%s\n", _time_step, this->space_name_.meteo, this->object_name_.meteo);
    int  rc = PALM_Put( this->space_name_.meteo, this->object_name_.meteo, &this->pl_no_time_, &this->pl_no_tag_, this->m_buf_);

    return  rc;
}


void
ldndc_cmf_transfer_t::set_solutes(
                size_t  _kernel_index,
                /* list of solutes ordered according to CMF_SOLUTES */
                lvector_t< double > const *  _s_bufs[])
{
    ldndc_assert( _kernel_index < this->kernel_cnt_);
    ldndc_assert( _s_bufs);

    for ( int  s = 0;  s < CMF_SOLUTES_CNT;  ++s)
    {
        ldndc_assert( _s_bufs[s]);
        ldndc_assert( _s_bufs[s]->size() == (unsigned int)SOIL_LAYER_CNT);
        _s_bufs[s]->copy_to_carray( this->s_buf_ +
                ( _kernel_index*SOIL_LAYER_FULL_CNT) +
                (s * SOIL_LAYER_CNT));

// sk:dbg        fprintf( stderr, "r:%d\t%d @ %d, offs=%llu\n", ldndc::mpi::comm_rank( PL_COMM_EXEC), (int)_kernel_index, s, (unsigned int long long)(( _kernel_index*SOIL_LAYER_FULL_CNT) + (s * SOIL_LAYER_CNT)));
// sk:dbg        LOGDEBUG( "ldndc: c=",ldndc::mpi::comm_rank( PL_COMM_EXEC), " krnl=",_kernel_index, " addr[",s,"]=",this->s_buf_, "offs=", (size_t)(( _kernel_index*SOIL_LAYER_FULL_CNT) + (s * SOIL_LAYER_CNT)));
    }
}

int
ldndc_cmf_transfer_t::send_solutes_data()
{
// sk:dbg    fprintf( stderr, "s:ldndc-run-time=%d/?, solutes-space-name=%s, solutes-object-name=%s\n", _time_step, this->space_name_.solutes, this->object_name_.solutes);
    int  rc = PALM_Put( this->space_name_.solutes, this->object_name_.solutes, &this->pl_no_time_, &this->pl_no_tag_, this->s_buf_+1);

    return  rc;
}


void
ldndc_cmf_transfer_t::get_solutes(
                size_t  _kernel_index,
                /* list of solutes ordered according to CMF_SOLUTES */
                lvector_t< double > *  _s_bufs[])
{
    ldndc_assert( _kernel_index < this->kernel_cnt_);
    ldndc_assert( _s_bufs);

    for ( int  s = 0;  s < CMF_SOLUTES_CNT;  ++s)
    {
        ldndc_assert( _s_bufs[s]);
        ldndc_assert( _s_bufs[s]->size() == (unsigned int)SOIL_LAYER_CNT);
        _s_bufs[s]->copy_from_carray( this->s_buf_ + ( _kernel_index*SOIL_LAYER_FULL_CNT) + ( s * SOIL_LAYER_CNT));

// sk:dbg        LOGVERBOSE( "r:",ldndc::mpi::comm_rank( PL_COMM_EXEC), "k/K=",_kernel_index,"/",this->kernel_cnt_, "a-size=",_s_bufs[s]->size(), "a[0,6558,6559]=", this->s_buf_[0], this->s_buf_[6558], this->s_buf_[6559], "offs=",(unsigned int long long)(( _kernel_index*SOIL_LAYER_FULL_CNT) + ( s * SOIL_LAYER_CNT)));
// sk:dbg        _s_bufs[s]->dump_to_log( state_item_info_varname( _s_bufs[s]->name_id));
    }
}

int
ldndc_cmf_transfer_t::receive_solutes_data()
{
// sk:dbg    fprintf( stderr, "r:ldndc-run-time=%d/?, solutes-space-name=%s, solutes-object-name=%s\n", _time_step, this->space_name_.solutes, this->object_name_.solutes);
    int  rc = PALM_Get( this->space_name_.solutes, this->object_name_.solutes, &this->pl_no_time_, &this->pl_no_tag_, this->s_buf_+1);

    return  rc;
}

void
ldndc_cmf_transfer_t::store_solutes(
                size_t  _kernel_index,
                /* list of solutes ordered according to CMF_SOLUTES */
                lvector_t< double > const *  _s_bufs[])
{
    ldndc_assert( _kernel_index < this->kernel_cnt_);
    ldndc_assert( _s_bufs);

    for ( int  s = 0;  s < CMF_SOLUTES_CNT;  ++s)
    {
        ldndc_assert( _s_bufs[s]);
        ldndc_assert( _s_bufs[s]->size() == (unsigned int)SOIL_LAYER_CNT);
        _s_bufs[s]->copy_to_carray( this->s_buf_store_ +
                ( _kernel_index*SOIL_LAYER_FULL_CNT) +
                (s * SOIL_LAYER_CNT));

// sk:dbg        fprintf( stderr, "r:%d\t%d @ %d, offs=%llu\n", ldndc::mpi::comm_rank( PL_COMM_EXEC), (int)_kernel_index, s, (unsigned int long long)(( _kernel_index*SOIL_LAYER_FULL_CNT) + (s * SOIL_LAYER_CNT)));
// sk:dbg        LOGDEBUG( "ldndc: c=",ldndc::mpi::comm_rank( PL_COMM_EXEC), " krnl=",_kernel_index, " addr[",s,"]=",this->s_buf_, "offs=", (size_t)(( _kernel_index*SOIL_LAYER_FULL_CNT) + (s * SOIL_LAYER_CNT)));
    }
}

void
ldndc_cmf_transfer_t::compute_solutes_update()
{
    ldndc_assert( this->s_buf_);
    ldndc_assert( this->s_buf_store_);

#ifdef  _HAVE_OPENMP
#  pragma omp parallel for
#endif
    for ( int i = 0;  i < this->s_buf_size_;  ++i)
        this->s_buf_store_[i] = this->s_buf_[i] - this->s_buf_store_[i];
}

void
ldndc_cmf_transfer_t::update_solutes()
{
    ldndc_assert( this->s_buf_);
    ldndc_assert( this->s_buf_store_);

#ifdef  _HAVE_OPENMP
#  pragma omp parallel for
#endif
    for ( int i = 0;  i < this->s_buf_size_;  ++i)
    {
        this->s_buf_[i] += this->s_buf_store_[i];
        this->s_buf_[i] = std::max( this->s_buf_[i], 0.0);
    }
}


void
ldndc_cmf_transfer_t::get_water(
                size_t  _kernel_index,
                __LDNDC_SUBSTATE_CLASS(watercycle) *  _wc)
{
        ldndc_assert( _kernel_index < this->kernel_cnt_);

    double *  buf = this->w_buf_ + _kernel_index * WATER_RECORD_SIZE;

    /* water content */
    _wc->nd_wc_sl.copy_from_carray( buf);
    _wc->nd_wc_sl.dump_to_log( state_item_info_varname( _wc->nd_wc_sl.name_id));

    /* transpiration (nd_transp) */
    _wc->nd_transp = buf[W_TRANSP];
    /* evaporation from interception (nd_evacep) */
    _wc->nd_evacep = buf[W_EVACEP];
    /* evaporation from soil (nd_evasoil) */
    _wc->nd_evasoil = buf[W_EVASOIL];
    /* potential evaporation (nd_evapot) */
    _wc->nd_evapot = buf[W_EVAPOT];
// sk:TODO  needed? no!        /* water table */
// sk:TODO    _wc.water_table = buf[W_WTABLE];

#ifdef  _DEBUG
// sk:off    if ( _kernel_index == 0)
// sk:off    {
// sk:off        fprintf( stderr, "ldndc[i]:\ttransp=%f, evacep=%f, evasoil=%f, evapot=%f\n", _wc->nd_transp, _wc->nd_evacep, _wc->nd_evasoil, _wc->nd_evapot);
// sk:off        fprintf( stderr, "ldndc[b]:\ttransp=%f, evacep=%f, evasoil=%f, evapot=%f\n", buf[W_TRANSP], buf[W_EVACEP], buf[W_EVASOIL], buf[W_EVAPOT]);
// sk:off    }
#endif
}
int
ldndc_cmf_transfer_t::receive_water_data()
{
// sk:dbg    fprintf( stderr, "r:ldndc-run-time=%d/?, solutes-space-name=%s, solutes-object-name=%s\n", _time_step, this->space_name_.solutes, this->object_name_.solutes);
    int  rc = PALM_Get( this->space_name_.water, this->object_name_.water, &this->pl_no_time_, &this->pl_no_tag_, this->w_buf_+1);

    return  rc;
}


/* specialized landscape class */

cmf_palm_llandscape_t::cmf_palm_llandscape_t(
        ldndc::config_file_t const *  _config)
        : llandscape_t( _config),

          run_time_( 0),
          region_size_( 0)
{
}

cmf_palm_llandscape_t::~cmf_palm_llandscape_t()
{
}


lerr_t cmf_palm_llandscape_t::palm_initialize()
{
    LOGVERBOSE( "palm initialization started");

    LOGDEBUG( "#kernels=", this->get_full_cores_cnt());
    if ( this->get_full_cores_cnt() == 0u)
    {
        return  LDNDC_ERR_INPUT_EXHAUSTED;
    }

    int  node;
    MPI_Comm_rank( PL_COMM_EXEC, &node);

    if ( !this->kernels_[0])
    {
        LOGERROR( "make sure at least one kernel is active!");
        PALM_Abort();
    }

    int  pl_no_time( PL_NO_TIME);
    int  pl_no_tag( PL_NO_TAG);

    {
        // put the simulation run time
        char  space_name[PL_LNAME];
        opalm_name(space_name,"one_integer");
        char object_name[PL_LNAME];
        opalm_name(object_name,"run_time");
        this->run_time_ = this->clock_.running_time();
        PALM_Put( space_name, object_name, &pl_no_time, &pl_no_tag, &this->run_time_);
    }

    {
        // send soil properties to cmf
        char  space_name[PL_LNAME];
        opalm_name(space_name,"soilprops_space");
        char object_name[PL_LNAME];
        opalm_name(object_name,"soilprops");

        int  soilprops_buffer_size[1] = { 0};
        PALM_Space_get_shape( space_name, 1, soilprops_buffer_size);
        fprintf( stderr, "#kernels=%d, s-prop-buf-size=%d, s-prop-size=%d, s-prop-rec-size=%d\n", (int)(this->get_full_cores_cnt()), soilprops_buffer_size[0], SOIL_PROPERTIES_SIZE, SOIL_PROPERTIES_RECORD_SIZE);

        double *  soilprops_buffer = new double[soilprops_buffer_size[0]];
        for ( size_t  c = 0;  c < this->get_full_cores_cnt();  ++c)
        {
            lkernel_t *  krnl = this->kernels_[c];
            if ( krnl)
            {
                __LDNDC_SUBSTATE_CLASS(soilchemistry) const *  sc = krnl->state().get_substate< __LDNDC_SUBSTATE_CLASS(soilchemistry) >();
                __LDNDC_SUBSTATE_CLASS(watercycle) const *  wc = krnl->state().get_substate< __LDNDC_SUBSTATE_CLASS(watercycle) >();

                double *  s_props = soilprops_buffer + c*SOIL_PROPERTIES_RECORD_SIZE;

                lvector_t< double > const *  soilprops_v[SOIL_PROPERTIES_SIZE] = { &sc->h_sl, &sc->dens_sl, &sc->poro_sl, /*&sc->wcmin_sl,*/ &sc->wcmax_sl, &sc->sks_sl, &sc->clay_sl, &wc->nd_wc_sl};
                for ( size_t  k = 0;  k < SOIL_PROPERTIES_SIZE;  ++k)
                {
                    soilprops_v[k]->copy_to_carray( s_props + k*SOIL_LAYER_CNT);
                }
            }
        }
        PALM_Put( space_name, object_name, &pl_no_time, &pl_no_tag, soilprops_buffer+1);
        delete[]  soilprops_buffer;
    }

    {
        // create a transfer object and allocate solutes buffer
        this->region_size_ = this->get_full_cores_cnt();

        this->palm_transfers_ = ldndc_cmf_transfer_t( this->region_size_);
        this->palm_transfers_.alloc_buffers();
    }

    LOGVERBOSE( "palm initialization complete");
    return  LDNDC_ERR_OK;
}


#if  defined(_HAVE_OPENMP) && defined(_DEBUG)
#  define  __OPENMP_INFO(__reg__)                                \
{                                                \
    if ( __reg__ && ( cores_alive_ > 1))                            \
    {                                            \
        fprintf( stderr, "OpenMP: parallelizing onto #threads=%d, nested=%d\n",        \
                ldndc::omp::get_num_threads(),                    \
                omp_get_nested());                        \
        LOGDEBUG2( "OpenMP: parallelizing onto #threads", ldndc::omp::get_num_threads(),\
                "nested", lstring::n2s( omp_get_nested()));            \
        __reg__ = false;                                \
    }                                            \
}
#else
#  define  __OPENMP_INFO(__reg__)                                \
    LDNDC_NOOP
#endif  /* _DEBUG && _HAVE_OPENMP */


lerr_t
cmf_palm_llandscape_t::launch(
        unsigned int  /*_timesteps*/)
{
    /* exit function if no cores exist */
    if ( this->clock_.running_time() == 0)
    {
        LOGINFO( "launching simulation with zero time steps. goodbye.");
        return  LDNDC_ERR_OK;
    }

    if (( ! this->kernels_) || ( this->get_valid_cores_cnt() == 0))
    {
        LOGINFO( "launching simulation without active cores ;-)");
        return  LDNDC_ERR_OK;
    }
    LOGVERBOSE( "simulation run started");

    TIC(lruntime);

#ifdef  _HAVE_OPENMP
    /* set nested to enable parallelism in input class state update */
    omp_set_nested( 1);

    /* only show openMP message once at startup */
    bool  OPENMP_MSG( true);

    LOGINFO( "number of cpus: ", omp_get_num_procs(), ", maximum number of threads: ", omp_get_max_threads());
    LOGINFO( "number of cores: ", this->cores_alive_);
#endif  /* _HAVE_OPENMP */

    /* model time loop */
#ifdef  _HAVE_OPENMP
#  pragma omp parallel if( this->cores_alive_ > 1) num_threads(std::min(omp_get_max_threads(), (int)this->cores_alive_))
#endif
    int  ldndc_run_time = 0;
    fprintf( stderr, "ldndc-run-time=%d\n", this->run_time_);
    while ( ldndc_run_time++ < this->run_time_)
    {
        fprintf( stderr, "LDNDC-TIME=%d\n", ldndc_run_time);
#ifdef  _HAVE_OPENMP
#  pragma omp master
        __OPENMP_INFO(OPENMP_MSG);
#  pragma omp single
#endif  /* _HAVE_OPENMP */
        /* boundary conditions internal state update */
        if ( this->io_agent_.update( this->clock_) != LDNDC_ERR_OK)
        {
            LOGERROR( "detected error in input data. cannot recover. sorry, bye.");
#ifdef  _HAVE_OPENMP
            // TODO  deactivate all cores --> will abort simulation
#else
            return  LDNDC_ERR_FAIL;
#endif
        } /* implied barrier here! */

        this->palm_send_meteo_data( ldndc_run_time-1);

        this->palm_receive_water_data();

        this->palm_store_solutes();

        lkernel_t *  m_core( NULL);
        // In OpenMP 2.5, the iteration variable in for must be a signed integer variable type. In
        // OpenMP 3.0, it may also be an unsigned integer variable type, a pointer type or a constant-time
        // random access iterator type
        int  c( 0), lstat_buf_size_prior_to_step( 0);
        lerr_t  m_status( LDNDC_ERR_OK);
#ifdef  _HAVE_OPENMP
#  pragma omp for private(c,m_core,m_status,lstat_buf_size_prior_to_step)
#endif  /* _HAVE_OPENMP */
        for ( c = 0;  c < (int)cores_cnt_;  ++c)
        {
            m_core = this->kernels_[c];

            if ( m_core  &&  m_core->active())
            {
// sk:off                // sk:off #ifdef  _HAVE_STATUS_QUEUE
// sk:off                // sk:off                 lstat_buf_size_prior_to_step = lstat_buf.size();
// sk:off                // sk:off #endif  /* _HAVE_STATUS_QUEUE */
// sk:off                // TODO  add time measurements for load balancer (see work dispatcher)
                m_status = m_core->step();

                if ( m_status != LDNDC_ERR_OK)
                {
// sk:off                    if ( this->deactivate_core_( *m_core) != LDNDC_ERR_OK)
// sk:off                    {
// sk:off                        // TODO  use private status variable and query at top of loop
// sk:off                        LSTAT_INIT_ERRNO(lstat,LDNDC_ERR_INPUT_EXHAUSTED,LSTAT_ONCE);
// sk:off                        LSTAT_QUEUE(lstat,"no more cores alive :(");
// sk:off                    }
                }
                // sk:off #ifdef  _HAVE_STATUS_QUEUE
                // sk:off                 /* evaluate global status buffer... */
                // sk:off                 if ( (size_t)lstat_buf.size() - lstat_buf_size_prior_to_step)
                // sk:off                 {
                // sk:off                     LOGERROR( "errors occured... ");
                // sk:off                     // openmp: check for my id in new messages
                // sk:off                 }
                // sk:off #endif  /* _HAVE_STATUS_QUEUE */

            }
        } /* cores loop  (possibly openMP'd:  implicit barrier here) */

        // compute the update
        this->palm_compute_solutes_update();

        // receive solutes
        this->palm_receive_solutes_data();

        // update solutes
        this->palm_update_solutes();

        // send updated solutes
        this->palm_send_solutes_data();

//        llog::flush_logfile_stream();


        MPI_Barrier( PL_COMM_EXEC);
#ifdef  _HAVE_OPENMP
#  pragma omp barrier
#  pragma omp single
#endif
        /* next time step */
        this->clock_.advance();
    }

    TOC(lruntime);
    fprintf( stderr, "sim-time=%.8f\n", lruntime);

    LOGINFO( "model run finished [time=",this->clock_.fromto().to().to_string(),"]");
    return  ( this->get_valid_cores_cnt() == size_t(this->cores_alive_)) ? LDNDC_ERR_OK : LDNDC_ERR_RUN_INCOMPLETE;
}


#define  SOLUTE_VECTOR_(__kernel__,__solute_vector__) &this->kernels_[__kernel__]->state().get_substate< __LDNDC_SUBSTATE_CLASS(soilchemistry) >()->__solute_vector__
#define  SOLUTE_VECTOR_DOC(__kernel__)  SOLUTE_VECTOR_(__kernel__,doc_sl)
#define  SOLUTE_VECTOR_DON(__kernel__)  SOLUTE_VECTOR_(__kernel__,don_sl)
#define  SOLUTE_VECTOR_NH3(__kernel__)  SOLUTE_VECTOR_(__kernel__,nh3_sl)
#define  SOLUTE_VECTOR_NH4(__kernel__)  SOLUTE_VECTOR_(__kernel__,nh4_sl)
#define  SOLUTE_VECTOR_NO3(__kernel__)  SOLUTE_VECTOR_(__kernel__,no3_sl)
#define  SOLUTE_VECTOR_UREA(__kernel__)  SOLUTE_VECTOR_(__kernel__,urea_sl)

#define  SOLUTE_VECTORS(__kernel__)        \
    /* SOLUTE_VECTOR_DOC(__kernel__), */    \
    /* SOLUTE_VECTOR_DON(__kernel__), */    \
    /* SOLUTE_VECTOR_NH3(__kernel__), */    \
    /* SOLUTE_VECTOR_NH4(__kernel__), */    \
    SOLUTE_VECTOR_NO3(__kernel__)        \
    /* SOLUTE_VECTOR_UREA(__kernel__) */    \
    /* no more solutes ... */

lerr_t
cmf_palm_llandscape_t::palm_send_meteo_data(
        size_t  _timestep)
{
    /* NOTE  for now we assume all climate records to be identical, i.e. all kernels share one climate station */
    __LDNDC_INPUT_CLASS(climate) const *  meteo( this->kernels_[0]->input().get_input_class< __LDNDC_INPUT_CLASS(climate) >());
    this->palm_transfers_.set_meteo( _timestep, meteo);
    this->palm_transfers_.send_meteo_data();

    return  LDNDC_ERR_OK;
}

lerr_t
cmf_palm_llandscape_t::palm_send_solutes_data()
{
    for ( size_t  c = 0;  c < this->get_full_cores_cnt();  ++c)
    {
        if ( this->kernels_[c])
        {
            /* solutes */
            lvector_t< double > const *  solutes[CMF_SOLUTES_CNT] =
            {
                SOLUTE_VECTORS(c)
            };

            this->palm_transfers_.set_solutes( c, solutes);

            /* meteorological boundary conditions
             *
             * note that this requires substate microclimate to be set correctly.
             * this is is only true for mobile based kernels.
             */
// sk:off            this->palm_transfers_.set_meteo( c, mc);
        }
        else
        {
            // TODO  optimize out null kernels .. assume there are none
            LOGDEBUG( "kernel is null,  c=", c);
        }
    }

    /* send solutes buffer to cmf */
    this->palm_transfers_.send_solutes_data();

    return  LDNDC_ERR_OK;
}


lerr_t
cmf_palm_llandscape_t::palm_receive_solutes_data()
{
    /* receive solutes buffer from cmf */
    this->palm_transfers_.receive_solutes_data();
    //this->palm_transfers_.receive_water_data();

    return  LDNDC_ERR_OK;
}

lerr_t
cmf_palm_llandscape_t::palm_store_solutes()
{
    for ( size_t  c = 0;  c < this->get_full_cores_cnt();  ++c)
    {
        if ( this->kernels_[c])
        {
            /* solutes */
            lvector_t< double > const *  solutes[CMF_SOLUTES_CNT] =
            {
                SOLUTE_VECTORS(c)
            };

            this->palm_transfers_.store_solutes( c, solutes);

            /* meteorological boundary conditions
             *
             * note that this requires substate microclimate to be set correctly.
             * this is is only true for mobile based kernels.
             */
// sk:off            this->palm_transfers_.set_meteo( c, mc);
        }
        else
        {
            // TODO  optimize out null kernels .. assume there are none
            LOGDEBUG( "kernel is null,  c=", c);
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
cmf_palm_llandscape_t::palm_compute_solutes_update()
{
    // extract the solute states from the kernels
    for ( size_t  c = 0;  c < this->get_full_cores_cnt();  ++c)
    {
        if ( this->kernels_[c])
        {
            /* solutes */
            lvector_t< double > const *  solutes[CMF_SOLUTES_CNT] =
            {
                SOLUTE_VECTORS(c)
            };

            this->palm_transfers_.set_solutes( c, solutes);

            /* meteorological boundary conditions
             *
             * note that this requires substate microclimate to be set correctly.
             * this is is only true for mobile based kernels.
             */
// sk:off            this->palm_transfers_.set_meteo( c, mc);
        }
        else
        {
            // TODO  optimize out null kernels .. assume there are none
            LOGDEBUG( "kernel is null,  c=", c);
        }
    }

    // compute the update using the stored data
    this->palm_transfers_.compute_solutes_update();

    return  LDNDC_ERR_OK;
}

lerr_t
cmf_palm_llandscape_t::palm_update_solutes()
{
    this->palm_transfers_.update_solutes();

        /* distribute data to kernels */
    for ( size_t  c = 0;  c < this->get_full_cores_cnt();  ++c)
    {
        if ( this->kernels_[c])
        {
            /* solutes */
            lvector_t< double > *  solutes[CMF_SOLUTES_CNT] =
            {
                SOLUTE_VECTORS(c)
            };

            this->palm_transfers_.get_solutes( c, solutes);
            //__LDNDC_SUBSTATE_CLASS(watercycle) *  wc = this->kernels_[c]->state().get_substate< __LDNDC_SUBSTATE_CLASS(watercycle) >();
            //this->palm_transfers_.get_water( c, wc);
        }
        else
        {
            // TODO  optimize out null kernels .. assume there are none
            LOGDEBUG( "kernel is null,  c=", c);
        }
    }

    return  LDNDC_ERR_OK;
}

lerr_t
cmf_palm_llandscape_t::palm_receive_water_data()
{
    /* receive solutes buffer from cmf */
    //this->palm_transfers_.receive_solutes_data();
    this->palm_transfers_.receive_water_data();

    /* distribute data to kernels */
    for ( size_t  c = 0;  c < this->get_full_cores_cnt();  ++c)
    {
        if ( this->kernels_[c])
        {
            /* solutes
            lvector_t< double > *  solutes[CMF_SOLUTES_CNT] =
            {
                SOLUTE_VECTORS(c)
            };*/

            //this->palm_transfers_.get_solutes( c, solutes);
            __LDNDC_SUBSTATE_CLASS(watercycle) *  wc = this->kernels_[c]->state().get_substate< __LDNDC_SUBSTATE_CLASS(watercycle) >();
            this->palm_transfers_.get_water( c, wc);
        }
        else
        {
            // TODO  optimize out null kernels .. assume there are none
            LOGDEBUG( "kernel is null,  c=", c);
        }
    }

    return  LDNDC_ERR_OK;
}

