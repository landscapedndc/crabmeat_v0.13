
abort "Run 'source [MUSCLE_HOME]/etc/muscle.profile' before this script" if not ENV.has_key?('MUSCLE_HOME')

# configure cxa properties
cxa = Cxa.LAST

ldndc_home = ENV['LDNDC_HOME']
puts "#{ldndc_home}"
abort "set LDNDC_HOME" if not ENV.has_key?('LDNDC_HOME')

# declare kernels
climate_mapper = Instance.new( 'climate_mapper', 'muscle.core.kernel.DuplicationMapper')

climate = NativeInstance.new( 'climate', "#{ldndc_home}/ldndc-comp")

biogeochem_1 = NativeInstance.new( 'biogeochem_1', "#{ldndc_home}/ldndc-comp")
biogeochem_2 = NativeInstance.new( 'biogeochem_2', "#{ldndc_home}/ldndc-comp")

cxa.env['default_dt'] = 1
cxa.env['max_timesteps'] = 1000

climate['LDNDC_USE_KERNEL'] = 'climate.ldndc'
climate['dt'] = 1
climate['name'] = 'climate boundary data provider'
climate['out.climate'] = 'climate'

biogeochem_1['LDNDC_USE_KERNEL'] = 'test:consumer'
biogeochem_1['name'] = 'biogeochemistry model 1'
biogeochem_1['in.climate'] = 'climate'
biogeochem_1['dt'] = 1

biogeochem_2['LDNDC_USE_KERNEL'] = 'test:consumer'
biogeochem_2['name'] = 'biogeochemistry model 2'
biogeochem_2['in.climate'] = 'climate'
biogeochem_2['dt'] = 1

cxa.env["configuration_file"] = "~/.ldndc/ldndc.conf"
cxa.env["project_file"] = "~/projects/arable/FR_grignon/FR_grignon.xml"


climate.couple( climate_mapper, 'climate', ['console'], ['console'])

climate_mapper.couple( biogeochem_1, { 'climate_1' => biogeochem_1['in.climate']})
climate_mapper.couple( biogeochem_2, { 'climate_2' => biogeochem_2['in.climate']})


## 
cxa.env["hydrology:comm.request"] = "climate solutes"
cxa.env["hydrology:comm.reply"] = "solutes water"
cxa.env["biogeochem:comm.request"] = "solutes water"
cxa.env["biogeochem:comm.reply"] = "climate solutes"

