
package supkernels;

import muscle.core.standalone.NativeKernel;

public class ClimateLdndcNative extends NativeKernel
{
	public void addPortals()
	{
		addEntrance( "climate", double[].class);
	}
}

