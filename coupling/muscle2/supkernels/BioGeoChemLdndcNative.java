
package supkernels;

import muscle.core.standalone.NativeKernel;

public class BioGeoChemLdndcNative extends NativeKernel
{
	public void addPortals()
	{
		addExit( "climate", double[].class);
	}
}

