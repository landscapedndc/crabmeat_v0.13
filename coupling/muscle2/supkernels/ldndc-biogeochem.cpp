
#include  "ldndc-client.h"
#include  "landscape/landscapedndc.h"

#include  <stdio.h>

#include  <muscle2/cppmuscle.hpp>

#ifdef  _HAVE_MPI_LDNDC
#  include  "io/work-dispatcher/work-dispatcher-roundrobin.h"
#else
namespace ldndc {
class  work_dispatcher_t;
class  work_dispatcher_roundrobin_t;
}
#endif

#include  "utils/lutils-env.h"
#include  "lkerneldefault.h"
void
ldndc_export_default_kernelname()
{
#define  LDNDC_USE_KERNEL  "LDNDC_USE_KERNEL"
    std::string  dflt_kernel = DEFAULT_KERNEL_NAME;
    if ( lutil::getenv( LDNDC_USE_KERNEL))
    {
        dflt_kernel = lutil::getenv( LDNDC_USE_KERNEL);
    }
    if ( muscle::cxa::has_property( LDNDC_USE_KERNEL))
    {
        dflt_kernel = muscle::cxa::get_property( LDNDC_USE_KERNEL);
    }
    lutil::setenv( LDNDC_USE_KERNEL, dflt_kernel.c_str(), 1);
}

lerr_t
run_sim_text(
        char const *  _project_fname,
        char const *  _cfg_fname,
        char const *  _schedule)
{
        lerr_t  rc( LDNDC_ERR_OK);
    {

#ifdef  _HAVE_MPI_LDNDC
    ldndc::work_dispatcher_roundrobin_t  wd( 0);
    wd.set_rank( ldndc::mpi::comm_rank( ldndc::mpi::comm_ldndc));
    wd.set_size( ldndc::mpi::comm_size( ldndc::mpi::comm_ldndc));
    ldndc::work_dispatcher_t *  this_work_dispatcher = &wd;
#else
    ldndc::work_dispatcher_t *  this_work_dispatcher = NULL;
#endif

    ldndc::llandscapedndc_t  ldndc_reg(
            _project_fname, _schedule, _cfg_fname, this_work_dispatcher);
    if ( ldndc_reg.status != 0)
    {
        fprintf( stderr, "setting up ldndc instance failed with status %d\n", (int)ldndc_reg.status);
        ldndc_reg.finalize();
        return  LDNDC_ERR_FAIL;
    }

    ldndc::mpi::barrier( ldndc::mpi::comm_ldndc);

#ifdef  _HAVE_SERIALIZE
    if ( ldndc_reg.restore_core_states() > 0)
    {
        ldndc_reg.finalize();
        return  LDNDC_ERR_OBJECT_INIT_FAILED;
    }
#endif
    int const  dt = atoi( muscle::cxa::get_property( "dt").c_str());
    while ( !muscle::env::will_stop())
    {
        rc = ldndc_reg.step( dt);
        if ( rc == LDNDC_ERR_RUN_INCOMPLETE)
        {
            LOGWARN( "there were ",ldndc_reg.get_valid_kernels_cnt() - ldndc_reg.get_active_kernels_cnt()," crashed kernels");

            // TODO  get list of crashed kernel IDs
        }
    }
#ifdef  _HAVE_SERIALIZE
    else
    {
        if ( ldndc_reg.dump_core_states() > 0)
        {
            ldndc_reg.finalize();
            return  LDNDC_ERR_FAIL;
        }
    }
#endif
    ldndc_reg.finalize();

#ifdef  _LDNDC_MONITOR_RESOURCES
    ldndc_reg.dump_resources();
#endif
    }

    /* at this point the logging system has been deinitialized: do not use it */

    return  rc;
}


int
main(
        int  _argc,
        char **  _argv)
{
//    __LDNDC_APPLICATION_INIT__(_argc,_argv,NULL,NULL,0)

//    (void)ldndc_ieee_set_mode( LDNDC_FE_DIVBYZERO|LDNDC_FE_OVERFLOW|/*LDNDC_FE_UNDERFLOW|*/LDNDC_FE_INVALID);

    (void)ldndc_signal_traps_init();
    (void)ldndc_have_signal_trap( LDNDC_SIGFPE);
    (void)ldndc_have_signal_trap( LDNDC_SIGSEGV);
    (void)ldndc_have_signal_trap( LDNDC_SIGUSR1);

    muscle::env::init( &_argc, &_argv);

    std::string  config_file = muscle::cxa::get_property( "configuration_file");
    std::string  project_file = muscle::cxa::get_property( "project_file");

    ldndc_export_default_kernelname();

    lerr_t  rc = run_sim_text( project_file.c_str(), config_file.c_str(), NULL);

    fprintf( stderr, "node %4d: %s.\n",
            ldndc::mpi::comm_rank( ldndc::mpi::comm_ldndc),
            ( rc) ? "premature termination or incomplete run" : "normal termination");


    muscle::env::finalize();
    
//    __LDNDC_APPLICATION_DEINIT__(_argc,_argv,NULL,NULL,0)
    return  ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;

}

