
## source muscle environment
. ~/opt/muscle/etc/muscle.profile

BINDADDR=localhost
BINDPORT=9001

CXA="~/ldndc-dev/coupling/muscle2/cxa/ldndc-cb.cxa.rb"

export LDNDC_USE_KERNEL=climate.ldndc
muscle2 --bindaddr $BINDADDR --bindport $BINDPORT --cxa ${CXA} --main climate &

sleep 2

export LDNDC_USE_KERNEL=test:consumer
muscle2 --manager $BINDADDR:$BINDPORT --cxa ${CXA} climate_mapper biogeochem_1 biogeochem_2


echo  DONE.

