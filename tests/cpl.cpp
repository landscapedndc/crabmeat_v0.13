/*!
 * @brief
 *    test app for coupler implementation
 *
 * @author
 *    steffen klatt (created on: nov 01, 2016),
 */

#include  "cpl/cbm_cpl.h"
#include  "log/cbm_baselog.h"

#include  <stdio.h>
#include  <cstring>
#include  <vector>

struct field_t
{
    char const *  name;
    char const *  unit;
    int  size;
};

field_t const  publish_fields[] =
{
#define HS_size 256
#define Hs 0
    { "f_one", "cm", HS_size },
#define Hs_alias 1
    { "f_one", "m", HS_size },
    { "f_two", "kg", 5 },
#define HS_dynsize 13
#define Hs_dyn 3
    { "f_three", "mm/min", CBM_DynamicSize },
//    { "f_three", "m/h", 2 },
    { "f_four/three/two/one", "s", 24 },
    { NULL, NULL, 0 }
};
field_t const  subscribe_fields[] =
{
    { NULL, NULL, 0 },
    { "f_one", "m", 0 },
#define Hr 0
    { "f_one", "m", 0 },
    { "f_one", "cm", 0 },
//    { "f_two", "g", 0 },
//    { "f_two", "t", 0 },
#define Hr_dyn 3
    { "f_three", "m/s", 0 },
//    { "f_four", "h", 0 },
//    { "f_four", "h", 0 },
//    { "f_four", "d", 0 },
//    { "f_four", "a", 0 },
//
#define Hr_nopub 3
    { "f_notpublished", CBM_NoUnit, 0 },

    { NULL, NULL, 0 }
};

std::vector< CBM_Handle >  publish( CBM_Coupler  _cpl)
{
    int  rc;
    field_t const *  p = publish_fields;
    char  uri[CBM_UriSize];
    std::vector< CBM_Handle >  hv;
    while ( p->name)
    {
        snprintf( uri, CBM_UriSize, "/%s?%s=%d&%s=%d&%s=%s", p->name,
            CBM_UriQSpatial, 1, CBM_UriQTemporal, 1, CBM_UriQUnit, (p->unit ? p->unit : ""));

        CBM_Handle  handle = CBM_CouplerPublishField( _cpl, uri,
                NULL, p->size, CBM_DOUBLE, 0, &rc);
        CBM_LogWrite( "publish:  handle for '",p->name,"'",
            " [",p->unit,"] is ",CBM_HandleId(handle), "  [slot=",hv.size(),"]",
            " <",uri,">", "\n");

        if ( CBM_HandleOk(handle))
            { hv.push_back( handle); }

        ++p;
    }
    return  hv;
}

std::vector< CBM_Handle >  subscribe( CBM_Coupler  _cpl)
{
    int  rc;
    field_t const *  p = subscribe_fields;
    char  uri[CBM_UriSize];
    std::vector< CBM_Handle >  hv;
    while ( p->name)
    {
        CBM_Transform *  transf = 0;
        if (( strcmp( p->name, "f_one") == 0)
            || ( strcmp( p->name, "f_three") == 0))
        {
// sk:off            temporal.dT = 4;
            transf = CBM_TransformDotSum;
        }
        snprintf( uri, CBM_UriSize, "/%s?%s=%d&%s=%d&%s=%s", p->name,
            CBM_UriQSpatial, 1, CBM_UriQTemporal, 1, CBM_UriQUnit, (p->unit ? p->unit : ""));

        CBM_Handle  handle = CBM_CouplerSubscribeField( _cpl,
            uri, transf, CBM_CallbackNone, 0, &rc);
        CBM_LogWrite( "subscribe:  handle for '",p->name,"'",
                " [",p->unit,"] is ", CBM_HandleId(handle),
            " <",uri,">");
        if ( CBM_HandleOk(handle))
            { hv.push_back( handle); }

        ++p;
    }
    return  hv;
}

#include  "containers/cbm_set.h"
static CBM_Set manyhandles;
int  publish_many( CBM_Coupler  _cpl)
{
    manyhandles = CBM_SetCreate(
        CBM_MemPoolRoot, sizeof(CBM_Handle), 100, 0);

    field_t const *  p = &publish_fields[0];
    char  p_name[CBM_UriSize];
    int  i = 0;
#define  PUB_SZ 2000
    while ( i<PUB_SZ)
    {
        sprintf( p_name, "/%s.%d?%s=0", p->name, i, CBM_UriQSpatial);
        CBM_Handle  handle = CBM_CouplerPublishField( _cpl,
                p_name, NULL, p->size, CBM_DOUBLE, 0, NULL);
        CBM_Handle *  h = (CBM_Handle*)CBM_SetInsert(
                manyhandles, p_name, NULL);
        *h = handle;

        i += 1;
    }
    CBM_LogWrite( "set-size=", CBM_SetSize( manyhandles));
    CBM_Handle *  hdl = (CBM_Handle*)CBM_SetFindHead( manyhandles).value;
    CBM_LogWrite( "i am handle ", CBM_CouplerGetFieldUri( _cpl, *hdl));
    return  PUB_SZ;
}

void  unsubscribe( CBM_Coupler  _cpl,
        std::vector< CBM_Handle > &  _handles)
{
    for ( size_t  k = _handles.size(); k != 0; --k)
    {
        CBM_Handle  handle = _handles[k-1];
        CBM_CouplerUnsubscribeField( _cpl, handle, NULL);
    }
}

void  unpublish( CBM_Coupler  _cpl,
        std::vector< CBM_Handle > &  _handles)
{
    for ( size_t  k = _handles.size(); k != 0; --k)
    {
        CBM_Handle  handle = _handles[k-1];
        CBM_CouplerUnpublishField( _cpl, handle, NULL);
    }
}

void  setarray( double *  _a, size_t  _sz,  double  _value)
{
    for ( size_t k=0; k<_sz; ++k)
        { _a[k] = _value; }
}
void  writearray( char const *  _name, double *  _a, size_t  _sz)
{
    printf( "%s=", _name);
    for ( size_t k=0; k<_sz; ++k)
        { printf( "%f;", _a[k]); }
    printf( "\n");
}

int
main( int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);

//    CBM_SetLogLevel( LOGLEVEL_WARN);
    int  rc_unitsinit = cbm_units_t::initialize( NULL);
    if ( rc_unitsinit)
        { exit( 1); }

    int  rc;
    CBM_Coupler  cpl = CBM_CouplerCreate( "TestCoupler", NULL, NULL, &rc);
    if ( rc)
    {
        CBM_LogError( "Failed to create coupler");
    }
    else
    {
//        bool  no_send = false;
//        (void)no_send;
//
//        CBM_LogWrite( "\npublish...");
//        std::vector< CBM_Handle >  pub_hdl;
//        pub_hdl = publish( cpl);
//        if ( pub_hdl.size()==0)
//            { no_send = true; }
//
//        CBM_LogWrite( "\nsubscribe...");
//        std::vector< CBM_Handle >  sub_hdl;
//        sub_hdl = subscribe( cpl);
//        if ( sub_hdl.size()==0)
//            { no_send = true; }
//
//
//        if ( !no_send)
//        {
//            /* fixed size */
//            CBM_Double  s[HS_size];
//            setarray( s, HS_size, 1.0);
//            writearray( "s", s, HS_size);
//            CBM_Double  r[HS_size];
//            setarray( r, HS_size, 0.0);
//            writearray( "r", r, HS_size);
//
//#define NB_S 5
//            CBM_LogWrite( "\n\nsend ", NB_S," times ...");
//            for ( int  nb_s = 0; nb_s < NB_S; ++nb_s)
//            {
//                CBM_CouplerSendField( cpl, pub_hdl[Hs], s, publish_fields[Hs].size, CBM_DOUBLE, 0, &rc);
//            }
//            CBM_LogWrite( "\nreceive...");
//            CBM_CouplerReceiveField( cpl, sub_hdl[Hr], r, publish_fields[Hs].size, CBM_DOUBLE, 0, &rc);
//            writearray( "r", r, HS_size);
//
//            CBM_LogWrite( "\n\nsend from alias ...");
//            CBM_CouplerSendField( cpl, pub_hdl[Hs_alias], s, publish_fields[Hs_alias].size, CBM_DOUBLE, 0, &rc);
//
//            CBM_LogWrite( "\nreceive...");
//            CBM_CouplerReceiveField( cpl, sub_hdl[Hr], r, publish_fields[Hs].size, CBM_DOUBLE, CBM_RECV_CLEAR, &rc);
//            writearray( "r", r, HS_size);
//
//            CBM_LogWrite( "\nping...");
//            int  hr_nopub = CBM_CouplerPingField( cpl, sub_hdl[Hr_nopub], NULL, NULL);
//            CBM_LogWrite( "size(\"",subscribe_fields[Hr_nopub].name,"\")=", hr_nopub);
//            CBM_FieldPingInfo  finfo = CBM_FieldPingInfoNone;
//            int  hr_pub = CBM_CouplerPingField( cpl, sub_hdl[Hr], &finfo, NULL);
//            CBM_LogWrite( "size(\"",subscribe_fields[Hr].name,"\")=", hr_pub);
//
//            
//            /* dynamic size */
//            CBM_LogWrite( "\n\nsend from dynamic size ...");
//            CBM_Double  s_dyn[HS_dynsize];
//            setarray( s_dyn, HS_dynsize, 3.0);
//            writearray( "s-dyn", s_dyn, HS_dynsize);
//            CBM_CouplerSendField( cpl, pub_hdl[Hs_dyn], s_dyn, HS_dynsize, CBM_DOUBLE, 0, &rc);
//            CBM_CouplerSendField( cpl, pub_hdl[Hs_dyn], s_dyn, HS_dynsize, CBM_DOUBLE, 0, &rc);
//            CBM_CouplerSendField( cpl, pub_hdl[Hs_dyn], s_dyn, HS_dynsize, CBM_DOUBLE, 0, &rc);
//
//            int  hr_dyn = CBM_CouplerPingField( cpl, sub_hdl[Hr_dyn], NULL, NULL);
//            CBM_LogWrite( "size(\"",subscribe_fields[Hr_dyn].name,"\")=", hr_dyn);
//            CBM_Double  r_dyn[HS_dynsize];
//            setarray( r_dyn, HS_dynsize, 0.0);
//            CBM_LogWrite( "\n\nreceive from dynamic size ...");
//            CBM_CouplerReceiveField( cpl, sub_hdl[Hr_dyn], r_dyn, hr_dyn, CBM_DOUBLE, 0, &rc);
//            writearray( "r-dyn", r_dyn, HS_dynsize);
//        }
//
//        CBM_LogWrite( "\ncurrently published: ",CBM_CouplerNumberOfRegisteredFields( cpl));
//        CBM_LogWrite( "\nunsubscribe...");
//        unsubscribe( cpl, sub_hdl);
//        CBM_LogWrite( "\ncurrently published: ",CBM_CouplerNumberOfRegisteredFields( cpl));
//        CBM_LogWrite( "\nunpublish...");
//        unpublish( cpl, pub_hdl);


        publish_many( cpl);
        CBM_LogWrite( "\ncurrently published: ",CBM_CouplerNumberOfRegisteredFields( cpl));

        CBM_CouplerDestroy( cpl);
    }

    cbm_units_t::finalize();
}

