/*!
 * @brief
 *    sandbox app for unqlite
 *
 * @author
 *    steffen klatt (created on: dec 10, 2014),
 */

#include  "nosql/cbm_kvstore.h"
#include  "string/cbm_string.h"
#include  "log/cbm_baselog.h"

#include  <stdlib.h>
#include  <stdio.h>

#if 0
char const *  kv_key = "foo";
unqlite *  db_1;
unqlite *  db_2;

void
unqlite_db_insert(
        unqlite *  _db, char const *  _kv_value)
{
    CBM_LogWrite( "write..");

    int  rc_insert = unqlite_kv_store_fmt( _db,
        kv_key, cbm::strlen( kv_key), "package name=\"%s\"", _kv_value);
    if ( rc_insert)
    {
        fprintf( stderr, "unqlite_kv_store_fmt() failed\n");
    }
}

void
unqlite_db_retrieve(
        unqlite *  _db, char const *  _db_name)
{
    CBM_LogWrite( "read..");

    char  value[2048];
    unqlite_int64  n_value;

    int  rc_retrieve = unqlite_kv_fetch( _db,
            kv_key, cbm::strlen( kv_key), value, &n_value);
    if ( rc_retrieve)
    {
        fprintf( stderr, "unqlite_kv_fetch() failed\n");
        return;
    }

    value[n_value] = '\0';
    printf( "%s[%s]=%s\n", kv_key, _db_name, value);
}
#endif

struct  pod_t
{
    double  d;
    int  i;
    char  c;
    float  f;
    char  s[4];
};

void
unqlite_unset_pod(
        struct pod_t *  _p)
{
    _p->d = invalid_dbl;
    _p->i = invalid_int;
    _p->c = '-';
    _p->f = invalid_flt;
    _p->s[0] = '\0';
}
void
unqlite_set_pod(
        struct pod_t *  _p)
{
    _p->d = -9.123000003141;
    _p->i = 23;
    _p->c = 'c';
    _p->f = 3.141;
    _p->s[0] = 'A'; _p->s[1] = 'B'; _p->s[2] = 'C'; _p->s[3] = '\0';
}
void
unqlite_write_pod(
        char const *  _ti, struct pod_t *  _p)
{
    _p->s[3] = '\0';
    fprintf( stdout, "%s:  float=%0.12f;  double=%.12f; char=%c;  int=%d;  string=%s\n",
            _ti, _p->f, _p->d, _p->c, _p->i, _p->s);
}
void
unqlite_storefetch_pod(
        cbm::kvstore_t *  _db)
{
    pod_t  p;

    unqlite_unset_pod( &p);
    unqlite_write_pod( " p clr", &p);
    unqlite_set_pod( &p);
    unqlite_write_pod( " p ini", &p);
    _db->store_pod( "pod", p);
    /* retrieve back */
    pod_t  p_cpy;
    unqlite_unset_pod( &p_cpy);
    unqlite_write_pod( "p' clr", &p_cpy);
    _db->fetch_pod( &p_cpy, "pod");
    unqlite_write_pod( "p' ini", &p_cpy);
}

int
main(
        int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);

    cbm::kvstore_t  db;
    if ( !db.is_connected())
    {
        return  EXIT_FAILURE;
    }

    db.store( "string", "hello world");
// sk:off    cbm::string_t  hw;
// sk:off           /*lerr_t  rc_fetch = */db.fetch( &hw, "string");
// sk:off    printf( "%s=\"%s\"\n", "string", hw.c_str());

// sk:off    db.store( "float64", "3.141");
// sk:off    cbm::string_t  pi;
// sk:off    /*lerr_t  rc_fetch_float64 = */db.fetch( &pi, "float64");
// sk:off    printf( "%s=%f\n", "float64", pi.as_float64());

    ldndc_flt64_t  pi_flt = ldndc::invalid_t< ldndc_flt64_t >::value;
    db.fetch_pod< ldndc_flt64_t >( &pi_flt, "float64");
    printf( "%s=%f\n", "float64", pi_flt);


    unqlite_storefetch_pod( &db);

    char const *  listvalues[] = { "1", "2", "3", "4", "5", "6", "7", NULL};
    char  listvalue[2048];
    ldndc_int64_t  listlength = 0;
//    db.store( "list", "0");
    int  j = 0;
    while ( listvalues[j])
    {
        db.append( "list", -1, listvalues[j], -1, "&");
        db.length( "list", -1, &listlength);
        db.fetch( "list", -1, listvalue, listlength);
        listvalue[listlength] = '\0';
        printf( "|list|=%4ld list='%s'\n", (long int)listlength, listvalue);
        ++j;
    }



// sk:off    db.store( "float64_array", "1.0 2.0 3.0 4.0 5.0");
// sk:off    cbm::string_t::float64_array_t  float64_array;
// sk:off    db.fetch_as_array< ldndc_flt64_t >( &float64_array, "float64_array");
// sk:off    printf( "float64_array( )=[");
// sk:off    for ( size_t  k = 0;  k < float64_array.size();  ++k)
// sk:off    {
// sk:off        printf( " %.3f", float64_array[k]);
// sk:off    }
// sk:off    printf( "]\n");
// sk:off    db.store( "float64_array_komma", "1.0e-3,   .20,3.0,4.0,6.0,  ,  ,");
// sk:off    cbm::string_t::float64_array_t  float64_array_komma;
// sk:off    db.fetch_as_array< ldndc_flt64_t >( &float64_array_komma, "float64_array_komma", -1, ',');
// sk:off    printf( "float64_array(,)=[");
// sk:off    for ( size_t  k = 0;  k < float64_array_komma.size();  ++k)
// sk:off    {
// sk:off        printf( " %.6e", float64_array_komma[k]);
// sk:off    }
// sk:off    printf( "]\n");

    {
    lerr_t  rc_remove = db.remove( "string");
    if ( rc_remove)
    {
        fprintf( stderr, "no such item \"%s\"\n", "string");
    }
    else
    {
        fprintf( stderr, "removed item \"%s\"\n", "string");
    }
    }
    {
    lerr_t  rc_remove = db.remove( "string");
    if ( rc_remove)
    {
        fprintf( stderr, "no such item \"%s\"\n", "string");
    }
    else
    {
        fprintf( stderr, "removed item \"%s\"\n", "string");
    }
    }

    static char const *  WORDS = "Copies the first n characters from the array of characters pointed by s.";
    cbm::string_t  words( WORDS);
    cbm::string_t::string_array_t  string_array =
        words.as_string_array( ' ');
    fprintf( stdout, "exploding \"%s\"\n", WORDS);
    for ( size_t  w = 0;  w < string_array.size();  ++w)
    {
        fprintf( stdout, "[%2d] \"%s\"\n", (int)w, string_array[w].c_str());
    }

#if 0
    {
    int  rc_open = unqlite_open( &db_1, ":mem:", UNQLITE_OPEN_IN_MEMORY);
    if ( rc_open != UNQLITE_OK)
    {
        __LDNDC_APPLICATION_DEINIT__(argc,_argv,NULL,NULL,LDNDC_APPFLAG_DEINITLOG)
        return  EXIT_FAILURE;
    }
    }

    {
    int  rc_open = unqlite_open( &db_2, ":mem:", UNQLITE_OPEN_IN_MEMORY);
    if ( rc_open != UNQLITE_OK)
    {
        __LDNDC_APPLICATION_DEINIT__(argc,_argv,NULL,NULL,LDNDC_APPFLAG_DEINITLOG)
        return  EXIT_FAILURE;
    }
    }

    unqlite_db_insert( db_1, "ONE");
    unqlite_db_insert( db_2, "TWO");

    unqlite_db_retrieve( db_1, "1");
    unqlite_db_retrieve( db_2, "2");

    {
    int  rc_close = unqlite_close( db_1);
    if ( rc_close != UNQLITE_OK)
    {
        __LDNDC_APPLICATION_DEINIT__(argc,_argv,NULL,NULL,LDNDC_APPFLAG_DEINITLOG)
        return  EXIT_FAILURE;
    }
    }

    unqlite_db_retrieve( db_2, "2");

    {
    int  rc_close = unqlite_close( db_2);
    if ( rc_close != UNQLITE_OK)
    {
        __LDNDC_APPLICATION_DEINIT__(argc,_argv,NULL,NULL,LDNDC_APPFLAG_DEINITLOG)
        return  EXIT_FAILURE;
    }
    }
#endif
}

