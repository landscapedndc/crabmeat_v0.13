/*!
 * @brief
 *    testing and learning tinyXML-2
 *
 * @author
 *    steffen klatt (created on: aug 12, 2012),
 *    edwin haas
 */


#include  "string/lstring-convert.h"
#include  "log/cbm_baselog.h"

#include  <stdlib.h>
#include  <tinyxml2.h>

#define  TINYXML_TEST_FILE  "./test/data/test.xml"

lerr_t
test_tinyxml2()
{
    tinyxml2::XMLDocument *  xmldoc = new tinyxml2::XMLDocument();

    if ( !xmldoc)
    {
        CBM_LogError( "allocating xml-document failded");
        return  LDNDC_ERR_FAIL;
    }

    xmldoc->LoadFile( TINYXML_TEST_FILE);

    if ( xmldoc->Error())
    {
        CBM_LogInfo( "error occured during load,  error=",xmldoc->ErrorID());
        if ( xmldoc->GetErrorStr1())
        {
            CBM_LogInfo( "    error diagnostic-1:",xmldoc->GetErrorStr1());
        }
        if ( xmldoc->GetErrorStr2())
        {
            CBM_LogInfo( "    error diagnostic-2: ",xmldoc->GetErrorStr2());
        }

        xmldoc->PrintError();

        delete  xmldoc;

        return  LDNDC_ERR_FAIL;
    }
    else
    {
        CBM_LogInfo( "successfully opened file");
    }

    // not available  xmldoc->SetTabSize( 4);

    tinyxml2::XMLNode *  xmlelem = xmldoc->RootElement();
    CBM_LogWrite( "root-elem-name=", xmlelem->Value());

    tinyxml2::XMLElement *  gblk = xmlelem->FirstChildElement( "global");
    CBM_LogWrite( "global-elem-name=", gblk->Value(), "  time=", gblk->Attribute( "time"));

    CBM_LogWrite( "parent=", gblk->Parent()->Value());

    tinyxml2::XMLElement *  cblk = xmlelem->FirstChildElement( "setup");
    lid_t  cid( invalid_lid);
    cbm::s2n( cblk->Attribute( "id"), &cid);
    double  dbl( 0.0);
    cbm::s2n( cblk->Attribute( "dbl"), &dbl);
    CBM_LogWrite( "id=", cid, "  model=", cblk->Attribute( "model"), "  dbl=", dbl);

    cblk = cblk->NextSiblingElement( "setup");
    cbm::s2n( cblk->Attribute( "id"), &cid);
    CBM_LogWrite( "id=", cid, "  model=", cblk->Attribute( "model"));

    delete  xmldoc;

    return  LDNDC_ERR_OK;
}

int
main(
        int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);
    test_tinyxml2();
}

