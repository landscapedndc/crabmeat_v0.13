/*!
 * @brief
 *    test app for hashmap implementation
 *
 * @author
 *    steffen klatt (created on: mar 15, 2014),
 */

#include  "containers/ltinyhashmap.h"
#include  "log/cbm_baselog.h"

#include  "resources/Lresources.h"

void
read_from_Lresources()
{
    if ( CBM_DefaultResources.read( NULL) != LDNDC_ERR_OK)
    {
        CBM_LogError( "failed to read Lresources");
        return;
    }

    size_t  number_of_predefined_species = 0;
    CBM_DefaultResources.query( "species.count", &number_of_predefined_species);
    CBM_LogWrite( "number_of_predefined_species=",number_of_predefined_species);

    CBM_LogWrite( "piab longname=",CBM_DefaultResources.find( "species.PIAB.name"));
    CBM_LogWrite( "#entries matching 'species.piab'=",CBM_DefaultResources.find_num( "^species.PIAB"));
    CBM_LogWrite( "#entries matching 'species.piab'=",CBM_DefaultResources.find_num( "^species.PIAB.*"));
    CBM_LogWrite( "#entries matching 'species.piab'=",CBM_DefaultResources.find_num( "^species.PIAB*$"));
    CBM_LogWrite( "#entries matching 'species.piab'=",CBM_DefaultResources.find_num( "^species.PIAB$"));
}

int
main(
        int  argc,
        char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);

    cbm::tinyhashmap_t< 23, int, 1, 64 >  ht;
    ht.set( "foo1", 2);
    ht.set( "foo2", 12);
    ht.set( "foo3", -4);

    CBM_LogWrite( "foo1=", *ht.get( "foo1"));
    CBM_LogWrite( "foo2=", *ht.get( "foo2"));
    CBM_LogWrite( "foo3=", *ht.get( "foo3"));

    cbm::tinyhashmap_t< 23, char, 12, 64 >  ht_char;
    ht_char.set( "foo1", "abc", 4);
    ht_char.set( "foo2", "mash", 5);
    ht_char.set( "foo3", "a", 2);

    CBM_LogWrite( "foo1=", ht_char.get( "foo1"));
    CBM_LogWrite( "foo2=", ht_char.get( "foo2"));
    CBM_LogWrite( "foo3=", ht_char.get( "foo3"));

    read_from_Lresources();
}

