/*!
 * @brief
 *    sandbox app for CBM_Set
 *
 * @author
 *    steffen klatt (created on: nov 23, 2018),
 */

#include  "containers/cbm_set.h"
#include  "memory/cbm_mem.h"
#include  "log/cbm_baselog.h"

#include  <stdlib.h>
#include  <stdio.h>

struct SetElement
{
	int a;
	double b;
};

void SetIterate( char const * note, CBM_Set set )
{
	CBM_LogWrite( "--- ", note, " [", CBM_SetSize( set ), "]" );
	CBM_SetNodeIterator i = CBM_SetFindHead( set );
	while ( !CBM_SetNodeIteratorIsEnd( i ) )
	{
		SetElement * e = static_cast< SetElement * >( i.value );
		CBM_LogWrite( note, " e@",(void*)e, "  a=",e->a, "  b=",e->b );

		i = CBM_SetFindNext( i );
	}
}

int SetInsertAndRemove( CBM_Set set )
{
	CBM_LogWrite( "inserting A" );
	SetElement * A = (SetElement*)CBM_SetInsert( set, "A", NULL );
	A->a = 1;
	A->b = 1.5;
	SetIterate( "+A", set );

	CBM_LogWrite( "inserting B" );
	SetElement * B = (SetElement*)CBM_SetInsert( set, "B", NULL );
	B->a = 2;
	B->b = 2.5;
	SetIterate( "+B", set );

	CBM_LogWrite( "inserting C" );
	SetElement * C = (SetElement*)CBM_SetInsert( set, "C", NULL );
	C->a = 3;
	C->b = 3.5;
	SetIterate( "+C", set );

	CBM_LogWrite( "removing A" );
	CBM_SetRemove( set, "A" );
	SetIterate( "-A", set );
	CBM_LogWrite( "removing B" );
	CBM_SetRemove( set, "B" );
	SetIterate( "-B", set );

	CBM_LogWrite( "inserting D" );
	SetElement * D = (SetElement*)CBM_SetInsert( set, "D", NULL );
	D->a = 4;
	D->b = 4.5;
	SetIterate( "+D", set );

    CBM_LogWrite( "inserting E" );
	SetElement * E = (SetElement*)CBM_SetInsert( set, "E", NULL );
	E->a = 5;
	E->b = 5.5;
	SetIterate( "+E", set );

	CBM_SetRemove( set, "D" );
	SetIterate( "-D", set );

	CBM_SetRemove( set, "E" );
	SetIterate( "-E", set );

	CBM_SetRemove( set, "C" );
	SetIterate( "-C", set );

    return 0;
}

#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cmath>
#include <list>
#include <set>

static int s_NumberOfElements = 1000000;
static int s_SizeOfKeys = 8;
static double s_OpThreshold = 0.8;	/* add:='less than', del:='else' */
static int s_NumberOfOperations = 50;

int SetRandomInsertAndRemove( CBM_Set set )
{
	fprintf( stdout, "#elements=%d  keysize=%d\n", s_NumberOfElements, s_SizeOfKeys );

	static char const * alpha = "abcdefghijklmnopqrstuvwxyz0123456789";
	static int alpha_length = strlen( alpha );

	fprintf( stderr, "#elements=%d  #generatable=%.0f\n", s_NumberOfElements, pow( s_SizeOfKeys, alpha_length ) );
	if ( (double)s_NumberOfElements > pow( s_SizeOfKeys, alpha_length ) )
	{
		fprintf( stderr, "[EE] size of keys must be larger to generate requested number of elements!\n" );
	   	return -1;
	}

	/* generate keys (result in vector v) */
	std::list< std::string > v;
	{
		std::set< std::string > m;

		int r = 0;

		char * key = new char[s_SizeOfKeys+1];
		do
		{
			for ( int a = 0;  a < s_SizeOfKeys;  ++a )
			{
				key[a] = alpha[ rand() % alpha_length ];
			}
			key[s_SizeOfKeys] = '\0';
			//fprintf( stderr, "%s\n", key );

			if ( m.find( key ) == m.end() )
			{ 
				v.push_back( key );
				m.insert( key );

				++r;
			}
		}
		while ( r < s_NumberOfElements );
		delete key;

		fprintf( stdout, "#s=%d; #m=%d;  #keys=%d\n", (int)CBM_SetSize( set ), (int)m.size(), (int)v.size() );
		CBM_Assert( CBM_SetSize( set ) == 0 );
	}

	/* insert all generated keys */
	{
		SetElement e0;
		e0.a = 1; e0.b = 1.0;
		for ( std::list< std::string >::iterator key = v.begin();
				key != v.end();  ++key )
		{
			CBM_SetInsert( set, key->c_str(), &e0 );
		}
		fprintf( stdout, "#s=%d; #keys=%d\n", (int)CBM_SetSize( set ), (int)v.size() );
		CBM_Assert( CBM_SetSize( set ) == s_NumberOfElements );
	}


	/* stress test: inserts / deletes */
	{
		CBM_SetClear( set );
		fprintf( stdout, "#s=%d; #keys=%d\n", (int)CBM_SetSize( set ), (int)v.size() );
		CBM_Assert( CBM_SetSize( set ) == 0 );

		std::list< std::string > v_add = v;
		std::list< std::string > v_del;
		SetElement e0;
		int n=0, nb_add=0, nb_del=0;
		int s_min=s_NumberOfElements+1, s_avg=0, s_max=0;
		int q = 0;
		while ( q < s_NumberOfOperations )
		{
			double op = (double)rand()/RAND_MAX;
//			fprintf( stderr, "OP: %s (%.2f)\n", ((op<s_OpThreshold)?"ADD":"DEL"), op );
			if ( op < s_OpThreshold && !v_add.empty() )
			{
				/* add */
				{
					e0.a = q;
					v_del.push_back( v_add.front() );
					CBM_SetInsert( set, v_add.front().c_str(), &e0 );
//					fprintf( stdout, "ADD: %s\n", v_add.front().c_str() );
					CBM_Assert( ((SetElement*)CBM_SetGetValue( set, v_add.front().c_str() ))->a == q );
					v_add.pop_front();

					nb_add += 1;
				}
			}
			else
			{
				/* del */
				if ( ! v_del.empty() )
				{
					CBM_Assert( CBM_SetSize( set ) > 0 );

					v_add.push_back( v_del.front() );
					CBM_SetRemove( set, v_del.front().c_str() );
//					fprintf( stdout, "DEL: %s\n", v_del.front().c_str() );
					v_del.pop_front();

					nb_del += 1;
				}
			}
			s_min = std::min( s_min, CBM_SetSize( set ) );
			s_avg = ( n*s_avg + CBM_SetSize( set ) ) / ( n + 1 );
			s_max = std::max( s_max, CBM_SetSize( set ) );
			n += 1;

			CBM_Assert( CBM_SetSize( set ) == std::abs( nb_add - nb_del ) );

			q = nb_add + nb_del;
		}
		fprintf( stdout, "#iterations=%d; min/avg/max-size=%d/%d/%d; set-size=%d;  #ops=%d; #add=%d; #del=%d\n",
				q, s_min, s_avg, s_max, (int)CBM_SetSize( set ), s_NumberOfOperations, nb_add, nb_del );
	}

	return 0;
}

int main( int argc, char * argv[])
{
	fprintf( stderr, "usage:\n $> set [number-of-elements{%d} [size-of-keys{%d} [number-of-operations{%d} [add/del ratio{%f}] ] ] ]\n",
			s_NumberOfElements, s_SizeOfKeys, s_NumberOfOperations, s_OpThreshold );
	if ( argc > 1 )
		{ s_NumberOfElements = atoi( argv[1] ); }
	if ( argc > 2 )
		{ s_SizeOfKeys = atoi( argv[2] ); }
	if ( argc > 3 )
		{ s_NumberOfOperations = atoi( argv[3] ); }
	if ( argc > 4 )
		{ s_OpThreshold = atof( argv[4] ); }

	srand( time( NULL ) );

	CBM_Set set = CBM_SetCreate( CBM_MemPoolRoot, sizeof( SetElement ), 8192, 0 );

	SetInsertAndRemove( set );
	SetRandomInsertAndRemove( set );

	CBM_SetDestroy( set );
}

