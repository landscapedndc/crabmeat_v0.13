/*!
 * @brief
 *    test app for redis key/value store
 *
 * @author
 *    steffen klatt (created on: may 29, 2014),
 */

#include  "log/cbm_baselog.h"
#include  "liblog/res.h"
#include  "stats/lcputime.h"

#include  <stdlib.h>
#include  <stdio.h>

#include  <hiredis/hiredis.h>

redisContext *
redis_connect(
        int  argc, char * argv[])
{
    redisContext *  redis_context = NULL;

    const char *  hostname = ( argc > 1) ? argv[1] : "127.0.0.1";
    int port = ( argc > 2) ? atoi( argv[2]) : 6379;

    struct timeval  timeout = { 1, 500000 }; // 1.5 seconds
    redis_context = redisConnectWithTimeout( hostname, port, timeout);
    if ( redis_context == NULL || redis_context->err)
    {
        if ( redis_context)
        {
            printf( "Connection error: %s\n", redis_context->errstr);
            redisFree( redis_context);
        }
        else
        {
            printf( "Connection error: can't allocate redis context\n");
        }
        return  NULL;
    }

    /* PING server */
    redisReply *  reply = (redisReply *)redisCommand( redis_context, "PING");
    printf( "PING: %s\n", reply->str);
    freeReplyObject( reply);

    return  redis_context;
}
void
redis_disconnect(
        redisContext *  _redis_context)
{
    redisFree( _redis_context);
}


void
redis_write_store(
        redisContext *  _redis_context)
{
    /* Set a key */
    redisReply *  reply = (redisReply *)redisCommand(
            _redis_context, "SET %s %s", "ldndc.pragma", "biogeochemistry rocks");
    printf( "SET: %s\n", reply->str);
    freeReplyObject( reply);
}
#define  LDNDC_REDIS_LISTSIZE  10000
void
redis_read_store(
        redisContext *  _redis_context)
{
    /* Try a GET and two INCR */
    redisReply *  reply = (redisReply *)redisCommand(
            _redis_context, "GET ldndc.pragma");
    printf( "GET ldndc.pragma: %s\n", reply->str);
    freeReplyObject( reply);


    /* Create a list of numbers, from 0 to 9 */
    reply = (redisReply *)redisCommand(
            _redis_context, "DEL ldndc.list");
    freeReplyObject( reply);
    for ( unsigned int  j = 0;  j < LDNDC_REDIS_LISTSIZE;  ++j)
    {
        char  buf[64];

        snprintf( buf, 64, "%u", j);
        reply = (redisReply *)redisCommand(
                _redis_context, "LPUSH ldndc.list element-%s", buf);
        freeReplyObject( reply);
    }    
}

int
main(
        int  argc, char *  argv[])
{
    redisContext *  redis_context = redis_connect( argc, argv);
    if ( !redis_context)
    {
        return  EXIT_FAILURE;
    }

//    TIC(t_w);
    redis_write_store( redis_context);
//    TOC(t_w);
//    fprintf( stderr, "time write_store(): %f\n", t_w);

//    TIC(t_r);
    redis_read_store( redis_context);
//    TOC(t_r);
//    fprintf( stderr, "time read_store(): %f\n", t_r);

    redis_disconnect( redis_context);
}

