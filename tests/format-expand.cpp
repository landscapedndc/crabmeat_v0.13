/*!
 * @brief
 *    testing string format expansion
 *
 * @author
 *    steffen klatt (created on: jan 05, 2012),
 *    edwin haas
 */

#include  "string/lstring-compare.h"
#include  "crabmeat.h"

#include  <stdlib.h>
#include  <stdio.h>

void
usage()
{
    fprintf( stderr, "usage:  ");
    fprintf( stderr, "format-expand '%s'\n", "<STRING>");
    fprintf( stderr, "\n");
    fprintf( stderr, "STRING    string to be expand\n");
}

int
main(
        int  argc, char *  argv[])
{
    lerr_t  rc( LDNDC_ERR_FAIL);

    if (( argc > 1) && !cbm::is_empty( argv[1]))
    {
        char const *  i_str = argv[1];

        std::string  o_str;
        cbm::format_expand( &o_str, i_str);

        printf( "s='%s', s-expand='%s'\n", i_str, o_str.c_str());
    }
    else
    {
        fprintf( stderr, "not enough arguments\n");
        usage();
        rc = LDNDC_ERR_FAIL;
    }

    return ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

