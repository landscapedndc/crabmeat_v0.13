/*!
 * @brief
 *    dumps day-of-year / year-start / year-end depending on latitude
 *
 *   ./lat <latitude> [<first day> [<number of days> [<year>]]]
 *
 * @author
 *    steffen klatt (created on: aug 04, 2012),
 *    edwin haas
 */

#include  "time/clock.h"
#include  "scientific/meteo/lmeteo.h"
#include  "log/cbm_baselog.h"

#include  <stdlib.h>
#include  <stdio.h>

#define  LAT_YEAR  2001
#define  LAT_FULL_YEAR  366


lerr_t
list_days(
        double  _latitude,
        unsigned int  _offs = 1,
        unsigned int  _fin = LAT_FULL_YEAR,
        unsigned int  _year = LAT_YEAR)
{
    if ( _latitude < -90.0 || _latitude > 90.0)
    {
        CBM_LogError( "invalid latitude");
        return  LDNDC_ERR_FAIL;
    }

    for ( unsigned int k = _offs;  k < _offs+_fin;  ++k)
    {
        unsigned int  jd( k);
        unsigned int  ys( 0u), ye( ldate_t::days_in_year( _year));

        /* transform */
        ldndc::meteo::latitude_rad( _latitude, &jd, &ys, &ye);

        printf( "lat=%3.1f:  d=%u  s=%u  e=%u\n", _latitude, jd, ys, ye);
    }

    return  LDNDC_ERR_OK;
}

int
main(
        int  argc, char *  argv[])
{
    lerr_t  rc( LDNDC_ERR_FAIL);

    if ( argc > 1)
    {
        rc = list_days(
                /*latitude*/ atof( argv[1]),
                /*day offs*/ ((argc>2) ? atoi(argv[2]) : 1),
                /*days*/     ((argc>3) ? atoi(argv[3]) : LAT_FULL_YEAR),
                /*year*/     ((argc>4) ? atoi(argv[4]) : LAT_YEAR));
    }
           else
    {
        /* abort if no latitude was given as parameter */
        fprintf( stderr, "no height [m] specified.\n");
    }

    return ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

