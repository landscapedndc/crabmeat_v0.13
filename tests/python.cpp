/*!
 * @brief
 *    test app for embedded python interpreter
 *
 * @author
 *    steffen klatt (created on: nov 09, 2016),
 */

#include  "scripting/cbm_python.h"
#include  "log/cbm_baselog.h"
#include  "memory/cbm_mem.h"

#include  <unistd.h>

char const * const  script_01a =
 "import datetime\n"
 "T = datetime.datetime.now()\n"
 "print 'T(1)=',T\n"
 "";

char const * const  script_01b =
 "print 'T(2)=',T\n"
 "";

char const * const  script_02 =
 "import numpy\n"
 "a = numpy.array( [1.0, 2.0])\n"
 "print a\n"
 "\n"
 "import json\n"
 "print json.__version__\n"
 "";

char const * const  script_03 =
 "def foo( _string='TestString') :\n"
 "    return _string.title()\n"
 "";

char const * const  script_04 =
 "def inout_json():\n"
 "\tA = [1.0, 2.0]\n"
 "\tprint A\n"
 "\t\n"
 "\tjson_rv = '{ \"result\": [%s] }' % ( ','.join( ['%s' % str(a) for a in A]))\n"
 "\treturn json_rv\n"
 "";


int  execute_SimpleString(
    char const *  _script1, char const *  _script2)
{
    CBM_LogDebug( "execute_SimpleString()  IN");
    cbm_python_t  pyengine( "TestEngine");
    int  rc_exe = pyengine.execute( _script1);
    if ( rc_exe)
        { CBM_LogError( "Error when executing script"); }
    if ( _script2)
    {
        rc_exe = pyengine.execute( _script2);
        if ( rc_exe)
            { CBM_LogError( "Error when executing script"); }
    }
    CBM_LogDebug( "execute_SimpleString()  OUT");
    return  rc_exe;
}

int  execute_SimpleStringTwoEngines(
    char const *  _script1, char const *  _script2)
{
    CBM_LogDebug( "execute_SimpleStringTwoEngines()  IN");
    cbm_python_t  pyengine1( "TestEngineOne");
    int  rc_exe1 = pyengine1.execute( _script1);
    if ( rc_exe1)
        { CBM_LogError( "Error when executing script 1"); }

    cbm_python_t  pyengine2( "TestEngineTwo");
    int  rc_exe2 = pyengine2.execute( _script2);
    if ( rc_exe2)
        { CBM_LogError( "Error when executing script 2"); }
    CBM_LogDebug( "execute_SimpleStringTwoEngines()  OUT");
    return  rc_exe1 + rc_exe2;

}

int  execute_SimpleStringAndFunction(
    char const *  _script, char const *  _function_name,
    char const *  _argument)
{
    CBM_LogDebug( "execute_SimpleStringAndFunction()  IN");
    cbm_python_t  pyengine( "TestEngine");
    int  rc_exe = pyengine.execute( _script);
    if ( rc_exe)
        { CBM_LogError( "Error when executing script"); }
    if ( _function_name)
    {
        char *  rv = NULL;
        rc_exe = pyengine.call_function(
            _function_name, _argument, &rv);
        if ( rc_exe)
            { CBM_LogError( "Error when executing script"); }
        if ( rv)
        {
            CBM_LogWrite( "Result in C:\n", rv);
            CBM_Free( rv);
        }
    }
    CBM_LogDebug( "execute_SimpleStringAndFunction()  OUT");
    return  rc_exe;
}


int  execute_PythonFunction(
    char const *  _script, char const *  _function)
{
    CBM_LogDebug( "execute_PythonFunction()  IN");
    cbm_python_t  pyengine( "TestEngine");
//    pyengine.execute( "import inspect");
    char *  rv;
    int  rc_exe = pyengine.execute_function(
            _script, _function, NULL, &rv);
    if ( rc_exe)
        { CBM_LogError( "Error when executing script"); }
    if ( rv)
    {
        CBM_LogWrite( "Result in C: '", rv, "'");
        CBM_Free( rv);
    }
//    pyengine.execute( "print inspect.getsource( \"inout_json\")");
    CBM_LogDebug( "execute_PythonFunction()  OUT");
    return  rc_exe;
}


int
main( int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);

    cbm_python_t::initialize( argv[0]);

    CBM_LogWrite( "Two scripts, one context");
    execute_SimpleString( script_01a, script_01b);

    CBM_LogWrite( "Two scripts with function definition, one context");
    execute_SimpleStringAndFunction(
            script_03, "foo", "I walked into a hospital\n" "where none was sick and none was well,\n"
        "when at night the nurses left\n" "I could not walk at all.\n" "\t-- Leonard Cohen 1967\n");

    CBM_LogWrite( "InOut JSON");
    execute_PythonFunction( script_04, "inout_json");

    CBM_LogWrite( "One script, two contexts");
    execute_SimpleStringTwoEngines( script_02, script_02);

    cbm_python_t::finalize();
}

