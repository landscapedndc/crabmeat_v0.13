/*!
 * @brief
 *    assigning multi-dimensional (currently d=2) array a
 *    preallocated memory buffer (sort of a placement new
 *    for arrays of array of arrays of ...)
 *
 * @author
 *    steffen klatt (created on: mar 27, 2012),
 */

#include  <stdlib.h>
#include  <stdio.h>

#define  M  10
#define  N  8


void
print_matrix( int**  _mat, int  _M, int  _N)
{
    for ( int  k = 0;  k < _M;  ++k)
    {
        for ( int  j = 0;  j < _N;  ++j)
        {
            printf( "  %3d", _mat[k][j]);
        }
        printf( "\n");
    }
}

void
multidim_array_from_mempool_v1()
{
    printf( "\n *** two allocations ***\n\n");

    int *  buffer = new int[M*N];
    int **  arr2d = new int*[M];

    for ( int  k = 0;  k < M;  ++k)
    {
        arr2d[k] = buffer + ( k * N);
        for ( int  j = 0;  j < N;  ++j)
        {
            arr2d[k][j] = k * N + j;
        }

        printf( "a[%2d] @ %p\n", k, (void*)&(arr2d[k]));
    }

    printf( "\n");

    print_matrix( arr2d, M, N);

    delete[]  buffer;
    delete[]  arr2d;
}

void
multidim_array_from_mempool_v2()
{
    printf( "\n *** one allocation ***\n\n");

    char *  buffer = new char[ M * N * sizeof( int) + M * sizeof( int*)];

    int **  arr2d = (int **)buffer;

    for ( int  k = 0;  k < M;  ++k)
    {
        arr2d[k] = (int *)( buffer + M * sizeof( int*) + k * N * sizeof( int));
        for ( int  j = 0;  j < N;  ++j)
        {
            arr2d[k][j] = k * N + j;
        }

        printf( "a[%2d] @ %p\n", k, (void*)&(arr2d[k]));
    }

    print_matrix( arr2d, M, N);

    delete[]  buffer;
}

int
main(
        int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);

    /* using two allocations */
    multidim_array_from_mempool_v1();
    /* using only one allocation */
    multidim_array_from_mempool_v2();
}

