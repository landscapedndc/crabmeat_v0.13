/*!
 * @file
 *    test/assert.cpp
 *
 * @brief
 *    execute assertions
 *
 * @author
 *    steffen klatt (created on: may 25, 2012)
 */

#include  "containers/cbm_typewrap.h"

#include  "crabmeat-version.h"
#include  "crabmeat.h"

#include  <stdlib.h>
#include  <stdio.h>


int
main(
        int,
        char *[])
{
    fprintf( stderr, "package name: '%s'\n", crabmeat_package_name());
    fprintf( stderr, "package short name: '%s'\n", crabmeat_package_name_short());
    fprintf( stderr, "package version: '%s'\n", crabmeat_package_version());
    fprintf( stderr, "package revision: '%s'\n", crabmeat_package_revision());
    fprintf( stderr, "package build timestamp: '%s'\n", crabmeat_package_build_timestamp());
    fprintf( stderr, "\n\n");
    fprintf( stderr, "package build configuration: %s\n", crabmeat_package_build_configuration());

#ifdef  _DEBUG
#  ifdef  __WORDSIZE
    fprintf( stderr,
          "|word|=%u,\n|lid_t|=%u,\n|void*|=%u,\n|char*|=%u,\n|char|=%u,\n|ltypewrap_t<float>|=%u,\n|ltypewrap_t<double>|=%u,\n|ltypewrap_t<>::flt_t|=%u\n\n",
          (unsigned int)__WORDSIZE,
          8*(unsigned int)sizeof( lid_t),
          8*(unsigned int)sizeof( void*),
          8*(unsigned int)sizeof( char*),
          8*(unsigned int)sizeof( char),
          8*(unsigned int)sizeof( ldndc::ltypewrap_t<float>),
          8*(unsigned int)sizeof( ldndc::ltypewrap_t<double>),
          8*(unsigned int)sizeof( ldndc::ltypewrap_t<float>::float_type)
    );
#  endif
#else
    fprintf( stderr, "no assertions available in release build (move them elsewhere?)\n");
#endif

    printf( "all good :-)\n");

    return  EXIT_SUCCESS;
} // end main

