/*!
 * @brief
 *    testing LDNDC normalized string
 *
 * @author
 *    steffen klatt (created on: jan 05, 2012),
 *    edwin haas,
 */

#include  "string/lstring-compare.h"
#include  "string/lstring-nstr.h"
#include  "crabmeat.h"

#include  <stdlib.h>
#include  <stdio.h>

void
usage()
{
    fprintf( stderr, "usage:  ");
    fprintf( stderr, "nstr '%s' '%s'\n", "<STRING>", "[<CHAR-SEQ>]");
    fprintf( stderr, "\n");
    fprintf( stderr, "STRING    string to be normalized\n");
    fprintf( stderr, "CHAR-SEQ  character sequence to be stripped\n");
}

int
main(
        int  argc, char *  argv[])
{
    lerr_t  rc( LDNDC_ERR_FAIL);

    if (( argc > 1) && !cbm::is_empty( argv[1]))
    {
        char const *  s = argv[1];
        char const *  c( CNAME_WHITESPACE);
        if (( argc > 2) && !cbm::is_empty( argv[2]))
        {
            c = argv[2];

        }
        else
        {
            fprintf( stderr, "no character sequence given. using default.\n");
        }

        printf( "s='%s', c='%s'\n", s, c);

        cbm::nstr const  nh( s, CNAME_STRIP_HEAD, c);
        printf( "\thead:\ts='%s'\n", nh.c_str());

        cbm::nstr const  nt( s, CNAME_STRIP_TAIL, c);
        printf( "\ttail:\ts='%s'\n", nt.c_str());

        cbm::nstr const  nht( s, CNAME_STRIP_HEAD|CNAME_STRIP_TAIL, c);
        printf( "\ttail:\ts='%s'\n", nht.c_str());
    }
    else
    {
        fprintf( stderr, "not enough arguments\n");
        usage();
        rc = LDNDC_ERR_FAIL;
    }

    return ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

