/*!
 * @brief
 *    test app for UDUnits2 library
 *    http://www.unidata.ucar.edu/software/udunits/
 *
 * @author
 *    steffen klatt (created on: nov 15, 2016),
 */

#include  <cstdio>

#include  "log/cbm_baselog.h"


#include  "units/cbm_units.h"
void
cbm_units( char const *  _units_db,
    char const *  _unit1, char const *  _unit2)
{
    cbm_units_t  cbmunits( _units_db);

    cbm_unit_t  u1 = cbmunits.parse_unit( _unit1);
    cbm_unit_t  u1a = cbmunits.parse_unit( _unit1);
    cbm_unit_t  u1b = cbmunits.parse_unit( _unit1);
    cbm_unit_t  u2 = cbmunits.parse_unit( _unit2);
    cbm_unit_t  u2a = cbmunits.parse_unit( _unit2);
    (void)u1a; (void)u1b; (void)u2a;

    if ( !cbmunits.convertible( u1, u2))
        { CBM_LogWrite( "Units are not convertible"); }
    else
    {
        double  s[] = { 1.0, 2.0, 3.0, 4.0};
        double  t[] = { 0.0, 0.0, 0.0, 0.0};

        fprintf( stdout, "S:%.3f  %.3f  %.3f  %.3f\n", s[0],s[1],s[2],s[3]);
        fprintf( stdout, "T:%.3f  %.3f  %.3f  %.3f\n", t[0],t[1],t[2],t[3]);
        fprintf( stdout, "convert %s -> %s ... [%s->%s]\n",
                cbmunits.symbols( u1).c_str(), cbmunits.symbols( u2).c_str(),
                _unit1, _unit2);
        cbmunits.convert_double( t, u2, s, u1, sizeof(s)/sizeof(s[0]));
        fprintf( stdout, "T:%e  %e  %e  %e\n", t[0],t[1],t[2],t[3]);
    }
}

#include  <udunits2.h>
void
ud_units( char const *  _units_db,
    char const *  _unit1, char const *  _unit2)
{
    ut_system *  u_sys = ut_read_xml( _units_db);
    CBM_Assert( u_sys);

    ut_unit *  u1 = ut_parse( u_sys, _unit1, UT_ASCII);
    CBM_Assert( u1);

    ut_unit *  u2 = ut_parse( u_sys, _unit2, UT_ASCII);
    CBM_Assert( u2);


    char  b1[128];
    (void)ut_format( u1, b1, sizeof( b1), UT_ASCII/*|UT_DEFINITION*/);
    char  b2[128];
    (void)ut_format( u2, b2, sizeof( b2), UT_ASCII/*|UT_DEFINITION*/);

//    CBM_LogWrite( "unit-A: ", _unit1, " parsed to ", b1);
//    CBM_LogWrite( "unit-B: ", _unit2, " parsed to ", b2);

    if ( !ut_are_convertible( u1, u2))
        { CBM_LogWrite( "Units are not convertible"); }
    else
    {
        double  s[] = { 1.0, 2.0, 3.0, 4.0};
        double  t[] = { 0.0, 0.0, 0.0, 0.0};

        fprintf( stdout, "S:%.3f  %.3f  %.3f  %.3f\n", s[0],s[1],s[2],s[3]);
        fprintf( stdout, "T:%.3f  %.3f  %.3f  %.3f\n", t[0],t[1],t[2],t[3]);
        fprintf( stdout, "convert %s -> %s ... [%s->%s]\n", b1, b2, _unit1, _unit2);
        cv_converter *  converter = ut_get_converter( u1, u2);
        cv_convert_doubles( converter, s, sizeof(s)/sizeof(s[0]), t);
        fprintf( stdout, "T:%e  %e  %e  %e\n", t[0],t[1],t[2],t[3]);
    }

    ut_free( u1);
    ut_free( u2);
    ut_free_system( u_sys);
}


int
main( int  argc, char *  argv[])
{
    if ( argc < 3)
    {
        CBM_LogError( "usage: cbm_units <unit-from> <unit-to>");
    }
    else
    {
        cbm_units_t::initialize( NULL);

        char const *  unit_from = argv[1];
        (void)unit_from;
        char const *  unit_to = argv[2];
        (void)unit_to;

        fprintf( stdout, "\nUDUNITS:\n");
        /* play with UniData Units */
        ud_units( NULL, unit_from, unit_to);

        fprintf( stdout, "\nCBM/UDUNITS:\n");
        /* play with cbm interface to units */
        cbm_units( NULL, unit_from, unit_to);
        cbm_units_t::finalize();
    }
}

