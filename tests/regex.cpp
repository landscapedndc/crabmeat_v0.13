/*!
 * @brief
 *    sandbox app for regex api
 *
 * @author
 *    steffen klatt (created on: jun 18, 2021),
 */

#include  "regex/cbm_regex.h"
#include  "string/cbm_string.h"

#include  "log/cbm_baselog.h"

#include  <stdlib.h>
#include  <stdio.h>

static bool t_regexmatch( char const *text, char const* regex, int matchlength )
{
	int M = matchlength;
	if ( M == -1 ) {
		M = cbm::strlen( text );
	}
	int i = 0;
	int m = cbm::regex_match( regex, text, 0, &i, 0 );
	if ( m==-1 ) {
		CBM_LogWrite( ">> '",text, "' ~ '", regex, "'  no-match" );
	} else {
		CBM_LogWrite( ">> '",text, "' ~ '", regex, "'  match=", m, " (", M, "@", i, ")" );
	}
	return m == M;
}

static int regex_run_samples()
{
	t_regexmatch( "", "", 0 );
	t_regexmatch( "ab.CD.ef", "^ab.[A-Z]+.ef$", -1 );
	t_regexmatch( "ab.CD.ef", "^ab.[A-Z]+", -1 );
	t_regexmatch( "ab.CD.ef", "^ab\\.[A-Z]+\\.ef$", -1 );
	t_regexmatch( "ab.CD.ef", "^ab\\.\\s*\\.ef$", -1 );
	t_regexmatch( "ab.CD.ef", "^ab\\.[^\\.]+\\.ef$", -1 );
	t_regexmatch( "ab.CD.ef", "^ab\\.[^\\.]+", -1 );

	t_regexmatch( "ab.CD.gh", "^ab\\.[^\\.]+\\.ef$", 6 );
	t_regexmatch( "ab.CD.gh", "ab\\.[^\\.]+\\.ef", 6 );
	t_regexmatch( "ab.CD.gh", "\\.[^\\.]+\\.ef", 4 );
	t_regexmatch( "ab.CD.gh", "^ab\\.[^\\.]+\\.", 6 );
	t_regexmatch( "ab.CD.gh", "ab\\.[^\\.]+\\.", 6 );
	t_regexmatch( "ab.CD.gh", "\\.[^\\.]+\\.", 4 );

    return 0;
}

int main( int /*argc*/, char * /*argv*/[])
{
	if ( regex_run_samples() ) {
		return EXIT_SUCCESS;
	}
	return EXIT_FAILURE;
}

