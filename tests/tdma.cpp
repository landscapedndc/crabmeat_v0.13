/*!
 * @brief
 *    test program for tdma solver
 *
 * @author
 *    Edwin Haas,
 *    Steffen Klatt
 */

#include  "math/lmath-tdma.h"
#include  "crabmeat.h"

#include  <stdlib.h>
#include  <stdio.h>

#include  <iostream>
#include  <fstream>
#include  <sstream>

int
tdma_run(
        size_t  _size, char const *  _sys_file)
{
    solver_tridiagonal_t  solver( _size);

    std::ifstream  lse( _sys_file);
    std::string  s( "");

    std::getline( lse, s);

    if ( lse.is_open())
    {
        printf( "reading system from file \"%s\"\n", _sys_file);

        size_t  l( 0);
        while ( l < _size)
        {
            std::getline( lse, s);
            std::stringstream  iss( s);

            size_t  k( 0);
            double  d( 0.0), a[3] = { 0.0, 0.0, 0.0}, x( 0.0), b( 0.0);
            // read matrix
            while (( k < _size)  &&  ( iss >> d))
            {
                if (( k+1) == l)
                    a[0] = d;
                else if ( k == l)
                    a[1] = d;
                else if ( k  &&  (( k-1) == l))
                    a[2] = d;
                else
                {
                    // no op
                }
                printf( " %.3f", d);
                ++k;
            }
            // read lhs vector x' and rhs vector b
            iss >> x >> b;
            printf( "    %+.3f    %+.3f\n", x, b);
            solver.set_row( l, a[0], a[1], a[2], b);
            ++l;
        }

        solver.solve_in_place();

        printf( "\nx = [");
        for ( size_t  k = 0;  k < _size;  ++k)
        {
            printf( " %+.3f", solver.x( k));
            // TODO  calculate  ||x - x'|| / ||x'||
        }
        printf( " ]\n");
    }
    else
    {
        fprintf( stderr, "failed to open file\n");
        return  1;
    }


    return  0;
}


int
main(
        int  argc, char *  argv[])
{
    if ( argc > 2)
    {
        /* start simulation with given setup file parameter */
        if ( tdma_run( (size_t)atoi(argv[1]), argv[2]) != 0)
        {
            return EXIT_FAILURE;
        }
    }
    else
    {
        fprintf( stderr, "tdma <size> <matrix file>.\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

