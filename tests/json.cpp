/*!
 * @brief
 *    sandbox app for json api
 *
 * @author
 *    steffen klatt (created on: jan 28, 2015),
 */

#include  "json/cbm_json.h"
#include  "string/cbm_string.h"
#include  "constants/cbm_const.h"

#include  "log/cbm_baselog.h"

#include  <stdlib.h>
#include  <stdio.h>

static char const *  JSONDOC =
    "{ \"modules\" : [\"microclimate:dndc\", \"microclimate:canopyecm\", \"watercycle:dndc\", \"physiology:dndc\", \"soilchemistry:metrx\"],"
      "\"module.microclimate:dndc\" : { \"timemode\": \"subdaily\"},"
      "\"module.microclimate:canopyecm\" : { \"timemode\": \"subdaily\"},"
      "\"module.watercycle:dndc\" : { \"timemode\": \"subdaily\"},"
      "\"module.physiology:dndc\" : { \"timemode\": \"subdaily\"},"
      "\"module.soilchemistry:metrx\" : { \"timemode\": \"subdaily\", \"output_daily\": false, \"output_subdaily\": false},"

      "\"int23\" : 23, \"dbl\" : 3.141,"
	  "\"int32list\" : [ 0, 1, 2, 3, 4, 5, 6, 7],"
	  "\"flt64list\" : [ 0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]"
    "}";

int json_query( char const *  _doc)
{
    cbm::jquery_t  jquery( _doc);
    if ( jquery.parse_ok())
    {
        CBM_LogWrite( "query \"int23\" -> ", jquery.query_int( "/int23", -1));

        CBM_LogWrite( "\"/modules/0\" -> ", jquery.get( "/modules/0", "?"));
        CBM_LogWrite( "\"/dbl\" -> ", jquery.get( "/dbl", 0.0));
    }
    return 0;
}

int json_build()
{
    cbm::jbuild_t  jbuild;
    jbuild.put_int( "/int", 23);
    jbuild.put( "/x-int", 23);

    jbuild.put_bool( "/bool-true", true);
    jbuild.put( "/x-bool-true", true);
    jbuild.put_bool( "/bool-false", false);
    jbuild.put( "/x-bool-false", false);

    jbuild.put_double( "/double-pi", cbm::PI);
    jbuild.put( "/x-double-pi", double(cbm::PI));
    jbuild.put_float( "/float-pi", float(cbm::PI));
    jbuild.put( "/x-float-pi", float(cbm::PI));

    jbuild.put_string( "/model", "LandscapeDNDC");
    jbuild.put( "/x-model", "LandscapeDNDC");

    jbuild.put( "/parameters/wue", 0.5);
    jbuild.put( "/parameters/straw-cn", 0.35);

    CBM_LogWrite( "B='", jbuild.serialize(), "'");

    return 0;
}

int json_build_noinvalid()
{
    cbm::jbuild_t  jbuild( CBM_JBUILD_IGNOREINVALID);
    jbuild.put_int( "/int", -9999);
    jbuild.put( "/x-int", -9999);

    jbuild.put_bool( "/bool-true", true);
    jbuild.put( "/x-bool-true", true);
    jbuild.put_bool( "/bool-false", false);
    jbuild.put( "/x-bool-false", false);

    jbuild.put_double( "/double-pi", -99.99);
    jbuild.put( "/x-double-pi", ldndc::invalid_t<double>::value);
    jbuild.put_float( "/float-pi", -99.99);
    jbuild.put( "/x-float-pi", -99.99);

//    jbuild.put_string( "/model", invalid_string);
//    jbuild.put( "/x-model", "LandscapeDNDC");

    jbuild.put( "/parameters/wue", ldndc::invalid_t<float>::value);
    jbuild.put( "/parameters/straw-cn", ldndc::invalid_t<float>::value);

    CBM_LogWrite( "B-no-invalid='", jbuild.serialize(), "'");

    return 0;
}

int main( int  argc, char *  argv[])
{
    json_query( argc > 1 ? argv[1] : JSONDOC);

    json_build();
    json_build_noinvalid();
}

