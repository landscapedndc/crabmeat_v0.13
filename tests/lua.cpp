/*!
 * @brief
 *    test app for LUA scripting
 *
 * @author
 *    steffen klatt (created on: feb 18, 2015)
 *
 * @note
 *    based on https://csl.name/post/lua-and-cpp/
 */

#include  <stdlib.h>
#include  <stdio.h>

extern "C"
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}

int
main(
        int  argc, char *  argv[])
{
    lua_State *  L = luaL_newstate();
    if ( !L)
    {
        return  1;
    }

    /* load standard libraries */
    luaL_requiref( L, "io", luaopen_io, 1);

    fprintf( stdout, "open script..\n");
    char const *  file = ( argc>1) ? argv[1] : "example.lua";
    int s = luaL_loadfile( L, file);
    if ( s == 0)
    {
	    fprintf( stdout, "execute script..\n");
	    /* execute Lua program */
	    s = lua_pcall( L, 0, LUA_MULTRET, 0);
	    fprintf( stdout, "\ncompleted.\n");
    }
    if ( s != 0)
    {
	    fprintf( stderr, "errors:\n%s\n", lua_tostring( L, -1));
        lua_pop( L, 1);
    }

    lua_close(L);
}

