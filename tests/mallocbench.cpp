/*!
 * @brief
 *    measuring time for consecutive calls to malloc 
 *
 * @author
 *    steffen klatt (created on: jul 1, 2012),
 */

#include  "liblog/res.h"

#include  <stdlib.h>
#include  <stdio.h>

/* switch on time measuring */
//#define  _LDNDC_MEASURE_RESOURCES

/* default number of blocks to allocate */
#define  __MALLOC_N    ((size_t)1 << 20)
/* default number of bytes to allocate within one block */
#define  __MALLOC_M     ((size_t)1 << 10)
/* maximum variance of M */
#define  __MALLOC_R     ((size_t)1 << 7)

typedef  unsigned long int  ul_t;

void
usage()
{
    fprintf( stderr, "mallocbench allocates N blocks of size M. memory is deallocated after each malloc.\n");
    fprintf( stderr, "usage:\n");
    fprintf( stderr, "mallocbench [<N> [[<M> [<R>]]]\n");
    fprintf( stderr, "\n");
    fprintf( stderr, "N:    number of mallocs/frees to perform\n");
    fprintf( stderr, "M:    size of block of memory to allocate\n");
    fprintf( stderr, "R:    maximum number of bytes M is varied (M' in {M, ..., M+R-1})\n");
}

int
main(
        int  argc, char *  argv[])
{
    size_t  n( __MALLOC_N);
    size_t  m( __MALLOC_M);
    size_t  r( __MALLOC_R);

    if ( argc > 1)
    {
        int  _n = atoi( argv[1]);
        if ( _n < 1)
        {
            fprintf( stderr, "invalid argument for N.\n");
            usage();
            exit( EXIT_FAILURE);
        }
        n = (size_t)_n;

        if ( argc > 2)
        {
            int  _m = atoi( argv[2]);
            if ( _m < 1)
            {
                fprintf( stderr, "invalid argument for M.\n");
                usage();
                exit( EXIT_FAILURE);
            }
            m = (size_t)_m;

            if ( argc > 3)
            {
                int  _r = atoi( argv[3]);
                if ( _r < 2)
                {
                    fprintf( stderr, "invalid argument. R too small.\n");
                    usage();
                    exit( EXIT_FAILURE);
                }
                r = (size_t)_r;
            }
        }
    }

    {
        printf( "allocating %lu blocks of %lu bytes using 'malloc/free'\n", (ul_t)n, (ul_t)m);
        char *  mem( NULL);

        {
            TIC(tmal);
            for ( size_t  k = 0;  k < n;  ++k)
            {
                mem = (char*)malloc( m * sizeof( char));
                //mem[0] = 'a';
                free( mem);
            }
            TOC(tmal);
            printf( "t(%u)=%.8fs  (s=-)\n", 0u, tmal);
        }
        {
            size_t  s( 0);
            srand((unsigned int)m);

            TIC(tmal);
            for ( size_t  k = 0;  k < n;  ++k)
            {
                size_t  m_var( m + (size_t)( (rand() % r)));
                s += m_var;

                mem = (char*)malloc( m * sizeof( char));
                //mem[0] = 'a';
                free( mem);
            }
            TOC(tmal);
            printf( "t(%u)=%.8fs  (s=%lu)\n", 1u, tmal, (ul_t)s);
        }
        {
            size_t  s( 0);
            srand((unsigned int)m);

            TIC(tmal);
            srand((unsigned int)m);
            for ( size_t  k = 0;  k < n;  ++k)
            {
                size_t  m_var( m + (size_t)( (rand() % r)));
                s += m_var;

                mem = (char*)malloc( m_var * sizeof( char));
                //mem[0] = 'a';
                free( mem);
            }
            TOC(tmal);
            printf( "t(%u)=%.8fs  (s=%lu)\n", (unsigned int)r, tmal, (ul_t)s);
        }
    }

    printf( "\n");

    {
        printf( "allocating %lu blocks of %lu bytes using operators 'new[]/delete[]'\n", (ul_t)n, (ul_t)m);
        char *  mem( NULL);

        {
            TIC(tmal);
            for ( size_t  k = 0;  k < n;  ++k)
            {
                mem = new char[m];
                //mem[0] = 'a';
                delete[] mem;
            }
            TOC(tmal);
            printf( "t(%u)=%.8fs  (s=-)\n", 0u, tmal);
        }
        {
            size_t  s( 0);
            srand((unsigned int)m);

            TIC(tmal);
            for ( size_t  k = 0;  k < n;  ++k)
            {
                size_t  m_var( m + (size_t)( (rand() % r)));
                s += m_var;

                mem = new char[m];
                //mem[0] = 'a';
                delete[] mem;
            }
            TOC(tmal);
            printf( "t(%u)=%.8fs  (s=%lu)\n", 1u, tmal, (ul_t)s);
        }
        {
            size_t  s( 0);
            srand((unsigned int)m);

            TIC(tmal);
            for ( size_t  k = 0;  k < n;  ++k)
            {
                size_t  m_var( m + (size_t)( (rand() % r)));
                s += m_var;
                mem = new char[m_var];
                //mem[0] = 'a';
                delete[] mem;
            }
            TOC(tmal);
            printf( "t(%u)=%.8fs  (s=%lu)\n", (unsigned int)r, tmal, (ul_t)s);
        }
    }
}

