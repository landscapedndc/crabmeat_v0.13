/*!
 * @brief
 *    testing LDNDC date parser
 *
 * @author
 *    steffen klatt (created on: jan 05, 2012),
 *    edwin haas,
 */

#include  "time/cbm_time.h"
#include  "string/cbm_string.h"
#include  "log/cbm_baselog.h"
#include  "cbm_rtcfg.h"

#include  <stdlib.h>
#include  <stdio.h>

#define  ui unsigned int
#define  REF_LDATE "1953-02-13-01/48 -> +0"

static char const *  s_AsIso8601( cbm::tm_t const *  _tm)
{
    static char  buf[128];
    cbm::snprintf( buf, 128,
        "%u-%02u-%02u %02u:%02u:%02u",
            _tm->tm_year, _tm->tm_month+1, _tm->tm_day+1,
            _tm->tm_hour, _tm->tm_min, _tm->tm_sec);
    return buf;
}

int
main(
        int  argc, char *  argv[])
{
    lerr_t  rc( LDNDC_ERR_OK);

    if ( argc == 1)
    {
        printf( "julian day to calendar day (day/month) conversion:\n");
        int  j = 0;
        static const ldate_t::tm_scalar_t  jdays[] = { 1, 13, 31, 32, 58, 59, 60, 61, 333, 334, 364, 365, 366, 0};
        while ( jdays[j] != 0)
        {
            if ( jdays[j] < 366)
            {
                cbm::tm_t  t1( 1995, 0, 0);
                cbm::tm_iyearday( jdays[j]-1, &t1);
                printf( "%5u -> %02u.%02u\t\t",
                    jdays[j], (unsigned)t1.tm_day+1, (unsigned)t1.tm_month+1);
            }
            else
            {
                printf( "%5u -> skipped\t", jdays[j]);
            }
            cbm::tm_t  t2( 1996, 0, 0);
            cbm::tm_iyearday( jdays[j]-1, &t2);
            printf( "%5u -> %02u.%02u\t(ly)\n",
                jdays[j], (unsigned)t2.tm_day+1, (unsigned)t2.tm_month+1);
            ++j;
        }

        printf( "\n\n");

        cbm::tm_t  t1( 1995,11,30);
        cbm::tm_t  t2( 1996,11,30);
        CBM_LogWrite( "t1=",t1.td_seconds, " t2=",t2.td_seconds);
        CBM_LogWrite( "D(t1)=",cbm::tm_daysinyear( &t1), "  D(t2)=",cbm::tm_daysinyear( &t2));
        CBM_LogWrite( "M(t1)=",cbm::tm_daysinmonth( &t1), "  M(t2)=",cbm::tm_daysinmonth( &t2));

        cbm::tm_t tm0( 1995,11,30);
        CBM_LogWrite( "tm-0=",s_AsIso8601( &tm0), "  sse-0=",tm0.td_seconds, "  sse-0()=",cbm::date_to_sse( &tm0));

        cbm::tm_t tm;
        cbm::td_scalar_t  sse_ret = cbm::sse_to_date( tm0.td_seconds, &tm);
        CBM_LogWrite( "tm=",s_AsIso8601( &tm), "  rc=",sse_ret);

        cbm::tp_t tp;
        tp.tp_month = 12;
//        tp.tp_day = 366;
        CBM_LogWrite( "sec=",tp.tp_sec, "  days=",tp.tp_day, "  months=",tp.tp_month);
        cbm::tm_addperiod( &tm, &tp);
        CBM_LogWrite( "tm=",s_AsIso8601( &tm));

        cbm::td_scalar_t  sse_1 = cbm::date_to_sse( &t1);
        cbm::tm_t  t_1r;
        cbm::sse_to_date( sse_1, &t_1r);
        CBM_LogWrite( s_AsIso8601( &t1)," -> ", sse_1," -> ", s_AsIso8601( &t_1r));

        cbm::td_scalar_t  sse_2 = cbm::date_to_sse( &t2);
        cbm::tm_t  t_2r;
        cbm::sse_to_date( sse_2, &t_2r);
        CBM_LogWrite( s_AsIso8601( &t2)," -> ", sse_2," -> ", s_AsIso8601( &t_2r));

// sk:off        static size_t const  l_N = 1600000;
// sk:off        ldate_t  t_date;
// sk:off        for ( size_t  l = 0;  l < l_N;  ++l)
// sk:off        {
// sk:off            ldate_t const  t_date_0 = t_date;
// sk:off            t_date += lperiod_one_day;
// sk:off
// sk:off            printf( "%s (%u) <-> %s (%u)\n",
// sk:off                    t_date_0.to_string().c_str(), (unsigned int)t_date_0.sse(),
// sk:off                    t_date.to_string().c_str(), (unsigned int)t_date.sse());
// sk:off
// sk:off            if (( t_date_0.sse() + 86400) != t_date.sse())
// sk:off            {
// sk:off                fprintf( stderr, "ERROR!\n");
// sk:off                break;
// sk:off            }
// sk:off        }
    }
    else if ( argc == 2)
    {
        /* ldndc date object */
        printf( "-10-");
        ltime_t  ldate;
        printf( "-11-");
        /* set date from string object */
        printf( "-20-");
        rc = ldate.from_string( argv[1]);
        printf( "-21-");

        ldate_t const &  f = ldate.from();
        ldate_t const &  t = ldate.to();

        std::string const  ds = ldate.to_string();
        char const *  ds_c = ds.c_str();

        if ( rc == LDNDC_ERR_OK)
        {
            printf( "dateIS=%s:  y=%u; m=%u; d=%u; H=%u; M=%u; S=%u;\n",
                    ds_c,
                  (ui)f.year(), (ui)f.month(), (ui)f.day(),
                  (ui)f.hour(), (ui)f.minute(), (ui)f.second());
                    
            printf( "dateIF=%s:  julian day=%u/%u;  diff=%u/%u;  period=%u/%u;  trs=%u;  sse=%u;\n",
                    ds_c,
                    (ui)t.yearday(), (ui)f.yearday(),
                    (ui)ldate.diff( 1), (ui)ldate.diff( f.dt()),
                    (ui)ldate.period( 1), (ui)ldate.period( f.dt()),
                    (ui)f.dt(),
                    (ui)f.sse());

//            printf( "merging: %s && %s = %s\n",
//                    argv[1],
//                    ltime_t( REF_LDATE).to_string().c_str(),
//                    ltime_t( argv[1], ltime_t( REF_LDATE)).to_string().c_str());
        }
        else
        {
            fprintf( stderr, "date parsing failed,  date=%s\n", argv[1]);
            rc = LDNDC_ERR_FAIL;
        }

        cbm::sclock_t  clk( ldate);

        while ( clk.run())
        {
            std::string  cs = clk.to_string();
            char const *  cs_c = cs.c_str();

            std::string  pn = clk.position_names();
            printf( "clkIS=%s;  y=%u; m=%u; d=%u; H=%u; M=%u; S=%u;\n",
                    cs_c,
                  (ui)clk.year(), (ui)clk.month(), (ui)clk.day(),
                  (ui)clk.hour(), (ui)clk.minute(), (ui)clk.second());
 
            printf( "clkAT=%s: @=%s\n", cs_c, pn.c_str());

            clk.advance();
        }
    }
    else if ( argc == 3)
    {
        ltime_t  tm;
        cbm::string_t  tm_ref = argv[2];
        cbm::sclock_t  clk( tm.parse( tm_ref));
        CBM_LibRuntimeConfig.clk = &clk;
        CBM_LogWrite( "tm-ref=", clk.now().to_string());

        cbm::string_t  tm_fmt = argv[1];
        cbm::string_t  tm_fmt_exp = argv[1];
        CBM_LogWrite( "tm-fmt=", tm_fmt, " -> ", tm_fmt_exp.expand());
    }

    return ( rc == LDNDC_ERR_OK) ? EXIT_SUCCESS : EXIT_FAILURE;
}

