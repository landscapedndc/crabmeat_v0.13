/*!
 * @brief
 *    test app for c-array wrapper with support
 *    for multi-dimensional indexing
 *
 * @author
 *    steffen klatt (created on: may 14, 2012),
 */

#include  "containers/lvector.h"
#include  "constants/cbm_const.h"

#include  "openmp/cbm_omp.h"

#include  <stdlib.h>
#include  <stdio.h>


typedef  lvector_t< float, 3 >  matrix_3d_flt_t;

template < typename _T, typename _S >
void
print_list( _T const *  list, _S  _size, char const *  _seq)
{
    for ( _S  k = 0;  k < _size;  ++k)
    {
        printf( _seq, list[k]);
    }
    printf( "\n");
}

int
listop( int const *  _list)
{
    if ( _list && ( *_list != -1))
    {
        CBM_LogDebug( "i=",*_list);
        int  r = listop( _list+1) + *_list;
        CBM_LogDebug( "i=",*_list, "  r=",r);
        return  r;
    }
    return  0;
}
int
listsop()
{
    static int const  int_l[] = { 2,3,4,5,6, -1 /*sentinel*/};
    CBM_LogWrite( "i1*i2*..*iN=", listop( int_l));

    return  1;
}


int
main(
        int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);
    printf( "|bool| = %u, size-a1-no-buf=%u, size-a1=%u, size-a2=%u, size-a3=%u, a4=%u, a5=%u, a6=%u\n\n",
            (unsigned int)sizeof( bool),
            (unsigned int)lvector_memsize_t< vector_flt_t >::SIZE_BUFFER,
            (unsigned int)lvector_memsize_t< vector_flt_t >::SIZE_VECTOR,
            (unsigned int)lvector_memsize_t< matrix_2d_flt_t >::SIZE_VECTOR,
            (unsigned int)lvector_memsize_t< lvector_t< float, 3 > >::SIZE_VECTOR,
            (unsigned int)lvector_memsize_t< lvector_t< float, 4 > >::SIZE_VECTOR,
            (unsigned int)lvector_memsize_t< lvector_t< float, 5 > >::SIZE_VECTOR,
            (unsigned int)lvector_memsize_t< lvector_t< float, 6 > >::SIZE_VECTOR
            );

    unsigned int  c(0);

    {
        printf( "\ncase %u:\n", ++c);
        CBM_LogDebug( "0:----");

        vector_flt_t::extent_type  dim1 = 2;
        vector_flt_t  a1( dim1, cbm::PI);
        a1 *= -23.0;
        CBM_LogDebug( "a1=", a1);
        a1[0] = cbm::PI;
        printf( "a1-capacity = %u, a1[0] = %.6f, a1[1] = %.6f\n", (unsigned int)a1.capacity(), a1[0], a1[1]);
        std::cout << "a1="<<a1 << std::endl;

        CBM_LogDebug( "1:----");

        vector_flt_t  b1( a1);
        a1 = 17.9;
        std::cout << "b1="<<b1 << "  a1="<<a1<< std::endl;

        CBM_LogDebug( "2:----");
        
        vector_flt_t  c1 = a1;
        std::cout << "c1="<<c1 << std::endl;

        CBM_LogDebug( "3:----");

        vector_flt_t  d1( dim1, 42.0);
        a1 = d1;
        std::cout << "a1="<<a1 << std::endl;
        d1[1] = 21.0;
        std::cout << "a1="<<a1 << "  d1="<<d1 << std::endl;

        CBM_LogDebug( "4:----");

        vector_flt_t const &  e1( c1);
        std::cout << "e1="<<-e1 << std::endl;
        std::cout << "e1="<<(e1/(a1[0]*3.0)) + ((float)0.2*(-e1))/e1 << std::endl;
        // not allowed  e1[0] = 4.0;
        CBM_LogDebug( "e1[0]=", e1[0]);
    }

    CBM_LogDebug( "++++");

    {
        printf( "\ncase %u:\n", ++c);
        CBM_LogDebug( "0:----");

        matrix_2d_flt_t::extent_type const  dim2[] = { 2, 3};
        matrix_2d_flt_t  a2( dim2, 2.0*cbm::PI);
        a2[1][2] = (matrix_2d_flt_t::value_type)10.0;
        printf( "a2-capacity = %u, a2[0]-capacity = %u, a2[1,3] = %.6f, a2[2,3] = %.6f\n", (unsigned int)a2.capacity(), (unsigned int)a2[0].capacity(), a2[0][2], a2[1][2]);
        printf( "a2-sum = %.6f\n", a2.sum());

        CBM_LogDebug( "1:----");

        vector_flt_t::extent_type const  dim1 = dim2[1];
        vector_flt_t  a1( dim1, 1.0);

        matrix_2d_flt_t const &  b2( a2);
        /* nasty.. ;-) */
        std::cout << "b2="<<b2 << std::endl;
        b2[0] = a1;
        std::cout << "b2="<<b2 << std::endl;
        printf( "b2[0]-sum = %.6f, b2[0,0] = %.6f\n", b2[0].sum(), b2[0][0]);

        CBM_LogDebug( "2:----");

        matrix_2d_flt_t  c2( a2);
        c2[0] = -1.0;
        c2[1] = a1;
        std::cout << "c2="<<c2 << std::endl;

        CBM_LogDebug( "3:----");

        matrix_2d_flt_t  d2( dim2);
        d2 = a2;
        std::cout << "d2="<<d2 << std::endl;
    }
    {
        matrix_2d_flt_t::extent_type const  dim2[] = { 2, 3};
        matrix_2d_int_t  a2( dim2, 3);
        std::cout << "a2="<<a2 << std::endl;
        std::cout << "-a2="<<-a2 << std::endl;
        std::cout << "~a2="<<~a2 << "   ~3="<<~3<< std::endl;
        std::cout << "!a2="<<!a2 << std::endl;
        matrix_2d_int_t  d2( dim2, 2);
        std::cout << "d2="<<d2 << std::endl;
// sk:off        d2 = -a2;
// sk:off        std::cout << "d2=" << d2 << std::endl;
// sk:off        d2 = ~a2;
// sk:off        std::cout << "d2=" << d2 << std::endl;
// sk:off        d2 = !a2;
// sk:off        std::cout << "d2=" << d2 << std::endl;
        matrix_2d_int_t  c2 = (4+d2) + (9*a2);
        std::cout << "c2="<<c2 << std::endl;
//        d2 = 5*a2;
//        std::cout << "d2=" << d2 << std::endl;
    }
    {
        printf( "\ncase 3d:\n");
        matrix_3d_flt_t::extent_type const  dim3[] = { 2, 3, 4};
        matrix_3d_flt_t  a3( dim3, 2.0);
        std::cout << "["<<a3.rank()<<","<<a3.size()<<"]  a3="<<a3 << std::endl;
        printf( "a3-capacity = %u, a3[0]-capacity = %u, a3[0][0]-capacity = %u, a3[1,2,3] = %.6f\n", (unsigned int)a3.capacity(), (unsigned int)a3[0].capacity(), (unsigned int)a3[0][0].capacity(), a3[0][1][2]);
        printf( "a3-size = %u, a3[0]-size = %u, a3[0][0]-size = %u, a3[1,2,3] = %.6f\n", (unsigned int)a3.size(), (unsigned int)a3[0].size(), (unsigned int)a3[0][0].size(), a3[0][1][2]);
        printf( "a3-size = %u, a3-size-0 = %u, a3-size-1 = %u, a3-size-2 = %u\n", (unsigned int)a3.size(), (unsigned int)a3.extent(0), (unsigned int)a3.extent(1), (unsigned int)a3.extent(2));

        matrix_3d_flt_t  a3_a( a3);
        printf( "a3'-size = %u, a3'-size-0 = %u, a3'-size-1 = %u, a3'-size-2 = %u\n", (unsigned int)a3_a.size(), (unsigned int)a3_a.extent(0), (unsigned int)a3_a.extent(1), (unsigned int)a3_a.extent(2));


        matrix_2d_flt_t::extent_type const  dim2[] = { dim3[1], dim3[2]};
        matrix_2d_flt_t  a2( dim2, 4.0);

        printf( "|a3[1]| = %d, |a2| = %d\n", (int)a3[1].capacity(), (int)a2.capacity());
        std::cout << "a3=" << a3 << std::endl;
        a3[1] = a2;
        std::cout << "a3=" << a3 << std::endl;

        /* crash */
        //a3[0][9][0] = -1.0;

#define  MA_N  (100000)
        vector_flt_t  ma[MA_N];
        int  j = 0;
#ifdef  CRABMEAT_OPENMP
#  pragma omp parallel
#  pragma omp for private(j)
#endif
        for ( j = 0;  j < MA_N;  ++j)
        {
            ma[j].resize_and_preserve( dim2[1], static_cast< float >( j));
            ma[j] = static_cast< float >( j);
            if ( j > MA_N-1-cbm::omp::get_num_threads())
            {
                CBM_LogDebug( "ma-",j,"=  ",ma[j]);
            }
        }

        CBM_LogDebug( "-------------------------------");
        matrix_2d_flt_t::extent_type const  dim2b[] = { 0, 1};
#ifdef  CRABMEAT_OPENMP
#  pragma omp parallel
#  pragma omp for private(j)
#endif
        for ( j = 0;  j < MA_N;  ++j)
        {
            ma[j].resize_and_preserve( dim2b[1], static_cast< float >( j));
            ma[j] = static_cast< float >( j);
            if ( j > MA_N-1-cbm::omp::get_num_threads())
            {
                CBM_LogDebug( "ma-",j,"=  ",ma[j]);
            }
        }


    }

// sk:off    listsop();

}

