/*!
 * @brief
 *    test app for (nested) string expansion,
 *    i.e. expanding %X and ${VAR} sequences
 *
 * @author
 *    steffen klatt (created on: mar 04, 2015)
 */

#include  <stdio.h>

#include  "string/cbm_string.h"
#include  "time/cbm_time.h"
#include  "utils/cbm_utils.h"

#include  "cbm_rtcfg.h"

int
stringexpand_expand(
        char const *  _str, char const *  _expected)
{
    std::string  s;
    lerr_t  rc_expand = cbm::format_expand( &s, _str);
    if ( rc_expand || !cbm::is_equal( s, _expected))
    {
        fprintf( stderr, "[%d] failed to expand \"%s\"\n", (int)rc_expand, _str);
        fprintf( stderr, "\t-> \"%s\"  (expected \"%s\")\n", s.c_str(), _expected);
        return  1;
    }
    return  0;
}

int
stringexpand_expand_fail(
        char const *  _str)
{
    std::string  s;
    lerr_t  rc_expand = cbm::format_expand( &s, _str);
    if ( !rc_expand)
    {
        fprintf( stderr, "[%d] expanded string that was expected to fail \"%s\"\n", (int)rc_expand, _str);
        return  1;
    }
    return  0;
}

void
stringexpand_run_tests()
{
    cbm::setenv( "FOO", "foo", 1);
    cbm::setenv( "foo", ":-o", 1);
    cbm::setenv( "BAR", "bar", 1);
    cbm::setenv( "foobar", ":-)", 1);

    cbm::setenv( "X", "${X}", 1); /*evil*/

    int  n_fails = 0;
    char const *  S01 = "${FOO}";
    n_fails += stringexpand_expand( S01, "foo");

    char const *  S02 = "${BAR}${FOO}";
    n_fails += stringexpand_expand( S02, "barfoo");

    char const *  S03 = "${${FOO}${BAR}}";
    n_fails += stringexpand_expand( S03, ":-)");

    char const *  S04 = "${{${FOO}}";
    n_fails += stringexpand_expand( S04, "${{foo}");

    char const *  S05 = "${${FOO}}";
    n_fails += stringexpand_expand( S05, ":-o");

    char const *  S06 = "${{${FOO}}}";
    n_fails += stringexpand_expand( S06, "");

    char const *  S07 = "${foo${BAR}}";
    n_fails += stringexpand_expand( S07, ":-)");

    char const *  S08 = "{foobar}";
    n_fails += stringexpand_expand( S08, "{foobar}");

    char const *  S09 = "{{foobar}";
    n_fails += stringexpand_expand( S09, "{{foobar}");

    char const *  S10 = "{foobar}}";
    n_fails += stringexpand_expand( S10, "{foobar}}");

    char const *  S11 = "{$foobar}";
    n_fails += stringexpand_expand( S11, "{$foobar}");

    char const *  S12 = "$$";
    n_fails += stringexpand_expand( S12, "$$");

    char const *  S13 = "$${FOO}";
    n_fails += stringexpand_expand( S13, "$foo");

    char const *  S14 = "${}";
    n_fails += stringexpand_expand( S14, "${}");

    char const *  S15 = "${**foo*bar**}"; /*unlikely defined in environment*/
    n_fails += stringexpand_expand( S15, "");

    char  m_pid[16];
    cbm::snprintf( m_pid, 16, "%012d", static_cast< int >( cbm::get_pid()));

    char const *  S16 = "%012p";
    n_fails += stringexpand_expand( S16, m_pid);

    cbm::setenv( "M_PID", "%012p", 1);
    char const *  S17 = "${M_PID}";
    n_fails += stringexpand_expand( S17, m_pid);

    CBM_LibRuntimeConfig.io.input_path = "input/base/path";
    char const *  S18 = "%I";
    n_fails += stringexpand_expand( S18, "input/base/path");

    CBM_LibRuntimeConfig.io.output_path = "output/base/path";
    char const *  S19 = "%O";
    n_fails += stringexpand_expand( S19, "output/base/path");

    char const *  S20 = "%%"; /*shall fail*/
    n_fails += stringexpand_expand_fail( S20);

    ltime_t  tm;
    cbm::string_t  tm_s = "2004-06-10-17/24";
    cbm::sclock_t  clk( tm.parse( tm_s));
    CBM_LibRuntimeConfig.clk = &clk;
    char const *  S21 = "%Y";
    n_fails += stringexpand_expand( S21, "2004");


    if ( n_fails)
    {
        fprintf( stderr, "%d expansion%s failed\n", n_fails, n_fails==1?"":"s");
    }
}

int
main(
        int  argc, char *  argv[])
{
    CRABMEAT_FIX_UNUSED(argc); CRABMEAT_FIX_UNUSED(argv);

    stringexpand_run_tests();

    if ( argc < 2)
    {
        fprintf( stderr, "give me a string man!\n");
		return  1;
    }

	char const *  s = argv[1];
    printf( "string before expanding \"%s\"\n", s);
	std::string  expanded_s;
	cbm::format_expand( &expanded_s, s);
    printf( "string after expanding  \"%s\"\n", expanded_s.c_str());
}

