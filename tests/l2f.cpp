/*!
 * @brief
 *    split string into list of items of numeric type
 *    using different approaches
 *
 * @author
 *    steffen klatt (created on: jun 30, 2013),
 */

#include  <string.h>
#include  <stdlib.h>
#include  <stdio.h>

#include  "string/lstring-basic.h"
//#define  _LDNDC_MEASURE_RESOURCES
#include  "liblog/res.h"

#include  "crabmeat.h"

#define  L2F_RUNS  1 //(1<<20)
#define  L2F_BUFSIZE  512


int
l2f_chararray( char *  _rec_d, char const *  _rec_s)
{
    int  rsize = cbm::strlen( _rec_s);
    if ( rsize > L2F_BUFSIZE)
    {
        fprintf( stderr, "%d vs. %d\n", rsize, L2F_BUFSIZE);
        return  -1;
    }
    printf( "record-size=%d\n", rsize);
    memcpy( _rec_d, _rec_s, rsize);
    _rec_d[rsize] = '\0';

    int  c = 0;
    while ( _rec_d[c] != '\0')
    {
        printf( "%c", _rec_d[c]);
        ++c;
    }
    printf( "\n");

    return  rsize;
}

int
l2f_extract_items_atof( double *  _f, char *  _rec, int  _rsize)
{
    /* assumption: first character is non-blank */
    int  p = 0;
    int  q = 0;
    for ( int  k = 0;  k < _rsize+1;  ++k)
    {
        if (( is_space(_rec[k]) || ( _rec[k] == '\0')) && ( p < k))
        {
            _f[q] = atof( _rec+p);
            ++q;
        }
        if ( is_space(_rec[k]))
        {
            p = k+1;
        }
    }

    return  q;
}

int
l2f_extract_items_strtod( double *  _f, char *  _rec, int  /*_rsize*/)
{
    char *  endptr = _rec;
    char *  nptr = _rec;
    double  f = 0.0;
    int  p = 0;
    while ( endptr)
    {
        f = strtod( nptr, &endptr);
        if ( nptr != endptr)
        {
            _f[p] = f;
            nptr = endptr;
            ++p;
            continue;
        }
        endptr = NULL;
    }

    return  p;
}

int
l2f_extract_items_stringstream( double *  _f, char *  _rec, std::istringstream &  _iss)
{
    _iss.str( _rec);
    double  f = 0.0;
    int  p = 0;

    while ( _iss >> f)
    {
        _f[p] = f;
        ++p;
    }
    _iss.clear();

    return  p;
}

int
l2f_usage( int  _rv, char const *  _exmpl_l, int  _exmpl_m)
{
    fprintf( stderr, "l2f  [\"<blanks separated floats>\" [<method>]]\n");
    fprintf( stderr, "method is one of 'a' (atof), 'd' (strtod), 's' (std::istringstream)\n");
    fprintf( stderr, "example:\nl2f  \"%s\" %c\n", _exmpl_l, _exmpl_m);

    return  _rv;
}

int
main(
        int  argc, char *  argv[])
{
    char const *  exmpl = "1.12  \t  2.65 3.47  \t4.21 785.0  0.0 7.0 872.1   647.1656871646  8";
    if ( argc == 1)
    {
        l2f_usage( 1, exmpl, 0);
    }
    char const *  l_rec = (( argc < 2) || ( argv[1][0] == '-')) ? exmpl : argv[1];
    char  method = ( argc < 3) ? 'a' : argv[2][0];
    fprintf( stderr, "using method '%c'\n", method);

    char  rec[L2F_BUFSIZE+1];
    int  recsize = l2f_chararray( rec, l_rec);
    if ( recsize < 1)
    {
        return  recsize;
    }
    double  f_rec[L2F_BUFSIZE];
    int  f_size = 0;

    std::istringstream  iss;

    TIC(lruntime);
    for ( int  k = 0;  k < L2F_RUNS;  ++k)
    {
        switch ( method)
        {
/* METHOD 2:  using stdtod */
            case 'a':
                f_size = l2f_extract_items_atof( f_rec, rec, recsize);
                break;
/* METHOD 2:  using stdtod */
            case 'd':
                f_size = l2f_extract_items_strtod( f_rec, rec, recsize);
                break;
/* METHOD 2:  using stringstream */
            case 's':
                f_size = l2f_extract_items_stringstream( f_rec, rec, iss);
                break;
            default:
                fprintf( stderr, "unknown method\n");
                return  l2f_usage( 1, exmpl, 0);
        }
    }
    TOC(lruntime);
    fprintf( stderr, "sim-time=%.8f\n", lruntime);

    for ( int  p = 0;  p < f_size;  ++p)
    {
        printf( "%3d = %.12f\n", p+1, f_rec[p]);
    }
}

