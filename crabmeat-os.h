/*!
 * @brief
 *    attempts to detect platform and compiler. we
 *    also set compiler specific and platform specific
 *    macros.
 *
 *    based on:
 *    http://sourceforge.net/apps/mediawiki/predef/index.php?title=Main_Page
 *    or
 *    http://sourceforge.net/p/predef/wiki/Compilers/
 *
 * @author 
 *    steffen klatt (created on: sep 02, 2012)
 */

#ifndef  CRABMEAT_OS_H_
#define  CRABMEAT_OS_H_

#include  "crabmeat-arch.h"

/** platform detection **/

/* linux */
#if  defined(__linux) || defined(__linux__) || defined(linux)
#  define  CRABMEAT_OS_LINUX
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_LINUX32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_LINUX64
#  else
#    error  "linux platform detection problem"
#  endif

/* freebsd */
#elif  defined(__FreeBSD__)
#  define  CRABMEAT_OS_FREEBSD
#  define  CRABMEAT_OS_BSD
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_FREEBSD32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_FREEBSD64
#  else
#    error  "freebsd platform detection problem"
#  endif

/* netbsd */
#elif  defined(__NetBSD__)
#  define  CRABMEAT_OS_NETBSD
#  define  CRABMEAT_OS_BSD
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_NETBSD32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_NETBSD64
#  else
#    error  "netbsd platform detection problem"
#  endif

/* openbsd */
#elif  defined(__OpenBSD__)
#  define  CRABMEAT_OS_OPENBSD
#  define  CRABMEAT_OS_BSD
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_OPENBSD32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_OPENBSD64
#  else
#    error  "openbsd platform detection problem"
#  endif

/* hp-ux */
#elif  defined(_hpux)
#  define  CRABMEAT_OS_HPUX
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_HPUX32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_HPUX64
#  else
#    error  "hp-ux platform detection problem"
#  endif

/* solaris */
#elif  defined(sun) || defined(_sun)
#  define  CRABMEAT_OS_SUN
#  if  defined(__SVR4) || defined(__svr4__)
#    define  CRABMEAT_OS_SUNSOL
#  else
#    define  CRABMEAT_OS_SUNOS
#  endif
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_SUN32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_SUN64
#  else
#    error  "sun solaris platform detection problem"
#  endif

/* mac os x */
#elif  defined(__APPLE__) && defined(__MACH__)
#  define  CRABMEAT_OS_MACOSX
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_MACOSX32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    if defined(CRABMEAT_ARCH_arm_64)
#      define  CRABMEAT_OS_MACOSXARM64
#    else
#      define  CRABMEAT_OS_MACOSX64
#    endif
#  else
#    error  "mac os x platform detection problem"
#  endif

/* cygwin */
#elif  defined(__CYGWIN__)
#  define  CRABMEAT_OS_CYGWIN
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_CYGWIN32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_CYGWIN64
#  else
#    error  "cygwin platform detection problem"
#  endif

/* ms windows flavors */
#elif  defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(WIN64) || defined(_WIN64) || defined(__WINDOWS__) || defined(_MSC_VER)
#  define  CRABMEAT_OS_MSWIN
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_MSWIN32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_MSWIN64
#  else
#    error  "ms windows platform detection problem"
#  endif

/* ms dos .. ?! */
#elif  defined(MSDOS) ||  defined(_MSDOS) || defined(__MSDOS__) || defined(__DOS__)
#  define  CRABMEAT_OS_MSDOS
#  error  "comment out this error line and give it a shot. we would like to hear from you."

/* ? */
#else
        /* give us feedback :-) */
#endif


/* do we deal with a unixoid system? */
#if  defined(__unix) || defined(__unix__) || defined(unix) || defined(CRABMEAT_OS_LINUX) || defined(CRABMEAT_OS_MACOSX) || defined(CRABMEAT_OS_BSD) || defined(CRABMEAT_OS_HPUX) || defined(CRABMEAT_OS_SUN)
#  define  CRABMEAT_OS_UNIXOID
#  if  defined(CRABMEAT_ARCH_IS_32)
#    define  CRABMEAT_OS_UNIXOID32
#  elif  defined(CRABMEAT_ARCH_IS_64)
#    define  CRABMEAT_OS_UNIXOID64
#  else
#    error  "is there such a thing?!"
#  endif
#endif



#ifdef  CRABMEAT_OS_UNIXOID
#  ifndef  _HAVE_ASCII_COLOR
#    define  _HAVE_ASCII_COLOR
#  endif
#else
#  ifdef  _HAVE_ASCII_COLOR
#    undef  _HAVE_ASCII_COLOR
#  endif
#endif



#endif  /*  !CRABMEAT_OS_H_  */

